<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Database\PostgreSQL;

	class Opcenter
	{
		const DATABASE = POSTGRESQL_DATABASE;
		/**
		 * @var \PDO $dbh database handler
		 */
		private $dbh;

		/**
		 * @var bool transaction pending
		 */
		private $pending = false;

		public function __construct(\PDO $dbh)
		{
			$this->dbh = $dbh;
		}

		public function beginTransaction(): bool
		{
			$this->pending = true;

			return $this->dbh->beginTransaction();
		}

		public function commit(): bool
		{
			if ($ret = $this->dbh->commit()) {
				$this->pending = false;
			}

			return $ret;
		}

		public function __destruct()
		{
			if ($this->pending) {
				if (is_debug()) {
					warn("`%s' fell out of scope without commit(), rolling back database", static::class);
				}
				$this->dbh->rollBack();
			}
		}

		/**
		 * Populate site
		 *
		 * @param int    $site_id
		 * @param string $domain
		 * @param string $email
		 * @param string $admin_user
		 * @return bool
		 */

		public function createSite(int $site_id, string $domain, string $email, string $admin_user): bool
		{
			$stmt = $this->dbh->prepare('INSERT INTO siteinfo (site_id, domain, email, admin_user) ' .
				'VALUES(:site_id, :domain, :email, :admin_user)'
			);
			$resp = $stmt->execute([
				'site_id'    => $site_id,
				'domain'     => $domain,
				'email'      => $email,
				'admin_user' => $admin_user
			]);

			return $resp && !(int)$stmt->errorCode() ?: error(implode(': ', $stmt->errorInfo()));
		}

		/**
		 * Change domain for site id
		 *
		 * @param int    $site_id
		 * @param string $domain
		 * @return bool
		 */
		public function changeDomain(int $site_id, string $domain): bool
		{
			$stmt = $this->dbh->prepare('UPDATE siteinfo SET domain = :domain WHERE site_id = :site_id');
			$resp = $stmt->execute([
				'site_id' => $site_id,
				'domain'  => $domain,
			]);

			return $resp && !(int)$stmt->errorCode() ?: error(implode(': ', $stmt->errorInfo()));
		}

		/**
		 * Remove a site ID from database
		 *
		 * @param int    $site_id site id
		 * @param string $domain  confirmation domain to delete site
		 * @return bool
		 */
		public function deleteSite(int $site_id, string $domain): bool
		{
			$stmt = $this->dbh->prepare('DELETE FROM siteinfo WHERE site_id = :site_id AND domain = :domain');

			return $stmt->execute([
				'site_id' => $site_id,
				'domain'  => $domain
			]);
		}

		public function changeEmail(int $site_id, string $email): bool
		{
			$stmt = $this->dbh->prepare('UPDATE siteinfo SET email = :email WHERE site_id = :site_id');

			return $stmt->execute([
				'site_id' => $site_id,
				'email'   => $email
			]);
		}

		/**
		 * Read site list from siteinfo table
		 *
		 * @return array [siteid => domain]
		 */
		public function readSitesFromSiteinfo(): array
		{
			$rs = $this->dbh->query('SELECT site_id, domain FROM siteinfo');
			$sites = [];
			while (false !== ($row = $rs->fetchObject())) {
				$sites[$row->site_id] = $row->domain;
			}
			return $sites;
		}
	}