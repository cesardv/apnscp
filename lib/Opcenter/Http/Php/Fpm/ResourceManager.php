<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

namespace Opcenter\Http\Php\Fpm;

use Opcenter\System\Cgroup;

class ResourceManager {
	// @var group
	protected $group;

	public function getGroup() {
		return $this->group ?? 'system.slice';
	}

	public function setGroup(string $name) {
		$group = new Cgroup\Group($name);
		if (!Cgroup::exists(Cgroup\Controllers\Memory::make($group, null), $group)) {
			warn("Named group `%s' does not exist", $name);
		}
		$this->group = $name;
	}
}