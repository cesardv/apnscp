<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2018
	 */

	namespace Opcenter\Http;


	use Opcenter\Process;

	class Apnscp
	{
		private const PID_FILE = 'httpd.pid';
		public const CHECK_URL = 'http://localhost:' . \Auth_Redirect::CP_PORT;

		/**
		 * Verify frontend is processing requests
		 *
		 * @return bool
		 */
		public static function check(): bool
		{
			return silence(static function () {
				$ctx = stream_context_create(array(
					'http' =>
						array(
							'timeout' => 15,
							'method'  => 'HEAD',
							'header'  => [
								'User-agent: apnscp Internal check'
							]
						)
				));

				return (bool)get_headers(static::CHECK_URL, PHP_VERSION_ID >= 80000 ? false : 0, $ctx);
			});

		}

		/**
		 * Stop frontend
		 *
		 * @return bool
		 */
		public static function stop(): bool
		{
			if (!static::running()) {
				info('Frontend is not running - attempting stop anyway');
			}
			$ret = static::command('stop');

			return $ret['return'] > 0;
		}

		/**
		 * apnscpd frontend is running
		 *
		 * @return bool
		 */
		public static function running(): bool
		{
			if (!($pid = static::getPid())) {
				return false;
			}

			return Process::pidMatches($pid, 'httpd') && posix_kill($pid, 0);
		}

		/**
		 * Kill HTTP workers exceeding memory
		 *
		 * @return int
		 */
		public static function cull(): int
		{
			if (APNSCPD_HEADLESS || !static::running()) {
				return 0;
			}
			$pid = static::getPid();

			if (!$children = Process::children($pid)) {
				return 0;
			}

			$killed = 0;
			foreach (Process::statm($children) as $pid => $child) {
				if ($child['resident']/1024 > APNSCPD_HTTP_HIGH_WATERMARK) {
					dlog("Killed pid `%(pid)d' (RSS %(high)lu MB > %(mark)lu MB)", [
						'pid' => $pid,
						'high' => $child['resident']/1024,
						'mark' => APNSCPD_HTTP_HIGH_WATERMARK
					]);
					Process::kill($pid, SIGTERM) && $killed++;
				}
			}

			return $killed;
		}

		private static function getPid(): ?int
		{
			if (!file_exists($file = run_path(self::PID_FILE))) {
				return null;
			}

			return (int)file_get_contents($file);
		}

		/**
		 * Process apnscpd frontend command
		 *
		 * @param string $mode
		 * @return array
		 */
		private static function command(string $mode): array
		{
			if (!\in_array($mode, ['start', 'restart', 'graceful', 'graceful-stop', 'stop'], true)) {
				return error("Unknown command type `%s'", $mode);
			}
			$env = ['', 'APNSCP_ROOT=' . INCLUDE_PATH];
			if (getenv('PREFORK')) {
				$env[] = 'PREFORK';
			}
			if (FRONTEND_DISPATCH_HANDLER === 'fpm') {
				$env[] = 'FPM';
			}
			if (FRONTEND_HTTPS_ONLY) {
				$env[] = 'SECURE';
			}
			$args = [
				'process' => '/usr/sbin/httpd',
				'config'  => config_path('httpd.conf'),
				'args'    => implode(' -D', $env),
			];
			$procClass = \Util_Process_Fork::class;
			if ($mode === 'stop') {
				$procClass = \Util_Process::class;
			}
			$proc = new $procClass();
			// otherwise Apache won't process
			$proc->setOption('leader', true);
			// 384 MB limit per process
			$proc->setOption('resource', [
				'rss' => (int)APNSCPD_RLIMIT_RSS*1024**2
			]);
			$proc->setEnvironment('HOSTNAME', getenv('HOSTNAME'));
			return $proc->run('%(process)s -f %(config)s %(args)s -k ' . $mode, $args);
		}

		/**
		 * Start apnscpd frontend
		 *
		 * @return bool
		 */
		public static function start(): bool
		{
			if (APNSCPD_HEADLESS) {
				return warn('Cannot start apnscp frontend - running in headless mode');
			}

			if (static::running()) {
				return warn('%s already running. Ignoring start command.', PANEL_BRAND);
			}

			if (file_exists($pidFile = run_path(self::PID_FILE))) {
				unlink($pidFile);
			}
			$ret = static::command('start');
			if ($ret['return']) {
				info("Frontend started with pid `%s'", $ret['return']);
			}

			return $ret['return'] > 0;
		}

		/**
		 * Reload frontend
		 *
		 * @return bool
		 */
		public static function reload(): bool
		{
			if (APNSCPD_HEADLESS) {
				return true;
			}
			$ret = static::command('graceful');

			return $ret['return'] > 0;
		}
	}
