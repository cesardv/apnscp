<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

namespace Opcenter\Crypto\Letsencrypt\Solvers;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use HTTP\SelfReferential;
use Module\Support\Letsencrypt;
use Opcenter\Filesystem;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use function debug_ex as debug;

class Http extends Base {
	// @var string file path
	protected $path;

	public function __destruct()
	{
		if (!$this->path || !file_exists($this->path)) {
			return;
		}
		unlink($this->path);
	}

	public function canUse(): bool
	{
		$domain = $this->authorization->getDomain();

		// substitute wildcard matches
		if (0 === strncmp($domain, '*.', 2)) {
			return false;
		}

		return $this->reachable($domain);
	}

	/**
	 * Domain is reachable
	 *
	 * @param string $domain
	 * @return bool
	 */
	public function reachable(string $domain): bool
	{
		if (0 === strncmp($domain, '*.', 2)) {
			$domain = substr($domain, 2);
		}

		if (!preg_match(\Regex::DOMAIN, $domain)) {
			return error("Skipping SSL for domain `%s' - domain is not fully-qualified", $domain);
		}

		$dir = Letsencrypt::ACME_WORKDIR . Letsencrypt::ACME_URI_PREFIX;
		$path = $dir . '/' . uniqid('acme-test', true);
		if (!is_dir($dir)) {
			Filesystem::mkdir($dir);
		}
		$marker = uniqid('ssl-check', true);
		file_put_contents($path, $marker);
		$uri = substr($path, \strlen(Letsencrypt::ACME_WORKDIR));

		$http = SelfReferential::instantiateContexted($this->getAuthContext(), [$domain, \Net_Gethost::gethostbyname_t($domain) ?: null]);
		$http->setOption(
			RequestOptions::ALLOW_REDIRECTS . '.on_redirect',
			static function (RequestInterface $request, ResponseInterface $response, UriInterface $uri) use ($domain) {
				if (!preg_match('!^https?://' . preg_quote($domain, '!') . '(?::\d+)?!', (string)$uri)) {

					throw new RequestException(
						sprintf('Domain deviated from expected in redirect: %s to %s', $domain, $uri->getHost()),
						$request,
						$response
					);
				}
				info('%(code)d redirect encountered to %(location)s',
					['code' => $response->getStatusCode(), 'location' => $uri]);
			});

		try {
			$response = $http->get($uri);
			$code = $response->getStatusCode();
			switch ($code) {
				case 200:
					$contents = $response->getBody()->getContents();

					return trim(strip_tags($contents)) === $marker;
				default:
					warn("Request for `%(location)s' returned HTTP code %(code)d",
						['location' => $uri, 'code' => $code]);

					return false;
			}
		} catch (\Exception $e) {
			debug('Invalid response on HTTP solver: %s', $e->getMessage());
			return false;
		} finally {
			file_exists($path) && unlink($path);
		}

		return false;
	}

	public function resolve(): bool
	{
		$dir = Letsencrypt::ACME_WORKDIR . Letsencrypt::ACME_URI_PREFIX;
		$this->path = $dir . '/' . $this->token;
		debug("HTTP: setting `%s' in `%s'", $this->payload, $this->path);
		if (!\is_dir($dir)) {
			Filesystem::mkdir($dir, APACHE_UID, APACHE_GID, 0110);
		}
		return file_put_contents($this->path, $this->payload) > 0;
	}

}

