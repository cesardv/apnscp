<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

namespace Opcenter\Crypto\Letsencrypt;

use AcmePhp\Core\AcmeClient;
use AcmePhp\Core\Exception\AcmeCoreClientException;
use AcmePhp\Core\Exception\AcmeCoreServerException;
use AcmePhp\Core\Exception\Protocol\CertificateRequestFailedException;
use AcmePhp\Core\Exception\Protocol\ChallengeFailedException;
use AcmePhp\Core\Exception\Protocol\ChallengeTimedOutException;
use AcmePhp\Core\Protocol\AuthorizationChallenge;
use AcmePhp\Core\Protocol\CertificateOrder;
use AcmePhp\Ssl\CertificateRequest;
use AcmePhp\Ssl\DistinguishedName;
use Lararia\Bootstrapper;
use Opcenter\Crypto\Letsencrypt;
use Opcenter\Crypto\Letsencrypt\Contracts\DispatchableModuleInterface;
use Opcenter\Crypto\Letsencrypt\DispatchServices\Acmephp;
use Opcenter\Filesystem;
use function debug_ex as debug;

class AcmeDispatcher implements DispatchableModuleInterface {
	use \ContextableTrait;
	use \NamespaceUtilitiesTrait;

	const VALIDATION_MODES = [
		'dns',
		'http'
	];

	/**
	 * @var string challenge mode
	 */
	protected $mode = 'http';
	private $endpoint;

	// stage result, don't complete challenge (for use with DNS)
	private $stage = false;

	/**
	 * @var bool strict mode - loss of 1 certificate invalidates order
	 */
	protected $strict = false;

	/**
	 * @var string storage prefix for certificates
	 */
	protected $storageMarker;


	/**
	 * @var AcmeClient
	 */
	private $client;

	/**
	 * @var array pruned hostnames in non-strict mode
	 */
	private $pruned;

	/**
	 * @var array hosts authorized
	 */
	private $succeeded = [];

	/**
	 * AcmeDispatcher constructor.
	 *
	 * @param string|null $endpoint
	 */

	// @var array saved global buffer
	private $savedBuffer = [];

	private function __construct(string $marker = null, string $endpoint = null)
	{
		if (!$endpoint) {
			$endpoint = Letsencrypt::activeServer();
		}
		$this->endpoint = $endpoint;
		$this->storageMarker = $marker;
		$this->client = Acmephp::client($this->getAuthContext());
		$this->savedBuffer = \Error_Reporter::flush_buffer();
	}

	public function __destruct()
	{
		\Error_Reporter::set_buffer($this->savedBuffer);
	}

	/**
	 * Stage request only, do not complete
	 *
	 * @param bool $yes
	 * @return AcmeDispatcher
	 */
	public function setStagingOnly(bool $yes): self
	{
		$this->stage = $yes;
		return $this;
	}

	/**
	 * Set strict mode
	 *
	 * @param bool $mode
	 * @return $this
	 */
	public function setStrictMode(bool $mode): self
	{
		$this->strict = $mode;
		return $this;
	}

	/**
	 * SSL request is staged prior to completion
	 *
	 * @return bool
	 */
	public function isStaged(): bool
	{
		return $this->stage;
	}

	protected function getStorageDirectory(): string
	{
		return Letsencrypt::acmeDataDirectory();
	}

	/**
	 * Set preferred challenge mechanism
	 *
	 * @param string $mode
	 */
	public function setChallengeMechanism(string $mode): void
	{
		if ($this->mode === $mode) {
			return;
		}
		if (!\in_array($mode, static::VALIDATION_MODES)) {
			fatal("Unknown challenge mode `%s'", $mode);
		}
		$this->mode = $mode;
	}

	/**
	 * Issue SSL certificates
	 *
	 * @param string|array $hostnames
	 * @return bool
	 */
	public function issue($hostnames): bool
	{
		try {
			$challenges = $this->challenges((array)$hostnames);
		} catch (\ArgumentError | AcmeCoreServerException $ex) {
			return error('Failed acquiring challenges for one or more domains: %s', $ex->getMessage());
		}

		try {
			return (bool)$this->solve($challenges);
		} catch (AcmeCoreServerException $e) {
			$method = new \ReflectionMethod($this->client, 'getHttpClient');
			$method->setAccessible(true);
			if ($method->invoke($this->client)->getLastCode() === 400 && !(new Acmephp\KeypairFinder($this->getAuthContext()))->registered()) {
				/**
				 * @TODO use email from site?
				 */
				if (!$this->register(null)) {
					fatal('Failed to register, cannot request certificate!');
				}
				return $this->issue($hostnames);
			}
			if (false !== strpos($e->getMessage(), '[malformed]')) {
				return error('Failed to issue certificate. ACME server reported the request was malformed. Invalid client key perhaps? Response: %s', $e->getMessage());
			}
			return error('Failed to issue certificate. ACME server responded: %s', $e->getMessage());
		}
	}

	/**
	 * Get challenges from set
	 *
	 * @param array $hostnames
	 * @return CertificateOrder
	 */
	public function challenges(array $hostnames): CertificateOrder
	{
		foreach ($hostnames as $hostname) {
			if (0 === strncmp($hostname, '*.', 2)) {
				$hostname = substr($hostname, 2);
			}
			if (!preg_match(\Regex::DOMAIN, $hostname)) {
				throw new \ArgumentError("Invalid hostname provided `${hostname}'");
			}
		}

		return $this->client->requestOrder($hostnames);
	}

	/**
	 * Store certificate after issuance
	 *
	 * @param string $certificate
	 * @param string $chain
	 * @param string $pkey
	 * @return bool
	 */
	public function store(string $certificate, string $chain, string $pkey): bool
	{
		$path = Letsencrypt::acmeSiteStorageDirectory($this->storageMarker, $this->endpoint);
		if (0 !== strpos($path, Letsencrypt::acmeDirectory())) {
			fatal('assertion failed: %s != %s', $path, Letsencrypt::acmeDirectory());
		}
		if (!is_dir($path) && !Filesystem::mkdir($path, APNSCP_SYSTEM_USER, 'root', 0770)) {
			return error("Failed to create certificate storage path `%s'", $path);
		}
		$items = [
			'cert' => $certificate,
			'chain' => $chain,
			'fullchain' => rtrim($certificate) . "\n" . $chain,
			'key' => $pkey
		];
		foreach ($items as $file => $data) {
			if (!file_put_contents("${path}/${file}.pem",  $data)) {
				Filesystem::rmdir("${path}/");
				return error('Failed to store certificate component %s, undoing', $file);
			}
		}
		return true;
	}

	/**
	 * Resolve ACME challenge
	 *
	 * @param CertificateOrder $order
	 * @return bool|null success or null if staged
	 */
	public function solve(CertificateOrder $order): ?bool
	{
		$dnName = [];

		foreach ($order->getAuthorizationsChallenges() as $host => $challenges) {
			$resolved = \array_filter($challenges, static function ($c) {
				return $c->isValid();
			});
			if (\count($resolved) > 0) {
				$mode = substr(array_get($resolved, 0)->getType(), 0, -3);
				debug('%(host)s already resolved by %(mode)s',
					['host' => $host, 'mode' => $mode]
				);
				// already valid
				$dnName[] = $host;
				continue;
			}

			$challenges = Acmephp::orderChallenges($challenges, $this->mode);
			/** @var AuthorizationChallenge $challenge */
			foreach ($challenges as $challenge) {
				$mode = substr($challenge->getType(), 0, -3);
				if (\in_array($mode, LETSENCRYPT_DISABLED_CHALLENGES, true)) {
					continue;
				}
				$resolver = static::appendNamespace('Solvers\\' . ucwords($mode));
				if (!class_exists($resolver)) {
					continue;
				}
				try {
					debug('SSL challenge attempt: %(mode)s (%(host)s)', ['mode' => $mode, 'host' => $host]);
					$handler = $resolver::instantiateContexted($this->getAuthContext(),
						[$challenge->getToken(), $challenge->getPayload()])->setAuthorization($challenge);

					if (!$this->stage && (!$handler->canUse() || !$handler->resolve())) {
						continue;
					}

					$challengeResponse = $this->client->challengeAuthorization($challenge);

					// release resource
					$handler = null;
					$resp = array_get($challengeResponse, 'status', 'invalid');
					if ($resp !== 'valid') {
						debug('%(host)s %(mode)s challenge failed, resp: %(err)s',
							['host' => $host, 'mode' => $mode, 'err' => var_export($challengeResponse, true)]);
						continue;
					}
					debug('SUCCESS! SSL challenge response: %(host)s (%(mode)s) - %(resp)s',
						['host' => $host, 'mode' => $mode, 'resp' => strtoupper($resp)]);
					// save success as $challengeResponse doesn't update
					$this->succeeded[$host] = $resp;
					$dnName[] = $host;

					// challenge solved
				} catch (ChallengeFailedException $e) {
					debug('%(mode)s challenge failed: %(err)s', ['mode' => $mode, 'err' => $e->getMessage()]);
				} catch (ChallengeTimedOutException $e) {
					debug('%(mode)s challenge timed out: %(err)s', ['mode' => $mode, 'err' => $e->getMessage()]);
				}

				continue 2;
			}

			$solvers = implode(', ', array_map(static function ($c) {
				return $c->getType();
			}, $challenges));

			if ($this->strict) {
				return error(
					"Failed to address SSL challenge for `%(host)s'. Solvers attempted: %(solvers)s", [
						'host' => $host,
						'solvers' => $solvers
					]
				);
			}

			warn(
				'No suitable solver found for %(host)s. Dropping request. Solvers attempted: %(solvers)s', [
					'host' => $host,
					'solvers' => $solvers
				]
			);
		}

		if ($this->stage) {
			return null;
		}

		if (!$dnName) {
			return false;
		}

		$keyPair = (new Acmephp\KeypairFinder($this->getAuthContext()))->create();
		$dn = new DistinguishedName(current($dnName), null, null, null, null, null, null, \array_slice($dnName, 1));

		// remove unvalidated hostnames
		// @todo move to auxilliary cleanup/retry function
		foreach ($order->getAuthorizationsChallenges() as $hostname => $challenges) {
			foreach ($challenges as $challenge) {
				if ($challenge->isValid()) {
					$this->succeeded[$hostname] = $challenge->toArray();
					continue 2;
				}
			}
		}

		$droppedNames = array_keys(array_diff_key($order->getAuthorizationsChallenges(), $this->succeeded));

		if (!$this->strict && $droppedNames) {
			// allow certificates to be dropped, certificates failed in challenge

			if ($this->pruned) {
				// already tried once, can't satisfy order after initial pruning
				report(
					'Request failed after pruning? Pruned: %s || Retained: %s',
					implode(', ', $this->pruned),
					implode(', ', $dnName)
				);
				return false;
			}

			// unsolved challenges
			$this->pruned = $droppedNames;

			debug('Retrying request in non-strict mode. Pruned %s', implode(', ', $this->pruned));

			// @todo finish order of previous, failed attempt?
			return $this->issue(array_keys($this->succeeded));
		}

		$csr = new CertificateRequest($dn, $keyPair);

		try {
			$certificateResponse = $this->client->finalizeOrder($order, $csr);
			$key = $keyPair->getPrivateKey()->getPEM();

			if ($this->pruned) {
				warn('Following hostnames pruned to satisfy request: %s', implode(', ', $this->pruned));
				$email = \apnscpFunctionInterceptor::factory($this->getAuthContext())->common_get_email();
				Bootstrapper::minstrap();
				$mail = \Illuminate\Support\Facades\Mail::to($email);
				if (\Crm_Module::COPY_ADMIN) {
					$mail->bcc(\Crm_Module::COPY_ADMIN);
				}
				$msg = \Lararia\Mail\Simple::instantiateContexted(
					$this->getAuthContext(), [
						'email.letsencrypt.hostnames-changed',
						[
							'domain' => $this->getAuthContext()->domain ?? current($dnName),
							'pruned' => $this->pruned
						]
					]
				)->asMarkdown();
				$msg->attachData(var_export(\Error_Reporter::get_buffer(), true), 'request-verbose.txt');
				$mail->send($msg);
			}

			$chain = $certificateResponse->getCertificate()->getIssuerCertificate()->getPEM();
			$x509 = $certificateResponse->getCertificate()->getPEM();
			if (!openssl_x509_check_private_key($x509, $key)) {
				return error('Failed to validate certificate against key: %s', openssl_error_string());
			}
			return $this->store($x509, $chain, $key);
		} catch (CertificateRequestFailedException $e) {
			return error('Failed to finalize certificate request: %s', $e->getMessage());
		} finally {
			if (!is_debug()) {
				\Error_Reporter::clear_buffer(\Error_Reporter::E_DEBUG);
			}
		}
	}

	/**
	 * Revoke issued certificate
	 *
	 * @return bool
	 */
	public function revoke(): bool
	{
		$cert = Acmephp::getCertificate($this->storageMarker);
		if (!$cert) {
			return error("No certificate issued for `%s'", $this->storageMarker);
		}
		try {
			$this->client->revokeCertificate($cert);
		} catch (AcmeCoreClientException $e) {
			return error('Revocation failed: %s', $e->getMessage());
		}
		return true;
	}

	/**
	 * Register or update account to use Let's Encrypt
	 *
	 * @param string|null $email optional email for certificate notices
	 * @return bool
	 */
	public function register(?string $email = ''): bool
	{
		$context = $this->getAuthContext();
		if (LETSENCRYPT_UNIFY_REGISTRATION) {
			$context = \Auth::context(\Auth::get_admin_login(), null);
		}
		$afi = \apnscpFunctionInterceptor::factory($context);
		if ('' === $email) {
			$email = array_get((array)$afi->common_get_email(), 0, '');
			if (!preg_match(\Regex::EMAIL, $email)) {
				return error("Invalid email `%s'", $email);
			}
		}
		return Acmephp::register($context, $email);
	}
}