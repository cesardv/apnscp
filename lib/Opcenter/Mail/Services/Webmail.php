<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Mail\Services;

	use Opcenter\Contracts\VirtualizedContextable;
	use Opcenter\Filesystem;
	use Opcenter\Map;
	use Opcenter\Provisioning\ConfigurationWriter;
	use Opcenter\SiteConfiguration;

	class Webmail implements VirtualizedContextable
	{
		use \ContextableTrait {
			instantiateContexted as instantiateContextedReal;
		}

		public const CACHE_KEY = 'em.webmail';
		public const REDIRECTION_PATH = '/var/www/.MAIL-REDIRECT';
		public const WEBMAIL_META_FILE = 'webmail';
		public const WEBMAILS = [
			'sqmail' => [
				'subdomain' => 'mail',
				'path'      => '/var/www/html/webmail',
				'storage'   => '/var/lib/sqmail/data'
			],
			'horde' => [
				'subdomain' => 'horde',
				'path'      => '/var/www/html/horde'
			],
			'roundcube' => [
				'subdomain' => 'roundcube',
				'path'      => '/var/www/html/roundcube'
			]
		];

		protected $root;

		private function __construct(\Auth_Info_User $context)
		{
			$this->root = $context->domain_fs_path();
		}

		public static function instantiateContexted(\Auth_Info_User $context): self
		{
			return static::instantiateContextedReal($context, [$context]);
		}

		/**
		 * Set webmail app to use subdomain
		 *
		 * @param string $app
		 * @param string $subdomain
		 * @return bool
		 */
		public function set(string $app, string $subdomain): bool
		{
			if (!$this->exists($app)) {
				return error("Unknown webmail app `%s'", $app);
			}
			if (!ctype_alnum($subdomain)) {
				return error("Subdomain `%s' is invalid", $subdomain);
			}
			$baseDir = $this->getAuthContext()->domain_fs_path($this->getPathFromApp($app));
			if (!file_exists($baseDir)) {
				Filesystem::mkdir($baseDir, $this->getAuthContext()->user_id, $this->getAuthContext()->group_id, 0711);
			}

			$map = Map::load($this->getCustomMapFile(), 'cd');
			$map[$app] = $subdomain;
			$map->close();
			$htaccess = $baseDir . '/.htaccess';

			return file_put_contents($htaccess, $this->getRedirectionRules($app)) &&
				Filesystem::chogp($htaccess, $this->getAuthContext()->user_id, $this->getAuthContext()->group_id);
		}

		/**
		 * Get subdomain path from app
		 *
		 * @param string $app
		 * @return null|string
		 */
		public function getPathFromApp(string $app): string
		{
			return self::REDIRECTION_PATH . "/${app}";
		}

		/**
		 * Webmail locations for account
		 *
		 * @return array
		 */
		public function getAll(): array {
			return array_merge($this->getDefaultSubdomains(), $this->getCustomSubdomains());
		}

		/**
		 * Get default webmail subdomains
		 *
		 * @return array
		 */
		public function getDefaultSubdomains(): array {
			return array_combine(array_keys(static::WEBMAILS), array_column(static::WEBMAILS, 'subdomain'));
		}

		/**
		 * Webmail app exists
		 * @param string $app
		 * @return bool
		 */
		public function exists(string $app): bool {
			return isset(static::WEBMAILS[$app]);
		}

		/**
		 * Forget custom location from index
		 *
		 * @param string $app
		 * @return bool
		 */
		public function forget(string $app): bool {
			$map = Map::load($this->getCustomMapFile(), 'cd');
			return $map->delete($app);
		}

		/**
		 * Get custom webmail subdomains
		 *
		 * @return array
		 */
		public function getCustomSubdomains(): array {
			$file = $this->getCustomMapFile();
			if (!file_exists($file)) {
				return [];
			}
			return (array)(Map::load($file, 'r')->fetchAll());
		}

		/**
		 * Webmail storage directory
		 *
		 * @param string $app   webmail app name
		 * @return string|null  path
		 */
		public function getStorageDirectory(string $app): ?string {
			if (!isset(static::WEBMAILS[$app])) {
				fatal("Unknown app `%s'", $app);
				return null;
			}

			$path = static::WEBMAILS[$app]['storage'] ?? null;

			return \is_string($path) ? $path : null;
		}

		/**
		 * Get custom webmail maps
		 *
		 * @return string
		 */
		protected function getCustomMapFile(): string {
			return $this->getAuthContext()->domain_info_path(self::WEBMAIL_META_FILE);
		}

		private function getRedirectionRules(string $type): string {
			if (!$this->exists($type)) {
				fatal("Unknown webmail type `%s'", $type);
			}
			$folder = basename(array_get(self::WEBMAILS, "${type}.path"));
			$config = (new ConfigurationWriter('mail/webmail-redirect', SiteConfiguration::shallow($this->authContext)))
				->compile([
					'mailpath' => $folder
				]);
			return (string)$config;
		}




	}