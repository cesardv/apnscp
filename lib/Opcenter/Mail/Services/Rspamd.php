<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Mail\Services;

	use Opcenter\Mail\Contracts\FilterProvider;
	use Opcenter\Mail\Services\Rspamd\Configuration;
	use Opcenter\System\GenericSystemdService;

	class Rspamd extends GenericSystemdService implements FilterProvider
	{
		protected const SERVICE = 'rspamd';
		public const ADMIN_PREFKEY = 'rspamd.pw';

		// @var float threshold mail is rejected
		public const REJECTION_THRESHOLD = 25;

		/**
		 * rspamd is present
		 *
		 * @return bool
		 */
		public static function present(): bool {
			return (bool)MAIL_RSPAMD_PRESENT;
		}

		/**
		 * Set admin password
		 *
		 * @param string $val
		 * @return bool
		 */
		public static function setPassword(string $val): bool {
			if (posix_getuid()) {
				return error('Must be run as root');
			}
			$config = new Configuration('worker-controller.inc', 'r+');
			$proc = \Util_Process_Safe::exec('rspamadm pw -p %s', $val);
			if (!$proc['success']) {
				return error('Failed to compute rspamd password: %s', $proc['stderr']);
			}
			$config['password'] = trim($proc['stdout']);
			$config->save();
			return static::restart('now');
		}

		/**
		 * Get admin password
		 *
		 * @return string
		 */
		public static function getPassword(): ?string {
			$ctx = \Auth::context(null);
			return array_get(\Preferences::factory($ctx), self::ADMIN_PREFKEY);
		}


	}

