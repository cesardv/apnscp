<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
 */

namespace Opcenter\Mail\Services\Rspamd;

use Opcenter\Map\UnboundedJson;

class Configuration extends UnboundedJson
{
	protected const CONF_DIR = '/etc/rspamd/local.d';

	public function __construct(string $name, string $mode = 'r')
	{
		$path = static::CONF_DIR . "/${name}";
		if (0 !== strpos(realpath($path), self::CONF_DIR . '/')) {
			fatal('Configuration must be within %s', self::CONF_DIR);
		}

		parent::__construct($path, $mode);
	}
}
