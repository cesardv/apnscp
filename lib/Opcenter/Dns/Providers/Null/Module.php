<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2018
	 */

	namespace Opcenter\Dns\Providers\Null;

	use Module\Provider\Contracts\ProviderInterface;
	use Opcenter\Dns\Record;

	class Module extends \Dns_Module implements ProviderInterface
	{
		/**
		 * apex markers are marked with @
		 */
		protected const HAS_ORIGIN_MARKER = true;
		public const DNS_TTL = 1800;
		use \NamespaceUtilitiesTrait;
		// @var array API credentials
		protected static $permitted_records = [
			'A',
			'AAAA',
			'CNAME',
			'MX',
			'LOC',
			'SRV',
			'SPF',
			'TXT',
			'NS',
			'CAA',
			'PTR',
			'CERT',
			'DNSKEY',
			'DS',
			'NAPTR',
			'SMIMEA',
			'SSHFP',
			'TLSA',
			'URI'
		];

		/**
		 * Add a DNS record
		 *
		 * @param string $zone
		 * @param string $subdomain
		 * @param string $rr
		 * @param string $param
		 * @param int    $ttl
		 * @return bool
		 */
		public function add_record(
			string $zone,
			string $subdomain,
			string $rr,
			string $param,
			int $ttl = self::DNS_TTL
		): bool {
			return true;
		}

		/**
		 * @inheritDoc
		 */
		public function record_exists(
			string $zone,
			string $subdomain,
			string $rr = 'ANY',
			string $parameter = ''
		): bool {
			return true;
		}

		/**
		 * @inheritDoc
		 */
		public function remove_record(string $zone, string $subdomain, string $rr, string $param = ''): bool
		{
			return true;
		}

		/**
		 * Get hosting nameservers
		 *
		 * @param string|null $domain
		 * @return array
		 */
		public function get_hosting_nameservers(string $domain = null): array
		{
			return [];
		}

		/**
		 * Add DNS zone to service
		 *
		 * @param string $domain
		 * @param string $ip
		 * @return bool
		 */
		public function add_zone_backend(string $domain, string $ip): bool
		{
			return true;
		}

		/**
		 * Add DNS zone
		 *
		 * @param string $domain
		 * @param string $ip
		 * @return bool
		 */
		public function add_zone(string $domain, string $ip): bool
		{
			return true;
		}

		/**
		 * @inheritDoc
		 */
		public function configured(): bool
		{
			return false;
		}

		/**
		 * DNS zone exists
		 *
		 * @param string $zone
		 * @return bool
		 */
		public function zone_exists(string $zone): bool
		{
			return true;
		}


		/**
		 * Remove DNS zone from nameserver
		 *
		 * @param string $domain
		 * @return bool
		 */
		public function remove_zone_backend(string $domain): bool
		{
			return true;
		}

		/**
		 * Remove zone stub
		 *
		 * @param string $domain
		 * @return bool
		 */
		public function remove_zone(string $domain, bool $force = false): bool
		{
			return true;
		}

		public function _create()
		{
			return;
		}

		public function _delete()
		{
			return;
		}

		/**
		 * Get raw zone data
		 *
		 * @param string $domain
		 * @return null|string
		 */
		protected function zoneAxfr(string $domain): ?string
		{
			return '';
		}

		/**
		 * Modify a DNS record
		 *
		 * @param string $zone
		 * @param Record $old
		 * @param Record $new
		 * @return bool
		 */
		protected function atomicUpdate(string $zone, Record $old, Record $new): bool
		{
			return true;
		}
	}
