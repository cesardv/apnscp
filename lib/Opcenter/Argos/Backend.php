<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */


	namespace Opcenter\Argos;

	use Illuminate\Contracts\Support\Arrayable;
	use Symfony\Component\Yaml\Yaml;

	/**
	 * Class Backend
	 *
	 * Generic backend configuration
	 *
	 * @package Opcenter\Argos
	 */
	abstract class Backend implements \ArrayAccess, Arrayable
	{
		use \NamespaceUtilitiesTrait;
		use \PropertyMutatorAccessorTrait;

		/**
		 * @var string backend driver
		 */
		protected $backend;
		private $dirty = false;
		private $locked;

		private function __construct(array $config)
		{
			foreach ($config as $k => $v) {
				$this->__set($this->normalizeIn($k), $v);
			}
		}

		/**
		 * Normalize configuration into class property
		 *
		 * @param string $offset
		 * @return string
		 */
		protected function normalizeIn(string $offset)
		{
			return camel_case($offset);
		}

		/**
		 * Load a backend with requested configuration
		 *
		 * @param array       $config
		 * @param string|null $provider
		 * @return Backend
		 */
		public static function load(array $config = [], string $provider = null): Backend
		{
			if (!$provider) {
				return new static($config);
			}
			$c = self::appendNamespace('Backends\\' . ucwords($provider));

			return new $c($config);
		}

		/**
		 * Get available backends
		 *
		 * @return array
		 */
		public static function getBackends(): array
		{
			$backends = [];
			if (!$dh = opendir(__DIR__ . '/Backends')) {
				return $backends;
			}
			while (false !== ($file = readdir($dh))) {
				if ('.php' !== substr($file, -4)) {
					continue;
				}
				$backends[] = strtolower(basename($file, '.php'));
			}
			closedir($dh);

			return $backends;
		}

		public function lock()
		{
			$this->locked = true;
		}

		public function __toString()
		{
			return (string)Yaml::dump($this->__debugInfo());
		}

		public function __debugInfo()
		{
			return $this->toArray();
		}

		/**
		 * Encapsulate data as array
		 *
		 * @return array
		 * @throws \ReflectionException
		 */
		public function toArray(): array
		{
			$config = [];
			foreach ((new \ReflectionClass($this))->getProperties(~\ReflectionProperty::IS_PRIVATE) as $prop) {
				$name = $prop->getName();
				if (!isset($this->{$name})) {
					continue;
				}
				$config[$this->normalizeOut($name)] = $this->{$name};
			}

			return $config;
		}

		/**
		 * Convert class property into backend variable
		 *
		 * @param string $offset
		 * @return string
		 */
		protected function normalizeOut(string $offset)
		{
			return snake_case($offset);
		}

		public function dirty(): bool
		{
			return $this->dirty;
		}

		/**
		 * Get backend authentication
		 *
		 * @return mixed
		 */
		abstract public function getAuthentication();

		/**
		 * Set backend authentication
		 *
		 * @param mixed ...$vars
		 * @return bool
		 */
		abstract public function setAuthentication(...$vars): bool;

		public function offsetExists($offset)
		{
			$offset = $this->normalizeIn($offset);

			return property_exists($this, $offset);
		}

		public function offsetGet($offset)
		{
			$offset = $this->normalizeIn($offset);

			return $this->__get($offset);
		}

		public function offsetSet($offset, $value)
		{
			if ($this->locked && $offset !== 'backend') {
				fatal('Cannot edit locked resource');
			}
			$this->dirty = true;
			$offset = $this->normalizeIn($offset);
			$this->__set($offset, $value);
		}

		public function offsetUnset($offset)
		{
			if ($this->locked) {
				fatal('Cannot edit locked resource');
			}
			$this->dirty = true;
			$offset = $this->normalizeIn($offset);
			$this->__set($offset, null);
		}


	}