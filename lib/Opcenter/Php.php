<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */


	namespace Opcenter;

	class Php
	{
		const PHP_BIN = '/usr/bin/php';

		/**
		 * Verify PHP extension enabled in system build
		 *
		 * @param string $extension
		 * @return bool
		 */
		public static function extensionEnabled(string $extension): bool
		{
			$code = '$ret = extension_loaded("' . $extension . '"); echo  $ret;';
			$ret = \Util_Process_Safe::exec(self::PHP_BIN . ' -r %(code)s', ['code' => $code]);

			return $ret['output'] !== '';
		}

		/**
		 * Get PHP version
		 *
		 * @return string
		 */
		public static function version(): ?string
		{
			static $version;
			if (null === $version) {
				$cmd = \Util_Process::exec('/usr/bin/php -nv 2> /dev/null');
				preg_match('/^PHP (\d+\.\d+\.\d+)/m', $cmd['output'], $match);
				$version = $match[1];
			}
			return $version;
		}
	}