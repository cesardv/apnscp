<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Diskquota;

	use Opcenter\Service\ServiceValidator;

	class Quota extends ServiceValidator
	{
		const DESCRIPTION = 'Account storage quota';

		public function valid(&$value): bool
		{
			if (!$value) {
				$value = null;
				return true;
			}

			if (!is_numeric($value)) {
				return error("quota threshold must be numeric, `%s' given", $value);
			}

			$hasAmnesty = $this->ctx->getServiceValue('diskquota', 'amnesty');
			if ($hasAmnesty && !empty($this->ctx->getNewServiceValue('diskquota',
					'quota')) && $this->ctx->getServiceValue('diskquota', 'amnesty')) {
				// quota set independent of amnesty, clear amnesty flag
				// as storage has been permanently upgraded
				warn('diskquota changed, amnesty flag cleared');
				$this->ctx->set('amnesty', null);
			}

			$units = $this->ctx->getServiceValue('diskquota', 'units');
			$tmp = \Formatter::changeBytes($value, 'B', $units);
			if ($tmp < 1) {
				return error("diskquota must be a non-negative number, `%s' found", $value);
			}

			if (!$this->checkQuota($value)) {
				return false;
			}

			return true;
		}

		/**
		 * Validate if change will put site over usage
		 *
		 * @param null|int $quota
		 * @return bool
		 */
		private function checkQuota(?int $quota) {
			if (!$quota || !$this->ctx['enabled'] || !$this->ctx->isEdit()) {
				return true;
			}
			$newquota = (int)\Formatter::changeBytes(
				$quota,
				'KB',
				$this->ctx->getServiceValue(null, 'units')
			);
			$usage = array_get(\Opcenter\Filesystem\Quota::getGroup($this->ctx->getServiceValue('siteinfo', 'admin')), 'qused', 0);
			if ($usage > $newquota) {
				if (!$this->ctx->getConfigurationContainer()->hasValidatorOption('force')) {
					return error("Storage usage will exceed diskquota,quota by %d KB. Rejecting change without --force.", $usage - $newquota);
				}
				return warn("Storage usage will exceed diskquota,quota by %d KB", $usage - $newquota);
			}

			return true;
		}

		public function getValidatorRange()
		{
			// @todo get available storage from device∞
			return '[null,0-∞]';
		}

	}