<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Diskquota;

	use Opcenter\Service\ServiceValidator;

	class Fquota extends ServiceValidator
	{
		const DESCRIPTION = 'Account inode quota';

		public function valid(&$value): bool
		{
			if (!$value || !$this->ctx->getServiceValue('diskquota', 'enabled')) {
				$value = null;
			}

			if ($value !== null && !is_numeric($value)) {
				return error("inode quota threshold must be numeric, `%s' given", $value);
			}

			if ($value !== null && $value < 1) {
				return error("diskquota must be a non-negative number, `%s' found", $value);
			}

			if (!$this->checkQuota($value)) {
				return false;
			}

			return true;
		}

		/**
		 * Validate if change will put site over inode usage
		 *
		 * @param null|int $quota
		 * @return bool
		 */
		private function checkQuota(?int $quota)
		{
			if (!$quota || !$this->ctx['enabled'] || !$this->ctx->isEdit()) {
				return true;
			}

			$usage = array_get(\Opcenter\Filesystem\Quota::getGroup($this->ctx->getServiceValue('siteinfo', 'admin')),
				'fused', 0);
			if ($usage > $quota) {
				if (!$this->ctx->getConfigurationContainer()->hasValidatorOption('force')) {
					return error("Storage usage will exceed diskquota,fquota by %d inodes. Rejecting change without --force.",
						$usage - $quota);
				}

				return warn("Storage usage will exceed diskquota,fquota by %d inodes", $usage - $quota);
			}

			return true;
		}

		public function getValidatorRange()
		{
			// @todo get available storage from device∞
			return '[null,0-∞]';
		}

	}