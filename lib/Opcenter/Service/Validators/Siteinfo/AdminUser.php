<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Siteinfo;

	use Opcenter\Process;
	use Opcenter\Provisioning\Siteinfo;
	use Opcenter\Role\Group;
	use Opcenter\Role\User;
	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\SiteConfiguration;

	class AdminUser extends ServiceValidator implements AlwaysValidate, ServiceReconfiguration
	{
		use \FilesystemPathTrait;
		const DESCRIPTION = 'Administrative user of account';

		public function valid(&$value): bool
		{
			if (!preg_match(\Regex::USERNAME, $value)) {
				return error("invalid username `%s' specified", $value);
			} else if (0 === strncmp($value, 'site', 4) && ctype_digit(substr($value, 4))) {
				return error("usernames that begin with 'site' followed by numbers are reserved");
			} else if ($value === APNSCP_SYSTEM_USER) {
				return error("Username may not duplicate system user `%s'", APNSCP_SYSTEM_USER);
			} else if (0 === strncmp($value, 'admin', 5) && ctype_digit(substr($value, 5))) {
				// potentially cause conflict if 1 virtual admin ("admin_user") is adminXX and
				// new account slotted to XX site ID ("admin") with MySQL/PostgreSQL peer authentication
				return error("usernames that begin with 'admin' followed by numbers are reserved");
			}

			/** @var \User_Module $class */
			$class = \a23r::get_autoload_class_from_module('user');
			if (\strlen($value) > $class::USER_MAXLEN) {
				return error('user max length %d', $class::USER_MAXLEN);
			}

			if (($id = \Auth::get_site_id_from_admin($value)) && ('site' . $id !== $this->site)) {
				return error("username `%s' is already in use by site id `%d'",
					$value, $id);
			}
			if (!$this->ctx->isEdit()) {
				return true;
			}
			if ($value === $this->ctx->getOldServiceValue(null, 'admin_user')) {
				// no change
				return true;
			}
			// edit checks
			if (User::bindTo($this->domain_fs_path())->exists($value)) {
				return error("target user `%s' already exists", $value);
			}

			return true;
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			// AlwaysValidate will always call hooks
			if ($old === $new) {
				return true;
			}
			if (!Group::bindTo($svc->getAccountRoot())->change($old, [
				'name' => $new
			])) {
				return error("failed to rename group from `%s' to `%s'", $old, $new);
			}

			Process::killGroup($svc->getAuthContext()->group_id);
			if (!Siteinfo::changeAdmin($old, $new, $svc->getAccountRoot(), "/home/${new}", $svc->getSiteId())) {
				return false;
			}

			$svc->getSiteFunctionInterceptor()->user_flush();

			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			if (User::bindTo($svc->getAccountRoot())->exists($new)) {
				if (!Siteinfo::changeAdmin($new, $old, $svc->getAccountRoot(), "/home/${old}", $svc->getSiteId())) {
					return error("failed to revert user `%s' to `%s'", $new, $old);
				}
			}
			if (Group::bindTo($svc->getAccountRoot())->exists($new)) {
				if (!Siteinfo::changeAdmin($new, $old, $svc->getAccountRoot(), "/home/${old}", $svc->getSiteId())) {
					return error("failed to rename group from `%s' to `%s'", $new, $old);
				}
			}

			return true;
		}
	}

