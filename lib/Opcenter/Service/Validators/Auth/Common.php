<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Auth;

	use Opcenter\Service\ServiceValidator;

	abstract class Common extends ServiceValidator
	{
		/**
		 * Other passwd fields set
		 *
		 * @param string[] ...$fields
		 * @return bool
		 */
		protected function isSet(string ...$fields): bool
		{
			foreach ($fields as $field) {
				if (isset($this->ctx[$field])) {
					return true;
				}
			}

			return false;
		}

		/**
		 * Clear additional fields if present
		 *
		 * @param string[] ...$fields
		 */
		protected function clear(string ...$fields): void
		{
			foreach ($fields as $field) {
				if ($this->ctx[$field]) {
					$this->ctx->set($field, null);
					debug("cleared value from `%s'", $field);
				}
			}
		}
	}