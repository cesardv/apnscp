<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Apache;

	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\SiteConfiguration;

	class Webuser extends ServiceValidator implements ServiceReconfiguration, AlwaysValidate
	{
		const DESCRIPTION = 'Use account user to serve HTTP content';

		public function valid(&$value): bool
		{
			$admin = $this->ctx->getServiceValue('siteinfo', 'admin_user');
			if ($value === null || ($this->ctx->isEdit() && $this->ctx->serviceValueChanged('siteinfo', 'admin_user')) ) {
				$value = $admin;
			}

			if ($value !== $admin && $value !== \Web_Module::WEB_USERNAME) {
				return error("unknown or unsupported username `%s' specified", $value);
			}

			return true;
		}

		public function getValidatorRange()
		{
			return '[' . \Web_Module::WEB_USERNAME . ']';
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if ($old === $new) {
				return true;
			} else if (!$this->ctx['jail']) {
				// jail not enabled, defaults to system user
				return true;
			}
			if ($old === $this->ctx->getOldServiceValue('siteinfo', 'admin_user') && $new === $this->ctx->getNewServiceValue('siteinfo','admin_user')) {
				// admin username change, uid same
				return true;
			}
			if ($new === \Web_Module::WEB_USERNAME && $old === $this->ctx->getServiceValue('siteinfo', 'admin_user')) {
				return warn('Ignoring ownership update. Changing web owner from %s to %s will change ' .
					'ALL system files to %s. Manually assign web write access via Web > Web Apps > Fortification', $old, $new, $new);
			}
			// change ownership if necessary
			info('Changing web content ownership from %s to %s', $old, $new);
			return \is_array($svc->getSiteFunctionInterceptor()->file_takeover_user($old, $new));
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			if ($old === \Web_Module::WEB_USERNAME && $new === $this->ctx->getServiceValue('siteinfo', 'admin_user')) {
				// no good way to do this without corrupting system files
				return true;
			}
			return $this->reconfigure($new, $old, $svc);
		}


	}
