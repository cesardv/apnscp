<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Common;

	use Event\Cardinal;
	use Event\Contracts\Publisher;
	use Event\Events;
	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\Versioning;

	class Version extends ServiceValidator implements AlwaysValidate
	{
		const DESCRIPTION = 'Set module service definition';
		const VALUE_RANGE = '<float>';

		public function valid(&$value): bool
		{
			if ($value === null) {
				$value = APNSCP_VERSION;
			}

			$value = str_replace(',', '.', (string)$value);
			$value = Versioning::asMinor($value);
			if ($value === Versioning::asMinor(APNSCP_VERSION)) {
				return true;
			}

			if (version_compare($value, APNSCP_VERSION, '>')) {
				return error("service definition for `%s' newer than %s version - asked %s, have %s - upgrade %s",
					$this->ctx->getModuleName(),
					PANEL_BRAND,
					$value,
					APNSCP_VERSION,
					PANEL_BRAND
				);
			}

			if (0 !== strpos(APNSCP_VERSION, $value . '.')) {
				debug('Updating service definition from %s to %s', $value, APNSCP_VERSION);
				$this->ctx->set('version', APNSCP_VERSION);
			}
			// support fractional tenth. Convert for locale-specific formatters
			$value = (string)number_format((float)$value, 1, '.', '');
			return true;
		}

		public function update($event, Publisher $caller): bool
		{
			if (Cardinal::is($event, Events::SUCCESS)) {
				return $this->ctx->commitService();
			}

			return true;
		}

	}

