<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Common;

	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\ServiceValidator;

	abstract class IpValidation extends ServiceValidator implements ServiceInstall
	{
		use \NamespaceUtilitiesTrait;
		const VALUE_RANGE = '[addr1, addr2...]';
		const DESCRIPTION = 'Assign IP addresses uniquely to account';

		public function autoToggle(&$value): void
		{
			if (!$this->ctx->isEdit()) {
				return;
			}

			$svcvar = strtolower(static::getBaseClassName(null));
			$comp = $svcvar === 'ipaddrs' ? 'nbaddrs' : 'ipaddrs';
			$nbchg = $this->ctx->getOldServiceValue(null, 'nbaddrs') !== $this->ctx->getNewServiceValue(null,
					'nbaddrs');
			$ipchg = $this->ctx->getOldServiceValue(null, 'ipaddrs') !== $this->ctx->getNewServiceValue(null,
					'ipaddrs');
			// let ipaddrs change always win unless nbaddrs was null

			$blanked = null;
			if (null !== ($namebased = $this->ctx->getNewServiceValue(null, 'namebased'))) {
				// namebased explicitly provided
				$blanked = $namebased ? 'ipaddrs' : 'nbaddrs';
				$changed = $namebased ? 'nbaddrs' : 'ipaddrs';
			} else if ($ipchg && $this->ctx['ipaddrs']) {
				$namebased = 0;
				$blanked = 'nbaddrs';
				$changed = 'ipaddrs';
			} else if ($nbchg && $this->ctx['nbaddrs']) {
				$namebased = 1;
				$blanked = 'ipaddrs';
				$changed = 'nbaddrs';
			} else {
				// no change
				return;
			}

			$oldip = implode(',', $this->ctx->getOldServiceValue(null, $changed));
			$newip = implode(',', $this->ctx[$changed]);
			if ($oldip === $newip) {
				return;
			}
			$this->ctx['namebased'] = $namebased;
			$this->ctx[$blanked] = [];
			info("IP address change detected, `%s' -> `%s', disabled %s", $oldip, $newip, $blanked);
		}

		private function toggleFlag(int $val)
		{
			$new = $old = $this->ctx['namebased'];
			$val = (int)$val;
			if ($old === $val) {

			}
			$this->ctx['namebased'] = $new;
		}
	}