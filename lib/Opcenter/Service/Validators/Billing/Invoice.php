<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Billing;

	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Validators\Common\GenericDuplicateMap;
	use Opcenter\SiteConfiguration;

	class Invoice extends GenericDuplicateMap implements ServiceReconfiguration, ServiceInstall
	{
		const MAP_FILE = 'billing.domainmap';
		const DESCRIPTION = 'Unique identifier assigned to account';

		public function valid(&$value): bool
		{
			if (!$value && $this->ctx['parent_invoice']) {
				$value = null;

				return true;
			}
			if (ctype_digit($value)) {
				return error("Invoice may not only consist of numbers");
			}
			if ($value && $this->ctx['parent_invoice']) {
				return error('both parent_invoice and invoice may not be set');
			}
			if (!$value) {
				$value = $this->generate();
				info("no billing identifier specified for site, generated `%s'", $value);
			} else if (!is_scalar($value)) {
				return error("billing identified must be scalar value, complex type `%s' provided", \gettype($value));
			} else if (0 === strncmp($value, 'site', 4) && ctype_digit(substr($value, 4))) {
				return error('billing identifier cannot follow form siteXX');
			}

			return parent::valid($value);
		}

		private function generate()
		{
			$id = 'apnscp-';
			for ($i = 0; $i < 8; $i++) {
				$id .= \chr(random_int(\ord('A'), \ord('Z')));
			}
			if ($this->mapKeyInUse((string)$id, self::MAP_FILE)) {
				return $this->generate();
			}

			return $id;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if ($new === $old) {
				return true;
			}
			$old = (string)$old;
			$new = (string)$new;
			if ($old && $this->keyExists($old)) {
				$values = preg_split('/\s*,\s*/', (string)$this->getMapValue($old), -1, PREG_SPLIT_NO_EMPTY);
				if (false !== ($key = array_search($svc->getSite(), $values))) {
					unset($values[$key]);
				}
				$values ? $this->addMap($old, implode(',', $values)) : $this->removeMap($old);
			}
			if (!$new) {
				// invoice -> parent_invoice
				return true;
			}
			$values = (string)$this->getMapValue($new);
			$values = preg_split('/\s*,\s*/', $values, -1, PREG_SPLIT_NO_EMPTY);
			if (!\in_array($svc->getSite(), $values, true)) {
				$values[] = $svc->getSite();
			}

			return $this->addMap($new, implode(',', $values));
		}

		public function populate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure(null, $this->ctx[$this->getKey()], $svc);
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure($this->ctx[$this->getKey()], null, $svc);
		}

		private function getKey(): string
		{
			return static::class === self::class ? 'invoice' : 'parent_invoice';
		}
	}
