<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Reseller;

	use Opcenter\Service\ServiceValidator;

	class ResellerId extends ServiceValidator
	{
		public function valid(&$value): bool
		{
			if ($value !== 0) {
				warn('0 is only acceptable reseller_id value');
				$value = 0;
			}

			return $value === 0;
		}

	}