<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Dns;

	use Opcenter\Account\Enumerate;
	use Opcenter\Net\Iface;
	use Opcenter\Net\Ip4;
	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Validators\Common\IpValidation;
	use Opcenter\SiteConfiguration;

	class Proxy6addr extends Proxyaddr
	{
	}