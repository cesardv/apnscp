<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Crontab;

	use Opcenter\Service\ServiceValidator;

	class Permit extends ServiceValidator
	{
		const DESCRIPTION = 'Permit activation of task scheduler';
		const VALUE_RANGE = '[0,1]';

		public function valid(&$value): bool
		{
			if ($value && !$this->ctx->getServiceValue('ssh', 'enabled')) {
				return error('ssh service must be enabled to allow crontab, disable crontab');
			}

			return $value === 1 || $value === 0;
		}
	}
