<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Crontab;


	class Enabled extends \Opcenter\Service\Validators\Common\Enabled
	{
		public const DESCRIPTION = 'Task scheduling support';

		public function valid(&$value): bool
		{
			if ($value && !$this->ctx->getServiceValue('ssh', 'enabled')) {
				return error('ssh service must be enabled to allow crontab');
			}
			if ($value && !$this->ctx['permit']) {
				warn('crontab service is enabled. Implicitly setting crontab,permit=1');
				$this->ctx['permit'] = 1;
			}
			return parent::valid($value);
		}
	}
