<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Aliases;

	use Opcenter\Service\Contracts\AlwaysValidate;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements AlwaysValidate
	{
		public const DESCRIPTION = 'Addon domain support';

		public function valid(&$value): bool
		{
			if (!parent::valid($value)) {
				return false;
			}

			if ($value && 0 === $this->ctx['max']) {
				warn("User may not use addon domains while aliases,max=0");
			}

			if ($this->ctx->isEdit() && !$this->ctx->isRevalidation() &&
				$this->ctx->getServiceValue('siteinfo', 'domain') !== $this->ctx->getOldServiceValue('siteinfo', 'domain'))
			{
				$this->ctx->revalidateService();
			}

			return true;
		}

	}
