<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Aliases;

	use Opcenter\License;
	use Opcenter\Map;
	use Opcenter\Provisioning\Apache;
	use Opcenter\Provisioning\Traits\MapperTrait;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Validators\Common\GenericDomainMap;
	use Opcenter\SiteConfiguration;

	class Aliases extends GenericDomainMap implements ServiceReconfiguration, ServiceInstall
	{
		use MapperTrait {
			mapWrap as realMapWrap;
		}

		const DESCRIPTION = 'Addon domains';
		const VALUE_RANGE = '<array>';
		const MAP_FILE = 'domainmap';

		/**
		 * Add or remove a domain
		 *
		 * @param string      $mode   map mode
		 * @param null|string $site   site or null to remove
		 * @param string      $domain domain name
		 * @return bool
		 */
		protected static function mapWrap(string $mode, ?string $site, string $domain): bool
		{
			$ret = true;
			foreach ([Map::DOMAIN_TXT_MAP, Map::DOMAIN_MAP] as $db) {
				$ret &= static::realMapWrap($mode, $site, $domain, $db);
			}

			return (bool)$ret;
		}

		public function valid(&$value): bool
		{
			if (!$value) {
				$value = [];
			}

			$license = License::get();
			$siteid = (int)substr($this->site, 4);
			$primary = $this->ctx->getServiceValue('siteinfo', 'domain');
			foreach ($value as &$domain) {
				if (0 === strncmp($domain, 'www.', 4)) {
					$domain = substr($domain, 4);
				}

				$domain = strtolower($domain);
				if (!preg_match(\Regex::DOMAIN, $domain)) {
					return error("invalid domain `%s'", $domain);
				}

				if ($domain === $primary) {
					warn("Primary domain duplicated in aliases. Removing `%s'. Remove this domain from aliases when promoting in the future!", $domain);
					$domain = null;
					continue;
				}

				if (null !== ($id = \Auth::get_site_id_from_domain($domain)) && $id !== $siteid) {
					return error("domain `%s' already hosted on `%s' (site ID: %d)",
						$domain,
						\Auth::get_domain_from_site_id($id), $id
					);
				}
				if ($domain === SERVER_NAME) {
					return error("Domain `%s' may not duplicate system hostname", $domain);
				}

				if ($license->isDevelopment() && substr($value, -5) !== '.test') {
					return error("License permits only .test TLDs. `%s' provided.", $value);
				}
			}
			unset($domain);
			$value = array_filter($value);
			$count = \count(Map::load(Map::home() . '/' . Map::DOMAIN_MAP, 'r-')->fetchAll());
			if ($license->hasDomainCountRestriction() && ++$count > $license->getDomainLimit()) {
				return error('License limit reached for domain count: %(limit)d',
					['limit' => $license->getDomainLimit()]);
			}

			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			$add = array_diff($new, $old);
			$remove = array_diff($old, $new);
			$afi = $svc->getSiteFunctionInterceptor();
			$knownDomains = $afi->aliases_list_shared_domains();
			foreach ($remove as $r) {
				// @XXX aliases:remove-domain will detach and journal a domain pending aliases:synchronize-changes
				// calling aliases:remove-domain queries web:get-docroot, which checks aliases:list-shared-domains
				// although it's already been removed from the map
				//
				// avoid a nasty explosion by checking if the domain still exists in the alias map
				if (\in_array($r, $knownDomains, true)) {
					$afi->aliases_remove_domain($r);
				}

				if ($r === $svc->getAuthContext()->domain) {
					$map = \Opcenter\Http\Apache\Map::fromSiteDomainMap($svc->getAuthContext()->domain_info_path(),
						$svc->getSite(), \Opcenter\Http\Apache\Map::MODE_WRITE);
					$docroot = data_get($map, $r, \Web_Module::MAIN_DOC_ROOT);
					$map->offsetUnset($r);
					if ($docroot !== \Web_Module::MAIN_DOC_ROOT) {
						warn("`%(domain)s' has been remapped into the primary domain. Its old document root `%(oldroot)s' will now serve from `%(newroot)s'.", [
							'domain'  => $r,
							'oldroot' => $svc->getSiteFunctionInterceptor()->file_unmake_path($docroot),
							'newroot' => \Web_Module::MAIN_DOC_ROOT
						]);
					}

					continue;
				}

				parent::removeMap($r);
			}
			foreach ($add as $a) {
				parent::addMap($a, $svc->getSite());
			}

			if ($svc->getServiceValue('apache', 'enabled') && !$this->ctx->isDelete()) {
				Apache::createConfiguration($svc);
			}

			return true;
		}

		public function populate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure([], $this->ctx['aliases'], $svc);
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure($this->ctx['aliases'], [], $svc);
		}


	}
