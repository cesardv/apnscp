<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Users;

	use Opcenter\Filesystem;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Validators\Common\MustBeEnabled;
	use Opcenter\SiteConfiguration;


	/**
	 * Class Enabled
	 *
	 * @package Opcenter\Service\Validators\Siteinfo
	 *
	 * Critical account bootstrapping
	 */
	class Enabled extends MustBeEnabled implements ServiceInstall
	{
		use \FilesystemPathTrait;

		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			$this->site = $svc->getSite();
			$path = $this->domain_info_path('users');
			if (!is_dir($path) && !Filesystem::mkdir($path)) {
				return error('failed to create user preference meta directory');
			}

			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			foreach ($svc->getSiteFunctionInterceptor()->user_get_users() as $user => $pwd) {
				if ($user === $svc->getAuthContext()->username) {
					// admin user
					continue;
				}
				$svc->getSiteFunctionInterceptor()->user_delete($user);
			}
			return true;
		}
	}

