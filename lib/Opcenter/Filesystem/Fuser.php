<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

namespace Opcenter\Filesystem;

use Opcenter\Contracts\Virtualizable;

/**
 * Class Fuser
 *
 * fuser manipulation
 *
 * @package Opcenter\Filesystem
 */
class Fuser implements Virtualizable
{
	const BIN = '/usr/sbin/fuser';
	protected $root;

	public function __construct(string $root = '/')
	{
		$this->root = '/';
	}

	public static function bindTo(string $root = '/')
	{
		return new static($root);
	}

	/**
	 * List PIDs held by file
	 *
	 * @param string $file
	 * @return array|bool
	 */
	public function interrogate(string $file)
	{
		$ret = \Util_Process_Safe::exec(static::BIN . ' %s%s', $this->root, ltrim($file, '/'));
		if (!$ret['success']) {
			return error($ret['error']);
		}
		return array_map('\intval', preg_split('/\s+/', $ret['stdout'], -1, PREG_SPLIT_NO_EMPTY));
	}

	/**
	 * Kill all PIDs held by file
	 *
	 * @param string $file
	 * @param bool   $writeonly restrict to write-only
	 * @return bool
	 */
	public function kill(string $file, bool $writeonly = false): bool
	{
		$path = rtrim($this->root, '/') . DIRECTORY_SEPARATOR . ltrim($file, '/');
		$path = realpath($path);
		if (false === $path) {
			return error("Unable to kill `%s' - invalid path", $file);
		}
		$ret = \Util_Process_Safe::exec(static::BIN . ' %s -k %s', $writeonly ? '-w' : null, $path);
		if (!$ret['success']) {
			/**
			 * fuser returns 1 when:
			 * - read-only processes remain
			 * - no processes found
			 * - acts of god
			 * Clean up implementation and worry only about the last scenario
			 */
			$pids = preg_split('/\s+/', $ret['stdout'], -1, PREG_SPLIT_NO_EMPTY);
			if (\count($pids) > 0) {
				return warn('PIDs found read-only: %s', implode(', ', $pids));
			}
			return empty($ret['stderr']) ? true : error($ret['stderr']);
		}
		return true;
	}

}