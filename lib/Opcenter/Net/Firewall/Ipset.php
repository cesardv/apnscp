<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Net\Firewall;

	use Opcenter\Net\Firewall\Ipset\FirewalldMapper;

	/**
	 * Class Ipset
	 *
	 * @package Opcenter\Net\Firewall
	 *
	 * Faster hash-based rule evaluation
	 */
	class Ipset
	{

		/**
		 * List ipset sets
		 *
		 * @return array
		 */
		public static function sets(): array
		{
			$proc = \Util_Process::exec('ipset list -o xml -q -n');
			if (!$proc['success']) {
				return [];
			}
			$xml = simplexml_load_string($proc['stdout']);
			$set = [];
			foreach ($xml->ipset as $item) {
				$set[] = (string)$item['name'];
			}

			return $set;
		}

		/**
		 * Get information on ip set
		 *
		 * @param string $set
		 * @return array
		 */
		public static function setInfo(string $set): array
		{
			$proc = \Util_Process_Safe::exec('ipset list -o xml -q %(set)s', ['set' => $set]);
			if (!$proc['success']) {
				return [];
			}
			$xml = simplexml_load_string($proc['stdout']);
			$data = (array)$xml->ipset->header;
			array_walk($data, static function (&$v, $k) {
				if ($k === 'hashsize' || $k === 'maxelem' || $k === 'timeout' || $k === 'memsize' || $k === 'references') {
					$v = (int)$v;
				}
			});

			return [
					'type' => (string)$xml->ipset->type
				] + $data;
		}

		/**
		 * Get members of set
		 *
		 * @param string|null $set
		 * @return array
		 */
		public static function getSetMembers(string $set = null): array
		{
			$proc = \Util_Process_Safe::exec('ipset list -o xml -q %(set)s', ['set' => $set]);
			if (!$proc['success']) {
				return [];
			}
			$xml = simplexml_load_string($proc['stdout']);
			$members = [];
			if (!$set) {
				$sets = $xml->ipset;
			} else {
				$sets = (array)$xml->xpath("ipset[@name='{$set}']");
			}
			foreach ($sets as $s) {
				$setName = (string)$s['name'];
				if (!$s->members->member) {
					continue;
				}
				$members[$setName] = [];
				foreach ($s->members->member as $m) {
					$members[$setName][] = [
						'host'    => (string)$m->elem,
						'timeout' => (int)$m->timeout
					];
				}
			}

			return $set ? $members[$set] ?? [] : $members;
		}

		public static function getChainFromSet(string $set): ?string
		{
			if (null === ($rule = self::getRuleFromSet($set))) {
				return null;
			}

			return $rule->getGroup();
		}

		/**
		 * Get rule from set name
		 *
		 * Named sets must appear only once
		 *
		 * @param string $set
		 * @return null|Iptables\Rule
		 */
		public static function getRuleFromSet(string $set): ?\Opcenter\Net\Firewall\Iptables\Rule
		{
			static $lookup = [];
			if (isset($lookup[$set])) {
				return $lookup[$set];
			}
			$LOOKFOR = 'match-set ';
			foreach (Iptables::getEntriesFromChain() as $chain => $entries) {
				foreach ($entries as $entry) {
					if (false === ($pos = strpos($entry['type'],
							$LOOKFOR)) || ($pos > 0 && $entry['type'][$pos - 1] !== ' ')) {
						continue;
					}
					$setname = substr($entry['type'], $pos + \strlen($LOOKFOR),
						strpos($entry['type'], ' ', $pos + \strlen($LOOKFOR)) - $pos - \strlen($LOOKFOR)/*spacing*/);
					$lookup[$setname] = new \Opcenter\Net\Firewall\Iptables\Rule($entry + ['chain' => $chain]);
				}
			}

			return $lookup[$set] ?? null;
		}

		/**
		 * Escape ipset options
		 *
		 * @param array $opts
		 * @return string
		 */
		private static function escapeOptions(array $opts): string
		{
			return implode(' ', array_key_map(static function ($k, $v) {
				return escapeshellarg($k) . ' ' . escapeshellarg((string)$v);
			}, $opts));
		}

		/**
		 * Create new set
		 *
		 * @param string $name
		 * @param string $type
		 * @param array  $opts
		 * @return bool
		 */
		public static function create(string $name, string $type, array $opts = []): bool
		{
			$opts = self::escapeOptions($opts);
			$ret = \Util_Process_Safe::exec('ipset create %(name)s %(type)s ' . $opts,
				['name' => $name, 'type' => $type]);

			return $ret['success'];
		}

		/**
		 * Add host to set
		 *
		 * @param string $set
		 * @param string $host
		 * @param array  $opts
		 * @return bool
		 */
		public static function add(string $set, string $host, array $opts = []): bool
		{
			$opts = self::escapeOptions($opts);
			$proc = new \Util_Process_Safe();
			$proc->addCallback(static function ($str) {
				if (false !== strpos($str, 'while userspace supports protocol versions')) {
					return false;
				}
				return true;
			});
			$ret = $proc->run('ipset -! add %(set)s %(host)s ' . $opts, ['set' => $set, 'host' => $host]);
			if ($ret['success']) {
				(new FirewalldMapper($host))->addTo($set);
			}

			return $ret['success'];
		}

		/**
		 * Remove host from set
		 *
		 * @param string $set
		 * @param string $host
		 * @param array  $opts
		 * @return bool
		 */
		public static function remove(string $set, string $host, array $opts = []): bool
		{
			$opts = self::escapeOptions($opts);
			$ret = \Util_Process_Safe::exec('ipset -! del %(set)s %(host)s ' . $opts, ['set' => $set, 'host' => $host]);
			if ($ret['success']) {
				(new FirewalldMapper($host))->removeFrom($set);
			}

			return $ret['success'];
		}

	}
