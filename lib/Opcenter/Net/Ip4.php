<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Net;

	use IPTools\Range;

	class Ip4 extends IpCommon
	{
		const NB_POOL = 'namebased_ip_addrs';
		const IP_DISCOVERY = [
			'dig TXT +short -4 o-o.myaddr.l.google.com @ns1.google.com',
			'dig +short -4 myip.opendns.com @resolver1.opendns.com'
		];
		const IP_ALLOCATION_RANGE = DNS_ALLOCATION_CIDR;
		const RESERVED_BLOCKS = [
			'0.0.0.0/8',
			'10.0.0.0/8',
			'100.64.0.0/10',
			'127.0.0.0/8',
			'169.254.0.0/16',
			'172.16.0.0/12',
			'192.0.0.0/24',
			'192.0.2.0/24',
			'192.88.99.0/24',
			'192.168.0.0/16',
			'198.18.0.0/15',
			'198.51.100.0/24',
			'203.0.113.0/24',
			'224.0.0.0/4',
			'240.0.0.0/4',
			'255.255.255.255/32'
		];
		public static function release(string $ip): bool
		{
			$dev = Iface::ip_iface($ip);
			if (OPCENTER_IP_BIND_CHECK && !Iface::bound($ip, $dev)) {
				return warn("IP address `%s' not actively bound to server", $ip);
			}

			return Iface::unbind($ip, $dev);
		}

		/**
		 * Allocate an IP address from a pool
		 *
		 * @param bool $assign bind on success
		 * @return string
		 */
		public static function allocate(bool $assign = true): string
		{
			foreach (static::IP_ALLOCATION_RANGE as $allocation) {
				try {
					$ipmask = Range::parse($allocation);
				} catch (\Exception $e) {
					warn("skipping invalid allocation block `%s'", $allocation);
					continue;
				}

				$min = (int)$ipmask->getFirstIP()->toLong() + 1;
				$max = (int)$ipmask->getLastIP()->toLong();
				for ($i = $min; $i < $max; $i++) {
					$ip = long2ip($i);
					if (!parent::ip_allocated($ip)) {
						if ($assign && !static::assign($ip, Iface::detect())) {
							fatal("failed to assign `%s' to interface `%s'",
								$ip,
								Iface::detect()
							);
						}

						return $ip;
					}
				}
			}
			fatal('cannot find free ip address!');
		}

		public static function assign(string $ip, string $iface = null): bool
		{
			if (null === $iface) {
				$iface = Iface::detect();
				info("Detected external interface `%s'. Using for `%s'.", $iface, $ip);
			}
			$interface = \Opcenter\Net\Iface::interfaces();
			if (!\in_array($iface, $interface, true)) {
				return error("unknown interface requested `%s'", $iface);
			}
			if (!Iface::bind($ip, $iface)) {
				return error("failed to bind `%s' to interface `%s'", $ip, $iface);
			}

			return true;

		}

		/**
		 * Get a pool of candidate addresses
		 *
		 * @return array
		 */
		public static function nb_pool(): array
		{
			$path = \Opcenter::mkpath(self::NB_POOL);
			if (file_exists($path)) {
				return file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			}

			return [static::my_ip()];
		}

		/**
		 * Get primary server IP address
		 *
		 * @return string
		 */
		public static function my_ip(): string
		{
			static $ip;
			if (!empty(DNS_MY_IP4)) {
				return DNS_MY_IP4;
			}
			if (!isset($ip)) {
				foreach ((array)static::IP_DISCOVERY as $cmd) {
					$ret = \Util_Process::exec($cmd);
					$ip = trim($ret['stdout'], "\n  \"");
					if ($ret['success'] && $ip) {
						break;
					}
				}

				if (!$ip) {
					fatal('IPv4 requires manual configuration in config.ini ([dns] => my_ip4)');
				}
			}

			return $ip;
		}

		public static function announce(string $ip, $iface = null)
		{
			if (null === $iface) {
				$iface = Iface::ip_iface($ip);
			}
			if (OPCENTER_IP_BIND_CHECK && !$iface) {
				fatal("could not detect interface for IP address `%s' - is IP bound?", $ip);
			} else if (!$iface) {
				$iface = Iface::detect();
			}
			$proc = new \Util_Process_Schedule('now');
			$newpath = join(PATH_SEPARATOR, array('/sbin', getenv('PATH')));
			$proc->setEnvironment('PATH', $newpath);
			$ret = $proc->run(
				'arping -U -I %s -c 1 %s',
				$iface,
				$ip
			);

			return $ret['success'];
		}

		/**
		 * Address is valid
		 *
		 * @param string $addr
		 * @return bool
		 */
		public static function valid(string $addr): bool
		{
			return false !== ip2long($addr);
		}

		public static function enabled(): bool
		{
			return file_exists('/proc/net/tcp');
		}


	}
