<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2019
	 */

	namespace Opcenter\Admin\Settings\System;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;

	class VirusScanner extends \Opcenter\Admin\Settings\VirusScanner\Enabled {
		public function set($val = ''): bool
		{
			warn('Scope is deprecated. Use scope:set virus-scanner.enabled');

			return parent::set($val);
		}

		public function get()
		{
			warn('Scope is deprecated. Use scope:get virus-scanner.enabled');

			return parent::get();
		}
	}