<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\VirusScanner;

	use Opcenter\Admin\Settings\Cp\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Malware\Clamd;

	class SignatureWhitelist implements SettingsInterface
	{
		const SIGNATURE_FILE = '/var/lib/clamav/custom-whitelist.ign2';

		public function set($val): bool
		{
			if ((new Bootstrapper())->get('clamav_client_only')) {
				return error("ClamAV in client-only mode. Make changes on %s",
					(new Bootstrapper())->get('clamav_clamd_tcp_addr')
				);
			}

			if (!ANTIVIRUS_INSTALLED) {
				return error('No anti-virus package installed');
			}

			if ($val === $this->get()) {
				// no need to set the value again
				return true;
			}


			$allSignatures = $this->get();
			$val = (array)$val;

			foreach ($val as &$v) {
				if (false !== ($pos = strpos($v, '(')) && $pos < strrpos($v, ')')) {
					warn("Removed `%s' from signature", substr($v, $pos));
					$v = substr($v, 0, $pos);
				}

				if (substr($v, ($len = -\strlen('.UNOFFICIAL'))) === ".UNOFFICIAL") {
					$v = substr($v, 0, $len);
				}

				if (\in_array($v, $allSignatures, true)) {
					$v = null;
				}
			}
			unset ($v);

			$path = $this->getStorage();
			if ( !($filtered = array_filter($val)) ) {
				return true;
			}
			$filtered = array_merge($allSignatures, $filtered);
			if (false === file_put_contents($path, implode("\n", $filtered))) {
				return error('Failed to write to %s', $path);
			}

			Clamd::reload('2 minutes');
			return info('clamd will reload in 2 minutes');
		}

		public function get()
		{
			return file(static::SIGNATURE_FILE, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		}

		private function getStorage(): string
		{
			return static::SIGNATURE_FILE;
		}

		public function getHelp(): string
		{
			return 'Whitelist virus signatures (append-only)';
		}

		public function getValues()
		{
			return 'array';
		}

		public function getDefault()
		{
			return [];
		}

	}
