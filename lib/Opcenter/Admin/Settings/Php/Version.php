<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\Php;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Versioning;

	class Version implements SettingsInterface
	{
		public function set($val): bool
		{
			$val = false === strpos((string)$val, '.') ? "${val}.0" : (string)$val;

			if (file_exists('/usr/bin/php') && $val === $this->get()) {
				// allow removal of /usr/bin/php to shortcircuit check
				return true;
			}

			self::versionCheck($val);
			$cfg = new Config();

			$cfg['system_php_version'] = str_replace(',', '.', $val);
			unset($cfg);
			Bootstrapper::run('php/install', 'php/install-pecl-module', 'php/create-configuration', 'apnscp/php-filesystem-template', ['php_force_build' => true]);

			return true;
		}

		public function get()
		{
			$version = (new Config())->offsetGet('system_php_version');
			return false === strpos((string)$version, '.') ? Versioning::asMinor((string)$version) : (string)$version;
		}

		public function getHelp(): string
		{
			return 'Change system-wide PHP version';
		}

		public function getValues()
		{
			return ['5.6','7.0','7.1','7.2','7.3','7.4','8.0'];
		}

		public function getDefault()
		{
			return '7.4';
		}

		/**
		 * Check if version EOL'd
		 *
		 * @param string $version
		 * @return bool version is recent
		 */
		public static function versionCheck(string $version): bool
		{
			if (version_compare($version, '7.3', '<')) {
				warn('Using PHP below 7.3 is not recommended. PHP 5.6/7.0 reached end-of-life December 2018. ' .
					'PHP 7.1 reached end-of-life December 2019. PHP 7.2 reached end-of-life November 2020. ' .
					'See also %s', 'http://php.net/supported-versions.php'
				);

				return false;
			}

			return true;
		}
	}