<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, November 2019
 */

	namespace Opcenter\Admin\Settings\Opcenter;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Filesystem;
	use Opcenter\Provisioning\ConfigurationWriter;

	class AccountCleanup implements SettingsInterface
	{
		const CRON_FILE = '/etc/cron.daily/remove_suspended_accounts';

		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}
			if (!$val) {
				if (file_exists(self::CRON_FILE)) {
					return unlink(self::CRON_FILE);
				}
				return true;
			}

			$parsed = strtotime($val);
			$now = time();
			if ($parsed > $now) {
				return error("Timespec `%s' is set in the future. Current time %d, timespec parsed as %d", $val, $now, $parsed);
			}

			return (new ConfigurationWriter('opcenter/account-cleanup-cron', null))->compile(
				[
					'timespec' => $val,
					'apnscp_root' => INCLUDE_PATH
				]
			)->write(self::CRON_FILE) && Filesystem::chogp(self::CRON_FILE, 0, 0, 0700);
		}

		public function get()
		{
			if (!file_exists(self::CRON_FILE)) {
				return false;
			}
			$contents = file_get_contents(self::CRON_FILE);
			if (preg_match('/^\s*TIMESPEC\s*=\s*(.*)$/m', $contents, $matches)) {
				return trim($matches[1], '"\'');
			}

			warn('Cannot determine timespec from cron %s', self::CRON_FILE);
			return false;
		}

		public function getHelp(): string
		{
			return 'Periodically clean-up accounts on the server';
		}

		public function getValues()
		{
			return 'string|false';
		}

		public function getDefault()
		{
			return true;
		}

	}
