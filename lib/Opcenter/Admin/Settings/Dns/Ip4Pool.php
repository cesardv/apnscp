<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2019
	 */

	namespace Opcenter\Admin\Settings\Dns;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Net\Iface;

	class Ip4Pool implements SettingsInterface
	{
		use \NamespaceUtilitiesTrait;

		public function get(...$var)
		{
			$pool = '\\Opcenter\\Net\\' . $this->getPoolType();
			return $pool::nb_pool();
		}

		/**
		 * Set pool
		 *
		 * @param       $val
		 * @return bool
		 */
		public function set($val): bool
		{
			if (!\is_array($val)) {
				$val = [$val];
			}

			if ($val === $this->get()) {
				return true;
			}

			$pool = '\\Opcenter\\Net\\' . $this->getPoolType();
			foreach ($val as $v) {
				if (!$pool::valid($v)) {
					return error("Invalid %s address specified `%s'", $pool, $v);
				}
				if (!Iface::bound($v)) {
					return error("IP address `%s' is not bound to any interface on the server", $v);
				}
			}
			$path = \Opcenter::mkpath($pool::NB_POOL);
			if (file_put_contents($path, implode("\n", $val)) === false) {
				return false;
			}

			return \Opcenter\Admin\Bootstrapper::run('apache/configure');
		}

		public function getHelp(): string
		{
			$type = $this->getPoolType();
			return "Set ${type} namebased pool";
		}

		public function getValues()
		{
			return 'array';
		}

		public function getDefault()
		{
			$pool = '\\Opcenter\\Net\\' . $this->getPoolType();
			return (array)$pool::my_ip();
		}

		/**
		 * Get pool type
		 *
		 * @return string
		 */
		protected function getPoolType(): string {
			return static::class === self::class ? 'Ip4' : 'Ip6';
		}
	}