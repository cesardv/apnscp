<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Opcenter\Admin\Settings\Net;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Map;
	use Opcenter\Net\Iface;

	class Hostname implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}
			$ip = \Net_Gethost::gethostbyname_t($val);
			if (!preg_match(\Regex::DOMAIN, $val)) {
				warn("Provided hostname `%s' is not fully-qualified", $val);
			} else if (!$ip) {
				warn("Cannot resolve `%s' to an IP address - is this domain configured?", $val);
			} else if (!Iface::bound((string)$ip)) {
				warn("Hostname IP address `%s' not detected on this server", $ip);
			}
			if (null !== ($siteid = \Auth::get_site_id_from_domain($val))) {
				return error("Server hostname cannot be same as hosted domain. `%s' attached to `site%d'", $val, $siteid);
			}
			$cfg = new Config();
			$cfg['system_hostname'] = $val;
			$cfg->sync();
			return Bootstrapper::run(
				'network/hostname',
				'mail/configure-postfix',
				'apnscp/bootstrap',
				'apache/configure',
				[
					'force' => true
				]
			);
		}

		public function get()
		{
			return gethostname();
		}

		public function getHelp(): string
		{
			return 'Change system hostname';
		}

		public function getValues()
		{
			return 'string';
		}

		public function getDefault()
		{
			return gethostname();
		}
	}