<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Mail;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;

	class InsecureSsl implements SettingsInterface
	{
		const KEY = 'mail_insecure_ssl';

		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			(new Bootstrapper\Config())->offsetSet(self::KEY, (bool)$val);

			return Bootstrapper::run('mail/configure-dovecot', 'mail/configure-postfix', 'software/haproxy');
		}

		public function get()
		{
			return array_get(new Bootstrapper\Config(), self::KEY, $this->getDefault());
		}

		public function getHelp(): string
		{
			return 'Enable legacy TLS v1.0 and v1.1 support';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}

	}
