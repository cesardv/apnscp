<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2019
	 */

	namespace Opcenter\Admin\Settings\Mail;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class Enabled implements SettingsInterface
	{

		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			if (!\is_bool($val)) {
				return error("Unknown value `%s'", $val);
			}

			$cfg = new Config();
			$cfg['mail_enabled'] = $val;
			if (!$val && !$cfg['rspamd_enabled']) {
				warn('rspamd is disabled. This server will not be able to rate-limit ' .
					'or filter any mail that leaves this server. Consider turning rspamd on with ' .
					'cpcmd scope:set mail.spam-filter rspamd');
			}
			if (!$val) {
				$cpcfg = new \Opcenter\Admin\Settings\Cp\Config();
				$cpcfg->setRestart(false);
				$cpcfg->set('mail', 'provider_default', 'null');
			} else if (MAIL_PROVIDER_DEFAULT === 'null') {
				warn(
					"Mail enabled for platform but default mail provider setting is still 'null'. To enable " .
					"mail on new accounts, change the provider from 'null' to 'builtin': " .
					'cpcmd scope:set cp.config mail provider_default builtin');
			}
			unset($cfg, $cpcfg);
			// @todo remove webmail, make config.ini entry
			Bootstrapper::run('mail/configure-dovecot', 'mail/rspamd', 'mail/spamassassin', 'mail/configure-postfix',
				'software/haproxy');

			return true;
		}

		public function get()
		{
			$config = new Config();
			return (bool)$config['mail_enabled'];
		}

		public function getHelp(): string
		{
			return 'Enable mail reception on server';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return true;
		}
	}
