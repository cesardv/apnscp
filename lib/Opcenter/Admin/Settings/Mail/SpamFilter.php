<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, February 2019
 */

	namespace Opcenter\Admin\Settings\Mail;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class SpamFilter implements SettingsInterface
	{
		const FILTERS = ['spamassassin', 'rspamd', false];

		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			if (!\in_array($val, static::FILTERS, true)) {
				return error("Unknown spam filter `%s'", $val);
			}
			$cfg = new Config();
			$cfg['spamfilter'] = $val;
			if ($val === 'spamassassin') {
				if (!$cfg['mail_enabled']) {
					warn("`mail_enabled' is set to false. spamfilter is implicitly disabled when `spamassassin' used");
				}
				$cfg->sync();
				if (!$cfg['spamassassin_enabled']) {
					warn("`spamassassin_enabled' is set to false. Changing to true.");
					$cfg['spamassassin_enabled'] = true;
				}
			}
			unset($cfg);
			Bootstrapper::run('mail/configure-dovecot', 'mail/configure-postfix', 'mail/spamassassin', 'mail/rspamd', 'software/argos');

			return true;
		}

		public function get()
		{
			$config = new Config();

			if ('rspamd' === ($filter = $config['spamfilter'])) {
				return 'rspamd';
			}
			return $config['mail_enabled'] ? $filter : false;
		}

		public function getHelp(): string
		{
			return 'Mail spam filter';
		}

		public function getValues()
		{
			return self::FILTERS;
		}

		public function getDefault()
		{
			return 'spamassassin';
		}
	}