<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Opcenter\Admin\Settings;

	class Setting
	{
		use \NamespaceUtilitiesTrait;

		private static $bound = [];

		/**
		 * Register additional Scope bindings
		 */
		public static function register(string $class, string $alias = null)
		{
			if (null === $alias) {
				$alias = static::scopeName($class);
			}
			if (!class_exists($class)) {
				fatal('Named Scope %(scope)s implementation %(class)s does not exist',
				['scope' => $alias, 'class' => $class]);
			}
			self::$bound[$alias] = $class;
		}

		/**
		 * Load all Scopes
		 *
		 * @return array
		 */
		public static function list(): array
		{
			$path = INCLUDE_PATH . '/lib/Opcenter/Admin/Settings';
			$c = [];
			if (!$dh = opendir($path)) {
				return $c;
			}
			while (false !== ($class = readdir($dh))) {
				if ($class === '.' || $class === '..') {
					continue;
				}
				if (false !== strpos($class, '.')) {
					continue;
				}
				$config = glob("${path}/${class}/*.php");
				$class = strtolower(snake_case(basename($class, '.php'), '-'));


				array_push($c, ...array_map(static function ($f) use ($class) {
					return $class . '.' . strtolower(snake_case(basename($f, '.php'), '-'));
				}, $config));
			}
			closedir($dh);
			$c = array_merge($c, array_keys(self::$bound));
			return array_values($c);
		}

		/**
		 * Load instance of setting
		 *
		 * @param string $name
		 * @return SettingsInterface
		 */
		public static function load(string $name): SettingsInterface
		{
			$c = static::className($name);

			return new $c;
		}

		/**
		 * Convert Scope into class
		 *
		 * @param string $name
		 * @return string|null
		 */
		public static function className(string $name): ?string
		{
			$parts = explode('.', $name);
			if ($parts[0] === 'apnscp') {
				debug('Forwarding request to cp.%s', $parts[1]);
				$parts[0] = 'cp';
			}
			$parts = implode('\\', array_map('studly_case', $parts));

			if (!class_exists($c = self::appendNamespace($parts))) {
				return null;
			}

			return get_parent_class($c) ?: $c;
		}

		/**
		 * Convert class into Scope name
		 *
		 * @param string $name
		 * @return string
		 */
		public static function scopeName(string $name): string
		{
			$name = str_replace('\\', '/', $name);
			$fqn = [
				snake_case(basename($name), '-'),
				snake_case(basename(\dirname($name)), '-')
			];
			return implode('.', array_reverse($fqn));
		}
	}