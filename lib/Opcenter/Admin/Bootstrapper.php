<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin;

	use Illuminate\Contracts\Queue\ShouldQueue;
	use Lararia\JobDaemon;
	use Lararia\Jobs\BootstrapperTask;
	use Lararia\Jobs\Job;
	use Opcenter\Process;

	class Bootstrapper
	{
		private const PID_FILE = '.ansible.lock';

		public const RUNTIME_VARS = '/root/apnscp-vars-runtime.yml';

		public const ROLE_HOME = 'playbooks/roles/';

		/**
		 * Run a bootstrapper job
		 *
		 * @param mixed $tasks tags are string, extra-vars passed as assoc
		 * @return bool
		 */
		public static function run(...$tasks): bool
		{
			$tags = [];
			$args = [];
			for ($i = 0, $n = \count($tasks); $i < $n; $i++) {
				$t = $tasks[$i];
				if (\is_string($t)) {
					$tags[] = $t;
				} else if (\is_array($t)) {
					$args[] = json_encode($t);
				}
			}
			// @TODO queue support
			$job = Job::create(BootstrapperTask::class, 'bootstrap.yml', $args);
			if ($tags) {
				$job->setTags($tags);
			}
			$job->dispatch();
			if (($job instanceof ShouldQueue) && JobDaemon::checkState()) {
				$frag = $tags ? 'with roles: ' . implode(', ', $tags) : '';
				info('Bootstrapper task running in background %s', $frag);
			}
			return true;
		}

		/**
		 * Service is running
		 *
		 * @return bool
		 */
		public static function running(): bool
		{
			$pidPath = static::pidFile();
			if (!file_exists($pidPath)) {
				return false;
			}

			$pid = (int)file_get_contents($pidPath);

			if (!Process::exists($pid)) {
				return false;
			}
			$stat = Process::stat($pid);
			if (!$stat) {
				return false;
			}
			// disambiguate "python" commands by checking command-line
			return 0 === strncmp($stat['comm'], 'ansible', 7);
		}

		/**
		 * Get PID file
		 *
		 * @return string
		 */
		private static function pidFile(): string
		{
			return run_path(self::PID_FILE);
		}

		public static function rolePath(string $role): string
		{
			return resource_path(self::ROLE_HOME . $role);
		}
	}