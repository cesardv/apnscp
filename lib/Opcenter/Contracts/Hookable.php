<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	/**
	 * Enables module callbacks via virtdomain operations
	 * AddDomain, EditDomain, DeleteDomain
	 */

	namespace Opcenter\Contracts;

	use Opcenter\Service\ConfigurationContext;

	interface Hookable
	{
		/**
		 * Configuration verification
		 *
		 * @param ConfigurationContext $ctx configuration class
		 * @return bool
		 */
		public function _verify_conf(ConfigurationContext $ctx): bool;

		/**
		 * Post-account creation
		 *
		 * Configuration is imported via Auth::profile()->conf
		 *
		 * @return bool|null
		 */
		public function _create();

		/**
		 * Fired before an account is deleted
		 *
		 * Configuration is imported via Auth::profile()->conf
		 *
		 * @see \Opcenter\Account\Delete
		 * @return bool|null
		 */
		public function _delete();

		/**
		 * Fired after service metadata change
		 *
		 * Configuration is imported via Auth::profile()->conf
		 *
		 * @see \Opcenter\Account\Edit
		 * @return bool|null
		 */
		public function _edit();


		/**
		 * Hook called after a user is created
		 *
		 * @param string $user
		 * @return mixed
		 */
		public function _create_user(string $user);

		/**
		 * Hook called before a user is deleted
		 *
		 * @param string $user
		 * @return mixed
		 */
		public function _delete_user(string $user);

		/**
		 * Hook called after a user is edited
		 *
		 * @param string $userold
		 * @param string $usernew
		 * @param array  $oldpwd
		 * @return mixed
		 */
		public function _edit_user(string $userold, string $usernew, array $oldpwd);

	}


