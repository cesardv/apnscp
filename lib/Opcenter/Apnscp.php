<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2018
	 */

	namespace Opcenter;

	class Apnscp
	{
		private const RELOAD_KEY = 'apnscprld';
		public const PID = '/storage/run/apnscpd.pid';
		public const HOUSEKEEPER_PID = 'housekeeper.pid';

		/**
		 * Restart apnscp
		 *
		 * @param string $timespec
		 * @return bool
		 */
		public static function restart(string $timespec = 'now'): bool
		{
			if ($timespec === 'now') {
				$proc = new \Util_Process_Fork();
			} else {
				$proc = new \Util_Process_Schedule($timespec);
				$proc->setID(self::RELOAD_KEY);

				if ($proc->preempted()) {
					return true;
				}
				info('%s will restart in %s', PANEL_BRAND, $timespec);
			}
			$cmd = platform_is('6.5') ? '/usr/bin/systemctl restart apiscp' : '/usr/bin/service apiscp restart';
			$res = $proc->run(
				$cmd,
				array('mute_stdout' => true)
			);

			return $res['success'];
		}

		/**
		 * Wait for backend activation
		 *
		 * @return bool
		 */
		public static function wait(): bool
		{
			$pid = INCLUDE_PATH . self::PID;
			$self = basename(PHP_BINARY);
			for ($i = 0; $i < 30; usleep(500000), $i++) {
				if (!file_exists($pid)) {
					continue;
				}

				if (!Process::pidMatches((int)file_get_contents($pid), $self)) {
					continue;
				}

				$ds = \DataStream::get();
				if (0 !== strncmp((string)$ds->writeSocket('PING' . \DataStream::MAGIC_MARKER), 'PONG', 4)) {
					return true;
				}
			}

			warn('Failed to query backend - is apnscpd dead?');
			return false;
		}

		public static function running(): bool
		{
			if (!file_exists(INCLUDE_PATH . self::PID)) {
				return false;
			}

			if (null === ($pid = self::pid())) {
				return false;
			}

			return \Opcenter\Process::pidMatches($pid, basename(PHP_BINARY));
		}

		public static function pid(): ?int
		{
			if (!file_exists(INCLUDE_PATH . self::PID)) {
				return null;
			}

			return (int)file_get_contents(INCLUDE_PATH . self::PID);
		}
	}