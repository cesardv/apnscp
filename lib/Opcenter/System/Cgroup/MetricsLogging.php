<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, November 2019
 */

namespace Opcenter\System\Cgroup;

class MetricsLogging
{
	private static $attributeCache = [];

	private $controller;

	public function __construct(Controller $controller)
	{
		$this->controller = $controller;
	}

	/**
	 * Get loggable attributes
	 *
	 * @return array
	 */
	public function getLoggableAttributes(): array
	{
		$missing = $attrs = array_fill_keys(
			array_values($this->controller->getMetrics()),
			null
		);
		if ($knownAttrs = array_get(static::$attributeCache, $this->controller->getType(), [])) {
			$missing = array_diff_key($attrs, $knownAttrs);
			if (!$missing) {
				return $knownAttrs;
			}
		}
		$attrs = $knownAttrs + $this->populateMissingAttributes($missing);

		self::$attributeCache[$this->controller->getType()] = $attrs;

		return $attrs;
	}

	/**
	 * Convert controller attributes into DAPHNIE metric symbols
	 *
	 * @param string|array $attrs
	 * @return array
	 */
	public function getMetricTokensFromAttributes($attrs): array
	{
		return $this->buildTokensFromAttributes((array)$attrs);
	}

	/**
	 * Fetch or create attribute IDs for unknown metrics
	 *
	 * @param array $attrs
	 * @return array [metric name => id]
	 */
	private function populateMissingAttributes(array $attrs): array
	{
		if (!$attrs) {
			debug("Empty attribute set for `%s' passed", $this->controller->getType());
			return [];
		}
		$tokens = $this->buildTokensFromAttributes($attrs);
		$db = \PostgreSQL::pdo();
		$query = 'SELECT attr_id, name FROM metric_attributes WHERE name IN(' .
			implode(',', array_map(static function ($v) use ($db) {
				return $db->quote($v);
			}, $tokens)) . ')';
		if (!$rs = $db->query($query)) {
			error('Failed to collect attribute tokens for %s', $this->controller->getType());
			return [];
		}

		$attrs = array_build($rs->fetchAll(\PDO::FETCH_ASSOC), static function ($k, $v) use (&$tokens) {
			$idx = array_search($v['name'], $tokens, true);
			if (false === $idx) {
				fatal('Assertion failed. %s not found in token list?', $v['name']);
			}
			return [$idx, $v['attr_id']];
		});

		$missing = array_diff_key($tokens, $attrs);
		$stmt = $db->prepare('INSERT INTO metric_attributes(name, label, type) VALUES(:name, :label, :type)');
		foreach ($missing as $attr => $token) {
			if (!$rs = $stmt->execute([
				':name'  => $token,
				':label' => $this->controller->getMetricLabel($attr),
				':type'  => $this->controller->getMetricDataType($attr)
			])) {
				warn('Failed to add metric %s: (%s) %s', $token, $stmt->errorCode(), $stmt->errorInfo());
				unset($attrs[$attr]);
				continue;
			}
			$attrs[$attr] = (int)$db->lastInsertId('metric_attributes_attr_id_seq');
		}

		return $attrs;
	}

	/**
	 * Build database tokens from group attributes
	 *
	 * @param array $attrs
	 * @return array
	 */
	private function buildTokensFromAttributes(array $attrs): array
	{
		return array_build($attrs, function ($k, $v) {
			return [$k, 'c-' . $this->controller->getType() . '-' . $k];
		});
	}
}