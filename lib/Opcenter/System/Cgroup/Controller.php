<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace Opcenter\System\Cgroup;

	use Opcenter\System\Cgroup;
	use Opcenter\System\Cgroup\Contracts\ControllerAttribute;

	abstract class Controller implements Cgroup\Contracts\Renderable
	{
		use \NamespaceUtilitiesTrait;
		// @var array account meta <=> controller attributes
		protected const ATTRIBUTES = [];
		// @var string $type
		protected $type;
		protected $permissions;
		// @var ControllerAttribute $attributes
		protected $attributes = [];
		protected $group;
		protected $dirty = false;

		// @var array metrics logged by crontab::_cron()
		public const LOGGABLE_METRICS = [];

		private function __construct(Group $group, array $permissions)
		{
			$this->group = $group;
			$this->type = strtolower(static::getBaseClassName());
			$this->permissions = new Permissions($permissions);
		}

		/**
		 * Get controller type
		 *
		 * @return string
		 */
		public function getType(): string
		{
			return $this->type;
		}

		public static function make(Group $group, ?string $type, array $permissions = []): self
		{
			if (null === $type && ($type = static::getBaseClassName()) === static::getBaseClassName(self::class)) {
				fatal('$type must be specified when calling %s directly', self::class);
			}
			$c = self::class . 's\\' . ucwords($type);

			return new $c($group, array_merge($group->getPermissions(), $permissions));
		}

		public function __destruct()
		{
			if ($this->dirty) {
				$this->create();
			}
		}

		public function create(): bool
		{
			$this->dirty = false;
			$args = $this->permissions->get();
			$args['controller'] = $this->type;
			$args['group'] = $this->group;
			$ret = \Util_Process::exec('cgcreate --dperm=%(admin.dperm)o -a %(admin.uid)s:%(admin.gid)s --fperm=%(admin.fperm)o --tperm=%(task.fperm)o -g %(controller)s:%(group)s ' .
				'-t %(task.uid)s:%(task.gid)s', $args);
			if (!$ret['success']) {
				return error("failed to create cgroup `%s:%s': %s", $args['controller'], $args['group'],
					$ret['stderr']);
			}
			foreach ($this->getAttributes() as $attr) {
				$attr->activate();
			}

			return true;
		}

		/**
		 * Get configured attributes
		 *
		 * @return ControllerAttribute[]
		 */
		public function getAttributes(): array
		{
			return $this->attributes;
		}

		public function exists(): bool
		{
			return file_exists($this->getPath() . '/tasks');
		}

		/**
		 * Get controller path
		 *
		 * @return string
		 */
		public function getPath(): string
		{
			return CGROUP_HOME . '/' . $this->type . '/' . $this->group;
		}

		/**
		 * Import account meta into cgroup vars
		 *
		 * @param \apnscpFunctionInterceptor $afi
		 * @return int
		 */
		public function import(\apnscpFunctionInterceptor $afi, \Auth_Info_User $ctx = null): int
		{
			$ctx = $ctx ?? \Auth::profile();
			$cnt = 0;
			foreach (array_intersect_key($afi->cgroup_get_limits(), static::ATTRIBUTES) as $name => $value) {
				$cgparam = static::ATTRIBUTES[$name];
				$attribute = $this->makeAttribute($cgparam);
				$attribute = new $attribute($value, $this);
				$this->setAttribute($attribute);
				$cnt++;
			}

			return $cnt;
		}

		/**
		 * Convert a service value into cgroup attribute
		 *
		 * @param string $attribute
		 * @return Controller
		 */
		public function fromParameter(string $parameter): Cgroup\Attributes\BaseAttribute
		{
			$c = $this->makeAttribute(static::ATTRIBUTES[$parameter]);
			return new $c(null, $this);
		}

		/**
		 * Controller has cgroup service parameter
		 *
		 * @param string $parameter
		 * @return bool
		 */
		public function hasParameter(string $parameter): bool
		{
			return isset(static::ATTRIBUTES[$parameter]);
		}

		/**
		 * Get class representation of attribute
		 *
		 * @param string $param
		 * @return string
		 */
		private function makeAttribute(string $param)
		{
			[$controller, $param] = explode('.', $param, 2);

			return implode('\\',
				[self::getNamespace(__CLASS__), 'Attributes', ucwords($controller), studly_case(str_replace('.', '-', $param))]);
		}

		public function setAttribute(ControllerAttribute $attr): bool
		{
			$this->dirty = true;
			$this->attributes[(string)$attr] = $attr;

			return true;
		}

		public function __toString()
		{
			return $this->type;
		}

		public function build(): string
		{
			if (empty($attr = $this->getAttributes())) {
				return '';
			}
			$config = $this->type . ' { ';

			foreach ($this->getAttributes() as $attribute) {
				if (null === ($value = $attribute->getValue())) {
					continue;
				}
				if (\is_string($value) && false !== strpos($value, ' ')) {
					$value = '"' . str_replace('"', '\\"', $value) . '"';
				}
				$config .= "${attribute}=" . $value . ';';
			}

			return $config . '} ';
		}


		/**
		 * Get logged cgroup metrics
		 *
		 * @return array
		 */
		public function getMetrics(): array
		{
			return array_keys(static::LOGGABLE_METRICS);
		}

		/**
		 * Get metric type
		 *
		 * @param string $metric
		 * @return string monotonic or value
		 */
		public function getMetricDataType(string $metric): string
		{
			return static::LOGGABLE_METRICS[$metric]['type'] ?? 'value';
		}

		/**
		 * Get metric label
		 *
		 * @param string $metric metric name
		 * @return string label
		 */
		public function getMetricLabel(string $metric): string
		{
			return static::LOGGABLE_METRICS[$metric]['label'] ?? "Resource $metric";
		}

		/**
		 * Get metric counter
		 *
		 * @param string $metric
		 * @return string|null
		 */
		public function getCounter(string $metric): string
		{
			$counter = static::LOGGABLE_METRICS[$metric]['counter'] ?? null;
			if ($counter === null) {
				fatal("Unknown counter for metric `%s' in %s", $metric, $this->getType());
			}

			return $counter;
		}

		/**
		 * Read counters from controller
		 *
		 * @param array $params
		 * @return array
		 */
		public function readCounters(array $params): array
		{
			$read = [];

			if (!is_dir($path = $this->getPath())) {
				return [];
			}
			foreach ($params as $p) {
				$counterPath = $this->getPath() . '/' . $this->getCounter($p);
				$read[$p] = (int)$this->readRawCounter($counterPath);
			}
			return $read;
		}

		/**
		 * Direct counter read
		 *
		 * @param string $path
		 * @return string
		 */
		protected function readRawCounter(string $path): string
		{
			return file_get_contents($path);
		}
	}