<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
	 */


	namespace Opcenter\Map;

	use Opcenter\MapInterface;

	/**
	 * Manage a JSON representation
	 *
	 * @package Opcenter\Map
	 */
	class UnboundedJson implements MapInterface, \ArrayAccess, \Iterator
	{
		// @var string map filename
		protected $file;
		// @var resource map fp
		protected $fp;

		/**
		 * @var mixed
		 */
		protected $contents;

		public function __construct(string $file, string $mode)
		{
			if ($file[0] !== '/') {
				fatal('File must be absolute path');
			}

			$this->file = $file;
			if (!$this->fp = fopen($file, $mode)) {
				fatal("Failed to open file `%s'", $file);
			}
			$this->readFile();
		}

		/**
		 * Process textfile
		 */
		protected function readFile()
		{
			$this->contents = json_decode('{' . fread($this->fp, filesize($this->file)) . '}', true);
			if (null === $this->contents) {
				fatal('Failed to decode JSON in %(file)s: %(err)s',
					['file' => $this->file, 'err' => json_last_error_msg()]);
			}

		}

		public function __debugInfo()
		{
			return $this->contents;
		}

		public function __destruct()
		{
			$this->close();
		}

		public function close()
		{
			if (\is_resource($this->fp)) {
				fclose($this->fp);
			}

		}

		public function __toString()
		{
			return (string)substr(\json_encode($this->contents, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES), 1, -1);
		}

		public function section(?string $term): MapInterface
		{
			return $this;
		}

		public function quoted(bool $val = null)
		{
			// no-op
			return;
		}

		public function fetch($key)
		{
			return $this->offsetGet($key);
		}

		public function offsetGet($offset)
		{
			return $this->contents[$offset];
		}

		public function delete($key): bool
		{
			if (!$this->offsetExists($key)) {
				return false;
			}
			$this->offsetUnset($key);

			return true;
		}

		public function offsetExists($offset)
		{
			return isset($this->contents[$offset]);
		}

		public function offsetUnset($offset)
		{
			$this->contents[$offset] = null;
		}

		public function insert(string $key, string $value): bool
		{
			if ($this->exists($key)) {
				return false;
			}
			$this->offsetSet($key, $value);

			return true;
		}

		public function exists(string $key): bool
		{
			return $this->offsetExists($key);
		}

		public function offsetSet($offset, $value)
		{
			$this->contents[$offset] = $value;
		}

		public function set(string $key, string $value): bool
		{
			$this->offsetSet($key, $value);

			return true;
		}

		public function fetchAll(): array
		{
			return $this->contents;
		}

		public function replace(string $key, string $value): bool
		{
			$this->offsetSet($key, $value);

			return true;
		}

		public function save(): bool
		{
			return ftruncate($this->fp, 0) && rewind($this->fp) && fwrite($this->fp, (string)$this);
		}

		public function truncate()
		{
			$this->contents = [];

			return ftruncate($this->fp) && fseek($this->fp, 0);
		}

		public function copy($contents)
		{
			// hold off for now, potential to duplicate comment lines
			return error('Copy operation not supported on %s', __CLASS__);
		}

		public function optimize()
		{
			return true;
		}

		public function current()
		{
			return current($this->contents);
		}

		public function rewind()
		{
			rewind($this->contents);
		}

		public function key()
		{
			return key($this->contents);
		}

		public function valid()
		{
			return null !== key($this->contents);
		}

		public function next()
		{
			next($this->contents);
		}
	}
