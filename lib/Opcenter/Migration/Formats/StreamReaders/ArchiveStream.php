<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2019
 */

namespace Opcenter\Migration\Formats\StreamReaders;

use Opcenter\Filesystem;
use Opcenter\Migration\Contracts\BackupStreamInterface;
use Opcenter\Migration\Locker;
use Opcenter\System\Memory;

class ArchiveStream extends \PharData implements BackupStreamInterface {

	// @var string cPanel backup root
	/**
	 * @var Locker
	 */
	protected $lock;
	private $root;
	private $file;
	private $baseFormat;

	public function __construct($fname, $flags = null, $alias = null, $format = \Phar::TAR)
	{
		/**
		 * PharData is tricky. via ext/phar/phar.c, it won't handle existing streams negating
		 * a wrapper, e.g. test://foo where foo is a gzopen() stream. Likewise this buffers
		 * the entire archive into memory creating OOM conditions if fsize(archive) > memory_limit
		 *
		 * Do an intermediate decompression to tar and cleanup at end
		 */
		if (substr($fname, -3) === '.gz') {
			$format |= \Phar::GZ;
		}
		$this->file = $fname;
		// order of magnitude for safety
		if (filesize($fname)*10 > min(8*1024*1024*1024, \Formatter::changeBytes(Memory::stats()['memavailable']*0.90, 'B', 'KB'))) {
			throw new \RuntimeException('Archive too large for in-situ import, switching to extraction');
		}
		parent::__construct($this->file, $flags, $alias, $format);
		$this->root = $this->getPathname();
	}

	public function __destruct()
	{
		parent::__destruct();
		if (!$this->baseFormat) {
			return;
		}
		info('Recompressing %s to %s%s', $this->file, $this->file, $this->baseFormat);
		\Util_Process_Safe::exec($this->getBin() . ' %s', $this->file);

		$this->lock->unlock();
	}

	public static function getBin() {
		if (file_exists('/usr/bin/pigz')) {
			return '/usr/bin/pigz -p' . (NPROC + 1);
		}
		return '/usr/bin/gzip';
	}

	/**
	 * Hold lock until archive can be recompressed
	 *
	 * @param Locker $lock
	 */
	public function bind(Locker $lock): void {
		$this->lock = $lock;
	}

	/**
	 * Get archive base path
	 *
	 * @return string
	 */
	public function getPrefix(): string {
		return  $this->root;
	}

	/**
	 * Return iterator from path
	 *
	 * @param string $file
	 * @return \DirectoryIterator
	 */
	public function archivePath(string $file): \DirectoryIterator {
		return new \DirectoryIterator($this->getPrefix() . '/' . $file);
	}

	/**
	 * Extract directory from archive
	 *
	 * @param string $subdir
	 * @param string $dest
	 * @param int    $uid
	 * @param int    $gid
	 * @return bool
	 */
	public function externalExtract(string $subdir, string $dest, int $uid = 0, int $gid = 0): bool
	{
		$this->rewind();
		if (false === $this->getPathname()) {
			return warn('Requested path missing, skipping extraction');
		}
		$proc = new \Util_Process_Safe();
		echo 'INITIALIZING...';
		$proc->addCallback(static function ($output) {
			echo "\r" . rtrim($output);
		}, 'read');
		// allow unprivileged user to read privileged file
		$proc->setDescriptor(0, 'file', [$this->file, 'rb'], null, ['mute_stdin' => false]);
		$proc->addCallback(static function ($output) use ($subdir, $dest) {
			echo "\nFINISHED - $subdir => $dest\n";
		}, 'close');
		$compressionFlag = substr($this->file, -3) === '.gz' ? 'z' : null;
		if (!is_dir($dest) && !is_link($dest)) {
			Filesystem::mkdir($dest, $uid, $gid);
		}
		$ret = $proc->run(
			'/usr/bin/setpriv --keep-groups --euid %d --egid %d -- /bin/tar -x%sspf - --checkpoint=10 --strip-components=%d -C %s --no-xattrs --no-acls --no-same-owner --numeric-owner %s%s',
			$uid,
			$gid,
			$compressionFlag,
			\count(explode('/', str_replace('//', '/', $subdir))),
			$dest,
			\basename($this->getPathname()),
			$subdir
		);
		return $ret['success'] ?: error("Failed to extract `%s' from `%s': `%s'", $subdir, $this->file, $ret['stderr']);
	}

	public function validateArchive(): bool {
		info('Verifying files...');
		$count = \count($this) * 100;
		$incr = $count / 100;
		$i = 0;
		fwrite(STDOUT, 'Progress [  0%]');
		try {
			foreach (new \RecursiveIteratorIterator($this) as $f) {
				if ($i && ($count % $i) === 0) {
					fwrite(STDOUT, "\033[5D");
					fwrite(STDOUT, str_pad((string)($i / $count * 100), 3, ' ', STR_PAD_LEFT) . '%]');
				}
				if (!$f->isCRCChecked()) {
					return error("CRC mismatch on `%s'", $f->getPath());
				}
				$i += $incr;
			}
		} catch (\RuntimeException $e) {
			fwrite(STDOUT, "\n");
			warn('Archive uses POSIX.2001 format. Extracting first to process');
			throw $e;
		}
		fwrite(STDOUT, " COMPLETE\n");

		return true;
	}
}

