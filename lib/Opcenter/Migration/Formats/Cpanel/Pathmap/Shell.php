<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
	 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;


	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;

	class Shell extends ConfigurationBuilder
	{
		public function parse(): bool
		{
			if (!file_exists($path = $this->getReader()->getPathname())) {
				return true;
			}
			$state = false === strpos(file_get_contents($path), 'bin/noshell');
			$this->bill->set('ssh','enabled', (int)$state);
			if (!$state) {
				// implicitly disable to prevent error during account creation unless cron set
				$this->bill->set('crontab', 'permit', 0);
				$this->bill->set('crontab', 'enabled', 0);
			}
			return true;
		}

	}

