<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
	 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;


	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\SiteConfiguration;

	class Psql extends ConfigurationBuilder
	{
		const DEPENDENCY_MAP = [
			PsqlGrantsSql::class,
			PsqlUsersSql::class
		];

		public function parse(): bool
		{
			// empty directory
			if (!$path = $this->getDirectoryEnumerator()->getPathname()) {
				return true;
			}

			// callback once account is processed
			Cardinal::register(
				[\Opcenter\Account\Import::HOOK_ID, Events::SUCCESS],
				function ($event, SiteConfiguration $p) {
					$this->stream->externalExtract(
						'/psql',
						$p->getAccountRoot() . $p->getSiteFunctionInterceptor()->user_get_home() . '/psql',
						$p->getAuthContext()->user_id,
						$p->getAuthContext()->group_id
					);

				}
			);

			return true;
		}
	}

