<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	namespace Opcenter\Migration\Formats;

	use Opcenter\Filesystem;
	use Opcenter\Migration\Contracts\ImportInterface;
	use Opcenter\Migration\DependencyMap;
	use Opcenter\Migration\Formats\Cpanel\Pathmap\Cp;
	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\Migration\FormatType;

	class Cpanel extends FormatType implements ImportInterface
	{
		use \NamespaceUtilitiesTrait;

		/**
		 * Pre-run checks
		 *
		 * @return bool
		 */
		public function prerun(): bool
		{
			return true;
		}

		/**
		 * Run migration motherload
		 *
		 * @return bool
		 */
		public function run(): bool
		{
			$stream = $this->getStream();

			// domain is resolved later
			$baseDir = $stream->getPrefix();

			if ( !($this->bill = Cp::createBill($this->migration, $stream)) ) {
				fatal('Failed to pase basic account metadata');
			}

			$maps = [];
			Filesystem::readdir($baseDir, static function ($path) use (&$maps) {
				$class = static::appendNamespace('Cpanel\\Pathmap\\' . studly_case(str_replace(['.'], '-', $path)));
				if (!class_exists($class)) {
					return;
				}
				debug('Mapping %s to %s', $path, $class);
				$maps[$class] = $class::DEPENDENCY_MAP;
				foreach ($class::DEPENDENCY_MAP as $required) {
					// unconditionally add required deps to map
					if (!isset($maps[$required])) {
						$maps[$required] = ConfigurationBuilder::DEPENDENCY_MAP;
					}
				}
				/** @var ConfigurationBuilder $c */
			});

			foreach ((new DependencyMap($maps))->order() as $class) {
				if ($class === Cp::class) {
					continue;
				}
				$c = (new $class($this->migration, $stream))->setBill($this->getBill());
				if (!$c->parse()) {
					return error("Failed parsing on `%s'", static::getBaseClassName($class));
				}
			}

			return true;
		}


		/**
		 * Convert cPanel quantity to apnscp
		 *
		 * @param $val
		 */
		public static function quantityConverter(string $val) {
			if (\in_array($val, ['unlimited', '0', 'null'], true)) {
				return null;
			}
			return \Util_Conf::inferType($val);
		}

		/**
		 * @inheritDoc
		 */
		public function valid(): bool
		{
			return false;
		}
	}