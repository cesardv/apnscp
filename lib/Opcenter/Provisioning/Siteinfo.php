<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Opcenter\Map;
	use Opcenter\Provisioning\Traits\FilesystemPopulatorTrait;
	use Opcenter\Provisioning\Traits\GroupCreationTrait;
	use Opcenter\Provisioning\Traits\MapperTrait;
	use Opcenter\Role\Group;
	use Opcenter\Role\User;
	use Opcenter\SiteConfiguration;

	/**
	 * Class Siteinfo
	 *
	 * @package Opcenter\Provisioning
	 */
	class Siteinfo
	{
		use FilesystemPopulatorTrait;
		use MapperTrait {
			mapWrap as realMapWrap;
		}
		use GroupCreationTrait;

		const DEFAULT_SHELL = '/bin/bash';
		const NOLOGIN_SHELL = '/bin/false';
		const TEMPLATE_FILES = [
			'/etc/passwd'       => ['root', 'root', 0644],
			'/etc/shadow'       => ['root', 'root', 0600],
			'/etc/group'        => ['root', 'root', 0644],
			'/etc/site_id'      => ['root', 'root', 0644],
			'/etc/HOSTNAME'     => ['root', 'root', 0644],
			'/etc/usertemplate' => [null, APNSCP_SYSTEM_USER, 0640],
			'/etc/maildroprc'   => [null, null, 0644]
		];

		const SUPPLEMENTAL_GROUPS = [
			'root'   => ['root', null],
			'utmp'   => ['utmp', null],
			'sshd'   => ['sshd', null],
			'wheel'  => ['wheel', null],
			'mail'   => ['mail', null],
			'nobody' => ['nobody', null]
		];

		const TEMPLATE_DIRECTORIES = [
			'/var/log'           => ['root', 'root', 0755],
			'/var/tmp'           => [null, null, 03777],
			'/tmp'               => [null, null, 03777],
			''                   => ['root', 'root', 0755],
			'/usr/local'         => [null, null, 0755],
			'/usr/local/bin'     => [null, null, 0755],
			'/usr/local/lib64'   => [null, null, 0755],
			'/usr/local/share'   => [null, null, 0755],
			'/usr/local/include' => [null, null, 0755],
			'/usr/local/lib'     => [null, null, 0755],
		];

		/**
		 * Create system admin account
		 *
		 * @param string $sysuser system admin name (adminXX)
		 * @param string $root    account root
		 * @return bool
		 */
		public static function createSysAdmin(string $sysuser, string $root): bool
		{
			if (0 !== strpos($root, FILESYSTEM_VIRTBASE)) {
				fatal("sys user root must begin with `%s', got `%s'", FILESYSTEM_VIRTBASE, $root);
			}
			if (!Group::bindTo('/')->create($sysuser)) {
				fatal("failed to create admin group `%s' in system context", $sysuser);
			}
			if (!User::bindTo('/')->create($sysuser, [
				'home'     => $root,
				'shell'    => static::NOLOGIN_SHELL,
				'nohome'   => true,
				'system'   => true,
				'logindef' => [
					// system users work decrementally
					'SYS_UID_MIN=' . \User_Module::MIN_UID,
					'SYS_UID_MAX=9999' // account users begin at UID 20000
				],
				'gid'      => posix_getgrnam($sysuser)['gid']
			])) {
				fatal("failed to create admin user `%s' in system context", $sysuser);
			}

			return true;
		}

		/**
		 * Create admin user on account
		 *
		 * @param string $sysuser    system admin name (adminXX)
		 * @param string $admin_user local username
		 * @param string $root       filesystem root
		 * @param string $home       local home
		 * @return bool
		 */
		public static function createAdmin(string $sysuser, string $admin_user, string $root, string $home): bool
		{
			$pwd = posix_getpwnam($sysuser);
			if (!$pwd) {
				return error("failed to lookup admin `%s' in /etc/passwd", $sysuser);
			} else if (0 !== strpos($pwd['dir'], FILESYSTEM_VIRTBASE)) {
				fatal("home `%s' for system user `%s' does not reside within `%s'",
					$pwd['home'], $sysuser, FILESYSTEM_VIRTBASE);
			}

			if (!Group::bindTo($root)->create($admin_user, [
				'gid' => $pwd['gid']
			])) {
				fatal("failed to create admin group `%s' in system context", $sysuser);
			}
			if (!User::bindTo($root)->create($admin_user, [
				'home'  => $home,
				'shell' => static::DEFAULT_SHELL,
				'gid'   => $admin_user,
				'uid'   => $pwd['uid']
			])) {
				fatal("failed to create admin user `%s' in site context", $admin_user);
			}

			static::createGroups($root);
			User::bindTo($root)->mirror('nobody');
			if (!User::bindTo($root)->create('root', [
				'nohome' => true,
				'home'   => '/dev/null',
				'shell'  => static::NOLOGIN_SHELL,
				'uid'    => 0,
				/**
				 * @XXX
				 * If USERGROUPS_ENAB is set to TRUE, as it is in /etc/login.defs
				 * then without a GID specified useradd will attempt to create the group.
				 * It's created 2 lines above, so it must be specified since gid placement
				 * is special but also we don't want admins chroot'ing, mucking around,
				 * and leaving evidence in /root/.bash_history, so replacing the
				 * shell too is necessary to hide one's evidence
				 */
				'gid'    => 0,
				'system' => true
			])) {
				fatal("failed to create `root' in account context");
			}
			// add site admin to wheel for sudo support
			(new Group($root))->addMember($admin_user, 'wheel');

			return true;
		}

		/**
		 * Change account username
		 *
		 * @param string $old
		 * @param string $new
		 * @param string $root
		 * @param string $newhome
		 * @param int    $site_id
		 * @return bool
		 */
		public static function changeAdmin(string $old, string $new, string $root, string $newhome, int $site_id): bool
		{
			if (!User::bindTo($root)->change($old, [
				'move_home' => true,
				'username'  => $new,
				'home'      => $newhome
			])) {
				return error('failed to change username');
			}

			$pdo = \PostgreSQL::pdo();
			if (!$pdo->exec(\Opcenter\Database\PostgreSQL::vendor('user')->renameAdminUser($old, $new))) {
				return error('failed to rename admin user');
			}

			if (!Users::renameUser($old, $new, $site_id)) {
				return false;
			}

			return true;
		}

		/**
		 * Delete admin from system
		 *
		 * @param string $sysuser
		 * @return bool
		 */
		public static function deleteAdmin(string $sysuser): bool
		{
			$user = User::bindTo('/');
			if ($user->exists($sysuser)) {
				$user->delete($sysuser) || warn("failed to remove system user `%s'", $sysuser);
			}
			$group = Group::bindTo('/');
			if ($group->exists($sysuser)) {
				$group->delete($sysuser) || warn("failed to remove group `%s'", $sysuser);
			}

			return true;
		}

		/**
		 * Set virtual domain hostname
		 *
		 * @param string $root filesystem root
		 * @param string $hostname
		 * @return bool
		 */
		public static function setHostname(string $root, string $hostname): bool
		{
			return (int)file_put_contents("${root}/etc/HOSTNAME", $hostname) > 0;
		}

		/**
		 * Create cron that runs nightly
		 *
		 * @param SiteConfiguration $container
		 * @return bool
		 */
		public static function createCron(SiteConfiguration $container): bool
		{
			$path = self::getCronPath($container->getSite());
			if (!(new ConfigurationWriter('siteinfo.master-cron', $container))
				->compile([
					'site'   => $container->getSite(),
					'fspath' => $container->getAccountRoot(),
					'svc'    => $container
				])->write($path)) {
				return error("failed to create cron in `%s'", $path);
			}

			return chmod($path, 0755);
		}

		private static function getCronPath(string $site): string
		{
			return "/etc/cron.daily/0virtualhosting.${site}";
		}

		/**
		 * Remove cron from /etc/cron.daily
		 *
		 * @param SiteConfiguration $container
		 * @return bool
		 */
		public static function removeCron(SiteConfiguration $container): bool
		{
			$path = static::getCronPath($container->getSite());
			if (!file_exists($path)) {
				return true;
			}

			return unlink($path);
		}

		public static function setSiteLinks(SiteConfiguration $svc): bool
		{
			foreach (static::getLinks($svc) as $link) {
				$path = FILESYSTEM_VIRTBASE . DIRECTORY_SEPARATOR . $link;
				if (!is_link($path) && !symlink($svc->getAccountRoot(), $path)) {
					warn("failed to create symlink from `%s' to `%s'", $svc->getAccountRoot(), $path);
				}
			}

			return true;
		}

		/**
		 * Get /home/virtual links
		 *
		 * @param SiteConfiguration $svc
		 * @return array
		 */
		private static function getLinks(SiteConfiguration $svc): array
		{
			return [
				$svc->getServiceValue('siteinfo', 'admin'),
				$svc->getServiceValue('siteinfo', 'domain')
			];
		}

		public static function removeSiteLinks(SiteConfiguration $svc): bool
		{
			foreach (static::getLinks($svc) as $link) {
				$path = FILESYSTEM_VIRTBASE . DIRECTORY_SEPARATOR . $link;
				if (is_link($path)) {
					unlink($path);
				}
			}

			return true;
		}

		/**
		 * Add or remove a domain
		 *
		 * @param string      $mode   map mode
		 * @param null|string $site   site or null to remove
		 * @param string      $domain domain name
		 * @return bool
		 */
		protected static function mapWrap(string $mode, ?string $site, string $domain): bool
		{
			$ret = true;
			foreach ([Map::DOMAIN_TXT_MAP, Map::DOMAIN_MAP] as $db) {
				$ret &= static::realMapWrap($mode, $site, $domain, $db);
			}

			return (bool)$ret;
		}
	}