<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Filesystem;
	use Opcenter\Http\Apache as ApacheSvc;
	use Opcenter\Http\Apache\Map;
	use Opcenter\Http\Php\Fpm;
	use Opcenter\Provisioning\Traits\FilesystemPopulatorTrait;
	use Opcenter\Provisioning\Traits\GroupCreationTrait;
	use Opcenter\Provisioning\Traits\MapperTrait;
	use Opcenter\Role\User;
	use Opcenter\Service\Validators\Apache\Webserver;
	use Opcenter\SiteConfiguration;

	/**
	 * Class Siteinfo
	 *
	 * @package Opcenter\Provisioning
	 */
	class Apache
	{
		use FilesystemPopulatorTrait {
			populateFilesystem as populateFilesystemReal;
			depopulateFilesystem as depopulateFilesystemReal;
		}
		use MapperTrait;
		use GroupCreationTrait;

		const TEMPLATE_DIRECTORIES = [
			'/var/www'        => [null, null, 0755],
			'/var/www/html'   => [null, null, 0755],
			'/var/subdomain'  => [APNSCP_SYSTEM_USER, 'root', 0711],
			'/etc/httpd/conf' => [null, 'root', 0700],
			'/var/log/httpd'  => [APACHE_USER, null, 02750],
			'/var/cache/pagespeed' => [APACHE_USER, null, 05750],
			'/tmp/sessions'   => ['root', null, 03777]
		];

		const TEMPLATE_FILES = [];

		const SUPPLEMENTAL_GROUPS = [
			\Web_Module::WEB_USERNAME => [\Web_Module::WEB_USERNAME, null],
		];

		const MAP_FILE = Webserver::MAP_FILE;
		// @var string rewrite rules that contain suspension rewrite
		const SUSPENSION_MARKER = '00-suspend';

		public static function populateFilesystem(SiteConfiguration $container, $service = null): bool
		{
			if (!static::populateFilesystemReal($container, $service)) {
				return false;
			}
			$afi = $container->getSiteFunctionInterceptor();
			if (!$afi->file_exists('~/mainwebsite_html')) {
				$afi->file_symlink('/var/www/html', '~/mainwebsite_html');
			}
			$afi->file_create_directory('~/all_domains');
			$afi->file_create_directory('~/all_subdomains');
			$placeholderPath = $container->getAuthContext()->domain_fs_path('/var/www/html/index.html');
			if (!file_exists($placeholderPath)) {
				file_put_contents($placeholderPath, (string)(new ConfigurationWriter('apache.placeholder', $container))->compile([])) &&
					Filesystem::chogp($placeholderPath, $container->getAuthContext()->user_id, $container->getAuthContext()->group_id);
			}
			return true;

		}

		/**
		 * Get list of directories to populate
		 *
		 * @return array
		 */
		protected static function getPopulatedDirectoriesList(): array
		{
			$dirs = static::TEMPLATE_DIRECTORIES;
			if (FILESYSTEM_TYPE === 'xfs') {
				/**
				 * @XXX CAP_SYS_RESOURCE allows
				 */
				$dirs['/var/log/httpd'] = [APACHE_USER, null, 0750];
			}
			return $dirs;
		}

		public static function depopulateFilesystem(SiteConfiguration $container, $service = null): bool
		{
			if (!static::depopulateFilesystemReal($container, $service)) {
				return false;
			}
			$afi = $container->getSiteFunctionInterceptor();
			foreach (['~/mainwebsite_html', '~/all_domains', '~/all_subdomains'] as $path) {
				$afi->file_delete($path, true);
			}

			return true;
		}


		/**
		 * Create new Apache user
		 *
		 * @param string   $root
		 * @param string   $sys_admin system admin user (adminXX)
		 * @param string   $user      web username
		 * @param int|null $uid       optional user id
		 * @return bool
		 */
		public static function createUser(string $root, string $sys_admin, string $user, ?int $uid): bool
		{
			$args = [
				'nohome' => true,
				'gid'    => posix_getpwnam($sys_admin)['gid'],
			];
			if ($uid) {
				$args['uid'] = posix_getpwnam($user)['uid'];
				$args['supplement'] = \Web_Module::WEB_USERNAME;
				if ($uid < \User_Module::MIN_UID) {
					$args['system'] = true;
				}
			}

			return (new User($root))->create($user, $args);
		}

		/**
		 * Create account configuration
		 *
		 * @param SiteConfiguration $container
		 * @return bool
		 */
		public static function createConfiguration(SiteConfiguration $container): bool
		{
			$ips = array_merge(Ipinfo::getAddresses($container), array_map(static function ($v) {
				return '[' . $v . ']';
			}, Ipinfo6::getAddresses($container)));
			$paths = $params = [];
			if (ApacheSvc::HTTP_NOSSL_PORT) {
				$params[] = [
					'port'     => ApacheSvc::HTTP_NOSSL_PORT,
					'ips'      => $ips,
					'proto'    => 'http',
					'template' => 'virtualhost',
				];
				$paths = [
					ApacheSvc::siteStoragePath($container->getSite())
				];
			}

			if (Ssl::enabled($container) && Ssl::installed($container) && ApacheSvc::HTTPD_SSL_PORT) {
				$params[] = [
					'port'     => ApacheSvc::HTTPD_SSL_PORT,
					'ips'      => $ips,
					'proto'    => 'https',
					'template' => 'virtualhost-ssl'
				];
				$paths[] = ApacheSvc::siteStoragePath($container->getSite()) . '.ssl';
			}
			$config = '';
			foreach ($params as $vars) {
				$vars['svc'] = $container;
				$vars['fpmConfig'] = Fpm\Configuration::bindTo($container->getAccountRoot())->
					setServiceContainer($container);
				$config .= (new ConfigurationWriter("apache/${vars['template']}", $container))
					->compile($vars);
			}
			$file = ApacheSvc::siteConfigurationPath($container->getSite());
			$oldchecksum = file_exists($file) ? md5_file($file) : null;
			if (file_put_contents($file, $config) <= 0) {
				return error('failed to create Apache configuration');
			}
			$buildconf = $oldchecksum !== md5($config);
			foreach ($paths as $path) {
				if (!is_dir($path) && !Filesystem::mkdir($path)) {
					return error("failed to create Apache configuration directory `%s'", $path);
				}
			}

			if (!file_exists($path = ApacheSvc::DOMAIN_PATH . '/' . $container->getSite())) {
				touch($path);
				Map::open($path, Map::MODE_WRITE)->sync();
			}

			if ($buildconf) {
				register_shutdown_function(static function () {
					ApacheSvc::activate();
				});
			}

			return true;

		}

		/**
		 * Remove Apache configuration
		 *
		 * @param string $site
		 * @return bool
		 */
		public static function removeConfiguration(string $site): bool
		{
			$paths = [
				ApacheSvc::siteStoragePath($site),
				ApacheSvc::siteStoragePath($site) . '.ssl'
			];
			foreach ($paths as $path) {
				if (!file_exists($path)) {
					continue;
				}
				foreach (glob("${path}/*") as $file) {
					unlink($file);
				}
				if (!Filesystem::rmdir($path)) {
					warn("failed to remove directory `%s'", $path);
				}
			}
			$files = [
				ApacheSvc::siteConfigurationPath($site),
				ApacheSvc::siteInactiveConfigurationPath($site)
			];
			foreach ($files as $file) {
				// priority linking, moving site12 to priority-site12 if
				// site10 duplicates site12 as a subdomain
				if (is_link($file) && file_exists($ref = realpath($file))) {
					\unlink($ref);
				}

				if (file_exists($file) || is_link($file)) {
					\unlink($file);
				}
			}
			foreach (glob(ApacheSvc::DOMAIN_PATH . "/${site}{,.*}", GLOB_BRACE) as $file) {
				\unlink($file);
			}
			/**
			 * @TODO move to shutdown function to allow mass delete without rebuilding http config
			 *       on each deletion?
			 */
			Cardinal::register([SiteConfiguration::HOOK_ID, Events::SUCCESS], static function () {
				ApacheSvc::activate();
			});

			return true;
		}

		/**
		 * Suspend site access
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public static function suspend(SiteConfiguration $svc): bool
		{
			return static::writeConfiguration($svc, self::SUSPENSION_MARKER, 'apache/suspend-rules');
		}

		/**
		 * Write configuration
		 *
		 * @param SiteConfiguration $svc
		 * @param string            $file
		 * @param null|string       $template template to write or null to remove
		 * @return bool
		 */
		public static function writeConfiguration(SiteConfiguration $svc, string $file, ?string $template): bool
		{
			$file = ApacheSvc::siteStoragePath($svc->getSite()) . '/' . $file;
			if (null === $template) {
				return (file_exists($file) && unlink($file)) || true;
			}
			// necessary for cgroup, which PHP-FPM depends upon, to write its config fragment
			// @TODO Apache event in Cardinal?
			if (!\is_dir($parent = \dirname($file)) && !Filesystem::mkdir($parent)) {
				return error("Failed to create directory `%s'", $parent);
			}
			$config = (new ConfigurationWriter($template, $svc))
				->compile(['svc' => $svc]);
			$md5sum = null;
			if (file_exists($file)) {
				$md5sum = md5_file($file);
			}
			if ($md5sum === md5((string)$config)) {
				return true;
			}

			if (file_put_contents($file, $config) <= 0) {
				return error("failed to create HTTP configuration ${file}");
			}
			Cardinal::register([SiteConfiguration::HOOK_ID, Events::SUCCESS], static function () {
				ApacheSvc::activate();
			});

			return true;
		}

		/**
		 * Unsuspend site access
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */

		public static function activate(SiteConfiguration $svc): bool
		{
			return static::writeConfiguration($svc, self::SUSPENSION_MARKER, null);
		}
	}