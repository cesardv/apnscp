<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Opcenter\Provisioning\Traits\FilesystemPopulatorTrait;

	/**
	 * Class Users
	 *
	 * @package Opcenter\Provisioning
	 */
	class Users
	{
		use FilesystemPopulatorTrait;

		const DEFAULT_SHELL = '/bin/bash';
		const NOLOGIN_SHELL = '/bin/false';

		public static function renameUser(string $old, string $new, int $site_id): bool
		{
			$pdo = \PostgreSQL::pdo();
			$pdo->exec(\Opcenter\Database\PostgreSQL::vendor('user')->renameUser($old, $new, $site_id));
			if ((int)$pdo->errorCode()) {
				return error("failed to rename user in db from `%s' to `%s'", $old, $new);
			}

			return true;
		}
	}