<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning\Traits;

	use Opcenter\Role\Group;

	trait GroupCreationTrait
	{

		/**
		 * Create supplemental groups for site
		 *
		 * @param string $root account root
		 * @return bool
		 */
		public static function createGroups(string $root): bool
		{
			if (!\defined('static::SUPPLEMENTAL_GROUPS')) {
				fatal('SUPPLEMENTAL_GROUPS not set in %s', static::class);
			}
			foreach (static::SUPPLEMENTAL_GROUPS as $grname => [$gid, $members]) {
				if (!\is_int($gid)) {
					$gid = posix_getgrnam($gid)['gid'] ?? fatal("failed to lookup group id for `%s'", $gid);
				}

				$grcontrol = new Group($root);
				$gwd = $grcontrol->getgrnam($grname);

				if ((null === $gwd) && !$grcontrol->create($grname, [
						'gid'       => $gid,
						'system'    => $gid < \User_Module::MIN_UID,
						'duplicate' => true
					]))
				{
					return error("failed to create group `%s'", $grname);
				}
				if (!$members) {
					continue;
				}

				foreach ((array)$members as $member) {
					if (\is_int($member)) {
						if (null === ($tmp = posix_getpwuid($member)['name'] ?? null)) {
							warn("Unknown member requested `%s' - no matching uid found", $member);
							continue;
						}
						$member = $tmp;
					}
					if ((new Group($root))->addMember($member, $grname)) {
						return error("Failed to add member `%s' to group `%s'", $member, $group);
					}
				}
			}

			return true;
		}

		/**
		 * Remove groups from root
		 *
		 * @param string $root
		 * @param bool   $force
		 * @return bool
		 */
		public static function removeGroups(string $root, bool $force = false): bool
		{
			if (!\defined('static::SUPPLEMENTAL_GROUPS')) {
				fatal('SUPPLEMENTAL_GROUPS not set in %s', static::class);
			}
			foreach (static::SUPPLEMENTAL_GROUPS as $grname => [$gid, $members]) {
				$grp = new Group($root);
				if (!$grp->exists($grname)) {
					continue;
				}
				$knownmembers = $grp->members($grname);
				$extra = $force ? [] : array_diff((array)$members, $knownmembers);
				$remove = $force ? $knownmembers : array_intersect($knownmembers, (array)$members);
				foreach ($remove as $r) {
					if (!$grp->removeMember($r, $grname)) {
						return error("failed to remove user `%s' from group `%s'", $r, $grname);
					}
				}
				if ($extra) {
					warn("extra members still part of group: %s, not removing group `%s'",
						implode(',', $extra), $grname
					);
					continue;
				}
				if (!$grp->delete($grname)) {
					return error("failed to remove group `%s'", $grname);
				}
			}

			return true;
		}
	}