<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
 */

namespace Service;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Module\Support\Webapps;
use Opcenter\Filesystem;
use Opcenter\Http\Php\Fpm;
use Opcenter\Net\IpCommon;
use Service\CaptureDevices\Contracts\CaptureService;

class BulkCapture {
	const QUEUE_KEY = 'capture-queue';
	/**
	 * @var CaptureDevices\Chromedriver
	 */
	protected $daemon;
	/**
	 * @var \Facebook\WebDriver\Remote\RemoteWebDriver
	 */
	protected $driver;
	/**
	 * @var float|string
	 */
	protected $start;

	public function __construct(CaptureService $capture)
	{
		$this->stats = [
			'ts'      => microtime(true),
			'success' => 0,
			'failure' => 0
		];

		$this->daemon = $capture;
	}

	private function instantiateOnDemand(): void
	{
		if ($this->driver) {
			return;
		}

		try {
			$this->daemon->start();
			$this->daemon->wait();
			$this->driver = $this->createDriver();
		} finally {
			if (!$this->driver) {
				$this->daemon->stop();
				$this->daemon = null;
			}
		}
	}

	public function __destruct()
	{
		if ($this->driver) {
			$this->driver->close();
		}
		$this->daemon->stop();
		$this->daemon = null;
		$now = microtime(true);
		if (0 === ($this->stats['success'] + $this->stats['failure'])) {
			return;
		}

		info(
			'%(total)d screenshots attempted. %(success)d success, %(fail)d failure. %(duration).2f seconds (%(each).1f ms each)',
			[
				'total'    => $this->stats['success'] + $this->stats['failure'],
				'success'  => $this->stats['success'],
				'fail'     => $this->stats['failure'],
				'duration' => $now - $this->stats['ts'],
				'each'     => ($now - $this->stats['ts'])/($this->stats['success'] + $this->stats['failure'])*1000
			]
		);
	}

	private function createDriver(): RemoteWebDriver {
		$capabilities = \Facebook\WebDriver\Remote\DesiredCapabilities::chrome();
		$capabilities->setCapability('chromeOptions', [
			'args' => [
				'--window-size=1366,768',
				'--no-default-browser-check',
				'--no-first-run',
				'--headless',
				'--no-sandbox',
				'--disable-automation',
				'--disable-boot-animation',
				'--disable-default-apps',
				'--disable-extensions',
				'--disable-translate',
				'--user-agent=' . escapeshellarg(PANEL_BRAND . ' ' . APNSCP_VERSION),
				'--hide-scrollbars'
			],
		]);

		return \Facebook\WebDriver\Remote\RemoteWebDriver::create(
			$this->daemon->getConnectionUri(),
			$capabilities
		);
	}

	/**
	 * Process batch up to n screenshots
	 *
	 * @param int $n
	 * @return bool
	 */
	public function batch(int $n): void
	{
		$queue = \Cache_Global::spawn();
		$batch = $queue->get(self::QUEUE_KEY);

		defer($_, static function () use (&$batch) {
			$cache = \Cache_Global::spawn();
			debug('%d entries remain in current queue', \count($batch));
			$cache->set(self::QUEUE_KEY, $batch, (int)SCREENSHOTS_TTL+1);
		});

		if (!$batch) {
			debug('Batch exhausted - collecting sites');
			$batch = $this->collectAll();
			debug('Collected %d sites', \count($batch));
			if (\count($batch)/SCREENSHOTS_BATCH > SCREENSHOTS_TTL/(CRON_RESOLUTION * 2)) {
				warn('Raise [screenshots] => batch to at least %(minbatch)d or ttl to %(minttl)d - ' .
					'current cycle %(batch)d every %(crontime)d second cron cycle may overlap screenshot TTL %(ttl)d seconds',
					[
						'minbatch' => ceil(\count($batch)/(SCREENSHOTS_TTL / (CRON_RESOLUTION * 2))),
						'minttl'   => ceil(\count($batch)/SCREENSHOTS_BATCH * (CRON_RESOLUTION * 2)),
						'batch'    => SCREENSHOTS_BATCH,
						'crontime' => CRON_RESOLUTION,
						'ttl'      => SCREENSHOTS_TTL
					]
				);
			}
		}

		$serviceRef = null;
		while ($n > 0 && $batch) {
			$item = array_shift($batch);
			$outputPath = static::getOutputPath($item['hostname'], $item['path']);
			if (file_exists($outputPath) && ($this->stats['ts'] - filemtime($outputPath) < SCREENSHOTS_TTL)) {
				continue;
			}

			$n--;
			if (!posix_getuid() && (!$serviceRef || $serviceRef->getGroup() !== $item['site'])) {
				// record PHP-FPM status to determine whether it should be shutdown after
				$serviceRef = (new Fpm\StateRestore($item['site']));
			}

			$this->snap($item['hostname'], $item['path'], $item['ip']);
		}
	}

	/**
	 * Collect all sites to batch screenshots
	 *
	 * @return array
	 */
	public function collectAll(): array
	{
		$batch = [];
		foreach (\Opcenter\Account\Enumerate::sites() as $site) {
			$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL | \Error_Reporter::E_ERROR);
			try {
				$ctx = \Auth::context(null, $site);
				$afi = \apnscpFunctionInterceptor::factory($ctx);
			} catch (\apnscpException $e) {
				continue;
			} finally {
				\Error_Reporter::exception_upgrade($oldex);
			}
			$ip = $afi->site_ip_address();
			array_push($batch, ...array_map(static function ($hostname) use ($ip, $site, $ctx) {
				if (false !== strpos($hostname, '*')) {
					$hostname = str_replace('*', $ctx->domain, $hostname);
				}
				return [
					'site'     => $site,
					'ip'       => $ip,
					'hostname' => $hostname,
					'path'     => ''
				];
			}, array_values(Webapps::getAllHostnames($ctx))));
		}

		return $batch;
	}

	/**
	 * Generate screenshot
	 *
	 * @param string      $hostname
	 * @param string      $path
	 * @param string|null $ip
	 * @return bool
	 */
	public function snap(string $hostname, string $path = '', string $ip = null, string $outputPath = null): bool
	{
		$this->instantiateOnDemand();

		// for potential Timed out ereceiving message from renderer errors
		// https://stackoverflow.com/questions/60114639
		if ($ip) {
			if (!IpCommon::valid($ip)) {
				fatal("Invalid IP `%s'", $ip);
			}

			$this->daemon->addHost($hostname, $ip);
			$this->daemon->addHost("www.${hostname}", $ip);
		}

		if (!posix_getuid()) {
			$sysid = array_get(posix_getpwnam(APNSCP_SYSTEM_USER), 'uid', 0);
			posix_seteuid($sysid);
			defer($_, static function () {
				posix_seteuid(0);
			});
		}
		$url = $hostname;
		if (false === strpos($url, ':')) {
			$url = "http://${hostname}";
		}
		if ($ip) {
			$url .= ':' . HTTPD_NOSSL_PORT;
		}
		$url .= '/' . ltrim($path, '/');
		$outputPath = $outputPath ?? static::getOutputPath($hostname, $path);

		if (!is_dir($dir = \dirname($outputPath))) {
			Filesystem::mkdir($dir, posix_geteuid(), posix_getegid(), 0755, true);
		}
		Filesystem::touch($outputPath, posix_geteuid(), posix_getegid(), 0644);

		try {
			$this->driver->get($url)->takeScreenshot($outputPath);
		} catch (\Exception $e) {
			$this->stats['failure']++;
			return error('Capture failed: %s', $e->getMessage());
		} finally {
			if (!posix_getuid()) {
				posix_seteuid(posix_getuid());
			}
		}
		$this->stats['success']++;

		return true;
	}

	/**
	 * Get output path
	 *
	 * @param string $hostname
	 * @param string $path
	 * @return string
	 */
	public static function getOutputPath(string $hostname, string $path = ''): string
	{
		return public_path(static::getOutputUrl($hostname, $path));
	}

	/**
	 * Get output URL
	 *
	 * @param string $hostname
	 * @param string $path
	 * @return string
	 */
	public static function getOutputUrl(string $hostname, string $path): string
	{
		if (false !== strpos($hostname, '*')) {
			$hostname = str_replace('*', $_SESSION['domain'], $hostname);
		}
		return '/images/screenshots/' . urlencode($hostname) . '-' . urlencode($path) . '.png';
	}
}