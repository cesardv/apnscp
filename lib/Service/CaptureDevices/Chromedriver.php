<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
 */

namespace Service\CaptureDevices;

use Opcenter\Filesystem;
use Opcenter\Net\Port;
use Opcenter\Process;
use Service\CaptureDevices\Contracts\CaptureService;
use Util\Process\Unshare;
use Util\Process\Unshared\Resources\Mount;

class Chromedriver implements CaptureService {
	private const PID_FILE = 'chromedriver.pid';
	private const BIN_PATH = '/usr/bin/chromedriver';

	/**
	 * @var self
	 */
	protected $hostPointer;

	// @var int port
	protected $port;
	public function __construct()
	{
		$this->resumeOrCreateHostMap();
		$this->checkHoldLock();
	}

	private function checkHoldLock(): void {
		for ($i = 0; $i < 5; $i++) {
			flock($this->hostPointer, LOCK_EX | LOCK_NB, $would);
			if (!$would) {
				return;
			}
			sleep(1);
		}

		throw new \RuntimeException('Lock cannot be acquired');
	}

	public function __destruct()
	{
		if (!$this->hostPointer) {
			return;
		}

		flock($this->hostPointer, LOCK_UN);

		if (file_exists($file = $this->getHostFile())) {
			unlink($file);
		}
	}

	/**
	 * Create reference to /etc/hosts unshare
	 */
	protected function resumeOrCreateHostMap() {
		if (!$this->running()) {
			$file = tempnam(TEMP_DIR, 'host');
			$this->hostPointer = fopen($file, 'w');
			chown($this->getHostFile(), $this->getChromeUser());
			return;
		}

		$pidPath = '/proc/' . $this->getPid();
		if (!file_exists($pidPath)) {
			fatal('PID %d does not exist', $this->getPid());
		}
		$mounts = file_get_contents($pidPath . '/mountinfo');
		if (!preg_match_all('!^(?<mountid>\d+) (?<pmountid>\d+) (?<major>\d+):(?<minor>\d+) (?<root>\S+) /etc/hosts !m', $mounts, $matches, PREG_SET_ORDER)) {
			fatal('Failed to locate /etc/hosts mount - service corrupted');
		}

		$matches = array_pop($matches);
		// get path from major:minor mount
		$base = \Opcenter\Filesystem\Mount::getMountPointFromDevice($matches['major'] . ':' . $matches['minor']);
		if (!$base) {
			fatal('Failed to find mount point for %d:%d', $matches['major'], $matches['minor']);
		}
		if (false !== strpos($matches['root'], '/deleted')) {
			// file has been removed, fp invalid
			$this->stop();
			return $this->resumeOrCreateHostMap();
		}
		$this->hostPointer = fopen($base . $matches['root'], 'w');
	}

	/**
	 * Get chromedriver user
	 *
	 * @return string
	 */
	protected function getChromeUser(): string
	{
		return 'nobody';
	}

	/**
	 * Get port from service
	 *
	 * @return int
	 */
	public function getPort(): int {
		if (isset($this->port)) {
			return $this->port;
		}
		if (!$this->running()) {
			// let OS assign
			return 0;
		}

		$ports = Port::fromPid($this->getPid());

		if (\count($ports) > 1) {
			debug('Multiple ports found for chromedriver: %s', implode(', ', array_column($ports, 'local_port')));
		} else if (!$ports) {
			usleep(2500);
			return $this->getPort() ?: fatal('Failed to find listener port for chromedriver!');
		}

		return $this->port = array_get(array_shift($ports), 'local_port');
	}

	public function getConnectionUri(): string
	{
		$str = SCREENSHOTS_CHROMEDRIVER;
		return substr($str, 0, strrpos($str, ':')) . ':' . $this->getPort();
	}

	public function start(): bool
	{
		if (SCREENSHOTS_ACQUISITION !== 'self') {
			fatal("Only 'self' acquisition supported");
		}

		if ($this->running()) {
			return error('Instance already running');
		}

		$unshare = new Unshare();

		$unshare->add(
			(new Mount('/etc/hosts'))->fromResource($this->hostPointer)
		);
		// minimal nsswitch.conf that works off /etc/hosts
		$unshare->add(
			(new Mount('/etc/nsswitch.conf'))->fromContent(
				implode("\n", [
					'passwd: files systemd',
					'shadow: files',
					'group: files systemd',
					'hosts: files resolve [UNAVAIL=continue] mdns4_minimal [NOTFOUND=return] dns myhostname',
					'ethers: files',
					'networks: files dns',
					'netmasks: files',
					'protocols: files',
					'rpc: files',
					'services: files',
				])
			)
		);

		$proc = new \Util_Process_Fork();
		$proc->setOption('suid', $this->getChromeUser());
		$proc->setUnshare($unshare);
		$proc->setOption('mute_stdout', true);
		$listen = SCREENSHOTS_CHROMEDRIVER;
		$port = (int)substr($listen, strrpos($listen, ':')+1);
		$ret = $proc->run('%s --port=%d', [$this->getCommandName(), $port]);
		if (!$ret['success']) {
			return error('Failed to start %(prog)s: %(err)s',
				['prog' => basename($this->getCommandName()), 'err' => $ret['stderr']]);
		}
		debug('Starting Chromedriver (PID %d)', $ret['return']);
		$this->savePid($ret['return']);
		return true;

	}

	/**
	 * Fake DNS for host
	 *
	 * @param string $host
	 * @param string $addr
	 */
	public function addHost(string $host, string $addr) {
		fwrite($this->hostPointer, "${addr} ${host}\n");
	}

	/**
	 * Get host file
	 *
	 * @return string
	 */
	public function getHostFile(): string
	{
		return stream_get_meta_data($this->hostPointer)['uri'];
	}

	public function getCommandName(): string
	{
		return static::BIN_PATH;
	}

	/**
	 * Wait for chromedriver connection
	 *
	 * @return bool
	 */
	public function wait(): bool
	{
		for ($i = 0; $i < 60; $i++) {
			$port = $this->getPort();
			if (0 === $port) {
				usleep(250000);
				continue;
			}
			debug('Discovered port %d for chromedriver PID %d', $port, $this->getPid());
			$fp = @fsockopen('localhost', $this->getPort(), $err, $null, 1);
			if (!$err) {
				return true;
			}
			usleep(250000);
		}

		return error('Failed to contact chromedriver service');
	}

	/**
	 * Stop ChromeDriver
	 *
	 * @param bool $force
	 * @return bool
	 */
	public function stop(bool $force = false): bool
	{
		dlog('Attempting to stop chromedriver PID %d', $this->getPid());
		if (!$this->running()) {
			return true;
		}
		$pid = $this->getPid();

		$ret = Process::killTree($pid, $force ? SIGKILL : SIGTERM);

		if ($force) {
			return $ret;
		}

		for ($i = 0; $i < 20; $i++) {
			dlog('Force stop chromedriver PID %d', $this->getPid());
			if (!$this->running()) {
				return true;
			}
			usleep(250000);
		}

		return Process::killTree($pid, SIGKILL);
	}

	/**
	 * ChromeDriver is running
	 *
	 * @return bool
	 */
	public function running(): bool {
		if (!($pid = $this->getPid())) {
			return false;
		}

		return Process::pidMatches($pid, 'chromedriver');
	}

	/**
	 * Get ChromeDriver PID
	 *
	 * @return int|null
	 */
	public function getPid(): ?int {
		$pidFile = run_path(self::PID_FILE);
		if (!file_exists($pidFile)) {
			return null;
		}
		$pid = (int)file_get_contents($pidFile);

		return $pid ?: null;
	}

	/**
	 * Save PID
	 *
	 * @param int $pid
	 * @return bool
	 */
	protected function savePid(int $pid): bool
	{
		$pidFile = run_path(self::PID_FILE);
		return file_put_contents($pidFile, $pid) > 0;
	}
}
