<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	if (isset($_SERVER['DEBUG'])) {
		putenv('DEBUG=' . $_SERVER['DEBUG']);
	}
	if (isset($_SERVER['VERBOSE'])) {
		putenv('VERBOSE=' . $_SERVER['VERBOSE']);
	}
	ini_set('display_errors', 'On');
	ini_set('display_startup_errors', 'On');
	ini_set('memory_limit', '128M');
	define('IS_CLI', 1);
	define('INCLUDE_PATH', realpath(dirname($_SERVER['SCRIPT_FILENAME']) . '/../'));
	include(INCLUDE_PATH . '/lib/apnscpcore.php');
	ListenerServiceCommon::init();