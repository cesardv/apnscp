<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Module\Support;

	use Module_Skeleton;
	use Opcenter;
	use Opcenter\Filesystem;
	use Opcenter\Http\Apache\Map;
	use Opcenter\Provisioning\ConfigurationWriter;
	use User_Module;

	/**
	 * Support structure for auth module
	 */
	abstract class Aliases extends Module_Skeleton implements \Opcenter\Contracts\Hookable
	{
		const CONFIG_DB_DIR = Opcenter\Http\Apache::DOMAIN_PATH;
		const BYPASS_FILE = '/etc/virtualhosting/dnsbypass';
		const CACHE_KEY = 'aliases';

		/**
		 * Add domain to Apache map
		 *
		 * @param string $domain
		 * @param string $path
		 * @return bool
		 */
		protected function addMap(string $domain, string $path): bool
		{
			$map = Map::fromSiteDomainMap($this->domain_info_path(), $this->site,
				Map::MODE_WRITE);
			$domain = strtolower($domain);
			if (isset($map[$domain])) {
				warn("overwriting domain `%s' with new path `%s'", $domain, $path);
			}
			if (0 !== strpos($path, FILESYSTEM_VIRTBASE)) {
				$path = $this->domain_fs_path($path);
			}
			$map[$domain] = $path;
			\Cache_Account::spawn($this->getAuthContext())->del(self::CACHE_KEY);

			return true;
		}


		/**
		 * Remove domain from map
		 *
		 * @param string $domain
		 * @return bool
		 */
		protected function removeMap(string $domain): bool
		{
			$map = Map::fromSiteDomainMap($this->domain_info_path(), $this->site,
				Map::MODE_WRITE);
			$domain = strtolower($domain);
			unset($map[$domain]);
			\Cache_Account::spawn($this->getAuthContext())->del(self::CACHE_KEY);

			return true;
		}

		protected function cache(array $data): void
		{
			$cache = \Cache_Account::spawn($this->getAuthContext());
			$cache->set(static::CACHE_KEY, $data);
		}

		/**
		 * Return a transformed map of domains
		 *
		 * @return array
		 */
		protected function transformMap(): array
		{
			$map = Map::fromSiteDomainMap($this->domain_info_path(), $this->site,
				Map::MODE_READ);
			$domains = [];
			if (!$map) {
				return $domains;
			}

			foreach ($map as $domain => $path) {
				$domains[$domain] = $this->jailDomain($path);
			}

			unset($domains[$this->domain]);
			$this->cache($domains);

			return $domains;
		}

		private function jailDomain(string $path): string {
			$fst = $this->domain_fs_path();
			$len = \strlen($fst);
			return rtrim(0 === strpos($path, $fst) ? substr($path, $len) : $path, '/');
		}

		/**
		 * Remove a domain from DNS bypass check
		 *
		 * @param  string $domain
		 * @return bool
		 */
		protected function removeBypass(string $domain): bool
		{
			if (!file_exists(self::BYPASS_FILE)) {
				return true;
			}

			$recs = file(self::BYPASS_FILE, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

			if (false === ($line = array_search($domain, $recs, true))) {
				return -1;
			}

			unset($recs[$line]);
			if (\count($recs) < 1) {
				// save a few cpu cycles
				unlink(self::BYPASS_FILE);
			} else {
				file_put_contents(self::BYPASS_FILE, implode("\n", $recs));
			}

			return true;
		}

		protected function createDocumentRoot(string $path)
		{
			// sync up file cache
			$this->file_purge();
			if (file_exists($this->domain_fs_path() . '/' . $path)) {
				return true;
			}
			$gid = $this->group_id;
			if (preg_match('!^/home/([^/]+)/!', $path, $user)) {
				$user = $user[1];
				$home = '/home/' . $user;
				$users = $this->user_get_users();
				$uid = $users[$user]['uid'];
				if ($uid < User_Module::MIN_UID) {
					return error('%s: user unknown in system', $user);
				}
				$stat = $this->file_stat($home);
				$perms = decoct($stat['permissions'] | ($this->php_jailed() ? 0010 : 0001));
				if ($stat && !$this->file_chmod($home, $perms)) {
					warn("Failed to open permissions on `%s' to allow HTTP access");
				}
			} else {
				$uid = $this->user_id;
			}
			$fullpath = $this->domain_fs_path() . '/' . $path;
			if (!mkdir($fullpath)) {
				return error("z'huh!? failed to create doc root?");
			}
			chown($fullpath, $uid);
			chgrp($fullpath, $gid);
			$index = $fullpath . '/index.html';
			return file_put_contents($index, (string)(new ConfigurationWriter('apache.placeholder',
				\Opcenter\SiteConfiguration::import($this->getAuthContext())))->compile([])) &&
				Filesystem::chogp($index, $uid, $gid, 0644);
		}

		/**
		 * Check to bypass addon domain DNS validation test
		 *
		 * @param string $domain
		 * @return bool domain is bypassed
		 */
		protected function isBypass(string $domain): bool
		{
			if (\defined('DOMAINS_DNS_CHECK') && !\constant('DOMAINS_DNS_CHECK')) {
				return true;
			}
			if (!file_exists(self::BYPASS_FILE)) {
				return false;
			}

			$recs = file(self::BYPASS_FILE, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

			return (false !== ($line = array_search($domain, $recs, true)));
		}
	}
