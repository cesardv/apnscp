<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	declare(strict_types=1);


	namespace Module\Support\Webapps;

	use Module\Support\Webapps;
	use Module\Support\Webapps\App\Loader;

	class Finder
	{
		use \FilesystemPathTrait;
		use \apnscpFunctionInterceptorTrait;
		use \ContextableTrait;
		protected $inodeVisited = [];

		/**
		 * Finder constructor.
		 *
		 * @param string|\Auth_Info_User $context
		 */
		public function __construct($context)
		{
			if (\is_string($context)) {
				$this->setContext(\Auth::context(null, $context));
			} else if ($context instanceof \Auth_Info_User) {
				$this->setContext($context);
			} else {
				fatal("unknown context parameter `%s'", $context);
			}
			$this->site = $this->getAuthContext()->site;
			$this->setApnscpFunctionInterceptor(\apnscpFunctionInterceptor::factory($this->getAuthContext()));
		}

		public static function filterActive(array $appmeta): bool
		{
			if (!empty($appmeta['failed'])) {
				return false;
			}

			if (empty($appmeta['version'])) {
				// @XXX no version detected - possible bug?
				return false;
			}

			/**
			 * move autoupdate to options.autoupdate
			 */
			return (bool)array_get($appmeta, 'options.autoupdate', array_get($appmeta, 'autoupdate', true));
		}

		public static function filterNone(array $appmeta): bool
		{
			return true;
		}

		/**
		 * Enumerate sites for webapps
		 *
		 * @param mixed $sites single or array of sites to examine
		 * @return array
		 */
		public static function find($sites = null): array
		{
			$apps = static::invoker($sites, 'searchSite');
			return (!$apps ?: \count($apps) > 1) ? $apps : array_pop($apps);
		}

		public static function prune($sites = null): void
		{
			static::invoker($sites, 'pruneWebapps');
		}

		/**
		 * Invocation wrapper
		 *
		 * @param $sites
		 * @param $method
		 * @return array
		 */
		private static function invoker($sites, $method) {
			$sites = (array)$sites;
			if (!$sites) {
				$sites = \Opcenter\Account\Enumerate::active();
			}
			$results = [];
			foreach ($sites as $site) {
				\Error_Reporter::print_buffer(\Error_Reporter::flush_buffer());
				$finder = new static($site);
				if ($apps = $finder->{$method}()) {
					$apps[$site] = $apps;
				}
			}
			return $results;
		}

		/**
		 * Search site for undiscovered apps
		 *
		 * @return array
		 */
		public function searchSite(): array
		{
			info("Searching on `%s' (%s)",
				$this->getAuthContext()->site,
				$this->getAuthContext()->domain
			);
			$apps = [];
			$known = array_filter($this->getActiveApplications(), static function ($app) {
				return isset($app['version']) && !empty($app['version']) && isset($app['hostname']);
			});
			$docroots = array_merge(array_flip($this->web_list_domains()), array_flip($this->web_list_subdomains()));
			$search = array_diff_key($docroots, $known);

			foreach ($search as $docroot => $hostname) {
				if (!$docroot) {
					continue;
				}
				info("Searching docroot `%s' (%s) for webapps", $docroot, $hostname);
				$fspath = $this->domain_shadow_path() . $docroot;
				if (!file_exists($fspath)) {
					continue;
				}
				$found = '';
				if ($app = $this->directoryRecursor($fspath, $found)) {
					$localpath = !is_link($found) ? $this->file_unmake_shadow_path($found) : $this->file_unmake_path($found);
					$path = (string)substr($localpath, \strlen($this->web_get_docroot($hostname)));
					// location symlinks to other known location
					// e.g. testing.domain.com (/var/www/testing) -> domain.com (/var/www/html)
					$pathNormalized = $this->web_normalize_path($hostname, $path);
					if (isset($known[$pathNormalized])) {
						continue;
					}
					$known[$pathNormalized] = 1;
					$loader = Loader::fromHostname($app, $hostname, $path, $this->getAuthContext());
					if (!$loader->getDocumentMetaPath()) {
						warn("Bad or undetectable docroot in `%s/%s' (%s)", $hostname, ltrim($path, '/'),
							$this->getAuthContext()->site);
						continue;
					}

					$this->webapp_discover($hostname, $path);
					$apps[$pathNormalized] = $app;
				}
			}

			return $apps;
		}

		/**
		 * Prune entries from list
		 */
		public function pruneWebapps(): void {
			info("Searching to prune entries from `%s' (%s)",
				$this->getAuthContext()->site,
				$this->getAuthContext()->domain
			);
			$prune = function (string $reason, string $docroot) {
				info("Pruned `%s': %s", $docroot, $reason);
				MetaManager::factory($this->getAuthContext())->forget($docroot);
			};
			foreach ($this->getAllApplicationRoots() as $docroot => $app) {
				$reason = null;
				if (!isset($app['hostname'])) {
					$reason = 'Missing hostname';
				} else if (!Finder::rootIsCurrent($docroot, $app['hostname'], $this->getApnscpFunctionInterceptor())) {
					$reason = 'Docroot different from physical directory';
				} else if (!file_exists($this->domain_fs_path($docroot))) {
					$reason = 'Document root missing';
				} else if (empty($app['version'])) {
					$reason = 'No version detected';
				}

				if ($reason) {
					$prune($reason, $docroot);
				}
			}
		}

		/**
		 * Recurse directory searching for app
		 *
		 * @param string $path  base path to search
		 * @param string $found path app is found
		 * @return string discovered app name
		 */
		protected function directoryRecursor(string $path, ?string &$found): ?string
		{
			if (!is_dir($path)) {
				return null;
			}
			$jailpath = $this->file_unmake_shadow_path($path);
			foreach (Loader::getKnownApps() as $app) {
				if (Loader::isApp($jailpath, $app, $this->getAuthContext())) {
					return $app;
				}
			}
			$app = null;
			$dh = opendir($path);
			while (false !== ($file = readdir($dh))) {
				if ($file === '..' || $file === '.') {
					continue;
				}
				if (!is_dir($path . DIRECTORY_SEPARATOR . $file)) {
					continue;
				}
				$inode = fileinode($path . DIRECTORY_SEPARATOR . $file);
				if (isset($this->inodeVisited[$inode])) {
					continue;
				}
				$this->inodeVisited[$inode] = 1;

				if ($app = $this->directoryRecursor($path . DIRECTORY_SEPARATOR . $file, $found)) {
					$found = $path . DIRECTORY_SEPARATOR . $file;
					break;
				}
			}
			closedir($dh);

			return $app;
		}

		/**
		 * Docroot matches configured hostname docroot
		 *
		 * @param string                     $docroot
		 * @param string                     $hostname
		 * @param \apnscpFunctionInterceptor $afi
		 * @return bool
		 */
		public static function rootIsCurrent(string $docroot, string $hostname, \apnscpFunctionInterceptor $afi): bool
		{
			if (!$afi->web_subdomain_exists($hostname) && !$afi->aliases_domain_exists($hostname)) {
				return false;
			}
			$testroot = $afi->web_get_docroot($hostname);
			if ($testroot === null) {
				return false;
			}

			return 1 === preg_match('!^' . preg_quote($testroot, '!') . '(/|$)!',
					$docroot) && $afi->file_exists($testroot);
		}

		/**
		 * Get all applications stored in webapps
		 *
		 * @param \Closure|null $filter
		 * @return array
		 */
		public function getAllApplicationRoots(\Closure $filter = null): array
		{
			$prefs = \Preferences::factory($this->getAuthContext());
			$apps = (array)array_get($prefs, Webapps::APPLICATION_PREF_KEY, []);
			array_walk($apps, static function (&$v) {
				if (isset($v['options']) && $v['options'] instanceof \stdClass) {
					// *sigh*
					$v['options'] = (array)$v['options'];
				}

			});

			return array_filter($apps, static function (&$app) use ($filter) {
				if (!$filter) {
					return true;
				}

				return $filter($app);
			});
		}

		/**
		 * Get docroots with a web app present
		 *
		 * @param \Closure|null $filter
		 * @return array
		 */
		public function getActiveApplications(\Closure $filter = null): array
		{
			return $this->getAllApplicationRoots(static function($app) use ($filter) {
				if (!isset($app['type'])) {
					return false;
				}
				if (!$filter) {
					return true;
				}

				return $filter($app);
			});
		}
	}
