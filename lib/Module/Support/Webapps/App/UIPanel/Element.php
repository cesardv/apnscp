<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, June 2020
 */

namespace Module\Support\Webapps\App\UIPanel;

use Illuminate\Contracts\Support\Arrayable;
use Module\Support\Webapps\App\Loader;
use Module\Support\Webapps\App\Type\Unknown\Handler as Unknown;
use Module\Support\Webapps\App\UIPanel;
use Module\Support\Webapps\MetaManager;
use Service\BulkCapture;

/**
 * Class Element
 *
 * @package Module\Support\Webapps\App\UIPanel
 * @mixin Unknown
 */
class Element implements Arrayable {
	use \ContextableTrait;
	use \apnscpFunctionInterceptorTrait;

	protected $hostname;
	/**
	 * @var string
	 */
	protected $path;
	/**
	 * @var MetaManager\Meta
	 */
	protected $meta;
	/**
	 * @var bool|string
	 */
	protected $documentRoot;
	/**
	 * @var string
	 */
	protected $classification = 'subdomain';

	/**
	 * @var Unknown
	 */
	protected $instance;

	/**
	 * Element constructor.
	 *
	 * @param UIPanel     $panel
	 * @param string      $hostname
	 * @param string|null $path
	 */

	protected function __construct(UIPanel $panel, string $hostname, ?string $path = '')
	{
		$this->hostname = $hostname;
		$this->path = ltrim((string)$path, '/');

		$this->documentRoot = \Error_Reporter::limit_emit(\Error_Reporter::E_WARNING, function () use ($hostname, $path) {
			// if a docroot cannot be resolved it falls through to MAIN_DOC_ROOT
			return $this->getApnscpFunctionInterceptor()->web_get_docroot($hostname, $path) ?: \Web_Module::MAIN_DOC_ROOT;
		});

		$this->meta = $panel->getMeta()->get($this->documentRoot);
		$this->classification = $this->getApnscpFunctionInterceptor()->web_domain_exists($hostname) ?
			'domain' : 'subdomain';
	}

	public function __call($function, $args = [])
	{
		if (!isset($this->instance)) {
			$this->instance = Loader::fromDocroot($this->getApplicationType(), $this->getDocumentRoot());
		}

		return $this->instance->$function(...$args);
	}

	public function isSubdomain(): bool
	{
		return $this->classification === 'subdomain';
	}

	public function getDocumentRoot(): string
	{
		return $this->documentRoot;
	}

	public function getApplicationType(): ?string
	{
		return $this->meta['type'] ?? null;
	}

	public function getHostname(bool $showWildcard = false): string
	{
		$hostname = $this->hostname;
		if ($showWildcard && false === strpos($hostname, '.')) {
			$hostname .= '.*';
		}

		return $hostname;
	}

	public function getMeta(string $key, $default = null)
	{
		return array_get($this->meta, $key, $default);
	}

	public function getLocation(): string
	{
		$hostname = $this->getHostname();
		if (false === strpos($hostname, '.')) {
			$hostname .= '.*';
		}
		if ($path = $this->getPath()) {
			$hostname .= "/${path}";
		}

		return $hostname;
	}

	public function getUrl(): string
	{
		$uri = 'http';
		if (array_get($this->meta->getOptions(),'ssl')) {
			$uri .= 's';
		}

		return "${uri}://" . str_replace(
			'.*',
			'.' . $this->getAuthContext()->domain,
			$this->getLocation()
		);
	}

	/**
	 * Get path component
	 *
	 * @return string
	 */
	public function getPath(): string
	{
		return $this->path;
	}

	/**
	 * Get link to management in panel
	 *
	 * @return string
	 */
	public function addonTypeMangementLink(): string
	{
		$te = \Template_Engine::init();
		$isSubdomain = $this->isSubdomain();
		$app = $te->getApplicationFromId($isSubdomain ? 'subdomains' : 'domainmanager');
		$params = $isSubdomain ? [
			'mode'      => 'list',
			'subdomain' => $this->getHostname()
		] : [
			'mode'   => 'view',
			'domain' => $this->getHostname()
		];

		return \HTML_Kit::new_page_url_params($app->getLink(), $params);
	}

	public function toArray(): array {
		return [
			'hostname' => $this->getHostname(),
			'path'     => $this->getPath()
		];
	}

	/**
	 * App has generated screenshot
	 *
	 * @return bool
	 */
	public function hasScreenshot(): bool
	{
		$url = \Service\BulkCapture::getOutputUrl($this->getHostname(true), $this->getPath());
		$path = public_path($url);

		return file_exists($path) && filesize($path) > 0;
	}

	/**
	 * Get screenshot path
	 *
	 * @return string
	 */
	public function getScreenshot(): string
	{
		if ($this->pendingScreenshot()) {
			return '/images/screenshot-placeholder.svg';
		}

		return \Service\BulkCapture::getOutputUrl($this->getHostname(true), $this->getPath());
	}

	public function pendingScreenshot(): bool
	{
		$url = \Service\BulkCapture::getOutputUrl($this->getHostname(true), $this->getPath());
		return !file_exists(public_path($url));
	}
	/**
	 * Remove cached screenshot
	 */
	public function cleanScreenshot(): void
	{
		$screenshot = BulkCapture::getOutputPath(
			str_replace('*', $this->getAuthContext()->domain, $this->getHostname(true)),
			$this->getPath()
		);
		if (file_exists($screenshot)) {
			unlink($screenshot);
		}
	}

	/**
	 * Freshen cached app meta
	 *
	 * @param bool $screenshot generate new screenshot
	 */
	public function freshen(bool $screenshot = true): void
	{
		UIPanel::instantiateContexted($this->getAuthContext())->freshen($this->getHostname(), $this->getPath(), $screenshot);
	}

	public function displayIcon(): string
	{
		if (null === $this->instance) {
			$this->__call('hasFortification');
		}
		return (string)$this->instance;
	}
}