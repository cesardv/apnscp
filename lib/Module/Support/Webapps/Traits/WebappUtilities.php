<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2021
 */

/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2021
 */

namespace Module\Support\Webapps\Traits;

use Opcenter\System\Memory;

trait WebappUtilities {
	use \ContextableTrait;
	use \apnscpFunctionInterceptorTrait;
	use \AccountInfoTrait;

	/**
	 * Get non-system user docroot ownership
	 *
	 * @param string $docroot
	 * @return string user or active username if system user
	 */
	protected function getDocrootUser(string $docroot): string
	{
		if (!($this->getAuthContext()->level & PRIVILEGE_SITE)) {
			return $this->getAuthContext()->username;
		}
		$stat = $this->file_stat($docroot);
		if (!$stat) {
			return $this->getAuthContext()->username;
		}
		// don't change if system user
		if ($stat['uid'] < \apnscpFunctionInterceptor::get_autoload_class_from_module('user')::MIN_UID) {
			return $this->getAuthContext()->username;
		}

		if (!($username = $this->user_get_username_from_uid($stat['uid']))) {
			return $this->getAuthContext()->username;
		}

		return $username;
	}

	/**
	 * Create an afi instance based on directory ownership
	 *
	 * @param string               $docroot
	 * @param \Auth_Info_User|null $context context reference
	 * @return \apnscpFunctionInterceptor
	 */
	protected function getApnscpFunctionInterceptorFromDocroot(
		string $docroot,
		?\Auth_Info_User &$context = null
	): \apnscpFunctionInterceptor {
		$user = $this->getDocrootUser($docroot);
		if ($user === $this->username) {
			$context = $this->getAuthContext();

			return $this->getApnscpFunctionInterceptor();
		}
		$context = \Auth::context($user, $this->site);

		return \apnscpFunctionInterceptor::factory($context);
	}

	/**
	 * Account has sufficient memory in MB
	 *
	 * @param int $memory    memory in MB
	 * @param int $available detected memory available
	 * @return bool
	 */
	protected function hasMemoryAllowance(int $memory, int &$available = null): bool
	{
		return ($available = $this->getMaximalMemory()) >= $memory;
	}

	/**
	 * Maximum memory permitted in MB
	 *
	 * @return int
	 */
	protected function getMaximalMemory(): int
	{
		$memMax = (int)(Memory::stats()['memtotal'] / 1024);

		return $this->cgroup_enabled() ? $this->getConfig('cgroup', 'memory') ?? $memMax :
			$memMax;
	}

	/**
	 * Account has sufficient storage in MB
	 *
	 * @param int $storage   storage in MB
	 * @param int $available detected storage in MB
	 * @return bool
	 */
	protected function hasStorageAllowance(int $storage, int &$available = null): bool
	{
		$quota = $this->site_get_account_quota();
		if (!$this->getConfig('diskquota', 'enabled')) {
			return true;
		}

		$available = (int)(($quota['qhard'] - $quota['qused']) / 1024);

		return $available >= $storage;
	}

	/**
	 * Kill processes running under location
	 *
	 * @param string $hostname
	 * @param string $path
	 * @return int
	 */
	protected function kill(string $hostname, string $path = ''): int
	{
		$approot = $this->getAppRoot($hostname, $path);
		$procs = $this->pman_get_processes();
		$count = 0;
		if ($this->permission_level & PRIVILEGE_SITE | PRIVILEGE_USER) {
			// cwd is /home/virtual/siteXX/fst
			$approot = $this->domain_fs_path() . $approot;
		}
		$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_ERROR);
		foreach ($procs as $pid => $data) {
			if (0 !== strpos($data['cwd'], $approot)) {
				continue;
			}
			try {
				$this->pman_kill($pid);
				$count++;
			} catch (\apnscpException $e) {
				warn($e->getMessage());
			}
		}
		\Error_Reporter::exception_upgrade($oldex);

		return $count;
	}

}
