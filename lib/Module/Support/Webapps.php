<?php

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Module\Support;

	use File_Module;
	use Module\Support\Webapps\App\Loader;
	use Module\Support\Webapps\App\Reconfigurator;
	use Module\Support\Webapps\App\Type\Adhoc\Manifest;
	use Module\Support\Webapps\App\Type\Unknown\Handler as Unknown;
	use Module\Support\Webapps\App\UIPanel;
	use Module\Support\Webapps\Contracts\Webapp;
	use Module\Support\Webapps\Git;
	use Module\Support\Webapps\MetaManager;
	use Module\Support\Webapps\MetaManager\Meta;
	use Module\Support\Webapps\Traits\WebappUtilities;
	use Opcenter\Blacklist;
	use Opcenter\Filesystem;
	use Opcenter\Versioning;
	use User_Module;
	use Web_Module;
	use Regex;

	/**
	 * Class Webapps
	 *
	 * @todo
	 *  - Improved handling of symlinked document roots
	 *      Divided into getDocumentRoot/normalizeDocumentRoot()
	 * 		for web path + real path. Confusing & needs rework.
	 *
	 *      When installing to symlink'd docroots, metamanager uses referent
	 *      path for meta storage. Streamline this.
	 *
	 * @package Module\Support
	 */
	abstract class Webapps extends \Module_Skeleton implements Webapps\Contracts\Webapp
	{
		use \NamespaceUtilitiesTrait;
		use WebappUtilities {
			instantiateContexted as private instantiateContextedReal;
			setContext as private setContextReal;
		}

		const APPLICATION_PREF_KEY = 'webapps.paths';
		const APP_NAME = 'undefined';
		const DEFAULT_VERSION_LOCK = 'none';
		const BANNED_DIRECTIVES = [
			'FollowSymlinks',
			'Includes',
			'All'
		];

		const LEARNING_DURATION = 60;

		/**
		 * List of modes and files
		 *
		 * @var array
		 */
		protected $aclList = [];

		protected static $registeredProviders = [];

		public $exportedFunctions = [
			'*'            => PRIVILEGE_SITE | PRIVILEGE_USER,
			'get_versions' => PRIVILEGE_ALL,
			'is_current'   => PRIVILEGE_ALL,
			'next_version' => PRIVILEGE_ALL
		];

		public function __construct()
		{
			parent::__construct();
			if (static::blacklisted($this->getAppName())) {
				$this->exportedFunctions = [
					'*' => PRIVILEGE_NONE
				];
			}
		}

		public static function instantiateContexted(
			\Auth_Info_User $context,
			array $constructorArgs = []
		): \Module_Skeleton {
			return self::instantiateContextedReal($context, $constructorArgs);
		}

		public function setContext(\Auth_Info_User $context): \Module_Skeleton
		{
			return $this->setContextReal($context);
		}

		/**
		 * Merge interface methods
		 *
		 * @return array
		 */
		public function getExportedFunctions(): array
		{
			$base = array_column((new \ReflectionClass(Webapp::class))->getMethods(\ReflectionMethod::IS_PUBLIC), 'name');
			return $this->exportedFunctions + array_fill_keys($base, PRIVILEGE_SITE|PRIVILEGE_USER);
		}


		/**
		 * Get webapp name
		 *
		 * @return null|string
		 */
		protected function getAppName(): ?string
		{
			if (static::class === self::class) {
				return null;
			}
			$name = strtolower(static::getBaseClassName());
			$rfxn = new \ReflectionClass($this);
			if ($rfxn->isAnonymous()) {
				// module has callback
				$name = strtolower(static::getBaseClassName($rfxn->getParentClass()->getName()));
			}
			return substr($name, 0, strpos($name, '_'));
		}

		/**
		 * Vendor blocked app
		 *
		 * @param string $app app name
		 * @return bool
		 */
		public static function blacklisted(string $app): bool
		{
			static $blacklist;

			if (null === $blacklist) {
				$blacklist = (new Blacklist(WEBAPPS_BLACKLIST))->filter(static::allApps());
			}

			return !\in_array($app, $blacklist, true);
		}


		/**
		 * All apps known to panel
		 *
		 * Excludes blacklisted apps. @see knownApps
		 *
		 * @return array
		 */
		public static function allApps(): array
		{
			return array_filter(array_map(static function ($app) {
				return strtolower($app);

			}, Filesystem::readdir(__DIR__ . DIRECTORY_SEPARATOR . 'Webapps' . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'Type')))
				+ append_config(array_keys(self::$registeredProviders));
		}

		/**
		 * List known apps, factoring in exclusions
		 *
		 * @return array
		 */
		public static function knownApps(): array
		{
			/**
			 * Localize apps per account per featureset in the future?
			 */
			$key = 'webapps.applist';
			$cache = \Cache_Global::spawn();
			$apps = $cache->get($key);
			if (!is_debug() && false !== $apps) {
				return $apps;
			}
			$apps = array_filter(static::allApps(),
				static function ($app) {
					if (static::blacklisted($app)) {
						return false;
					}

					$appclass = self::handlerFromApplication($app);
					$class = (new \ReflectionClass($appclass))->newInstanceWithoutConstructor();
					return $class->display();
			});
			asort($apps);
			$cache->set($key, $apps, 300);

			return $apps;
		}

		/**
		 * Restrict write-access by the app
		 *
		 * @param string $hostname
		 * @param string $path
		 * @param string $mode app-specific mode or magic mode: ['learn', 'write', 'reset']
		 * @param array  $args
		 * @return bool
		 */
		public function fortify(string $hostname, string $path = '', string $mode = 'max', $args = []): bool
		{
			$app = Loader::fromHostname(null, $hostname, $path, $this->getAuthContext());
			if (\in_array($mode, ['learn', 'write', 'reset'], true)) {
				$ret = false;
				switch ($mode) {
					case 'reset':
						$ret = $this->file_reset_path($app->getDocumentMetaPath(),
							array_get($app->getOptions(), 'user', ''), 644,
							755);
						break;
					case 'learn':
						$docroot = $app->getAppRoot();
						$owner = array_get($app->getOptions(), 'user', $this->username);
						if (!$this->file_set_acls($docroot, [
							$app->getWebUser() => 'rwx',
							$owner             => 'drwx',
						], [\File_Module::ACL_MODE_RECURSIVE])) {
							return error('failed to open permissions');
						}

						$limit = (int)($args[0] ?? static::LEARNING_DURATION);
						if ($limit < 1 || $limit > 60 * 48) {
							$limit = 10;
						}

						$checkpoint = $this->watch_checkpoint($docroot);
						if (!$checkpoint) {
							return error('failed to enable learning mode');
						}

						info('Server in learning mode for the next %(duration)d minutes. Use the web site as you normally would. ' .
							'A report will be sent to %(email)s once learning concludes.',
							['duration' => $limit, 'email' => $this->common_get_admin_email()]
						);

						$ret = $this->pman_schedule_api_cmd(
							'watch_batch',
							[$docroot, $checkpoint],
							$limit . ' minutes'
						);

						break;
					case 'write':
						$approot = $app->getAppRoot();
						$limit = (int)($args[0] ?? 10);
						if ($limit < 1 || $limit > 60 * 48) {
							$limit = 10;
						}

						if ($this->file_set_acls($approot, [$app->getWebUser() => 'rwx'],
							[\File_Module::ACL_MODE_RECURSIVE])) {
							info('web server may write to any files under %(approot)s for the next %(limit)d minutes',
								compact('approot', 'limit')
							);
						}
						// make sure everything gets chown'd
						$cmdbatch = array(
							array('file_chown', array($approot, $this->username, true)),
						);

						if ($app->hasFortification()) {
							// we know how permissions should be setup for this app, change
							// ownership to fix things
							$module = $app->getClassMapping();
							$instance = \apnscpFunctionInterceptor::get_class_from_module($module);
							if (method_exists($instance, 'fortify')) {
								$func = $module . '_fortify';
								$root = $this->web_get_docroot($app->getHostname());
								$path = (string)$app->getAppRoot();
								if ($root === $path) {
									$pathcomponent = '';
								} else {
									$pathcomponent = substr($approot, \strlen($path));
								}
								$cmdbatch[] = array(
									$func,
									array($hostname, $pathcomponent, $app::DEFAULT_FORTIFICATION)
								);
								info('Fortification %(mode)s will be enabled in %(time)d minutes',
									['mode' => $app::DEFAULT_FORTIFICATION, 'time' => $limit]);
							} else {
								// this really shouldn't happen unless the web app class is misconfigured
								warn('A known app without fortification support resides here - you will be ' .
									'responsible for opening up permissions to allow write-access ' .
									'to whatever folders are necessary in %d minutes once Write Mode is ' .
									'disabled', $limit);
							}
						} else {
							$id1 = $this->watch_checkpoint($approot);
							$cmdbatch[] = array(
								'watch_batch',
								array($approot, $id1, 'lock')
							);
							warn('an unknown app resides here - files that have changed within the next %d ' .
								'minutes will be converted to your account. Double-check that all permissions are intact ' .
								'after %d minutes have elapsed and Write Mode is disabled', $limit, $limit);

						}

						$ret = $this->pman_schedule_api_cmd($cmdbatch, $limit . ' minutes');

						break;

				}

				if ($ret) {
					$app->setOption('fortify', $mode);
				}

				return $ret;
			}
			$approot = $this->getAppRoot($hostname, $path);
			if (static::class !== \a23r::get_class_from_module('webapp') && (!$approot || !$this->valid($hostname, $path))) {
				return error("path `%(path)s' is not a valid %(app)s install", ['path' => $approot, 'app' => static::APP_NAME]);
			}

			if (null === $this->getACLFiles($mode, $app->getDocumentRoot())) {
				return error("unknown mode `%s'", $mode);
			}
			$username = $this->getDocrootUser($approot);
			// clear everything out
			$this->file_set_acls($approot, null, array(File_Module::ACL_MODE_RECURSIVE));
			$files = $this->mapFilesFromList($this->getACLFiles($mode, $app->getDocumentRoot()), $approot);
			$webuser = $this->web_get_user($hostname, $path);
			$users = array(
				array($webuser => 'drwx'),
				array($webuser => 'rwx'),
				array($username => 'rwx'),
				array($username => 'drwx'),
			);
			$flags = array(
				File_Module::ACL_MODE_RECURSIVE => true,
			);

			// @TODO clobber ownership?
			if (!$this->file_set_acls($files, $users, $flags)) {
				return warn("fortification failed on `%s/%s'", $hostname, $path);
			}

			$app->setOption('fortify', $mode);

			return true;
		}

		/**
		 * Convert hostname/path into application root following symlinks
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return bool|string
		 */
		protected function getAppRoot(string $hostname, string $path = ''): ?string
		{
			if (!($approot = $this->normalizeDocumentRoot($hostname, $path))) {
				return null;
			}
			if (!$stat = $this->file_stat($approot)) {
				// directory doesn't exist yet
				return $approot;
			}

			if ($stat['link']) {
				$approot = $stat['referent'];
			}

			return $approot ?: null;
		}

		/**
		 * Get module symbol
		 *
		 * @return string
		 */
		protected function getInternalName(): string
		{
			return strtolower(static::APP_NAME);
		}

		/**
		 * Create docroot map of files
		 *
		 * @param array  $files
		 * @param string $docroot
		 * @return array
		 */
		protected function mapFilesFromList(array $files, string $docroot): array
		{
			$prefix = $this->domain_fs_path();

			return array_filter(array_map(function ($f) use ($docroot, $prefix) {
				return $this->buildFileMapList($f, $docroot, $prefix);
			}, $files));
		}

		/**
		 * Build map of physical files from glob-style list
		 *
		 * @param string $f       file name
		 * @param string $docroot document root
		 * @param string $prefix  path to prepend
		 * @return null|string
		 */
		protected function buildFileMapList(string $f, string $docroot, string $prefix = ''): ?string
		{
			if ($f[0] !== '/') {
				$f = $docroot . '/' . $f;
			}
			$path = $prefix . $f;
			/**
			 * allow specifying additional files that may not
			 * exist in the install
			 */
			if (false === strpbrk($f, '[]*') && !file_exists($path)) {
				return null;
			}

			return $f;
		}

		/**
		 * Get ACL files
		 *
		 * @param null|string $mode
		 * @param string app root
		 * @return array
		 */
		protected function getACLFiles(?string $mode, string $docroot): ?array
		{
			$app = Loader::fromDocroot(null, $docroot, $this->getAuthContext());
			$manifest = Manifest::instantiateContexted($this->getAuthContext(), [$app]);
			$maps = $this->aclList;
			if ($manifest->exists() && $manifest->verifySignature()) {
				$maps = array_merge_recursive($this->aclList, array_get($manifest, 'fortification', []));
			}

			if (null === $mode) {
				return $maps;
			}

			return $maps[$mode] ?? null;
		}

		public function setOptions(string $docroot, ?array $options)
		{
			if (null === $options) {
				return $this->getMap($docroot)->setOption($options);
			}
			return $this->getMap($docroot)->getOptions()->replace($options);
		}

		protected function initializeMeta(string $docroot, array $opts = []): bool
		{
			$type = $this->getModule();
			if (isset($opts['type'])) {
				$type = $opts['type'];
			}
			$loader = Loader::fromDocroot($type, $docroot, $this->getAuthContext());
			$params = array(
				'version'    => $loader->getVersion(true),
				'hostname'   => $loader->getHostname(),
				'path'       => $loader->getPath(),
				'type'       => $type,
				'autoupdate' => (bool)($opts['autoupdate'] ?? false),
				'options'    => array_intersect_key(
					$opts,
					array_diff_key(
						array_flip($this->reconfigurables($loader->getHostname(), $loader->getPath())),
						array_flip($loader::TRANSIENT_RECONFIGURABLES)
					)
				),
			);

			return $this->map('add', $docroot, $params)
				&& (Webapps\App\UIPanel::instantiateContexted(
					$this->getAuthContext())->freshen($loader->getHostname(), $loader->getPath()
				) || true);
		}

		/**
		 * Set or replace single element of map
		 *
		 * @param string       $docroot
		 * @param string|array $param
		 * @param mixed         $val
		 * @return bool
		 */
		private function setMap(string $docroot, $param, $val = null): bool
		{
			$map = $this->getMap($docroot);

			if ($param === 'options') {
				$this->getMap($docroot)->setOption($param, $val);
				return true;
			}

			if ($param instanceof Meta) {
				$map = $param;
			} else if (!\is_array($param)) {
				if (null === $val) {
					unset($map[$param]);
				} else {
					$map[$param] = $val;
				}
			} else {
				$map = $param;
			}

			return $this->saveMap($docroot, $map);
		}

		/**
		 * Get webapp metadata
		 *
		 * @param string $docroot
		 * @return array
		 */
		private function getMap(string $docroot): Meta
		{
			return MetaManager::factory($this->getAuthContext())->get($docroot);
		}

		/**
		 * Save map
		 *
		 * @param string     $docroot
		 * @param array|Meta $map
		 * @return bool
		 */
		private function saveMap(string $docroot, $map): bool
		{
			if ($map instanceof Meta) {
				// already synchronized
				return true;
			}
			$docroot = rtrim($docroot, '/');
			// can't use \Preferences::get('webapps.paths.foo.com') because foo.com -> foo['com']
			$prefs = MetaManager::factory($this->getAuthContext());
			if (null === $map) {
				$prefs->forget($docroot);
			} else {
				$prefs->set($docroot, $map);
			}

			return true;
		}

		/**
		 * Get webapp options
		 *
		 * @param $docroot
		 * @return MetaManager\Options
		 */
		public function getOptions($docroot): MetaManager\Options
		{

			return $this->getMap($docroot)->getOptions();
		}

		/**
		 * Parse general installer options
		 *
		 * Handles: version, ssl, autoupdate, email, user, and git attributes
		 *
		 * @param array  $options
		 * @param string $hostname
		 * @param string $path
		 * @return bool
		 */
		protected function parseInstallOptions(array &$options, string $hostname, string $path = ''): bool
		{
			$docroot = $this->getDocumentRoot($hostname, $path);
			if (!empty($options['empty']) && $this->file_exists($docroot)) {
				$this->file_delete($docroot, true);
			}
			if (!$this->checkDocroot($docroot, $options['user'] ?? null)) {
				return false;
			}

			if  (!empty($options['git'])) {
				defer($options['callbacks'], function () use ($hostname, $path) {
					$approot = $this->getAppRoot($hostname, $path);
					$docroot = $this->getDocumentRoot($hostname, $path);
					$git = Git::instantiateContexted(
						$this->getAuthContext(), [
							$approot,
							MetaManager::factory($this->getAuthContext())->get($docroot)
						]
					);
					$git->createRepository() && $git->snapshot(_('Initial install'));
				});
				// git requires a special callback not handled yet
				unset($options['git']);
			}

			$changes = array_intersect_key($options, array_flip($this->reconfigurables($hostname, $path)));
			$app = Loader::fromDocroot(null, $docroot, $this->getAuthContext());
			$reconfigurator = Reconfigurator::instantiateContexted($this->getAuthContext(), [$app]);

			foreach ($changes as $cname => $cval) {
				$reconfigurator->reconfigure($cname, $options[$cname]);
			}

			if ($this->prepareSquash($options)) {
				// squash requested, flip ownership to API owner, workout in unsquash later
				$this->file_chown($docroot, $this->user_id);
			}

			if (!\array_key_exists('verlock', $options)) {
				$options['verlock'] = static::DEFAULT_VERSION_LOCK;
			}

			if (!\array_key_exists('autoupdate', $options)) {
				$options['autoupdate'] = true;
			}

			if (!$this->checkEmail($options)) {
				return false;
			}

			if (!$this->checkVersion($options)) {
				return false;
			}

			$options['email'] = $options['email'] ?? $this->getConfig('siteinfo', 'email');

			if (!preg_match(Regex::EMAIL, $options['email'])) {
				return error("invalid email address `%s' specified", $options['email']);
			}

			if (!empty($options['user'])) {
				if (!$this->user_exists($options['user'])) {
					return error("user `%s' does not exist", $options['user']);
				}

				if (!$this->file_chown($docroot, $options['user'])) {
					return error("failed to prep docroot for ownership by `%s'", $options['user']);
				}
			} else {
				$options['user'] = $this->username;
			}

			// ensure the docroot is owned by the target uid to permit installation
			// correct it at the end
			if ($options['squash']) {
				$this->file_chown($docroot, $this->user_id);
				defer($options['callbacks'], function () use ($docroot) {
					// hook into xxx:install() last when fn returns
					return $this->unsquash($docroot);
				});
			}


			return true;
		}

		/**
		 * Notify application installed
		 *
		 * @param string $hostname
		 * @param string $path
		 * @param array  $args
		 * @return bool
		 */
		protected function notifyInstalled(string $hostname, string $path = '', array $args = []): bool
		{
			if (!array_get($args, 'notify', true)) {
				return true;
			}

			$args['email'] = $args['email'] ?? $this->common_get_email();

			if (empty($args['email'])) {
				return warn("Notification requested but no 'email' parameter found for %(app)s install on %(hostname)s/%(path)s", [
					'app' => $this->getAppName(), $hostname, $path
				]);
			}

			\Lararia\Bootstrapper::minstrap();

			$fqdn = $this->web_normalize_hostname($hostname);
			$args['url'] = rtrim($fqdn . '/' . $path, '/');
			$app = Loader::fromHostname(null, $hostname, $path, $this->getAuthContext());
			if ('webapp' === ($type = $app->getModuleName())) {
				$type = $this->getModule();
			}

			\Illuminate\Support\Facades\Mail::to($args['email'])->
				send((new \Module\Support\Webapps\Mailer('@webapp(' . $type . ')::job-installed', $args + [
					'email'    => $args['email'],
					'login'    => $args['user'] ?? null,
					'password' => $args['password'] ?? null,
					'uri'      => $args['url'],
					'proto'    => empty($args['ssl']) ? 'http://' : 'https://',
					'appname'  => static::APP_NAME,
					'approot'  => $app->getAppRoot(),
					'app'      => $app
				]))->setAppName(static::APP_NAME));

			return true;
		}

		/**
		 * Get or set failed status
		 *
		 * @param string $docroot
		 * @param null|bool $failedFlag
		 * @return bool
		 */
		public function failed(string $docroot, bool $failedFlag = null): bool
		{
			$prefs = $this->getMap($docroot);
			if ($failedFlag === null) {
				return (bool)array_get($prefs, 'failed', false);
			}
			$prefs['failed'] = (bool)$failedFlag;

			return $this->setMap($docroot, $prefs);
		}

		/**
		 * Get application version lock
		 *
		 * @param string $docroot
		 * @return null|string
		 */
		public function getVersionLock(string $docroot): ?string
		{
			$options = $this->getOptions($docroot);
			$lock = array_get($options, 'verlock', static::DEFAULT_VERSION_LOCK);
			return $lock === 'none' ? null : $lock;
		}

		/**
		 * Web application supports fortification
		 *
		 * @param string      $hostname
		 * @param string      $path
		 * @param string|null $mode optional mode (min, max)
		 * @return bool
		 */
		public function has_fortification(string $hostname, string $path = '', string $mode = null): bool
		{
			$docroot = $this->getDocumentRoot($hostname, $path);
			if ($mode) {
				return null !== $this->getACLFiles($mode, $docroot);
			}

			return !empty($this->aclList);
		}

		/**
		 * Supported Fortification modes
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return array
		 */
		public function fortification_modes(string $hostname, string $path = ''): array
		{
			$docroot = $this->getDocumentRoot($hostname, $path);
			return array_keys($this->getACLFiles(null, $docroot));
		}


		/**
		 * Relax permissions to allow write-access
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return bool
		 */
		public function unfortify(string $hostname, string $path = ''): bool
		{
			$docroot = $this->getAppRoot($hostname, $path);
			if (!$docroot) {
				return error("docroot for `%s' is unknown ", $hostname);
			}
			$webuser = $this->web_get_user($hostname, $path);

			$users = array(
				array($webuser => 'rxw'),
				array($webuser => 'drwx'),
				$this->username => 'drwx'
			);
			if (!$this->file_set_acls($docroot, $users, array(File_Module::ACL_MODE_RECURSIVE => true))) {
				return warn("unfortification failed on `%s/%s'", $hostname, $path);
			}
			$app = Loader::fromHostname(null, $hostname, $path, $this->getAuthContext());
			$app->setOption('fortify', false);
			return true;
		}

		/**
		 * Remove an installed web application
		 *
		 * @param string $hostname
		 * @param string $path
		 * @param string $delete "all", "db", or "files"
		 * @return bool
		 */
		public function uninstall(string $hostname, string $path = '', string $delete = 'all'): bool
		{
			$docroot = $this->getDocumentRoot($hostname, $path);

			if (!$docroot) {
				return error('failed to determine path');
			}

			if (!$this->valid($hostname, $path)) {
				return error("`%(path)s' does not contain a valid %(app)s install", ['path' => $docroot, 'app' => static::APP_NAME]);
			}

			$delete = strtolower($delete);
			if ($delete && !\in_array($delete, array('all', 'db', 'files'), true)) {
				return error("unknown delete option `%s'", $delete);
			}
			$config = $this->db_config($hostname, $path);
			$dbtype = $config['type'] ?? 'mysql';
			if (($delete === 'all' || $delete === 'db') && $config !== []) {
				if (!$config) {
					warn('cannot remove database, config missing?');
				} else if ($this->{$dbtype . '_database_exists'}($config['db']) && !$this->{$dbtype . '_delete_database'}($config['db'])) {
					warn("failed to delete mysql database `%s'", $config['db']);
				} else if ($config['user'] !== $this->getServiceValue($dbtype, 'dbaseadmin')) {
					if ($this->{$dbtype . '_user_exists'}($config['user'],
							$config['host']) && !$this->{$dbtype . '_delete_user'}($config['user'], $config['host'])
					) {
						warn("failed to delete mysql user `%s' on localhost", $config['user']);
					}
				}
			} else if ($config) {
				warn("database kept, delete user: `%s'@`%s', db: `%s' manually",
					$config['user'],
					$config['host'],
					$config['db']
				);
			}
			$options = $this->getOptions($docroot);
			$this->map('delete', $docroot);
			$saved = array_intersect_key(
				$options->toArray(),
				array_flip($this->webapp_reconfigurables($hostname, $path))
			);
			$prefs = [
				'type' => null,
				'failed' => false,
				'options' => $saved
			];
			// reapply generic settings
			if (!$delete || $delete === 'db') {
				return info("removed configuration, manually delete files under `%s'", $docroot);
			}
			$approot = $this->getAppRoot($hostname, $path);
			if (($oldroot = $this->getDocumentRoot($hostname, $path)) !== $this->getAppRoot($hostname, $path)) {
				$oldroot = $this->web_normalize_path($hostname, $path);
				$docroot = $this->remapPublic($hostname, $path, '') ?? $docroot;
				// rename normalized root, can't do before it's remapped as symlinks are always resolved
				MetaManager::instantiateContexted($this->getAuthContext())->rename($oldroot, $docroot);
			}

			$this->map('add', $docroot, $prefs);

			$this->file_delete($approot, true);
			if ($approot !== $docroot && 0 !== strpos("${docroot}/", "${approot}/")) {
				// remapping /var/www/html to /var/www/ghost/public
				$this->file_delete($docroot, true);
			}
			$url = rtrim(join('/', array($hostname, $path)), '/');
			$this->file_purge();
			$this->file_create_directory($docroot, 0755, true);
			if (!array_get($options, 'squash', true)) {
				$this->unsquash($docroot);
			}

			UIPanel::instantiateContexted($this->getAuthContext())->freshen($hostname, $path);

			return info("deleted %s `%s' located under `%s'", static::APP_NAME, $url, $docroot);
		}

		/**
		 * Get document root from hostname
		 *
		 * @param string $hostname subdomain or domain
		 * @param string $path     optional path under hostname
		 * @return bool|string
		 */
		protected function getDocumentRoot(string $hostname, string $path = ''): ?string
		{
			return $this->web_get_docroot($hostname, $path);
		}

		/**
		 * Retrieve document root realpath for given host
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return string|null
		 */
		protected function normalizeDocumentRoot(string $hostname, string $path = ''): ?string
		{
			return $this->web_normalize_path($hostname, $path);
		}

		/**
		 * Map management
		 *
		 * @param string $mode    add or delete
		 * @param string $docroot document root
		 * @param array|null $params map parameters
		 * @return bool
		 */
		protected function map(string $mode, string $docroot, array $params = null): bool
		{
			if ($mode !== 'add' && $mode !== 'delete') {
				return error("failed to set map for `%(docroot)s', unknown mode `%(mode)s'",
					['docroot' => $docroot, 'mode' => $mode]);
			}

			$stat = $this->file_stat($docroot);
			if (!empty($stat['referent']) && $mode !== 'delete') {
				debug('Symlink encountered %(docroot)s -> %(referent)s, changing to %(referent)s',
					['docroot' => $docroot, 'referent' => $stat['referent']]
				);
				$docroot = $stat['referent'];
			}

			unset($params['options']['callbacks']);

			if ($mode === 'delete') {
				return $this->saveMap($docroot, null);
			}

			if (!\array_key_exists('type', $params)) {
				$params['type'] = $this->getModule();
			}

			return $this->saveMap($docroot, $params);
		}

		/**
		 * Get Web App module name
		 *
		 * @return string
		 * @throws \ReflectionException
		 */
		protected function getModule(): string
		{
			$rfxn = new \ReflectionClass($this);
			$class = static::getBaseClassName($rfxn->isAnonymous() ? $rfxn->getParentClass()->getName() :  $rfxn->getName());
			return strtolower(substr($class, 0, strpos($class, '_')) ?? 'skeleton');
		}

		/**
		 * Migrate a document root to public/
		 *
		 * @param string $hostname hostname
		 * @param string $path     subdomain path
		 * @param string $public   optional public folder, leave empty to restore docroot
		 * @return null|string
		 */
		protected function remapPublic(string $hostname, string $path = '', string $public = 'public'): ?string
		{
			if (!$public && !$path && $hostname === $this->domain) {
				// can't relocate primary docroot, short-circuit it
				$this->file_delete(Web_Module::MAIN_DOC_ROOT, true);
				$this->file_create_directory(Web_Module::MAIN_DOC_ROOT);
				return Web_Module::MAIN_DOC_ROOT;
			}
			$rootsave = $this->getDocumentRoot($hostname, $path);
			if (false === ($docroot = $this->movePrimaryDocumentRoot($rootsave))) {
				warn('failed to relocate docroot for %s - installation incomplete', static::APP_NAME);

				return null;
			}
			if (!$public) {
				$public = $this->getAppRoot($hostname, $path);
				$stat = $this->file_stat($public);
				if (!$stat || $stat['link']) {
					warn("Cannot remap public, public `%s' does not exist or is a symlink", $public);

					return null;
				}
			}

			if ($rootsave !== $docroot) {
				$stat = $this->file_stat($rootsave);
				// remove dangling symlink
				if ($stat && $stat['link']) {
					$this->file_delete($rootsave, false);
				}
			}
			if ($public[0] !== '/') {
				if (!$this->file_exists("${docroot}/${public}")) {
					if (!$stat = $this->file_stat($docroot)) {
						error("document root `%s' missing - unable to stat", $docroot);

						return null;
					}
					$this->file_create_directory("${docroot}/${public}");
					$this->file_chown("${docroot}/${public}", $stat['owner']);
				}
				if ($rootsave !== $docroot) {
					// docroot was moved, bad fringe case
					$stat = $this->file_stat($docroot);
					if (!$stat) {
						error("emerg! failed to stat path `%s'", $docroot);

						return null;
					}
					if ($public) {
						// otherwise restoring docroot
						$this->file_symlink($docroot . '/' . $public, $rootsave);
					}
					$this->file_chown_symlink($rootsave, $stat['owner']);
					$this->web_purge();

					return $rootsave;
				}
			}

			$normalized = $this->web_normalize_hostname($hostname);
			[$subdomain, $domain] = array_values($this->web_split_host($normalized));
			// restoring or moving;
			$newpath = strncmp($public, '/', 1) === 0 ? $public : "${docroot}/${public}";
			$ret = true;
			if ($docroot !== rtrim("${docroot}/${public}", '/')) {
				// in case of /var/www/html no modification is performed
				if ($subdomain) {
					$ret = $this->web_rename_subdomain($hostname, $hostname, $newpath)
						&& info("Moved document root for `%s' from `%s' to `%s'", $normalized, $docroot, $newpath);
				} else {
					$ret = $this->aliases_modify_domain($domain, ['path' => $newpath])
						&& info("Moved document root for `%s' from `%s' to `%s'", $normalized, $docroot, $newpath);
				}
			}
			$this->web_purge();

			return !$ret ? null : $newpath;
		}

		/**
		 * Migrate document root elsewhere
		 *
		 * @param string $docroot
		 * @return mixed string new root on relocation, false on error
		 */
		protected function movePrimaryDocumentRoot(string $docroot)
		{
			if ($docroot !== \Web_Module::MAIN_DOC_ROOT) {
				return $docroot;
			}
			$app = $this->getInternalName();
			// @todo objects make more sense now...
			$suffix = '';
			$i = 0;
			do {
				$newroot = \dirname($docroot) . DIRECTORY_SEPARATOR . 'html-' . $app . $suffix;
				if (!$this->file_exists($newroot)) {
					break;
				}
				$newroot = null;
				$i++;
				$suffix = '-' . date('Ymd-' . $i);
			} while ($i < 100);

			if (null === $newroot) {
				return error("failed to rename docroot `%s' - cannot find a suitable location", $docroot);
			}

			if ($this->file_exists($newroot)) {
				return error("attempted to install %(app)s in `%(location)s', but directory exists - remove first before proceeding",
					['app' => ucwords($app), 'location' => $newroot]);
			}
			if (!$this->file_rename($docroot, $newroot)) {
				return error("failed to rename docroot from `%(old)s' to `%(new)s'", ['old' => $docroot, 'new' => $newroot]);
			}
			info("Relocated primary document root from `%(old)s' to `%(new)s'", ['old' => $docroot, 'new' => $newroot]);
			MetaManager::instantiateContexted($this->getAuthContext())->rename(Web_Module::MAIN_DOC_ROOT, $newroot);
			$this->web_purge();

			return $newroot;
		}

		/**
		 * Change ownership from active uid to parent directory uid
		 *
		 * @param string $docroot document root
		 * @return bool
		 */
		protected function unsquash(string $docroot): bool
		{
			$stat = $this->file_stat(\dirname($docroot));
			$newuid = array_first([$stat['uid'], $this->user_id], static function ($uid) {
				return $uid >= User_Module::MIN_UID;
			});
			if ($newuid === $this->user_id) {
				return true;
			}
			$ret = $this->file_chown($docroot, $newuid, true);
			if (!$ret) {
				warn("failed to unsquash `%(docroot)s', ownership will remain as `%(owner)s'", ['docroot' => $docroot, 'owner' => $this->username]);
			}
			// @todo this is a kludge to allow updates by site admin without sudo to user
			// adjust update routines to sudo?
			$this->file_set_acls(
				$docroot,
				$this->username,
				'rwx',
				[File_Module::ACL_MODE_RECURSIVE]
			);
			foreach ($this->web_list_subdomains('path', $docroot) as $subdomain => $path) {
				// update ownership on subdomain link
				$this->web_rename_subdomain($subdomain, $subdomain, $path);
			}
			return true;
		}

		/**
		 * Get next version in hierarchy
		 *
		 * @param string $version
		 * @param string $maximalbranch
		 * @return null|string
		 */
		public function next_version(string $version, string $maximalbranch = '99999999.99999999.99999999'): ?string
		{
			$versions = $this->get_versions();

			return Versioning::nextVersion($versions, $version, $maximalbranch);
		}

		/**
		 * Check if version is latest or get latest version
		 *
		 * @param null|string $version
		 * @param string|null $branchcomp branch to compare against
		 * @return int|string
		 */
		public function is_current(string $version = null, string $branchcomp = null)
		{
			$versions = $this->get_versions();
			if (!$version) {
				return array_pop($versions);
			}

			return Versioning::current($versions, $version, $branchcomp);
		}

		/**
		 * Check email option
		 *
		 * @param array $options
		 * @return bool
		 */
		protected function checkEmail(array &$options): bool
		{
			if (isset($options['email']) && !preg_match(Regex::EMAIL, $options['email'])) {
				return error("invalid email address `%s' specified", $options['email']);
			}
			$options['email'] = $options['email'] ?? $this->common_get_email();

			return true;
		}

		/**
		 * Convert ownership to match parent directory of docroot
		 *
		 * In relocatable docroots (moving /var/www/html to /var/www/foo-bar/public)
		 * this always runs as the same user as "user" option
		 *
		 * @param array $options
		 * @return bool
		 */
		protected function prepareSquash(array &$options): bool
		{
			$squash = array_get($options, 'squash', false);
			if ($squash && $this->permission_level & PRIVILEGE_USER) {
				warn('cannot squash privileges as secondary user');
				$squash = false;
			}
			$options['squash'] = $squash;

			return (bool)$squash;
		}

		/**
		 * Check and prepare version information
		 *
		 * @param array $options
		 * @return bool
		 */
		protected function checkVersion(array &$options): bool
		{
			$version = array_get($options, 'version');
			if (null === $version) {
				$version = $this->get_versions();
				$version = array_pop($version);
			} else if (null !== $version && strcspn($version, '.0123456789')) {
				return error('invalid version number, %s', $version);
			}
			$options['version'] = $version;

			return true;
		}

		/**
		 * Verify docroot is writeable before beginning installation
		 *
		 * @param string      $docroot docroot to check
		 * @param string|null $user    optional docroot owner
		 * @return bool
		 */
		protected function checkDocroot(string $docroot, string $user = null): bool
		{
			if (!$user) {
				$user = $this->username;
			}
			if (!$this->user_exists($user)) {
				return error("target user for install `%s' does not exist", $user);
			}
			$this->file_purge();

			if (!$this->file_exists($docroot) && !$this->file_create_directory($docroot, 0755, true)) {
				return error("failed to create installation directory `%s'", $docroot);
			}

			$publicpath = $this->domain_fs_path() . DIRECTORY_SEPARATOR . $docroot;
			$files = glob($publicpath . DIRECTORY_SEPARATOR . '{.,}*', GLOB_BRACE|GLOB_NOSORT);

			$placeholder = $docroot . '/index.html';

			// extract to sweepDocroot()
			if ($this->file_exists($docroot . '/.htaccess') &&
				array_get($this->file_stat($docroot . '/.htaccess'), 'size', null) === 0)
			{
				$this->file_delete($docroot . '/.htaccess');
				$files--;
			}
			if (\count($files) === 3 /* . + .. links */ && $this->file_exists($placeholder)) {
				$this->file_delete($placeholder);
				info('removed placeholder file');
			} else if (\count($files) > 2 /* . + .. */) {
				return error("install path `%s' must be empty, %d files located in `%s'", $docroot, \count($files)-2,
					$docroot);
			}

			$this->file_chown($docroot, $user);
			$this->file_purge();

			return true;
		}

		/**
		 * Get latest release
		 *
		 * @param string $branch branch to discover latest version
		 * @return null|string
		 */
		protected function getLatestVersion(string $branch = null): ?string
		{
			$versions = $this->get_versions();
			if (!$versions) {
				return null;
			}
			if (!$branch) {
				return array_pop($versions);
			}

			$latest = null;
			$search = $branch . '.0';

			foreach ($versions as $version) {
				if (0 === strpos($version, $branch) && version_compare($version, $search, '>=')) {
					$latest = $version;
				}
			}

			return $latest;
		}

		/**
		 * Add RewriteBase to htaccess
		 *
		 * @param        $docroot
		 * @param string $path
		 * @return bool
		 * @throws \FileError
		 */
		protected function fixRewriteBase(string $docroot, string $path = ''): bool
		{
			$file = $docroot . $path . '/.htaccess';
			if (!$this->file_exists($file)) {
				return true;
			}
			$htaccess = $this->file_get_file_contents($file);
			$replacement = '$1RewriteEngine On' . "\n" .
				'$1RewriteBase ' . '/' . ltrim($path, '/');

			$newhtaccess = preg_replace('/^(\s*)RewriteEngine On(?!\s+RewriteBase)/mi', $replacement, $htaccess);
			return $this->file_put_file_contents($file, $newhtaccess);
		}

		/**
		 * Remove invalid directives from .htaccess
		 *
		 * @param string $docroot document root
		 * @param string $path subpath
		 * @return bool
		 * @throws \FileError
		 */
		protected function removeInvalidDirectives(string $docroot, string $path = ''): bool
		{
			$file = $docroot . '/' . ltrim($path, '/') . '/.htaccess';
			if (!$this->file_exists($file)) {
				return true;
			}
			$htaccess = $this->file_get_file_contents($file);
			foreach (static::BANNED_DIRECTIVES as $directive) {
				$htaccess = preg_replace(
					[
						'/^(\s*Options\s+.*?(?=[+-]?' . $directive . '))(?:[+-]?' . $directive . '(?=\s+|$))/m',
						'/^\s*Options\s*$/m'
					],
					'\1',
					$htaccess
				);
			}
			return $this->file_put_file_contents($file, $htaccess);
		}

		/**
		 * Download specified file
		 *
		 * @param string $url
		 * @param string $dest
		 * @param bool   $extract
		 * @throws HTTP_Request2_Exception
		 * @return string|bool
		 */
		protected function download(string $url, string $dest, bool $extract = true)
		{
			// @todo refactor into helper, cf. filemanager
			$prefix = $this->domain_fs_path();
			// necessary because sometimes file_extract can be dumb
			$basename = basename($url);
			if (false !== ($pos = strpos($basename, '?'))) {
				$basename = substr($basename, 0, $pos);
			}
			// $dest specified as filename.ext
			if (false !== strpos(basename($dest), '.')) {
				$basename = basename($dest);
				$dest = dirname($dest);
			}

			if (false !== ($pos = strpos($basename, '.tar.'))) {
				$suffix = substr($basename, $pos);
			} else {
				$suffix = substr($basename, strrpos($basename, '.'));
			}

			$tmp = tempnam($prefix . '/' . TEMP_DIR, 'wget') . $suffix;
			if (file_exists($tmp)) {
				return error("cannot download, dest file/suffix `%s' exists", $tmp);
			}
			if (\extension_loaded('curl')) {
				$adapter = new \HTTP_Request2_Adapter_Curl();
			} else {
				$adapter = new \HTTP_Request2_Adapter_Socket();
			}

			$http = new \HTTP_Request2(
				$url,
				\HTTP_Request2::METHOD_GET,
				array(
					'adapter'    => $adapter,
					'store_body' => false
				)
			);

			$observer = new \HTTP_Request2_Observer_SaveDisk($tmp);
			$http->attach($observer);
			try {
				$response = $http->send();
				$code = $response->getStatus();
				switch ($code) {
					case 200:
						break;
					case 403:
						return error("URL `%s' request forbidden by server", $url);
					case 404:
						return error("URL `%s' not found on server", $url);
					case 301:
					case 302:
						$newLocation = $response->getHeader('location');

						return self::download($newLocation, "$dest/$basename", $extract);
					default:
						return error("URL request failed, code `%d': %s",
							$code, $response->getReasonPhrase());
				}
				$content = $response->getHeader('content-type');
				$okcontent = array('application/octet-stream', 'application/zip', 'application/x-bzip2');
				if (!\in_array($content, $okcontent, true)) {
					\Error_Reporter::report($content);
				}
				// this returns nothing as xfer is saved directly to disk
				$http->getBody();
			} catch (\HTTP_Request2_Exception $e) {
				return error("fatal error retrieving URL: `%s'", $e->getMessage());
			}
			if (!posix_getuid()) {
				chown($tmp, WS_USER);
			}
			// write file
			$jailtmp = $this->file_unmake_path($tmp);
			$this->file_endow_upload(basename($jailtmp));
			if (version_compare(platform_version(), '6.5', '>=')) {
				// Luna+ platforms use OverlayFS
				$this->file_purge();
			}

			if ($extract && $this->file_is_compressed($jailtmp)) {
				if (!$this->file_extract($jailtmp, $dest)) {
					error("failed to extract downloaded file to `%s'", $dest);
					$dest = null;
				}
			}
			$this->file_delete($jailtmp);

			return $dest;
		}

		/**
		 * Set webapp meta
		 *
		 * @param string     $docroot document root
		 * @param array|null $info
		 * @return Meta
		 */
		protected function setInfo(string $docroot, ?array $info): void
		{
			$fn = null === $info ? 'set' : 'replace';
			MetaManager::factory($this->getAuthContext())->get($docroot)->{$fn}($info);
		}

		/**
		 * Get all hostnamers on site
		 *
		 * @param \Auth_Info_User $ctx
		 * @return array
		 */
		public static function getAllHostnames(\Auth_Info_User $ctx): array
		{
			$afi = \apnscpFunctionInterceptor::factory($ctx);
			$domainfill = array_merge([$ctx->domain], $afi->aliases_list_aliases());
			$hosts = array_combine($domainfill, $domainfill);
			$subdomains = array_keys($afi->web_list_subdomains());

			foreach ($subdomains as $subdomain) {
				if (false === strpos($subdomain, '.')) {
					$hosts[$subdomain] = $subdomain . '.' . $ctx->domain;
					continue;
				}
				$tmp = $afi->web_split_host($subdomain);
				if (!isset($hosts[$tmp['domain']])) {
					// @XXX
					// detached domain, subdomain no longer applicable
					// this should be removed automatically by removal
					// of domain
					continue;
				}
				$hosts[$subdomain] = $subdomain;
			}

			return array_values($hosts);
		}

		/**
		 * Get hostnames grouped by subdomain and domain
		 *
		 * @param \Auth_Info_User $ctx
		 * @param bool            $showWWW
		 * @param bool            $sort
		 * @return array|array[]
		 */
		public static function getAllPartitionedHostnames(\Auth_Info_User $ctx, bool $showWWW = true, bool $sort = false): array
		{
			$afi = \apnscpFunctionInterceptor::factory($ctx);
			$ret = ['domain' => [], 'subdomain' => []];
			$domainfill = array_merge([$ctx->domain], $afi->aliases_list_aliases());
			$subdomains = array_keys($afi->web_list_subdomains());
			if ($sort) {
				asort($subdomains);
				asort($domainfill);
			}

			$domains = array_fill_keys($domainfill, array());
			array_unshift($subdomains, '');
			if ($showWWW) {
				array_unshift($subdomains, 'www');
			}
			foreach ($subdomains as $subdomain) {
				if (false === strpos($subdomain, '.')) {
					foreach (array_keys($domains) as $d) {
						$ret['domain'][$d][] = $subdomain;
					}
					$ret['subdomain'][$subdomain] = $domainfill;
					continue;
				}
				$tmp = $afi->web_split_host($subdomain);
				if (!isset($domains[$tmp['domain']])) {
					// @XXX
					// detached domain, subdomain no longer applicable
					// this should be removed automatically by removal
					// of domain
					continue;
				}
				$ret['subdomain'][$tmp['subdomain']][] = $tmp['domain'];
				$ret['domain'][$tmp['domain']][] = $tmp['subdomain'];
			}

			return $ret;
		}

		/**
		 * Document root contains expected app type
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return bool
		 */
		abstract public function valid(string $hostname, string $path = ''): bool;

		/**
		 * @inheritDoc
		 */
		public function theme_status(string $hostname, string $path = '', string $theme = null)
		{
			return [];
		}

		/**
		 * @inheritDoc
		 */
		public function install_theme(string $hostname, string $path, string $theme, string $version = null): bool
		{
			return error('not implemented');
		}

		/**
		 * @inheritDoc
		 */
		public function uninstall_theme(string $hostname, string $path, string $theme, bool $force = false): bool
		{
			return error('not implemented');
		}

		/**
		 * @inheritDoc
		 */
		public function plugin_status(string $hostname, string $path = '', string $plugin = null)
		{
			return [];
		}

		/**
		 * @inheritDoc
		 */
		public function install_plugin(string $hostname, string $path, string $plugin, string $version = ''): bool
		{
			return error("not implemented");
		}

		/**
		 * @inheritDoc
		 */
		public function uninstall_plugin(string $hostname, string $path, string $plugin, bool $force = false): bool
		{
			return error("not implemented");
		}

		/**
		 * @inheritDoc
		 */
		public function disable_all_plugins(string $hostname, string $path = ''): bool
		{
			return error("not implemented");
		}

		/**
		 * @inheritDoc
		 */
		public function db_config(string $hostname, string $path = '')
		{
			return [];
		}

		/**
		 * @inheritDoc
		 */
		public function change_admin(string $hostname, string $path, array $fields): bool
		{
			return error("not implemented");
		}

		/**
		 * @inheritDoc
		 */
		public function get_admin(string $hostname, string $path = ''): ?string
		{
			return null;
		}

		/**
		 * @inheritDoc
		 */
		public function update_all(string $hostname, string $path = '', string $version = null): bool
		{
			return error("not implemented");
		}

		/**
		 * @inheritDoc
		 */
		public function update(string $hostname, string $path = '', string $version = null): bool
		{
			return error("not implemented");
		}

		/**
		 * @inheritDoc
		 */
		public function update_plugins(string $hostname, string $path = '', array $plugins = array()): bool
		{
			return error("not implemented");
		}

		/**
		 * @inheritDoc
		 */
		public function update_themes(string $hostname, string $path = '', array $themes = array()): bool
		{
			return error("not implemented");
		}

		/**
		 * Asset skipped for update
		 *
		 * @param string      $hostname hostname
		 * @param string      $path     path
		 * @param string      $name     asset name
		 * @param string|null $type     asset subtype
		 * @return bool
		 */
		public function asset_skipped(string $hostname, string $path, string $name, ?string $type): bool
		{
			return false;
		}

		/**
		 * Reconfigure a webapp
		 *
		 * @param string       $hostname hostname
		 * @param string       $path     optional path
		 * @param string|array $param    array of reconfigurables or single value
		 * @param mixed        $value    value to set in single-value mode
		 * @return bool
		 */
		public function reconfigure(string $hostname, string $path, $param, $value = null): bool
		{
			if (static::class !== \a23r::get_class_from_module('webapp') && !$this->valid($hostname, $path)) {
				// switch back to webapp module
				return $this->webapp_reconfigure($hostname, $path, $param, $value);
			}

			if (\is_array($param) && $value) {
				return error('Invalid parameter format');
			}

			if (!\is_array($param)) {
				$param = [$param => $value];
			}

			$bad = array_diff(array_keys($param), $this->reconfigurables($hostname, $path));
			if ($bad) {
				return error('Unknown reconfigurable directives: %s', implode(', ', $bad));
			}
			$original = $this->getOptions($docroot = $this->getDocumentRoot($hostname, $path));
			$app = Loader::fromDocroot($this->getAppName(), $docroot, $this->getAuthContext());
			$changes = $original->diff($param) + array_intersect_key($param, array_flip($app::TRANSIENT_RECONFIGURABLES));
			$reconfigurator = Reconfigurator::instantiateContexted($this->getAuthContext(), [$app]);
			foreach ($changes as $cname => $cval) {
				$reconfigurator->reconfigure($cname, $cval);
			}

			return true;
		}

		/**
		 * List of reconfigurable elements
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return array
		 */
		public function reconfigurables(string $hostname, string $path = ''): array
		{
			$transients = Loader::fromHostname($this->getAppName(), $hostname, $path, $this->getAuthContext());
			return array_merge(
				['user', 'http10', 'ssl', 'verlock', 'autoupdate', 'git', 'affixed'],
				$transients::TRANSIENT_RECONFIGURABLES
			);
		}

		/**
		 * Get reconfigurable setting
		 *
		 * @param string $hostname
		 * @param string $path
		 * @param string|string[] $setting
		 * @return mixed
		 */
		public function get_reconfigurable(string $hostname, string $path, $setting)
		{
			if (static::class !== \a23r::get_class_from_module('webapp') && !$this->valid($hostname, $path)) {
				return error("%(hostname)s/%(path)s does not contain a valid %(app)s install", [
					'hostname' => $hostname, 'path' => $path, 'app' => $this->getAppName()
				]);
			}
			$wantArray = is_array($setting);
			$setting = array_flip((array)$setting);

			$bad = array_diff(array_keys($setting), $this->reconfigurables($hostname, $path));
			if ($bad) {
				return error('Unknown reconfigurable directives: %s', implode(', ', $bad));
			}
			$app = Loader::fromHostname($this->getAppName(), $hostname, $path, $this->getAuthContext());
			$reconfigurator = Reconfigurator::instantiateContexted($this->getAuthContext(), [$app]);
			foreach ($setting as $cname => $cval) {
				$setting[$cname] = $reconfigurator->handler($cname)->getValue();

			}

			return $wantArray ? $setting : current($setting);
		}

		/**
		 * Register new web application
		 *
		 * @param string $app
		 * @param string $handler
		 * @return bool
		 */
		public static function registerApplication(string $app, string $handler): bool
		{
			if (isset(self::$registeredProviders[$app])) {
				return error("Web App provider `%s' is already registered", $app);
			}
			try {
				if (!(new \ReflectionClass($handler))->isSubclassOf(Unknown::class)) {
					fatal("Cannot register %s: %s", $handler, Unknown::class . ' is not implemented');
				}
			} catch (\ReflectionException $e) {
				fatal("Cannot register %s: %s", $handler, $e->getMessage());
			}
			self::$registeredProviders[$app] = $handler;

			return true;
		}

		/**
		 * Snapshot a web application
		 *
		 * @param string $hostname
		 * @param string $path
		 * @param string $comment
		 * @return bool
		 */
		public function snapshot(string $hostname, string $path = '', string $comment = 'snapshot'): bool
		{
			$approot = $this->getAppRoot($hostname, $path);
			$docroot = $this->getDocumentRoot($hostname, $path);
			$git = Git::instantiateContexted(
				$this->getAuthContext(), [
					$approot,
					MetaManager::factory($this->getAuthContext())->get($docroot)
				]
			);
			if (!$git->enabled()) {
				return error("Snapshots are not enabled for `%s%s'", $hostname, $path);
			}

			return $git->snapshot($comment) !== null;
		}

		/**
		 * Rollback specified snapshot
		 *
		 * @param string      $hostname
		 * @param string      $path
		 * @param string|null $commit   optional commit
		 * @return bool
		 */
		public function rollback(string $hostname, string $path = '', string $commit = null): bool
		{
			$approot = $this->getAppRoot($hostname, $path);
			$docroot = $this->getDocumentRoot($hostname, $path);
			$meta = MetaManager::factory($this->getAuthContext())->get($docroot);
			$git = Git::instantiateContexted(
				$this->getAuthContext(), [
					$approot,
					$meta
				]
			);
			if (!$git->enabled()) {
				return error("Snapshots are not enabled for `%s%s'", $hostname, $path);
			}

			$prune = null === $commit;
			if (!$commit && null === ($commit = $git->lastSnapshot())) {
				return error("No snapshots found for `%s%s'", $hostname, $path);
			}

			$handler = static::handlerFromApplication($this->getAppName());
			return $git->restore($commit, $prune) &&
				$this->fortify($hostname, $path, data_get($meta, 'fortify', $handler::DEFAULT_FORTIFICATION));
		}

		/**
		 * Get handler from application
		 *
		 * @param string $app
		 * @return string
		 */
		public static function handlerFromApplication(string $app): string
		{
			static $map = [];
			if (isset($map[$app])) {
				return $map[$app];			}
			return $map[$app] = self::$registeredProviders[$app] ?? (self::class . '\\App\\Type\\' . ucwords($app ?: 'unknown') . '\\Handler');
		}
	}