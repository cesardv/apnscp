<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	/**
	 * A priority heap that supports caching/serialization
	 */

	namespace Module\Support\Personality\Helpers;

	use ArrayObject;

	class CacheablePriorityHeap extends \SplPriorityQueue implements \Serializable
	{

		public function compare($a, $b)
		{
			return $b - $a;
		}

		public function serialize()
		{
			$objects = [];
			$heap = clone $this;
			$heap->setExtractFlags(\SplPriorityQueue::EXTR_BOTH);
			while (null !== ($node = $heap->current())) {
				$objects[] = $node;
				$heap->next();
			}

			return serialize($objects);
		}

		public function unserialize($serializedData)
		{
			$objects = \Util_PHP::unserialize($serializedData, [ArrayObject::class]);
			foreach ($objects as $node) {
				$this->insert($node['data'], $node['priority']);
			}

			return $this;
		}
	}