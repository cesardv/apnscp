<?php declare(strict_types=1);

/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
 */

namespace Module\Support;

use ArgumentError;
use Exception;
use Module_Skeleton;
use MySQL;
use Opcenter\Database\DatabaseCommon;
use Opcenter\Filesystem;
use Regex;
use Util_Process_Safe;

abstract class Sql extends Module_Skeleton implements \Opcenter\Contracts\Hookable
{
	// default number of concurrent connections to permit per user

	const DEFAULT_CONCURRENCY_LIMIT = 20;

	const MIN_PASSWORD_LENGTH = 5;

	/**
	 * @var int minimum db prefix length, to reduce collisions on server xfers
	 */
	const MIN_PREFIX_LENGTH = 3;

	const MASTER_USER = 'root';

	/**
	 * a bullshit constant to decide whether to
	 * up the ulimit fsize or not before exporting a db
	 */
	const DB_BIN2TXT_MULT = 1.5;

	// @var array Temporarily created users
	private static $mysql_admin_pass;
	protected $_tempUsers = [];

	/**
	 * Rename databases on prefix change
	 *
	 * @param string $prefixold
	 * @param string $prefixnew
	 * @return int|bool # databases changed or false on failure
	 */
	protected function renameDatabasePrefix($prefixold, $prefixnew)
	{
		$ctr = $this->renameDatabasePrefixByType('mysql', $prefixold, $prefixnew);
		if ($ctr !== false && $this->enabled('pgsql')) {
			$ctr = $this->renameDatabasePrefixByType('pgsql', $prefixold, $prefixnew);
		}

		return $ctr;
	}

	protected function renameDatabasePrefixByType($type, $prefixold, $prefixnew): bool
	{
		$class = '\Opcenter\Database\\' . self::pedantize($type);
		$changed = 0;
		$dbs = $this->{"${type}_list_databases"}();
		foreach ($dbs as $db) {
			$new = DatabaseCommon::convertPrefix($db, $prefixold, $prefixnew);
			if ($new === $db) {
				continue;
			}
			if ($class::moveDatabase($db, $new)) {
				$changed++;
			}
		}

		return (bool)$changed;
	}

	/**
	 * Convert shorthand to formal name
	 *
	 * @param string $name
	 * @return string pedantic name of database
	 */
	protected static function pedantize($name): string
	{
		return DatabaseCommon::canonicalizeBrand($name);
	}

	/**
	 * Rename secondary database users
	 *
	 * @param $prefixold
	 * @param $prefixnew
	 * @return int|bool # users changed or false on failure
	 */
	protected function renameUserPrefix($prefixold, $prefixnew)
	{
		return $this->sqlWrapper('renameUserByType', $prefixold, $prefixnew);
	}

	private function sqlWrapper(string $fn, ...$args)
	{
		foreach (['mysql', 'pgsql'] as $sql) {
			if (!$this->enabled($sql)) {
				continue;
			}
			if (!$this->$fn($sql, ...$args)) {
				return false;
			}
		}

		return true;
	}

	protected function renameUser(string $old, string $new): bool
	{
		return (bool)$this->sqlWrapper('renameUserByType', $old, $new);
	}

	protected function renameUserPrefixByType($type, $prefixold, $prefixnew): int
	{
		$changed = 0;
		$users = $this->{"list_${type}_users"}();
		foreach (array_keys($users) as $user) {
			$new = $new = DatabaseCommon::convertPrefix($user, $prefixold, $prefixnew);
			if ($new === $user) {
				continue;
			}
			if ($this->renameUserByType($type, $user, $new)) {
				$changed++;
			}
		}

		return $changed;
	}

	/**
	 * Rename database user
	 *
	 * @param string $type
	 * @param string $old
	 * @param string $new
	 * @return bool
	 */
	protected function renameUserByType(string $type, string $old, string $new): bool
	{
		$class = '\Opcenter\Database\\' . self::pedantize($type);
		$ret = true;
		if ($class::userExists($old)) {
			$ret = $class::renameUser($old, $new);
			if ($type === 'pgsql' && $new === ($user = $this->getServiceValue('pgsql', 'dbaseadmin'))) {
				info("Updating PostgreSQL password for user `%s'", $user);
			}
		}

		return $ret;
	}

	/**
	 * Install database service
	 *
	 * @param string $svc
	 * @return null|bool
	 */
	protected function installDatabaseService(string $svc): bool
	{
		if ($svc !== 'pgsql' && $svc !== 'mysql') {
			return error("unknown database service `%s'", $svc);
		}

		$conf = $this->getAuthContext()->getAccount()->new;
		// @todo separate mysql/pgsql configuration
		if (null === ($passwd = array_get($conf, 'mysql.passwd'))) {
			if ($tmp = $this->mysql_get_option('password', 'client')) {
				$passwd = $tmp;
			} else {
				$passwd = \Opcenter\Auth\Password::generate(32);
			}
		}
		$domain = $conf['siteinfo']['domain'];
		$proc = new Util_Process_Safe();
		$proc->setEnvironment('HOME', '/root');
		$ret = $proc->run('/usr/local/sbin/add%s-nodb.sh %s %s',
			$svc, $domain, $passwd);
		if (!$ret['success']) {
			return error("failed to add %s for site `%s'",
				$svc, $domain);
		}

		return true;
	}

	/**
	 * Delete all databases from account
	 *
	 * @param string $svc service
	 * @return bool
	 */
	protected function uninstallDatabaseService(string $svc): ?bool
	{
		if (!$this->enabled($svc)) {
			return error("database service `%s' not enabled", $svc);
		}
		$helper = '\\Opcenter\\Database\\' . self::pedantize($svc);
		$dbs = $this->{$svc . '_list_databases'}();
		foreach ($dbs as $db) {
			if ($this->{$svc. '_database_exists'}($db)) {
				$fn = "${svc}_delete_database";
				$this->$fn($db);
			}
		}
		$users = $this->{$svc . '_list_users'}();
		$admin = $this->getServiceValue('mysql', 'dbaseadmin');
		$delete = [];
		foreach ($users as $user => $tmp) {
			/**
			 * @todo mysql and postgresql should return a consistent
			 */
			$hosts = $helper::hasHosts() ? array_keys($tmp) : ['localhost'];
			if ($user === $admin) {
				$delete = $hosts;
				/**
				 * admin user is a special case and needs to be removed last
				 */
				continue;
			}
			foreach ($hosts as $host) {
				if (!$this->{$svc . '_user_exists'}($user, $host)) {
					continue;
				}
				$fn = "${svc}_delete_user";
				$this->$fn($user, $host);
			}
		}

		foreach ($delete as $host) {
			$helper::deleteMainUser($admin, $host);
		}

		if ($delete) {
			// if we enumerate and come across the admin user as properly provisioned
			// then let's call delete otherwise skip
			$helper::uninstallPostHook($admin, $host);
		}

		return true;
	}

	/**
	 * Add temporary *sql user for cleanup
	 *
	 * @param string $user
	 * @return bool
	 */

	protected function _register_temp_user($user)
	{
		$this->_tempUsers[] = $user;

		return true;
	}

	/**
	 * Establish privileged connection to MySQL server
	 *
	 * @param bool $pdo return as PDO
	 * @return \MySQL|\PDO
	 */
	protected function _connect_root(bool $pdo = false)
	{
		if (!$pdo) {
			$conn = new \MySQL('localhost', self::MASTER_USER, $this->_get_elevated_password());
			$conn->select_db('mysql');
			$conn->set_charset('utf8mb4');
			return $conn;
		}

		return new \PDO('mysql:host=localhost;dbname=mysql;charset=utf8mb4', self::MASTER_USER, $this->_get_elevated_password());
	}

	protected function _get_elevated_password()
	{
		if (self::$mysql_admin_pass === null) {
			self::$mysql_admin_pass = $this->query('mysql_get_elevated_password_backend');
		}

		return self::$mysql_admin_pass;
	}

	protected function add_backup_real($type, $db, $extension, $span, $preserve, $email)
	{
		if (!$preserve) {
			$preserve = '0';
		}
		if ($type !== 'mysql' && $type !== 'pgsql') {
			return error("invalid database type `%s'", $type);
		}
		$fn = "${type}_list_databases";
		$dbs = $this->$fn();

		if (!\in_array($db, $dbs, true)) {
			return error('invalid database ' . $db);
		}
		if (!\in_array($extension, array('gz', 'bz', 'zip', 'none'))) {
			return error('Invalid extension');
		}

		if (\intval($span) != $span || \intval($preserve) != $preserve) {
			return error('Non-numeric type for day span/preservation amount');
		} else if ($span < 1) {
			return error('Day span value must be > 0');
		} else if ($email && is_string($email) && !preg_match(Regex::EMAIL, $email)) {
			return error('Invalid e-mail address');
		}
		$dbconn = MySQL::initialize();
		$q = 'INSERT INTO
                    sql_dumps
                        (site_id,
                         db_type,
                         db_name,
                         day_span,
                         extension,
                         preserve,
                         next_date,

                         email)
                 VALUES
                        (' . $this->site_id . ",
                         '" . $type . "',
                         '" . str_replace('\\', '', $db) . "',
                         " . $span . ",
                         '" . $extension . "',
                         " . $preserve . ",
                         NOW(),
                         " . ($email ? $dbconn->escape($email) : "NULL" ). ")
                 ON DUPLICATE KEY UPDATE
                    day_span  = " . $span . ",
                    extension = '" . $extension . "',
                    preserve  =  " . $preserve . ",
                    email     =  " . ($email ? $dbconn->escape($email) : 'NULL');
		try {
			$q = $dbconn->query($q);
		} catch (Exception $e) {
			return error('general error setting backup routine');
		}

		return true;
	}

	/**
	 * Verify if service is enabled
	 *
	 * @param $type
	 * @return mixed|NULL
	 */
	protected function svc_enabled($type)
	{
		return $this->getServiceValue($type, 'enabled');
	}

	protected function edit_backup_real($type, $db, $extension, $span, $preserve, $email)
	{
		if (!$preserve) {
			$preserve = '0';
		}
		if ($type !== 'mysql' && $type !== 'pgsql') {
			return new ArgumentError('Invalid type ' . $type);
		}
		if ($type === 'mysql') {
			$dbs = $this->list_databases();
		} else {
			$dbs = $this->list_pgsql_databases();
		}
		if (!\in_array($db, $dbs, true)) {
			return error("invalid database `%s'", $db);
		}

		if (!\in_array($extension, array('gz', 'bz', 'zip', 'none'))) {
			return error("unrecognized extension `%s'", $extension);
		} else if (!ctype_digit((string)$span) || !ctype_digit((string)$preserve)) {
			return error('non-numeric type for day span/preservation amount');
		} else if ($email && !preg_match(Regex::EMAIL, $email)) {
			return error('invalid e-mail address');
		}
		$dbconn = MySQL::initialize();
		$q = $dbconn->query("UPDATE
                                    sql_dumps
                                SET
                                    extension = '" . $extension . "',
                                    email = " . ($email ? $dbconn->escape($email) : 'NULL') . ",
                                    day_span = " . (!$span ? 'NULL' : $span) . ',
                                    preserve = ' . $preserve . ',
                                    next_date = ' . ($span > 1 ? 'DATE_ADD(NOW(), INTERVAL ".$span." DAY)' : 'NOW()') . "
                                WHERE
                                        db_type = '" . $type . "'
                                    AND
                                        db_name = '" . $db . "'
                                    AND
                                        site_id = " . $this->site_id . ';');

		return $dbconn->affected_rows() > 0;

	}

	/**
	 * Wrapper to list backups for db type
	 *
	 * @param string $type
	 * @return ArgumentError|array
	 */
	protected function list_backups_real(string $type)
	{
		if ($type !== 'pgsql' && $type !== 'mysql') {
			fatal("Invalid database type `%s'" . $type);
		}

		$backups = array();
		$fn = "${type}_list_databases";
		foreach ($this->$fn() as $db) {
			$task = $this->get_backup_config_real($type, $db);
			if (!$task) {
				continue;
			}
			$backups[$db] = $task;

		}

		return $backups;
	}

	protected function get_backup_config_real($type, $db)
	{
		if ($type !== 'mysql' && $type !== 'pgsql') {
			return error($type . ': unknown type');
		}
		$dbconn = MySQL::initialize();
		$q = $dbconn->query("SELECT day_span,
                                             preserve,
                                             UNIX_TIMESTAMP(next_date) AS next_date,
                                             extension,
                                             email
                                     FROM sql_dumps
                                     WHERE db_type = '" . $type . "'
                                     AND site_id = " . $this->site_id . "
                                     AND db_name = '" . $db . "'");

		if ($q->num_rows < 1) {
			return false;
		}

		$row = $q->fetch_object();

		return array(
			'span'      => $row->day_span,
			'hold'      => $row->preserve,
			'next'      => $row->next_date,
			'extension' => $row->extension,
			'email'     => $row->email
		);
	}

	protected function delete_backup_real($type, $db)
	{
		if ($type !== 'mysql' && $type !== 'pgsql') {
			return error('Invalid type ' . $type);
		}
		$dbconn = \MySQL::initialize();
		$rs = $dbconn->query('DELETE FROM sql_dumps WHERE site_id = ' . $this->site_id
			. " AND db_type = '" . $type . "' AND db_name = '"
			. $dbconn->escape_string($db) . "';"
		);

		return $rs && $dbconn->affected_rows > 0;
	}

	protected function _escape(string $str)
	{
		return str_replace('_', '\_', $str);
	}

	protected function _preImport($file, &$unlink)
	{
		$realfile = $this->file_make_path($file);
		if (!file_exists($realfile)) {
			return error("file `%s' does not exist", $file);
		}

		if (!$this->file_is_compressed($file)) {
			return $realfile;
		}
		$tmpdir = tempnam($this->domain_fs_path() . '/tmp', 'db');
		unlink($tmpdir);
		$ret = $this->file_extract($file, $this->file_unmake_path($tmpdir), true);
		if (!$ret) {
			return error("failed to extract archive `%s'", $file);
		}
		// shouldn't be necessary to kill open file handles; import is non-privileged user
		// definer remap must match user on account
		Filesystem::chogp($tmpdir, 'root', 'root', 0600);
		$files = Filesystem::readdir($tmpdir, static function ($f) use ($tmpdir) {
			return filetype($tmpdir .'/' . $f) === 'file' ? $f : null;
		});
		if (!$files) {
			return error('empty archive');
		}
		if (\count($files) > 1) {
			warn("Multiple files found in archive - using first found: %s", $files[0]);
		}
		$unlink = $this->file_unmake_path($tmpdir);

		return $tmpdir . '/' . array_pop($files);

	}

	protected function _postImport($file)
	{
		if (!\is_null($file)) {
			return $this->file_delete($file, true);
		}

		return true;
	}
}