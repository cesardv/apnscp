<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

namespace HTTP;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Str;
use Opcenter\Net\Iface;
use Opcenter\Net\Ip4;
use Opcenter\Net\Ip6;
use Opcenter\Net\IpCommon;

/**
 * Class SelfReferential
 *
 * Perform a check on the domain taking into account DNS mappings
 *
 * @package HTTP
 */
class SelfReferential {
	use \AccountInfoTrait;


	// @var bool result of hairpin check
	static $hairpinCheck;

	// @var Client
	private $client;
	// @var array
	private $options = [];

	public function __construct(string $domain, string $ip = null)
	{
		$baseUri = (false === strpos($domain, '://') ? 'http://' : '') . $domain;
		$this->options = [
			RequestOptions::ALLOW_REDIRECTS => [
				'max' => 2,
			],
			RequestOptions::HEADERS         => [
				'User-Agent' => PANEL_BRAND . ' ' . APNSCP_VERSION . ' Self-Referential Check'
			],
			RequestOptions::VERIFY          => false,
			RequestOptions::VERSION         => 1.1,
			RequestOptions::CONNECT_TIMEOUT => 15
		];

		if (!$ip || ($ip && !Iface::bound($ip) && !$this->hairpinCheck($domain))) {
			if ($this->getAuthContext()->level & (PRIVILEGE_SITE | PRIVILEGE_USER)) {
				$afi = \apnscpFunctionInterceptor::factory($this->getAuthContext());
				$ip = $afi->common_get_ip_address() ?: $afi->common_get_ip6_address();
			} else if (!$this->hairpinCheck($domain)) {
				// admin, hairpin check failed - default to first routed IP
				$ip = [Iface::routed()];
			}
			if (!$ip) {
				// if hairpin fails/disabled, fallback to configured IP
				$ip = [Ip4::my_ip() ?? Ip6::my_ip()];
			}
			if (!$ip) {
				fatal("Eep! Possible bug! Unable to detect internal IP address for `%s'", $domain);
			}
			$ip = $ip[0];
		}

		$baseUri = Str::replaceFirst($domain, IpCommon::format($ip), $baseUri);
		$this->setOption(RequestOptions::HEADERS . '.Host', $domain);

		$this->client = new Client([
			'base_uri' => $baseUri
		]);
	}

	/**
	 * Determine if network supports hairpin requests
	 *
	 * @param $domain
	 * @return bool whether hairpin is supported (and no workaround required)
	 */
	private function hairpinCheck($domain): bool
	{
		if (isset(self::$hairpinCheck)) {
			return self::$hairpinCheck;
		}

		if (DNS_HAIRPIN === 1) {
			// a request will successfully return back to the device that made the request
			return true;
		} else if (DNS_HAIRPIN === 0) {
			// router does not support hairpin requests
			return false;
		} else if (!DNS_PROXY_IP4 && !DNS_PROXY_IP6) {
			// IP is not NAT'd
			return true;
		}

		$ip = \Net_Gethost::gethostbyname_t($domain, 2500);
		if (!$ip) {
			// IP cannot be resolved
			return true;
		}

		if (DNS_PROXY_IP4 && $ip !== DNS_PROXY_IP4) {
			// IP doesn't match proxied IP
			return true;
		}

		if (DNS_PROXY_IP6 && $ip !== DNS_PROXY_IP6) {
			// IP doesn't match proxied IP6
			return true;
		}

		if (!\in_array($ip, $this->getServiceValue('dns', 'proxyaddr', [$ip]), true)) {
			// IP irresolvable or does not match proxy address
			return true;
		}
		if (!\in_array($ip, $this->getServiceValue('dns', 'proxy6addr', [$ip]), true)) {
			// IP irresolvable or does not match proxy address
			return true;
		}
		$challenge = uniqid('test-123', false);
		$path = \Web_Module::MAIN_DOC_ROOT . '/' . $challenge;
		file_put_contents($path, $challenge);
		try {
			$contents = @file_get_contents('http://' . $ip . '/' . $challenge, false, stream_context_create([
				'ssl' => [
					'verify_peer' => false,
					'verify_peer_name' => false
				],
				'http' => [
					'timeout' => 15,
					'protocol_version' => '1.1',
					'user_agent' => PANEL_BRAND . ' ' . APNSCP_VERSION . ' Hairpin Check',
				]
			]));
			return self::$hairpinCheck = $challenge === $contents;
		} catch (\Throwable $e) {
			return false;
		} finally {
			file_exists($path) && unlink($path);
		}
	}

	/**
	 * Set option
	 *
	 * @param string $opt
	 * @param        $value
	 * @return SelfReferential
	 */
	public function setOption(string $opt, $value): self
	{
		array_set($this->options, $opt, $value);
		return $this;
	}

	public function get(string $uri = '/')
	{
		return $this->client->request('GET', $uri, $this->options);
	}

	public function post(string $uri, array $params = [])
	{
		$opts = [
			'form_params' => $params
			] + $this->options;
		return $this->client->request('POST', $uri, $opts);
	}
}
