<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, April 2018
	 */

	use Opcenter\Net\Fail2ban;
	use Opcenter\Net\Firewall;
	use Opcenter\Net\Firewall\Delegated;
	use Opcenter\Net\Firewall\Ipset;
	use Opcenter\Net\Ip6;
	use Opcenter\Net\IpCommon;
	use Opcenter\Service\Contracts\DefaultNullable;

	/**
	 * Class Rampart_Module
	 *
	 * Integrates into fail2ban. Provides short-term and long-term blocks
	 */
	class Rampart_Module extends Module_Skeleton
	{
		public const FAIL2BAN_IPT_PREFIX = RAMPART_PREFIX;
		public const FAIL2BAN_DRIVER = RAMPART_DRIVER;
		public const FAIL2BAN_CACHE_KEY = 'f2b';
		// @var array list of permanent lists
		public const PLIST_NAMES = [Delegated::IPSET_NAME, 'blacklist', Delegated::IPSET_NAME . '6', 'blacklist6'];
		protected $confMapping;

		protected $exportedFunctions = [
			'*'                  => PRIVILEGE_ADMIN | PRIVILEGE_SITE,
			'ban'                => PRIVILEGE_ADMIN,
			'blacklist'          => PRIVILEGE_ADMIN,
			'whitelist'          => PRIVILEGE_ADMIN,
			'get_plist'          => PRIVILEGE_ADMIN,
			'get_jail_entries'   => PRIVILEGE_ADMIN,
			'flush'              => PRIVILEGE_ADMIN,
			'get_delegated_list' => PRIVILEGE_SITE,
			'can_delegate'       => PRIVILEGE_SITE,
			'max_delegations'    => PRIVILEGE_SITE,
			'temp'               => PRIVILEGE_SITE
		];

		public function __construct()
		{
			parent::__construct();
			if (static::FAIL2BAN_DRIVER !== 'ipset') {
				$this->exportedFunctions = [
					'whitelist' => PRIVILEGE_NONE,
					'blacklist' => PRIVILEGE_NONE
				] + $this->exportedFunctions;
			} else if (version_compare(APNSCP_VERSION, '3.1', '>=')) {
				$this->exportedFunctions['whitelist'] |= PRIVILEGE_SITE;
			}
		}

		/**
		 * Authenticated client IP or $ip is banned
		 *
		 * @param string|null $ip
		 * @param string|null $jail optional jail to check
		 * @return bool
		 */
		public function is_banned(string $ip = null, string $jail = null): bool
		{
			if (!IS_CLI) {
				return $this->query('rampart_is_banned', $ip, $jail);
			}
			if (false === ($ip = $this->checkInput($ip, $jail))) {
				return false;
			}

			return count($this->getMatches($ip, $jail)) > 0;
		}

		/**
		 * Get services for which IP is banned
		 *
		 * @param string $ip
		 * @return array
		 */
		public function banned_services(string $ip = null): array
		{
			if (!IS_CLI) {
				return $this->query('rampart_banned_services', $ip);
			}
			if (false === ($ip = $this->checkInput($ip))) {
				return false;
			}

			return $this->getMatches($ip);
		}

		/**
		 * Get reason for ban
		 *
		 * @param string|null $ip
		 * @param string|null $jail
		 * @return string|null
		 */
		public function get_reason(string $ip = null, string $jail = null): ?string
		{
			if (!IS_CLI) {
				return $this->query('rampart_get_reason', $ip, $jail);
			}
			if (!RAMPART_SHOW_REASON && !($this->permission_level & PRIVILEGE_ADMIN)) {
				return null;
			}

			if (false === ($ip = $this->checkInput($ip))) {
				return null;
			}

			$matches = $this->getMatches($ip);

			$db = Fail2ban::getDatabaseHandler();
			$fragment = "ip = '$ip'";
			if ($jail) {
				$fragment .= ' AND jail = ' . $db->quote($jail) . '';
			}

			$query = "SELECT data FROM bans WHERE $fragment ORDER BY timeofban DESC LIMIT 1";
			$rs = $db->query($query);
			if (!$rs) {
				return null;
			}
			$data = json_decode(array_get($rs->fetch(\PDO::FETCH_ASSOC), 'data', 'null'), true);
			$last = array_get($data, 'matches', []);
			return  $data ? array_pop($last) : null;
		}

		/**
		 * Perform permission validation and IP transformation
		 *
		 * @param string|null $ip
		 * @param string|null $jail
		 * @return false|string
		 */
		protected function checkInput(string $ip = null, string $jail = null)
		{
			if ($this->permission_level & PRIVILEGE_SITE) {
				if ($ip) {
					return error('IP address may not be specified if site admin');
				}
				if ($jail) {
					return error('jail may not be specified if site admin');
				}
				if (!$this->enabled()) {
					return error('user may not remove block');
				}
			}
			if (!$ip) {
				$ip = \Auth::client_ip();
			}
			if (!$ip) {
				report('Odd?' . var_export($_ENV, true));
			}
			if (false === inet_pton($ip)) {
				return error("invalid IP address `%s'", $ip);
			}

			return $ip;
		}

		/**
		 * Get matching rules where IP is banned
		 *
		 * @param string      $ip
		 * @param string|null $jail optional jail to restrict check
		 * @return array jails banned
		 */
		private function getMatches(string $ip, string $jail = null): array
		{
			$banned = [];
			if ($jail) {
				$jail = static::FAIL2BAN_IPT_PREFIX . $jail;
			}
			$matches = (new Firewall)->getEntriesFromChain($jail);
			if ($jail) {
				$matches = [$jail => $matches];
			}

			foreach ($matches as $chain => $records) {
				foreach ($records as $record) {
					if ($record->getHost() === $ip && $record->isBlocked()) {
						$banned[$chain] = 1;
					}
				}
			}

			return array_keys($banned);
		}

		/**
		 * Disallow an IP address from service
		 *
		 * @param string $ip
		 * @param string $jail
		 * @return bool
		 */
		public function ban(string $ip, string $jail): bool
		{
			if (!IS_CLI) {
				return $this->query('rampart_ban', $ip, $jail);
			}
			if (!in_array($jail, $this->get_jails(), true)) {
				return error("Unknown jail `%s'", $jail);
			}
			if (false === $this->checkInput($ip, $jail)) {
				return false;
			}
			$ret = \Util_Process_Safe::exec('fail2ban-client set %s banip %s', $jail, $ip);

			return $ret['success'];
		}

		/**
		 * Temporarily whitelist an IP
		 *
		 * @param string $ip
		 * @param int    $duration
		 * @return bool
		 */
		public function temp(string $ip = null, int $duration = 7200): bool
		{
			if ($duration < 1) {
				return error('Non-sensical usage of %s: %s', 'duration', $duration);
			}
			$ip = $ip ?? \Auth::client_ip();
			// fetch whitelist
			if (\in_array($ip, $this->get_delegated_list(), true)) {
				return true;
			}

			if (!$this->whitelist($ip, 'add')) {
				return false;
			}

			return $this->pman_schedule_api_cmd('rampart_whitelist', [$ip, 'remove'], "now + ${duration} seconds");
		}

		/**
		 * Whitelist IP acccess
		 *
		 * @param string|null $ip whitelist named or present IP
		 * @param string $mode
		 * @return bool
		 */
		public function whitelist(string $ip = null, string $mode = 'add'): bool
		{
			if (!IS_CLI) {
				if (!$this->query('rampart_whitelist', $ip, $mode)) {
					return false;
				}
				$this->getAuthContext()->reset();
				return true;
			}

			if (!$ip) {
				$ip = \Auth::client_ip();
			}

			if ($mode !== 'add' && $mode !== 'remove' && $mode !== 'delete') {
				return error('Unknown whitelist operation %s', $mode);
			}
			$fn = $mode === 'add' ? 'add' : 'remove';

			if ( !($this->permission_level & PRIVILEGE_SITE) ) {
				if (!$this->ipsetWrapper('whitelist', $ip, $mode)) {
					return false;
				}

				if (Delegated::IPSET_NAME !== 'whitelist') {
					// ignorelist doesn't require delegation locking
					return true;
				}

				// use "site0" for admin
				return $fn === 'add' ? Delegated::markDelegated($ip, 'site0') :
					Delegated::releaseDelegation($ip, 'site0');
			}

			if ($this->auth_is_demo()) {
				return error('Demo accounts cannot alter whitelist');
			} else if (!version_compare(APNSCP_VERSION, '3.1', '>=')) {
				return error('Delegated whitelisting supported on v8+ platforms');
			} else if (false !== strpos($ip, '/')) {
				return error('Delegated whitelisting cannot accept ranges');
			} else if (!$this->addressValid($ip)) {
				return false;
			}

			return Delegated::instantiateContexted($this->getAuthContext())->$fn($ip);

		}

		/**
		 * Account supports delegation abilities
		 *
		 * @return bool
		 */
		public function can_delegate(): bool
		{
			return Delegated::instantiateContexted($this->getAuthContext())->permitted();
		}

		/**
		 * Get maximum number of delegated entries
		 *
		 * @return int|null
		 */
		public function max_delegations(): ?int
		{
			if (!$this->can_delegate()) {
				return 0;
			}
			$val = $this->getServiceValue('rampart', 'max', RAMPART_DELEGATED_WHITELIST);
			if ($val === DefaultNullable::NULLABLE_MARKER) {
				/**
				 * (new ConfigurationContext('rampart', new SiteConfiguration('')))->getValidatorClass('max')->getDefault()
				 * is a little slow, let's bypass this logic and just pull from config.ini
				 */
				$val = RAMPART_DELEGATED_WHITELIST;
			}
			return $val;
		}

		/**
		 * Rampart service enabled
		 *
		 * @return bool
		 */
		public function enabled(): bool
		{
			return (bool)$this->getConfig('rampart', 'enabled', true);
		}

		/**
		 * Return a list of delegated whitelist entries
		 *
		 * @return array
		 */
		public function get_delegated_list(): array
		{
			if (!version_compare(APNSCP_VERSION, '3.1', '>=')) {
				return [];
			}
			// wrapper for get_plist
			return Delegated::instantiateContexted($this->getAuthContext())->get();
		}

		/**
		 * ipset wrapper
		 *
		 * @param string $set set name
		 * @param string $address ip address or CIDR
		 * @param string $mode
		 * @return bool
		 */
		private function ipsetWrapper(string $set, string $address, string $mode): bool {
			if (!$this->addressValid($address)) {
				return false;
			} else if (static::FAIL2BAN_DRIVER !== 'ipset') {
				return error('ipset is not configured as Rampart driver');
			}

			if (($set === 'ignorelist' || $set === 'whitelist' || $set === 'blacklist') && Ip6::valid($address)) {
				$set .= '6';
			}
			if ($mode === 'delete') {
				$mode = 'remove';
			}

			if ($mode !== 'add' && $mode !== 'remove') {
				return error("Unknown ipset wrapper mode `%s'", $mode);
			}

			return Ipset::$mode($set, $address);
		}

		/**
		 * Get permanent list entries
		 *
		 * @param string $list "blacklist" or "whitelist"
		 * @return array|bool
		 */
		public function get_plist(string $list)
		{
			if (!IS_CLI) {
				return $this->query('rampart_get_plist', $list);
			}
			$valid = static::PLIST_NAMES;
			if ($this->permission_level & PRIVILEGE_ADMIN) {
				$valid = array_merge($valid, ['whitelist', 'whitelist6']);
			}
			if (!\in_array($list, $valid, true)) {
				return error("Unknown permanent list `%s'", $list);
			}
			return array_column(Ipset::getSetMembers($list), 'host');
		}

		/**
		 * Get jail entries
		 *
		 * @param string $jail
		 * @return array|bool array or false on failure
		 */
		public function get_jail_entries(?string $jail)
		{
			if (!IS_CLI) {
				return $this->query('rampart_get_jail_entries', $jail);
			}
			$list = static::FAIL2BAN_IPT_PREFIX . $jail;
			if (!$jail) {
				$items = (array)(new Firewall())->getEntriesFromChain();
				return json_decode(json_encode($items), true);
			}
			if (!\in_array($jail, $this->get_jails(), true)) {
				return error("Unknown jail `%s'", $jail);
			}

			return array_column((array)(new Firewall())->getEntriesFromChain($list), 'host');
		}

		/**
		 * Flush jails
		 *
		 * @param string|null $jail
		 * @return bool
		 */
		public function flush(string $jail = null): bool
		{
			if (!IS_CLI) {
				return $this->query('rampart_flush', $jail);
			}

			if ($jail && !\in_array($jail, $this->get_jails(), true)) {
				return error("Unknown jail `%s'", $jail);
			} else if (!$jail) {
				$ret = \Util_Process::exec('fail2ban-client unban --all');
				return $ret['success'];
			}

			foreach ((array)$jail as $j) {
				$ret = \Util_Process::exec('fail2ban-client reload --unban %s', $j);
				if (!$ret['success']) {
					warn("Failed to empty jail `%s'", $jail);
				}
			}
			return true;
		}

		/**
		 * Permanently block access
		 *
		 * @param string $ip
		 * @param string $mode add, remove, or set
		 * @return bool
		 */
		public function blacklist(string $ip, string $mode = 'add'): bool
		{
			if (!IS_CLI) {
				return $this->query('rampart_blacklist', $ip, $mode);
			}

			return $this->ipsetWrapper('blacklist', $ip, $mode);

		}

		/**
		 * Specified address is valid address or range
		 *
		 * @param string $address
		 * @return bool
		 */
		private function addressValid(string $address): bool
		{
			$class = '';
			if (!IpCommon::supported($address, $class)) {
				return error('Requested address family %s disabled on server', $class);
			}
			if (!IpCommon::valid($address)) {
				return error("Address `%s' is invalid %s",
					$address, $class
				);
			}

			return true;
		}

		/**
		 * Get active jails
		 *
		 * @return array
		 */
		public function get_jails(): array
		{
			static $jails;
			if ($jails === null) {
				$cache = \Cache_Global::spawn($this->getAuthContext());
				if (false === ($jails = $cache->get('rampart.jails'))) {
					if (!IS_CLI) {
						return $this->query('rampart_get_jails');
					}
					$jails = Fail2ban::getJails();
					$cache->set('rampart.jails', $jails, 1800);
				}
			}

			return $jails ?? [];
		}

		/**
		 * Unban an IP address
		 *
		 * @param string|null $ip
		 * @param string|null $jail optional jail to remove
		 * @return bool
		 */
		public function unban(string $ip = null, string $jail = null): bool
		{
			if (!IS_CLI) {
				return $this->query('rampart_unban', $ip, $jail);
			}
			if ($this->auth_is_demo()) {
				return error('cannot unban IP address in demo mode');
			}
			if (false === ($ip = $this->checkInput($ip, $jail))) {
				return false;
			}
			foreach ($this->getMatches($ip, $jail) as $chain) {
				if (!$jail = $this->chain2Jail($chain)) {
					warn("Address blocked in `%s' but not recognized Rampart jail - cannot unban %s", $chain, $ip);
					continue;
				}

				$ret = \Util_Process_Safe::exec('fail2ban-client set %s unbanip %s', $jail, $ip);
				if ($ret['success']) {
					info("Unbanned `%s' from jail `%s'", $ip, $jail);
				} else {
					warn("Failed to unban `%s' from jail `%s'", $ip, $jail);
				}
			}

			return true;
		}

		/**
		 * Get ban counts for each jail
		 *
		 * @param int        $begin begin ts inclusive
		 * @param int|null   $end   end ts exclusive
		 * @param array|null $jails restrict to jails
		 * @return array
		 */
		public function bans_since(int $begin, int $end = null, array $jails = null): array
		{
			if (!IS_CLI) {
				return $this->query('rampart_bans_since', $begin, $end, $jails);
			}
			if ($begin < 0 || $end && $end < 0) {
				error('Invalid timestamp');
				return [];
			} else if ($end && $begin > $end) {
				return error('Begin TS may not be newer than end TS');
			}
			$fragment = "timeofban >= $begin";
			if ($end) {
				$fragment .= " AND timeofban < $end";
			}
			if ($jails) {
				$diff = array_diff($jails, $this->get_jails());
				if ($diff) {
					error('Invalid jails specified: %s', implode(',', $diff));
					return [];
				}
				$fragment .= " AND jail IN ('" . implode("','", $jails) . "')";
			}
			$query = "SELECT count(*) AS count, jail FROM bans WHERE $fragment GROUP BY (jail)";
			$db = Fail2ban::getDatabaseHandler();
			$built = [];
			foreach ($db->query($query)->fetchAll(\PDO::FETCH_ASSOC) as $rec) {
				$built[$rec['jail']] = (int)$rec['count'];
			}

			return $built;
		}

		/**
		 * Convert an iptables rule into a fail2ban jail
		 *
		 * @param string $chain iptables chain
		 * @return null|string
		 */
		protected function chain2Jail(string $chain): ?string
		{
			if (isset($this->confMapping[$chain])) {
				return $this->confMapping[$chain];
			}
			$jails = $this->getJailConfig();
			$chain = ' ' . $chain . ' ';
			foreach ($jails as $jail => $actions) {
				foreach ($actions as $action) {
					if (false !== strpos($action, $chain)) {
						$this->confMapping[$chain] = $jail;

						return $jail;
					}
				}
			}

			return null;
		}

		protected function getJailConfig(): ?array
		{
			$cache = \Cache_Global::spawn();
			$key = static::FAIL2BAN_CACHE_KEY . '.jail-config';
			if (false === ($jails = $cache->get($key))) {
				if (null === ($jails = Fail2ban::map())) {
					warn('error retrieving fail2ban jail configuration');

					return [];
				}
				$cache->set($key, $jails, 86400);
			}

			if (is_string($jails)) {
				report($jails);
			}

			return $jails;
		}

		public function _cron(Cronus $cron)
		{
			if (!TELEMETRY_ENABLED) {
				return;
			}

			$rampartMetric = new \Daphnie\Metrics\Rampart();
			$lastRun = 0;
			$attrCheck = $rampartMetric->mutate('recidive')->metricAsAttribute();
			if ($this->telemetry_has($attrCheck)) {
				$lastRun = array_get($this->telemetry_get($attrCheck, null), 'ts', 0);
			}

			$attrs = \Daphnie\Metrics\Rampart::getAttributeMap();
			$jails = array_keys($attrs);
			$bans = $this->bans_since($lastRun, time(), $jails);

			$collector = new \Daphnie\Collector(PostgreSQL::pdo());
			foreach ($jails as $jail) {
				$name = $rampartMetric->mutate($jail);
				$collector->add($name->metricAsAttribute(), null, array_get($bans, $jail, 0));
			}
		}

		public function _housekeeping()
		{
			$this->getJailConfig();
		}
	}