<?php
	declare(strict_types=1);
	/**
	 *  +------------------------------------------------------------+
	 *  | apnscp                                                     |
	 *  +------------------------------------------------------------+
	 *  | Copyright (c) Apis Networks                                |
	 *  +------------------------------------------------------------+
	 *  | Licensed under Artistic License 2.0                        |
	 *  +------------------------------------------------------------+
	 *  | Author: Matt Saladna (msaladna@apisnetworks.com)           |
	 *  +------------------------------------------------------------+
	 */

	use AcmePhp\Core\Exception\AcmeCoreServerException;
	use AcmePhp\Core\Protocol\AuthorizationChallenge;
	use Module\Support\Letsencrypt;
	use Opcenter\Crypto\Letsencrypt as LetsencryptAlias;

	/**
	 * Let's Encrypt integration utilities
	 *
	 * @author Matt Saladna <matt@apisnetworks.com>
	 */
	class Letsencrypt_Module extends Letsencrypt implements \Opcenter\Contracts\Hookable
	{
		const DEPENDENCY_MAP = [
			'ssl'
		];
		// @var int max number of SSL bootstrap attempts
		const BOOTSTRAP_ATTEMPTS = LETSENCRYPT_BOOTSTRAP_ATTEMPTS;
		// production
		const LETSENCRYPT_SERVER = 'acme-v02.api.letsencrypt.org/directory';
		// staging
		const LETSENCRYPT_TESTING_SERVER = 'acme-staging-v02.api.letsencrypt.org/directory';
		protected const LE_AUTHORITY_FINGERPRINT = LETSENCRYPT_KEYID;
		protected const LE_STAGING_AUTHORITY_FINGERPRINT = LETSENCRYPT_STAGING_KEYID;
		// include complementary hostname variant, e.g. foo.com + www.foo.com or www.foo.com + foo.com
		const INCLUDE_ALT_FORM = LETSENCRYPT_ALTERNATIVE_FORM;
		// @var int maximum duration to wait for A record resolution
		const DNS_VERIFY_IP_TIMEOUT = 1500;
		protected $activeServer;

		/**
		 * {{{ void __construct(void)
		 *
		 * @ignore
		 */

		public function __construct()
		{
			parent::__construct();

			$this->activeServer = \Opcenter\Crypto\Letsencrypt::activeServer();
			if ($this->supported()) {
				$fns = array(
					'*' => PRIVILEGE_SITE,
					'request'        => PRIVILEGE_SITE|PRIVILEGE_ADMIN,
					'append'         => PRIVILEGE_SITE|PRIVILEGE_ADMIN,
					'renew_expiring' => PRIVILEGE_ADMIN,
					'revoke'         => PRIVILEGE_SITE|PRIVILEGE_ADMIN,
					'challenges'     => PRIVILEGE_SITE|PRIVILEGE_ADMIN,
					'solve'          => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
					'renew'          => PRIVILEGE_SITE|PRIVILEGE_ADMIN
				);
			} else {
				$fns = array(
					'supported' => PRIVILEGE_SITE,
					'permitted' => PRIVILEGE_SITE,
					'is_ca'     => PRIVILEGE_SITE,
					'*'         => PRIVILEGE_NONE
				);
			}
			$this->exportedFunctions = $fns;
		}

		/**
		 * Let's Encrypt is supported on this platform
		 *
		 * @return bool
		 */
		public function supported()
		{
			return true;
		}

		/**
		 * Client may generate a LE certificate
		 *
		 * @return bool
		 */
		public function permitted()
		{
			return $this->supported() && $this->ssl_permitted();
		}

		/**
		 * Let's Encrypt issues bogus certificates
		 *
		 * @return bool
		 */
		public function debug(): bool
		{
			return (bool)LETSENCRYPT_DEBUG;
		}

		/**
		 * Renew Let's Encrypt SSL
		 *
		 * @param bool $verifyip perform IP verification prior to issuance
		 * @return bool
		 */
		public function renew(bool $verifyip = null)
		{
			if (null === $verifyip) {
				$verifyip = (bool)array_get(\Preferences::factory($this->getAuthContext()), LetsencryptAlias\Preferences::VERIFY_IP, true);
			}
			if ($this->permission_level & PRIVILEGE_SITE && $this->auth_is_inactive()) {
				return error("account `%s' is inactive - not renewing SSL", $this->domain);
			}

			$cns = $this->getSanFromCertificate();
			if ($cns === []) {
				return error('no certificates installed on account');
			} else if ($cns === null) {
				return warn("certificate for `%s' is not provided by LE", $this->domain);
			}
			$ret = $this->request($cns, $verifyip);
			if (null === $ret) {
				// case in which a request is processed OK, but
				// there are no valid hostnames on the account to renew
				return warn('request failed, lack of valid hostnames to renew');
			}
			if (!$ret) {
				return error('failed to renew certificate');
			}

			return info('successfully renewed certificate for 90 days');
		}

		/**
		 * Renew all expiring certificates
		 *
		 * @return void
		 */
		public function renew_expiring(): void {
			$this->renewExpiringCertificates();

			if (!$this->systemNeedsIssuance()) {
				return;
			}

			// issue system certificate on bootstrap or hostname change
			$cns = [SERVER_NAME];
			if (SERVER_NAME !== ($name = gethostname())) {
				$cns[] = $name;
			}
			if (LETSENCRYPT_ADDITIONAL_CERTS) {
				$cns = array_merge($cns, LETSENCRYPT_ADDITIONAL_CERTS);
			}
			if ($this->requestReal($cns, LetsencryptAlias::SYSCERT_NAME)) {
				$this->installSystemCertificate();
			}
		}

		/**
		 * Get ACME challenges associated with hostnames
		 *
		 * "payload" must be converted to sha256 hash when entering as a TXT record
		 *
		 * @param $cnames
		 * @return array
		 */
		public function challenges($hostnames): array
		{
			$hostnames = (array)$hostnames;
			$sans = [];
			foreach ($hostnames as $host) {
				$chk = $host;
				if (0 === strncmp($host, '*.', 2)) {
					$chk = substr($chk, 2);
				}
				if (($this->permission_level & PRIVILEGE_SITE) && !$this->web_split_host($chk)) {
					error("Invalid hostname `%s'", $chk);
					continue;
				}
				$sans[$host] = null;
			}
			$dispatch = LetsencryptAlias\AcmeDispatcher::instantiateContexted($this->getAuthContext(), [$this->site]);
			if ( !($challenges = $dispatch->challenges(array_keys($sans))) ) {
				// exception generated
				return [];
			}
			foreach (array_get($challenges->toArray(), 'authorizationsChallenges', []) as $domain => $challengeTypes) {
				$sans[$domain] = array_map(static function (AuthorizationChallenge $c) {
					return $c->toArray();
				}, $challengeTypes);
			}

			return $sans;
		}

		/**
		 * Solve pending challenges
		 *
		 * @param string|array $hostname array of hostnames or map of hostname: solver
		 * @param string|null $solver type of "http", "dns", or "alpn"
		 * @return array
		 * @throws ArgumentError
		 */
		public function solve($hostname, string $solver = null): bool
		{
			$dispatch = LetsencryptAlias\AcmeDispatcher::instantiateContexted($this->getAuthContext(), [$this->site]);
			$dispatch->setStagingOnly(true);
			if ($solver) {
				array_fill_keys((array)$hostname, $solver);
			} else if (!\is_array($hostname) || isset($hostname[0])) {
				return error('$hostname parameter is not a map of hostnames => solvers');
			}
			$order = $dispatch->challenges((array)array_keys($hostname));

			$challengeSet = $order->getAuthorizationsChallenges();
			foreach ($challengeSet as $host => &$challenges) {
				foreach ($challenges as &$challenge) {
					if (!isset($hostname[$host]) || 0 !== strpos($challenge->getType(), $hostname[$host])) {
						$challenge = null;
						continue;
					}
				}
				unset($challenge);
				$challenges = array_filter($challenges);
			}
			unset($challenges);
			$revised = new \AcmePhp\Core\Protocol\CertificateOrder($challengeSet);

			return null === $dispatch->solve($revised);
		}

		/**
		 * Request a Let's Encrypt certificate for the given common names
		 *
		 * Because there is no unreasonable limit on SANs, a www and non-www
		 * variant for each CN will be generated
		 *
		 * @param array|string $cnames   list of hosts
		 * @param bool         $verifyip verify IP matches account before issuing
		 * @param bool         $strict   loss of any hostname from certificate causes operation to fail
		 * @return bool|null
		 */
		public function request($cnames, ?bool $verifyip = null, ?bool $strict = null): ?bool
		{
			// allow CLI debugging, job runner drops only effective uid
			if (posix_geteuid() && !IS_CLI) {
				return $this->query('letsencrypt_request', $cnames, $verifyip, $strict);
			}

			if (null === $verifyip) {
				$verifyip = (bool)array_get(\Preferences::factory($this->getAuthContext()),
					LetsencryptAlias\Preferences::VERIFY_IP, LETSENCRYPT_VERIFY_IP);
			}

			if (null === $strict) {
				$strict = (bool)array_get(\Preferences::factory($this->getAuthContext()),
					LetsencryptAlias\Preferences::SENSITIVITY, LETSENCRYPT_STRICT_MODE);
			}

			$cnreq = array();
			if ($this->permission_level & PRIVILEGE_ADMIN) {
				// admin works differently, check configured certificates versus what is provided
				// request behavior overrides certificate set, which requires updating
				// @TODO update for consistency, WIP for debugging
				return $this->requestReal((array)$cnames, LetsencryptAlias::SYSCERT_NAME, $strict) && $this->_moveCertificates(LetsencryptAlias::SYSCERT_NAME);
			}
			$myip = ($this->permission_level & PRIVILEGE_ADMIN) ? \Opcenter\Net\Ip4::my_ip() : $this->dns_get_public_ip();
			// @TODO normalize certificates, prune duplicates based on WC
			foreach ((array)$cnames as $c) {
				$isWildcard = false;
				if (!is_string($c)) {
					error('Skipping garbled input - hostname not presented as string, is %s', gettype($c));
				}
				if (0 === strncmp($c, '*.', 2)) {
					$c = substr($c, 2);
					$isWildcard = true;
				}
				$c = $this->web_split_host($c);
				$domain = $c['domain'];
				$subdomain = $c['subdomain'];

				if (!$this->web_domain_exists($domain)) {
					error("cannot register lets encrypt: domain `%s' not a valid domain on this account",
						$domain);
					if ($strict) {
						return false;
					}
					continue;
				}

				$host = ltrim($subdomain . '.' . $domain, '.');
				if (!preg_match(Regex::HTTP_HOST, $host)) {
					error("invalid server name `%s' specified", $c);
					if ($strict) {
						return false;
					}
					continue;
				}

				if (LETSENCRYPT_VERIFY_IP && $verifyip && self::INCLUDE_ALT_FORM && !$isWildcard) {
					$altform = null;
					if (0 === strncmp($host, 'www.', 4)) {
						// add www.example.com if example.com given
						$altform = 'www.' . $host;
					} else {
						// add example.com if www.example.com given
						$altform = substr($host, 4);
					}
					if ($this->_verifyIP($altform, (array)$myip)) {
						$cnreq[] = $altform;
					} else if ($strict) {
						return error("Domain `%s' would be dropped from renewal", $altform);
					} else {
						info("skipping alternative hostname form `%s', IP does not resolve to `%s'",
							$altform, $myip);
					}
				}

				if (LETSENCRYPT_VERIFY_IP && $verifyip && !$this->_verifyIP($host, (array)$myip)) {
					$msg = [
						"hostname `%s' IP `%s' doesn't match hosting IP `%s', "
						. '%s request',
						$host,
						$this->dns_gethostbyname_t($host, static::DNS_VERIFY_IP_TIMEOUT),
						$myip,
						$strict ? 'aborting' : 'skipping'
					];
					if ($strict) {
						return error(...$msg);
					}
					warn(...$msg);
					continue;
				}
				if ($isWildcard) {
					$host = '*.' . $host;
				}
				$cnreq[] = $host;
			}
			if (!$cnreq) {
				error('no hostnames to register');

				return null;
			}

			$storageMarker = ($this->permission_level & PRIVILEGE_ADMIN) ? LetsencryptAlias::SYSCERT_NAME : $this->site;
			if (! ($ret = $this->requestReal($cnreq, $storageMarker, $strict)) ) {
				return $ret;
			}

			if ($strict && ($hosts = $this->filterMissingHostnames($cnames))) {
				return error('Failed to append hostnames. Hostnames missing from new certificate: %s',
					implode(', ', $hosts)
				);
			}

			info(':letsencrypt_issuance_limit',
				'reminder: only 5 duplicate certificates and ' .
				'50 unique certificates may be issued per week per account'
			);

			return $this->_moveCertificates($storageMarker);
		}

		/**
		 * Verify hostname matches IP
		 *
		 * LE will fail issuance if request fails,
		 * verify the challenge points to this server
		 *
		 * @param $hostname
		 * @param $myip
		 * @return bool
		 */
		private function _verifyIP($hostname, array $myip)
		{
			$ip = null;
			for ($i = 0; $i < 2; $i++) {
				if ($ip = $this->dns_gethostbyname_t($hostname, static::DNS_VERIFY_IP_TIMEOUT)) {
					break;
				}
				warn('DNS resolver failed to return answer in %dms', static::DNS_VERIFY_IP_TIMEOUT);
				usleep(500000);
			}

			if (!$ip) {
				return false;
			}

			foreach ($myip as $chkip) {
				if ($chkip === $ip) {
					return true;
				}
			}
			return false;
		}

		private function _moveCertificates($site)
		{
			if ($site === LetsencryptAlias::SYSCERT_NAME) {
				return $this->installSystemCertificate();
			}
			$files = LetsencryptAlias::getCertificateComponentData($site);
			if (!$files) {
				return false;
			}

			return $this->ssl_install($files['key'], $files['crt'], $files['chain']);
		}

		/**
		 * Certificate is generated by LE
		 *
		 * @param string $crt certificate data
		 * @return bool
		 */
		public function is_ca($crt)
		{
			$cert = $this->ssl_parse_certificate($crt);
			if (!$cert) {
				return error('invalid ssl certificate');
			}

			if (!isset($cert['extensions']['authorityKeyIdentifier'])) {
				return false;
			}
			$authority = $cert['extensions']['authorityKeyIdentifier'];
			$prefix = 'keyid:';
			if (!strncmp($authority, $prefix, strlen($prefix))) {
				$authority = substr($authority, strlen($prefix));
			}
			$authority = trim($authority);

			return in_array($authority, self::LE_AUTHORITY_FINGERPRINT, true) ||
				in_array($authority, self::LE_STAGING_AUTHORITY_FINGERPRINT, true);
		}

		/**
		 * Append hostnames to request in non-destructive manner
		 *
		 * @param      $cnames
		 * @param bool|null $verifyip
		 * @return bool
		 */
		public function append($cnames, bool $verifyip = null): bool
		{
			if (null === $verifyip) {
				$verifyip = (bool)array_get(\Preferences::factory($this->getAuthContext()),
					LetsencryptAlias\Preferences::VERIFY_IP, true);
			}

			$cnames = array_flip((array)$cnames);
			$old = (array)$this->getSanFromCertificate();
			$new = self::filterDomainSet(array_keys($cnames + array_flip($old)));

			if (!array_diff($new, $old)) {
				// no change
				return true;
			}

			return (bool)$this->request($new, $verifyip, true);
		}

		/**
		 * Invalidate issued certificate
		 *
		 * @return bool
		 */
		public function revoke(): bool
		{
			if (!IS_CLI) {
				return $this->query('letsencrypt_revoke');
			}

			if (!$this->certificateIssued()) {
				return error('no certificate issued to revoke');
			}

			$cert = ($this->permission_level & PRIVILEGE_ADMIN) ? LetsencryptAlias::SYSCERT_NAME : $this->site;
			$ret = LetsencryptAlias\AcmeDispatcher::instantiateContexted($this->getAuthContext(), [$cert, $this->activeServer])
				->revoke();

			if (!$ret) {
				return error('revocation failed');
			}

			$this->_deleteAcmeCertificate($cert);

			return true;
		}

		private function _deleteAcmeCertificate($account)
		{
			$acmeDir = LetsencryptAlias::acmeSiteStorageDirectory($account);
			if (!file_exists($acmeDir)) {
				return;
			}
			$dir = opendir($acmeDir);
			while (false !== ($f = readdir($dir))) {
				if ($f === '..' || $f === '.') {
					continue;
				}
				unlink($acmeDir . '/' . $f);

			}
			closedir($dir);
			rmdir($acmeDir);

			return;

		}

		public function exists()
		{
			$path = LetsencryptAlias::acmeSiteStorageDirectory($this->site);

			return file_exists($path);
		}

		/**
		 * Retrieve absolute storage path for site certificate
		 *
		 * @param string $site
		 * @return string
		 */
		public function storage_path(string $site): string
		{
			return LetsencryptAlias::acmeSiteStorageDirectory($site);
		}

		/**
		 * Request a SSL certificate for all domains on the account
		 *
		 * bootstrap verifies IP address before attempt
		 *
		 * @param int|null $attempt attempt counter
		 * @return bool|null boolean on success (or error), null on reschedule
		 */
		public function bootstrap(?int $attempt = null): ?bool
		{
			if ($attempt !== null && ($attempt > 10 || $attempt < 0)) {
				return error('Invalid attempt count provided: %d', $attempt);
			}
			$domains = array_keys($this->web_list_domains());
			$domains += append_config(array_map(
				static function ($domain) {
					return "*.${domain}";
				}, $domains)
			);
			$domains = Letsencrypt::filterDomainSet($domains);
			if (\count($domains) > 100) {
				warn('Hostname count exceeds 100 (%d hostnames). Taking first 100 hostnames', \count($domains));
				$domains = array_slice($domains, 0, 100);
			}
			if ($attempt !== null && $this->request($domains, true)) {
				// @todo send email
				return true;
			}
			// retry or initial attempt
			$delay = 43200;
			if ($attempt === null) {
				$attempt = static::BOOTSTRAP_ATTEMPTS;
				$delay = 0;
			} else if (--$attempt < 0) {
				return error('Failed to bootstrap SSL');
			}
			$er = \Error_Reporter::flush_buffer();
			$job = \Lararia\Jobs\Job::create(
				\Lararia\Jobs\SimpleCommandJob::class,
				$this->getAuthContext(),
				'letsencrypt_bootstrap',
				$attempt
			);
			$job->setTags([$this->site, 'letsencrypt_bootstrap']);
			$job->delayedDispatch($delay);
			\Error_Reporter::merge_buffer($er);
			info('Scheduled letsencrypt:bootstrap job');
			return null;
		}

		public function _housekeeping()
		{
			// Let's Encrypt supported on Luna + Sol only
			if (!$this->supported()) {
				return;
			}
			if (!$this->_registered() && !$this->_register()) {
				return error("failed to register with Let's Encrypt");
			}

			$this->renew_expiring();
		}

		private function _registered()
		{
			$key = str_replace(['/'], '.', $this->activeServer) . '.pem';
			$storageDir = LetsencryptAlias::acmeDataDirectory();

			return file_exists($storageDir . '/accounts/' . $key);
		}

		private function _register($email = null)
		{
			$acctdir = LetsencryptAlias::acmeDataDirectory();
			if (!file_exists($acctdir)) {
				mkdir($acctdir, 0700, true);
			}

			$email = $this->admin_get_email() ?: \Crm_Module::FROM_ADDRESS;

			if (!$email) {
				return error("Cannot register Let's Encrypt without an email address. Run 'cpcmd common_set_email newemail' from command-line.");
			}

			$marker = $this->site ?? Opcenter\Crypto\Letsencrypt::SYSCERT_NAME;
			$ret = LetsencryptAlias\AcmeDispatcher::instantiateContexted($this->getAuthContext(), [$marker, $this->activeServer])
				->register($email);
			if (!$ret) {
				return error("Let's Encrypt registration failed");
			}

			return true;
		}

		public function _edit()
		{
			$conf_new = $this->getAuthContext()->getAccount()->new;
			$conf_cur = $this->getAuthContext()->getAccount()->old;
			$ssl = \Opcenter\SiteConfiguration::getModuleRemap('openssl');
			if (!$conf_new[$ssl]['enabled']) {
				$this->_delete();
			}
		}

		public function _delete()
		{
			$this->_deleteAcmeCertificate($this->site);
		}

		public function _verify_conf(\Opcenter\Service\ConfigurationContext $ctx): bool
		{
			return true;
		}

		public function _create()
		{
			return;
		}

		public function _create_user(string $user)
		{
			return;
		}

		public function _delete_user(string $user)
		{
			return;
		}

		public function _edit_user(string $userold, string $usernew, array $oldpwd)
		{
			return;
		}
	}