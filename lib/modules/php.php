<?php
	declare(strict_types=1);

	/**
	 *  +------------------------------------------------------------+
	 *  | apnscp                                                     |
	 *  +------------------------------------------------------------+
	 *  | Copyright (c) Apis Networks                                |
	 *  +------------------------------------------------------------+
	 *  | Licensed under Artistic License 2.0                        |
	 *  +------------------------------------------------------------+
	 *  | Author: Matt Saladna (msaladna@apisnetworks.com)           |
	 *  +------------------------------------------------------------+
	 */

	use Daphnie\Collector;
	use Daphnie\Metrics\Php as PhpMetrics;
	use Module\Support\Php;
	use Opcenter\Http\Php\Fpm;
	use Opcenter\Http\Php\Fpm\CacheInspector;
	use Opcenter\Http\Php\Fpm\PoolPolicy;

	/**
	 * Class Php_Module
	 *
	 * @package core
	 */
	class Php_Module extends Php implements \Opcenter\Contracts\Hookable
	{
		const COMPOSER_LOCATION = '/usr/share/pear/composer.phar';

		public $exportedFunctions = array(
			'*'       => PRIVILEGE_SITE,
			'version' => PRIVILEGE_ALL
		);

		/**
		 * Install PEAR package
		 *
		 * @param  string $module
		 * @return bool
		 */
		public function install_package($module)
		{
			if (!IS_CLI) {
				return $this->query('php_install_package', $module);
			}


			if (!preg_match('!^[a-zA-Z0-9_-]+$!', $module)) {
				return error($module . ': invalid package name');
			}

			$args = '-d display_errors=0 -d track_errors=1 -d include_path=/usr/local/share/pear:/usr/share/pear';
			if (version_compare(platform_version(), '4.5', '<')) {
				$args .= ' -d disable_functions=ini_set';
			}
			$pearcmd = '/usr/share/pear/pearcmd.php';
			$proc = Util_Process_Tee::watch(new Util_Process_Sudo);
			$proc->log('Installing ' . $module);
			if (file_exists($this->domain_fs_path() . '/usr/local/share/pear/pearcmd.php')) {
				$this->_unsetPearIni();
				$pearcmd = '/usr/local/share/pear/pearcmd.php';
			}

			$status = $proc->exec('php %s %s install -f -o %s',
				$args,
				$pearcmd,
				escapeshellarg($module)
			);

			return $status['success'];
		}

		private function _unsetPearIni()
		{
			$pearfile = $this->domain_fs_path() . '/usr/local/share/pear/PEAR.php';
			if (!file_exists($pearfile)) {
				return false;
			}
			$content = file_get_contents($pearfile, 0, null, 0, 1024);
			$changed = false;
			$pos = strpos($content, 'ini_set');
			if ($pos === false) {
				return false;
			}
			$content = file_get_contents($pearfile);
			file_put_contents($pearfile, str_replace('@ini_set', '// @ini_set', $content));

			return true;
		}

		/**
		 * List PEAR packages installed for account
		 *
		 * Keys-
		 * is_local (bool) : package is local to account
		 * version (double): version number
		 *
		 * @return array
		 */
		public function list_installed_packages()
		{
			if (!IS_CLI) {
				return $this->query('php_list_installed_packages');
			}
			$status = Util_Process::exec('pear list');
			if ($status instanceof Exception) {
				return $status;
			}
			$packages = array();

			$packageSizeSys = preg_match_all('!^(\S+)\s+([0-9,\. ]+)!m', $status['output'], $packageMatches);

			for ($i = 1; $i < $packageSizeSys; $i++) {
				$packages[$packageMatches[1][$i]] = array('is_local' => false, 'version' => $packageMatches[2][$i]);
			}


			$status = Util_Process_Sudo::exec('pear list');
			if ($status instanceof Exception) {
				return $status;
			}
			$packageSize = preg_match_all('!^(\S+)\s+([0-9,\. ]+)!m', $status['output'], $packageMatches);

			for ($i = 1; $i < $packageSize; $i++) {

				$packages[$packageMatches[1][$i]] = array(
					'is_local' => true,
					'version'  => trim($packageMatches[2][$i])
				);
			}
			ksort($packages);

			return $packages;
		}

		/**
		 * string get_pear_description (string)
		 * Fetches the description for a PEAR package
		 *
		 * @param  string $mModule package name
		 * @return string description of the package
		 */

		public function package_description($mModule)
		{
			$packages = $this->list_remote_packages();
			if (!isset($packages[$mModule])) {
				return false;
			}

			return $packages[$mModule]['description'];
		}

		/**
		 * array list_remote_packages (void)
		 * Queries PEAR for all available PEAR packages, analogous to
		 * running pear list-all from the command line.
		 *
		 * @return array Listing of PEAR modules with the following indexes:
		 * versions, description.  :KLUDGE: versions only contains one version
		 * number, the most current on PEAR at this time.  This index is kept for
		 * consistency with the "Package Manager" component of the control panel
		 */
		public function list_remote_packages()
		{
			if (file_exists(TEMP_DIR . '/pear-cache') && ((time() - filemtime(TEMP_DIR . '/pear-cache')) < 86400)) {
				$data = unserialize(file_get_contents(TEMP_DIR . '/pear-cache'));

				return $data;
			}
			$status = Util_Process::exec('/usr/bin/pear list-all');
			if ($status instanceof Exception) {
				return $status;
			}
			$pear = array();
			$pearCount = preg_match_all('!^pear/(\S+)\s+(\S+)\s+([0-9\.]*)\s+(.*)$!m', $status['output'],
				$pearTmp);

			for ($i = 0; $i < $pearCount; $i++) {
				$pear[$pearTmp[1][$i]] = array(
					'versions'    => array(trim($pearTmp[2][$i])),
					'description' => $pearTmp[4][$i]
				);
			}
			file_put_contents(TEMP_DIR . '/pear-cache', serialize($pear));

			return $pear;
		}

		/**
		 * Add PHP channel to PEAR package manager
		 *
		 * @param  string $xml URL reference to package.xml
		 * @return bool
		 */
		public function add_pear_channel($xml)
		{
			if (substr($xml, -4) != '.xml') {
				return error("channel `$xml' must refer to .xml");
			}
			$status = Util_Process_Sudo::exec('pear add-channel %s', $xml);

			return $status['success'];
		}

		/**
		 * Remove PEAR channel from PEAR package manager
		 *
		 * @param  string $channel channel previously added
		 * @return bool
		 */

		public function remove_channel($channel)
		{
			$status = Util_Process_Sudo::exec('pear remove-channel %s', $channel);

			return $status['success'];
		}

		/**
		 * List all channels configured in PHP PEAR package manager
		 *
		 * Sample response-
		 * array(2) {
		 *      [0]=>
		 *      array(5) {
		 *        ["channel"]=>    string(12) "pear.php.net"
		 *        ["summary"]=>    string(40) "PHP Extension and Application Repository"
		 *      }
		 *      [1]=>
		 *      array(5) {
		 *        ["channel"]=>    string(12) "pecl.php.net"
		 *        ["summary"]=>    string(31) "PHP Extension Community Library"
		 *      }
		 *    }
		 *
		 * @return array
		 */
		public function list_channels()
		{
			$channels = array();
			$status = Util_Process_Sudo::exec('pear list-channels');
			if (!$status['success']) {
				return $channels;
			}
			$chmatches = null;
			if (!preg_match_all(Regex::PEAR_CHANNELS_LONG, $status['output'], $chmatches, PREG_SET_ORDER)) {
				return $channels;
			}
			foreach ($chmatches as $channel) {
				$channels[] = array(
					'channel' => $channel['channel'],
					'summary' => $channel['summary']
				);
			}

			return $channels;
		}

		/**
		 * Retrieve PEAR channel information
		 *
		 * Basic wrapper to pear channel-info <channel> command
		 * Sample response-
		 * array(4) {
		 *      ["server"]=>
		 *      string(12) "pear.php.net"
		 *      ["alias"]=>
		 *      string(4) "pear"
		 *      ["summary"]=>
		 *      string(40) "PHP Extension and Application Repository"
		 *      ["version"]=>
		 *      NULL
		 * }
		 *
		 * @param string $channel
		 * @return array
		 */
		public function get_channel_info($channel)
		{
			$info = array();
			$status = Util_Process_Sudo::exec('pear channel-info %s', $channel);
			if (!$status['success']) {
				return false;
			}
			$line = strtok($status['output'], '=');
			$parse = false;

			for ($idx = null; $line !== false; $line = strtok("\n")) {
				// delimiter ===
				if (!$parse) {
					if ($line[0] != '=') {
						continue;
					} else {
						$parse = true;
					}
				}

				if ($idx) {
					$info[$idx] = trim($line);
				}
				$idx = null;

				$lookup = strtok(" \n");
				if ($lookup == 'Name') {
					strtok(' ');
					strtok(' ');
					$idx = 'server';
				} else {
					if ($lookup == 'Alias') {
						$idx = 'alias';
					} else {
						if ($lookup == 'Summary') {
							$idx = 'summary';
						} else {
							if ($lookup == 'Version') {
								// Special case if Version field is null
								$version = null;
								$line = strtok("\n");
								if (false === strpos($line, 'SERVER CAPABILITIES')) {
									$version = trim($line);
								}
								$info['version'] = $version;
							} else {
								if ($lookup[0] == '=') {
									break;
								}
							}
						}
					}
				}
			}

			return $info;
		}

		/**
		 * string get_php_version()
		 * Returns the available PHP interpreter version on the server
		 *
		 * @cache     yes
		 * @privilege PRIVILEGE_ALL
		 *
		 * @return string
		 */
		public function version()
		{
			static $ver;
			if (null === $ver) {
				$key = 'php.version';
				$ver = apcu_fetch($key);
				if ($ver) {
					return $ver;
				}
				$ver = \Opcenter\Php::version();
				apcu_add($key, $ver, 86400);
			}

			return $ver;
		}

		public function _housekeeping()
		{
			// composer.phar seems standard nowadays..
			if ($this->composer_exists()) {
				return true;
			}

			$versions = file_get_contents('https://getcomposer.org/versions');
			if (!$versions) {
				return false;
			}
			$versions = json_decode($versions, true);
			$url = 'https://getcomposer.org/' . $versions['stable'][0]['path'];
			$res = Util_HTTP::download($url, self::COMPOSER_LOCATION);
			if (!$res) {
				return error('failed to download composer');
			}
			chmod(self::COMPOSER_LOCATION, 0755);
			copy(self::COMPOSER_LOCATION, $this->service_template_path('siteinfo') . self::COMPOSER_LOCATION);

			info('installed %s!', basename(self::COMPOSER_LOCATION));

			return true;
		}

		public function composer_exists()
		{
			return file_exists(self::COMPOSER_LOCATION);
		}

		public function _delete()
		{
			foreach ($this->get_fallbacks() as $fallback) {
				if ($this->fallback_enabled($fallback)) {
					$this->disable_fallback($fallback);
				}
			}
		}

		public function get_fallbacks()
		{
			return $this->getPersonalities();
		}

		/**
		 * Verify if fallback enabled for given personality
		 *
		 * @param string|null $mode
		 * @return bool
		 */
		public function fallback_enabled($mode = null)
		{
			if ($this->jailed()) {
				return false;
			}
			if (is_null($mode)) {
				$mode = $this->getPersonalities();
				$mode = array_pop($mode);
			}

			return file_exists($this->getPersonalityPathFromPersonality($mode, $this->site));
		}

		/**
		 * Disable PHP fallback support
		 *
		 * @return bool
		 */
		public function disable_fallback($mode = '')
		{
			if (!IS_CLI) {
				return $this->query('php_disable_fallback');
			}
			if ($this->jailed()) {
				return false;
			}

			if ($mode) {
				$personalities = [$mode];
			} else {
				$personalities = $this->getPersonalities();
			}
			foreach ($personalities as $personality) {
				if (!$this->personalityExists($personality)) {
					error("unknown personality `%s', skipping", $personality);
					continue;
				}
				$path = $this->getPersonalityPathFromPersonality($personality, $this->site);
				if (file_exists($path)) {
					unlink($path);
				} else {
					warn("fallback `%s' not enabled", $personality);
				}
			}

			// defer reloading to a later date
			return true;
		}

		public function _verify_conf(\Opcenter\Service\ConfigurationContext $ctx): bool
		{
			// TODO: Implement _verify_conf() method.
		}

		public function _create()
		{
			// TODO: Implement _create() method.
		}

		public function _edit()
		{
			foreach ($this->get_fallbacks() as $fallback) {
				if ($this->fallback_enabled($fallback)) {
					$this->disable_fallback($fallback) && $this->enable_fallback($fallback);
				}
			}
		}

		/**
		 * Enable fallback interpreter support
		 *
		 * @param null|string $mode specific personality in multi-personality environments
		 * @return bool
		 */
		public function enable_fallback($mode = null)
		{
			if (!IS_CLI) {
				return $this->query('php_enable_fallback', $mode);
			}
			if ($this->jailed()) {
				return error('Fallbacks may be used when PHP jails are disabled');
			}
			if (!$mode) {
				$mode = $this->getPersonalities();
			}

			$file = file_get_contents($this->web_config_dir() . '/virtual/' .
				$this->site);
			// @todo helper function?
			$config = preg_replace(Regex::compile(
				Regex::PHP_COMPILABLE_STRIP_NONHTTP_APACHE_CONTAINER,
				['port' => 80]
			), '', $file);
			$serverip = (array)$this->common_get_ip_address();
			$in = $serverip[0] . ':80';
			foreach ((array)$mode as $m) {
				if (!$this->personalityExists($m)) {
					error("unknown personality `%s' - not enabling", $m);
					continue;
				}
				$port = $this->getPersonalityPort($m);
				$out = $serverip[0] . ':' . $port;
				$newconfig = str_replace($in, $out, $config);
				$confpath = $this->getPersonalityPathFromPersonality($m, $this->site);
				file_put_contents($confpath, $newconfig) && info("enabled fallback for `%s'", $m);
			}
			Util_Account_Hooks::instantiateContexted($this->getAuthContext())->run('reload', ['php']);

			return true;
		}

		/**
		 * PHP is jailed with PHP-FPM
		 *
		 * @return bool
		 */
		public function jailed(): bool
		{
			return (bool)$this->getConfig('apache', 'jail');
		}

		/**
		 * Change pool owner
		 *
		 * @param string      $owner
		 * @param string|null $pool  optional pool name
		 * @return bool
		 */
		public function pool_change_owner(string $owner, string $pool = null): bool
		{
			if (!IS_CLI) {
				return $this->query('php_pool_change_owner', $owner, $pool);
			}

			if (!HTTPD_FPM_USER_CHANGE) {
				return error('Pool ownership disallowed');
			}

			if (!$this->jailed()) {
				return error('Pools not utilized for site');
			}

			$pools = $pools = $this->pools();
			if ($pool && !\in_array($pool, $pools, true)) {
				return error("Unknown pool `%s'", $pool);
			} else if ($pool) {
				warn('Per-pool ownership not supported yet - applying bulk change');
			}

			if (!$this->user_exists($owner)) {
				return error("User `%s' does not exist", $owner);
			}
			if ($owner !== $this->web_get_sys_user() && $owner !== $this->username) {
				return error("Unknown or unsupported pool owner `%s'", $owner);
			}

			$editor = new \Util_Account_Editor($this->getAuthContext()->getAccount(), $this->getAuthContext());
			$editor->setConfig('apache', 'webuser', $owner);
			if (!$editor->edit()) {
				return false;
			}

			$systemd = Fpm\SystemdSynthesizer::mock($this->site . '-' . ($pool ?? array_shift($pools)));
			return $systemd::waitFor(10);
		}

		/**
		 * Get pool cache statistics
		 *
		 * @param string|null $pool
		 * @return array
		 */
		public function pool_cache_status(string $pool = null): ?array
		{
			if (!IS_CLI) {
				return $this->query('php_pool_cache_status', $pool);
			}
			if (!$this->jailed()) {
				error('Jails disabled for site');
				return null;
			}

			$ip = $this->site_ip_address();
			// @TODO pull configuration, determine deets in multipool
			$hostname = $this->domain ?? SERVER_NAME;
			$docroot = $this->web_get_docroot($hostname);
			return CacheInspector::instantiateContexted($this->getAuthContext(), [$ip, $hostname])->readFromPath($docroot);
		}

		/**
		 * Restart PHP-FPM pool state
		 *
		 * @param string $pool
		 * @return bool
		 */
		public function pool_restart(string $pool = null): bool
		{
			return $this->pool_set_state($pool, 'restart');
		}

		/**
		 * Set PHP-FPM pool state
		 *
		 * @param string $pool
		 * @param string $state active systemd state type (stop, start, restart)
		 * @return bool
		 */
		public function pool_set_state(?string $pool = null, string $state = 'stop'): bool
		{
			if (!IS_CLI) {
				return $this->query('php_pool_set_state', $pool, $state);
			}
			if (!\in_array($state, ['stop', 'start', 'restart'], true)) {
				return error("Unknown pool state `%s'", $state);
			}

			if ($pool && !\in_array($pool, $this->pools(), true)) {
				return error("Invalid pool specified `%s'", $pool);
			}
			if (!$pool) {
				$pool = $this->pools();
			}
			foreach ((array)$pool as $p)
			{
				$svc = Fpm\SystemdSynthesizer::mock($this->site . '-' . $p);
				if ($state !== 'stop' && $svc::failed()) {
					$svc::reset();
				}
				if (!$svc::run($state)) {
					warn('Failed to %s %s-%s', $state, $this->site, $p);
				}
			}

			return true;
		}

		/**
		 * Get jail pools
		 *
		 * @return array
		 */
		public function pools(): array
		{
			if (!$this->jailed()) {
				return [];
			}
			$key = 'php.pools';
			$cache = \Cache_Account::spawn($this->getAuthContext());
			if (false !== ($pools = $cache->get($key))) {
				return $pools;
			}
			$pools = array_map(function (string $pool) {
				return substr($pool, strlen($this->site) + 1);
			}, Fpm::getPoolsFromGroup($this->site));
			// @todo possible ghosting
			$cache->set($key, $pools, 30);
			return $pools;
		}

		/**
		 * Get pool status
		 *
		 * @param string $pool named pool or default
		 * @return array
		 */
		public function pool_status(string $pool = ''): array
		{
			if (!($status = $this->pool_info($pool))) {
				return [];
			}
			return (new Fpm\PoolStatus($status))->getMetrics();
		}

		public function _cron(Cronus $c) {
			static $ctr = 0;

			if (!TELEMETRY_ENABLED || ++$ctr < 600 / CRON_RESOLUTION) {
				return;
			}

			$ctr = 0;
			$oldex = \Error_Reporter::exception_upgrade(Error_Reporter::E_FATAL, true);
			$collector = new Collector(PostgreSQL::pdo());
			foreach (\Opcenter\Account\Enumerate::active() as $site) {
				try {
					$ctx = \Auth::context(null, $site);
					$afi = \apnscpFunctionInterceptor::factory($ctx);
					if (!$afi->php_jailed() || !($service = $afi->php_pool_info())) {
						continue;
					}

					$inspector = new Fpm\PoolStatus($service);
					if (!$inspector->running()) {
						continue;
					}
					$cacheMetrics = $inspector->getCacheMetrics($ctx);
					$stats = $afi->php_pool_status();
					if (null === array_get($stats, 'traffic')) {
						// pool outage/not started - let's not trigger the pool
						$stats = PhpMetrics::fill(0);
					} else {
						$stats['uptime'] = time() - $inspector->getStart();
						$stats['cache-used'] = array_get($cacheMetrics, 'memory_usage.used_memory', 0)/1024;
						$stats['cache-total'] = (array_get($cacheMetrics, 'memory_usage.free_memory') + $stats['cache-used'])/1024;
						$stats['cache-hits']  = array_get($cacheMetrics, 'opcache_statistics.hits', 0);
						$stats['cache-misses'] = array_get($cacheMetrics, 'opcache_statistics.misses', 0);
						$stats['traffic'] *= 100;
					}


					foreach (PhpMetrics::getAttributeMap() as $attr => $metric) {
						$val = $stats[$metric];

						if ($val instanceof Closure) {
							$val = $val($stats);
						}
						// @TODO register additional pool names
						$collector->add("php-${attr}", $ctx->site_id, $val);
					}
				} catch (\apnscpException $e) {
					// site doesn't exist
					continue;
				}
			}
			\Error_Reporter::exception_upgrade($oldex);

		}
		/**
		 * Get pool info
		 *
		 * @param string $pool named pool or default
		 * @return array
		 */
		public function pool_info(string $pool = ''): array
		{
			$pools = $this->pools();
			if ($pool && !in_array($pool, $pools, true)) {
				error("Invalid pool specified `%s'", $pool);

				return [];
			}

			if (!$pool) {
				if (\in_array($this->domain, $pools, true)) {
					$pool = $this->domain;
				} else {
					$pool = array_shift($pools);
				}
			}

			// assume default is named after the domain otherwise grab first
			return Fpm\SystemdSynthesizer::mock($this->site . '-' . $pool)->status() ?: [];
		}

		/**
		 * Get PHP version
		 *
		 * @param string $pool
		 * @return string
		 */
		public function pool_get_version(string $pool = ''): string
		{
			if (!IS_CLI) {
				return $this->query('php_pool_get_version', $pool);
			}
			if (!$this->jailed()) {
				return $this->php_version();
			}

			return PoolPolicy::instantiateContexted($this->getAuthContext())->getPoolVersion($pool);
		}

		/**
		 * Set PHP version on pool
		 *
		 * @param string $version
		 * @param string $pool optional pool name
		 * @return bool
		 */
		public function pool_set_version(string $version, string $pool = ''): bool
		{
			if (!IS_CLI) {
				return $this->query('php_pool_set_version', $version, $pool);
			}

			if (!$this->jailed()) {
				return error("Cannot change version for non-jailed account");
			}

			$origPool = $pool;
			if (!$pool) {
				$pool = $this->pools();
			} else if (!$this->pool_exists($pool)) {
				return error("Invalid pool specified `%s'", $pool);
			}

			if (!PoolPolicy::instantiateContexted($this->getAuthContext())->allowed($version)) {
				return error("PHP version `%s' not installed or permitted on account", $version);
			}

			$version = \Opcenter\Versioning::asMinor($version);

			if (!array_key_exists((string)$version, Fpm\MultiPhp::list())) {
				if ($version !== ($tmp = \Opcenter\Versioning::asMinor($version))) {
					warn("Truncating version to `%s'", $tmp);
					return $this->switch_version($version, $pool);
				}

				return error("PHP interpreter %s does not exist", $version);
			}

			$svc = \Opcenter\SiteConfiguration::shallow($this->getAuthContext());

			foreach ((array)$pool as $p) {
				// @TODO multipool
				$config = Fpm\Configuration::bindTo($this->domain_fs_path());
				$config->setServiceContainer($svc);
				$config->setVersion($version);
			}
			// write configuration
			unset($config);

			return \Util_Account_Editor::instantiateContexted($this->getAuthContext())->edit() &&
				$this->pool_restart($origPool);
		}

		/**
		 * Get available PHP pool versions
		 *
		 * @return array
		 */
		public function pool_versions(): array
		{
			if (!IS_CLI) {
				return $this->query('php_pool_versions');
			}
			return array_keys(Fpm\MultiPhp::list());
		}

		/**
		 * PHP pool exists
		 *
		 * @param string $pool pool name
		 * @return bool
		 */
		public function pool_exists(string $pool): bool
		{
			return in_array($pool, $this->pools(), true);
		}

		/**
		 * Migrate PHP directives to configured engine format
		 *
		 * @param string      $hostname
		 * @param string      $path
		 * @param string|null $from null to auto-detect (jail=1 implies "isapi", otherwise "fpm")
		 * @return bool
		 */
		public function migrate_directives(string $hostname, string $path = '', string $from = null): bool
		{
			if (!IS_CLI) {
				return $this->query('php_migrate_directives', $hostname, $path, $from);
			}
			if (null === $from) {
				$from = $this->jailed() ? 'isapi' : 'fpm';
			}

			if ($from !== 'isapi' && $from !== 'fpm') {
				return error("Unknown underlying PHP engine `%s'", $from);
			}

			if (! ($docroot = $this->web_get_docroot($hostname, $path)) ) {
				return false;
			}

			$srcFile = $docroot . DIRECTORY_SEPARATOR . ($from === 'isapi' ? '.htaccess' : '.user.ini');
			$destFile = $docroot . DIRECTORY_SEPARATOR . ($from === 'isapi' ? '.user.ini' : '.htaccess');
			if (!$this->file_exists($srcFile)) {
				return true;
			}
			$srcContents = $destContents = '';
			if ($this->file_exists($srcFile)) {
				$srcContents = $this->file_get_file_contents($srcFile);
			}
			if ($this->file_exists($destFile)) {
				$destContents = $this->file_get_file_contents($destFile);
			}
			$directives = [];
			$lines = explode("\n", $srcContents);
			if ($from === 'fpm') {
				foreach ($lines as $line) {
					if (false === strpos($line, '=')) {
						continue;
					} else if (strpos($line, ';') === strcspn(';', $line)) {
						// comment
						continue;
					}
					[$ini, $val] = explode('=', trim($line), 2);
					$directive = 'php_value';
					if (strtolower($val) === 'on' || strtolower($val) === 'off') {
						$directive = 'php_flag';
					}
					$directives[] = "${directive} ${ini} ${val}";
				}
			} else {
				foreach ($lines as &$line) {
					if (false === strpos($line, 'php_flag') && false === strpos($line, 'php_value')) {
						continue;
					}
					if (!preg_match('/^\s*(?<directive>php_(?:flag|value))\s+(?<ini>[^ ]+)\s+(?<value>.+)$/', $line, $match)) {
						debug("Unknown line `%s' encountered - ignoring", $line);
						continue;
					}

					$directives[] = $match['ini'] . '=' . $match['value'];
					$line = null;
				}
			}

			// @TODO validate consistency
			return $this->file_put_file_contents($destFile, rtrim(rtrim($destContents) . "\n" . implode("\n", $directives)) . "\n") &&
				$this->file_put_file_contents($srcFile, implode("\n", array_filter($lines)) . "\n");
		}

		public function _create_user(string $user)
		{ }

		public function _delete_user(string $user)
		{ }

		public function _edit_user(string $userold, string $usernew, array $oldpwd)
		{ }
	}