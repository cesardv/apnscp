<?php
	declare(strict_types=1);
	/**
	 *  +------------------------------------------------------------+
	 *  | apnscp                                                     |
	 *  +------------------------------------------------------------+
	 *  | Copyright (c) Apis Networks                                |
	 *  +------------------------------------------------------------+
	 *  | Licensed under Artistic License 2.0                        |
	 *  +------------------------------------------------------------+
	 *  | Author: Matt Saladna (msaladna@apisnetworks.com)           |
	 *  +------------------------------------------------------------+
	 */

	use Module\Support\Dns;
	use Opcenter\Account\Ephemeral;
	use Opcenter\SiteConfiguration;

	/**
	 * Provides DNS functions to apnscp.
	 *
	 * @package core
	 */
	class Dns_Module extends Dns
	{
		use NamespaceUtilitiesTrait;

		const DEPENDENCY_MAP = [
			'siteinfo'
		];
		/**
		 * apex markers are marked with @
		 */
		protected const HAS_ORIGIN_MARKER = false;
		/**
		 * Maximum contiguous name length is 255 per RFC 1035 2.3.4
		 */
		protected const HAS_CONTIGUOUS_LIMIT = false;

		/** primary nameserver */
		const MASTER_NAMESERVER = DNS_INTERNAL_MASTER;
		/**
		 * @var array 1 or more authoritative nameservers
		 */
		const AUTHORITATIVE_NAMESERVERS = DNS_AUTHORITATIVE_NS;
		/**
		 * @var array 1 or more recursive nameservers
		 */
		const RECURSIVE_NAMESERVERS = DNS_RECURSIVE_NS;
		const UUID_RECORD = DNS_UUID_NAME;

		// default DNS TTL for records modified via update()
		const DYNDNS_TTL = 300;

		// @var int minimum module TTL
		const DNS_TTL_MIN = 5;

		// @var int default DNS TTL
		const DNS_TTL = DNS_DEFAULT_TTL;

		// standard hosts file location
		const HOSTS_FILE = '/etc/hosts';

		// dig command for short lookup
		const DIG_SHLOOKUP = 'dig +norec +time=3 +tcp +short @%(nameserver)s %(hostname)s %(rr)s';

		// NS apex is modifiable and thus shall be displayed
		public const SHOW_NS_APEX = true;

		/** @var array quick lookup */
		protected static $zoneExistsCache = [];
		/**
		 * @var string TSIG key
		 * @ignore
		 */
		protected static $dns_key = DNS_TSIG_KEY;
		/** mapping of RR types to constants */
		protected static $rec_2_const = array(
			'ANY'   => DNS_ANY,
			'A'     => DNS_A,
			'AAAA'  => DNS_AAAA,
			'MX'    => DNS_MX,
			'NS'    => DNS_NS,
			'SOA'   => DNS_SOA,
			'TXT'   => DNS_TXT,
			'CNAME' => DNS_CNAME,
			'SRV'   => DNS_SRV,
			'PTR'   => DNS_PTR,
			'HINFO' => DNS_HINFO,
			'A6'    => DNS_A6,
			'NAPTR' => DNS_NAPTR,
			'CAA'   => DNS_CAA,
			'TLSA'  => 0,
			'DS'    => 0,
			'RP'    => 0,
			'SSHFP' => 0,
			'SPF'   => 0,
			'URI'   => 0,
			'CERT'  => 0

		);
		/** array of 1 or more nameservers used */
		protected static $nameservers;

		/**
		 * Legal DNS resource records permitted by provider
		 * A
		 * AAAA
		 * MX
		 * CNAME
		 * DNAME
		 * HINFO
		 * TXT
		 * NS
		 * SRV
		 *
		 * @var array
		 */
		protected static $permitted_records = array(
			'A',
			'AAAA',
			'A6',
			'CAA',
			'CNAME',
			'DNAME',
			'HINFO',
			'MX',
			'NAPTR',
			'NS',
			'SOA',
			'SRV',
			'TXT',
		);

		protected $exportedFunctions = [
			'*'                      => PRIVILEGE_SITE,
			'get_public_ip'          => PRIVILEGE_SITE | PRIVILEGE_USER,
			'get_public_ip6'         => PRIVILEGE_SITE | PRIVILEGE_USER,
			'enabled'                => PRIVILEGE_SITE | PRIVILEGE_USER,
			'configured'             => PRIVILEGE_ALL,
			'get_whois_record'       => PRIVILEGE_ALL,
			'get_records_by_rr'      => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'get_records'            => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'record_exists'          => PRIVILEGE_ALL,
			'modify_record'          => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'remove_record'          => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'add_record'             => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'get_zone_information'   => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'get_all_domains'        => PRIVILEGE_ADMIN,
			'get_parent_domain'      => PRIVILEGE_ADMIN,
			'get_server_from_domain' => PRIVILEGE_ADMIN,
			'release_ip'             => PRIVILEGE_ADMIN,
			'ip_allocated'           => PRIVILEGE_ADMIN,
			'gethostbyaddr_t'        => PRIVILEGE_ALL,
			'gethostbyname_t'        => PRIVILEGE_ALL,
			'get_provider'           => PRIVILEGE_ALL,
			'uuid'                   => PRIVILEGE_ALL,
			'get_default'            => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'provisioning_records'   => PRIVILEGE_SITE,
			'remove_zone'            => PRIVILEGE_ADMIN,
			'remove_zone_backend'    => PRIVILEGE_ADMIN | PRIVILEGE_SITE | PRIVILEGE_SERVER_EXEC,
			'add_zone'               => PRIVILEGE_ADMIN,
			'add_zone_backend'       => PRIVILEGE_ADMIN | PRIVILEGE_SITE | PRIVILEGE_SERVER_EXEC,
			'providers'              => PRIVILEGE_ADMIN,
			'zone_exists'            => PRIVILEGE_ADMIN | PRIVILEGE_SITE,
			'validate_template'      => PRIVILEGE_ADMIN,
			'import_from_domain'     => PRIVILEGE_ADMIN | PRIVILEGE_SITE,
			'export'                 => PRIVILEGE_ADMIN | PRIVILEGE_SITE,
			'import'                 => PRIVILEGE_ADMIN | PRIVILEGE_SITE,
			'auth_test'              => PRIVILEGE_SITE | PRIVILEGE_ADMIN
		];

		/**
		 * Provider loader
		 *
		 * @return Module_Skeleton
		 */
		public function _proxy(): \Module_Skeleton
		{
			$provider = $this->get_provider();

			if ($provider === \Opcenter\Service\Contracts\DefaultNullable::NULLABLE_MARKER) {
				// BUG. An account's provider is substituted with the provider default at creation
				// Check for an in-place upgrade. If plan wasn't substituted because no prior def exists
				// the marker is used.
				$provider = \Opcenter\Dns::default();
			}

			if ($provider === 'builtin') {
				return $this;
			}

			return \Module\Provider::get('dns', $provider, $this->getAuthContext());
		}

		/**
		 * Get DNS provider
		 *
		 * @return string
		 */
		public function get_provider(): string
		{
			if ($this->permission_level & (PRIVILEGE_SITE|PRIVILEGE_USER)) {
				if (!$this->enabled()) {
					return 'null';
				}
				return $this->getServiceValue('dns', 'provider', DNS_PROVIDER_DEFAULT);
			}
			return \Opcenter\Dns::default();
		}

		/**
		 * Get known mail providers
		 *
		 * @return array
		 */
		public function providers(): array
		{
			return \Opcenter\Dns::providers();
		}

		/**
		 * Get DNS UUID record name
		 *
		 * @return string
		 */
		public function uuid_name(): string
		{
			return static::UUID_RECORD;
		}

		/**
		 * Query database for domain expiration
		 *
		 * On multi-server lookups that perform DNS lookups independent,
		 * perform batch lookups and pull those records from the database
		 *
		 *
		 * A return of 0 indicates failure
		 * null indicates unknown expiration
		 *
		 * @param string $domain domain owned by the account
		 * @return null|int expiration as unix timestamp
		 */
		public function domain_expiration(string $domain): ?int
		{
			return null;
		}

		/**
		 * Fetches all domains across all servers
		 *
		 * Used in multi-server layouts
		 *
		 * @return array
		 */
		public function get_all_domains(): array
		{
			return array_keys(\Opcenter\Map::load(\Opcenter\Map::DOMAIN_MAP)->fetchAll());
		}

		/**
		 * Get server on which a domain is hosted
		 *
		 * @param string $domain
		 * @param bool   $all show all server matches, $all = true: array of all servers, else server
		 * @return string|array
		 */
		public function get_server_from_domain(string $domain, bool $all = false)
		{
			return SERVER_NAME_SHORT;
		}

		/**
		 * Get primary domain affiliated with account
		 *
		 * In multi-server setups query the master DB to find
		 * domains whose invoice matches the parent invoice or domains
		 * that share the same site ID
		 *
		 * @param string $domain
		 * @return bool|string primary domain or false on error
		 */
		public function get_parent_domain(string $domain)
		{
			if (false === ($id = \Opcenter\Map::load(\Opcenter\Map::DOMAIN_MAP)->fetch($domain))) {
				return false;
			}

			return \Auth::get_domain_from_site_id((int)substr($id, 4));
		}

		/**
		 * Query WHOIS server for record
		 *
		 * @param  string $domain domain name to look up the whois record for
		 * @return string|bool whois data
		 *
		 */
		public function get_whois_record(string $domain)
		{
			Error_Reporter::suppress_php_error('require_once');
			if (!preg_match(Regex::DOMAIN, $domain)) {
				return error('%s: invalid domain', $domain);
			}
			if (!class_exists('Net_Whois', false) &&
				!include 'Net/Whois.php'
			) {
				return error('Unable to include Whois module');
			}

			$whois = new Net_Whois();
			$data = $whois->query($domain);
			if (PEAR::isError($data)) {
				return error("Failed to lookup whois data: `%s'", $data->message);
			}

			return $data;
		}

		/**
		 * DNS zone exists
		 *
		 * @param string $zone
		 * @return bool
		 */
		public function zone_exists(string $zone): bool
		{
			if (!$this->configured()) {
				return false;
			}
			if (!$this->owned_zone($zone)) {
				return false;
			}
			if (!isset(static::$zoneExistsCache[$zone])) {
				$resp = ($this->zoneAxfr($zone));
				static::$zoneExistsCache[$zone] = (null !== $resp);
			}
			return static::$zoneExistsCache[$zone];
		}

		/**
		 * DNS handler is configured for account
		 *
		 * Weaker form of enabled()
		 *
		 * @return bool
		 */
		public function configured(): bool
		{
			return static::class !== self::class;
		}

		/**
		 * DNS is enabled for account
		 *
		 * Stricter version of configured().
		 * Requires module to faithfully implement DNS functions.
		 *
		 * @return bool
		 */
		public function enabled(): bool
		{
			return (bool)$this->getServiceValue('dns', 'enabled');
		}

		/**
		 * Requested domain is manageable by the account
		 *
		 * @param  string $zone zone name
		 * @return bool
		 */
		protected function owned_zone(string $zone): bool
		{
			if ($this->getAuthContext()->level & PRIVILEGE_ADMIN) {
				return true;
			}

			if ($this->parented($zone)) {
				return true;
			}

			$aliases = $this->aliases_list_aliases();

			if ( ($zone === $this->domain) || in_array($zone, $aliases, true) ) {
				return true;
			}
			if ($zone === array_get((array)$this->getOldServices('siteinfo'), 'domain', null)) {
				return true;
			}
			return \in_array($zone, array_get((array)$this->getOldServices('aliases'), 'aliases', []), true);
		}

		/**
		 * Perform a full zone transfer
		 *
		 * @param string $domain
		 * @return null|string
		 */
		protected function zoneAxfr(string $domain): ?string
		{
			if (!static::MASTER_NAMESERVER) {
				error("Cannot fetch zone information for `%s': no master nameserver configured in config.ini",
					$domain);

				return null;
			}
			$data = Util_Process::exec('dig -t AXFR -y %s @%s %s',
				escapeshellarg($this->getTsigKey($domain)), static::MASTER_NAMESERVER, $domain, [-1, 0]);

			// AXFR can fail yet return a success RC
			if (false !== strpos($data['output'], '; Transfer failed.')) {
				return null;
			}

			return $data['success'] ? $data['output'] : null;
		}

		/**
		 * Get AXFR key for domain
		 *
		 * @param string $domain
		 * @return null|string
		 */
		private function getTsigKey(string $domain): ?string
		{
			return static::$dns_key;
		}

		/**
		 * Export zone configuration in BIND-friendly notation
		 *
		 * @param string|null $zone
		 * @return bool|string
		 */
		public function export(string $zone = null)
		{
			if (null === $zone) {
				$zone = $this->domain;
			}

			if (!$this->permission_level & PRIVILEGE_ADMIN && !$this->owned_zone($zone)) {
				return error("access denied - cannot view zone `%s'", $zone);
			}
			$recs = $this->get_zone_data($zone);
			if (null === $recs) {
				return error("failed to export zone `%s'", $zone);
			}
			$soa = $recs['SOA'][0];
			$soadata = preg_split('/\s+/', $soa['parameter']);
			$format = ';; ' . "\n" .
				";; Domain:\t" . $zone . "\n" .
				";; Exported:\t" . date('r') . "\n" .
				';; ' . "\n" .
				'$ORIGIN . ' . "\n" .
				"@\t" . $soa['ttl'] . "\tIN\tSOA\t$zone.\t" . $soadata[1] . ' ( ' . "\n" .
				"\t" . $soadata[2] . "\t; serial" . "\n" .
				"\t" . $soadata[3] . "\t; refresh" . "\n" .
				"\t" . $soadata[4] . "\t; retry" . "\n" .
				"\t" . $soadata[5] . "\t; expire" . "\n" .
				"\t" . $soadata[6] . ")\t; minimum" . "\n\n";
			$buffer = array();
			$buffer[] = ';; NS Records (YOU MUST CHANGE THIS)';
			foreach ($recs['NS'] as $ns) {
				$ns['parameter'] = 'YOU_MUST_CHANGE_THIS_VALUE';
				$buffer[] = $ns['name'] . "\t" . $ns['ttl'] .
					"\t" . "IN NS\t" . $ns['parameter'];
			}
			$buffer[] = '';
			$ignore = array('SOA', 'NS', 'TSIG');
			foreach (static::$permitted_records as $rr) {
				$rr = strtoupper($rr);
				if (!isset($recs[$rr]) || in_array($rr, $ignore, true)) {
					continue;
				}
				$buffer[] = ';; ' . $rr . ' Records';
				foreach ($recs[$rr] as $r) {
					$buffer[] = $r['name'] . "\t" . $r['ttl'] .
						"\t" . 'IN ' . $rr . "\t" . $r['parameter'];
				}
				$buffer[] = "\n";
			}
			$format .= implode("\n", $buffer);

			return $format;
		}

		/**
		 * Get all zone records parsed
		 *
		 * @param string $domain
		 * @return array|null zone data or null if zone not present
		 */
		protected function get_zone_data(string $domain): ?array
		{
			if (!$this->owned_zone($domain)) {
				error("Domain `%s' not owned by account", $domain);

				return null;
			}

			if (null === ($data = $this->zoneAxfr($domain))) {
				return $data;
			}

			$zoneData = array();
			$offset = strlen($domain) + 1; // domain.com.
			$regexp = \Regex::compile(\Regex::DNS_AXFR_REC,
				['rr' => implode('|', static::$permitted_records + [99999 => 'SOA'])]);

			foreach (explode("\n", $data) as $line) {
				if (false !== strpos($line, 'Transfer failed.')) {
					return null;
				}
				if (!preg_match($regexp, $line, $match)) {
					continue;
				}
				[$name, $ttl, $class, $rr, $parameter] = array_slice($match, 1);
				$rr = strtoupper($rr);
				// TXT records should always be balanced with quotes
				// assume this to be the case if " present
				// don't pretty-print if more than 1 quote pair present
				if ($rr == 'TXT' && $parameter[0] == '"') {
					$end = strlen($parameter) - 1;
					if (strpos($parameter, '"', 1) === $end && strpos($parameter, '"', 1) === $end) {
						// parameter formatted as "foobar"
						// preserve TXT records formatted as '"foo" "bar" "baz"'
						$parameter = substr($parameter, 1, -1);
					}
				}
				$zoneData[$rr][] = array(
					'name'      => $name,
					'subdomain' => rtrim(substr($name, 0, strlen($name) - $offset), '.'),
					'domain'    => $domain,
					'class'     => $class,
					'ttl'       => (int)$ttl,
					'parameter' => $parameter
				);
			}

			return $zoneData;
		}

		/**
		 * Get permitted records for DNS provider
		 *
		 * @return array
		 */
		public function permitted_records(): array
		{
			return static::$permitted_records;
		}

		/**
		 * Get DNS record(s) from a third-party nameserver
		 *
		 * When using same nameservers as hosting ns, ensure ns uses split-view
		 *
		 * @param  string $subdomain   optional subdomain
		 * @param  string $rr          optional RR type
		 * @param  string $domain      optional domain
		 * @param  array  $nameservers optional nameserver to query
		 * @return array|bool false on error
		 */
		public function get_records_external(
			string $subdomain = '',
			string $rr = 'any',
			string $domain = null,
			array $nameservers = null
		) {
			if (!$domain) {
				$domain = $this->domain;
			}
			$host = $domain;
			if ($subdomain) {
				$host = $subdomain . '.' . $host;
			}
			$rr = strtoupper($rr);
			if (null === self::record2const($rr)) {
				return error("unknown rr record type `%s'", $rr);
			}
			if (!$nameservers) {
				$nameservers = static::RECURSIVE_NAMESERVERS;
			}
			$resolvers = [];
			foreach ($nameservers as $ns) {
				if (strspn($ns, '1234567890.') === \strlen($ns)) {
					$resolvers[] = $ns;
				} else if ($ip = Net_Gethost::gethostbyname_t($ns)) {
					$resolvers[] = $ip;
				}
			}
			$resolver = new Net_DNS2_Resolver([
				'nameservers' => $resolvers,
				'ns_random'   => true
			]);

			for ($i = 0; $i < 5; $i++) {
				// @todo remove dependency on dig
				$recraw = Error_Reporter::silence(static function () use ($host, $rr, $resolver) {
					try {
						return $resolver->query($host, $rr);
					} catch (Net_DNS2_Exception $e) {
						return false;
					}
				});
				if (!empty($recraw->answer)) {
					break;
				}
				usleep(50000);
			}
			if (empty($recraw->answer)) {
				$host = ltrim(implode('.', array($subdomain, $domain)), '.');
				warn("failed to get external raw records for `%s' on `%s'", $rr, $host);

				return [];
			}

			$records = array();
			foreach ($recraw->answer as $r) {
				$target = null;

				// most records
				if (isset($r->target)) {
					$target = $r->target;
				}
				// A
				if (isset($r->ip)) {
					$target = $r->ip;
				} else if (isset($r->address)) {
					$target = $r->address;
				}
				// SRV
				if (isset($r->weight)) {
					// ignore PRI that comes before WEIGHT
					// it is handled next
					$target = $r->weight . ' ' . $target .
						$r->port;
				}
				// MX, SRV
				if (isset($r->pri)) {
					$target = $r->pri . ' ' . $target;
				}
				// TXT
				if (isset($r->txt)) {
					$target = $r->txt;
				}
				// HINFO
				if (isset($r->cpu)) {
					$target = $r->cpu . ' ' . $r->os;
				}
				// SOA
				if (isset($r->mname)) {
					$target = $r->mname . ' ' . $r->rname . ' ' .
						$r->serial . ' ' . $r->refresh . ' ' .
						$r->retry . ' ' . $r->expire . ' ' .
						$r->minimum;
				}
				// AAAA, A6
				if (isset($r->ipv6)) {
					$target = $r->ipv6;
				}
				// A6
				if (isset($r->masklen)) {
					$target = $r->masklen . ' ' . $target . ' ' .
						$r->chain;
				}
				// NAPTR
				if (isset($r->order)) {
					$target = $r->order . ' ' . $r->pref . ' ' .
						$r->flags . ' ' . $r->services . ' ' .
						$r->regex . ' ' . $r->replacement;
				}
				$records[] = array(
					'name'      => $host,
					'subdomain' => $subdomain,
					'domain'    => $domain,
					'class'     => 'IN',
					'type'      => $rr,
					'ttl'       => $r->ttl,
					'parameter' => $target
				);
			}

			return $records;
		}

		/**
		 * Translate RR into PHP constant
		 *
		 * NB Used by DNS Manager
		 *
		 * @param string $rr
		 * @return int
		 */
		public static function record2const(string $rr): ?int
		{
			$rr = strtoupper($rr);

			return static::$rec_2_const[$rr] ?? null;
		}

		/**
		 * Returns the host name of the Internet host specified by $ip with timeout
		 *
		 * @param string $ip
		 * @param int    $timeout
		 * @return string|null|false false on error, null on missing record
		 */
		public function gethostbyaddr_t(string $ip, int $timeout = 1000)
		{
			return Net_Gethost::gethostbyaddr_t($ip, $timeout);
		}

		/**
		 * Returns the IP of the Internet host specified by $host with timeout
		 *
		 * @param string $name
		 * @param int    $timeout
		 * @return bool|null|string
		 */
		public function gethostbyname_t(string $name, int $timeout = 1000)
		{
			return Net_Gethost::gethostbyname_t($name, $timeout);
		}

		/**
		 * Check whether a domain is hosted on any server
		 *
		 * In multi-server setups, ensure all servers are named
		 * SERVER_NAME_SHORT (first component of "hostname" command)
		 *
		 * @param string $domain
		 * @param bool   $ignore_on_account domains hosted on account ignored
		 * @return bool
		 */
		public function domain_hosted(string $domain, bool $ignore_on_account = false): bool
		{
			$domain = strtolower($domain);
			if (0 === strncmp($domain, "www.", 4)) {
				$domain = substr($domain, 4);
			}

			$site_id = \Auth::get_site_id_from_domain($domain);
			// check if on same server
			if ($ignore_on_account && $site_id) {
				return $site_id !== $this->site_id;
			}

			// check if elsewhere
			if (false === ($server = \Auth_Redirect::lookup($domain))) {
				return warn('Domain validation endpoint failed. Treating lookup result as positive.');
			}

			// on another server
			if ($server && $server !== SERVER_NAME_SHORT) {
				return true;
			}

			// domain exists on server
			return (bool)$site_id;
		}

		/**
		 * Domain exists and is under the account on a multi-server instance
		 *
		 * Useful when doing cross-server transfers to ensure the domain to add
		 * is part of the account, which will allow the domain to be added via
		 * the aliases_add_domain @{see Aliases_Module::add_domain}
		 *
		 * Implementation details are available on github.com/apisnetworks/apnscp-modules
		 *
		 * @param string $domain
		 * @return bool
		 */
		public function domain_on_account(string $domain): bool
		{
			return false;
		}

		/**
		 * Get recently expiring domains
		 *
		 * Sample response:
		 * Array(
		 *  [0] => Array(
		 *      'domain' => 'apnscp.com',
		 *      'ts' => 1469937612
		 *  )
		 * )
		 *
		 * @param int  $days        lookahead n days
		 * @param bool $showExpired show domains expired within the last 10 days
		 *
		 * @return array
		 */
		public function get_pending_expirations(int $days = 30, bool $showExpired = true): array
		{
			return [];
		}

		/**
		 * Check zone data with bind
		 *
		 * @param string $zone
		 * @param Record[]  $recs
		 * @return bool
		 */
		public function check_zone(string $zone, array $recs = []): bool
		{
			if (!file_exists('/usr/sbin/named-checkzone')) {
				return warn('bind package is not installed - cannot check zone');
			}
			$tmpfile = tempnam('/tmp', 'f');
			if (null === ($axfr = $this->zoneAxfr($zone))) {
				return error("Failed to transfer zone `%s' - cannot check", $zone);
			}

			if ($recs) {
				$axfr .= "\n" . implode("\n", array_filter(array_map(static function ($r) {
					if (!$r instanceof \Opcenter\Dns\Record) {
						return $r[0] . ' ' . $r[1] . ' ' . $r[2] . ' ' . $r[3];
					}
					return (string)$r;
				})));

			}
			file_put_contents($tmpfile, $axfr);
			$status = Util_Process_Safe::exec('/usr/sbin/named-checkzone %s %s', [$zone, $tmpfile]);
			unlink($tmpfile);

			return $status['success'];
		}

		/**
		 * Update hostname with caller's IP4 address
		 *
		 * @param string $hostname fqdn
		 * @param string $ip       optional ip address to skip detection
		 * @return string|bool ip address or false on failure
		 */
		public function update(string $hostname, string $ip = null)
		{
			$chunk = $this->web_split_host($hostname);
			$domain = $chunk['domain'];
			$subdomain = $chunk['subdomain'];
			if (!$this->owned_zone($domain)) {
				return error("restricted zone `%s' specified", $domain);
			}
			if (!$ip) {
				$ip = Auth::client_ip();
			}
			if (false === ip2long($ip)) {
				return error('cannot detect ip!');
			}
			$record = $this->get_records($subdomain, 'A', $domain);
			if (count($record) > 1) {
				warn("%d records found for `%s'", count($record), $hostname);
			}
			if (!$record) {
				// no record set
				warn("no DNS record exists, setting new record for `%s'", $hostname);
				$add = $this->add_record($domain, $subdomain, 'A', $ip, static::DYNDNS_TTL);
				if (!$add) {
					return $add;
				}

				return $ip;
			}

			$newparams = array('ttl' => static::DYNDNS_TTL, 'parameter' => $ip);
			$ret = true;
			foreach ($record as $r) {
				if (!$this->modify_record($domain, $subdomain, $r['rr'], $r['parameter'], $newparams)) {
					$ret = false;
					error("record modification failed for `%s'", $hostname);
				}
			}

			return $ret ? $ip : $ret;
		}

		/*
		 * Check zone for errors
		 *
		 * Additional DNS records are formatted as arguments
		 * to add_record()-
		 * check_zone("debug.com",["mail","a",86400,"127.0.0.1"])
		 *
		 * Hostname, RR, TTL, and parameter are required
		 *
		 * @param $zone zone - usually a domain
		 * @param $recs additional DNS records used in check
		 *
		 * @return bool
		 */

		/**
		 * Get DNS record(s)
		 *
		 * @param string $subdomain optional subdomain
		 * @param string $rr        optional RR type
		 * @param string $domain    optional domain
		 * @return array|false
		 */
		public function get_records(?string $subdomain = '', string $rr = 'any', string $domain = null)
		{
			if (!$domain) {
				$domain = $this->domain;
			}
			if (!$this->owned_zone($domain)) {
				return error('cannot view DNS information for unaffiliated domain `' . $domain . "'");
			}
			if (null === $subdomain) {
				$records = $this->get_zone_information($domain);
				$rr = strtoupper($rr);
				if ($rr === 'ANY') {
					return $records;
				}

				return $records[$rr] ?? [];
			}
			if (false === ($recs = $this->get_records_raw($subdomain, $rr, $domain))) {
				// provisioning/internal error
				return false;
			}

			return (array)$recs;

		}

		/**
		 * get_records() unauthenticated DNS wrapper
		 *
		 * @param string $subdomain optional subdomain
		 * @param string $rr        optional RR type
		 * @param string $domain    optional domain
		 * @return array|bool records or false on failure
		 */
		protected function get_records_raw(string $subdomain = '', string $rr = 'ANY', string $domain = null)
		{
			if ($subdomain == '@') {
				$subdomain = '';
				warn("record `@' alias for domain - record stripped");
			}
			$rr = strtoupper($rr);
			if ($rr !== 'ANY' && !in_array($rr, static::$permitted_records, true)) {
				return error('%s: invalid resource record type', $rr);
			}
			$rr = strtoupper($rr);
			$recs = $this->get_zone_data($domain);
			// zone error, Transfer failed, i.e. zone not provisioned
			if (null === $recs) {
				return false;
			}
			$domain .= '.';
			if ($subdomain !== '') {
				$domain = $subdomain . '.' . $domain;
			}

			$newrecs = [];
			$keys = [$rr];
			if ($rr === 'ANY') {
				$keys = array_keys($recs);
			} else if (!isset($recs[$rr])) {
				return $newrecs;
			}
			foreach ($keys as $tmp) {
				foreach ($recs[$tmp] as $rec) {
					$rec['rr'] = $tmp;
					if ($rec['name'] === $domain) {
						$newrecs[] = $rec;
					}
				}
			}

			return $newrecs;
		}

		/**
		 * Add a DNS record to a domain
		 *
		 * @param string $zone      zone name (normally domain name)
		 * @param string $subdomain name of the record to add
		 * @param string $rr        resource record type [MX, A, AAAA, CNAME, NS, TXT, DNAME]
		 * @param string $param     parameter value
		 * @param int    $ttl       TTL value, default value 86400
		 *
		 * @return bool
		 */
		public function add_record(
			string $zone,
			string $subdomain,
			string $rr,
			string $param,
			int $ttl = self::DNS_TTL
		): bool {
			if (!$this->owned_zone($zone)) {
				return error('%s not owned by account', $zone);
			}
			if (!$this->canonicalizeRecord($zone, $subdomain, $rr, $param, $ttl)) {
				return false;
			}

			/**
			 * Implement your own!
			 */
			return true;
		}

		/**
		 * Add a DNS record to a domain if no other record exists
		 *
		 * Purposed for Lararia\Jobs\SimpleCommandJob
		 *
		 * @param string $zone      zone name (normally domain name)
		 * @param string $subdomain name of the record to add
		 * @param string $rr        resource record type [MX, A, AAAA, CNAME, NS, TXT, DNAME]
		 * @param string $param     parameter value
		 * @param int    $ttl       TTL value, default value 86400
		 *
		 * @return bool
		 */
		public function add_record_conditionally(
			string $zone,
			string $subdomain,
			string $rr,
			string $param,
			int $ttl = self::DNS_TTL
		): bool
		{
			if (!$this->owned_zone($zone)) {
				return error('%s not owned by account', $zone);
			}
			if (!$this->configured()) {
				return info('DNS not configured for account - cannot add record');
			}
			if (!$this->zone_exists($zone)) {
				return warn("DNS zone `%s' does not exist, skipping", $zone);
			}
			if (!$this->canonicalizeRecord($zone, $subdomain, $rr, $param, $ttl)) {
				return false;
			}
			if ($this->record_exists($zone, $subdomain, $rr)) {
				return info('Record %s%s already exists - not overwriting', ltrim($subdomain . '.', '.'), $zone);
			}
			if ($rr === 'CNAME' && $this->record_exists($zone, $subdomain, 'ANY')) {
				return info('Record %s%s already has existing record - not adding CNAME', ltrim($subdomain . '.', '.'), $zone);
			}
			if (($rr === 'A' || $rr === 'AAAA') && $this->record_exists($zone, $subdomain, 'CNAME')) {
				return info('Record %s%s already exists as CNAME - not adding %s', ltrim($subdomain . '.', '.'), $zone, $rr);
			}
			if (!$this->add_record($zone, $subdomain, $rr, $param)) {
				return error('%s%s: DNS master returned bad value', ltrim($subdomain . '.', '.'), $zone);
			}
			return true;
		}

		/**
		 * Fixup a DNS record before submitting
		 *
		 * @param string $zone
		 * @param string $subdomain
		 * @param string $rr
		 * @param string $param
		 * @param int    $ttl
		 * @return bool
		 */
		protected function canonicalizeRecord(
			string &$zone,
			string &$subdomain,
			string &$rr,
			string &$param,
			int &$ttl = null
		): bool {

			$rr = strtoupper($rr);
			$subdomain = rtrim($subdomain, '.');
			if ($rr == 'CNAME') {
				if (!$subdomain && $this->hasCnameApexRestriction()) {
					return error('CNAME record cannot coexist with zone root, see RFC 1034 section 3.6.2');
				}
				if (0 === strncmp($subdomain, 'http:', 5) || 0 === strncmp($subdomain, 'https:', 6)) {
					return error('CNAME must be a hostname. A protocol was specified (http://, https://)');
				}
			}

			if ($rr !== 'ANY' && !\in_array($rr, static::$permitted_records, true)) {
				return error('%s: invalid resource record type', $rr);
			}

			if (false !== strpos($subdomain, ' ')) {
				return error("DNS record `%s' must not contain any spaces", $subdomain);
			}
			if (!static::HAS_ORIGIN_MARKER && substr($subdomain, -\strlen($zone)) === $zone) {
				$subdomain = substr($subdomain, 0, -\strlen($zone));
			}

			if (!static::HAS_ORIGIN_MARKER && $subdomain === '@') {
				$subdomain = '';
				warn("record `@' alias for domain - record stripped");
			}

			// many DNS services do not allow hosting a child as a standalone, work up the
			// chain to see if user added a subdomain as an addon domain (for "miscellaneous reasons")
			if (null !== ($parent = $this->getParent($zone))) {
				$subdomain = ltrim($subdomain . '.' . substr($zone, 0, -strlen($parent) - 1 /* period */), '.');
				$zone = $parent;
			}

			if (static::HAS_ORIGIN_MARKER && $subdomain === '') {
				$subdomain = '@';
			}

			if (!$param) {
				return true;
			}

			if ($rr == 'MX' && preg_match('/(\S+) (\d+)$/', $param, $mx_flip)) {
				// user entered MX record in reverse, e.g. mail.apisnetworks.com 10
				$param = $mx_flip[2] . ' ' . $mx_flip[1];
			}

			// per RFC 4408 section 3.1.3,
			// TXT records limits contiguous length to 255 characters, but
			// may also concatenate
			if ($rr == 'TXT') {
				if ($param[0] !== '"' && $param[strlen($param) - 1] !== '"') {
					$param = '"' . str_replace('"', '\\"', $param) . '"';
				}
				if (static::HAS_CONTIGUOUS_LIMIT) {
					$param = '"' . implode('" "', str_split(trim($param, '"'), 253)) . '"';
				}
			}

			return true;
		}

		/**
		 * Abides by DNS RFC that restricts DNS records from having an apex CNAME
		 *
		 * See also RFC 1034 section 3.6.2
		 *
		 * @return bool
		 */
		protected function hasCnameApexRestriction(): bool
		{
			return true;
		}

		/**
		 * Modify a DNS record
		 *
		 * @param string $zone
		 * @param string $subdomain
		 * @param string $rr
		 * @param string $parameter
		 * @param array  $newdata new zone data (name, rr, ttl, parameter)
		 * @return bool
		 */
		public function modify_record(
			string $zone,
			string $subdomain,
			string $rr,
			string $parameter,
			array $newdata
		): bool {
			if (!$this->owned_zone($zone)) {
				return error($zone . ': not owned by account');
			}

			$ttl = (int)$this->get_default('ttl');
			if (!$this->canonicalizeRecord($zone, $subdomain, $rr, $parameter, $ttl)) {
				return false;
			}

			$newdata = static::createRecord($zone, array_merge([
				'name'      => $subdomain,
				'rr'        => $rr,
				'ttl'       => null,
				'parameter' => $parameter
			], $newdata));

			if (!$this->canonicalizeRecord($zone, $newdata['name'], $newdata['rr'], $newdata['parameter'],
				$newdata['ttl'])) {
				return false;
			}

			$rectmp = preg_replace('/\.?' . $zone . '\.?$/', '', $newdata['name']);

			if ($newdata['name'] !== $subdomain && $newdata['rr'] !== $rr &&
				$this->record_exists($zone, $rectmp, $newdata['rr'], $parameter)
			) {
				return error('Target record `' . $newdata['name'] . "' exists");
			}

			$old = static::createRecord($zone, [
				'name'      => $subdomain,
				'rr'        => $rr,
				'parameter' => $parameter
			]);

			if (false === ($ret = $this->atomicUpdate($zone, $old, $newdata))) {
				// nsUpdate failed, rollback records
				warn('record update failed');

				return (($subdomain === $newdata['name'] && $rr === $newdata['rr']) ||
						!$this->record_exists($zone, $subdomain, $rr, $parameter)) &&
					$this->record_exists($zone, $newdata['name'], $newdata['rr'], $newdata['parameter']);
			}

			return (bool)$ret;
		}

		/**
		 * Create a record using appropriate driver
		 *
		 * @param string $zone
		 * @param array  $data
		 * @return mixed
		 */
		protected static function createRecord(string $zone, array $data = []): \Opcenter\Dns\Record
		{
			$cls = static::getNamespace() . '\\Record';
			if (!class_exists($cls)) {
				$cls = \Opcenter\Dns\Record::class;
			}

			return new $cls($zone, $data);
		}

		/**
		 * DNS record exists
		 *
		 * @param string $zone
		 * @param string $subdomain
		 * @param string $rr
		 * @param string $parameter
		 * @return bool
		 */
		public function record_exists(
			string $zone,
			string $subdomain,
			string $rr = 'ANY',
			string $parameter = ''
		): bool {
			static $failed = [];
			if ($subdomain == '@') {
				$subdomain = '';
				if (!static::HAS_ORIGIN_MARKER) {
					warn("record `@' alias for domain - record stripped");
				}
			}
			// args passed as [zone:domain.com, subdomain:sub.domain.com]
			if ($subdomain && $subdomain[-1] === '.' && substr($subdomain, -\strlen($zone) - 1, -1) === $zone) {
				$subdomain = rtrim(substr($subdomain, 0, -\strlen($zone)-1), '.');
			}
			$record = trim($subdomain . '.' . $zone, '.');
			$rr = strtoupper($rr);
			if (null === static::record2const($rr)) {
				return error("unknown RR class `%s'", $rr);
			}
			$hostingns = array_diff($this->get_hosting_nameservers($zone), $failed);
			if (!$hostingns) {
				return warn("No hosting nameservers configured for `%s', cannot determine if record exists", $zone);
			}
			$status = Util_Process::exec(static::DIG_SHLOOKUP, [
					'nameserver' => $ns = $hostingns[array_rand($hostingns)],
					'rr' => escapeshellarg($record),
					'hostname' => array_key_exists($rr, static::$rec_2_const) ? $rr : 'ANY'
				]
			);
			if ($status['return'] === 9) {
				// nameserver timed out
				$failed[] = $ns;
				return warn("Query to `%s' failed - disabling from future queries", $ns);
			}
			// make sure there is some data in the response
			if (!$parameter) {
				$parameter = '.';
			} else {
				$parameter = str_replace("'", "\\'", preg_quote($parameter, '!'));
			}

			return (bool)preg_match('!' . $parameter . '!i', $status['output']);
		}

		/**
		 * Perform an atomic update of a record allowing reversion on failure
		 *
		 * @param string               $zone
		 * @param \Opcenter\Dns\Record $old
		 * @param \Opcenter\Dns\Record $newdata
		 * @return bool record succeeded
		 */
		protected function atomicUpdate(string $zone, \Opcenter\Dns\Record $old, \Opcenter\Dns\Record $newdata): bool
		{
			// Throw an exception to halt reversion
			return false;
		}

		/**
		 * Create DNS zone privileged mode
		 *
		 * @param string $domain
		 * @param string $ip
		 * @return bool
		 */
		public function add_zone_backend(string $domain, string $ip): bool
		{
			if (!static::MASTER_NAMESERVER) {
				return error("rndc not configured in config.ini. Cannot add zone `%s'", $domain);
			}
			$buffer = Error_Reporter::flush_buffer();
			$res = $this->get_zone_data($domain);

			if (null !== $res) {
				Error_Reporter::set_buffer($buffer);
				warn("DNS for zone `%s' already exists, not overwriting", $domain);

				return true;
			}
			// make sure DNS does not exist yet for the parent
			[$a, $b] = explode('.', $domain, 2);
			$res = $this->get_zone_data($b);
			Error_Reporter::set_buffer($buffer);
			if (null !== $res) {
				warn("DNS for zone `%s' already exists, not overwriting", $b);
				return true;
			}

			if (is_array($ip)) {
				$ip = array_pop($ip);
			}

			if (inet_pton($ip) === false) {
				return error("`%s': invalid address", $ip);
			}

			info("Added domain `%s'", $domain);

			return true;
		}

		/**
		 * Check whether IP is assigned
		 *
		 * Assigned IP addresses will have PTRs. Unassigned will be empty.
		 *
		 * @param $ip string ip address
		 * @return bool
		 */
		public function ip_allocated(string $ip): bool
		{
			return \Opcenter\Net\IpCommon::ip_allocated($ip);
		}

		/**
		 * Import zone data for domain, overwriting configuration on server
		 *
		 * @param string      $domain
		 * @param string      $nameserver
		 * @param string|null $key
		 * @return bool
		 */
		public function import_from_ns(string $domain, string $nameserver, $key = null): bool
		{
			$myip = Util_Conf::server_ip();
			$domain = strtolower($domain);
			if (!preg_match(Regex::DOMAIN, $domain)) {
				return error("invalid zone `%s' - not a domain name", $domain);
			}

			if (!preg_match(Regex::DOMAIN, $nameserver)) {
				return error("invalid nameserver to query `%s'", $nameserver);
			}

			if ($key && false === strpos($key, ':')) {
				return error("invalid dns key `%s', must be in format name:key", $key);
			}
			if ($key) {
				$key = '-y' . $key;
			}

			$cmd = 'dig %(key)s -b%(ip)s @%(nameserver)s %(zone)s +norecurse +nocmd +nonssearch +noadditional +nocomments +nostats AXFR ';
			$proc = Util_Process_Safe::exec($cmd, array(
				'key'        => $key,
				'ip'         => $myip,
				'zone'       => $domain,
				'nameserver' => $nameserver
			));
			$output = $proc['output'];
			if (!$proc['success'] || false !== strpos($output, '; Transfer failed')) {
				$output = $proc['stderr'] ?: $proc['output'];

				return error('axfr failed: %s', $output);
			}

			return $this->import($domain, output);
		}

		/**
		 * Import records from hosted domain
		 *
		 * @param string $domain target domain to import records into
		 * @param string $src    hosted domain to derive records from
		 * @return bool
		 */
		public function import_from_domain(string $domain, string $src): bool
		{
			if ($domain === $src) {
				return error('Cannot import - target is same as source');
			}
			if (false === ($recs = $this->export($src))) {
				return false;
			}
			return $this->import(
				$domain,
				preg_replace('!(\b|^)' . preg_quote($src, '!') . '(\b|$)!m', $domain, $recs)
			);
		}

		/**
		 * Import raw AXFR records into zone
		 *
		 * @param string $domain
		 * @param string $axfr
		 * @return bool
		 */
		public function import(string $domain, string $axfr): bool
		{
			// empty out old zone first
			$nrecs = 0;
			$zoneinfo = $this->get_zone_information($domain);
			if (null === $zoneinfo) {
				return error('unable to get old zone information - is this domain added to your account?');
			}
			foreach ($zoneinfo as $rr => $recs) {
				foreach ($recs as $rec) {
					$tmp = strtoupper($rr);
					// skip SOA + apex record
					if ($tmp === 'SOA' || ($tmp === 'NS' && !$rec['subdomain'])) {
						continue;
					}
					if (!$this->remove_record($domain, $rec['subdomain'], $rr, $rec['parameter'])) {
						warn('failed to purge record `%s` %s %s %s',
							$rec['name'],
							$rr,
							$rec['ttl'],
							$rec['parameter']
						);
						continue;
					}
					$nrecs++;
				}
			}
			info('purged %d old records', $nrecs);

			$nrecs = 0;
			$regex = \Regex::compile(\Regex::DNS_AXFR_REC_DOMAIN,
				[
					'rr' => implode('|', static::$permitted_records + [99999 => 'SOA']),
					'domain' => str_replace('.', '\\.', $domain)
				]);
			foreach (preg_split("/[\r\n]{1,2}/", $axfr) as $line) {

				if ('' === $line || $line[0] == ';') {
					continue;
				}
				if (!preg_match($regex, $line, $rec)) {
					continue;
				}
				$tmp = strtoupper($rec['rr']);
				// skip SOA + apex record
				if ($tmp === 'SOA' || ($tmp === 'NS' && !$rec['subdomain']) || ($rec['class'] ?? 'IN') !== 'IN') {
					continue;
				}
				$subdomain = trim($rec['subdomain'], '.');
				$rec['parameter'] = preg_replace('/\s+/', ' ', $rec['parameter']);
				if (!$this->add_record($domain, $subdomain, $rec['rr'], $rec['parameter'], (int)$rec['ttl'])) {
					warn('failed to add record `%s` -> `%s` (RR: %s, TTL: %s)',
						$subdomain,
						$rec['parameter'],
						$rec['rr'],
						$rec['ttl']
					);
					continue;
				}
				$nrecs++;
			}

			return info('imported %d records', $nrecs);
		}

		/**
		 * Validate DNS template
		 *
		 * @param string $file
		 * @param array  $overrideParams additional params to pass to site creation
		 * @return bool
		 */
		public function validate_template(string $file, array $overrideParams = []): bool
		{
			$blade = BladeLite::factory('templates/dns');
			if (!$blade->exists($file)) {
				return error("Requested DNS template `%s' does not exist", $file);
			}
			$acct = \Error_Reporter::silence(static function() use ($overrideParams) {
				return Ephemeral::create($overrideParams);
			});

			if (null === $acct) {
				return error('Failed to create test account to evaluate DNS template');
			}

			$zoneTxt = $blade->render($file, [
				'svc'       => SiteConfiguration::shallow($acct->getContext()),
				'ttl'       => 1800,
				'zone'      => $acct->getContext()->domain,
				'subdomain' => '',
				'hostname'  => $acct->getContext()->domain,
				'ips'       => array_filter([\Opcenter\Net\Ip4::my_ip(), \Opcenter\Net\Ip6::my_ip()])
			]);

			\Error_Reporter::silence(static function() use ($acct) {
				return $acct->destroy();
			});

			$regex = \Regex::compile(\Regex::DNS_AXFR_REC_DOMAIN,
				[
					'rr'        => implode('|', static::$permitted_records + [99999 => 'SOA']),
					'domain'    => str_replace('.', '\\.', $acct->getContext()->domain),
				]);

			foreach (explode("\n", $zoneTxt) as $line) {
				if (empty(trim($line))) {
					continue;
				}
				if (!preg_match($regex, $line)) {
					return error("Zone template `%s' failed on line: %s", $file, $line);
				}
			}
			return true;
		}

		/**
		 * array get_zone_information (string)
		 *
		 * Reads zone information for a given domain on the nameservers.
		 *
		 * @param string|null $domain domain or current domain to check
		 * @return array
		 */
		public function get_zone_information(string $domain = null): ?array
		{
			$domain = $domain ?? $this->domain;

			if (!$this->permission_level & PRIVILEGE_ADMIN && !$this->owned_zone($domain)) {
				error('access denied - cannot view zone `' . $domain . "'");

				return null;
			}
			$rec = $this->get_zone_data($domain);
			if (null === $rec) {
				error('Non-authoritative for zone ' . $domain);

				return null;
			}

			return $rec;

		}

		/**
		 * bool remove_record (string, string)
		 * Removes a record from a zone.
		 *
		 * @param  string $zone      base domain
		 * @param  string $subdomain subdomain, leave blank for base domain
		 * @param  string $rr        resource record type, possible values:
		 *                           [MX, TXT, A, AAAA, NS, CNAME, DNAME, SRV]
		 * @param  string $param     record context
		 * @return bool operation completed successfully or not
		 *
		 */
		public function remove_record(string $zone, string $subdomain, string $rr, string $param = ''): bool
		{
			$subdomain = rtrim($subdomain, '.');
			if (!$zone) {
				$zone = $this->domain;
			}
			if (!$this->owned_zone($zone)) {
				return error($zone . ': not owned by account');
			}

			if (!$this->canonicalizeRecord($zone, $subdomain, $rr, $param)) {
				return false;
			}
			// only supply parameter if parameter is provided


			/**
			 * Now purge the record
			 */

			return true;
		}

		public function release_ip(string $ip): bool
		{
			deprecated_func('use ipinfo_release_ip');

			return $this->ipinfo_release_ip($ip);
		}

		/**
		 * Get module default
		 *
		 * @param string $key
		 * @return mixed|null
		 */
		public function get_default(string $key)
		{
			switch (strtolower($key)) {
				case 'ttl':
					return static::DNS_TTL;
				case 'apex-ns':
					return static::SHOW_NS_APEX;
				default:
					return null;
			}
		}

		/**
		 * Get DNS minimum TTL
		 *
		 * @XXX DO NOT USE FOR VALIDATING TTL. DEFER TO API ALWAYS.
		 *      Modules have special restrictions: CloudFlare allows "1" for automatic
		 *
		 * @return int
		 */
		public function min_ttl(): int
		{
			return static::DNS_TTL_MIN;
		}

		public function _delete()
		{
			if (!$this->configured()) {
				return info("DNS not configured for `%s', bypassing DNS hooks", $this->domain);
			}
			if (!$this->getServiceValue('ipinfo', 'namebased')) {
				$ips = (array)$this->getServiceValue('ipinfo', 'ipaddrs');
				// pass the domain to verify the PTR isn't detached incorrectly
				// from another domain that has recycled it
				$domain = $this->getServiceValue('siteinfo', 'domain');
				foreach ($ips as $ip) {
					$this->__deleteIP($ip, $domain);
				}
			}

			return true;
		}

		/**
		 * Release PTR assignment from an IP
		 *
		 * @param        $ip
		 * @param string $domain confirm PTR rDNS matches domain
		 * @return bool
		 */
		protected function __deleteIP(string $ip, string $domain = null): bool
		{
			// @todo move to ipinfo
			return true;
		}

		/**
		 * Remove zone from nameserver
		 *
		 * @param string $domain
		 * @return bool
		 */
		public function remove_zone_backend(string $domain): bool
		{
			return warn("cannot remove zone - DNS provider `%s' not configured fully",
				$this->getServiceValue('dns', 'provider', 'builtin'));
		}

		public function _edit()
		{
			if (!$this->configured()) {
				return info("DNS not configured for `%s', skipping edit hook", $this->domain);
			}
			$ipconf_old = $this->getAuthContext()->conf('ipinfo', 'old');
			$ipconf_new = $this->getAuthContext()->conf('ipinfo', 'new');
			$domainold = \array_get($this->getAuthContext()->conf('siteinfo', 'old'), 'domain');
			$domainnew = \array_get($this->getAuthContext()->conf('siteinfo', 'new'), 'domain');
			$dnsconf_old = $this->getAuthContext()->conf('dns', 'old');
			$dnsconf_new = $this->getAuthContext()->conf('dns', 'new');

			if (\array_get($dnsconf_old, 'provider') !== array_get($dnsconf_new, 'provider') || array_get($dnsconf_new, 'enabled') && !array_get($dnsconf_old, 'enabled'))
			{
				$this->provisionProviderChange();
			}
			// domain name change via auth_change_domain()
			if ($domainold !== $domainnew) {
				$ip = (array)$this->publicIpWrapper('new', 4);
				$this->remove_zone($domainold);
				$this->add_zone($domainnew, $ip[0]);
				// domain name changed
				if (!$ipconf_new['namebased']) {
					$this->__changePTR($ip[0], $domainnew, $domainold);
				}
			}

			$aliasesnew = array_get($this->getAuthContext()->conf('aliases', 'new'), 'aliases', []);
			$aliasesold = array_get($this->getAuthContext()->conf('aliases', 'old'), 'aliases', []);
			$ip = $this->get_public_ip();

			$add = array_diff($aliasesnew, $aliasesold);
			$rem = array_diff($aliasesold, $aliasesnew);
			foreach ($add as $a) {
				$this->add_zone($a, $ip);
			}

			foreach ($rem as $r) {
				$this->remove_zone($r);
			}

			// enable ip hosting
			if ($ipconf_new === $ipconf_old && $this->getAuthContext()->conf('dns', 'old') === $this->getAuthContext('dns', 'new')) {
				return;
			}

			$ipadd = $ipdel = [];
			$ipnew = $this->publicIpWrapper('new', 4, 'ipaddrs');
			$ipold = $this->publicIpWrapper('old', 4, 'ipaddrs');
			if ($ipconf_old['namebased'] && !$ipconf_new['namebased']) {
				// enable ip hosting
				$ipadd = $ipnew;
			} else if (!$ipconf_old['namebased'] && $ipconf_new['namebased']) {
				// disable ip hosting
				$ipdel = $ipold;
			} else {
				// add/remove ip hosting
				$ipdel = array_diff((array)$ipold, (array)$ipnew);
				$ipadd = array_diff((array)$ipnew, (array)$ipold);
			}

			foreach ($ipdel as $ip) {
				// NB __changePTR is called before to update domain on change
				$this->__deleteIP($ip, $domainnew);
			}

			foreach ($ipadd as $ip) {
				$this->__addIP($ip, $domainnew);
			}

			// update nbaddrs
			$ipnew = $this->publicIpWrapper('new', 4, 'nbaddrs');
			$ipold = $this->publicIpWrapper('old', 4, 'nbaddrs');
			if ($ipconf_old['namebased'] && !$ipconf_new['namebased']) {
				// added ip-based hosting
				$ipadd = $this->publicIpWrapper('new', 4, 'ipaddrs');
				$ipdel = $ipold;
			} else if (!$ipconf_old['namebased'] && $ipconf_new['namebased']) {
				// removed ip-based hosting
				$ipdel = $this->publicIpWrapper('old', 4, 'ipaddrs');
				$ipadd = $ipnew;
			} else if ($ipconf_old['namebased'] === $ipconf_new['namebased'] && $ipconf_new['namebased']) {
				// no namebased change
				$ipdel = array_diff(
					(array)$this->publicIpWrapper('old', 4, 'nbaddrs'),
					(array)$this->publicIpWrapper('new', 4, 'nbaddrs')
				);
				$ipadd = array_diff(
					(array)$this->publicIpWrapper('new', 4, 'nbaddrs'),
					(array)$this->publicIpWrapper('old', 4, 'nbaddrs')
				);
			}

			// change DNS
			// there will always be a 1:1 pairing for IP addresses
			$domains = array_keys($this->web_list_domains());
			foreach ($ipadd as $newip) {
				$oldip = array_pop($ipdel);
				$newparams = array('ttl' => static::DNS_TTL, 'parameter' => $newip);
				foreach ($domains as $domain) {
					$records = $this->get_records_by_rr('A', $domain);
					foreach ($records as $r) {
						if ($r['parameter'] !== $oldip) {
							continue;
						}
						if (!$this->modify_record($r['domain'], $r['subdomain'], 'A', $oldip, $newparams)) {
							$frag = ltrim($r['subdomain'] . '.' . $r['domain'], '.');
							error('failed to modify record for `' . $frag . "'");
						} else {
							$pieces = array($r['subdomain'], $r['domain']);
							$host = trim(implode('.', $pieces), '.');
							info("modified `%s'", $host);
						}
					}
				}
			}

			return;
		}

		public function _create()
		{
			return true;
		}

		/**
		 * Provision fresh zone configuration on provider change
		 *
		 * @return bool|void
		 */
		private function provisionProviderChange()
		{
			if (!$this->configured()) {
				return info("DNS not configured for `%s', bypassing DNS hooks", $this->domain);
			}
			$ipinfo = $this->getAuthContext()->conf('ipinfo');
			$siteinfo = $this->getAuthContext()->conf('siteinfo');
			$domain = $siteinfo['domain'];
			$ip = (array)$this->publicIpWrapper('cur');
			$this->add_zone($domain, $ip[0]);

			if (!$ipinfo['namebased']) {
				$ips = array_merge($ip, (array)$ipinfo['ipaddrs']);
				foreach(array_unique($ips) as $ip) {
					$this->__addIP($ip, $siteinfo['domain']);
				}
			}
			if (!$this->domain_uses_nameservers($domain)) {
				warn("Domain `%s' doesn't use assigned nameservers. Change nameservers to %s",
					$domain, implode(',', $this->get_hosting_nameservers($domain))
				);
			}

			return true;
		}

		/**
		 * Helper function to fetch IP address from config
		 *
		 * @param string      $which
		 * @param int         $class
		 * @param string|null $svcvar class
		 * @return array
		 */
		protected function publicIpWrapper(string $which = 'cur', int $class = 4, string $svcvar = null): array
		{
			if ($class !== 4 && $class !== 6) {
				fatal("Unknown IP class `%s'", $class);
			}
			if ($which === 'current') {
				$which = 'cur';
			}
			$confctx = $this->getAuthContext()->conf('dns', $which);
			$proxyvar = 'proxy' . ($class === 4 ? '' : '6') . 'addr';
			if ($info = ($confctx[$proxyvar] ?? null)) {
				return $info;
			}
			$svccls = 'ipinfo' . ($class === 6 ? '6' : '');
			$confctx = $this->getAuthContext()->conf($svccls, $which);
			if (null === $svcvar) {
				$svcvar = $confctx['namebased'] ? 'nbaddrs' : 'ipaddrs';
			}
			return $confctx[$svcvar];
		}

		/**
		 * Add zone to DNS server
		 *
		 * @param string $domain
		 * @param string $ip
		 * @return bool
		 */
		public function add_zone(string $domain, string $ip): bool
		{
			if (!$this->configured()) {
				return warn("cannot create DNS zone for `%s' - DNS is not configured for account", $domain);
			}

			$buffer = Error_Reporter::flush_buffer();
			if ($this->getOldServices('dns') !== $this->getNewServices('dns')) {
				// flush cache to prevent false positives on provider change
				static::$zoneExistsCache[$domain] = null;
			}
			if (($parent = $this->getParent($domain)) || $this->zone_exists($domain)) {
				Error_Reporter::set_buffer($buffer);
				if ($parent) {
					warn("DNS for zone `%(domain)s' parented under `%(parent)s', not extending as separate zone",
						['domain' => $domain, 'parent' => $parent]);
				} else {
					warn("DNS for zone `%s' already exists, not overwriting", $domain);
				}

				return true;
			}

			if (!$this->query('dns_add_zone_backend', $domain, $ip)) {
				return false;
			}

			if (!$this->verified($domain) && !$this->verify($domain)) {
				warn("Domain `%s' must be verified before DNS activates", $domain);
			}

			// verify zone present; this is often async
			// @todo extend or exponential backoff?
			$data = null;
			$totalWait = 0;
			for ($i = 0; $i < 100; $i++) {
				if (null !== ($data = $this->zoneAxfr($domain))) {
					break;
				}
				$wait = 250000 * $i;
				$totalWait += ($wait/1000000);
				if ($totalWait >= DNS_VALIDATION_WAIT) {
					break;
				}
				usleep($wait);
			}
			if ($data !== null) {
				static::$zoneExistsCache[$domain] = true;
			} else {
				warn("%s master not reporting authoritative for zone `%s' - continuing to add DNS records",
					ucwords($this->get_provider()),
					$domain
				);
			}
			if ($this->permission_level & PRIVILEGE_ADMIN) {
				return warn('Zone added as administrator - unable to provision records automatically');
			}
			$records = $this->provisioning_records($domain) +
				append_config($this->email_provisioning_records($domain));
			foreach ($records as $record) {
				if ($this->record_exists($domain, $record['name'], $record['rr'], $record['parameter'])) {
					continue;
				}
				if (!$this->add_record($domain, $record['name'], $record['rr'], $record['parameter'],
					$record['ttl'])) {
					warn("Failed to add DNS record `%s' on `%s' (rr: %s)", $domain, $record['name'], $record['rr']);
				}
			}

			return true;
		}

		/**
		 * Hostname has parent
		 *
		 * @param string $hostname
		 * @return bool
		 */
		public function parented(string $hostname): bool
		{
			return $this->getParent($hostname) !== null;
		}

		/**
		 * Get hostname parent
		 *
		 * @param string $hostname
		 * @return string|null
		 */
		protected function getParent(string $hostname): ?string
		{
			// let's assume admin knows what she's doing
			if ($this->permission_level & PRIVILEGE_ADMIN) {
				return null;
			}
			$count = substr_count($hostname, '.');
			if ($count < 2) {
				return null;
			}
			$pieces = explode('.', $hostname, $count);
			// aliases:list-shared-domains lists domains *attached* with a defined document root,
			// we need to inspect all aliases attached to the site as well as the domain itself to see if this
			// domain is explicitly defined or fallthrough to /var/www/html
			$domains = [$this->getConfig('siteinfo', 'domain')] + append_config($this->aliases_list_aliases());

			$tmp = array_pop($pieces);
			do {
				if (\in_array($tmp, $domains, true)) {
					return $tmp;
				}
				$chk = array_pop($pieces);
				$tmp = "$chk.$tmp";
			} while ($tmp !== $hostname);

			return null;
		}

		/**
		 * Get public IPv4 address(es)
		 *
		 * @return string|array|null single IP, multiple IPs, or null if not configured
		 */
		public function get_public_ip()
		{
			$addr = $this->getServiceValue('dns','proxyaddr') ?: $this->common_get_ip_address();
			return \count($addr) > 1 ? $addr : ($addr[0] ?? null);
		}

		/**
		 * Get public IPv6 address(es)
		 *
		 * @return string|array|null single IP, multiple IPs, or null if not configured
		 */
		public function get_public_ip6()
		{
			$addr = $this->getServiceValue('dns', 'proxy6addr') ?: $this->common_get_ip6_address();

			return \count($addr) > 1 ? $addr : ($addr[0] ?? null);
		}

		/**
		 * Get base provisioning DNS records to setup automatically
		 *
		 * @param string $zone zone name
		 * @return \Opcenter\Dns\Record[]
		 */
		public function provisioning_records(string $zone): array
		{
			if (!IS_CLI) {
				return $this->query('dns_provisioning_records', $zone);
			}

			if (!$this->getConfig('dns','enabled')) {
				return [];
			}

			$ttl = $this->dns_get_default('ttl');
			$ips = [];
			if ($this->getServiceValue('ipinfo', 'enabled')) {
				$ips += (array)$this->get_public_ip();
			}
			if ($this->getServiceValue('ipinfo6', 'enabled')) {
				$ips += append_config((array)$this->get_public_ip6());
			}

			$template = BladeLite::factory('templates/dns')->render('dns', [
				'svc'       => \Opcenter\SiteConfiguration::shallow($this->getAuthContext()),
				'ttl'       => $ttl,
				'zone'      => $zone,
				'subdomain' => '',
				'hostname' => ltrim(implode('.', ['', $zone]), '.'),
				'ips'       => (array)$ips
			]);
			$regex = Regex::compile(Regex::DNS_AXFR_REC_DOMAIN, [
				'rr'     => implode('|', $this->dns_permitted_records() + [99999 => 'SOA']),
				'domain' => $zone
			]);
			if (!preg_match_all($regex, $template, $matches, PREG_SET_ORDER)) {
				debug('No provisioning records discovered from template');

				return [];
			}
			$records = [];
			foreach ($matches as $record) {
				$records[] = static::createRecord($zone, [
					'ttl'       => $record['ttl'],
					'parameter' => $record['parameter'],
					'rr'        => $record['rr'],
					'name'      => rtrim($record['subdomain'], '.')
				]);
			}

			return $records;
		}

		/**
		 * Get DNS UUID for host
		 *
		 * @return null|string
		 */
		public function uuid(): ?string
		{
			return DNS_UUID ?: null;
		}

		/**
		 * Add an IP address to hosting
		 *
		 * @param string $ip
		 * @param string $hostname
		 * @return bool
		 */
		protected function __addIP(string $ip, string $hostname = ''): bool
		{
			return true;
		}

		/**
		 * Lookup and compare nameservers for domain to host
		 *
		 * @param string $domain
		 * @return bool
		 */
		public function domain_uses_nameservers(string $domain): bool
		{
			if (!preg_match(Regex::DOMAIN, $domain)) {
				return error("malformed domain `%s'", $domain);
			}
			$hostingns = $this->get_hosting_nameservers($domain);
			if (!$hostingns) {
				// not configured under [dns] hosting_ns in config.ini
				return true;
			}
			$dns = mute(function () use ($domain) {
				return $this->get_authns_from_host($domain);
			});
			$found = false;
			if (!$dns) {
				return $found;
			}
			if (DNS_VANITY_NS) {
				$hostingns += append_config(DNS_VANITY_NS);
			}
			foreach ($dns as $ns) {
				if (in_array($ns, $hostingns, true)) {
					return true;
				}
			}

			return false;
		}

		/**
		 * Get configured hosting nameservers
		 *
		 * Toggled via config.ini > [dns] > hosting_ns
		 *
		 * @return array
		 */
		public function get_hosting_nameservers(string $domain = null): array
		{
			return DNS_HOSTING_NS;
		}

		/**
		 * Get authoritative nameservers for given hostname
		 *
		 * Example response:
		 *  Array
		 *   (
		 *   [0] => Array
		 *      (
		 *      [host] => ns2.apisnetworks.com
		 *      [type] => A
		 *      [ip] => 96.126.122.82
		 *      [class] => IN
		 *      [ttl] => 83137
		 *   )
		 * )
		 *
		 * @param string $host hostname
		 * @return array|null authoritative nameservers or resolver chain incomplete
		 */
		public function get_authns_from_host($host): ?array
		{
			$nameservers = static::RECURSIVE_NAMESERVERS;
			$authns = silence(static function () use ($host, $nameservers) {
				return dns_get_record($host, static::record2const('ns'), $nameservers);
			});
			if ($authns) {
				// domain is properly delegated, nameserver returns affirmative
				$tmp = array();
				foreach ($authns as $a) {
					if ($a['type'] == 'NS') {
						$tmp[] = $a['target'];
					}
				}

				return $tmp;
			}

			// domain delegated to hosting nameservers, but hosting servers don't
			// have dns provisioned yet for domain
			//
			// crawl
			$resolver = new Net_DNS2_Resolver([
				'nameservers' => $nameservers,
				'recurse'     => true
			]);
			try {
				$nameservers = $this->get_authns_from_host_recursive($host, $resolver);
			} catch (Net_DNS2_Exception $e) {
				warn("NS lookup failed for `%s': %s", $host, $e->getMessage());

				return array();
			}

			return $nameservers;
		}

		/**
		 * Fallback authoritative NS lookup
		 *
		 * Crawl the entire TLD hierarchy to find the last known nameserver
		 *
		 * @param string            $host
		 * @param Net_DNS2_Resolver $resolver
		 * @param string            $seen
		 * @return array|null nameservers or null if resolve failed before reaching end
		 */
		protected function get_authns_from_host_recursive($host, Net_DNS2_Resolver $resolver, $seen = ''): ?array
		{
			$components = explode('.', $host);
			$nameservers = null;
			try {
				$lookup = array_pop($components) . '.' . $seen;
				$res = silence(static function () use ($resolver, $lookup) {
					return $resolver->query($lookup, 'NS');
				});
				if ($res->answer) {
					$nameservers = array_filter(array_map(static function ($arr) {
						return gethostbyname($arr->nsdname);
					}, $res->answer));
					$resolver->setServers($nameservers);
				}
			} catch (Net_DNS2_Exception | \Error $e) {
				if ($components) {
					if (false !== strpos($e->getMessage(), 'member function open() on null')) {
						// invalid tld extension
						return null;
					}
					// resolver chain broken
					warn("failed to recurse on `%s': %s", $lookup, $e->getMessage());
				}

				return null;
			}
			if (!$components) {
				return array_map(static function ($a) {
					return $a->nsdname;
				}, $res->authority);
			}
			$resolver->recurse = 0;

			return $this->get_authns_from_host_recursive(implode('.', $components), $resolver, $lookup);

		}

		/**
		 * Remove a zone from DNS management
		 *
		 * No direct access to method as site.
		 *
		 * Use EditDomain or aliases:remove-domain to manage zone attachments.
		 * Certain DNS providers may also soft delete zones requiring manual removal.
		 *
		 * @param string $domain
		 * @param bool   $force  remove zone as admin bypassing all validation checks
		 * @return bool
		 */
		public function remove_zone(string $domain, bool $force = false): bool
		{
			if ($this->permission_level & PRIVILEGE_SITE && !$this->owned_zone($domain)) {
				return error("Unusual call to remove_zone() with specified unowned zone `%s' blocked", $domain);
			}

			if (!$this->configured()) {
				return warn("cannot remove DNS zone for `%s' - DNS is not configured for account", $domain);
			}

			if ($force) {
				return $this->remove_zone_backend($domain);
			}

			if (null !== ($parent = $this->getParent($domain))) {
				// @TODO cleaning up a parented domain and its children will be messy
				return true;
			}


			if (!$this->zone_exists($domain)) {
				return true;
			}
			if (false === ($record = $this->get_records(static::UUID_RECORD, 'TXT', $domain))) {
				// zone axfr failed?
				return warn("Zone transfer failed - ignoring removal of `%s'", $domain);
			}
			if (null !== ($uuid = $this->uuid()) && $uuid !== ($record = array_get($record, '0.parameter', null))) {
				return warn("Bypassing DNS removal. DNS UUID for `%s' is `%s'. Server UUID is `%s'", $domain, $record,
					$uuid);
			}
			if ($ret = $this->query('dns_remove_zone_backend', $domain)) {
				static::$zoneExistsCache[$domain] = null;
			}

			return $ret;
		}

		/**
		 * Test credentials against configured module
		 *
		 * @param string $provider DNS provider to test
		 * @param mixed  $key authentication key
		 * @return bool
		 */
		public function auth_test(string $provider = DNS_PROVIDER_DEFAULT, $key = ''): bool {
			if (!is_debug()) {
				return error('Only available in debug mode');
			}

			if (!\Opcenter\Dns::providerValid($provider)) {
				return error("DNS provider `%s' invalid", $provider);
			}

			if (!\Opcenter\Dns::providerHasHelper($provider)) {
				return true;
			}
			$helper = \Opcenter\Dns::getProviderHelper($provider);
			$ctx = new \Opcenter\Service\ConfigurationContext('dns', new SiteConfiguration($this->site));
			return $helper->valid($ctx, $key);
		}

		/**
		 * Change PTR name
		 *
		 * @param string $ip       IP address to alter
		 * @param string $hostname new PTR name
		 * @param string $chk      optional check hostname to verify
		 * @return bool
		 */
		protected function __changePTR(string $ip, string $hostname, string $chk = ''): bool
		{
			return true;
		}

		/**
		 * Query hosting nameservers for DNS records of named category
		 *
		 * {@see get_zone_information()}
		 *
		 * example:
		 * Account has two MX records assigned, first the
		 * default MX on debug.com, and a second user-created MX on debug.debug.com.
		 * debug.debug.com was designated an e-mail domain through Mail Routing
		 * {@see Email_Module::add_virtual_transport()}
		 *
		 * apis> $c->dns_get_records_by_rr("MX");
		 *
		 * array(2)
		 *    apis>
		 *    array(2) {
		 *      [0]=>
		 *      array(4) {
		 *        ["name"]=>
		 *        string(10) "debug.com."
		 *        ["class"]=>
		 *        string(2) "IN"
		 *        ["ttl"]=>
		 *        string(5) "86400"
		 *        ["parameter"]=>
		 *        string(18) "10 mail.debug.com."
		 *      }
		 *      [1]=>
		 *      array(4) {
		 *        ["name"]=>
		 *        string(16) "debug.debug.com."
		 *        ["class"]=>
		 *        string(2) "IN"
		 *        ["ttl"]=>
		 *        string(5) "86400"
		 *        ["parameter"]=>
		 *        string(24) "10 mail.debug.debug.com."
		 *      }
		 *    }
		 *
		 * @param  string $rr resource record [MX, A, AAAA, CNAME, DNAME, TXT, SRV]
		 * @param  string $zone
		 * @return array|null resource records
		 *
		 */
		public function get_records_by_rr(string $rr, string $zone = null): ?array
		{
			if (null === $zone) {
				$zone = $this->domain;
			}

			if (!$this->owned_zone($zone)) {
				if (!$this->owned_zone($rr)) {
					error('access denied - cannot view zone `' . $zone . "'");

					return null;
				}
				// confusing half-assed backwards
				// accept arguments in either form
				$t = $rr;
				$rr = $zone;
				$zone = $t;
			}

			$rr = strtoupper($rr);
			if ($rr !== 'ANY' && !in_array($rr, static::$permitted_records, true)) {
				error('%s: invalid resource record type', $rr);

				return null;
			}

			$recs = $this->get_zone_information($zone);
			if (!$recs) {
				return array();
			}
			if ($rr == 'ANY') {
				return $recs;
			}
			if (!isset($recs[strtoupper($rr)])) {
				return array();
			}

			return $recs[strtoupper($rr)];
		}

		/**
		 * Domain has been verified and permitted addition
		 *
		 * @param string $domain
		 * @return bool
		 */
		public function verified(string $domain): bool
		{
			return true;
		}

		/**
		 * Perform verification on domain
		 *
		 * @param string $domain
		 * @return bool
		 */
		public function verify(string $domain): bool
		{
			return true;
		}

		/**
		 * Get DNS zone challenges
		 *
		 * @param string $domain
		 * @return array ns, txt correspond to nameserver delegation, TXT record presence
		 */
		public function challenges(string $domain): array
		{
			return [];
		}

		public function _verify_conf(\Opcenter\Service\ConfigurationContext $ctx): bool
		{
			return true;
		}

		public function _create_user(string $user)
		{
			return;
		}

		public function _delete_user(string $user)
		{
			return;
		}

		public function _edit_user(string $userold, string $usernew, array $oldpwd)
		{
			return;
		}
	}
