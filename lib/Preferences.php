<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Preferences wrapper
	 *
	 * @todo share preferences across multiple sessions
	 *
	 * Class Preferences
	 */
	class Preferences implements ArrayAccess, IteratorAggregate, Serializable
	{
		const CACHE_KEY = 'userprf';
		const SESSION_KEY = 'preferences';
		const SYNCTS = '_ts';

		/**
		 * @var array list of classes that may be unserialized
		 */
		const WHITELIST_CLASSES = [
			\Module\Support\Webapps\MetaManager\Options::class,
			\stdClass::class
		];
		protected static $instance;
		/**
		 * @var string[] session IDs for multi-user contexts
		 */
		protected static $instanceMap = [];
		protected static $lock = 0;
		protected $validated = false;
		// @var \ArrayObject
		protected $preferences;
		protected $user;
		protected $readOnly = false;
		/**
		 * @var \Auth_Info_User multi-context: session id tagged to preference
		 */
		protected $ctx;
		/**
		 * @var apnscpFunctionInterceptor multi-context: instance to unlock read-only
		 */
		protected $afi;
		private $dirty = false;

		/**
		 * Preferences constructor.
		 *
		 *
		 * @param null $user
		 */
		private function __construct($user)
		{
			$this->validated = $user instanceof \Auth_Info_User || Auth::authenticated();
			if (!$this->validated) {
				return;
			}

			$this->guard();

			if ($user instanceof \Auth_Info_User) {
				$afi = \apnscpFunctionInterceptor::factory($user);
				$this->ctx = $user;
				$this->user = $user->username;
				$this->preferences = self::acquire($user, $afi);
			} else {
				// read-only loading secondary user preferences
				$this->preferences = new ArrayObject(\apnscpFunctionInterceptor::init()->common_get_user_preferences($user));
				$this->user = $user;
			}
		}

		public function serialize()
		{
			return \serialize(['ctx' => $this->ctx, 'readOnly' => $this->readOnly]);
		}

		public function unserialize($serialized)
		{
			$vars = \Util_PHP::unserialize($serialized);
			// user rename = boom
			$this->__construct($vars['ctx']);
			if (!($this->readOnly = $vars['readOnly'])) {
				$this->unlock(\apnscpFunctionInterceptor::factory($this->ctx));
			}
		}

		/**
		 * Inhibit saving preferences
		 */
		protected function guard()
		{
			$this->readOnly = true;
		}

		/**
		 * Return a read-only copy of preferences
		 *
		 * @param string|Auth_Info_User $user
		 * @return static
		 */
		public static function factory($user): self
		{
			if (!IS_CLI && $user instanceof \Auth_Info_User && $user->id === \session_id()) {
				return self::getInstance($user);
			}

			return new static($user);
		}

		/**
		 * Get active session
		 *
		 * @return static
		 */
		protected static function getInstance(\Auth_Info_User $user = null): self
		{
			if (null === self::$instance) {
				// pass \Auth_Info_User. Preference access happens in __wakeup during apnscpSession::restore_from_id()
				// but before \Auth::$_authProfile updates during impersonation, which when reverting back from a tunneled
				// impersonation (admin => site => user), going back from user to site can result in a profile mismatch from admin
				// @TODO add tunneling to unit test
				if (!\Auth::authenticated()) {
					return new static(null);
				}
				self::$instance = new static($user ?? \Auth::profile());
				self::$instance->unlock(\apnscpFunctionInterceptor::init());
			}

			return self::$instance;
		}

		public static function get($name, $default = null)
		{
			$prefs = self::getInstance();

			return array_get($prefs, $name, $default);
		}

		/**
		 * Return pref storage array
		 *
		 * Useful for get/exists
		 *
		 * @return ArrayObject
		 */
		public function all(): ArrayObject
		{
			return $this->preferences;
		}

		public static function set($name, $value)
		{
			/**
			 * @BUG when working with array_set + nested arrays,
			 *      the nested elements are accessed directly
			 *      as native arrays without invoking offsetSet
			 *      Thus to ensure config is sync'd open up
			 *      access to $dirty or rework Laravel's implementation
			 */
			$prefs = self::getInstance();
			$prefs->setDirty();
			return array_set($prefs, $name, $value);
		}

		public static function forget($name)
		{
			$prefs = self::getInstance();
			$prefs->setDirty();
			array_forget($prefs, $name);
		}

		private function setDirty(): void
		{
			$this->dirty = true;
			$this->preferences[self::SYNCTS] = microtime(true);
		}

		public static function exists($name)
		{
			$prefs = self::getInstance();

			return array_has($prefs, $name);
		}

		public static function write(): void
		{
			self::$instance = null;
		}

		public static function reload()
		{
			self::write();

			return self::getInstance();
		}

		public static function resetCaches(): void
		{
			self::$instance = null;
			self::$instanceMap = [];
		}

		public function unlock(\apnscpFunctionInterceptor $afi): self
		{
			if ($this->ctx && !$afi->context_matches_id($this->ctx->id)) {
				fatal("cannot unlock preferences - afi ID mismatch `%s'", $this->ctx->id);
			}
			if (!$this->ctx) {
				fatal('cannot unlock - preferences must be loaded from getAuthContext()');
			}
			$this->readOnly = false;
			if (!IS_CLI && $this->ctx === session_id()) {
				$this->preferences = self::acquire($this->ctx, \apnscpFunctionInterceptor::init());

				return $this;
			}
			$this->afi = $afi;
			$this->preferences = self::acquire($this->ctx, $afi);

			return $this;
		}

		public function __destruct()
		{
			if (!$this->dirty) {
				if ($this->ctx && isset(self::$instanceMap[$this->ctx->site_id][$this->ctx->user_id]['prefs']) &&
					$this->preferences === self::$instanceMap[$this->ctx->site_id][$this->ctx->user_id]['prefs'])
				{
					/*debug("Reference to pref map found but no usage encountered - %s %s", $this->id,
						spl_object_hash($this->preferences));*/
					self::release($this->ctx);
				}

				return;
			}

			$this->sync();
		}

		public function __debugInfo()
		{
			return [
				'id'    => $this->ctx->id,
				'prefs' => $this->preferences
			];
		}

		/**
		 * Force reload of Preferences
		 */
		public function freshen(): self
		{
			$this->dirty = false;
			if ($this->ctx->id === \session_id()) {
				self::$instance = null;
			}

			self::$instanceMap[$this->ctx->site_id][$this->ctx->user_id]['prefs']->exchangeArray(self::createPreferencesMap(
				\apnscpFunctionInterceptor::factory($this->ctx)
			));

			return $this;
		}

		/**
		 * Synchronize preferences
		 *
		 * @param bool $tsUpdate force dirty marker update
		 * @return bool
		 */
		public function sync(bool $tsUpdate = false): bool
		{
			if ($tsUpdate) {
				$this->setDirty();
			}

			$this->dirty = false;
			if ($this->ctx->id === \session_id()) {
				self::$instance = null;
			}

			if ($this->readOnly) {
				// assume write() was called
				fatal("preferences cannot be written in read-only mode, calling method: `%s'",
					\Error_Reporter::get_caller(2));
			}

			self::release($this->ctx);
			if (!$this->afi) {
				return debug('Preferences requested to be saved but no suitable process?');
			}


			if (!\apnscpSession::init()->exists($this->ctx->id)) {
				dlog("Preferences session `%s' missing on %s@%s - regenerating prior to sync",
					$this->ctx->id,
					$this->ctx->username,
					$this->ctx->domain
				);
				$ctx = \Auth::context($this->ctx->username, $this->ctx->domain);
			}
			// Drop the pending preferences forcing a write, then merge new with old
			// Doing so will result in loss of journal
			// @TODO journal in multicontext
			return apnscpFunctionInterceptor::factory($ctx ?? $this->ctx)->common_save_preferences($this->preferences->getArrayCopy());
		}

		public function dirty(): bool
		{
			// @XXX direct reference access via MetaManager breaks $dirty flag
			return $this->dirty;
		}

		/**
		 * Get preferences
		 *
		 * @param \Auth_Info_User           $ctx
		 * @param apnscpFunctionInterceptor $afi
		 * @return ArrayObject
		 */
		private static function acquire(\Auth_Info_User $ctx, \apnscpFunctionInterceptor $afi): ArrayObject
		{
			if (!$ctx->valid()) {
				$v = new ArrayObject();
				return $v;
			}

			if (!$afi->context_matches_id($ctx->id)) {
				fatal('Context does not match ID %s', $ctx->id);
			}

			if (isset(self::$instanceMap[$ctx->site_id][$ctx->user_id]['prefs'])) {
				self::$instanceMap[$ctx->site_id][$ctx->user_id]['ref']++;

				return self::$instanceMap[$ctx->site_id][$ctx->user_id]['prefs'];
			}

			self::$instanceMap[$ctx->site_id][$ctx->user_id] = [
				'prefs' => self::createPreferencesMap($afi),
				'ref'   => 1
			];

			return self::$instanceMap[$ctx->site_id][$ctx->user_id]['prefs'];
		}

		/**
		 * Fetch preferences from backend
		 *
		 * @param apnscpFunctionInterceptor $afi
		 * @return ArrayObject
		 */
		private static function createPreferencesMap(\apnscpFunctionInterceptor $afi): ArrayObject {
			return new ArrayObject((array)$afi->common_load_preferences());
		}

		/**
		 * Decrease reference count optionally releasing preference cache
		 *
		 * @param string $id
		 * @return bool
		 */
		private static function release(\Auth_Info_User $ctx): bool
		{
			if (!isset(self::$instanceMap[$ctx->site_id][$ctx->user_id]['ref'])) {
				return debug('Requested ID %s not found in preference cache', $ctx->id);
			}
			if (--self::$instanceMap[$ctx->site_id][$ctx->user_id]['ref'] > 0) {
				return false;
			}

			if (self::$instanceMap[$ctx->site_id][$ctx->user_id]['ref'] !== 0) {
				debug('Excessive decrement encountered - %s', $ctx->id);
			}
			unset(self::$instanceMap[$ctx->site_id][$ctx->user_id]);

			return true;
		}

		protected function multicontext(): bool
		{
			return (bool)$this->ctx;
		}

		public function getIterator()
		{
			return new ArrayIterator($this->preferences);
		}

		public function offsetExists($offset)
		{
			return null !== $this->preferences && $this->preferences->offsetExists($offset);
		}

		public function &offsetGet($offset)
		{
			return $this->preferences[$offset];
		}

		public function offsetSet($offset, $value)
		{
			if ($this->readOnly) {
				fatal("preferences loaded in read-only mode, cannot set `%s'", $offset);
			}
			$this->setDirty();
			$this->preferences[$offset] = $value;
		}

		public function offsetUnset($offset)
		{
			if ($this->readOnly) {
				fatal("preferences loaded in read-only mode, cannot set `%s'", $offset);
			}
			$this->setDirty();
			$this->preferences->offsetExists($offset) && $this->preferences->offsetUnset($offset);
		}

	}