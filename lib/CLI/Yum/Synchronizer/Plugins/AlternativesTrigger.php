<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace CLI\Yum\Synchronizer\Plugins;

	use CLI\Yum\Synchronizer\Utils;

	/**
	 * Class Binutils
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * @todo    support other packages
	 *
	 */
	abstract class AlternativesTrigger extends Trigger
	{
		use \FilesystemPathTrait;

		/**
		 * @var array
		 */
		protected $alternatives;

		public function update(string $package): bool
		{
			$svc = Utils::getServiceFromPackage($package);
			foreach ($this->getAlternatives() as $candidate) {
				$alternative = new Alternatives($svc, $candidate['name']);
				if (!$alternative->verify($candidate['src'], $candidate['dest'])) {
					info("Installing missing alternative `%s' => `%s'", $candidate['src'], $candidate['dest']);
					$alternative->install($candidate['src'], $candidate['dest'], $candidate['priority']);
				}
			}

			return true;
		}

		protected function getAlternatives(): array
		{
			return $this->alternatives;
		}

		public function install(string $package): bool
		{
			$svc = Utils::getServiceFromPackage($package);
			$status = true;
			foreach ($this->getAlternatives() as $candidate) {
				if (!file_exists($candidate['dest'])) {
					// old platform maybe?
					continue;
				}
				$status &= (new Alternatives($svc, $candidate['name']))->
				install($candidate['src'], $candidate['dest'], $candidate['priority']);
			}

			return true;
		}

		public function remove(string $package): bool
		{
			$svc = Utils::getServiceFromPackage($package);
			$status = true;
			foreach ($this->getAlternatives() as $candidate) {
				if (!file_exists($candidate['src']) || !file_exists($candidate['dest'])) {
					// old platform maybe?
					return true;
				}
				$status &= (new Alternatives($svc, $candidate['name']))->
				remove($candidate['dest']);
			}

			return true;
		}
	}