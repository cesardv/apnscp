<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace CLI\Yum\Synchronizer;

	use CLI\Yum\Synchronizer;
	use Opcenter\Service\ServiceLayer;

	/**
	 * Class CLI_Yum_Synchronizer_Generate
	 *
	 * Generate an index of RPM files
	 */
	class Resync extends Synchronizer
	{
		protected $force = false;

		public function __construct($force = '')
		{
			if ($force === '--force') {
				$this->force = true;
			}
		}

		public function run()
		{
			$db = \PostgreSQL::initialize()->getHandler();
			$res = pg_query($db, 'SELECT package_name, version, service, release FROM site_packages');
			while (false !== ($rs = pg_fetch_object($res))) {
				$package = $rs->package_name;
				$version = $rs->version;
				$service = $rs->service;
				$release = $rs->release;
				if (!Utils::packageInstalled($package)) {
					continue;
				}
				$meta = Utils::getMetaFromRPM($package);
				if (false === $meta) {
					// package removed
					warn("Package `%s' listed in site_packages table but missing from server - removing from `%s'",
						$package, $service);

					(new Remove($package))->run();
					continue;
				}

				if (!$release) {
					pg_query($db, "UPDATE site_packages SET release = '" . pg_escape_string($meta['release'])
						. "' WHERE package_name = '" . pg_escape_string($package) . "'");
					continue;
				} else if (!$this->force && $meta['release'] === $release && $meta['version'] === $version) {
					// no change
					continue;
				}

				$ret = (new Update($package, $meta['version'], $meta['release']))->run();
				if ($ret) {
					info("synchronizing `%s' to `%s'", $package, $service);
				} else {
					warn("failed to sync `%s' to `%s'", $package, $service);
				}
			}

			(new ServiceLayer(null))->dropVirtualCache();

			return true;
		}
	}