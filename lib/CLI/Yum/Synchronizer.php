<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace CLI\Yum;

	use CLI\Yum\Synchronizer\Plugins\Manager;
	use CLI\Yum\Synchronizer\Provides;
	use CLI\Yum\Synchronizer\Utils;
	use Opcenter\Filesystem\Mount;

	abstract class Synchronizer
	{
		const LOCK_FILE = 'synchronizer.lock';
		const FILESYSTEMTEMPLATE_PATH = FILESYSTEM_TEMPLATE;
		const COMMON_MOUNT_PATH = FILESYSTEM_SHARED;
		const ACCEPTABLE_ARCHES = ['noarch', 'x86_64'];
		const EQUIVALENCE_REMAPS = [
			'upgrade'    => 'update',
			'upgraded'   => 'updated',
			'reinstall'  => 'install',
			'downgrade'  => 'update',
			'downgraded' => 'updated'
		];

		/**
		 * @var string package name, e.g. yum
		 */
		protected $package;
		/**
		 * @var string package version, e.g. 3.2.22
		 */
		protected $version;
		// files located under here are mounted as an rbind
		// skip clobbering files under here
		/**
		 * @var string package release, e.g. 40.el5.centos
		 */
		protected $release;

		public function __construct($package, $version = null, $release = null)
		{
			$args = \func_get_args();
			while (0 === strncmp($args[0], '-', 1)) {
				array_shift($args);
			}
			$this->package = $args[0];
			$this->version = $args[1] ?? null;
			$this->release = $args[2] ?? null;
		}

		/**
		 * Create a new Synchronizer instance
		 *
		 * @param array $args ARGV: mode package version release
		 * @return CLI_Yum_Synchronizer|bool
		 */
		public static function factory($args)
		{
			if (false != ($pid = self::is_locked())) {
				fatal("already running? pid `%d'", $pid);
			}
			static::lock();
			$mode = $args[1];
			// remap dnf
			if (isset(self::EQUIVALENCE_REMAPS[$mode])) {
				$mode = self::EQUIVALENCE_REMAPS[$mode];
			}

			$arch = array_pop($args);
			if ($arch === 'i686' || $arch === 'i386') {
				// @TODO remove in a few months, post-actions needs update
				return true;
			} else if (!\in_array($arch, self::ACCEPTABLE_ARCHES, true)) {
				// panel hasn't run migrations yet?
				$args[] = $arch;
			}

			if ($mode === 'install') {
				if ($args[2] === '--update') {
					if (Utils::packageInstalled($args[3])) {
						unset($args[2]);
						$mode = 'update';
					} else {
						if (Manager::hasPlugin($args[3], 'trigger')) {
							// separate plugin exists, run it
							Manager::run($args[3], 'update');
						}
						// install happened, soft delete not found - let's ignore silently
						return true;
					}
				}
			} else if ($mode === 'updating') {
				// preupdate hook, ignore
				return true;
			} else if ($mode === 'updated') {
				// postupdate hook, fire
				$mode = 'update';
			} else if ($mode === 'obsoleted') {
				/**
				 * obsoleted packages are to be removed
				 * and appear before obsoleting calls
				 *
				 * obsoleted packages must be preserved to
				 * know the service under which it's installed
				 *
				 * let obsoleting remove the obsoleted and install itself
				 */
				return true;
			}
			$class = __CLASS__ . '\\' . ucwords($mode);
			if (!class_exists($class)) {
				return error("unknown mode `%s'", $mode);
			}
			$reflector = new \ReflectionClass($class);
			if ($reflector) {
				$c = $reflector->newInstanceArgs(\array_slice($args, 2));
			}

			return $c;
		}

		/**
		 * Get locked pid
		 *
		 * @return bool|string
		 */
		protected static function is_locked()
		{
			$f = static::getLock();
			if (!file_exists($f)) {
				return false;
			}
			$pid = file_get_contents($f);
			if (!is_dir('/proc/' . $pid)) {
				return false;
			}

			return $pid;
		}

		protected static function lock(): bool
		{
			$f = static::getLock();
			file_put_contents($f, getmypid());
			register_shutdown_function(static function () {
				static::unlock();
			});

			return true;
		}

		public static function unlock()
		{
			$lock = static::getLock();
			if (file_exists($lock)) {
				unlink($lock);
			}
		}

		protected static function getLock()
		{
			return run_path(self::LOCK_FILE);
		}

		abstract public function run();

		/**
		 * Get synchronizer mode
		 *
		 * @return string ['update','remove']
		 */
		public function getMode()
		{
			$class = static::class;
			$type = substr($class, strrpos($class, '_') + 1);

			return strtolower($type);
		}

		protected function getFilesFromCache($package)
		{
			return Provides::fetch($package);

		}

		protected function installFile($file, $svcpath)
		{
			$fstfile = $svcpath . '/' . ltrim($file, '/');
			clearstatcache(true, $file);

			if ($this->isCommonMount($file)) {
				// file installed under common mount point
				return true;
			} else if (SkipList::in($file)) {
				info("add bypass: in skiplist `%s'", $file);
				return -1;
			} else if (file_exists($fstfile) && fileinode($fstfile) === fileinode($file)) {
				// packages can provide duplicate files
				return true;
			} else if (!file_exists($file) && !is_link($file)) {
				// file does not exist :'(
				return false;
			}

			/**
			 * in a properly provisioned FST, there should
			 * be no need to build up parent directories of RPMs
			 * these directories should likewise also be declared
			 * in the rpm manifest
			 */
			$parent = \dirname($fstfile);
			if (!file_exists($parent)) {
				$this->createParent(\dirname($file), $svcpath);
			}

			$type = filetype($file);
			if ($type === 'link') {
				$stat = lstat($file);
			} else {
				$stat = stat($file);
			}

			if ($type != 'link') {
				// link perms do not matter
				// grab sticky/set[gu]id bits where appropriate
				$perms = $stat['mode'] & 0x00FFF;
			}
			if ($type === 'dir') {
				if (!\is_dir($fstfile)) {
					(mkdir($fstfile, 0755) && is_dir($fstfile)) || warn("failed to create directory `%s'", $fstfile);
				}
			} else if ($type === 'file') {
				if (Mount::sameMount($file, $parent)) {
					// inode is checked earlier, something screwed up
					if (file_exists($fstfile)) {
						warn("File `%s' resides under same mountpoint but FST inode `%d' mismatches `%d' inode - unlinking file first",
							$fstfile, fileinode($fstfile), fileinode($file)
						);
						unlink($fstfile);
					}
					link($file, $fstfile);
				} else {
					copy($file, $fstfile);
				}
			} else if ($type === 'link') {
				$link = readlink($file);
				/**
				 * Some RPMs duplicate symlinks in devel package, which isn't always present
				 * in another layer. PHP checks if the target is accessible prior to creation
				 * Ex: libgs.so in ssh/
				 */
				if ($link[0] !== '/' && !file_exists(\dirname($fstfile) . '/' . $link)) {
					if (\is_link($tmp = \dirname($fstfile) . '/' . $link)) {
						unlink($tmp);
					}
					$ret = \Util_Process_Safe::exec('ln -sf %(target)s %(link)s',
						['target' => $link, 'link' => $fstfile]);
					if (!$ret['success']) {
						warn("Failed to make symlink `%s' using ln", $fstfile);
					}
				} else {
					// @XXX honor link referents.
					// Referent to bad referent will trigger warning
					@symlink($link, $fstfile);
				}
			} else {
				warn("don't know how to handle `%s' for `%s'", $type, $file);

				return false;
			}

			$owner = $stat['uid'];
			$group = $stat['gid'];
			if ($owner || $group) {
				if ($type === 'link') {
					lchown($fstfile, $owner);
					lchgrp($fstfile, $group);
				} else {
					chown($fstfile, $owner);
					chgrp($fstfile, $group);
				}
			}

			if ($type !== 'link') {
				chmod($fstfile, $perms);
			}

			return true;
		}

		/**
		 * File resides under shared mount path (/.socket)
		 * @param $file
		 * @return bool
		 */
		protected function isCommonMount($file)
		{
			return 0 === strpos(realpath($file), self::COMMON_MOUNT_PATH);
		}

		/**
		 * Creatr parent directories
		 *
		 * @param string $dir
		 * @param string $svcpath
		 * @return bool
		 */
		protected function createParent(string $dir, string $svcpath): bool
		{
			$path = $svcpath . '/' . $dir;
			$parent = \dirname($dir);
			if (!$dir) {
				return error("missing service path `%s'", $svcpath);
			} else if (file_exists($path)) {
				return true;
			} else if ($dir !== '/' && !file_exists($svcpath . $parent)) {
				$this->createParent($parent, $svcpath);
			}

			$perms = 0755;
			$owner = $group = 0;
			if (file_exists($dir)) {
				$perms = fileperms($dir);
				$owner = fileowner($dir);
				$group = filegroup($dir);
			}

			return mkdir($path, $perms) && chown($path, $owner) && chgrp($path, $group);
		}

		protected function removeFile($file, $svcpath)
		{
			if ($this->isCommonMount($file)) {
				// file installed under common mount point
				return true;
			} else if (SkipList::in($file)) {
				info("remove bypass: in skiplist `%s'", $file);

				return -1;
			}
			if (!$file = ltrim($file, '/')) {
				// garbage file, not in synchronizer cache?
				return;
			}

			$tmp = $svcpath . '/' . $file;
			if (!is_link($tmp) && !file_exists($tmp)) {
				warn("file `%s' missing from service path `%s'", $file, $svcpath);

				return false;
			}
			if (!is_link($tmp) && is_dir($tmp)) {
				// glob won't return dangling symlinks, check and filter . + ..
				return (\count(scandir($tmp, SCANDIR_SORT_NONE)) === 2 && rmdir($tmp)) || debug("failed to rmdir `%s'",
						$tmp);
			}

			return unlink($tmp) || warn("failed to unlink `%s'", $tmp);
		}
	}