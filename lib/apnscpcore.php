<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	if (defined('APNSCP_VERSION')) {
		return;
	}
	ob_start();
	$_ENV['HOSTNAME'] = $_ENV['HOSTNAME'] ?? ($_SERVER['HOSTNAME'] ?? (getenv('HOSTNAME') ?: gethostname()));
	if (PHP_SAPI !== 'cli' && PHP_SAPI !== 'phpdbg') {
		define('STDERR', fopen('php://stderr', 'wb'));
		define('STDOUT', fopen('php://stdout', 'wb'));
	}
	include INCLUDE_PATH . '/lib/helpers.php';
	include INCLUDE_PATH . '/lib/error_reporter.php';
	include INCLUDE_PATH . '/lib/log_wrapper.php';
	include INCLUDE_PATH . '/lib/apnscpfunction.php';
	class_alias('apnscpFunctionInterceptor', 'a23r');
	spl_autoload_register('apnscpFunctionInterceptor::autoload');

	include INCLUDE_PATH . '/vendor/autoload.php';
	include INCLUDE_PATH . '/lib/constants.php';

	$tmp = conf_path('custom/vendor/autoload.php');
	if (file_exists($tmp)) {
		include $tmp;
	}

	if (extension_loaded('mbstring')) {
		mb_internal_encoding('UTF-8');
	}

	if (extension_loaded('intl') && isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
		Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']);
	}

	if (IS_CLI && (!extension_loaded('pcntl') || !extension_loaded('posix'))) {
		die('fatal: missing pcntl and posix extensions');
	}

	if (!extension_loaded('igbinary')) {
		function_exists('dl') && dl('igbinary.' . PHP_SHLIB_SUFFIX)
		or die('fatal: igbinary extension not loaded');
	}

	if (!extension_loaded('redis')) {
		function_exists('dl') && dl('redis' . PHP_SHLIB_SUFFIX)
		or die('fatal: redis extension not loaded');
	}

	if (!extension_loaded('dom')) {
		function_exists('dl') && dl('dom' . PHP_SHLIB_SUFFIX)
		or die('fatal: dom extension not loaded');
	}

	if (!function_exists('apcu_fetch') && !extension_loaded('apcu')) {
		function_exists('dl') && dl('apcu' . PHP_SHLIB_SUFFIX)
		or die('fatal: apcu extension not loaded');
	}

	if (\HTML_Kit::jsonify()) {
		define('AJAX', true);
		\Error_Reporter::set_verbose(0);
	} else {
		$level = !is_debug() ? 0 : (int)DEBUG_BACKTRACE_QUALIFIER;
		// debug, only backtrace error/warning
		if (!posix_getuid()) {
			$level = max($level, 1);
		}
		Error_Reporter::set_verbose($level);
	}
	if (BUG_REPORT) {
		Error_Reporter::set_report(BUG_REPORT);
	}

	Error_Reporter::add_filter(
		new class implements \Error_Reporter\ReportFilterInterface
		{
			public function filter($errno, $errstr, $errfile, $errline, $errcontext)
			{
				if ($errno !== \Error_Reporter::E_FATAL) {
					return false;
				}
				$time = date('Hm');

				// nightly updates
				return ($time >= 200 && $time <= 400);
			}
		});

	if (!is_debug()) {
		// gradually transitioning to strict typing
		Error_Reporter::add_filter(
			new class implements \Error_Reporter\ReportFilterInterface
			{
				public function filter($errno, $errstr, $errfile, $errline, $errcontext)
				{
					if ($errno & ~\Error_Reporter::E_EXCEPTION || !is_object($errcontext)) {
						return false;
					}
					if ($errcontext instanceof \TypeError) {
						// let strict typing trigger fatal in dev
						// incorrect argument types are \TypeError
						Error_Reporter::report('%s', [str_replace('%', '%%', var_export($errcontext, true))]);

						return true;
					}
				}
			}
		);
	}

	// mask interrupted select() calls from signals
	Error_Reporter::suppress_php_error('socket_select');
	Error_Reporter::suppress_php_error('stream_select');

	Error_Reporter::add_filter(
		new class implements \Error_Reporter\BacktraceFilterInterface
		{
			public function filter(array $caller)
			{
				if (isset($caller['function'])) {
					if (isset($caller['class']) && $caller['class'] === Error_Reporter::class &&
						($caller['function'] === 'merge_buffer' || 0 === strncmp($caller['function'], 'add_', 4))) {
						return true;
					}
					if ($caller['function'] === 'call_user_func_array' || $caller['function'] === 'call_user_func'
						|| $caller['function'] === '_invoke') {
						return true;
					}
				}
				if (isset($caller['class'])) {
					return 0 === strpos(\apnscpFunctionInterceptor::MODULE_SKELETON_NAMESPACE,
							$caller['class']) || $caller['class'] === 'afiProxy';
				}

				return false;
			}
		}
	);

	// ensure that INCLUDE_PATH is set immediately after . and preferrably
	// before /usr/share/pear
	$include = explode(PATH_SEPARATOR, ini_get('include_path'));
	$key = array_search('.', $include, true);
	if ($key !== false) {
		unset($include[$key]);
	}
	$include = implode(PATH_SEPARATOR,
		array_merge(array('.', INCLUDE_PATH . '/lib', '/usr/share/pear'), $include)
	);
	set_include_path($include);
	include INCLUDE_PATH . '/lib/apnscpsession.php';
	include INCLUDE_PATH . '/lib/apnscperror.php';

	include INCLUDE_PATH . '/lib/datastream.php';
	include INCLUDE_PATH . '/lib/mysql.php';

	if (PHP_SAPI === 'cli' && !headers_sent()) {
		apnscpSession::disable_session_header();
	}
	if (defined('SHIM') && SHIM) {
		class_alias(\Module\Skeleton\Standard::class, \Module_Skeleton::class, true);
		if (!IS_CLI) {
			apnscpSession::init();
		}

		if (file_exists($path = config_path('custom/boot.php'))) {
			include($path);
		}
		return;
	}

	include INCLUDE_PATH . '/lib/Auth.php';
	// HTML includes
	if (!IS_CLI) {
		/**
		 * @todo loading cache reinitializes classes, can't
		 *       pull $_SESSION['webhooks'] until classes
		 *       instantiated.. Module\Support\Auth can't resolve Module_Skeleton
		 */
		if (0 && is_debug()) {
			// testing for now
			class_alias(\Module\Skeleton\Webhooks::class, \Module_Skeleton::class, true);
		} else {
			class_alias(\Module\Skeleton\Standard::class, \Module_Skeleton::class, true);
		}
	} else {
		gc_disable();
		class_alias(\Module\Skeleton\Standard::class, \Module_Skeleton::class, true);
		// Backend includes
		ob_end_flush();
		if (file_exists(INCLUDE_PATH . '/lib/lservicelib.php')) {
			include INCLUDE_PATH . '/lib/lservicelib.php';
		} else {
			Error_Reporter::suppress_php_error('ListenerServiceCommon::master');
			include INCLUDE_PATH . '/lib/lservicelib.pht';
		}

		if (file_exists($path = config_path('custom/boot.php'))) {
			include($path);
		}
		return;
	}

	if (!constant('NO_AUTH') && Auth::get_driver()::AUTO_AUTH) {
		Auth::handle();
	} else {
		\apnscpSession::init();
	}

	if (!\Error_Reporter::get_verbosity() && is_debug()) {
		// session debug via misc:debug-session
		Error_Reporter::set_verbose((int)DEBUG_BACKTRACE_QUALIFIER);
	}

	if (file_exists($path = config_path('custom/boot.php'))) {
		include($path);
	}

	ob_end_flush();