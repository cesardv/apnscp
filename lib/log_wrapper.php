<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	Error_Reporter::init();

	/**
	 * apnscp in development/debug mode
	 *
	 * @return bool
	 */
	function is_debug(): bool
	{
		return $_SESSION['DEBUG'] ?? (defined('DEBUG') && DEBUG);
	}

	/**
	 * Current request is AJAX
	 *
	 * @return bool
	 */
	function is_ajax(): bool
	{
		return defined('AJAX') && AJAX;
	}

	/**
	 * Trigger fatal error
	 *
	 * @param       $msg
	 * @param mixed $args
	 * @return void
	 */
	function fatal(string $msg, ...$args): void
	{
		Error_Reporter::trigger_fatal($msg, $args);
	}

	function success(string $msg, ...$args): bool
	{
		return Error_Reporter::add_success($msg, $args);
	}

	/**
	 * Raise error
	 *
	 * @param string $msg
	 * @param mixed  $args
	 * @return bool
	 */
	function error(string $msg, ...$args): bool
	{
		return Error_Reporter::add_error($msg, $args);
	}

	/**
	 * Raise nullable error
	 *
	 * @param string $msg
	 * @param mixed  ...$args
	 * @return null
	 */
	function nerror(string $msg, ...$args)
	{
		\Error_Reporter::add_error($msg, $args);
		return null;
	}

	/**
	 * Raise non-fatal warning
	 *
	 * @param string $msg
	 * @param mixed  $args
	 * @return bool
	 */
	function warn(string $msg, ...$args): bool
	{
		return Error_Reporter::add_warning($msg, $args);
	}

	/**
	 * Raise info
	 *
	 * @param       $msg
	 * @param mixed $args
	 * @return bool
	 */
	function info(string $msg, ...$args): bool
	{
		return Error_Reporter::add_info($msg, $args);
	}

	/**
	 * Log debugging message, not used in production
	 *
	 * @param string $msg
	 * @param mixed  $args
	 * @return bool
	 */
	function debug(string $msg, ...$args): bool
	{
		if (is_debug()) {
			Error_Reporter::add_debug($msg, $args);
		}

		return true;
	}

	/**
	 * Explicit debug
	 *
	 * @param string $msg
	 * @param mixed  ...$args
	 * @return bool
	 */
	function debug_ex(string $msg, ...$args): bool
	{
		Error_Reporter::add_debug($msg, $args);

		return true;
	}

	/**
	 * Warn deprecated feature
	 *
	 * @param string $msg
	 * @param mixed  $args
	 * @return bool
	 */
	function deprecated(string $msg, ...$args): bool
	{
		if (!is_debug()) {
			return true;
		}

		if (Error_Reporter::get_caller(1) === 'deprecated_func') {
			return Error_Reporter::add_deprecated($msg, $args);
		}
		$caller = Error_Reporter::get_caller(1, '/Module_Skeleton/');
		if (0 !== strpos($msg, $caller)) {
			$msg = sprintf('%s(): %s', Error_Reporter::get_caller(1, '/Module_Skeleton/'), $msg);
		}

		return Error_Reporter::add_deprecated($msg, $args);

	}

	/**
	 * Warn deprecated function, log calling method
	 *
	 * @param string $msg
	 * @param mixed  $args
	 * @return bool
	 */
	function deprecated_func(string $msg = '', ...$args): bool
	{
		if (is_debug()) {
			$tmp = '%s(): is deprecated - called from %s(): %s';
			array_unshift($args, $tmp, Error_Reporter::get_caller(1), Error_Reporter::get_caller(2), $msg);

			return call_user_func_array('deprecated', $args);
		}

		Error_Reporter::report('deprecated func: %s', [Error_Reporter::get_caller(1)]);
		$tmp = '%s() is deprecated';
		if ($msg) {
			$tmp .= ': ' . $msg;
		}
		array_unshift($args, $tmp, Error_Reporter::get_caller(1));

		return call_user_func_array('deprecated', $args);
	}

	function report(string $msg = '', ...$args): bool
	{
		Error_Reporter::report($msg, $args);

		return true;
	}

	function mute_warn(bool $mute_php = false): bool
	{
		return Error_Reporter::mute_warning($mute_php);
	}

	function mute(Closure $func)
	{
		Error_Reporter::mute_warning();
		try {
			$ret = $func();
		} catch (\Throwable $e) {
			throw $e;
		} finally {
			Error_Reporter::unmute_warning();
		}

		return $ret;
	}

	function unmute_warn(): bool
	{
		return Error_Reporter::unmute_warning();
	}

	function silence(Closure $func)
	{
		return Error_Reporter::silence($func);
	}

	function dlog(string $msg, ...$args): bool
	{
		Error_Reporter::log($msg, $args);
		if (is_debug() && \Error_Reporter::is_verbose(9999)) {
			if ($args) {
				$msg = ArgumentFormatter::format(_($msg), $args);
			}
			fwrite(STDERR, "DEBUG: ${msg}\n");
		}

		return true;
	}

	/**
	 * Print unique object identifier
	 *
	 * @param object $obj
	 * @return void
	 */
	function print_object_hash(object $obj): void
	{
		if (!is_object($obj)) {
			fatal('object is not object, got %s', gettype($obj));
		}
		print object_hash($obj);
	}


	/**
	 * Compute unique hash for object
	 *
	 * @param object $obj
	 * @return string object hash
	 */
	function object_hash(object $obj): string
	{
		if (!is_object($obj)) {
			fatal('object is not object, got %s', gettype($obj));
		}

		return spl_object_hash($obj);
	}

	/**
	 * Pause execution for dev
	 *
	 * @param string $msg
	 * @param array  ...$args
	 */
	function pause(string $msg, ...$args): void
	{
		if (!is_debug() || !posix_isatty(1)) {
			return;
		}
		echo '>>> PAUSED', PHP_EOL, sprintf($msg, ...$args), PHP_EOL;
		fgetc(STDIN);
	}