<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	use Opcenter\Account\State;

	/**
	 * @property string $site
	 */
	class Auth_Info_User
	{
		use FilesystemPathTrait {
			domain_fs_path     as private real_domain_fs_path;
			domain_info_path   as private real_domain_info_path;
			domain_shadow_path as private real_domain_shadow_path;
		}

		public $username;
		public $domain;
		// @var string site
		protected $site;
		public $level = 0;
		public $site_id = 0;
		public $group_id;
		public $user_id;
		public $id;
		// @var reseller group membership
		public $rgroup = null;

		// @var string langauge
		public $language;
		// @var string timezone
		public $timezone;

		/**
		 * @var \Auth_Info_Account
		 */
		public $conf = null;

		private function __construct()
		{}

		public function __sleep()
		{
			$keys = get_object_vars($this);
			unset($keys['conf']);
			return array_keys($keys);
		}

		public function __wakeup()
		{
			$this->importAccount();
		}


		public function __get($name)
		{
			if ($name === 'site') {
				return $this->site;
			}
			fatal("Unknown user property `%s'", $name);
		}

		public function __set($name, $value)
		{
			if ($name === 'site') {
				$this->site = $value;
			}
			fatal("Unknown user property `%s'", $name);
		}

		public function __isset($name)
		{
			if ($name === 'site') {
				return isset($this->site);
			}
			return false;
		}

		public function domain_fs_path(string $subpath = null): string
		{
			return $this->real_domain_fs_path($subpath);
		}

		public function domain_info_path(string $subpath = null): string
		{
			return $this->real_domain_info_path($subpath);
		}

		public function domain_shadow_path(string $subpath = null): string
		{
			return $this->real_domain_shadow_path($subpath);
		}


		public function getImpersonator(): ?string
		{
			return \Session::get('auth.impersonator');
		}

		public function setImpersonator(string $id): void
		{
			if (!\apnscpSession::init()->exists($id)) {
				fatal("Impersonator `%s' doesn't exist", $id);
			}
			// User object can be regenerated thus losing the impersonator ID
			\Session::set('auth.impersonator', $id);
		}

		/**
		 * Initialize a new user
		 *
		 * @param string      $user
		 * @param null|string $domain
		 * @param null|string $password
		 * @param null|string $session_id session identifier to use
		 * @return Auth_Info_User|null
		 */
		public static function initializeUser(
			string $user,
			?string $domain = null,
			string $password = null,
			string $session_id = null
		): ?\Auth_Info_User {
			$session_id = $session_id ?? \apnscpSession::init()->get_id();
			if (!\apnscpSession::valid_id($session_id)) {
				fatal("invalid session id `%s'", $session_id);
			}
			$privilege_level = $user_id = $group_id = $site_id = null;
			if ($domain) {
				$site_id = \Auth::get_site_id_from_domain($domain);
				if (!$site_id) {
					error($domain . ': domain not found in lookup table');

					return null;
				}

				/** now lookup the main domain by site */
				$domain = \Auth::get_domain_from_site_id($site_id);
				if (!AUTH_SUSPENDED_LOGIN && State::disabled($site = 'site' . $site_id)) {
					$reason = ArgumentFormatter::format('Account is disabled');
					if (AUTH_SHOW_SUSPENSION_REASON && $tmp = file_get_contents(State::disableMarker($site)))
					{
						$reason = $tmp;
					}
					error($reason);

					return null;
				}
				$path = \Auth::get_domain_path('site' . $site_id) . '/etc/passwd';
				if (!file_exists($path) || !($fp = fopen($path, 'r'))) {
					error($domain . ': cannot access /etc/passwd');

					return null;
				}

				$admin = \Auth::get_admin_from_site_id($site_id);
				$privilege_level = $admin === $user ? PRIVILEGE_SITE : PRIVILEGE_USER;
				while (!feof($fp)) {
					$data = explode(':', trim(fgets($fp)));
					if ($data[0] === $user) {
						[$user_id, $group_id] = array_slice($data, 2, 2);
						break;
					}
				}
				fclose($fp);
				if (!$user_id) {
					return null;
				}
			} else {
				$pgdb = PostgreSQL::initialize();
				$passwd = $pgdb->query("SELECT password FROM reseller_info WHERE username = '" . pg_escape_string($user) . "'")->fetch_object();
				if (!$passwd) {
					$path = \apnscpFunctionInterceptor::get_autoload_class_from_module('auth')::ADMIN_AUTH;
					if (false === ($fp = fopen($path, 'r'))) {
						fatal('Failed to open admin auth!');
					}

					while (!feof($fp)) {
						$data = explode(':', trim(fgets($fp)));
						if ($data[0] === $user) {
							$pwd = posix_getpwnam(APNSCP_SYSTEM_USER);
							[$user_id, $group_id] = [$pwd['uid'], $pwd['gid']];
							$privilege_level = PRIVILEGE_ADMIN;
						}
					}
				} else {
					$privilege_level = PRIVILEGE_RESELLER;
					$user_id = null;
					$group_id = null;

					return error('not yet implemented');
					/** to-do verify reseller password */
					$passwd_state = true;
				}
			}

			if (!$privilege_level) {
				return null;
			}
			$db = \MySQL::initialize();
			$stmt = $db->stmt_init();
			$stmt->prepare("REPLACE INTO `session_information`
                (session_id,
                 username,
                 password,
                 domain,
                 level,
                 login_ts,
                 user_id,
                 group_id,
                 last_action,
                 site_number,
                 login_method,
                 auto_logout)
            VALUES
                ('" . $session_id . "',
                 ?,
                 NULL,
                 ?,
                 ?,
                 NOW(),
                 ?,
                 ?,
                 NOW(),
                 ?,
                 '" . \Auth::get_driver()->get_db_symbol() . "',
                 1);");
			$stmt->bind_param('ssisss',
				$user,
				$domain,
				$privilege_level,
				$user_id,
				$group_id,
				$site_id);

			if (!$stmt->execute()) {
				\Error_Reporter::report($db->error);
				fatal('failed to access credentials database');
			}

			return static::factory($session_id);
		}

		/**
		 * Contexted object is valid session
		 *
		 * @return bool
		 */
		public function valid(): bool
		{
			return !empty($this->username);
		}

		public static function factory($session)
		{
			$db = \MySQL::initialize();
			$rs = $db->query("SELECT username,
                domain,
                level,
                user_id,
                group_id,
                site_number AS site_id FROM session_information WHERE session_id = '" . $db->escape_string($session) . "'");
			if (!$rs || $rs->num_rows < 1) {
				fatal("invalid session `%s'", $session);
			}
			$rs = $rs->fetch_assoc();
			$user = new static;
			foreach ($rs as $key => $val) {
				$user->$key = $val;
			}
			$user->user_id = (int)$user->user_id;
			$user->group_id = (int)$user->group_id;
			$user->site_id = (int)$user->site_id;
			$user->level = (int)$user->level;
			$user->id = $session;
			if ($user->site_id) {
				$user->site = 'site' . $user->site_id;
				$user->importAccount();
			}

			return $user;
		}

		private function importAccount(): ?\Auth_Info_Account
		{
			return $this->conf = Auth_Info_Account::factory($this);
		}

		/**
		 * Reset contexted metadata
		 *
		 * @return Auth_Info_Account|null
		 */
		public function reset(): ?Auth_Info_Account
		{
			$this->conf = null;

			return $this->getAccount();
		}

		/**
		 * Get account metadata
		 *
		 * @return Auth_Info_Account|null
		 */
		public function getAccount(): ?Auth_Info_Account
		{
			if (!isset($this->conf)) {
				$this->importAccount();
			}

			return $this->conf;
		}

		/**
		 * Get account metadata configuration
		 *
		 * @param        $class
		 * @param string $type
		 * @return null
		 */
		public function conf($class, $type = 'cur')
		{
			return $this->conf->$type[$class] ?? null;
		}
	}