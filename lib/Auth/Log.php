<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2020
 */

namespace Auth;

class Log {
	/**
	 * @var \Auth auth instance
	 */
	protected $auth;

	public function __construct(\Auth $driver)
	{
		$this->auth = $driver;
	}

	public function success(array $args = []): void
	{
		$this->log("Login succeeded %(ip)s - %(username)s/%(domain)s", $args, LOG_INFO);
	}

	public function failure(array $args = []): void
	{
		$this->log("Login failure %(ip)s - %(username)s/%(domain)s", $args, LOG_WARNING);
	}

	public function log(string $msg, array $args = [], int $priority = LOG_INFO): void
	{
		$args = array_filter($args) + [
			'domain'   => "NULL",
			'username' => "NULL",
			'ip'       => \Auth::client_ip()
		];

		openlog("cp/" . strtolower($this->auth->getDriver()), LOG_PID | LOG_NDELAY, LOG_AUTHPRIV);
		syslog($priority, \ArgumentFormatter::format($msg, $args));
		closelog();
	}
}


