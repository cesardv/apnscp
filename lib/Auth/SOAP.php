<?php

	use Auth\IpRestrictor as IpRestrictorAlias;

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Auth_SOAP extends Auth
	{
		const DB_SYMBOL = 'soap';
		const TV_SEC = 1800;

		/** @var @ignore api key host */
		private static $_keyhost = SOAP_KEY_HOST;
		private static $_keyuser = SOAP_KEY_USER;
		/** @var @ignore api key password */
		private static $_keypass = SOAP_KEY_PASSWORD;
		private static $_keydb = SOAP_KEY_DB;
		/**
		 * @var mysqli
		 */
		private static $_db;

		public function verify($username, $password, $domain)
		{
			if (!IpRestrictorAlias::instantiateContexted($this->getProfile())->authorized(self::client_ip(),
				$this->getDriver()))
			{
				return false;
			}
			return true;
		}

		public function session_valid()
		{
			if (!SOAP_ENABLED) {
				// via config.ini
				return false;
			}

			if (!empty($_SESSION['valid'])) {
				return true;
			}

			// force authentication
			if (! ($key = self::_get_authkey())) {
				return false;
			}
			// do a cache fetch routine...
			if (! ($auth = $this->_check_auth_ttl($key))) {
				return false;
			}

			return static::_resume_session($auth['session']);
		}

		private static function _get_authkey(): string
		{
			$key = '';
			if (isset($_GET['authkey'])) {
				$key = $_GET['authkey'];
			} else if (isset($_SERVER['HTTP_AUTH_KEY'])) {
				$key = $_SERVER['HTTP_AUTH_KEY'];
			} else if (isset($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) &&
				$_SERVER['PHP_AUTH_USER'] == 'soap'
			) {
				$key = $_SERVER['PHP_AUTH_PW'];
			}

			return strtolower(str_replace('-', '', $key));
		}

		private function _check_auth_ttl($key)
		{
			$data = apcu_fetch('soap_ttl:' . $key);
			if (!$data) {
				return false;
			}

			if (($_SERVER['REQUEST_TIME'] - $data['ttl']) >= self::TV_SEC || self::client_ip() !== $data['ip']) {
				return false;
			}
			if (!self::_stat_check($data['id'], $data['stat'])) {
				return false;
			}
			self::_update_auth_ttl($key, $data);
			return $data;
		}

		private static function _update_auth_ttl($key, $data): bool
		{
			$data['ttl'] = $_SERVER['REQUEST_TIME'];
			apcu_store('soap_ttl:' . $key, $data, self::TV_SEC);

			return true;
		}

		/**
		 * Compare or get mtime on /etc/shadow
		 *
		 * @param string $site_id
		 * @param string $ttl
		 * @return bool|int
		 */
		private static function _stat_check($site_id, $ttl = null)
		{
			if ($site_id) {
				$file = FILESYSTEM_VIRTBASE . '/site' .
					$site_id . '/fst/etc/shadow';
			} else {
				$file = '/etc/shadow';
			}

			if (!$ttl) {
				return filemtime($file);
			}

			return $ttl === filemtime($file);
		}

		public function authenticate()
		{
			if (!SOAP_ENABLED) {
				http_response_code(404);
				exit(0);
			}
			Auth_Anvil::anvil();
			$authkey = self::_get_authkey();
			$db = self::get_api_db();
			if (SOAP_UNIFY_KEYS) {
				$query = "SELECT `username`, `api_keys`.site_id AS site_id,
		            domain_information.`site_id` AS sanity, domain_information.`domain` FROM `api_keys`
		            LEFT JOIN domain_information ON (di_invoice = invoice AND parent_domain IS NULL)
		            WHERE `api_key` = '" . $db->escape_string($authkey) . "'";
			} else {
				// single-panel installation
				$query = 'SELECT `username`, site_id AS sanity, `api_keys`.site_id AS site_id FROM `api_keys` ' .
					"WHERE `api_key` = '" . $db->escape_string($authkey) . "'";
			}
			// @FIXME migrating user to new server doesn't
			// properly initialize user if migration follows immediately after
			// before domainnotice runs to inventory new domains
			$q = $db->query($query);
			if ($db->error) {
				report($db->error);
			}
			if ($q->num_rows < 1) {
				// @TODO second query if on wrong server? makes it easier
				// to brute-force, at least it's two-factor this way...
				return false;
			}
			/**
			 * Situations in which an account is broken into multiple accounts
			 * with the same invoice can yield the wrong record. In such cases,
			 * use the site_id field in api_keys to confirm the right record
			 */
			$listed = false;
			if (is_debug()) {
				// debug environment is usually squirrely
				// bypass lookup and trust first response
				$info = $q->fetch_array();
				$site_id = $info['site_id'];
			} else if ($q->num_rows > 1) {
				while (false !== ($info = $q->fetch_array())) {
					if ($info['sanity'] == $info['site_id']) {
						$listed = true;
						break;
					}
				}
			} else {
				$info = $q->fetch_array();
			}
			if (empty($info['username'])) {
				fatal('???');
			}
			$username = $info['username'];
			$site_id = $info['site_id'];
			// development doesn't always update domain_information,
			// use site_id tagged in key creation instead
			$domain = Auth::get_domain_from_site_id($site_id);
			if (!$this->initializeUser($username, $domain, null)) {
				return false;
			}

			$db->query("UPDATE `api_keys` SET last_used = NOW()
                WHERE `api_key` = '" . $authkey . "'");
			self::_cache_auth($authkey, $username, $domain);
			Auth_Anvil::remove();

			return true;
		}

		public static function get_api_db()
		{
			if (null !== self::$_db && self::$_db->ping()) {
				return self::$_db;
			}
			$host = self::$_keyhost;
			$user = self::$_keyuser;
			$pass = self::$_keypass;
			$db = self::$_keydb;
			$conn = new mysqli();
			$conn->init();
			$conn->real_connect($host, $user, $pass, $db);
			if ($conn->connect_errno) {
				fatal('cannot connect to api database');
			}
			self::$_db = $conn;

			return self::$_db;
		}

		private static function _cache_auth($key, $user, $domain): void
		{
			$site_id = (int)self::get_site_id_from_domain($domain);

			$data = array(
				'user'    => $user,
				'domain'  => $domain,
				'id'      => $site_id,
				'stat'    => self::_stat_check($site_id),
				'ip'      => self::client_ip(),
				'session' => session_id()
			);
			self::_update_auth_ttl($key, $data);
		}

		private static function _resume_session($session_id): bool
		{
			$db = self::get_db();
			$q = $db->query('SELECT session_id FROM `session_information`
            WHERE last_action >= DATE_SUB(NOW(), INTERVAL ' . self::TV_SEC . ' SECOND) ' .
				" AND session_id = '" . $session_id . "'" .
				" AND `login_method` = '" . self::DB_SYMBOL . "'");
			if ($q->num_rows < 1) {
				\Auth_Anvil::anvil();

				return false;
			}
			// destroy auto-start session to avoid session pollution
			if (session_status() === PHP_SESSION_ACTIVE && \session_id() !== $session_id) {
				apnscpSession::init()->destroy(session_id());
			}
			return apnscpSession::restore_from_id($session_id);
		}

		public function unauthorized()
		{
			header('HTTP/1.1 401 Unauthorized');
			header('WWW-Authenticate: Digest realm="SOAP"' .
				' qop="auth" nonce="' . uniqid('soap', true) . '" opaque="SOAP"');
			exit;
		}
	}