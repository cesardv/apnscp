<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Auth_Stub extends Auth
	{
		const DB_SYMBOL = 'stub';
		const TV_SEC = 1800;

		public static function get_driver(): Auth
		{
			return new static;
		}

		protected static function _log_session()
		{
			return true;
		}

		public function authenticate()
		{
			return true;
		}

		public function initializeStubSession(
			string $username,
			?string $domain,
			?string $password,
			$timeout = Auth::TV_SEC
		): ?bool {
			return parent::initializeUser($username, $domain, $password, $timeout);
		}

		/**
		 * Change authentication gate
		 *
		 * @param string $gate
		 * @return bool
		 */
		public function setAuthenticationGate(string $gate): bool
		{
			$db = \MySQL::initialize();
			$symbol = $db->escape(\Auth::import($gate)->get_db_symbol());
			$rs = $db->query("UPDATE session_information SET login_method = '" . $symbol . "' WHERE session_id = '" . $db->escape($this->getID()) . "'");
			return $rs;
		}

		/**
		 * Verify credentials
		 */
		public function verify($username, $password, $domain)
		{
			return true;
		}

		public function session_valid()
		{
			return $_SESSION['valid'] ?? false;
		}

		public function unauthorized()
		{
			echo 'UNAUTHORIZED';
			return false;
		}


	}
