<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * function interceptor
	 * @mixin \Module_Skeleton
	 */

	class apnscpFunctionInterceptor
	{
		/**
		 * @var int maximum time to hold module entries in cache
		 */
		const MODULE_CACHE_TTL = 600;
		const MODULE_SKELETON_NAMESPACE = 'Module\\Skeleton\\';
		protected static $callbacks = [];
		private static $stickyModules;

		private static $sticky_mappings =
			array(
				'PostgreSQL'           => 'lib/postgresql.php',
				'MySQL'                => 'lib/mysql.php',
				'HTML_Kit'             => 'lib/html/html_kit.php',
				'Tip_Engine'           => 'lib/html/tip_engine.php',
				'apnscpException'      => 'lib/apnscperror.php',
				'JS_Template'          => 'lib/html/js_template.php',
				'CurlLayer'            => 'lib/curllayer.php',
				'UCard'                => 'lib/html/UCard.php',
				'Page_Container'       => 'lib/html/page_container.php',
				'Page_Renderer'        => 'lib/html/page_renderer.php',
				'Configuration_Driver' => 'lib/configuration_driver.php',
			);
		private static $cache = [];
		private static $knownTraits = [];
		/**
		 * @var array associated session cache
		 */
		private $session;
		private $modified_level = 0;
		private $session_id;
		/** array structured as follows:
		 * class: class name of the module\
		 *   |-> method name: method name
		 *         |-> minargs: minimum number of arguments
		 *         |-> maxargs: maximum number of arguments
		 *         |-> permissions: permissions applying to the function
		 *   |-> instance: instance of the class
		 */

		private $modules; // options sent via backend
		private $permission_level;
		/**
		 * @var Auth_Info_User
		 */
		private $auth = null;
		private $webHookBuffer = null;

		public static function init(bool $force = false): apnscpFunctionInterceptor
		{
			$id = \session_id();
			if ($force || !isset(self::$cache[$id])) {
				self::$cache[$id] = new static();
			}

			return self::$cache[$id];
		}

		/**
		 * Create an afi instance from an authenticated context
		 *
		 * @param Auth_Info_User $context
		 * @return Module_Skeleton code completion
		 */
		public static function factory(Auth_Info_User $context): self
		{
			if (isset(self::$cache[$context->id])) {
				// @TODO weakmap checks in 7.4 to avoid memory leaks in backend
				return self::$cache[$context->id];
			}

			/**
			 * @var \apnscpFunctionInterceptor $afi
			 */
			$afi = (new ReflectionClass(static::class))->newInstanceWithoutConstructor();
			/**
			 * Ensure the account configuration is imported from this context
			 */
			$afi->set_session_context($context);

			return self::$cache[$context->id] = $afi;
		}

		/**
		 * Clear cache
		 *
		 * @param string|null $id
		 */
		public static function expire(string $id = null): void
		{
			if ($id) {
				unset(self::$cache[$id]);
			} else {
				self::$cache = [];
			}
		}

		private function __construct()
		{
			$this->session_id = $this->session_id ?? (IS_CLI ? null : session_id());
			if ($this->session_id && $this->session_id === session_id()) {
				$this->session = &$_SESSION;
			}

			if (!IS_CLI && isset($this->session['webhooks'])) {
				$this->webHookBuffer = array();
			}
			$this->set_session_context($this->auth ?? \Auth::profile());
			assert(NO_AUTH || IS_CLI || $this->auth->username !== null, 'Authentication succeeded');
		}

		public function __wakeup()
		{
			if ($this->session_id === \session_id()) {
				$this->session = &$_SESSION;
			}
			if (!($this->session['cache']['modules'] ?? []) instanceof ModuleCache) {
				$this->session['cache']['modules'] = new ModuleCache($this->session['cache']['modules'] ?? []);
			}
			$this->modules = &$this->session['cache']['modules'];
			if (!isset(self::$cache[$this->session_id])) {
				self::$cache[$this->session_id] = $this;
			}

			$this->auth->reset();
			$this->_bind_localization();
		}

		public function __sleep()
		{
			$this->__destruct();

			$keys = get_object_vars($this);
			if ($this->session_id === \session_id()) {
				unset($keys['session']);
			}

			return array_keys($keys);
		}

		/**
		 * Set module context dumping active caches
		 *
		 * @param Auth_Info_User $context
		 * @return $this
		 */
		public function set_session_context(\Auth_Info_User $context): self
		{
			if ($context === $this->auth) {
				return $this;
			}

			if (!isset($this->session['cache'])) {
				$this->session['cache'] = new ArrayObject();
			}

			$this->session['cache']['modules'] = new ModuleCache();

			$this->modules = &$this->session['cache']['modules'];
			$this->auth = $context;
			$this->auth->level |= $this->modified_level;
			$this->permission_level = $this->auth->level;
			$this->_bind_localization();
			if (IS_CLI) {
				$this->permission_level |= PRIVILEGE_SERVER_EXEC;
			}
			// MUST come last to avoid DataStream::query()
			// short-circuiting full init on session change
			$this->session_id = $context->id;

			return $this;
		}

		private function _bind_localization(): bool
		{
			if (!isset($this->session[Preferences::SESSION_KEY]) || \session_id() !== $this->auth->id) {
				return false;
			}
			/**
			 * @todo contexted localization
			 */
			$preferences = \Preferences::factory($this->auth);

			$tz = \array_get(\Preferences::factory($this->auth), 'timezone');
			$this->auth->timezone = $tz ?? TIMEZONE;
			date_default_timezone_set($this->auth->timezone);

			$locale = \array_get($preferences, 'language');

			$this->auth->language = $locale ?? LOCALE;
			setlocale(LC_ALL, $this->auth->language);

			return true;
		}

		/**
		 * Get module name from file
		 *
		 * @param $file
		 * @return string
		 */
		public static function get_module_from_file(string $file): string
		{
			return basename($file, '.php');
		}

		/**
		 * Get module name from class
		 *
		 * @param $class
		 * @return string
		 */
		public static function get_module_from_class(string $class): string
		{
			$name = strtok($class, '_');
			if ($name === $class) {
				return static::get_module_from_class(
					(new ReflectionClass($class))->getParentClass()->getName()
				);
			}

			return strtolower($name);
		}


		/**
		 * Override class mappings
		 *
		 * @param array|string $maps single class or array of classes
		 * @param string|null  $file single file, omit if maps array
		 */
		public static function register($maps, string $file = null): void {
			if (null !== $file) {
				$maps = [$maps => $file];
			}
			if (!is_array($maps)) {
				fatal('Sticky map must be array');
			}
			foreach ($maps as $map => $file) {
				if ($file[0] === '@') {
					if (0 === strncmp($file, '@custom/', 8)) {
						// @custom/ => config/custom/abc....
						$file = 'config/' . substr($file, 1);
					} else {
						fatal("Unknown path alias `%s'", $file);
					}
				}
				self::$sticky_mappings[$map] = $file;
			}
		}

		public static function registerModule(string $name, string $class): void
		{
			self::$stickyModules[$name] = $class;
		}

		public static function autoload($class)
		{
			$appRoot = INCLUDE_PATH;
			$maps = self::$sticky_mappings;
			if (false !== isset($maps[$class])) {
				$file = "${appRoot}/" . $maps[$class];
				return require $file;
			}
			$magic = substr($class, -7);
			if ('_Module' === $magic) {
				$module = strtolower(substr($class, 0, -7));
				$tmp = "${appRoot}/lib/modules/${module}.php";
				if (file_exists($tmp)) {
					return require $tmp;
				}
				if (class_exists($name = self::get_surrogate_from_module($module))) {
					// dynamically load surrogate as class if surrogate but
					// class does not exit
					class_alias($name, $class);

					return true;
				}
			} else if ($magic === 'rrogate' && self::class_is_surrogate($class)) {
				// shitty optimization unit!, 10% speedup
				$module = strtolower(substr($class, 0, -17));
				$tmp = self::get_surrogate_file_from_module($module);
				if (file_exists($tmp)) {
					return require $tmp;
				}
			}
			$fileMap = str_replace(['_', '\\'], '/', $class) . '.php';
			if (file_exists("${appRoot}/lib/${fileMap}")) {
				return include "${appRoot}/lib/${fileMap}";
			}

			$p1 = null;
			if (false !== ($pos = strrpos($class, "\\"))) {
				$p1 = str_replace(['\\', '_'], [DIRECTORY_SEPARATOR, '-'], substr($class, 0, $pos));
				$class = basename(str_replace("\\", '/', $class));
				if ($class === 'Page') {
					// app entry points are named after the app + use Page as its class name
					// @fixme
					$file = $appRoot . '/' . $p1 . '/' . basename($p1) . '.php';
				} else {
					$file = $appRoot . '/' . $p1 . '/' . str_replace('_', '/', $class) . '.php';
				}
				if (file_exists($file)) {
					return require $file;
				}
			}

			return false;
		}

		public static function get_surrogate_from_module($module): string
		{
			return self::get_class_from_module($module) . '_Surrogate';
		}

		public static function get_class_from_module($module): string
		{
			return self::$stickyModules[$module] ?? (ucwords($module) . '_Module');
		}

		public static function class_is_surrogate($module): bool
		{
			return '_Surrogate' === substr($module, -10);
		}

		private static function get_surrogate_file_from_module($module): string
		{
			return INCLUDE_PATH . "/lib/modules/surrogates/${module}.php";
		}

		public function __destruct()
		{
			if ($this->webHookBuffer) {
				//$conn = new apnscpObject('webhook_dispatch', $this->_webHookBuffer);
				$ds = DataStream::get($this->auth);
				$ds->setOption(apnscpObject::NO_WAIT);
				$ds->query('test_nowait', $this->webHookBuffer);
			}

			// @todo ideally, dynamic compositions should be cleaned, including any swap()
			// statements used in debugging. Such action breaks GitTest's sabotage process
			if ($this->modules) {
				$this->modules->cleanDynamicCompositions();
			}
		}

		public function __debugInfo()
		{
			return [
				'id'   => $this->session_id,
				'auth' => $this->auth
			];
		}

		public function get_loaded_modules(): ModuleCache
		{
			return $this->modules;
		}

		/**
		 * @return array
		 * @throws \ModuleCache[]
		 */
		public function get_reflection_cache(): array
		{
			$modules = [];
			foreach (self::list_all_modules() as $module) {
				$modules[$module] = new ReflectionClass($this->get_instance($module));
			}

			return $modules;
		}

		/**
		 * Swap module instance
		 *
		 * @param string                    $module
		 * @param \Module\Skeleton\Standard $instance
		 * @return bool
		 */
		public function swap(string $module, \Module\Skeleton\Standard $instance): bool
		{
			$instance->setContext($this->auth);
			$this->initializeModule($module, $instance, true);
			return true;
		}

		/**
		 * Reset module instance
		 *
		 * @param string $module
		 */
		public function reset(string $module): void {
			$this->modules->forget($module);
		}

		public function set_session_id($id): apnscpFunctionInterceptor
		{
			$driver = Auth::get_driver();
			$driver->setID($id);

			return $this->set_session_context($driver->authInfo(true));
		}

		/**
		 * Active configuration context matches id
		 *
		 * @param string $id
		 * @return bool
		 */
		public function context_matches_id(string $id): bool
		{
			return $this->session_id === $id;
		}

		/**
		 * Authentication context matches bound context
		 *
		 * @param Auth_Info_User $ctx
		 * @return bool
		 */
		public function context_matches_self(\Auth_Info_User $ctx): bool
		{
			return $ctx === $this->auth;
		}

		public function load_all_backend_modules(): void
		{
			foreach (self::list_all_modules() as $module) {
				$this->instantiate_backend_module($module);
			}
		}

		public static function list_all_modules(): array
		{
			$files = glob(INCLUDE_PATH . '/lib/modules/{surrogates/,}*.php', GLOB_BRACE);

			return array_unique(array_map(array('self', 'get_module_from_file'), $files));
		}

		/**
		 * Loads up a module of a given file name and calls {@see add_exported_functions()}
		 *
		 * @param string $module short-hand module name, e.g. "file"
		 * @return bool
		 */
		private function instantiate_backend_module($module): bool
		{
			$timebeg = microtime(true);
			$class = self::get_autoload_class_from_module($module);
			if (!class_exists($class)) {
				fatal("Module `%s' not found", $module);
			}
			$surrogate = self::class_is_surrogate($class);

			$this->add_backend_class_mappings($module);
			$this->add_backend_exported_functions($module);

			dlog('+ Loaded %s %s in %.5f seconds',
				$surrogate ? 'surrogate' : 'module',
				$module,
				microtime(true) - $timebeg
			);

			return true;
		}

		/**
		 * Register API callback
		 *
		 * @param string  $module
		 * @param string  $fn
		 * @param Closure $callback
		 */
		public static function registerCallback(string $module, string $fn, Closure $callback) {
			if (empty(static::$callbacks[$module])) {
				static::$callbacks[$module] = [];
			} else if (isset(static::$callbacks[$module][$fn])) {
				debug('Callback fn %(fn)s overwritten in %(module)s - call cleanDynamicCompositions()', compact('module', 'fn'));
			}

			static::$callbacks[$module][$fn] = $callback;
		}

		/**
		 * Get autoloadable class name by module
		 *
		 * @param string $module
		 * @return string
		 */
		public static function get_autoload_class_from_module(string $module): string
		{
			if (self::surrogate_exists($module)) {
				return self::get_surrogate_from_module($module);
			}

			return self::get_class_from_module($module);
		}

		public static function surrogate_exists($module): bool
		{
			return file_exists(self::get_surrogate_file_from_module($module));
		}

		protected function add_backend_class_mappings($module): void
		{
			$this->load_module($module);
		}

		/**
		 * Fill missing parameters with defaults
		 *
		 * @param string $cmd  command
		 * @param array  $args command arguments
		 * @return array|null
		 */
		public function polyfill(string $cmd, ?array $args = []): ?array {
			if (!$args) {
				return [];
			}
			$pos = strpos($cmd, '_');
			$method = substr($cmd, $pos + 1);
			$class = substr($cmd, 0, $pos);
			if (is_string(key($args))) {
				$args = array_values($args);
			}
			$instance = (new ReflectionClass($this->get_instance($class)))->getMethod($method);
			$parameters = $instance->getParameters();
			for ($i = 0, $n = \count($parameters); $i < $n; $i++) {
				if (!array_key_exists($i, $args)) {
					$args[$i] = $parameters[$i]->getDefaultValue();
				}
			}
			ksort($args, SORT_NUMERIC);
			return $args;
		}

		private function load_module($module): bool
		{
			if (isset($this->modules[$module][ModuleCache::TYPE_INSTANCE])) {
				return true;
			}
			if (!self::module_exists($module) && !self::surrogate_exists($module)) {
				return error("`%s': no such module", $module);
			}

			if (self::blacklisted($module)) {
				fatal("module `%s' blacklisted - contact module author", $module);
			}

			$class = self::get_autoload_class_from_module($module);
			/**
			 * @var \Module\Skeleton\Standard
			 */

			$c = $class::autoloadModule($this->auth ?? \Auth::profile());
			$this->initializeModule($module, $c);

			if (IS_CLI) {
				return true;
			}

			return $this->add_module_cache($module);
		}

		/**
		 * Initialize module into inflector
		 *
		 * @param string                    $module
		 * @param \Module\Skeleton\Standard $instance
		 */
		private function initializeModule(string $module, \Module\Skeleton\Standard $instance, bool $swap = false): void
		{
			static $checkTs;

			$isAnonymous = (new ReflectionClass($instance))->isAnonymous();
			if (!$isAnonymous && isset(self::$callbacks[$module])) {
				// callbacks exist for named methods, for each designated callback
				// monkeypatch a proxy method to first dispatch the parent, then pass
				// result to closure
				//
				// implementation has blindspots: we're focusing on API entrypoints, not codepaths
				// once in a module,
				$maps = self::$callbacks[$module];
				$class = get_class($instance);
				eval('$instance = new class($instance, $maps, $this->auth) extends ' . $class . ' {
					private static $callbackMap = [];
					private $proxiedInstance;

					public function __construct(\Module\Skeleton\Standard $instance, &$maps, \Auth_Info_User $ctx)
					{
						self::$callbackMap = $maps;
						$this->proxiedInstance = $instance;
						$this->setUserParameters($ctx);
						parent::__construct();
					}

					public function _invoke($function, $args = null)
					{
						$ret = $this->proxiedInstance->_invoke($function, $args);
						if (isset(self::$callbackMap[$function])) {
							self::$callbackMap[$function]->bindTo($this->proxiedInstance, static::class)($ret, $args);
						}
						return $ret;
					}
				};');
			}

			$this->modules[$module] = array(
				ModuleCache::TYPE_INSTANCE  => $instance,
				ModuleCache::TYPE_FUNCTIONS => [],
				'ttl'       => 0,
				'surrogate' => self::class_is_surrogate(get_class($instance)),
				'dynamic'   => $isAnonymous,
				'swap'      => $swap
			);
		}

		public static function module_exists($module): bool
		{
			return isset(self::$stickyModules[$module]) || file_exists(self::get_file_from_module($module));
		}

		private static function get_file_from_module($module): string
		{
			return INCLUDE_PATH . '/lib/modules/' . $module . '.php';
		}

		/**
		 * Blacklist quarrelsome modules
		 *
		 * @param string $module
		 * @return bool
		 */
		private static function blacklisted(string $module): bool
		{
			return false;
		}

		private function add_module_cache($module): bool
		{
			// @BUG
			// PHP 5.3 (tested with 5.3.23) will, after repeated calls,
			// drop its class properties resulting in NULL values
			//
			// Call the constructor to reset these properties
			//
			// Perhaps also the cause of SOAP methods failing
			if (null === $this->permission_level) {
				$this->__construct();
			}

			$this->validate_functions($module);

			return true;
		}

		private function validate_functions($module): bool
		{
			if ($this->cache_is_valid($module)) {
				return true;
			}

			$this->update_module_ttl($module);
			$instance = $this->get_instance($module);
			$exports = self::_exported_commands($instance);
			$rfxncls = new ReflectionClass($instance);
			// wildcard permissions
			// Add additional debugging for @BUG in add_module_cache()
			if (!IS_SOAP && null === $this->permission_level) {
				$this->__construct();
				if (null === $this->permission_level) {
					// wtf?
					$msg = 'NULL permission on ' . $module;
					$msg .= "\n\n" . Error_Reporter::get_debug_bt();
					//$msg .= "\n\n" . var_export($this, true);
					if (is_debug()) {
						fatal($msg);
					}
					Error_Reporter::report($msg);
				}
			}
			$func_tbl = array();
			for ($i = 0, $k = array_keys($exports), $n = count($k); $i < $n; $i++) {
				$perms = $exports[$k[$i]];
				$function = $k[$i];
				// referenced command does not exist -- may want to do something about it
				// assume higher level permission bits are special backend-only attributes
				if (!($perms & $this->permission_level) || (!IS_CLI && $perms & 0xF0) || !method_exists($instance,
						$function)) {
					continue;
				}

				$method = $rfxncls->getMethod($function);
				$func_tbl[$function] = array(
					ModuleCache::TYPE_MINARG     => $method->getNumberOfRequiredParameters(),
					ModuleCache::TYPE_MAXARG     => $method->isVariadic() ? null : $method->getNumberOfParameters(),
					ModuleCache::TYPE_PERMISSION => $perms
				);
			}
			$this->modules[$module][ModuleCache::TYPE_FUNCTIONS] = $func_tbl;

			return true;
		}

		/**
		 * Validate module cache integrity
		 *
		 * @param string $module module name
		 * @return bool
		 */
		private function cache_is_valid(string $module): bool
		{
			// Compare mtime
			if (!isset($this->session['cache']['modules'][$module]['ttl'])) {
				return false;
			}

			$moduleName = self::get_class_from_module($module);
			if (self::class_is_surrogate($module)) {
				$moduleName .= '_Surrogate';
			}

			return filemtime((new ReflectionClass($moduleName))->getFileName()) <
				$this->session['cache']['modules'][$module]['ttl'];
		}

		private function update_module_ttl($module, $value = null): bool
		{
			$this->session['cache']['modules'][$module]['ttl'] = $value ?? $_SERVER['REQUEST_TIME'];

			return true;
		}

		/**
		 * @param string $module module name
		 * @return Module_Skeleton|null
		 */
		private function get_instance($module): ?Module_Skeleton
		{
			if (isset($this->modules[$module][ModuleCache::TYPE_INSTANCE]) || $this->load_module($module)) {
				return $this->modules[$module][ModuleCache::TYPE_INSTANCE] ?? null;
			}
			if (is_debug()) {
				fatal(Error_Reporter::get_debug_bt());
			} else {
				error_log(\Error_Reporter::get_debug_bt());
			}
			fatal("module `$module' not found");
		}

		private static function _exported_commands(Module_Skeleton $i): array
		{
			if (method_exists($i, '_init')) {
				$i->_init();
			}
			$exported = $i->getExportedFunctions();
			$reflection = new ReflectionClass($i);
			if (isset($exported['*'])) {
				$exported = array_merge(self::_apply_wildcard_permission(
					$reflection,
					$exported['*'],
					$exported
				), $exported);
				unset($exported['*']);
			}

			return $exported;
		}

		/**
		 * Apply default permission to undefined functions
		 *
		 * @param ReflectionClass $r        instance of class
		 * @param int             $permission
		 * @param array           $declared reference table of defined functions
		 * @return array
		 */
		private static function _apply_wildcard_permission(
			ReflectionClass $r,
			$permission,
			array $declared = array()
		): array {
			$methods = $r->getMethods();
			$apply = array();
			$traitNames = [];

			foreach ($r->getTraits() as $trait) {
				$hash = $trait->getName();
				if (!isset(self::$knownTraits[$hash])) {
					self::$knownTraits[$hash] = array_flip(array_column($trait->getMethods(), 'name'));
				}
				$traitNames += self::$knownTraits[$hash];
			}

			foreach ($methods as $method) {
				$name = $method->name;
				if (isset($declared[$name])) {
					continue;
				}
				$declaring = $method->getDeclaringClass()->getName();
				if ($name[0] === '_' || isset($traitNames[$name]) || !$method->isPublic() || 0 === strpos($declaring,
						self::MODULE_SKELETON_NAMESPACE) || 0 === strncmp($declaring, 'Module\\Support\\', 15)
				) {
					continue;
				}
				$apply[$name] = $permission;
			}

			return $apply;
		}

		/**
		 * Loads up imported functions from a given class name
		 *
		 * @param string $module file name of the module, extension and path stripped
		 * @access protected
		 */
		protected function add_backend_exported_functions(string $module): void
		{
			/** we'll need to use the reflection class to find out which methods
			 * are visible to the instance of the class */
			if ($this->modules[$module]['surrogate']) {
				$class = self::get_surrogate_from_module($module);
			} else {
				$class = self::get_class_from_module($module);

			}
			$reflection = new ReflectionClass($class);
			$modcache = &$this->modules[$module];
			$exported = self::_exported_commands($this->modules->getModule($module));
			foreach ($reflection->getMethods() as $method) {
				$methodName = $method->getName();
				if (array_key_exists($methodName, $exported) &&
					$method->isPublic() && !$method->isAbstract()
				) {
					/** the method exists, it's public, not abstract, and it is exported */
					//fwrite(STDOUT,"   - Exported function ".$method->getName()." for ".$class."\n");
					$modcache[ModuleCache::TYPE_FUNCTIONS][$methodName] =
						array(
							ModuleCache::TYPE_PERMISSION => $exported[$methodName],
							ModuleCache::TYPE_MINARG     => $method->getNumberOfRequiredParameters(),
							ModuleCache::TYPE_MAXARG     => $method->isVariadic() ? null : $method->getNumberOfParameters()
						);
				}
			}
		}

		public function authorized_functions($module): array
		{
			$this->validate_functions($module);

			return array_keys($this->modules[$module][ModuleCache::TYPE_FUNCTIONS]);

		}

		public function __call($cmd, array $args = array())
		{
			return $this->call($cmd, $args);
		}

		public function call($cmd, array $args = array())
		{
			/** fix if we pass zero args, transform it back to null */
			$pos = strpos($cmd, '_');
			$method = substr($cmd, $pos + 1);
			$class = substr($cmd, 0, $pos);
			if (!$method) {
				return error('%s: no such command', $cmd);
			}
			/**
			 * @var \Module\Skeleton\Standard $instance
			 */
			if (!$class) {
				return error('%s: invalid command signature', $cmd);
			}
			$instance = $this->get_instance($class);
			if (!$instance) {
				return error('%s: non-existent module', $cmd);
			}

			if (!$this->check_permissions($class, $method)) {
				if (!IS_CLI) {
					return false;
				}

				if (!method_exists($instance, $method)) {
					return error('%s: no such command', $cmd);
				}

				return error('%s: insufficient privileges to call', $cmd);
			}

			if (!isset($this->modules[$class][ModuleCache::TYPE_FUNCTIONS][$method])) {
				return error("command `%s' does not exist", $cmd);
			}

			if ($this->auth) {
				$instance->setUserParameters($this->auth);
			}

			if (null !== $this->webHookBuffer && $this->session['webhooks']) {
				$this->webHookBuffer[] = array(
					$class,
					$method,
					$args
				);
			}

			//if (IS_CLI) $instance->clean_user_parameters();
			return $instance->_invoke($method, $args);
		}

		public function check_permissions($module, $function): bool
		{
			if (!isset($this->modules[$module])) {
				return error('%s: module does not exist', $module);
			}

			$fns = &$this->modules[$module][ModuleCache::TYPE_FUNCTIONS];
			if ((!IS_CLI || $this->auth) && !isset($fns[$function])) {
				$this->add_module_cache($module);
			}

			// CLI and SOAP interfaces export all methods
			// if, after validating, the command still doesn't exist
			// then it *must* really not exist
			if ($this->auth || (!IS_CLI && !IS_SOAP)) {
				return isset($fns[$function]) ||
					($this->update_module_ttl($module, 0 /* force refresh next time */) &&
						error('%s_%s: command does not exist', $module, $function));
			}

			// SAPI loads only those for which the client has permission to access
			if (!isset($fns[$function][ModuleCache::TYPE_PERMISSION])) {
				return false;
			}
			$usrperms = $this->permission_level;
			$fnperm = $fns[$function][ModuleCache::TYPE_PERMISSION];

			if ($usrperms & $fnperm & ~0xfff0 & ~($usrperms & ~0xfff0) |
				$usrperms & $fnperm & ~0x000f & ~($fnperm & ~0x000f)
			) {
				// user lacked permissions
				return error('%s_%s: insufficient privileges to execute',
					$module, $function);
			}

			return true;
		}

		public function verify_args($cmd, $args): bool
		{
			$method = substr($cmd, strpos($cmd, '_') + 1);
			$class = substr($cmd, 0, strpos($cmd, '_'));
			$argc = count($args);
			$this->validate_functions($class);
			if (!isset($this->modules[$class][ModuleCache::TYPE_FUNCTIONS][$method])) {
				return true;
			}
			$max_args = $this->modules[$class][ModuleCache::TYPE_FUNCTIONS][$method][ModuleCache::TYPE_MAXARG];
			$min_args = $this->modules[$class][ModuleCache::TYPE_FUNCTIONS][$method][ModuleCache::TYPE_MINARG];
			if ($argc < $min_args || ($max_args !== null && $argc > $max_args)) {
				return error('%s_%s: wrong # args (got %u expected %u)',
					$class,
					$method,
					$argc,
					$argc < $min_args ? $min_args : $max_args
				);
			}

			return true;
		}

		/**
		 * Run a named hook on a module
		 *
		 * @param string $hook
		 * @param string $module
		 * @param mixed  $args
		 * @return bool
		 */
		public function runHook(string $hook, string $module, ...$args): bool
		{
			if (!self::module_exists($module)) {
				return error("unknown module `%s'", $module);
			}

			$className = self::get_autoload_class_from_module($module);
			if ($hook[0] !== '_') {
				$hook = '_' . $hook;
			}
			if (!method_exists($className, $hook)) {
				return true;
			}
			$class = $this->get_instance($module);

			return $class->$hook(...$args);
		}
	}