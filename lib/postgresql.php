<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class PostgreSQL
	{
		/* database connection */
		/** singleton tracking */
		static private $instance;
		static private $pdoConnection;
		private static $host = POSTGRESQL_HOST;
		private static $user = POSTGRESQL_USER;
		private static $passwd = POSTGRESQL_PASSWORD;
		private static $database = POSTGRESQL_DATABASE;
		/** mimic mysqli */
		public $error;
		private $dbh;
		private $qHandler;

		private const LOCAL_SOCKET = '/var/run/postgresql';

		private function __construct(
			$mHost = null,
			$mUsename = null,
			$mPassword = null,
			$mDatabase = null,
			$mPort = null
		) {
			$this->error = null;
			$this->connect($mHost, $mUsename, $mPassword, $mDatabase, $mPort);

		}

		public function connect(
			$mHost = null,
			$mUsename = null,
			$mPassword = null,
			$mDatabase = null,
			$mPort = null
		): self {
			if (is_null($mHost)) {
				$mHost = self::$host;
			}
			if (is_null($mUsename)) {
				$mUsename = self::$user;
			}
			if (is_null($mPassword)) {
				$mPassword = self::$passwd;
			}
			if (is_null($mDatabase)) {
				$mDatabase = self::$database;
			}
			if ($mHost === 'localhost' && $mUsename === APNSCP_SYSTEM_USER) {
				// use peer-based authentication
				$mHost = self::LOCAL_SOCKET;
			}
			$connStr = (!is_null($mHost) ? 'host=' . $mHost . ' ' : '');
			$connStr .= (!is_null($mUsename) ? 'user=' . $mUsename . ' ' : '');
			$connStr .= (!is_null($mPassword) ? "password='" . str_replace(["'", '\\'], ["\\'", '\\\\'],
						$mPassword) : '') . "' ";
			$connStr .= (!is_null($mDatabase) ? 'dbname=' . $mDatabase . ' ' : '');
			$connStr .= (!is_null($mPort) ? 'port=' . $mPort : '');

			if (!posix_getuid() && $mHost === self::LOCAL_SOCKET) {
				// allow peer authentication to succeed when intraserver communication blocked
				// via private network in systemd (PrivateNetworking=yes)
				posix_seteuid(posix_getpwnam(APNSCP_SYSTEM_USER)['uid']);
				defer($_, static function () {
					posix_seteuid(0);
				});
			}
			if (!($this->dbh = pg_connect($connStr))) {
				throw new PostgreSQLError('Unable to connect to pgsql db: ' . pg_last_error());

				return false;
			}

			return $this;
		}

		/**
		 * Get a PDO connection of the primary DB conn
		 *
		 * @param bool $force
		 * @return \PDO
		 */
		public static function pdo(bool $force = false): PDO
		{
			if (null === self::$pdoConnection || $force) {
				$connection = new PDO(
					'pgsql:host=' . self::$host . ';dbname=' . self::$database,
					self::$user,
					self::$passwd,
					[PDO::ATTR_PERSISTENT => false]
				);
				if (null === self::$pdoConnection) {
					self::$pdoConnection = $connection;
				} else if ($force) {
					return $connection;
				}
			}

			return self::$pdoConnection;
		}

		public static function stub(): self
		{
			$reflection = new ReflectionClass(__CLASS__);

			return $reflection->newInstanceWithoutConstructor();
		}

		public function __destruct()
		{
			if (!is_null($this->dbh)) {
				$this->close();
			}
		}

		public function close()
		{
			if (is_null($this->dbh) || !$this->dbh) {
				return false;
			}
			if (pg_close($this->dbh)) {
				$this->dbh = null;
			}

			return true;
		}

		public function __clone()
		{
			throw new apnscpException('Cannot clone PostgreSQL class');
		}

		public function __wakeup()
		{
			self::initialize();
		}

		public static function initialize(
			$mHost = null,
			$mUsename = null,
			$mPassword = null,
			$mDatabase = null,
			$mPort = null
		) {
			if (!self::$instance || !self::$instance->ping()) {
				self::$instance = new static($mHost, $mUsename, $mPassword, $mDatabase, $mPort);
			}

			return self::$instance;
		}

		public function query($mStr)
		{
			if (!$this->dbh) {
				$this->connect();
			}
			$this->error = false;
			if (!$this->ping()) {
				throw new PostgreSQLError('Unable to ping pg database: ' . pg_last_error());

				return false;
			}

			$this->qHandler = pg_query($this->dbh, $mStr);

			if (!$this->qHandler) {
				$this->error = pg_last_error($this->dbh);
				Error_Reporter::report($this->error);
				if (is_debug()) {
					print 'Query: ' . (IS_ISAPI ? '<code><pre>' : '') .
						$mStr . (IS_ISAPI ? '</pre></code>' : "\n");
				}
			}

			return $this;
		}

		public function ping()
		{
			return $this->dbh && pg_ping($this->dbh);
		}

		public function num_rows()
		{
			if (!$this->qHandler) {
				fatal('doh');
			}

			return pg_num_rows($this->qHandler);
		}

		public function affected_rows()
		{
			if (!$this->qHandler) {
				return false;
			}

			return pg_affected_rows($this->qHandler);
		}

		public function query_params($mStr, array $mParams)
		{
			if (!$this->dbh) {
				$this->connect();
			}
			$this->error = false;
			if (!pg_ping($this->dbh)) {
				throw new PostgreSQLError('Unable to ping pg database: ' . pg_last_error());

				return null;
			} else {
				$this->qHandler = pg_query_params($this->dbh, $mStr, $mParams);
			}
			$this->error = pg_last_error($this->dbh);

			return $this;
		}

		/**
		 * @param null $mHandle
		 * @return null|object
		 * @throws PostgreSQLError
		 */
		public function fetch_object($mHandle = null): ?stdClass
		{
			if (null === $this->dbh) {
				throw new PostgreSQLError('Unable to close non-existent PostgreSQL db connection.');
			}
			if (null === $mHandle) {
				$mHandle = $this->qHandler;
			}
			if ($mHandle === false) {
				report(pg_last_error());
				error('database connection error - failed to retrieve record');

				return null;
			}
			$row = pg_fetch_object($mHandle);

			if ($row === false) {
				if (($str = pg_last_error()) != false) {
					throw new PostgreSQLError('Error in row retrieval: ' . pg_last_error());
				}
				pg_free_result($mHandle);
				$this->error = false;

				return null;
			}

			return $row;
		}

		public function fetch_assoc($mHandle = null)
		{
			if (is_null($this->dbh)) {
				throw new PostgreSQLError('Unable to close non-existent PostgreSQL db connection.');

				return null;
			}
			if (is_null($mHandle)) {
				$mHandle = $this->qHandler;
			}
			$row = pg_fetch_assoc($mHandle);
			if ($row === false) {
				if (($str = pg_last_error()) != false) {
					throw new PostgreSQLError('Error in row retrieval: ' . pg_last_error());
				}
				pg_free_result($mHandle);
				$this->error = false;

				return null;
			}

			return $row;
		}

		public function escape_string($mString)
		{
			if (is_null($this->dbh)) {
				$this->connect();
			}

			return pg_escape_string($this->dbh, $mString);
		}

		public function error()
		{
			return !$this->dbh ?? pg_last_error($this->dbh);
		}

		public function getHandler()
		{
			return $this->dbh;
		}
	}
