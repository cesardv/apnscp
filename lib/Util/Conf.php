<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Util_Conf
	{
		// apnscpFunctionInterceptor instance
		private static $afi_caller;

		// locate Google Analytics here for now
		public static function get_analytics_api_key()
		{
			$card = UCard::get();
			$key = $card->getGoogleApiKey();

			return $card->getPref($key);
		}

		public static function set_analytics_api_key($user, $secret = null)
		{
			$card = UCard::get();
			$key = $card->getGoogleApiKey();
			$user = trim($user);
			if ($user && !preg_match(Regex::MISC_GOOGLE_API_CLIENT_ID, $user)) {
				return error('invalid Google API client ID');
			}
			if (!$user) {
				return $card->setPref($key, null);
			}
			$user = array('user' => $user, 'secret' => $secret, 'key' => $key);

			return $card->setPref($key, $user);
		}

		public static function get_timezone()
		{
			$card = UCard::get();
			$key = $card->getTimezoneKey();

			return $card->getPref($key);
		}

		public static function home_directory($user = null)
		{
			return self::call('user_get_user_home', $user);
		}

		/**
		 * Call arbitrary functions directly
		 */
		public static function call($function, $args = array())
		{
			return self::__callStatic($function, $args);
		}

		public static function __callStatic($function, $args = array())
		{
			if (!self::$afi_caller) {
				self::$afi_caller = apnscpFunctionInterceptor::init();
			}
			if (!is_array($args)) {
				$args = array($args);
			}
			$data = self::$afi_caller->call($function, $args);

			return $data;
		}

		/**
		 * @until 3.3
		 */
		public static function domain_fs_path()
		{
			deprecated("Use Auth_Info_User::domain_fs_path()");
			return FILESYSTEM_VIRTBASE . '/' . Auth::profile()->site . '/fst';
		}

		/**
		 * @until 3.3
		 */
		public static function domain_info_path()
		{
			deprecated("Use Auth_Info_User::domain_info_path()");
			return FILESYSTEM_VIRTBASE . '/' . Auth::profile()->site . '/info';
		}

		/**
		 * @until 3.3
		 */
		public static function domain_shadow_path()
		{
			deprecated("Use Auth_Info_User::domain_shadow_path()");
			return FILESYSTEM_VIRTBASE . '/' . Auth::profile()->site . '/shadow';
		}

		public static function mailbox_domains()
		{
			$domains = self::call('email_list_virtual_transports');
			Util_Conf::sort_domains($domains);

			return $domains;
		}

		/**
		 * @param        $domains
		 * @param string $method
		 * @param bool   $collapse_subdomains
		 */
		public static function sort_domains(&$domains, $method = 'value', $collapse_subdomains = false)
		{
			if (!$domains) {
				return;
			}
			$func = self::_sort_wrapper($method);
			if ($collapse_subdomains) {
				$algo = new \Util\Sort\CollapsibleDomain();
			} else {
				$algo = ['self', '_sort_algo_domains'];
			}

			return $func($domains, $algo);
		}

		private static function _sort_wrapper($method)
		{
			switch ($method) {
				case 'value':
					return 'usort';
				case 'key':
					return 'uksort';
				case 'assoc':
					return 'uasort';
			}
			fatal("unknown sort mode `%s'", $method);
		}

		/**
		 * Sort webapps
		 *
		 * @param array        $webapps assoc array, contains ['hostname', 'path']
		 * @param Closure|null $additional extra sorting function
		 * @return bool
		 */
		public static function sort_webapps(&$webapps, Closure $additional = null): bool
		{
			$collapsible = new \Util\Sort\CollapsibleDomain();

			return uasort($webapps, static function ($a, $b) use ($collapsible, $additional) {
				if ($additional && ($ret = $additional($a, $b))) {
					return $ret;
				}

				if ($a['hostname'] === $b['hostname']) {
					return strcmp((string)($a['path'] ?? ''), (string)($b['path'] ?? ''));
				}

				return $collapsible($a['hostname'], $b['hostname']);
			});
		}

		public static function users($user_key = true)
		{
			return $user_key ? self::call('user_get_users') : array_keys(self::call('user_get_users'));
		}

		public static function file_get_contents($file)
		{
			return self::call('file_get_file_contents', array($file));
		}

		public static function all_domains()
		{
			$domains = array(self::get_svc_config('siteinfo', 'domain'));
			$aliases = self::call('aliases_list_aliases');
			if ($aliases) {
				$domains = array_merge($domains, $aliases);
			}

			return $domains;
		}

		public static function get_svc_config($svc_class, $svc_param)
		{
			return self::call('common_get_service_value', array($svc_class, $svc_param));
		}

		public static function all_websites()
		{
			$domains = self::call('web_list_domains');
			$subdomains = self::call('web_list_subdomains');

			return array('domains' => $domains, 'subdomains' => $subdomains);
		}

		public static function user_role()
		{
			$permlvl = $_SESSION['level'];

			if ($permlvl & PRIVILEGE_SITE) {
				return 'site';
			} else if ($permlvl & PRIVILEGE_ADMIN) {
				return 'admin';
			} else if ($permlvl & PRIVILEGE_USER) {
				return 'user';
			} else if ($permlvl & PRIVIELGE_RESELLER) {
				return 'reseller';
			}

			return error($permlvl . ': unknown privilege level');
		}

		/**
		 * emulate php file_exists() function for  virtual domains
		 */
		public static function file_exists($file)
		{
			return self::call('file_exists', array($file));
		}

		/**
		 * current control panel version
		 *
		 * @return array
		 */
		public static function cp_version($field = '')
		{
			if (!isset($_SESSION['cp_rev'])) {
				$_SESSION['cp_rev'] = self::call('misc_cp_version');
			}
			if ($field === 'full') {
				$str = implode('.', ([
					$_SESSION['cp_rev']['ver_maj'],
					$_SESSION['cp_rev']['ver_min'],
					$_SESSION['cp_rev']['ver_patch']
				]));
				if (!empty($_SESSION['cp_rev']['ver_pre'])) {
					$str .= '-' . $_SESSION['cp_rev']['ver_pre'];
				}
				return $str;
			}
			return !$field ? $_SESSION['cp_rev'] : $_SESSION['cp_rev'][$field];
		}

		public static function login_domain()
		{
			return $_SESSION['entry_domain'];
		}

		/**
		 * Opens up a file and parses site metadata
		 *
		 * @param  string $file
		 * @return array
		 */
		public static function parse_ini($file): array
		{
			$conf = parse_ini_file($file, false, INI_SCANNER_RAW);
			if (false === $conf) {
				fatal("failed to parse file `%s', data: %s", $file, file_get_contents($file));
			}
			foreach ($conf as $k => $v) {
				$conf[$k] = static::inferType($v);
			}

			return $conf;
		}

		/**
		 * Infer data type from string
		 *
		 * @param $v
		 * @return array|float|int|null
		 */
		public static function inferType($v)
		{
			if ($v === 'None' || !isset($v[0])) {
				return null;
			} else if ($v[0] === '[') {
				$vals = preg_split('/,\s*/', trim($v, '[]'));

				return array_filter(array_build($vals, Closure::fromCallable('static::parseComplex')));
			} else if (is_numeric($v)) {
				return (int)$v == (float)$v ? (int)$v : (float)$v;
			} else if (($tmp = strtolower($v)) === 'false') {
				return false;
			} else if ($tmp === 'true') {
				return true;
			}

			return $v;
		}

		/**
		 * Commit metadata to record
		 *
		 * @param mixed $params
		 * @return string
		 */
		public static function build_ini($params): string
		{
			if (!is_array($params)) {
				return $params ?? 'None';
			} else if (empty($params)) {
				return '[]';
			}
			$collapse = static function ($a, $key) {
				if (is_array($a)) {
					return self::build_ini($a);
				}
				if (is_numeric($a)) {
					return (!is_int($key) ? $key . ':' : '') . $a;
				}

				return (!is_int($key) ? "'$key':" : '') . "'" . $a . "'";
			};
			$lines = '';
			// quicker than while()... by 0.003 ms per call
			// hooray microoptimzation
			foreach ($params as $k => $v) {
				$line = (is_int($k) ? '' : $k . '=');
				if (null !== $v && !is_array($v)) {
					$line .= $v;
				} else if (null === $v) {
					$line .= 'None';
				} else {
					$line .= '[' . join(',',
							array_map($collapse, $v, array_keys($v))) . ']';
				}
				$lines .= $line . "\n";
			}

			return $lines;
		}

		public static function server_ip()
		{
			static $ip;

			if (isset($_SERVER['SERVER_ADDR'])) {
				return $_SERVER['SERVER_ADDR'];
			} else if (isset($ip)) {
				return $ip;
			}
			$proc = Util_Process::exec('ip route get %s', Auth::client_ip());
			$output = $proc['output'];
			$ip = '127.0.0.1';
			if (!$proc['success']) {
				return $ip;
			}
			if (!preg_match('/src\s+([\d.:a-f]+)\s*$/m', $output, $myip)) {
				return $ip;
			}
			$ip = array_pop($myip);

			return $ip;
		}

		public static function sort_files(&$files, $method = 'value', $tree = false)
		{
			if (!$files) {
				return;
			}

			$func = self::_sort_wrapper($method);

			if ($tree) {
				$algo = '_sort_algo_file_tree';
			} else {
				$algo = '_sort_algo_files';
			}

			// PHP 8 is reporting $files passed by value?
			return @$func($files, ['self', $algo]);
		}

		private static function parseComplex($key, $value = null)
		{
			if ($value === '') {
				return '';
			}
			$value = trim($value, "'");
			if (ctype_digit($value)) {
				return [$key, (int)$value == (float)$value ? (int)$value : (float)$value];
			}
			if (false === strpos($value, ':')) {
				return [$key, $value];
			}
			[$key, $value] = explode(':', $value, 2);
			return [rtrim($key, "'"), self::inferType(ltrim($value, "'"))];
		}

		private static function _sort_algo_file_tree(&$a, &$b)
		{
			return strnatcasecmp($a['file_name'], $b['file_name']);
		}

		private static function _sort_algo_files(&$a, &$b)
		{
			if ((($a['file_type'] == 'dir') || $a['link'] == 2) &&
				(($b['file_type'] != 'dir') && $b['link'] != 2)
			) {
				return -1;
			} else {
				if ((($b['file_type'] == 'dir') || ($b['link'] == 2)) &&
					(($a['file_type'] != 'dir') && ($a['link'] != 2))
				) {
					return 1;
				} else {
					return strnatcasecmp($a['file_name'], $b['file_name']);
				}
			}
		}

		/**
		 * Sort hostnames allocating subdomains beneath the parent
		 *
		 * @param $a
		 * @param $b
		 * @return int
		 */
		private static function _sort_algo_domain_collapse($a, $b)
		{
			//var_dump("NEW SORT ROUND!!!!");

		}

		private static function _sort_algo_domains($a, $b)
		{
			if ($a === $b) {
				return 0;
			}
			if (false !== strpos($a, '.') && false === strpos($b, '.')) {
				return 1;
			}
			if (false === strpos($a, '.') && false !== strpos($b, '.')) {
				return -1;
			}
			$cnta = substr_count($a, '.') + 1;
			$cntb = substr_count($b, '.') + 1;
			if ($cnta === $cntb) {
				return strnatcmp($a, $b);
			}
			if ($cnta > $cntb) {
				return 1;
			}

			return -1;
		}
	}