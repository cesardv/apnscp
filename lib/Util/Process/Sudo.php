<?php

	/**
	 * Add sudo support to Util_Process
	 *
	 * MIT License
	 *
	 * @author  Matt Saladna <matt@apisnetworks.com>
	 * @license http://opensource.org/licenses/MIT
	 * @version $Rev: 2732 $ $Date: 2017-03-25 14:54:59 -0400 (Sat, 25 Mar 2017) $
	 */
	class Util_Process_Sudo extends Util_Process_Safe
	{
		use apnscpFunctionInterceptorTrait;
		use ContextableTrait;

		public function setUser($user)
		{
			$domain = $this->getAuthContext()->domain;

			if (false !== ($pos = strpos($user, '@'))) {
				$domain = substr($user, $pos + 1);
				$user = substr($user, 0, $pos);
			}
			if (!is_array($user)) {
				if (false !== ($pos = strpos($user, '@'))) {
					$domain = substr($user, $pos);
					$user = substr($user, $pos);
				}
			} else {
				if (isset($user['domain'])) {
					$domain = $user['domain'];
				}
				$user = $user['user'] ?? $this->getAuthContext()->username;
			}

			// for security reasons, reset the domain to the account
			if ($this->getAuthContext()->level & (PRIVILEGE_USER | PRIVILEGE_SITE)) {
				$domain = $this->getAuthContext()->domain;
			} else if ($domain && !Auth::domain_exists($domain)) {
				fatal("domain `%s' does not exist", $domain);
			}

			if ($domain && !\apnscpFunctionInterceptor::factory($this->getAuthContext())->user_exists($user)) {
				fatal("user `%s' does not exist", $user);
			}

			return $this->setUserRaw($user . ($domain ? '@' . $domain : ''));
		}

		public function setUserRaw($user)
		{
			/**
			 * Set user without added lookups
			 */
			$this->opts['user'] = $user;

			return true;
		}

		/**
		 * Additional options:
		 *    user:    target username
		 *  domain:  target domain (default to current domain)
		 *
		 * @param string $cmd
		 * @param mixed  $args
		 * @param mixed  $exits
		 * @param mixed  $options
		 */
		public function run($cmd, ...$args)
		{
			if (posix_getuid() != 0) {
				return error(
					'%s: cannot sudo without root',
					Error_Reporter::get_caller(1, '/^Util_/')
				);
			}

			if (null !== ($umask = $this->getOption('umask'))) {
				$umask = 'umask ' . decoct($umask) . ' &&';
			}
			$user = $this->getOption('user');
			if (!$user) {
				$cred = array(
					'domain' => $this->getAuthContext()->domain,
					'user'   => $this->getAuthContext()->username
				);
				$user = $cred['user'];
				if ($cred['domain']) {
					$user .= '@' . $cred['domain'];
				}
			}
			if (null === ($home = $this->getOption('home'))) {
				$home = '/tmp';
			} else {
				$home = '~';
			}

			$cmd = sprintf('/bin/su -s /bin/sh %s -%sc %s',
				$user,
				$this->getOption('login') ? 'i' : null,
				escapeshellarg($umask . 'cd ' . $home . ' && ' . $cmd)
			);
			/*
			 * Won't work on sudo 1.8.6p4+
			 * /etc/passwd is persisted after jail, need further investigation
			$cmd = sprintf("sudo %s -n%su %s -- /bin/sh -c %s",
				$this->getOption('home') ? '-H' : null,
				$this->getOption('login') ? 'i' : null,
				$user,
				escapeshellarg($umask . $cmd)
			);*/

			return parent::run($cmd, ...$args);
		}

		protected function synthesizeCommand(string $cmd, array $args = []): string
		{
			$args = $this->deepEscape($args);
			foreach ($args as $k => $v) {
				// double up since su -c '<CMD>' adds first round of single quotes
				$args[$k] = str_replace("'", "'\\''", $v);
			}
			return vsprintf($cmd, $args);
		}
	}
