<?php

	namespace Lararia\Console\Commands;

	use Illuminate\Console\Command;
	use Opcenter\Auth\Password;
	use Opcenter\CliParser;
	use Opcenter\Filesystem;
	use Opcenter\Map;
	use Opcenter\Service\Plans;
	use Opcenter\SiteConfiguration;
	use Symfony\Component\Console\Helper\Table;
	use Symfony\Component\Console\Helper\TableSeparator;
	use Symfony\Component\Finder\Finder;

	/**
	 * @property Finder finder
	 */
	class OpcenterPlanCommand extends Command
	{
		/**
		 * The name and signature of the console command.
		 *
		 * @var string
		 */
		protected $signature = 'opcenter:plan {plan?}
		{--list : Show available plans.}
    	{--new : Create a new plan.}
    	{--base= : Used with --new to rebase plan.}
    	{--c|config=* : Used with --new append config defaults.}
    	{--default : Set plan as default.} 
    	{--remove : Remove named plan.}
    	{--verify : Verify plan is coherent.}
    	{--diff= : Show differences in plan against target.}';

		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'Manage account plans';
		private $euid;

		/**
		 * Create a new command instance.
		 *
		 * @return void
		 */
		public function __construct()
		{
			parent::__construct();
			$this->finder = new Finder();
		}

		public function __destruct()
		{
			if ($this->euid) {
				posix_seteuid($this->euid);
			}
		}

		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle()
		{
			if ($this->option('list')) {
				$this->line('* marks default plan');
				foreach (Plans::list() as $plan) {
					if ($plan === OPCENTER_DEFAULT_PLAN) {
						$str = "<comment>*</comment> $plan";
					} else {
						$str = $plan;
					}
					$this->info("  $str");
				}

				return;
			}

			$plan = $this->argument('plan');
			if (null !== ($target = $this->option('diff'))) {
				$target = $this->option('diff');
				if (!Plans::exists($target)) {
					throw new \ArgumentError("Plan ${target} does not exist");
				}
				if (!Plans::exists($plan)) {
					throw new \ArgumentError("Plan ${target} does not exist");
				}
				$dummy = (new SiteConfiguration(null))->setPlanName($target)->getDefaultConfiguration();

				$table = new Table($this->output);
				$table->setHeaders(['Service', 'Var', $plan , $target]);
				$skip = true;
				foreach (Plans::diff($plan, $target) as $svc => $vars) {
					if (!$skip) {
						$table->addRow(new TableSeparator());
					}
					$skip = false;
					foreach ($vars as $var => $val) {
						$table->addRow([
							$svc,
							$var,
							$val,
							array_get($dummy, "${svc}.${var}", null)
						]);
					}
				}
				$table->render();
				return;
			}

			if ($this->option('verify')) {
				if (!Plans::exists($plan)) {
					throw new \ArgumentError("Plan ${plan} does not exist");
				}

				$site = (new SiteConfiguration('site0', [
					'siteinfo' => [
						'admin'       => 'admin9999',
						'admin_user'  => Password::generate(16, 'a-z'),
						'domain'      => Password::generate(32, 'a-z') . '.test'
					]
				]))->setPlanName($plan);
				$site->releaseOnShutdown();

				if ($this->verify($site)) {
					return true;
				}

				$errors = \Error_Reporter::get_errors();
				throw new \RuntimeException($errors[0]);
			}

			if ($this->option('default')) {
				if (!Plans::exists($plan)) {
					throw new \ArgumentError("Unknown plan ${plan}");
				}
				if ($plan === OPCENTER_DEFAULT_PLAN) {
					$this->warn("Default plan already set to ${plan}");

					return;
				}
				Map::load(config_path('custom/config.ini'), 'cd')->
				section('opcenter')->set('default_plan', $plan);
				\Opcenter\Admin\Settings\Setting::load('apnscp.restart')->set(true);
				$this->info("Default plan changed to ${plan}");

				return;
			}

			if ($this->option('new')) {
				if (Plans::exists($plan)) {
					throw new \ArgumentError("Plan ${plan} already exists");
				}
				$oldPlan = $this->option('base') ?? OPCENTER_DEFAULT_PLAN;
				if (!Plans::exists($oldPlan)) {
					throw new \ArgumentError("Base plan ${oldPlan} does not exist");
				}

				$overrides = $this->option('config') ?? [];
				$files = new \Illuminate\Filesystem\Filesystem();
				$files->copyDirectory(
					Plans::path($oldPlan),
					$path = Plans::path($plan)
				);

				if ($oldPlan === $plan) {
					throw new \RuntimeException('Old plan same as new plan');
				}
				$newParams = CliParser::parseServiceConfiguration($overrides);
				$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
				try {
					$site = (new SiteConfiguration(null, [
							'siteinfo' => [
								'admin'      => 'admin9999',
								'admin_user' => Password::generate(16, 'a-z'),
								'domain'     => Password::generate(32, 'a-z') . '.test'
							]
						] + $newParams));

					if ($oldPlan !== Plans::default()) {
						$site->setPlanName($oldPlan);
					}

					if (!$this->verify($site)) {
						throw new \RuntimeException('Failed to verify new plan');
					}
				} catch (\apnscpException|\RuntimeException $e) {
					Plans::remove($plan);
					throw $e;
				} finally {
					\Error_Reporter::exception_upgrade($oldex);
				}

				$defaults = array_replace_recursive($site->getDefaultConfiguration(), $newParams);

				if (!Plans::assignDefaults($plan, $defaults)) {
					$this->error("Failed to assign defaults to ${path}");
				}

				$this->info("Plan created in ${path}");

				return;
			}

			if ($this->option('remove')) {
				if ($plan === OPCENTER_DEFAULT_PLAN) {
					throw new \ArgumentError('Cannot remove default plan');
				}
				if (!Plans::exists($plan)) {
					throw new \ArgumentError("Unknown plan ${plan}");
				}
				if (!Filesystem::rmdir(Plans::path($plan))) {
					throw new \RuntimeException("Failed to remove plan directory ${plan}");
				}

				return;
			}
			return $this->error('Usage: ' . $this->getSynopsis());
		}

		/**
		 * Verify plan configuration
		 *
		 * @param SiteConfiguration $cfg
		 * @return bool
		 */
		private function verify(SiteConfiguration $cfg): bool
		{
			$this->euid = posix_geteuid();
			posix_seteuid(0);
			$cfg->releaseOnShutdown();
			$old = \Error_Reporter::set_verbose(0);
			$ret =  $cfg->verifyAll();
			\Error_Reporter::set_verbose($old);

			// replay buffer errors/exceptions
			\Error_Reporter::merge_buffer(
				\Error_Reporter::get_buffer(\Error_Reporter::E_ERROR|\Error_Reporter::E_EXCEPTION)
			);

			return $ret;
		}
	}
