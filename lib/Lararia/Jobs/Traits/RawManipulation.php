<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs\Traits;

	use Illuminate\Contracts\Queue\ShouldQueue;
	use Illuminate\Queue\InteractsWithQueue;
	use Illuminate\Redis\Connections\Connection;

	/**
	 * Trait RawManipulation
	 *
	 * @package Lararia\Jobs\Traits
	 */
	trait RawManipulation
	{
		use InteractsWithQueue;
		protected $jobId;

		/**
		 * Get job field
		 *
		 * @param string $field
		 * @return string
		 */
		public function getField(string $field): string
		{
			$connection = $this->getConnection();

			return (string)$connection->hget($this->getJobId(), $field);
		}

		/**
		 * Set a job field
		 *
		 * @param string $field
		 * @param mixed  $value
		 * @return int
		 */
		protected function setField(string $field, $value): int
		{
			$connection = $this->getConnection();
			if (!$connection) {
				return 0;
			}

			return $connection->hset($this->getJobId(), $field, $value);
		}

		/**
		 * Get Horizon connection
		 *
		 * @return Connection
		 */
		protected function getConnection(): ?Connection
		{
			if ('sync' === ($conn = $this->getConnectionName())) {
				return null;
			}
			if (!$conn) {
				report('Connection info: %s', str_replace('%', '%%', var_export($this, true)));
				//fatal('failed to acquire queue connection');
				// @xxx not recommended
				$conn = config('queue.default');
			}

			return app($conn)->connection('horizon');
		}

		protected function getConnectionName(): string
		{
			if (!$this instanceof ShouldQueue) {
				return 'sync';
			}

			if (isset($this->connection)) {
				return $this->connection;
			}

			$this->checkJobSet();
			return $this->job->getConnectionName();
		}

		/**
		 * Verify $job property is accessible
		 */
		private function checkJobSet(): void
		{
			if (!$this->job) {
				if (is_debug()) {
					\Error_Reporter::print_debug_bt();
				}
				fatal("setJob() not called first in `%s' %s", \get_class($this), \Error_Reporter::get_caller());
			}
		}

		/**
		 * Get job ID in Redis queue
		 *
		 * @return int|null
		 */
		public function getJobId(): ?int
		{
			if (isset($this->jobId)) {
				return $this->jobId;
			}
			$this->checkJobSet();

			return (int)array_get($this->job->payload(), 'id', null);
		}
	}
