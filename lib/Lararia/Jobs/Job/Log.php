<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs\Job;

	class Log
	{
		/**
		 * @var string error message
		 */
		protected $message;
		protected $severity;

		public function __construct(array $entry)
		{
			$this->severity = $entry['severity'] ?? \Error_Reporter::E_OK;
			$this->message = (string)$entry['message'];
		}

		public function __toString(): string
		{
			return $this->message;
		}
	}