<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class apnscpObject
	{
		const  USE_TEE = 0x01;
		const  LOCKED = 0x02;
		const  MULTI = 0x04;
		const  RESET = 0x08;
		const  NO_WAIT = 0x10;
		const UNCHARGED = 0x20;

		public $command;
		public $args;
		public $error;

		// use tee file
		public $session_id;
		public $returned_data;
		public $options;
		public $error_buffer;
		private $ip;

		public function __construct(
			$mCommand,
			$mArgs,
			$mError = null,
			$mSessid = null,
			$mReturnedData = null,
			$error_buffer = array(),
			$mOptions = 0
		) {
			$this->command = $mCommand;
			$this->args = $mArgs;
			$this->session_id = $mSessid;
			$this->error = $mError;
			$this->returned_data = $mReturnedData;
			$this->options = $mOptions;
			$this->error_buffer = $error_buffer;
			$this->ip = \Auth::client_ip();
		}

		/**
		 * DS object accessor
		 *
		 * @param string $name
		 * @return mixed
		 */
		public function getField(string $name)
		{
			return $this->$name;
		}
	}

	class DataStream
	{
		const MAGIC_MARKER = "\x1\x0\x1\x0";
		const MAGIC_MARKER_LENGTH = 4;
		/**
		 * 1 32-bit PID (2^22 pid_max on 64-bit)
		 * 2 Payload
		 */
		const PACKET_HEADER = 'VZ*';

		// @var int assigned backend worker ID
		private static $workerId = 0;

		private static $instance;
		private $apnscp_socket;
		private $options;
		private $multi;
		/**
		 * @var \Auth_Info_User authentication context
		 */
		private $authContext;

		private function __construct(\Auth_Info_User $context = null)
		{
			if (!IS_CLI && (!$context || \session_id() === $context->id)) {
				self::$workerId = (int)(self::$workerId ?: \Session::get('worker-id', 0));
			}
			$this->authContext = $context;
		}

		public static function get(\Auth_Info_User $context = null)
		{
			if ($context) {
				return new static($context);
			}
			if (null === self::$instance) {
				self::$instance = new static;
			}

			return self::$instance;
		}

		public function __destruct()
		{
			$this->disconnect();
			if (!IS_CLI) {
				\Session::set('worker-id', self::$workerId);
			}
		}

		public function disconnect()
		{
			if (is_resource($this->apnscp_socket)) {
				socket_close($this->apnscp_socket);
			}
			$this->apnscp_socket = null;
		}

		public function getOptions()
		{
			return $this->options;
		}

		public function getWorkerId(): ?int
		{
			return self::$workerId;
		}

		/**
		 * Complete a multi request
		 *
		 * @return int|null
		 */
		public function sendMulti()
		{

			if (IS_CLI) {
				$resp = $this->multi;
				$this->clearOption(apnscpObject::MULTI);

				return $resp;
			}
			$buffer = IS_CLI ? Error_Reporter::flush_buffer() : null;
			$payload = $this->pack(
				$this->createPayload(
					$this->multi,
					null,
					null,
					$this->authContext->id ?? Auth::get_driver()->getID(),
					null,
					$buffer
				)
			);

			$resp = $this->pipeline($payload);
			$this->clearOption(apnscpObject::MULTI);

			return $resp;
		}

		public function clearOption($mFlag)
		{
			$this->options &= ~$mFlag;
		}

		/**
		 * Pack payload
		 *
		 * @param apnscpObject|null $data
		 * @param int               $workerId worker ID to send request
		 * @return string
		 */
		public function pack(?apnscpObject $data, int $workerId = 0)
		{
			/* add the base64 encoding at the end
			 * to terminate transmission or else
			 * it blocks after EOF */
			$p = $this->_serialize($data);

			return pack(self::PACKET_HEADER, $workerId, $p) . self::MAGIC_MARKER;
		}

		private function _serialize($mObj)
		{
			return serialize($mObj);
		}

		/**
		 * Create payload from query
		 *
		 * @param       $mName
		 * @param null  $mArgs
		 * @param null  $mError
		 * @param null  $mSessionID
		 * @param null  $mReturnValue
		 * @param array $error_buffer
		 * @return apnscpObject
		 */
		public function createPayload(
			$mName,
			$mArgs = null,
			$mError = null,
			$mSessionID = null,
			$mReturnValue = null,
			$error_buffer = array()
		) {
			return new apnscpObject($mName,
				$mArgs,
				(IS_CLI ? $mError : null),
				$mSessionID,
				$mReturnValue,
				$error_buffer,
				$this->options
			);

		}

		public function writeSocket($payload): ?string
		{
			if (null === $this->apnscp_socket) {
				$this->connect();
			}
			socket_write($this->apnscp_socket, $payload);
			if ($this->options & apnscpObject::NO_WAIT) {
				$this->disconnect();

				return null;
			}
			$buff = array();
			$w = $e = null;
			$r = array($this->apnscp_socket);
			do {
				$changed = socket_select($r, $w, $e, null);
				// lost connection during xfer
				if (false === $changed) {
					// signal encountered
					break;
				}
				if ($changed > 0) {
					$totalRead = 0;
					do {
						if (!($bytes = socket_recv($r[0], $tmp, 16384, 0))) {
							if (!$totalRead) {
								// ???
								break 2;
							}
							break;
						}
						$totalRead += $bytes;
						$buff[] = $tmp;
						if (substr($tmp, -self::MAGIC_MARKER_LENGTH) === self::MAGIC_MARKER) {
							// potential boundary issues
							break 2;
						}
					} while ($tmp);
				}
			} while (true);
			$this->disconnect();
			return implode('', $buff);
		}

		public function connect()
		{
			$socket = socket_create(AF_UNIX, SOCK_STREAM, 0) or
			fatal('Unable to create socket' . "\n%s",
				socket_strerror(socket_last_error()));
			if (socket_connect($socket, APNSCPD_SOCKET)) {
				$this->apnscp_socket = $socket;

				return $this->apnscp_socket;
			}
			$time = date('Hm');
			if ($time >= 200 && $time <= 310) {
				fatal('Control panel is peforming a regularly scheduled update.  Please try again in a few minutes...');
			}
			fatal('Unable to connect to socket, is apnscp running? ' . "\n%s",
				socket_strerror(socket_last_error()));
		}

		public function unpack($mObj): ?\apnscpObject
		{
			if (!$mObj) {
				return null;
			}
			// PHP lacks support for var length strings
			$worker = ord($mObj[0]) | (ord($mObj[1]) << 8) | (ord($mObj[2]) << 16) | (ord($mObj[3]) << 24);
			if ($worker && self::$workerId !== $worker) {
				self::$workerId = $worker;
			}
			$raw = substr($mObj, 4, -self::MAGIC_MARKER_LENGTH);
			$data = $this->_unserialize($raw);
			if (null === $data) {
				return $data;
			}
			$this->options = $data->options;
			if ($data->error_buffer) {
				Error_Reporter::merge_buffer($data->error_buffer);
			}
			if ($this->options & apnscpObject::RESET) {
				$this->clearOption(apnscpObject::RESET);
			}

			return $data;
		}

		private function _unserialize($mObj)
		{
			return unserialize($mObj);
		}

		public function multi($mCommand, ...$args)
		{
			$this->setOption(apnscpObject::MULTI);

			if (IS_CLI) {
				$this->multi[] = $this->query($mCommand, ...$args);

				return $this;
			}
			$this->multi[] = array('cmd' => $mCommand, 'args' => $args);

			return $this;

		}

		/*
		 * function: Pack <string, integer> <string> <string, NULL>
		 * purpose:  packs a command call into an apnscpObject
		 *           to send between lService and apnscp.
		 * returns:  serialized object consisting of the primary
		 *           and auxillary data referenced through
		 *           $obj->command and $obj->aux respectively
		 * contract: <string, integer> <string> -> <serialized object>
		*/

		public function setOption($mOption)
		{
			if ($this->options & $mOption) {
				return $this;
			} else if ($mOption & apnscpObject::MULTI) {
				$this->multi = array();
			}
			$this->options |= $mOption;

			return $this;
		}

		/*
		 * function: Unpack <object>
		 * purpose:  unserializes an object into its native
		 *           apnscpObject format, used for communication
		 *           between apnscp and lService.
		 * returns:  deserialized object of type apnscpObject
		 * contract: <serialized object> -> <unserialized object>
		*/

		public function query($mCommand, ...$args)
		{
			if (IS_CLI) {
				$afi = \apnscpFunctionInterceptor::init();
				if ($this->authContext && !$afi->context_matches_id($this->authContext->id)) {
					return \apnscpFunctionInterceptor::factory($this->authContext)->call($mCommand, $args);
				}

				return $afi->call($mCommand, $args);
			}
			$payload = $this->pack(
				$this->createPayload(
					$mCommand,
					$args,
					null,
					$this->authContext->id ?? Auth::get_driver()->getID(),
					null,
					null
				),
				self::$workerId
			);

			return $this->pipeline($payload);
		}

		/**
		 * Send request to backend
		 *
		 * @param string $payload
		 * @return mixed
		 */
		protected function pipeline(string $payload) {
			$response = $this->unpack(
				$this->writeSocket($payload)
			);
			$expanded = null;
			if (null === $response) {
				$expanded = $this->unpack($payload);
				if ($expanded->options & apnscpObject::NO_WAIT) {
					return null;
				}
			}
			if (!is_object($response)) {
				$expanded = $expanded ?? $this->unpack($payload);
				$command = $expanded->command;
				if ($expanded->options & apnscpObject::MULTI) {
					$command = '(MULTI) ' . implode(',', array_slice(array_column($command, 'cmd'), 0, 3));
				}
				if (is_debug()) {
					\Error_Reporter::print_debug_bt();
					var_dump($expanded);
				}
				fatal("`%s': crash or other nasty error detected", $command);

				return null;
			}

			return $response->returned_data;
		}
	}