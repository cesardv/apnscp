<?php

	use Auth\IpRestrictor as IpRestrictorAlias;
	use Opcenter\Net\IpCommon;

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	abstract class Auth
	{
		use NamespaceUtilitiesTrait;

		// session length in seconds
		const TV_SEC = 900;
		const AUTO_AUTH = true;

		// auth driver
		protected static $db;
		private static $authName;

		// database connection
		private static $authDriver;
		// user info profile
		private $activeUser;
		private $_authProfile;
		private $_authID;

		public function __construct()
		{
			apnscpSession::init();
		}


		public static function handle(): bool
		{
			if (NO_AUTH) {
				return true;
			}
			$driver = self::autoload();

			if (!$driver->session_valid() && !$driver->authenticate()) {
				return $driver->invalidate_session() && $driver->unauthorized();
			}
			self::_log_session();

			return true;
		}

		/**
		 * Handle the request
		 */
		public static function autoload(): \Auth
		{
			if (null !== self::$authDriver) {
				return self::$authDriver;
			}

			self::import(static::_autoload_name());
			self::$authName = static::_autoload_name();
			self::$authDriver = self::import(self::$authName);

			return self::$authDriver;
		}

		/**
		 * Set auth handler
		 *
		 * Possible handlers - App, SOAP, DAV
		 *
		 * @param  string $module
		 * @return \Auth
		 */
		public static function import($module): \Auth
		{
			if (!static::driver_exists($module)) {
				fatal($module . ': no suitable driver found');
			}
			$c = 'Auth_' . $module;
			$auth = new $c;
			if (!$auth) {
				fatal("error instantiating auth module `%s'", $module);
			}
			if (null === self::$authDriver) {
				self::set_handler($auth);
			}

			return $auth;
		}

		/**
		 * Requested authentication driver exists
		 *
		 * @param string $module
		 * @return bool
		 */
		public static function driver_exists(string $module): bool
		{
			$c = 'Auth_' . $module;
			return is_subclass_of($c, self::class);
		}

		/**
		 * Get driver name
		 *
		 * @return string
		 */
		public function getDriver(): string
		{
			$driver = static::getBaseClassName();
			return substr($driver, strpos($driver, '_')+1);
		}

		/**
		 * Overwrite current authentication handler
		 *
		 * @param Auth $handler
		 */
		public static function set_handler(Auth $handler): void
		{
			// close old preferences/afi instance
			\Preferences::write();
			\apnscpFunctionInterceptor::expire(\session_id());

			self::$authDriver = $handler;
		}

		private static function _autoload_name(): ?string
		{
			if (null !== self::$authDriver) {
				$class = get_class(self::$authDriver);

				return substr($class, strpos($class, '_') + 1);
			}

			if (null !== self::$authName) {
				return self::$authName;
			}

			if (!IS_CLI && !IS_DAV && !IS_SOAP) {
				return 'UI';
			}
			if (IS_DAV) {
				return 'DAV';
			}
			if (IS_SOAP) {
				return 'SOAP';
			}
			if (IS_CLI) {
				return 'CLI';
			}
		}

		/**
		 * Determine if current session is authenticated and valid
		 *
		 * @return bool
		 */
		abstract public function session_valid();

		abstract public function authenticate();

		public function invalidate_session(): bool
		{
			$_SESSION = array();

			return true;
		}

		abstract public function unauthorized();

		/**
		 * Optional hook to run prior to entering impersonation
		 *
		 * @param Auth_Info_User $ctx new impersonation context
		 * @param string|null    $gate optional authentication gate
		 * @return void
		 */
		public function beginImpersonation(\Auth_Info_User $ctx, string $gate = null): void
		{ }

		/**
		 * Optional hook to run after exiting impersonation
		 *
		 * @param Auth_Info_User $ctx prior impersonated context
		 * @return void
		 */
		public function endImpersonation(\Auth_Info_User $ctx): void
		{ }

		protected static function _log_session()
		{
			if ((!IS_ISAPI && !IS_DAV && !IS_SOAP) || !isset($_SESSION['username'])) {
				return true;
			}
			if (function_exists('apache_note')) {
				apache_note('USERNAME', $_SESSION['username']);
				apache_note('MODE', static::_autoload_name());
				apache_note('SITE_ID', $_SESSION['site_id']);
			} else {
				header('X-Auth-Username: ' . $_SESSION['username']);
				header('X-Auth-Driver: ' . static::_autoload_name());
				header('X-Auth-Site-Id: ' . $_SESSION['site_id']);
			}

			return true;
		}

		public static function use_handler_by_name($handler): void
		{
			if (!self::driver_exists($handler)) {
				fatal("authentication handler `%s' does not exist", $handler);
			}
			self::$authName = $handler;
			self::$authDriver = null;
		}

		public static function site_exists($site): bool
		{
			return file_exists(self::get_domain_path($site));
		}

		public static function get_domain_path($site): string
		{
			return FILESYSTEM_VIRTBASE . "/{$site}/fst";
		}

		public static function get_group_from_site(string $site): ?string
		{
			if (0 !== strncmp($site, 'site', 4)) {
				return null;
			}

			return 'admin' . substr($site, 4);
		}

		/**
		 * Get site ID from posix group
		 *
		 * @param string $group
		 * @return int|null
		 */
		public static function get_site_id_from_group(string $group): ?int
		{
			if (0 !== strncmp($group, 'admin', 5)) {
				return null;
			}
			return (int)substr($group, 5);
		}

		public static function domain_exists($domain): bool
		{
			return self::get_site_id_from_domain($domain) > 0;
		}

		public static function get_site_id_from_domain($domain): ?int
		{
			if (!$domain) {
				return null;
			}
			// @XXX switch to TC once backend is rewritten
			$db = \Opcenter\Map::load(\Opcenter\Map::DOMAIN_TXT_MAP);
			$site = $db->fetch($domain);
			$db->close();
			if (!$site) {
				return null;
			}

			return (int)substr($site, 4);
		}

		/**
		 * Get site(s) attached to invoice
		 *
		 * @param string $invoice
		 * @return null|array
		 */
		public static function get_site_id_from_invoice(string $invoice): ?array
		{
			if (!$invoice) {
				return null;
			}

			$map = \Opcenter\Map::home() . '/' . \Opcenter\Service\Validators\Billing\Invoice::MAP_FILE;
			if (!file_exists($map)) {
				return null;
			}
			$db = \Opcenter\Map::load($map);
			$sites = $db->fetch($invoice);
			$db->close();
			if (!$sites) {
				return null;
			}

			return array_map(static function ($val) {
				return (int)substr($val, 4);
			}, preg_split('/\s*,\s*/', $sites, -1, PREG_SPLIT_NO_EMPTY));
		}

		/**
		 * Application post-processing hook
		 *
		 * Called from within /index.php
		 *
		 * @TODO better callback system
		 */

		public static function init_hook()
		{
			$driver = self::get_driver();
			if (!method_exists($driver, 'init') ||
				!$driver->session_valid()
			) {
				return;
			}

			return $driver->init();
		}

		/**
		 * Get auth handler instance
		 *
		 * @return Auth
		 */
		public static function get_driver(): Auth
		{
			if (!isset(self::$authDriver)) {
				self::$authDriver = self::autoload() or fatal('no suitable auth driver found');
			}

			return self::$authDriver;
		}

		public static function end_hook()
		{
			$driver = self::get_driver();
			if (!method_exists($driver, 'close') ||
				!$driver->session_valid()
			) {
				return;
			}

			return $driver->close();
		}

		public static function login_time()
		{
			if (!self::get_driver()->session_valid()) {
				return null;
			}

			return $_SESSION['time'];
		}

		/**
		 * Validate session is a logged in, authenticated user
		 *
		 * @return mixed
		 */
		public static function authenticated()
		{
			return self::get_driver()->session_valid();
		}

		public static function profile(): \Auth_Info_User
		{
			$profile = self::autoload();

			return $profile->getProfile();
		}

		public function getProfile()
		{
			if (null === $this->_authProfile) {
				// force hard reset
				$this->_authProfile = $this->authInfo(true);
				$this->_authProfile->getAccount();
			}

			return $this->_authProfile;
		}

		public function authInfo($force = false): \Auth_Info_User
		{
			if ($force) {
				$this->_authProfile = null;
				$this->_authID = $this->getID();
			}
			if (($force || null === $this->activeUser) &&
				!$this->__importUser($force)
			) {
				fatal('Cannot import user profile');
			}

			return $this->activeUser;
		}

		// inherited class methods

		public function getID(): string
		{
			return $this->_authID ?? session_id();
		}

		private function __importUser($force = false)
		{
			$found = 0;
			$session = $this->getID();
			$user = null;
			if (!$force) {
				$user = Cache_Global::spawn()->get('user:' . $session);
			}
			if ($user) {
				$found = 1;
			} else {
				$user = Auth_Info_User::factory($session);
				if ($user) {
					$found = -1;
					Cache_Global::spawn()->set('user:' . $session, $user, 7200);
				}
			}
			$this->activeUser = $user;

			return $found;
		}

		/**
		 * Create a session for a specified user
		 *
		 * @param null|string $user
		 * @param null|string $domain
		 * @return \Auth_Info_User
		 */
		public static function context(?string $user, ?string $domain = null): \Auth_Info_User
		{
			if ($domain) {
				$siteid = self::get_site_id_from_anything($domain);
				if (null === $siteid) {
					fatal("unknown domain `%s' to derive context", $domain);
				}
				if (!$user) {
					$user = self::get_admin_from_site_id($siteid);
				}
				$domain = self::get_domain_from_site_id($siteid);
				if (!$domain) {
					fatal("domain missing from domainmap but site id `%s' found", $siteid);
				}
			} else if (!$user) {
				$user = self::get_admin_login();
			}

			$id = \apnscpSession::init()->create_sid();
			$context = \Auth_Info_User::initializeUser($user, $domain, null, $id);
			if (!$context) {
				fatal("failed to generate user session `%s'@`%s'", $user, $domain);
			}

			return $context;
		}

		/**
		 * Create context gracefully
		 *
		 * @param string|null $user
		 * @param string|null $domain
		 * @return Auth_Info_User|null
		 */
		public static function nullableContext(?string $user, ?string $domain = null): ?\Auth_Info_User
		{
			$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
			try {
				return self::context($user, $domain);
			} catch (\apnscpException $e) {
				return null;
			} finally {
				\Error_Reporter::exception_upgrade($oldex);
			}
		}

		/**
		 * Get site id from domain, site, admin, or site id
		 *
		 * @param int|string $thing
		 * @return int|null
		 */
		public static function get_site_id_from_anything($thing): ?int
		{
			if (is_int($thing)) {
				return $thing;
			}

			if (0 === strncmp($thing, 'site', 4) && ctype_digit(substr($thing, 4))) {
				return (int)substr($thing, 4);
			}
			if (preg_match(Regex::DOMAIN, $thing)) {
				return self::get_site_id_from_domain($thing);
			}

			return self::get_site_id_from_admin($thing);
		}

		public static function get_site_id_from_admin($admin): ?int
		{
			$pg = PostgreSQL::initialize();
			$q = $pg->query('SELECT site_id FROM siteinfo WHERE admin_user = ' . pg_escape_literal($admin));
			if (!$q || $q->num_rows() < 1) {
				return null;
			}

			return (int)$q->fetch_object()->site_id;
		}

		public static function get_admin_from_site_id($site_id)
		{
			$pg = PostgreSQL::initialize();
			$q = $pg->query('SELECT admin_user FROM siteinfo WHERE site_id = ' . (int)$site_id);
			if (!$q || $q->num_rows() < 1) {
				return null;
			}

			return $q->fetch_object()->admin_user;
		}

		public static function get_domain_from_site_id($site_id)
		{
			if (0 === strncmp($site_id, 'site', 4)) {
				$site_id = substr($site_id, 4);
			}
			// otherwise 616ci.com is interpreted as site616...
			if (!ctype_digit((string)$site_id)) {
				return false;
			}
			$pg = PostgreSQL::initialize();
			$q = $pg->query('SELECT domain FROM siteinfo WHERE site_id = ' . (int)$site_id);
			if (!$q || $q->num_rows() < 1) {
				return null;
			}

			return $q->fetch_object()->domain;
		}

		public static function get_admin_login(): ?string
		{
			$file = \apnscpFunctionInterceptor::get_autoload_class_from_module('auth')::ADMIN_AUTH;

			return strtok(file_get_contents($file), ':') ?: null;
		}

		public static function client_ip()
		{
			static $ip;
			if (null !== ($ip = array_get($_ENV, 'APNSCP_CLIENT_IP', $ip))) {
				return $ip;
			}

			if (isset($_SERVER['REMOTE_ADDR'])) {
				$trusted = constant('HTTP_TRUSTED_FORWARD');
				if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $trusted && self::whitelisted($_SERVER['REMOTE_ADDR'])) {
					return strtok($_SERVER['HTTP_X_FORWARDED_FOR'], ',');
				}

				return $_SERVER['REMOTE_ADDR'];
			}
			if (isset($_SERVER['SSH_CLIENT'])) {
				$ip = explode(' ', $_SERVER['SSH_CLIENT']);
				$ip = $ip[0];
			}

			// assume connecting from tty1
			return $ip ?? '127.0.0.1';
		}

		/**
		 * Given IP address is whitelisted
		 *
		 * @param string $ip
		 * @return bool
		 */
		private static function whitelisted(string $ip): bool
		{

			if (!HTTP_TRUSTED_FORWARD) {
				return false;
			}

			foreach (HTTP_TRUSTED_FORWARD as $fwd) {
				if (IpCommon::in($ip, $fwd)) {
					return true;
				}
			}

			return false;
		}

		/**
		 * Separate login string into username, domain components
		 *
		 * @param  string $login
		 * @return array  username, domain component
		 */
		protected static function _parse_login($login): array
		{
			$duo = array($login, null);
			$sep = strpos($login, '@');
			if ($sep === false) {
				$sep = strpos($login, '#');
			}
			if ($sep === false) {
				return $duo;
			}

			$duo = array(
				substr($login, 0, $sep),
				substr($login, $sep + 1)
			);

			return $duo;
		}

		protected static function get_db(): \MySQL
		{
			if (!isset(self::$db)) {
				self::$db = MySQL::initialize();
			}

			return self::$db;

		}

		public function get_db_symbol()
		{
			return static::DB_SYMBOL;
		}

		/**
		 * Create a hash of session
		 *
		 * @return string
		 */
		public function hash(): string
		{
			return hash_hmac('sha256', $this->getID(), $this->getProfile()->username);
		}

		/**
		 * Restore impersonated ID
		 * @param string $id
		 * @return Auth|null
		 */
		public function resetID(string $id): ?self
		{
			return $this->setID($id);
		}

		public function setID(string $id): ?self
		{
			if (!\apnscpSession::init()->exists($id)) {
				if (is_debug()) {
					\Error_Reporter::print_debug_bt();
				}
				$this->login_error();

				return null;
			}

			if ($id === $this->_authID) {
				return $this;
			}

			if ($this === self::$authDriver && !IS_CLI) {
				// \Auth::profile() is called, which returns self::$authDriver.
				// setID() changes active ID in driver. Update session_id() to match
				// to avoid desync
				//
				// @TODO This would also be invoked on worker id change from the backend,
				// but backend has no session support.
				//\apnscpSession::restore_from_id($id, false);
			}

			$this->_authID = $id;
			$this->_authProfile = null;

			return $this;
		}

		protected function login_error()
		{
			return self::get_driver()->unauthorized();
		}

		public function login_success() {
			return true;
		}

		protected function logAction(string $msg, array $args = [], int $priority = LOG_INFO): void
		{
			openlog("cp/" . strtolower($this->getDriver()), LOG_PID | LOG_NDELAY, LOG_AUTHPRIV);
			syslog($priority, \ArgumentFormatter::format($msg, [
				'ip'       => \Auth::client_ip(),
			] + $args));
			closelog();
		}

		protected function initializeUser(
			string $username,
			?string $domain,
			?string $password,
			?int $timeout = self::TV_SEC
		): ?bool {
			if (!preg_match(Regex::USERNAME, $username)) {
				return error('missing username');
			}

			if (null === $timeout) {
				$timeout = self::TV_SEC;
			}

			$user = \Auth_Info_User::initializeUser($username, $domain, $password, $this->getID());

			if (!$user || !static::get_driver()->verify($username, $password, $domain)) {
				if ($user) {
					$this->authDelete($user);
				}

				(new Auth\Log($this))->failure(compact('username', 'domain'));
				\apnscpSession::init()->destroy($this->getID());

				return $this->login_error();
			}
			if ($timeout <= 0) {
				\MySQL::initialize()->query('UPDATE session_information ' .
					"SET auto_logout = 0 WHERE session_id = '" . $user->id . "'");
			}

			$this->sessionFromContext($user, $domain);

			if (!$user->level) {
				return error('failed to fetch level');
			}

			$_SESSION['valid'] = true;
			$_SESSION['auth_timeout'] = $timeout ? (int)$timeout : -1;
			$_SESSION['last_action'] = $_SERVER['REQUEST_TIME'];
			$this->authInfo(true);
			// load account metadata from <site>/info/current/
			if ($user->level & (PRIVILEGE_SITE | PRIVILEGE_USER)) {
				$prefs = \Datastream::get($user)->query('common_load_preferences');
				$_SESSION[\Preferences::SESSION_KEY] = $prefs;
				$this->activeUser->getAccount();
				// calling \Preferences::get('webhooks') will cause deadlock
				if (null !== ($hooks = array_get($prefs, \Webhook_Module::PREFERENCES_KEY))) {
					$_SESSION[\Webhook_Module::PREFERENCES_KEY] = \DataStream::get($user)->query('webhook_init');
				}
			}

			// initialize cache
			(new Auth\Log($this))->success(compact('username', 'domain'));
			$this->logAction("Login succeeded %(ip)s - %(username)s/%(domain)s", [
				'username' => $username,
				'domain'   => $domain ?: 'NULL'
			], LOG_INFO);
			return true;
		}

		protected function sessionFromContext(\Auth_Info_User $ctx, string $entryDomain = null): void
		{
			$_SESSION = [
				'lang'         => 'en_US',
				'domain'       => $ctx->domain,
				'username'     => $ctx->username,
				'user_id'      => $ctx->user_id,
				'group_id'     => $ctx->group_id,
				// domain Auth_info_User->domain changes upon login to
				// match primary domain
				'entry_domain' => $entryDomain ?? $ctx->domain,
				'level'        => $ctx->level,
				'time'         => $_SERVER['REQUEST_TIME'],
				'site_id'      => $ctx->site_id
			];
		}

		public function verify($username, $password, $domain)
		{
			return DataStream::get($this->getProfile())->
				setOption(apnscpObject::RESET)->query('auth_verify_password', $password) &&
				IpRestrictorAlias::instantiateContexted($this->getProfile())->authorized(self::client_ip(), $this->getDriver());
		}

		public function authDelete(\Auth_Info_User $context = null): void
		{
			Cache_Global::spawn()->del('user:' . $context->id ?? $this->getID());
		}
	}