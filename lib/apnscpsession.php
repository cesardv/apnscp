<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class apnscpSession implements SessionHandlerInterface, SessionIdInterface
	{
		const SESSION_ID = 'esprit_id';
		private static $db;
		private static $handler;

		protected $_id;

		public static function restore_from_id(string $session_id, bool $destroy = true): bool
		{
			// current session is target fixation
			if ($session_id === \session_id()) {
				return true;
			}
			if (!static::valid_id($session_id)) {
				return false;
			}
			$db = self::get_db();
			$q = $db->query('SELECT 1 FROM `session_information` ' .
				" WHERE session_id = '" . $db->escape_string($session_id) . "'");

			if ($q->num_rows < 1) {
				return false;
			}

			if (session_status() === PHP_SESSION_ACTIVE) {
				if (($module = Session::get('cache.modules')) instanceof ModuleCache) {
					// bug in PHP 7.4. Deep serialize not called on cache.modules
					$module->cleanDynamicCompositions();
				}

				$destroy ? session_destroy() : session_write_close();
			}

			session_id($session_id);
			if (!session_start()) {
				report('Failed to start session?');
			}
			if (session_status() !== PHP_SESSION_DISABLED) {
				session_decode(static::init()->read($session_id));
			}

			return true;
		}

		private static function get_db()
		{
			if (null === self::$db) {
				self::$db = MySQL::initialize();
			}

			return self::$db;
		}

		public function read($session_id)
		{
			$db = self::get_db();
			if (!self::valid_id($session_id)) {
				fatal($session_id . ': invalid id');
			}
			$qfrag = "WHERE session_id = '" . $session_id . "'";
			$q = $db->query('SELECT login_method, session_data, last_action FROM
                `session_information` ' . $qfrag);
			if (!$q) {
				fatal($session_id . ': unable to open session id (%s)', $db->error);
			}
			$this->_id = $session_id;
			if ($q->num_rows < 1) {
				return (string)$this->create($session_id);
			}
			return (string)$q->fetch_object()->session_data;
		}

		/* Close session */

		public static function valid_id($id)
		{
			return ctype_alnum($id);
		}

		/* Read session data from database */

		public function create($session_id, string $data = '')
		{
			$db = self::get_db();
			if (!self::valid_id($session_id)) {
				fatal($session_id . ': invalid id');
			}
			$safe_data = $db->escape_string($data);
			$ret = $db->query('INSERT INTO `session_information` ' .
				"(session_data, session_id) VALUES('" . $safe_data . "', '" . $session_id . "') " .
				'ON DUPLICATE KEY UPDATE login_method = NULL;');
			if (!$ret) {
				fatal("Failed to create session ID `%s'", $session_id);
			}
			return $data;
		}

		public function sync(): bool
		{
			return $this->write($this->get_id(), session_encode());
		}

		public static function init()
		{
			if (null !== self::$handler) {
				return self::$handler;
			}

			$handler = new self();
			if (session_status() === PHP_SESSION_NONE) {
				session_set_cookie_params([
					'secure'   => \Util_HTTP::isSecure(),
					'httponly' => IS_ISAPI
				]);
			}

			/**
			 * @XXX close() must be called last
			 *
			 * When register_shutdown is true and a destructor updates a _SESSION
			 * var, session data is committed before __destruct() fires resuflting
			 * in an inconsistent state. Alternative is to move __destruct() _SESSION
			 * manipulation to register_shutdown_function or
			 * pass false to register_shutdown
			 */
			session_set_save_handler($handler, false);
			self::$handler = $handler;
			if (isset($_GET[self::SESSION_ID]) && static::valid_id($_GET[self::SESSION_ID])) {
				$session_id = $_GET[self::SESSION_ID];
				unset($_GET[self::SESSION_ID]);
				if ($session_id !== \session_id()) {
					\apnscpSession::restore_from_id($session_id);
				}
			}

			if (session_status() === PHP_SESSION_NONE && !(defined('SHIM') && SHIM)) {
				try {
					session_start();
				} catch (UnexpectedValueException $e) {
					if (!is_debug()) {
						report('Session encoding error');
						throw $e;
					}
					/**
					 * On 7.2, a class with a circular reference that implements Serializable can cause
					 * an unserialization error in PHP (ArrayObject in afi, $modules <=> $session['cache']['modules']
					 * unset($modules) fails to decode session data; $modules = null is OK
					 */
					dd($e);
				}
			}

			return $handler;
		}

		/**
		 * Delete all sessions
		 *
		 * @param int         $site_id
		 * @param string|bool $keepid optional session id to preserve
		 * @return bool
		 */
		public static function invalidate_by_site_id(int $site_id, $keepid = null): bool
		{
			if (is_bool($keepid) && $keepid) {
				$keepid = session_id();
			} else if ($keepid && !self::valid_id($keepid)) {
				fatal("invalid session id to preserve, `%s'", $keepid);
			}
			$db = self::get_db();

			$query = 'DELETE FROM `session_information` WHERE `site_number` = ' .
				$site_id;
			if ($keepid) {
				$query .= " AND session_id != '" . $keepid . "'";
			}
			$res = $db->query($query);

			if (!$res) {
				error_log('unable to delete session from database: ' . $db->error);

				return false;
			}

			return true;
		}

		/* Write new data to database */

		/**
		 * @param int|null         $site_id site ID or null fo r
		 * @param string           $user
		 * @param bool|null|string $keepid  keep active ID or preserve an ID
		 * @return bool
		 */
		public static function invalidate_by_user(?int $site_id, string $user, $keepid = null): bool
		{
			if (!preg_match(Regex::USERNAME, $user)) {
				fatal("failed to invalidate session - invalid user `%s'", $user);
			}
			if ($keepid) {
				if (is_bool($keepid)) {
					$keepid = session_id();
				} else if (!self::valid_id($keepid)) {
					fatal("invalid session id to preserve, `%s'", $keepid);
				}
			}

			$db = self::get_db();

			$query = 'DELETE FROM `session_information` WHERE `site_number` ' .
				($site_id ? '= ' . (int)$site_id : 'IS NULL');
			if ($user) {
				$query .= " AND username = '" . $db->escape_string($user) . "'";
			}
			if ($keepid) {
				$query .= " AND session_id != '" . $keepid . "'";
			}
			$res = $db->query($query);

			if (!$res) {
				error_log('unable to delete session from database: ' . $db->error);

				return false;
			}

			return true;
		}

		/* Destroy session record in database */

		/**
		 * Disable implicit use of Cookie: header
		 *
		 * @return void
		 */
		public static function disable_session_header(): void
		{
			if (session_status() === PHP_SESSION_NONE) {
				ini_set('session.use_only_cookies', '0');
				ini_set('session.use_cookies', '0');
				ini_set('session.cache_limiter', '');
			}
		}

		/* Garbage collection, deletes old sessions */

		public function get_id()
		{
			return $this->_id;
		}

		public function open($path, $name): bool
		{
			self::get_db() or fatal('cannot access session db');

			return true;
		}

		public function close(): bool
		{
			self::$db = null;
			return true;
		}

		public function write($session_id, $data): bool
		{
			// ignore clobbering session data from backend if no data present
			if (IS_CLI || $data === '') {
				//\Error_Reporter::print_debug_bt();
				return true;
			}
			$db = self::get_db();
			$safe_data = $db->escape_string($data);
			$safe_id = $db->escape_string($session_id);
			$db->ping();
			$q = $db->query('UPDATE `session_information` SET ' .
				"session_data = '" . $safe_data . "' WHERE session_id = " .
				"'" . $safe_id . "'");
			if (!$q) {
				error_log("Unable to write session data:\n" . $db->error);

				return false;
			}

			return true;
		}

		public function destroy($session): bool
		{
			$db = self::get_db();
			//$db->ping();
			$res = $db->query("DELETE FROM `session_information` WHERE session_id = '" .
				$db->escape_string($session) . "';");
			if (!$res) {
				error_log('unable to delete session from database: ' . $db->error);

				return false;
			}
			// @TODO callback/decouple
			\apnscpFunctionInterceptor::expire($session);
			$_SESSION = [];

			return true;
		}

		public function gc($life): bool
		{
			$tv_expiry = $_SERVER['REQUEST_TIME'] ?? time() - \Auth::TV_SEC;
			$db = self::get_db();
			$db->query('LOCK TABLES session_information WRITE');
			$db->query('DELETE FROM `session_information` ' .
				'WHERE auto_logout != 0 AND ' .
				'`last_action` <  DATE_SUB(NOW(), INTERVAL ' . $tv_expiry . ' SECOND)');
			if (random_int(0, 1)) {
				$db->query('DELETE FROM `session_information` WHERE ' .
					'last_action < DATE_SUB(NOW(), INTERVAL 1 DAY);');
			}
			$db->query('UNLOCK TABLES');

			return true;
		}

		public function create_sid(): string
		{
			$valid_chars = '0123456789' .
				'abcdefghijklmnopqrstuvwxyz' .
				'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$session_id = '';
			$maxlen = 32;
			if (PHP_VERSION_ID >= 70100) {
				$maxlen = (int)ini_get('session.sid_length');
			}
			for ($i = 0, $char_size = strlen($valid_chars) - 1; $i < $maxlen; $i++) {
				$session_id .= $valid_chars[random_int(0, $char_size)];
			}
			if ($this->exists($session_id)) {
				return $this->create_sid();
			}

			return $session_id;
		}

		public function exists($session_id): bool
		{
			$db = self::get_db();
			if (!self::valid_id($session_id)) {
				fatal("invalid session id `%s'", $session_id);
			}
			$rs = $db->query("SELECT 1 FROM session_information WHERE session_id = '" . $session_id . "'");

			return is_object($rs) && $rs->num_rows >= 1;
		}
	}
