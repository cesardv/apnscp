<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/*
	*  apnscp Templating Engine
	*  Synopsis: handles all the ugly work to create objects for use
	*            with apnscp's skinnable functionality
	*/

	/* Category/Link Class */

	class Template_Object
	{
		// name
		public $name = '&nbsp;';
		// image
		public $image = 'images/spacer.gif';
		// conditions to validate app
		public $assertions;

		// show link/category or store in hierarchy
		public $display = true;

		public $internal = null;

		public function __construct($id = null)
		{
			$this->internal = $id;
		}

		public function getInternal(): ?string
		{
			return $this->internal;
		}

		public function getName(): string
		{
			return $this->name;
		}

		public function setName($name)
		{
			$this->name = _($name);

			return $this;
		}

		public function setIcon($icon)
		{
			$this->image = $icon;

			return $this;
		}

		public function getIcon(): ?string
		{
			return $this->image;
		}

		public function show()
		{
			$this->display = true;

			return $this;
		}

		/**
		 * Hide application from panel
		 *
		 * @return Template_Object
		 */
		public function hide(): self
		{
			$this->display = false;

			return $this;
		}

		/**
		 * Remove application access from panel
		 *
		 * @return Template_Object
		 */
		public function remove(): self {
			foreach(get_object_vars($this) as $var => $val) {
				$this->{$var} = null;
			}

			return $this;
		}

		/**
		 * Application exists
		 *
		 * @return bool
		 */
		public function exists(): bool
		{
			return $this->internal !== null;
		}

		/**
		 * Application is displayed
		 *
		 * @return bool
		 */
		public function getVisibility(): bool
		{
			return $this->display;
		}
	}

	class Template_Object_Module extends Template_Object
	{
		public $value;

		public function getValue()
		{
			return $this->value;
		}

		public function setValue($value)
		{
			$this->value = $value;

			return $this;
		}
	}

	class Template_Object_Category extends Template_Object
	{
	}

	class Template_Object_Link extends Template_Object
	{
		public $link;
		public $target;
		public $active = false;
		public $category;

		public function getLink()
		{
			return is_array($this->link) ? $this->link[0] : $this->link;
		}

		public function setLink($href)
		{
			$this->link = $href;

			return $this;
		}

		public function getLinkAliases()
		{
			return $this->link;
		}

		public function getTarget()
		{
			return $this->target;
		}

		public function setTarget($target)
		{
			$this->target = $target;

			return $this;
		}

		public function isActive(): bool
		{
			return (bool)$this->active;
		}

		public function setActive($active = true)
		{
			$this->active = $active;

			return $this;
		}

		public function getCategory()
		{
			return $this->category;
		}

		public function setCategory($name): void
		{
			$this->category = $name;
		}
	}

	class Template_Object_Info extends Template_Object
	{
		public $id = '';
		public $span = 1;
		public $value;

		public function getSpan(): int
		{
			return $this->span;
		}

		public function setSpan($span)
		{
			$this->span = $span;

			return $this;
		}

		public function getId(): ?string
		{
			return $this->id;
		}

		public function setId($id)
		{
			$this->id = $id;

			return $this;
		}

		public function getValue()
		{
			return $this->value;
		}

		public function setValue($value)
		{
			$this->value = $value;

			return $this;
		}
	}

	class Template_Engine
	{
		use apnscpFunctionInterceptorTrait;

		const CACHE_KEY = 'cache.engine';
		const LINK_TOP = 0x0001;
		const LINK_NEW = 0x0002;
		const LINK_NORMAL = 0x0003;
		const LINK_DEFAULT = 0x0004;
		const BASE_DIR = '/apps';
		private static $instance;        // internal pointer to the offset of the array
		// containing the category, USED INTERNALLY
		private $cPointer;          // array containing all the categories
		private $cArray;          // String containing information about the
		// links for categories, all ' occurrences
		// should be escaped
		private $lArray;            // internal pointer to the offset of the array
		// containing the link, USED INTERNALLY
		private $lPointer;              // array containing all the module names
		private $mArray;            // array containing all the categories
		private $mPointer;              // site info array
		private $iArray;            // internal pointer denoting offset
		// for site infos
		private $iPointer;       // cache of internal links for quicker
		// lookups when checking of category existence
		private $internalCache;         // map ID to location
		private $lookupArray; // internal counter for cells per row
		private $categoryCellCounter; // maximum cells per row
		private $categoryCellMaximum;
		private $helpCache;
		private $infoCellCounter;
		private $infoCellMaximum;
		private $linkCellCounter;
		private $linkCellMaximum;
		private $moduleCellCounter;
		private $moduleCellMaximum;
		private $categoryPages;
		private $activePage;

		/* default constructor */

		public static function call($method, array $args = array())
		{
			return call_user_func_array(array(apnscpFunctionInterceptor::init(), $method), $args);
		}

		public static function init(): \Template_Engine
		{
			if (self::$instance === null) {
				self::$instance = new self;
			}

			return self::$instance;
		}

		/**
		 * Get URI for app
		 *
		 * @param string $app
		 * @return string
		 */
		public function getPathFromApp(string $app): string {
			return static::BASE_DIR . "/${app}";
		}

		public function purgeCache()
		{
			\Session::forget(self::CACHE_KEY);
			$this->__construct();
		}

		private function __construct()
		{
			$this->clear();
			$this->load_configuration();
		}

		/**
		 * Clear menu configuration
		 *
		 * @return $this
		 */
		public function clear(): self
		{
			$this->categoryPages = array();
			$this->cArray = [
				(new Template_Object_Category(''))->setName('') // top-level
			];
			$this->lArray = array();
			$this->mArray = array();
			$this->iArray = array();
			$this->categoryCellCounter = 0;
			$this->categoryCellMaximum = 1;
			$this->infoCellCounter = 0;
			$this->infoCellMaximum = 1;
			$this->linkCellCounter = 0;
			$this->linkCellMaximum = 1;
			$this->moduleCellCounter = 0;
			$this->moduleCellMaximum = 1;

			return $this;
		}

		private function load_configuration(): ?array
		{
			if (!Auth::authenticated()) {
				return array();
			}
			if (\Session::exists(self::CACHE_KEY) && !is_debug()) {
				return $this->load_cache();
			}
			switch ($_SESSION['level']) {
				case PRIVILEGE_SITE:
					$role = 'site';
					break;
				case PRIVILEGE_ADMIN:
					$role = 'admin';
					break;
				case PRIVILEGE_USER:
					$role = 'user';
					break;
			}
			$plan = $_SESSION['level'] & (PRIVILEGE_USER|PRIVILEGE_SITE) ? $this->billing_get_package_type() : null;
			$function = $this->getApnscpFunctionInterceptor();
			$templateClass = $this;
			include __DIR__ . '/templateconfig-' . $role . '.php';
			foreach(["${role}-${plan}", $role] as $override) {
				// @TODO allow resellers to template custom menus
				$extra = conf_path('custom/templates/' . $override . '.php');
				if (file_exists($extra)) {
					include $extra;
					break;
				}
			}
			$this->save_cache();

			return [];
		}

		private function load_cache()
		{
			$cache = \Util_PHP::unserialize(\Session::get(self::CACHE_KEY), true);
			$this->mArray = $cache['modules'];
			$this->lArray = $cache['links'];
			$this->iArray = $cache['info'];
			$this->cArray = $cache['categories'];
			$this->lookupArray = $cache['lookup'];
			$this->helpCache = $cache['tooltips'];

			return $cache;

		}

		private function save_cache(): void
		{
			$cache = array(
				'modules'    => $this->mArray,
				'links'      => $this->lArray,
				'info'       => $this->iArray,
				'categories' => $this->cArray,
				'lookup'     => $this->lookupArray,
				'tooltips'   => $this->helpCache
			);
			\Session::set(self::CACHE_KEY, serialize($cache));
		}

		public function getCache()
		{
			return $this->load_cache();
		}

		public function getApplicationFromId($id): Template_Object_Link
		{
			$uri = self::BASE_DIR . '/' . $id;
			if (!array_key_exists($uri, (array)$this->lookupArray)) {
				return new Template_Object_Link();
			}
			$key = $this->lookupArray[$uri];

			return array_get($this->lArray, $key, new Template_Object_Link());
		}

		public function create_category
		(
			$mName,
			$mAssertion = true,
			$mIcon = '',
			$mReference = null
		): \Template_Object_Category {
			$offset = count($this->cArray);
			$obj = new Template_Object_Category($mReference ?? $mName);
			foreach ((array)$mAssertion as $assertion) {
				if (!$assertion) {
					return $obj;
				}
			}

			/*if (!isset($this->linkCellCounter[$mInternal]))
				$this->linkCellCounter[$mInternal] = 0;*/

			/* it's all true, plop it in */
			$id = $obj->getInternal();
			$this->internalCache[$id] = true;
			$obj->setName($mName)->setIcon($mIcon);
			$this->cArray[$id] = $obj;
			$this->lArray[$id] = array();

			return $obj;
		}

		/*
		*  Adds a glob to the site info table
		*  Value may be a function call and that is what is displayed
		*  verbatim (return value)
		*/
		public function create_info
		(
			$mName,
			$mValue,
			$mAssertion,
			$mID = null,
			$mSpan = 1,
			$mHelpValue = null,
			$mHelpName = null
		): \Template_Object_Info {
			$offset = count($this->iArray);
			/** add a new help object */
			$obj = new Template_Object_Info();
			$obj->setName($mName)->setValue($mValue)->
			setId($mID)->setSpan($mSpan);
			if ($mAssertion) {
				$obj->show();
			} else {
				$obj->hide();
			}
			$this->iArray[$offset] = $obj;
			// @TODO remove
			if ($mHelpValue !== null) {
				$rand_token = str_replace(' ', '_', strtolower($mName));
				Tip_Engine::add($rand_token, array('title' => $mHelpName, 'body' => $mHelpValue), 'dashboard', true);
				$this->helpCache[$mName] =
					array(
						'value' => $mHelpValue,
						'title' => $mHelpName,
						'token' => $rand_token
					);
			}

			return $obj;
		}

		/*
		*  special case exists for $mTarget where equal to literal
		*  string 'new'.  Will call new js window on click
		 *
		 * @return array|bool
		*/

		public function get_help($mName = null)
		{
			if ($mName === null) {
				return $this->helpCache;
			}

			if ($this->help_exists($mName)) {
				return $this->helpCache[$mName];
			} else {
				return false;
			}
		}

		public function help_exists($mName): bool
		{
			return isset($this->helpCache[$mName]);
		}

		public function create_link
		(
			$mName,
			$mHref,
			$mAssertion,
			$mIcon,
			$mCategory,
			$mTarget = LINK_NORMAL
		): ?\Template_Object_Link {
			// category referenced does not exist

			if ($mCategory && !$this->internal_exists($mCategory)) {
				return null;
			}

			foreach ((array)$mAssertion as $assertion) {
				if (!$assertion) {
					return new Template_Object_Link;
				}
			}
			/* it's all true, plop it in */
			$internal = '';
			$basedir_len = strlen(self::BASE_DIR);

			foreach ((array)$mHref as $href) {
				if (strpos($href, self::BASE_DIR) === 0) {
					$internal = substr($href, $basedir_len + 1);
					break;
				}
			}
			if (false !== ($pos = strpos($internal, '?'))) {
				$internal = substr($internal, 0, $pos);
			} else if (!$internal) {
				$internal = str_slug($mName);
			}
			$offset = $internal;
			$target = $this->get_target_type($mTarget);
			$obj = new Template_Object_Link($internal);
			$obj->setName($mName)
				->setLink($mHref)->setIcon($mIcon)
				->setTarget($target)->setCategory($mCategory);

			$this->lArray[$mCategory][$offset] = $obj;
			foreach ((array)$mHref as $href) {
				$this->lookupArray[strtok($href, '?')] = $mCategory . '.' . $internal;
			}

			return $obj;
		}

		/*
		*  Create a new module type
		*  Examples of such are the random tip and site info modules
		*  The site links module is independent of this and should
		*  not be added to this
		*/

		protected function internal_exists($mName): ?bool
		{
			/* check inside the cache to speed it up a notch */
			if (isset($this->internalCache[$mName])) {
				return true;
			}

			return false;
		}

		private function get_target_type($mTarget): ?string
		{
			switch ($mTarget) {
				case LINK_NORMAL:
					return '_self';
				case LINK_TOP:
					return 'top';
				case LINK_NEW:
					return '_blank';
				default:
					return '_self';
			}
		}

		public function create_module
		(
			$mName,
			$mValue,
			$mAssertion,
			$mIcon
		): \Template_Object_Module {
			$offset = count($this->mArray);
			$obj = new Template_Object_Module;
			foreach ((array)$mAssertion as $assertion) {
				if (!$assertion) {
					return $obj;
				}
			}
			/* it's all true, plop it in */

			$obj->setName($mName)->setImage($mIcon)
				->setValue($mValue);
			$this->mArray[$offset] = $obj;

			return $obj;
		}

		public function get_category()
		{
			if (count($this->cArray) == 0) {
				/* no categories */
				return false;
			}
			$this->categoryCellCounter++;
			if ($this->cPointer === null) {
				reset($this->cArray);
				$this->cPointer = $this->cArray[key($this->cArray)];

				return $this->cPointer;
			}

			$this->cPointer = next($this->cArray);
			if ($this->cPointer !== false) {
				if ($this->categoryCellCounter == $this->categoryCellMaximum) {
					$this->categoryCellCounter = 0;
				}

				return $this->cPointer;
			}

			if ($this->categoryCellCounter == $this->categoryCellMaximum) {
				return new Template_Object_Category;
			} else {
				return false;
			}
		}

		public function get_info()
		{
			if (count($this->iArray) == 0) {
				/* no site info */
				return false;
			}
			$this->infoCellCounter++;
			if ($this->iPointer === null) {
				reset($this->iArray);
				$this->iPointer = $this->iArray[key($this->iArray)];

				return $this->iPointer;
			}

			$this->iPointer = next($this->iArray);
			if ($this->iPointer !== false) {
				if ($this->infoCellCounter == $this->infoCellMaximum) {
					$this->infoCellCounter = 0;
				}

				return $this->iPointer;
			}

			if ($this->infoCellCounter == $this->infoCellMaximum) {
				return new Template_Object_Info;
			} else {
				return false;
			}
		}

		/* returns the target="%s" component of a link */

		public function get_link($mCategory)
		{
			if (!isset($this->lArray[$mCategory]) || count($this->lArray[$mCategory]) == 0) {
				/* no links */
				return false;
			}
			// new links
			if (!isset($this->lPointer[$mCategory])) {
				reset($this->lArray);
			} else {
				$this->lPointer[$mCategory] = next($this->lArray[(string)$mCategory]);
				if (false === $this->lPointer[$mCategory]) {
					$this->lPointer[$mCategory] = false;

					return false;
				}
			}
			//$this->lPointer = $this->lArray[strval($mCategory)][intval(key($this->lArray))];
			$this->lPointer[$mCategory] = current($this->lArray[(string)$mCategory]);
			$link = $this->lPointer[$mCategory];
			$href = $link->getLink();
			if (is_array($href)) {
				$href = array_shift($href);
			}

			return array(
				'name'     => $link->getName(),
				'href'     => $href,
				'icon'     => $link->getIcon(),
				'internal' => $link->getInternal(),
				'target'   => $link->getTarget()
			);
		}

		public function get_module()
		{
			if (count($this->mArray) == 0) {
				/* no categories */
				return false;
			}
			if ($this->mPointer === null) {
				reset($this->mArray);
				$this->mPointer = $this->mArray[key($this->mArray)];

				return $this->mPointer;
			}

			$this->mPointer = next($this->mArray);
			if ($this->mPointer !== false) {
				if ($this->moduleCellCounter == $this->moduleCellMaximum) {
					$this->moduleCellCounter = 0;
				}

				return $this->mPointer;
			}

			if ($this->moduleCellCounter == $this->moduleCellMaximum) {
				return new Template_Object_Module;
			} else {
				return false;
			}
		}

		function resolve_target($mTarget): ?string
		{
			switch ($mTarget) {
				case LINK_TOP:
					return '_top';
				case LINK_NEW:
					return '_blank';
				case LINK_NORMAL:
					return 'main';
				default:
					printf('%s', $mTarget);
					raise_notice(sprintf("Use of undefined constant value '0x%X', using '0x%X'", $mTarget, LINK_NORMAL),
						E_USER_NOTICE);
					break;
			}
		}

		public function set_category_cell_count($mNumber = 1): void
		{
			$this->categoryCellCounter = 0;
			$this->categoryCellMaximum = $mNumber;
		}

		public function set_info_cell_count($mNumber = 1): void
		{
			$this->infoCellCounter = 0;
			$this->infoCellMaximum = $mNumber;
		}

		public function set_link_cell_count($mNumber = 1): void
		{
			$this->linkCellCounter = 0;
			$this->linkCellMaximum = $mNumber;
		}
		/* Helper Functions */
		/* InternalExists(internalName):
		*      used as a quick lookup to see if the category internal
		*      name (quick alias, e.g. "Site Tools" => "tools") exists
		*      when displaying links belonging to a specific category
		*/

		public function set_module_cell_count($mNumber = 1): void
		{
			$this->moduleCellCounter = 0;
			$this->moduleCellMaximum = $mNumber;
		}

		/** debugging info function */
		public function dump_obj($mObj)
		{
			return $this->$mObj;
		}

		public function get_category_size($mCategory): int
		{
			if (!isset($this->lArray[$mCategory])) {
				return 0;
			}

			return count($this->lArray[$mCategory]);
		}

		public function getCategoryTitle(): string
		{
			return strtoupper($this->cArray[$this->activePage['internal']]['name']);
		}

		public function getLinks(): array
		{
			$links = array();
			$activeLink = $this->activePage['link'];
			foreach ($this->categoryPages as $category) {
				$active = false;
				$link = $category->getLink();
				if (is_array($link)) {
					// get first item, which is the main app
					$link = array_shift($link);
				}
				if ($activeLink == $link) {
					$active = true;
				}
				$obj = new Template_Object_Link();
				$obj->setName($category->getName())->setTarget($category->getTarget())
					->setLink($link)->setCategory($category);
				if ($active) {
					$obj->setActive();
				}
				$links[] = $obj;
			}

			return $links;
		}

		public function getActiveCategoryName()
		{
			if ($this->activePage === null) {
				$this->find_active_page();
			}

			return $this->activePage['category'] ?? '';
		}

		public function find_active_page(): ?array
		{
			if ($this->activePage) {
				return $this->activePage;
			}
			$seen = false;
			$url = $_SERVER['REDIRECT_URL'] ?? strtok($_SERVER['REQUEST_URI'], '?');

			if (substr($url, -4) === '.php') {
				$page = substr($url, 0, -4);
			} else {
				$page = rtrim($url, '/');
			}


			foreach ($this->cArray as $category) {
				$internal = $category->getInternal();
				if (empty($this->lArray[$internal])) {
					continue;
				}
				foreach ($this->lArray[$internal] as $linkItem) {
					$link = $linkItem->getLinkAliases();
					// skip empty items
					if (!$link) {
						continue;
					}

					$linktmp = null;
					if (is_array($link)) {
						$linktmp = $link;
						$link = array_shift($link);
					}
					/**
					 * @TODO filter same root APIs that may allow authorization to other users
					 */
					/*print '<code><pre>'.var_export($_SERVER,true).'</pre></code>'; die(__LINE__);*/
					if ($linktmp) {
						/** multiple link case */
						foreach ($linktmp as $linkEl) {
							if (!$seen && (0 === strpos($page, $linkEl))) {
								$seen = true;
								$this->activePage = array(
									'link'     => $link,
									'name'     => $linkItem->getName(),
									'internal' => $category->getInternal()
								);
								$linkItem->setActive();
								$this->categoryPages = $this->lArray[$internal];
								break;
							}
						}
					} else if (!$seen && (0 === strpos($page, $link))) {
						/* this is the active page */
						$seen = true;
						$this->activePage = array(
							'link'     => $link,
							'name'     => $linkItem->getName(),
							'internal' => $category->getInternal(),
						);
						$linkItem->setActive();
						$this->categoryPages = $this->lArray[$internal];
						break;

					}
				}
				reset($this->lArray[$internal]);
			}
			reset($this->cArray);

			return $this->activePage;
		}

		public function getActivePageName()
		{
			if ($this->activePage === null) {
				$this->find_active_page();
			}

			return $this->activePage['name'] ?? '';
		}

		public function print_quickmenu(): void
		{
			print '<div id="ui-menu" class="nav">';
			$seen = false;
			$buf = [];
			$app = Page_Container::get()->getApplicationID();

			foreach ($this->cArray as $category) {
				$internal = $category->getInternal();
				$menuid = $catname = '';
				if ($internal) {
					// not top-level
					$catname = $category->getName();
					$menuid = 'ui-menu-category-' . strtolower(str_replace(array(' '), '_', $internal));
					// do not print category contents
					if (!$category->getVisibility()) {
						continue;
					}
				}

				$seen = false;
				$linkbuff = [];
				if (empty($this->lArray[$internal])) {
					continue;
				}
				foreach ($this->lArray[$internal] as $linkItem) {
					$active = false;
					// skip empty items
					$link = $linkItem->getLink();
					if (!$link || !$linkItem->getVisibility()) {
						continue;
					}

					if (is_array($link)) {
						$link = array_shift($link);
					}
					if ($linkItem->getInternal() == $app) {
						/* this is the active page */
						$active = $seen = true;
						$this->activePage = array(
							'link'     => $link,
							'name'     => $linkItem->getName(),
							'category' => $category->getName(),
							'internal' => $category->getInternal()
						);
						$this->categoryPages = $this->lArray[$internal];
					}

					//type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					$baseclass = $internal ? 'dropdown-item ui-menu-link' : 'ui-menu-link ui-menu-category ui-menu-category-' . $linkItem->getInternal();
					$linkbuff[] = sprintf(
						'<a class="%s %s" href="%s" target="%s">%s</a>',
						$baseclass,
						($active ? 'active' : ''),
						$link,
						$linkItem->getTarget(),
						$linkItem->getName()
					);
				}
				reset($this->lArray[$internal]);
				if ($internal && \count($linkbuff) > 0) {
					$buf[] = sprintf('<div class="dropdown"><a class="ui-menu-category %s %s dropdown-toggle ui-menu-icon" data-target="#%sLinks" data-parent="#navbar-header" data-toggle="collapse" role="button" aria-haspopup="true" '
						. 'aria-expanded="%s"  id="%s" href="%s">%s</a>',
						$menuid,
						($seen ? 'ui-menu-active open' : ''),
						$menuid,
						$seen ? 'true' : 'false',
						$menuid,
						'#',
						$catname);

					$buf[] = '<div class="collapse ' . ($seen ? 'show' : '') . ' ui-menu-category-apps" role="tabpanel" id="' . $menuid . 'Links" aria-labelledby="' .
						$menuid . '" aria-expanded="' . ($seen ? 'true' : 'false') . '">' . implode('',
							$linkbuff) . '</div>';
					$buf[] = '</div>'; // close category
				} else {
					$buf[] = implode('', $linkbuff);
				}
			}
			print implode('', $buf) . '</div>';
			reset($this->cArray);
		}

		public function user_permitted(string $uri): bool
		{
			if (NO_AUTH) {
				return true;
			}
			$prefix = self::BASE_DIR;
			$prefixlen = strlen($prefix);
			if (0 !== strpos($uri, $prefix)) {
				return true;
			}

			$page = $this->find_active_page();

			return $page !== null;
		}
	}