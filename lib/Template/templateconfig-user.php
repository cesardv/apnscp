<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/** @var Template_Engine $templateClass */
	$templateClass->create_category(
		'Account', // canonical name
		true,               // assertions (may be an array)
		'/images/template/app-icons/account.png',  // image for the category
		'account'             // internal name
	);

	$templateClass->create_category(
		'Tools',
		false,
		'/images/template/app-icons/tools.png',
		'tools',
		null
	);

	$templateClass->create_category(
		'Files',
		true,
		'/images/template/app-icons/files.png',
		'files',
		null
	);

	$templateClass->create_category(
		'Databases',
		true,
		'/images/template/app-icons/databases.png',
		'databases'
	);

	$templateClass->create_category(
		'Mail',
		\cmd('email_user_enabled', null, 'imap') || \cmd('email_user_enabled', null, 'pop3'),
		'/images/template/app-icons/mail.png',
		'mail'
	);

	$templateClass->create_category(
		'Dev',
		true,
		'/images/template/app-icons/goodies.png',
		'dev'
	);

	$templateClass->create_category(
		'Reports', // canonical name
		true,               // assertions (may be an array)
		'/images/template/app-icons/reports.png',  // image for the category
		'reports'
	);

	$templateClass->create_category(
		'Help',
		true,
		'/images/template/app-icons/help.png',
		'help'
	);

	$templateClass->create_link(
		'Dashboard',
		array('/apps/dashboard', '/apps/dashboard-old'),
		true,
		null,
		null
	);

	$templateClass->create_link(
		'API Docs',
		'https://api.apnscp.com/',
		true,
		null,
		'help'
	);

	$templateClass->create_link(
		'Help Center',
		MISC_KB_BASE,
		true,
		null,
		'help'
	);

	$templateClass->create_link(
		'Summary',
		'/apps/summary',
		true,
		null,
		'account'
	);

	$templateClass->create_link(
		'Server Info',
		'/apps/stats',
		true,
		null,
		'reports'
	);

	$templateClass->create_link(
		'Settings',
		'/apps/changeinfo',
		true,
		null,
		'account'
	);

	$templateClass->create_link(
		'File Manager',
		'/apps/filemanager',
		true,
		null,
		'files'
	);

	$templateClass->create_link(
		'phpMyAdmin',
		'/apps/phpmyadmin',
		true,
		null,
		'databases'
	);

	$templateClass->create_link(
		'phpPgAdmin',
		'/apps/phppgadmin',
		Template_Engine::call('sql_enabled', array('postgresql')),
		null,
		'databases'
	);

	$templateClass->create_link(
		'WebDisk',
		'/apps/webdisk',
		cmd('auth_user_permitted', \Session::get('username'), 'dav'),
		null,
		'files'
	);


	$templateClass->create_link(
		'Vacation Responder',
		'/apps/vacation',
		\cmd('email_configured'),
		null,
		'mail'
	);

	$templateClass->create_link(
		'Spam Filter',
		'/apps/saconfig',
		\cmd('email_configured'),
		null,
		'mail'
	);


	$templateClass->create_link(
		'Webmail',
		'/apps/webmail',
		\cmd('email_configured'),
		null,
		'mail'
	);

	$templateClass->create_link(
		'Knowledge Base',
		MISC_KB_BASE,
		true,
		null,
		'help'
	);

	$templateClass->create_link(
		'Terminal',
		'/apps/terminal',
		SSH_EMBED_TERMINAL && Template_Engine::call('ssh_enabled'),
		null,
		'dev'
	);

	$templateClass->create_link(
		'Task Scheduler',
		'/apps/crontab',
		Template_Engine::call('common_get_service_value',
			array('ssh', 'enabled')) && Template_Engine::call('crontab_enabled'),
		null,
		'dev'
	);

	/***************** end resources links **************/

	/* let's only draw draw these objects for the view_shortcuts page */
	/*
	*  Modules
	*/

	#    $templateClass->create_module(
	#        "Getting Started",
	#        "shit",
	#        true,
	#        "/images/headers/gettingstarted.jpg"
	#    );
	#
	/*
	*  Site Info Table Information
	*/
	/*if (!stristr(HTML_Kit::page_url(),"dashboard"))
		return;*/
	$load = Template_Engine::call('common_get_load');

	$templateClass->create_info(
		'Load Average',
		implode(', ', $load),
		true,
		null,
		1,
		'LAs tell how many processes are enqueued, pending execution.  Lower numbers ' .
		'imply more time spent waiting, while higher numbers are indicative of bottlenecks and
            degraded server performance.' .
		'<br /><br />A LA of <em>' . sprintf('%.1f',
			NPROC) . ' is optimal</em> for this server &ndash; CPUs are always working ' .
		'and never wasting energy.',
		'Load Averages'
	);

	$rebootdays = Template_Engine::call('common_get_uptime', array(false));
	$reboot = time() - $rebootdays;
	$templateClass->create_info(
		'Last Reboot',
		date('F j, Y', $reboot) . ' (' . round($rebootdays / 86400) . ' days)',
		true,
		'Duration since the last server reboot.',
		'Uptime'
	);

	$isFirstLogin = UCard::init()->lastAccess();

	$templateClass->create_info(
		'MySQL Version',
		Template_Engine::call('sql_mysql_version', [true]),
		$isFirstLogin,
		null
	);

	$templateClass->create_info(
		'PGSQL Version',
		Template_Engine::call('sql_pgsql_version', [true]),
		$isFirstLogin,
		null,
		1,
		'Abbreviated term for PostgreSQL.'
	);


	$templateClass->create_info(
		'Perl Version',
		Template_Engine::call('perl_version'),
		$isFirstLogin
	);

	$templateClass->create_info(
		'PHP Version',
		Template_Engine::call('php_version'),
		$isFirstLogin
	);

	$templateClass->create_category(
		'Miscellaneous',
		true,
		null,
		'misc',
		null
	)->hide();


	$templateClass->create_link(
		'Sitemap',
		'/apps/sitemap',
		true,
		null,
		'misc'
	);

	$templateClass->create_link(
		'Changelog',
		'/apps/changelog',
		true,
		null,
		'misc'
	);

	// general error handler
	$templateClass->create_link(
		'Error',
		'/apps/error',
		true,
		null,
		'misc'
	);
