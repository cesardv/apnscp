<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class User_Defaults
	{
		private static $def_options;
		private static $options;

		public static function get_value($param)
		{
			$values = self::load_defaults();

			return isset($values[$param]) ?
				$values[$param] :
				error($param . ': unknown field in user defaults');
		}

		public static function load_defaults()
		{
			if (isset(self::$options)) {
				return self::$options;
			}
			$user_settings = array();

			if (Util_Conf::file_exists('/etc/usertemplate')) {
				$user_settings = parse_ini_string(Util_Conf::call('file_get_file_contents', ['/etc/usertemplate']));
				if (isset($user_settings['email_domains']) && false !== strpos($user_settings['email_domains'], ',')) {
					$user_settings['email_domains'] = preg_split('/,\s*/', $user_settings['email_domains']);
				}
				if (empty($user_settings['subdomain_domains'])) {
					$user_settings['subdomain_domains'] = Util_Conf::login_domain();
				} else {
					if (!empty($user_settings['subdomain_domains']) && false !== strpos($user_settings['subdomain_domains'],
							',')
					) {
						$user_settings['subdomain_domains'] = preg_split('/,\s*/', $user_settings['subdomain_domains']);
					}
				}
			}
			self::$options = array_merge(self::get_defaults(), $user_settings);

			return self::$options;
		}

		private static function get_defaults()
		{
			if (!isset(self::$def_options)) {
				self::$def_options = array(
					'disk_quota'        => 500,
					'email_enable'      => 1,
					'email_domains'     => Util_Conf::mailbox_domains(),
					'email_create'      => 1,
					'email_imap'        => 1,
					'email_smtp'        => 1,
					'ftp_jail'          => 1,
					'ftp_jail_path'     => '',
					'ftp_enable'        => 1,
					'subdomain_enable'  => 0,
					'subdomain_domains' => null,
					'ssh_enable'        => 0,
					'username'          => '',
					'password'          => '',
					'disk_quota_warn'   => 80,
					'disk_quota_crit'   => 95,
					'crontab_enable'    => 0,
					'cp_enable'         => 1,
					'dav_enable'        => 0,
					'shell'             => '/bin/bash'
				);
			}

			return self::$def_options;
		}

		public static function set_defaults($params)
		{
			$options = self::_merge_options($params);
			if ($params['disk_quota'] > Util_Conf::call('site_get_account_quota')) {
				$options['disk_quota'] = 0;
			}
			if (sizeof($options['email_domains']) == sizeof(Util_Conf::mailbox_domains())) {
				$options['email_domains'] = $email_domains = '*';
			} else {
				if (is_array($options['email_domains'])) {
					$email_domains = join(',', $options['email_domains']);
				} else {
					$email_domains = '';
				}
			}
			if (sizeof($options['subdomain_domains']) == sizeof(Util_Conf::all_domains())) {
				$options['subdomain_domains'] = $subdomains = '*';
			} else {
				if (is_array($options['subdomain_domains'])) {
					$subdomains = '';
					//join(',', $options['subdomain_domains']);
				} else {
					$subdomains = '';
				}
			}
			if (isset($params['disk-unlimited'])) {
				$quota = 0;
			} else {
				$quota = intval($options['disk_quota']);
			}
			$options['disk_quota'] = $quota;
			$template = '; Default settings for new users created via' . "\n" .
				'; Set User Defaults' . "\n" .
				'[default]' . "\n" .
				'; Default disk quota' . "\n" .
				'disk_quota=' . intval($quota) . "\n" .
				'; Enable IMAP/SMTP access' . "\n" .
				'email_enable=' . intval($options['email_enable']) . "\n" .
				'; Finer control-- enable IMAP, implies email_enable=true' . "\n" .
				'email_imap=' . intval($options['email_imap']) . "\n" .
				'; Finer control-- enable SMTP, implies email_enable=true' . "\n" .
				'email_smtp=' . intval($options['email_smtp']) . "\n" .
				'; Create e-mail addresses for user' . "\n" .
				'email_create=' . intval($options['email_create']) . "\n" .
				'; Domains to use in address creation, * = all' . "\n" .
				'; Separate multiple entities with comma, e.g.' . "\n" .
				'; foo.com,bar.com,baz.com' . "\n" .
				'email_domains=' . $email_domains . "\n" .
				'; Enable FTP access' . "\n" .
				'ftp_enable=' . intval($options['ftp_enable']) . "\n" .
				'; Jail FTP users to home' . "\n" .
				'ftp_jail=' . intval($options['ftp_jail']) . "\n" .
				'; Enable subdomain creation' . "\n" .
				'subdomain_enable=' . intval($options['subdomain_enable']) . "\n" .
				'; Subdomains to bind, * = all' . "\n" .
				'subdomain_domains=' . $subdomains . "\n" .
				'; Enable SSH access' . "\n" .
				'ssh_enable=' . intval(isset($options['ssh_enable']) && $options['ssh_enable']) . "\n" .
				'; Enable task scheduling via crontab' . "\n" .
				'crontab_enable=' . intval(isset($options['ssh_enable']) && $options['crontab_enable']) . "\n" .
				'; Enable CP access' . "\n" .
				'cp_enable=' . intval(isset($options['cp_enable']) && $options['cp_enable']) . "\n" .
				'; Enable DAV access' . "\n" .
				'dav_enable=' . intval(!empty($options['dav_enable']) && $options['dav_enable']) . "\n" .
				'; Enable API access' . "\n" .
				'api_enable=' . intval(isset($options['cp_enable']) && $options['cp_enable']) . "\n";
			if (!Util_Conf::call('file_put_file_contents', array('/etc/usertemplate', $template, true))) {
				return error('failed to set default options');
			}

			return $options;
		}

		private static function _merge_options($newopts)
		{
			$oldopts = self::load_defaults();
			foreach ($oldopts as $key => $val) {
				if (!isset($newopts[$key])) {
					$newopts[$key] = 0;
				}
			}

			return array_merge($oldopts, $newopts);
		}

	}

?>
