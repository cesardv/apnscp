<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class JS_Template
	{

		private static $templates;
		private static $_plugins = array();

		public static function get_template($template_name)
		{
			if (!self::$templates) {
				self::init();
			}

			if (isset(self::$templates[$template_name])) {
				return self::$templates[$template_name];
			}
			fatal("unknown JS template `%s'", $template_name);

			return array();

		}

		private static function init()
		{
			self::$templates = array(
				'prism'           => [
					'/js/prism.js'   => ['type' => 'js'],
					'/css/prism.css' => ['type' => 'css']
				],
				'ajax_unbuffered' => array('/js/ajax_unbuffered.js' => array('type' => 'js')),
				'slider'          => array(),

				'multiselect' => array('/js/multiselect.js' => array('type' => 'js')),

				'browser' => array(
					'/js/filetree.js' => array('type' => 'js')
				),

				'autocomplete' => array(
					'/js/autocomplete.js' => array('type' => 'js')
				),
				'lightbox'     => array(
					'/js/lightbox.js' => array('type' => 'js'),
				),
				'sorter'       => array('/js/tablesorter.js' => array('type' => 'js')),
				'tiny_mce'     => array('/js/tiny_mce/jquery.tinymce.js' => array('type' => 'js')),
				'tinymce'      => array('/js/tiny_mce/jquery.tinymce.js' => array('type' => 'js')),
				'upload'       => array(
					'/js/upload.js' => array('type' => 'js'),
				),
				'flot'         => array(
					'/js/flot/current/jquery.flot.js'        => array('type' => 'js'),
					'/js/flot/current/jquery.flot.resize.js' => array('type' => 'js'),
					'/js/flot/current/jquery.flot.time.js'   => array('type' => 'js'),
					'JS_Plugin_Flot'                         => array('type' => 'plugin'),
				),
				'flot.grow'    => array(
					'/js/flot/current/jquery.flot.growraf.js' => array('type' => 'js')
				),
				'flot.pie'     => array(
					'/js/flot/current/jquery.flot.pie.js' => array('type' => 'js')
				),

				'flot.time' => array(
					'/js/flot/current/jquery.flot.time.js' => array('type' => 'js'),
				),

				'flot.errorbars' => array(
					'/js/flot/current/jquery.flot.errorbars.js' => array('type' => 'js')
				),
				'flot.navigate'  => array(
					'/js/flot/current/jquery.flot.navigate.js' => array('type' => 'js')
				),
				'flot.time'      => array(
					'/js/flot/current/jquery.flot.time.js' => array('type' => 'js')
				),
				'flot.selection' => array(
					'/js/flot/current/jquery.flot.selection.js' => array('type' => 'js')
				),
				'flot.dashed'    => array(
					'/js/flot/current/jquery.flot.dashed.js' => array('type' => 'js')
				),
				'treemap'        => array(
					'/js/jquery.ui.treemap.js' => array('type' => 'js'),

				)


			);

		}

		public static function register_plugin($name, $fn)
		{
			self::$_plugins[$name] = $fn;
		}

		/**
		 * @TODO
		 */
		public static function load_plugin($name)
		{
			self::_load_interface();

			$class = ucwords(str_replace('_', ' ', $name));
			$class = str_replace(' ', '_', $class);
			if (class_exists('JS_Plugin_' . $class)) {
				return true;
			}

			if (!isset(self::$_plugins[$name])) {
				return error("plugin loading `%s' failed", $name);
			}

			$file = dirname(__FILE__) . '/JS_Plugins/' . $name . '.php';
			if (!file_exists($file)) {
				return error("plugin loading `%s' failed, " .
					'missing class', $name);
			}

			include($file);
			if (!class_exists($class)) {
				return error("plugin loading `%s' failed, " .
					'cannot load class', $name);
			}
			self::$_plugins[$name] = new $class;

			return true;
		}

		private static function _load_interface()
		{
			if (interface_exists('JS_Template_Interface')) {
				return true;
			}

			$file = dirname(__FILE__) . '/JS_Plugins/Interface.php';
			include($file);

			return;
		}

	}

?>
