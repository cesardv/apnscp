<?php
if (!isset($_SERVER['REQUEST_URI'])) {
    die("OK.");
}

define('INCLUDE_PATH', realpath(__DIR__ . '/..'));
$debug = 0&extension_loaded('xdebug');
if ($debug) $__pr_start_time = microtime(true);
$req_page = null;
$url = $_SERVER['REDIRECT_URL'] ?? strtok($_SERVER['REQUEST_URI'], '?');
if (0 === strpos($url, '/apps/') && isset($url[7])) {
    // these should be moved to public/assets/xyz in the future
    $extension = substr($url, strrpos($url, '.'));
    if ($extension !== ".php" && ($extension === ".js" || $extension === ".css" ||
        $extension === ".png" || $extension === ".svg"))
    {
    	$paths = [
		    INCLUDE_PATH . '/' . $url,
		    INCLUDE_PATH . '/config/custom/' . $url
	    ];

        foreach ($paths as $path) {
	        if (file_exists($path)) {
		        ob_end_clean();
		        if ($extension === '.js') {
			        $mime = 'application/javascript';
		        } else if ($extension === '.css') {
			        $mime = 'text/css';
		        } else if ($extension === '.png') {
			        $mime = 'image/png';
		        } else if ($extension === '.svg') {
			        $mime = 'application/svg+xml';
		        }
		        header('Content-Type:' . $mime, true);
		        @readfile($path, false);
		        exit();
	        }
        }
    }
    $end = strpos($url,'/',7);
    if ($end) $req_page = substr($url,6, $end-6);
    else      $req_page = basename($url,'.php');
} else if ($_SERVER['SERVER_PORT'] == 2077 || $_SERVER['SERVER_PORT'] == 2078) {
    include(INCLUDE_PATH. '/dav/index.php');
    exit;
}

if (!defined('IS_ISAPI'))
    define('IS_ISAPI', 1);
if ($req_page === 'login' || $req_page === 'logout' || $req_page === 'forgot_info') {
    define('NO_AUTH', 1);
}
include(INCLUDE_PATH."/lib/apnscpcore.php");

// default page
if (!isset($_SERVER['REDIRECT_URL'])) {
	$url = \Auth::authenticated() ? '/apps/dashboard' : '/apps/login';
	header('Location: ' . $url);
	exit;
}

include(INCLUDE_PATH."/lib/html/tip_engine.php");
// OPCache conflict? Needs to move over to namespace + autoload
if (!class_exists('Template_Engine', false)) {
	include(INCLUDE_PATH."/lib/Template/Engine.php");
}
	include(INCLUDE_PATH."/lib/html/page_container.php");
	include(INCLUDE_PATH."/lib/html/page_renderer.php");
	\apnscpFunctionInterceptor::init(true);
	$file = \Page_Container::resolve($req_page);
	if (null === $file) {
		if (!Auth::authenticated()) {
			header('Status: 307 Temporary Redirect',true,307);
			header('Location: /apps/login',true,307);
			exit();
		}
		http_response_code(404);
	} else {
		include(INCLUDE_PATH.'/lib/html/stats_logger.php');
	}

	/**
	 * deny access if app is not defined in lib/html/templateconfig-<role>.php
	 */
	if (!NO_AUTH && !Template_Engine::init()->user_permitted($url)) {
		http_response_code(404);
	}

	if (http_response_code() >= 400) {
		$req_page = 'error';
		$file = \Page_Container::resolve($req_page);
	}
	include($file . '/' . $req_page . '.php');
	include(INCLUDE_PATH."/lib/html/js_template.php");
	if (!NO_AUTH) {
		$UCard = UCard::init();
		if ($UCard->hasPrivilege('site', 'user') && $req_page != 'error') {
			$pagecnt = (int)$UCard->getPref2('pageview',$req_page);
			if ($pagecnt < 1) {
				if (!isset($_SESSION['first'])) $_SESSION['first'] = array();
				$_SESSION['first'][$req_page] = 1;
			}
			$UCard->setPref2('pageview',$req_page,++$pagecnt);
		}
	}

	$kernel = Page_Container::kernelFromApp($req_page);

	try {
		$Page = Page_Container::init($kernel);
		$Page->handle_request();
	} catch (\Throwable $e) {
		$Page = Page_Container::init(\apps\error\Page::class, [550]);
		$Page->setException($e);
		$Page->handle_request();
		\Error_Reporter::handle_exception($e);
	}
	Auth::end_hook();

	if (is_debug() && $debug) {
		$__pr_elapsed_time = microtime(true)-$__pr_start_time;
		if ($__pr_elapsed_time >= 5) {
			print "Time spent: ".$__pr_elapsed_time;
		} else if (0) {
			register_shutdown_function(create_function('', 'unlink("'.xdebug_get_profiler_filename().'");'));
		}
	}
