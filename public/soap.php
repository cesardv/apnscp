<?php
	define('INCLUDE_PATH', realpath(dirname($_SERVER['SCRIPT_FILENAME']) . '/../'));
	define('IS_SOAP', 1);

	include(INCLUDE_PATH . '/lib/apnscpcore.php');
	\Error_Reporter::set_verbose(0);

	class API_Reflector
	{
		private $afi;
		private $server;

		private function __construct(SoapServer $server)
		{
			$this->afi = apnscpFunctionInterceptor::factory(\Auth::profile());
			$this->server = $server;
			use_soap_error_handler(false);
		}

		public static function listen()
		{
			$opts = array(
				'uri'        => 'urn:apnscp.api.soap',
				'cache_wsdl' => WSDL_CACHE_BOTH
			);
			$server = new SoapServer(SOAP_WSDL, $opts);
			$reflector = new static($server);
			$server->setObject($reflector);
			$server->handle();

		}

		public function __call($func, $args)
		{
			// process args to remove any nillable=yes values...
			// this can be tricky because if null is passed, default parameters
			// are not substituted in for the function
			for ($i = sizeof($args) - 1; $i >= 0; $i--) {
				if (array_key_exists($i, $args) && !isset($args[$i])) {
					array_pop($args);
					continue;
				}
				break;
			}
			try {
				$ret = $this->afi->$func(...$args);
			} catch (\apnscpException $e) {
				// convert fatal() to exception
				return self::fault($e->getMessage());
			}
			if ($ret instanceof Exception) {
				return self::fault($ret->getMessage());
			} else if (Error_Reporter::is_error()) {
				return self::fault(Error_Reporter::get_errors());
			}
			$messages = Error_Reporter::get_buffer();
			$msg = array_map(function ($a) {
				return array(
					'message'  => $a['message'],
					'errno'    => $a['severity'],
					'severity' => Error_Reporter::errno2str($a['severity'])
				);
			}, $messages);
			if ($messages) {
				$header = new SoapHeader($func . '_Response', 'Messages', $msg);
				$this->server->addSoapHeader($header);
			}

			return self::post_parse($ret);
		}

		private static function fault($msg)
		{
			if (is_array($msg)) {
				$msg = join("\n", $msg);
			}
			return self::post_parse(
				new SoapFault("Server", $msg));
		}

		private static function post_parse($args)
		{
			if (isset($_GET['JSON'])) {
				return json_encode($args);
			}

			return $args;
		}

	}

	$exceptionUpgrade = \Error_Reporter::E_FATAL;
	if (isset($_SERVER['HTTP_ABORT_ON'])) {
		switch (strtolower($_SERVER['HTTP_ABORT_ON'])) {
			case 'error':
				$exceptionUpgrade = \Error_Reporter::E_ERROR;
				break;
			case 'warning':
			case 'warn':
				$exceptionUpgrade = \Error_Reporter::E_WARNING;
				break;
			case 'info':
				$exceptionUpgrade = \Error_Reporter::E_INFO;
				break;
			case 'deprecated':
				$exceptionUpgrade = \Error_Reporter::E_DEPRECATED;
				break;
			case 'debug':
				$exceptionUpgrade = \Error_Reporter::E_DEBUG;
				break;
			case 'fatal':
				break;
			default:
				fatal("Unknown tolerance mode `%s'", $_SERVER['HTTP_X_TOLERANCE']);
		}
	}
	\Error_Reporter::exception_upgrade($exceptionUpgrade);

	API_Reflector::listen();