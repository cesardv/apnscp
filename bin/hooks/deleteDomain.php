<?php declare(strict_types=1);
	/**
	 * Hook facility (DeleteDomain)
	 * Called BEFORE a domain is deleted
	 * Parameters:
	 *   - string site
	 *
	 * This file, if present in config/custom/hooks/ named either
	 * deleteDomain.sh or deleteDomain.php will be called AFTER
	 * the account has been deleted
	 */

	$n = 0;
	do {
		$path = dirname(__FILE__, ++$n);
	} while (!file_exists("$path/lib/config.php"));
	define('INCLUDE_PATH', realpath($path));
	include(INCLUDE_PATH . '/lib/CLI/cmd.php');

	$args = cli\parse();
	$c = cli\cmd(null, $args[0]);
	if (!is_object($c)) {
		fatal("Failed to create instance of `%s'", $args[0]);
	}

	/**
	 * Do whatever
	 */
	$ctx = \Auth::profile();
	echo "Hello as ", $c->common_whoami(), " on ", $ctx->domain, " (", $ctx->site, ")\n";
	echo "Service deletion order:\n";
	foreach (array_reverse(\Opcenter\SiteConfiguration::import($ctx)->getServices()) as $service) {
		echo $service, "\n";
	}

	// post hook formatting
	$buffer = Error_Reporter::get_buffer();
	dlog("Deletion hooks %s (%x) %s",
		$ctx->site,
		Error_Reporter::error_type(Error_Reporter::get_severity()),
		Error_Reporter::is_error() ? 'Failed' : 'Succeeded'
	);
	\cli\dump_buffer($buffer);

	exit (Error_Reporter::is_error());