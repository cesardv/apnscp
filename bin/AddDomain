#!/usr/bin/env apnscp_php
<?php
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
 */

define('INCLUDE_PATH', dirname(__DIR__) . '/');
include INCLUDE_PATH . '/lib/CLI/cmd.php';

$args = [];
register_shutdown_function(static function() use (&$args) {
	if (!\Error_Reporter::is_error()) {
		return;
	}
	if (is_debug() || array_get($args, 'options.output') === 'json') {
		// already listed or to be listed
		return;
	}
	foreach (\Error_Reporter::get_errors() as $e) {
		fwrite(STDERR, \Error_Reporter::errno2str(\Error_Reporter::E_ERROR) . ": $e\n");
	}
});
\Opcenter\Lock::lock();
$args = Opcenter\CliParser::parse();

// create auth context for _verify_conf to work
if (!\Auth::authenticated()) {
	\cli\cmd();
}

if (null === ($domain = array_pull($args, 'options.conf.siteinfo.domain'))) {
	fatal("cannot create account - missing domain name");
}
$instance = new \Opcenter\Account\Create($domain, array_get($args, 'options.conf', []), array_except($args['options'], ['conf'], []));
try {
	$ret = $instance->exec();
} catch (\Throwable $e) {
	\Error_Reporter::handle_exception($e);
}

$buffer = Error_Reporter::get_buffer();
dlog('Added %s (%s) %s',
    $domain,
    strtoupper(Error_Reporter::error_type(Error_Reporter::get_severity()) ?: 'success'),
    Error_Reporter::is_error() ? 'Failed' : 'Succeeded'
);
//cli\dump_buffer($buffer);
$status = $instance->getStatus();
exit ($status);