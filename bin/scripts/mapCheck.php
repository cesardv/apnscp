#!/usr/bin/env apnscp_php
<?php

	use Opcenter\Account\Enumerate;
	use Opcenter\Apnscp;
	use Opcenter\Database\PostgreSQL\Opcenter;
	use Opcenter\Filesystem;
	use Opcenter\Map;
	use function cli\get_instance;
	use function cli\parse;

	include(dirname(__FILE__, 3) . '/lib/CLI/cmd.php');

	$old = \Error_Reporter::set_verbose();
	\Error_Reporter::set_verbose(max($old, 2));

	$args = parse();
	$afi = get_instance();

	if (!$afi) {
		fatal("cannot init afi instance");
	}

	/**
	 * Check
	 *
	 * @param bool $rebuild
	 */

	$command = array_get($args, 0, 'check');
	$rebuild = false;
	switch ($command) {
		case 'rebuild':
			$rebuild = true;
			break;
		case 'check':
			break;
		default:
			fatal("Unknown mode `%s'. 'rebuild', 'check' modes supported.", $command);
	}

	// physical directories requiring map checks
	$allSites = array_map('basename', glob(FILESYSTEM_VIRTBASE . '/site[0-9]*', GLOB_ONLYDIR));
	// sites present in domainmap but have no filesystem mapping

	$missingSites = array_flip(array_diff(
		array_values(Map::read(Map::DOMAIN_TXT_MAP)->fetchAll()),
		$allSites
	));

	$mockFilesystem = new class {
		use FilesystemPathTrait;
		public function set(string $site)
		{
			$this->site = $site;
			return $this;
		}

		public function get(string $service)
		{
			return $this->domain_info_path("current/${service}");
		}
	};
	$orphans = [];

	foreach (Map::META_MAPS as $map => $cfg) {
		info ("Scanning %s", $map);
		if (!file_exists(Map::home($map))) {
			if (!$rebuild) {
				warn("Map %(map)s missing. %(rebuild)s.",
					[
						'map' => $map,
						'rebuild' => sprintf(_("Run '%s rebuild' to resolve map inconsistencies"), $_SERVER['argv'][0])
					]
				);
				continue;
			}
			file_put_contents(\Opcenter\Map::home() . "/$map", '[DEFAULT]');
		}
		if (!file_exists($map)) {
			touch(Map::home($map));
		}
		$mapper = Map::load($map, $rebuild ? 'cd' : 'r');

		foreach ($mapper->fetchAll() as $key => $site) {
			// look for maps which reference sites missing from /home/virtual
			if (isset($missingSites[$site])) {
				if ($rebuild) {
					info("Found dangling site %s in %s - removing", ['site' => $site, 'map' => $map]);
					$mapper->delete($key);
				} else {
					info("Found dangling site %s in %s. %(rebuild)s.", [
						'site' => $site,
						'map' => $map,
						'rebuild' => sprintf(_("Run '%s rebuild' to resolve map inconsistencies"), $_SERVER['argv'][0])
					]);
				}
			}
		}

		[$service, $servicevar] = explode('.', $cfg);
		foreach ($allSites as $site) {
			$ini = $mockFilesystem->set($site)->get($service);
			if (!file_exists($ini)) {
				if (!isset($orphans[$site])) {
					warn("%s MISSING %s - orphaned site?", $site, $service);
					$orphans[$site] = 1;
				}
				continue;
			}

			if (false === ($svcval = array_get(\Util_Conf::parse_ini($ini), $servicevar, false))) {
				fatal("EMERG failed to get %s from %s", $servicevar, $ini);
			}

			if ($map === 'domainmap') {
				$ini = $mockFilesystem->set($site)->get('aliases');
				if (!file_exists($ini)) {
					// *shrug*
					continue;
				}
				$aliases = \Util_Conf::parse_ini($ini);
				if (!array_key_exists('aliases', $aliases)) {
					fatal("EMERG failed to get %s from %s", $servicevar, $ini);
				}
				foreach ((array)$aliases['aliases'] as $alias) {
					if (!$mapper->exists($alias)) {
						if ($rebuild) {
							info("ADD %s=%s (%s - %s)", $alias, $site, $map, $servicevar);
							$mapper->set($alias, $site);
						} else {
							info("MISSING %s=%s (%s - %s)", $alias, $site, $map, $servicevar);
						}
					}
				}
			}

			foreach ((array)$svcval as $v) {
				if ($mapper->exists($v)) {
					continue;
				}
				warn("%(site)s NOT IN %(map)s map",
					['site' => $site, 'map' => $map]
				);
				if (!$rebuild) {
					continue;
				}
				info("ADD %s=%s (%s - %s)", $v, $site, $map, $servicevar);
				$mapper->set($v, $site);
			}

			$mapper->save();
		}
	}
	$mapper->close();

	//rebuild map
	Filesystem::readdir(FILESYSTEM_VIRTBASE, static function ($f) use ($rebuild) {
		$path = FILESYSTEM_VIRTBASE . "/${f}";
		if (is_link($path) && !file_exists($path)) {
			if (!$rebuild) {
				warn("Found dangling link %(path)s. %(rebuild)s.", [
					'path' => $path,
					'rebuild' => sprintf(_("Run '%s rebuild' to resolve map inconsistencies"), $_SERVER['argv'][0])
				]);
			} else {
				info("Removing dangling link %s",$path);
				unlink($path);
			}
		}
	});


	$present = array_flip(
		array_values(
			array_intersect_key(
				$all = Map::load(Map::DOMAIN_TXT_MAP,'r')->fetchAll(),
				array_flip($afi->admin_get_domains())
			)
		)
	);

	/**
	 * Populate missing siteinfo entries
	 */

	if ($rebuild) {
		info("Rebuilt %s", Map::DOMAIN_MAP);
		\Auth_Module::rebuildMap();
	} else {
		info("Run '%s rebuild' to resolve map inconsistencies", $_SERVER['argv'][0]);
	}

	foreach (Enumerate::sites() as $site) {
		if (isset($present[$site])) {
			continue;
		}

		$ini = $mockFilesystem->set($site)->get('siteinfo');

		if (!file_exists($ini)) {
			if (!isset($orphans[$site])) {
				warn("%s MISSING %s - orphaned site?", $site, 'siteinfo');
			}
			continue;
		}

		$cfg = \Util_Conf::parse_ini($ini);

		if (!(new Opcenter(\PostgreSQL::pdo()))->createSite(
			\Auth::get_site_id_from_anything($site),
			$cfg['domain'],
			$cfg['email'],
			$cfg['admin_user']
		)) {
			warn('failed to populate admin database entry for `%(domain)s\' (`%(site)s\')', [
				'domain' => $cfg['domain'],
				'site'   => \Auth::get_site_id_from_anything($site)
			]);
			continue;
		}

		info("Imported missing domain `%(domain)s' (`%(site)s')", [
			'domain' => $cfg['domain'],
			'site'   => \Auth::get_site_id_from_anything($site)
		]);
	}

	Apnscp::restart('now');
	exit(!\Error_Reporter::is_error());
