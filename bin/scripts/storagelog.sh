#!/bin/sh
[[ -f /etc/sysconfig/apnscp ]] && . /etc/sysconfig/apnscp
APNSCP_APPLIANCE_DB=${APNSCP_APPLIANCE_DB:-"appldb"}

echo $(/usr/sbin/repquota -uan | awk 'BEGIN { ORS="" ; print "INSERT INTO storage_log(uid, quota) SELECT uid, quota FROM (VALUES "; ORS=","  } {
  if ($1 ~ /^#([5-9][0-9][0-9]|[1-9][0-9][0-9][0-9][0-9]*)$/) {
    gsub("#","",$1);
    print "("$1"," $3")"}
  } END {
    ORS="" ; print "(0, 0)) AS t (uid, quota) JOIN uids USING (uid); "
}') | psql -q "$APNSCP_APPLIANCE_DB"
