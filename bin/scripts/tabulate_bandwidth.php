#!/usr/bin/env apnscp_php
<?php

	use Opcenter\Service\ServiceValidator;
	use Opcenter\Service\Validators\Bandwidth\Enabled;

	define('INCLUDE_PATH', realpath(dirname($_SERVER['SCRIPT_FILENAME']) . '/../../'));
	include INCLUDE_PATH . '/lib/CLI/cmd.php';
	define('RESOLUTION', BANDWIDTH_RESOLUTION); // 3 minutes
	//include(INCLUDE_PATH.'lib/apnscpcore.php');
	define('HTTP', 15);
	define('UI', 7);
	define('DAV', 6);
	define('SOAP', 9);
	define('FTP', 4);
	define('POP3', 2);
	define('IMAP', 3);

	class Bandwidth
	{
		protected static $bandwidthEnablementCache = [];
		private $_recs = array();
		private $_account = array();
		private $_db;
		private $_bad = false;

		public function __construct($site_id, $bw_svc, $ext_id = null)
		{
			$this->_db = PostgreSQL::initialize();
			$this->_account = array(
				'site_id'  => (int)$site_id,
				'svc_id'   => (int)$bw_svc,
				'ext_id'   => (int)$ext_id,
				'ext_name' => null
			);
		}

		public function setExtendedName($name)
		{
			if ($this->_bad) {
				return false;
			}
			$ext_id = get_extended_id($this->_account['site_id'], $this->_account['svc_id'], $name);
			if ($ext_id === false) {
				$ext_id = add_extended_info(
					$this->_account['site_id'],
					$this->_account['svc_id'],
					$name
				);
			}
			if ($ext_id === false) {
				$this->_bad = true;

				return false;
			}
			$this->_account['ext_id'] = $ext_id;
			$this->_account['ext_name'] = $name;

			return $ext_id;
		}

		public function add($ts, $in, $out): void
		{
			$ts -= $ts % RESOLUTION;
			if (!isset($this->_recs[$ts])) {
				$this->_recs[$ts] = array('in' => 0, 'out' => 0);
			}
			$this->_recs[$ts]['in'] += $in;
			$this->_recs[$ts]['out'] += $out;
		}

		public function count(): int
		{
			return count($this->_recs);
		}

		public function commit()
		{
			// check bad lookup
			if ($this->_bad) {
				return false;
			}
			$res = $this->_log_bandwidth();
			if (!$res && BUG_REPORT) {
				mail(BUG_REPORT,
					'Bandwidth Reconcile Error',
					var_export($this->_account, true) . ' ' . pg_last_error());
				fatal('Bye!');
			}

			return $res;
		}

		private function _log_bandwidth()
		{

			if (!$this->_recs) {
				return -1;
			}
			if (!static::hasBandwidthService($this->_account['site_id'])) {
				return -1;
			}
			$this->sort();
			$queries = array();
			$site_id = $this->_account['site_id'];
			$svc_id = $this->_account['svc_id'];
			$ext_id = $this->_account['ext_id'];
			$bw_in = $bw_out = 0;
			foreach ($this->_recs as $ts => $bw) {
				if (($bw['in'] + $bw['out']) < 1) {
					// potential record type in Dovecot:  Connection closed (No commands sent) bytes=0/0
					print 'Odd record... ' . $bw['in'] . ' ' . $bw['out'] . ' ' . $site_id . ' ' . $ext_id . "\n";
					continue;
				}
				$queries[] = sprintf("(%d, %d, %d, %d, TO_TIMESTAMP(%d), %s)",
					$site_id,
					$bw['in'],
					$bw['out'],
					$svc_id,
					$ts,
					null === $ext_id ? 'null' : (int)$ext_id);
				$bw_in += $bw['in'];
				$bw_out += $bw['out'];
			}
			if (!$queries) {
				return true;
			}
			printf("Logging svc %2d for site %3d %8.2f MB (%4d MB/%4d MB) %-12s\n",
				$svc_id,
				$site_id,
				($bw_in + $bw_out) / 1024 / 1024,
				$bw_in / 1024 / 1024,
				$bw_out / 1024 / 1024,
				$this->_account['ext_name']
			);
			$q = 'INSERT INTO bandwidth_log
                (site_id, in_bytes, out_bytes, svc_id, ts, ext_id) VALUES ' . implode(',', $queries) . ';';
			$res = $this->_db->query($q);

			return $res ? count($this->_recs) : $res;
		}

		/**
		 * Bypass logging for sites without bandwidth service enabled
		 *
		 * @param int $siteid
		 * @return bool
		 */
		protected static function hasBandwidthService(int $siteid): bool
		{
			if (!isset(static::$bandwidthEnablementCache[$siteid])) {
				$oldex = \Error_Reporter::exception_upgrade(Error_Reporter::E_FATAL);
				try {
					$ctx = \Auth::context(null, "site${siteid}");
					static::$bandwidthEnablementCache[$siteid] = (bool)array_get(
						$ctx->getAccount()->cur,
						ServiceValidator::configize(Enabled::class),
						false
					);
				} catch (\apnscpException $e) {
					static::$bandwidthEnablementCache[$siteid] = false;
				} finally {
					\Error_Reporter::exception_upgrade($oldex);
				}
			}

			return static::$bandwidthEnablementCache[$siteid];
		}

		public function sort(): bool
		{
			return asort($this->_recs);
		}

		public function isBad(): bool
		{
			return $this->_bad;
		}
	}


	$pg = PostgreSQL::initialize();

	function get_extended_id($site_id, $bw_svc, $info = '')
	{
		global $pg;
		$q = $pg->query('SELECT ext_id
        		FROM bandwidth_extendedinfo
        		WHERE
        		site_id = ' . $site_id . '
        		AND
        		svc_id = ' . $bw_svc . '
        		AND
        		info = ' . pg_escape_literal($info));
		if ($q && !$q->num_rows()) {
			return false;
		}

		return (int)$q->fetch_object()->ext_id;
	}

	function add_extended_info($site_id, $bw_svc, $info = '')
	{
		global $pg;
		$q = $pg->query('INSERT INTO bandwidth_extendedinfo
    		(info, site_id, svc_id) VALUES
    		(' . pg_escape_literal($info) . ', ' . $site_id . ', ' . $bw_svc . ');');
		if ($q) {
			return get_extended_id($site_id, $bw_svc, $info);
		}

		return false;
	}

	function open_file($file)
	{
		$wrapper = get_wrapper($file);
		$flags = $wrapper ? 'rb' : 'r';
		if (!file_exists($file)) {
			// file doesn't exist, not rotated yet?
			return info("Log file `%s' does not exist yet", $file);
		}

		return fopen($wrapper . $file, $flags);
	}

	function log_ftp($file)
	{
		global $pg;
		$bucket = $leaders = $totals = $map = array();
		if (!file_exists($file)) {
			return error("error opening $file");
		}

		// Remove "/site" (5 chars), strip suffix .1
		//Fri Dec 18 21:37:08 2009 [pid 24635] [nb@dbushell.com] OK DOWNLOAD: Client "88.86.120.23", "/home/nb/public_html/lj/tesdt.html", 985 bytes, 4.06Kbyte/sec
		//Fri Dec 18 21:37:09 2009 [pid 24635] [nb@dbushell.com] OK UPLOAD: Client "88.86.120.23", "/home/nb/public_html/lj/tesdt.html", 990 bytes, 2.56Kbyte/sec
		$pattern = '/^(?<ts>\w{3} \w{3} (?:\s\d|\d\d) (?:\s\d|\d\d):\d\d:\d\d \d{4}) ' .
			'\[pid \d+\] \[(?<login>[^]]+)\] (?:OK|FAIL) (?<action>[^:]+): Client (?:"([^"]+)")?, ".+?(?=",)", (?<xfer>\d+) bytes/mS';
		if (!is_resource($fp = open_file($file))) {
			return;
		}

		while (false !== ($line = fgets($fp))) {
			if (!preg_match($pattern, $line, $matches)) {
				continue;
			}
			$login = $matches['login'];
			$xfer = $matches['xfer'];
			$ts = strtotime($matches['ts']);
			$action = $matches['action'];
			$in = $out = 0;
			if ($action === 'UPLOAD') {
				$in = $xfer;
			} else {
				$out = $xfer;
			}
			$sep = '@';
			if (strpos($login, '#') !== false) {
				$sep = '#';
			}

			// Skip non-virtual hosting entries
			if (strpos($login, '@') === false) {
				continue;
			}

			[$user, $domain] = explode($sep, $login);
			if (!array_key_exists($domain, $map)) {
				$site_id = \Auth::get_site_id_from_domain($domain);
				if (!$site_id) {
					$site_id = null;
					Error_Reporter::report(
						"Cannot verify $domain during domain -> site translation" .
						"\n" . $line);
				}
				$map[$domain] = $site_id;
			}
			$site_id = $map[$domain];
			if (!$site_id) {
				continue;
			}
			$key = $site_id . ':' . $user;
			if (!isset($bucket[$key])) {
				$bucket[$key] = new Bandwidth($site_id, FTP);
				$leaders[$key] = 0;
			}
			$bucket[$key]->add($ts, $in, $out);
			$leaders[$key] += $in + $out;
		}
		fclose($fp);
		arsort($leaders);

		foreach ($leaders as $ldr_login => $ldr_bw) {
			$account = $bucket[$ldr_login];
			[$site_id, $user] = explode(':', $ldr_login);
			$account->setExtendedName($user);
			$account->commit();
			$bucket[$ldr_login] = null;
		}

		return true;
	}

	function log_cp($file)
	{
		global $pg;
		if (!is_resource($fp = open_file($file))) {
			return;
		}
		$bucket = $maps = $unknown = array();
		while (false !== ($line = fgets($fp))) {
			$tokens = explode(' ', trim($line));
			if (count($tokens) != 8) {
				continue;
			}
			[$site_id, $user, $com, $id, $port, $ts, $in, $out] = $tokens;
			/**
			 * @todo $site_id == 0 on admin or reseller, log bw?
			 */
			if (!$id || !$site_id) {
				continue;
			}
			if ($site_id == '-' && !isset($maps[$id])) {
				if (!isset($unknown[$id])) {
					$unknown[$id] = array();
				}
				$unknown[$id][] = array(
					'ts'  => $ts,
					'in'  => $in,
					'out' => $out
				);
				continue;
			}
			if ($site_id == '-') {
				$site_id = $maps[$id]['site_id'];
				$user = $maps[$id]['user'];
				$com = $maps[$id]['com'];
			} else {
				$maps[$id] = array(
					'site_id' => $site_id,
					'user'    => $user,
					'com'     => $com
				);
			}

			$key = 'site' . $site_id;

			if (!isset($bucket[$key])) {
				$bucket[$key] = array();
			}
			if (!isset($bucket[$key][$user])) {
				$bucket[$key][$user] = array();
			}
			$user_prof = &$bucket[$key][$user];

			if (!isset($user_prof[$com])) {
				$bw_svc = constant($com);
				$user_prof[$com] = new Bandwidth($site_id, $bw_svc);
				if ($user && !$user_prof[$com]->setExtendedName($user)) {
					$user_prof[$com] = null;
					unset($user_prof[$com]);
					continue;
				}
				if (isset($unknown[$id])) {
					foreach ($unknown[$id] as $rec) {
						$user_prof[$com]->add($rec['ts'], $rec['in'], $rec['out']);
					}
				}
			}
			$user_prof[$com]->add($ts, $in, $out);
		}
		foreach ($bucket as $site) {
			foreach ($site as $user) {
				foreach ($user as $svc) {
					$svc->commit();
				}
			}
		}

		return;
	}

	function log_http($file)
	{
		global $pg;
		$bucket = $leaders = $totals = array();
		if (!is_resource($fp = open_file($file))) {
			return;
		}

		// Remove "/site" (5 chars), strip suffix .1
		while (false !== ($line = fgets($fp))) {
			$line = explode(' ', trim($line));
			[$site_id, $ts, $in, $out] = $line;
			if (!isset($line[4])) {
				if (false === ($ip = get_ip_address('site' . $site_id))) {
					continue;
				}
				$line[4] = $ip;
			}

			/**
			 * Skip Apache internal requests on SSL sites that
			 * do not log "siteid" environment variable.
			 * No bandwidth is consumed.
			 */
			if ($site_id == '-') {
				continue;
			}

			$host = $line[4];
			if (!strncmp($host, 'www.', 4)) {
				$host = substr($host, 4);
			}

			if (!isset($bucket[$host])) {
				$bucket[$host] = new Bandwidth($site_id, HTTP);
				$leaders[$host] = 0;
			}
			// large file xfer
			/*if ($in+$out > 1024*1024*1024) {
				print "Skipping entry...\n";
				mail("msaladna@apisnetworks.com","bw error","$host\n".file_get_contents('/var/log/bw/httpd/site'.$host));
				continue;
			}*/
			$bucket[$host]->add($ts, $in, $out);
			$leaders[$host] += $in + $out;
		}
		fclose($fp);
		arsort($leaders);
		foreach ($leaders as $ldr_host => $ldr_bw) {
			$account = $bucket[$ldr_host];
			if ($ldr_bw == 0 || $account->count() < 3 &&
				$ldr_bw < 1024) {
				continue;
			}
			$account->setExtendedName($ldr_host);
			$account->commit();
			$bucket[$ldr_host] = null;
		}

		return true;
	}

	function get_wrapper(&$file)
	{
		if (!file_exists($file) && file_exists($file . '.gz')) {
			$file .= '.gz';
		}
		if (substr($file, -3) === '.gz') {
			return 'compress.zlib://';
		}

		return '';
	}

	function log_mail($file)
	{
		/**
		 * Example input:
		 * Mar 13 21:40:58 assmule dovecot: POP3(al@alanhier.com): Disconnected: Logged out top=0/0, retr=0/0, del=0/9, size=60977, bytes=18/842
		 * Mar 13 21:41:05 assmule dovecot: IMAP(tomclackett@303cafe.com): Disconnected: Logged out bytes=56/870
		 */
		global $pg;
		$bucket = $leaders = $totals = $map = array();
		if (!is_resource($fp = open_file($file))) {
			return;
		}
		$regex = '!^(?<ts>\w{3} \d{2} \d{2}:\d{2}:\d{2}) ' .
			'[\w\s]+? dovecot: (?<proto>IMAP|imap|POP3|pop3)\((?<login>[^\)]+)\):.+' .
			'bytes=(?<in>\d+)/(?<out>\d+)$!U';
		$sep = '@';
		while (false !== ($line = fgets($fp))) {
			if (!strstr($line, 'dovecot:', 16)) {
				continue;
			}
			if (!preg_match($regex, $line, $match)) {
				continue;
			}

			if (!array_key_exists('in', $match)) {
				continue;
			}

			$in = $match['in'];
			$out = $match['out'];
			$login = $match['login'];
			$ts = strtotime($match['ts']);
			$proto = strtoupper($match['proto']);
			[$user, $domain] = explode($sep, $login);
			if (!array_key_exists($domain, $map)) {
				$site_id = Auth::get_driver()->get_site_id($domain);
				if (!$site_id) {
					$site_id = null;
					Error_Reporter::report(
						"Cannot verify $domain during domain -> site translation" .
						"\n" . $line);
				}
				$map[$domain] = $site_id;

			}
			$site_id = $map[$domain];
			if (!$site_id) {
				continue;
			}
			$key = $site_id . ':' . $user;
			if (!isset($bucket[$key])) {
				$bucket[$key] = new Bandwidth($site_id, constant($proto));
				$leaders[$key] = 0;
			}
			$bucket[$key]->add($ts, $in, $out);
			$leaders[$key] += $in + $out;
		}
		fclose($fp);
		arsort($leaders);

		foreach ($leaders as $ldr_login => $ldr_bw) {
			$account = $bucket[$ldr_login];
			[$site_id, $user] = explode(':', $ldr_login);
			$account->setExtendedName($user);
			$account->commit();
			$bucket[$ldr_login] = null;
		}
	}


	function get_ip_address($site)
	{
		static $cache = array();
		if (isset($cache[$site])) {
			return $cache[$site];
		}
		if (!\Auth::get_site_id_from_domain($site)) {
			return false;
		}
		$afi = cli\cmd(null, $site);
		if (!$afi) {
			return false;
		}
		$ip = $afi->site_ip_address();
		$cache[$site] = $ip;

		return $ip;
	}

	$argc = 1;
	$argv = $_SERVER['argv'];
	switch ($argv[1]) {
		case 'http':
			log_http('/var/log/bw/apache.1');
			break;
		case 'cp':
			log_cp('/var/log/bw/cp.1');
			break;
		case 'ftp':
			log_ftp('/var/log/vsftpd.log.1');
			break;
		case 'mail':
			log_mail('/var/log/maillog.1');

	}
	exit(0);
