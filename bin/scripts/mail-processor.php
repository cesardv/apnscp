#!/bin/env apnscp_php
<?php
    /**
     *
     */
    ini_set('include_path',ini_get('include_path') . PATH_SEPARATOR . '/usr/local/share/pear');
    ini_set('memory_limit', '256M');
    include('PEAR.php');
    include('Net/IPv4.php');
    error_reporting(E_ALL&~E_STRICT);

    class Processor {
        /** network senders are permitted within */
        const PERMITTED_NETWORK = '64.22.68.0/24';

        /** global sa database user via -u */
        const GLOBAL_USER = 'global';
        /** learn-as command for spam */
        const SPAM_LEARN = 'sa-learn --spam --mbox';
        /** learn-as command for ham */
        const HAM_LEARN = 'sa-learn --ham --mbox';

        const SPAM_USER = 'spam';
        const HAM_USER = 'ham';

        const EX_UNAVAILABLE = 69;
        const EX_TEMPFAIL = 75;

        /** @var string message */
        private $_msg;
        private $_msgRaw;

        // sender address
        private $_from;
        // permitted senders
        private $_permittedSenders = array();

        private $_headers;
        private $_networks;

        public static function process($stream = STDIN)
        {
            $dl = function_exists('dl');
            if (!extension_loaded('mbstring')) {
                if (!$dl || !dl('mbstring.so')) {
                    die("unable to use mailparse: missing mbstring");
                }
            } else if (!extension_loaded('mailparse')) {
                if (!$dl || !dl('mailparse.so')) {
                    die('unable to load mailparse extension');
                }
            }

            $processor = new Processor($stream);
            return $processor;
        }

        private function __construct($stream)
        {
            $this->_networks = array(self::PERMITTED_NETWORK);
            // prepare the stream
            if (is_resource($stream)) {
                $msg = array();
                while (!feof($stream)) {
                    $msg[] = fread($stream, 8192);
                }
                fclose($stream);
                $msg = join("", $msg);
            } else {
                $msg = file_get_contents($stream);
            }

            if (!$msg) {
                die("input source `$stream'' not readable");
            }
            $this->_msg = mailparse_msg_create();
            $this->_msgRaw = $msg;
            if (!mailparse_msg_parse($this->_msg, $msg)) {
                die("unable to parse message into components");
            }
        }

        public function __destruct()
        {
            if (is_resource($this->_msg)) {
                mailparse_msg_free($this->_msg);
            }
        }

        /**
         * Add a trusted network
         *
         * @param string $network network address in CIDR notation
         * @return bool
         */
        public function addNetwork($network)
        {
            $resp = Net_IPv4::parseAddress($network);
            if (!$resp || PEAR::isError($resp)) {
                die("invalid network `" . $network ."'");
            }
            $this->_networks[] = $network;
            return true;
        }

        public function handle() {
            $this->_setHeaders();
            $from = $this->_parseFrom();
            if (!$this->_isPermitted()) {
                echo "sender comes from unauthorized network";
                exit(self::EX_UNAVAILABLE);
            }

            $mode = $this->_getMode();
            if (!$mode) {
                echo "unknown learning mode specified";
                exit(self::EX_UNAVAILABLE);
            }
            $parts = $this->_findPart();
            if (!$parts) {
                echo "cannot find e-mail specimen - send e-mails as attachment";
                exit(self::EX_UNAVAILABLE);
            }

            $pipes = array();
            $desc = array(
                array('pipe', 'r'),
                array('pipe', 'w'),
                array('pipe', 'a')
            );

            $cmd = $mode .  ' -u ' . self::GLOBAL_USER;
            $proc = proc_open($cmd, $desc, $pipes);
            foreach ($parts as $msg) {
                fwrite($pipes[0], $msg);
            }
            fclose($pipes[0]);
            $out = trim(fread($pipes[1], 8192));
            $err = fread($pipes[2], 8192);
            $status = proc_close($proc);
            $msgcount = count($parts);
            $msg = <<<EOF
Thank you for your feedback! $msgcount message(s) received!
$out

                                                   /
                                             --   -Nh-
                                             +d:  sMMh-
                                        +y-  dMN//MMMMy
                                    -  -NMh-yMMMNNMMMMMddhyo+:-
                                    yh:hMMMNMMMMMMMMMMMMMMMMMNNds+-
                                   :NMNMMMMMMMMMMMMMMMMMMMMMMMMMMMNh+-
                                  -dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMms:
                                 /dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNs-
                               -yNMMMMMMMMMMMMMMmso/oNMMMMMMMMMMMMMMMMMMMm+
                              :dMMMMMMMMMMMMMMMMss:- dMMMMMMMMMMMMMMMMMMMMNo
                             :mMMMMMMMMMMMMMMMMMNhoohMMMMMMMMMMMMMMMMMMMNmMd
            -:-             -mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmdNd-/o
            :Nmdyo:         yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmhNd-:+
            /MMMMMNs-      -NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmymd-:/
            oMMMMMMMd:     oMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNds/sdh -:
            +MMMMMMMMN+-   yMMMMMMMMMMMMMMMMMMMMMMMMMMmh+:-   -:
            -hMMMMMMMMMd+::hMMMMMMMMMMMMMMMMMMMMMMMMMNy+:-    -/
             -sNMMMMMMMMMNmNMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmho/odN-:s: --
               :yMMMMMMMMNmmMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMdNMo+h+ ::
               sNMMMMMMMy/--dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNMMhymh-/s
              /MMMMMMMMy-   /NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNdMN-
              sMMMMMMMN-     oMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm-
              oMMMMMMN+       oNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd:
              -mMMMNd/         +mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh-
               /mds/-           :yNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm+
                -                 /hNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMms-
                                    /smMMMMMMMMMMMMMMMMMMMMMMMMMMMMNd+-
                                      -/smNMMMMMMMMMMMMMMMMMMMMNNho:
                                          :+shdNNNNMMMMNNNNmhyo/-
                                               --://////::-

                                           * CHOMP CHOMP CHOMP *
EOF;

            $headers = "From: Apis Mail Processor <processor@apisnetworks.com>";
            $sendmailargs = "-f processor@apisnetworks.com";
            if ($mode == self::SPAM_LEARN) {
                $subject = 'Spam';
            } else {
                $subject = 'Ham';
            }
            $subject .= ' Filter Feedback Report';
            mail($from, $subject, $msg, $headers, $sendmailargs);
            mail('matt+report@apisnetworks.com', 'Filter Feedback Given!!', join("\r\n\r\n", $parts), $headers, $sendmailargs);
            echo $out . "\n";
            exit(0);
        }

        public function permitSender($from)
        {
            if (is_array($from)) {
                $this->_permittedSenders = array_merge($this->_permittedSenders, $from);
            } else {
                $this->_permittedSenders[] = $from;
            }
            return $this;
        }

        private function _parseFrom()
        {
            $from = $this->_headers['from'];
            $from = mailparse_rfc822_parse_addresses($from);
            $from = $from[0]['address'];
            $this->_from = $from;
            return $from;
        }
        /**
         * Grab headers from message
         *
         * @return bool
         */
        private function _setHeaders()
        {
            $res = mailparse_msg_get_part($this->_msg, '1');
            $part = mailparse_msg_get_part_data($res);
            $headers = $part['headers'];
            $this->_headers = $headers;
            return true;
        }

        /**
         * Verify message originated within trusted network
         *
         * @return bool
         */
        private function _isPermitted()
        {
            $headers = $this->_headers;

            $from = $this->_from;
            if (in_array($from, $this->_permittedSenders)) {
                return true;
            }

            if (!isset($headers['received'])) {
                return false;
            }

            // a message may go through multiple hops to reach
            // final destination. only care about the final hop;
            // all other hops can be forged
            $rcvd = (array)$headers['received'];
            $rcvd = array_shift($rcvd);

            if (!preg_match('/((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))/', $rcvd, $network)) {
                return false;
            }
            $ip = $network[1];
            $valid = false;
            foreach ($this->_networks as $network) {
                if (Net_IPv4::ipInNetwork($ip, $network)) {
                    $valid = true;
                    break;
                }
            }
            if ($valid) { return true; }
            return false;
        }

        /**
         * Parse addressee for ham/spam mode
         *
         * @return bool|string mode or false on failure
         */
        private function _getMode()
        {
            $headers = $this->_headers;
            $rfc822 = mailparse_rfc822_parse_addresses($headers['to']);
            foreach ($rfc822 as $group) {
                $to = $group['address'];
                $user = substr($to, 0, strpos($to, '@'));
                if ($user == self::SPAM_USER) {
                    return self::SPAM_LEARN;
                } else if ($user == self::HAM_USER) {
                    return self::HAM_LEARN;
                }
            }

            return false;
        }

        private function _findPart() {
            $parts = mailparse_msg_get_structure($this->_msg);
            // pop off first part + body
            array_shift($parts);
            array_shift($parts);
            if (count($parts) < 1) {
                echo "missing attachments; forward e-mail as attachment, not inline";
                exit (self::EX_UNAVAILABLE);
            }

            // single inline forward without attachments
            if (count($parts) == 1 && $parts[0] == "1.2") {
                return (array)$this->_extractPart($parts[0]);
            }

            $skip = 0;
            $msgs = array();
            foreach ($parts as $part) {
                // first part is attachment presented as-is in e-mail,
                // second part is secoded attachment
                // prune as-is attachment
                $skip ^= 1;
                if ($skip) { continue; }

                if (!version_compare($part, '1.2', '>=')) {
                    echo "error: assertion failed, part < 1.1";

                    exit (self::EX_UNAVAILABLE);
                }
                // skip nested attachments
                if (!preg_match('/^\d+\.\d+\.\d+$/', $part)) {
                    continue;
                }
                $msg = $this->_extractPart($part);
                $msgs[] = $msg . "\n";

                //var_dump($part, mailparse_msg_get_part_data($handle),"==============================");
            }
            return $msgs;
        }

        private function _extractPart($part)
        {
            $handle = mailparse_msg_get_part($this->_msg, $part);

            $data = mailparse_msg_get_part_data($handle);
            $start = $data['starting-pos'];
            $end = $data['ending-pos'];
            $headers = $data['headers'];
            // get from address, stripping optional decoration
            if (isset($headers['from'])) {
                $from = mailparse_rfc822_parse_addresses($headers['from']);
                $from = array_pop($from);
                $from = $from['address'];
            } else {
                $from = 'null';
            }
            $date = isset($headers['date']) ? strtotime($headers['date']) : time();
            $fmtdate = strftime("%c", $date);
            $msg = "From " . $from . " " . $fmtdate . "\n"; //"Sun May 25 00:32:00 2008" . "\n"; //. $headers['date'] . "\r\n";
            $msg .= substr($this->_msgRaw, $start, $end-$start);
            return $msg;
        }

        public function fatal($msg)
        {
            echo $msg;
            exit (self::EX_UNAVAILABLE);
        }

        public function fail($msg)
        {
            echo $msg;
            exit (self::EX_TEMPFAIL);
        }
    }

    $process = Processor::process();
    $process->handle();

    exit(0);
