<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

	require_once dirname(__FILE__) . '/../TestFramework.php';

	class ProcessTest extends TestFramework
	{
		public function testPriority()
		{
			$basePrio = pcntl_getpriority();
			do {
				$prio = random_int(-20, 19);
			} while ($prio !== $basePrio && $prio === 0);
			$proc = new \Util_Process();
			$proc->addCallback(function ($cmd, \Util_Process $self) use ($basePrio) {
				$pid = posix_getpid();
				$stat = \Opcenter\Process::stat($pid);
				$this->assertSame($basePrio, $stat['nice']);
			}, 'open');
			$proc->setPriority($prio);

			$proc->addCallback(function($cmd, \Util_Process $self) use ($prio) {
				$pid = $self->getProcessStatus()['pid'];
				$path = '/proc/' . $pid . '/stat';
				$this->assertFileExists($path, 'Process created in table');
				// process space isn't updated yet with new process name yet
				$stat = \Opcenter\Process::stat($pid);
				$this->assertSame($prio, $stat['nice']);
			}, 'exec');
			$proc->run('true');
		}

		public function testSigchldExitCodes() {
			if (!CFG_DEBUG) {
				return $this->markTestSkipped('DEBUG not enabled');
			}

			$this->assertEquals(12, $this->test_sigchld_exit(12, true), 'Backend sigchld');
			$this->assertEquals(
				$this->test_sigchld_exit(29, true),
				$this->test_sigchld_exit(29, false),
				'sigchld consistency check'
			);

		}

	}



