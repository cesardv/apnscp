#!/usr/bin/env apnscp_php
<?php
    /*
     * This file is part of PHPUnit. Modified for apnscp.
     *
     * (c) Sebastian Bergmann <sebastian@phpunit.de>
     *
     * For the full copyright and license information, please view the LICENSE
     * file that was distributed with this source code.
     */
    define('INCLUDE_PATH', __DIR__ . '/../');
    require_once(INCLUDE_PATH . '/lib/apnscpcore.php');
	\apnscpSession::disable_session_header();
	\Error_Reporter::set_verbose(-1);
	require_once(INCLUDE_PATH . '/lib/CLI/cmd.php');
	require_once(INCLUDE_PATH . '/tests/TestHelpers.php');
	require_once(INCLUDE_PATH . '/tests/Definitions.php');
    \Auth::use_handler_by_name('CLI');
    \Auth::handle();

    if (version_compare('7.1.0', PHP_VERSION, '>')) {
        fwrite(
            STDERR,
            sprintf(
                'This version of PHPUnit is supported on PHP 7.1 and PHP 7.2.' . PHP_EOL .
                'You are using PHP %s (%s).' . PHP_EOL,
                PHP_VERSION,
                PHP_BINARY
            )
        );

        die(1);
    }

    if (!ini_get('date.timezone')) {
        ini_set('date.timezone', 'UTC');
    }

    foreach (array(
                 __DIR__ . '/../../autoload.php',
                 __DIR__ . '/../vendor/autoload.php',
                 __DIR__ . '/vendor/autoload.php'
             ) as $file) {
        if (file_exists($file)) {
            define('PHPUNIT_COMPOSER_INSTALL', $file);

            break;
        }
    }

    unset($file);

    if (!defined('PHPUNIT_COMPOSER_INSTALL')) {
        fwrite(
            STDERR,
            'You need to set up the project dependencies using Composer:' . PHP_EOL . PHP_EOL .
            '    composer install' . PHP_EOL . PHP_EOL .
            'You can learn all about Composer on https://getcomposer.org/.' . PHP_EOL
        );

        die(1);
    }

    require PHPUNIT_COMPOSER_INSTALL;

    $verbose = null;
    if (!getopt('v',['verbose'])) {
		$verbose = \Error_Reporter::set_verbose(0);
	}

    PHPUnit\TextUI\Command::main();

    if ($verbose) {
    	\Error_Reporter::set_verbose($verbose);
	}
