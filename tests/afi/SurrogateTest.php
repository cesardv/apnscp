<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2019
 */
require_once dirname(__DIR__, 1) . '/TestFramework.php';

class SurrogateTest extends TestFramework
{
	/**
	 * @var apnscpFunctionInterceptor
	 */
	protected $afi;

	public function testCall() {
		if (!apnscpFunctionInterceptor::surrogate_exists('test')) {
			$this->markTestIncomplete('No surrogate test module exists');
		}
		if (!is_debug()) {
			$this->markTestSkipped('DEBUG mode deactivated');
		}
		$swap = new class extends Aliases_Module {
			public function __construct()
			{
				parent::__construct();
				$this->exportedFunctions['test'] = PRIVILEGE_ALL;
			}

			public function test() {
				return $this->test_now();
			}
		};
		$this->getApnscpFunctionInterceptor()->swap('aliases', $swap);
		$this->assertNotContains('test', $this->getApnscpFunctionInterceptor()->get_loaded_modules());
		$this->getApnscpFunctionInterceptor()->aliases_test();
		$this->assertTrue($this->getApnscpFunctionInterceptor()->get_loaded_modules()['test']['surrogate']);
	}
}

