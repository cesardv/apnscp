<?php
    require_once dirname(__DIR__, 1) . '/TestFramework.php';

    use cli\{parse,merge};

    class ArgumentParsingTest extends TestFramework
    {
        protected $filename;

        public function testParser()
        {
            $args = ['foo' => 'bar', ['baz' => 'qex'], true];
	        $collapsed = preg_split('//', \Opcenter\CliParser::collapse($args));
	        $this->assertSame($args, \Opcenter\CliParser::parseArgs($collapsed));
        }

        public function testIpv6()
		{
			$args = ["fe80::9b96:244b:985b:b934"];
			$collapsed = preg_split('//', \Opcenter\CliParser::collapse($args));
			$this->assertSame($args, \Opcenter\CliParser::parseArgs($collapsed));
		}

        public function testNonWellformedArray()
		{
			$args = ['foo]bar'];
			$this->assertSame($args[0], \Opcenter\CliParser::parseArgs($args));
		}

		public function testLookaheadNestedArray()
		{
			$sample = '[test:[bar:12345]]';
			$this->assertSame([['test' => ['bar' => 12345]]], array_map('\Opcenter\CliParser::parseArgs', [$sample]));
		}

        public function testHandleSpecial()
        {
            $this->assertSame([false, "false"], array_map('\Opcenter\CliParser::parseArgs', ["false", '"false"']));
            $this->assertSame([null, "null"], array_map('\Opcenter\CliParser::parseArgs', ["null", '"null"']));
        }

        public function testFloat()
		{
			$this->assertSame([7.5], array_map('\Opcenter\CliParser::parseArgs', ['7.5']));
			$this->assertSame(['7-5'], array_map('\Opcenter\CliParser::parseArgs', ['7-5']));
		}

        public function testColon() {
        	$sample = 'test:12345';
        	$this->assertSame([$sample], array_map('\Opcenter\CliParser::parseArgs', [$sample]));
		}

		public function testSpacedArray() {
			$sample = '[test: 12345]';
			$this->assertSame([['test' => 12345]], array_map('\Opcenter\CliParser::parseArgs', [$sample]));
			$sample ='"foo bar"';
			$this->assertSame([trim($sample, '"')], array_map('\Opcenter\CliParser::parseArgs', [$sample]));
		}

		public function testSpacedString() {
        	$sample = "foo bar";
        	$this->assertSame(['foo bar'], array_map('\Opcenter\CliParser::parseArgs', [$sample]));
		}
        public function testCommas()
        {
        	// -c auth,tpasswd='"foo,bar,baz"' -> foo,bar,baz
	        // -c auth,tpasswd='foo,bar,baz' => foo,bar,baz
        	$test = '"foo,bar,baz"';
	        $this->assertSame(trim($test,'"'), \Opcenter\CliParser::parseArgs($test));
	        $test = 'foo,bar,baz';
	        $this->assertSame($test.'', \Opcenter\CliParser::parseArgs($test));
	        $test = '[1,2,3]';
	        $this->assertSame([1,2,3], \Opcenter\CliParser::parseArgs($test));
	        $test = '["1","2,3",4]';
	        $this->assertSame(['1', '2,3', 4], \Opcenter\CliParser::parseArgs($test));
        }

        public function testWhitespaceList() {
        	$sample = '[12, 34, foo]';
        	$this->assertSame([[12, 34, "foo"]], array_map('\Opcenter\CliParser::parseArgs', [$sample]));
		}
    }

