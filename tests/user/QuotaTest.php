<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class QuotaTest extends TestFramework
	{

		public function testQuotaFormat()
		{
			$auth = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
			$afi = apnscpFunctionInterceptor::factory($auth);
			if (!$afi->common_get_service_value('diskquota', 'enabled')) {
				$editor = new \Util_Account_Editor($auth->getAccount(), $auth);
				$editor->setConfig('diskquota', 'enabled', true)->
					$editor->setConfig('diskquota', 'quota', 10)->
					$editor->setConfig('diskquota', 'units', 'gb');
				$this->asserTrue($editor->edit());
			}
			$quota = $afi->user_get_quota($auth->username);
			$this->assertArrayHasKey('qhard', $quota);
			$this->assertArrayHasKey($auth->username, $afi->user_get_quota([$auth->username]), 'Quota formatted singularly');
			$this->assertCount(2, $afi->user_get_quota([$auth->username, \Web_Module::WEB_USERNAME]), 'Quotas formatted as array');
			$this->assertGreaterThan(0, $quota['qhard'], 'User has quota set');

		}
	}

