<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class NextcloudTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;
		const INSTALL_VERSION = '19.0.0';

		public function testVersionFetch()
		{
			$afi = \apnscpFunctionInterceptor::init();
			$versions = $afi->nextcloud_get_versions();
			$this->assertNotEmpty($versions, 'Version check succeeded');
			$this->assertContains('19.0.1', $versions, '19.0.1 in version index');
			$this->assertGreaterThan(array_search('18.0.1', $versions), array_search('18.0.9', $versions),
				'Versions are ordered');
		}

		public function testInstallUpgrade() {
			$account = \Opcenter\Account\Ephemeral::create(['cgroup.enabled' => 0]);
			$afi = $account->getApnscpFunctionInterceptor();
			$domain = $account->getContext()->domain;
			$version = \Opcenter\Versioning::asMinor(self::INSTALL_VERSION);
			$maximal = \Opcenter\Versioning::maxVersion($afi->nextcloud_get_versions(), $version);

			// Laravel always installs maximal minor...
			$this->assertTrue(
				$afi->nextcloud_install(
					$domain,
					'',
					[
						'version' => self::INSTALL_VERSION,
						'notify'  => false,
						'ssl'     => false,
						'email'   => null,
						'verlock' => 'minor'
					]
				)
			);
			$this->assertEquals(self::INSTALL_VERSION, $afi->nextcloud_get_version($domain));

			$this->assertTrue(
				$afi->nextcloud_update_all($domain, ''),
				'Nextcloud update succeeded'
			);

			$this->assertEquals($maximal, $afi->nextcloud_get_version($domain), 'Correct Nextcloud version is installed');
		}
	}

