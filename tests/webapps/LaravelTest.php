<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class LaravelTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;
		const INSTALL_VERSION = '6.11.0';

		public function testVersionFetch()
		{
			$afi = \apnscpFunctionInterceptor::init();
			$versions = $afi->laravel_get_versions();
			$this->assertNotEmpty($versions, 'Version check succeeded');
			$this->assertContains('5.4.9', $versions, '5.4.9 in version index');
			$this->assertGreaterThan(array_search('5.4.9', $versions), array_search('5.4.15', $versions),
				'Versions are ordered');
		}

		public function testInstall() {
			$account = \Opcenter\Account\Ephemeral::create(['cgroup.enabled' => 0]);
			$afi = $account->getApnscpFunctionInterceptor();
			$domain = $account->getContext()->domain;
			// 6+ onward always update minors
			$version = \Opcenter\Versioning::asMajor(self::INSTALL_VERSION);
			$maximal = \Opcenter\Versioning::maxVersion($afi->laravel_get_versions(), $version);

			// Laravel always installs maximal minor...
			$this->assertTrue(
				$afi->laravel_install(
					$domain,
					'',
					[
						'version' => self::INSTALL_VERSION,
						'notify'  => false,
						'ssl'     => false,
						'email'   => null,
						'verlock' => 'major'
					]
				)
			);

			$this->assertEquals($maximal, $afi->laravel_get_version($domain), 'Correct Laravel version is installed');

		}
	}

