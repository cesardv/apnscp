<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class GhostTest extends TestFramework
	{
		const VERSION = '3.12.0';
		const VERSION_NEXT = '3.12.1';
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		public function testVersionFetch()
		{
			$afi = \apnscpFunctionInterceptor::init();
			$versions = $afi->ghost_get_versions();
			$this->assertNotEmpty($versions, 'Version check succeeded');
			$this->assertContains(self::VERSION, $versions, self::VERSION . ' in version index');
			$this->assertGreaterThan(array_search(self::VERSION, $versions), array_search(self::VERSION_NEXT, $versions), 'Versions are ordered');
		}
	}

