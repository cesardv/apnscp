@if (HTTPD_PAGESPEED_PERSITE)
<IfModule pagespeed_module>
	{{-- chmod 5750 --}}
ModPagespeedFileCachePath "{{ $svc->getAccountRoot() }}/var/cache/pagespeed"
</IfModule>
@endif