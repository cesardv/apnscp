# PHP policy template
# vim:et ts=2 sw=2 sts=2 syntax=yaml filetype=yaml
---
global:
  # defaults to system version
  # Bootstrapper value: system_php_version
  version:
  # Max PHP-FPM workers
  # PHP-FPM value: pm.max_children
  workers: {{ max(NPROC+2, 5) }}
  # Max idle duration before a PHP-FPM worker quits
  # PHP-FPM value: pm.process_idle_timeout
  idle_timeout: 60s
  # Max threads dedicated per server for queuing
  # Apache value: <Proxy max=n>
  threads: 3
  # Max backlog
  # PHP-FPM value: listen.backlog
  backlog: 30
  # Connection timeout
  # Apache value: <Proxy acquire=n>
  connect: 5s
  # All php_admin settings - cannot be overrode
  php_settings:
    # Maximum memory *per* PHP-FPM worker (keep low!)
    # PHP value: memory_limit
    memory_limit: {{ (int)min(\Opcenter\System\Memory::stats()['memtotal']/1024*0.15,384) }}M
pools:
  # Per-pool configuration is not supported yet!
  # "mydomain.com":
  #   workers: 5
  #   version: null or specific version
# List of versions to disallow
blacklist:
  # - 5.6
  # - 7.0
# List of versions to allow
# Omit to allow all versions
whitelist: