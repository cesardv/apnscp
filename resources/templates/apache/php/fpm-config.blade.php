[global]
pid = {{ $config->getPidPath(true) }}
daemonize = no
log_level = error
error_log = {{ $config->getLog() }}
emergency_restart_threshold = 3
emergency_restart_interval = 1m
process_control_timeout = 10s

[{{ $config->getName() }}]
listen = {{  $config->getSocketPath() }}
; Set higher to control pending connections
; Discretion advised: each pending connection occupies a slot in Apache.
; With ondemand this value cannot fall below 511.
; Max connections floor between <Proxy max=n> in Apache + listen.backlog
listen.backlog = {{ (int)$config->getPolicy()->get($config->getName(), 'backlog') }}
listen.mode=0600
; Maximum duration a script may run
request_terminate_timeout = 300s

; Choose how the process manager will control the number of child processes.
; Override for high throughput environments
pm = ondemand
pm.max_children = {{ (int)$config->getPolicy()->get($config->getName(), 'workers') }}
pm.start_servers = 2
pm.min_spare_servers = 0
pm.max_spare_servers = 3
pm.process_idle_timeout = {{ $config->getPolicy()->get($config->getName(), 'idle_timeout') }}
pm.max_requests = 2048

; Pass environment variables
@isset($svc)
env[HOSTNAME] = {{ $svc->getServiceValue('siteinfo', 'domain') }}
@endisset
env[PATH] = /usr/local/bin:/usr/bin:/bin
env[TMP] = /tmp
env[TMPDIR] = /tmp
env[TEMP] = /tmp

; Create a file named config/custom/resources/templates/apache/php/partials/fpm-config-custom.blade.php
; to add any additional directives after this line. These will override any php_admin_value settings
; within here or in a policy map.
@includeIf('apache.php.partials.fpm-config-custom')

; Values imported from PHP-FPM policy map take precedence
@foreach ($config->getPolicy()->getPhpAdminSettings($config->getName()) as $var => $val)
php_admin_value[{{ $var }}] = {{ $val }}
@endforeach
php_admin_value[disable_functions] = syslog
php_admin_value[session.save_path] = /tmp
php_admin_value[error_log] = {{ $config->getLog() }}
php_admin_value[open_basedir] =
php_admin_value[opcache.restrict_api] =

; Helpful in tracking down malicious mailings
; All mail endpoints should require authorization
php_admin_flag[mail.add_x_header] = On

@isset($svc)
php_value[sendmail_path] = "/usr/sbin/sendmail -t -i -f postmaster@@svc('siteinfo','domain')"
php_value[mail.force_extra_parameters] = "-f postmaster@@svc('siteinfo','domain')"
@endisset

; User overrides
include=/etc/php-fpm.d/*.conf
