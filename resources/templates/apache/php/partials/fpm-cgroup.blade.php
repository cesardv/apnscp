####################
# cgroup accounting
#
# See https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html
#
####################
# Prevent reallocation
Delegate=yes
CPUAccounting=yes
MemoryAccounting=yes
BlockIOAccounting=yes
TasksAccounting=yes