# via {{ $templatePath }}
[Unit]
Description=PHP worker for {!! $config->getGroup() !!}
After=network.target php-fpm.service
PartOf=php-fpm.service
RequiresMountsFor={!! $config->getRoot() !!}

[Service]
ExecStop=/bin/true
Type=oneshot
RemainAfterExit=yes

[Install]
WantedBy=php-fpm.service
