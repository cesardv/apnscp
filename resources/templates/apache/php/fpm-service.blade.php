[Unit]
Description=PHP worker for {!! $config->getGroup() !!} - {!! $config->getName() !!}
After=mariadb.service postgresql.service
After=network.target {{ $config->getSocketServiceName() }} cgconfig.service
PartOf={{ $config->getSocketServiceName() }}
Requires={{ $config->getSocketServiceName() }}
RequiresMountsFor={!! $config->getRoot() !!} {!! FILESYSTEM_SHARED !!}/systemd

[Service]
User={!! $config->getSysUser() !!}
Group={!! $config->getSysGroup() !!}
PIDFile={!! $config->getPidPath() !!}
Environment="FPM_SOCKETS={{ $config->getSocketPath() }}=3"
@if ($config->hasResourceAccounting())
PermissionsStartOnly=true
{{-- @TODO Move to slice.XXX layout --}}
ExecStartPost=/bin/sh -c 'for i in {{ \Opcenter\System\Cgroup::CGROUP_HOME }}/{{ str_replace('\\', '\\\\', \Opcenter\System\Cgroup::controllerPathPattern()) }}/{{ $config->getResourceManager()->getGroup() }}/tasks ; do echo $MAINPID > $i ; done'
@endif
ExecStart={!! $config->getBinary() !!} {!! implode(' ', $config->getArguments()) !!}
ExecReload=/bin/kill -USR2 $MAINPID

Type=notify
WatchdogSec=30s
Restart=on-failure
StartLimitInterval=15s
StartLimitBurst=3
StartLimitAction=none
KillMode=control-group
TimeoutStopSec=30s

# Jail all requests to account root
RootDirectory={{ $config->getRoot() }}
# Allow ExecStartPre= access outside as root if needed
RootDirectoryStartOnly=yes
SupplementaryGroups={{ APACHE_GID }} @if (version_compare(\Opcenter\Versioning::asMinor(os_version()), '8.0', '>=')) postdrop @endif

# Required for dropping privileges and running as a different user
# CAP_SETGID is used by postdrop to send mail
CapabilityBoundingSet=CAP_SETUID CAP_SETGID

# Set up a new file system namespace and mounts private /tmp and /var/tmp directories
# so this service cannot access the global directories and other processes cannot
# access this service's directories.
PrivateTmp=true

# Sets up a new /dev namespace for the executed processes and only adds API pseudo devices
# such as /dev/null, /dev/zero or /dev/random (as well as the pseudo TTY subsystem) to it,
# but no physical devices such as /dev/sda.
PrivateDevices=true

# Mounts the /usr, /boot, and /etc directories read-only for processes invoked by this unit.
ProtectSystem=full

# Allow setgid/setuid syscall for postdrop mail usage
NoNewPrivileges=@if (version_compare(\Opcenter\Versioning::asMinor(os_version()), '8.0', '>=')) yes @else no @endif

# Restricts the set of socket address families accessible to the processes of this unit.
# Protects against vulnerabilities such as CVE-2016-8655
RestrictAddressFamilies=AF_INET AF_INET6 AF_UNIX @if (version_compare(\Opcenter\Versioning::asMinor(os_version()), '8.0', '>=')) AF_NETLINK @endif

# Stopgap to limit runaway scripts, logs, or whatever
LimitFSIZE=4G
OOMScoreAdjust=500
@includeWhen($config->hasResourceAccounting(), 'apache.php.partials.fpm-cgroup')

[Install]
