# via {{ $templatePath }}
[Unit]
Description=PHP worker master control
After=network.target

[Service]
ExecStart=/bin/true
Type=oneshot
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
