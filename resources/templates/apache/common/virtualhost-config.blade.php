	{{-- common HTTP configuration shared by non-HTTP and HTTPs components --}}
	ServerName @svc("apache","webserver")

	ServerAlias @svc("siteinfo","domain") *.@svc("siteinfo","domain")

	DocumentRoot {!! $svc->getAccountRoot() . \a23r::get_autoload_class_from_module('web')::MAIN_DOC_ROOT !!}

	@foreach ($svc->getServiceValue('aliases','aliases',[]) as $alias)
	ServerAlias {{ $alias }} *.{{ $alias }}
	@endforeach

	suexecUserGroup @svc("siteinfo","admin") @svc("siteinfo","admin")

	SetEnvIf Request_Method ^ SITE_ID={{ $svc->getSiteId() }} SITE_ROOT={!! $svc->getAccountRoot() !!}
	RewriteEngine On
	RewriteMap DOMAIN_MAP "dbm={{ strtolower(\Opcenter\Http\Apache\Map::DEFAULT_FORMAT) }}:{{ \Opcenter\Http\Apache::CONFIG_PATH }}/domains/{{ $svc->getSite() }}"
	RewriteOptions Inherit

	@includeWhen(!$svc->getServiceValue('apache', 'jail'), 'php.partials.isapi-directives')
	@includeWhen($svc->getServiceValue('apache', 'jail'), 'php.partials.fpm-directives')

	Include {{ \Opcenter\Http\Apache::CONFIG_PATH }}/{{ $svc->getSite() }}

