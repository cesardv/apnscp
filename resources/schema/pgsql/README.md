# Regenerating database

Drop the database, then run apnscp/initialize-db role to recreate the database. 

THIS WILL RESULT IN DATA LOSS. Do not do this unless you know what you're doing.

```bash
sudo -u postgres dropdb appldb
upcp -sb apnscp/initialize-db
systemctl restart apiscp
./artisan migrate --dbonly --force
```