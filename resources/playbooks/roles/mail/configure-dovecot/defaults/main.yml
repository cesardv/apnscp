---
# INTERNAL
dovecot_shared_root: "{{ apnscp_shared_root }}/dovecot"
# Enable use of TLS v1.0/TLS v1.1
dovecot_insecure_ssl: "{{ mail_insecure_ssl }}"
# INTERNAL
dovecot_virtual_rundir: "{{ apnscp_filesystem_template }}/siteinfo/var/run/dovecot"
# Per-process limit to prevent runaway OOM conditions, increase for larger mailboxes
dovecot_per_process_limit: 512M
# INTERNAL
dovecot_relocatable_paths:
  - dest: /usr/lib64/dovecot
    src: "{{ dovecot_shared_root }}/lib64"
  - dest: /usr/libexec/dovecot
    src: "{{ dovecot_shared_root }}/libexec"
# System Dovecot group
dovecot_group: dovecot
# INTERNAL
dovecot_conflicting_conf:
  # Overrides userdb/passdb
  - 10-auth.conf
  # Mailbox namespace inbox=yes
  - 10-mail.conf
  # Conflicts with SSL
  - 10-ssl.conf
  # mailbox namespacing
  - 15-mailboxes.conf
dovecot_home: /etc/dovecot
config_file: "{{ dovecot_home }}/apnscp.conf"
postfix_auth_socket: /var/spool/postfix/private/auth

# Enable transparent reading of zlib compressed messages
# bzip, gzip, lz4, and xz supported
dovecot_enable_zlib: true
# Affects storage of mail generated in Dovecot, e.g. sent messages
# maildrop can be configured to compress prior to delivery with xfilter
# xfilter "gzip -qc"
# Enabling zlib will affect semantics of terminal mail interaction
# - grep => zgrep
# - rspamc => zcat | rspamc
# - sa-learn => zcat | sa-learn
dovecot_enable_zlib_storage: false
dovecot_zlib_compression_handler: gz
dovecot_zlib_compression_level: 6

dovecot_conf_dir: "{{ dovecot_home }}/conf.d"
# In secure_mode each IMAP connection creates a separate
# process. SSL/TLS also creates a separate process
dovecot_secure_mode: "{{ not (has_low_memory | bool) }}"
# Enabling disallows remote login without SSL. All logins must
# be sent over plain-text to work with PAM
dovecot_strict_ssl: false
# Enable stats module. Requires world-writeable socket.
dovecot_enable_stats: false

# INTERNAL
dovecot_bindir: "/usr/libexec/dovecot"

# Spam auto-learn
# Sieve for spam filtering
dovecot_sieve_dir: "/usr/lib64/dovecot/sieve"
# INTERNAL
dovecot_sieve_bindir: "{{ dovecot_bindir }}/sieve"

# Root folder is prefixed with "INBOX"
dovecot_imap_root: INBOX.
# Maximum run-queue depth until drag'n'drop learning is suspended.
# Inspired by Postfix's "stress" factor of 2x. 1-minute loads above this
# value immediately exit without learning.
dovecot_learn_load_limit: "{{ ansible_processor_count * 2 }}"
# Messages pulled out of here are learned as Ham
dovecot_learn_ham_folder: "{{ dovecot_imap_root }}Spam"
# Messages sent here are learned as Spam
dovecot_learn_spam_folder: "{{ dovecot_learn_ham_folder }}"
dovecot_mail_plugins: "expire quota acl cgroup fts fts_lucene {{ dovecot_enable_zlib | ternary('zlib', None) }} {{ dovecot_enable_stats | ternary('stats', None) }}"
dovecot_imap_plugins: "$mail_plugins imap_quota snarf imap_acl imap_sieve {{ dovecot_enable_stats | ternary('imap_stats', None) }}"

# Regex that matches before the subject line
dovecot_spam_rewrite_regex: '^\\[SPAM\\] \\([0-9\\.]+\\) (.*)$'
# Remove "[SPAM] (SCORE)" from messages dragged from spam folder
dovecot_strip_spam_rewrite: true

# Suspend compatible clients that send IDLE command until activity
dovecot_use_hibernate: true

# Duration for a connection to wait before freezing into hibernation
dovecot_hibernation_timeout: 10s

# Set of ranges for quota warnings. To disable quota warnings, leave this field empty
# Don't set value to 100 as such messages cannot be delivered
dovecot_quota_warnings:
#  - 85
#  - 95
