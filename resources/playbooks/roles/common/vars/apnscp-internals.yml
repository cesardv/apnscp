---
####################################
#     Don't change below here      #
####################################
DEV_TAG: ">>> DEVELOPMENT PLAY ONLY - REMOVE BEFORE FLIGHT <<<"
apnscp_release_repo: https://gitlab.com/apisnetworks/apnscp.git
apnscp_last_run_vars: /root/apnscp-vars-runtime.yml
apnscp_bootstrapper_log: /root/apnscp-bootstrapper.log
apnscp_user_defaults: /root/apnscp-vars.yml
# SRS - reformatting forwarded email
postsrsd_enabled: "{{ mail_enabled | bool }}"
# haproxy terminates SNI SSL for mail
haproxy_enabled: "{{ (mail_enabled | bool) and not (has_low_memory | bool) }}"
# Restrict last/lastlog usage
wtmp_limit_snooping: true
# Enable apnscp testing repo
apnscp_testing_repo: "{{ apnscp_debug }}"
# Location of apnscp
apnscp_root: /usr/local/apnscp
# Account root
apnscp_account_root: /home/virtual
# Location of FST
apnscp_filesystem_template: "{{ apnscp_account_root }}/FILESYSTEMTEMPLATE"
# Filelists provide additional resources for service installation
apnscp_filelists: "{{apnscp_root}}/storage/opcenter/filelists"
# Default apnscp user - not tested
apnscp_system_user: apnscp
# apnscp PHP version
apnscp_php_version: 7.4
# apnscp module API, ZTS is with event MPM. Must match apnscp_php_version build
apnscp_php_module_api: 20190902-zts
# Location of apnscp r/w shared path
apnscp_shared_root: /.socket
# apnscp repo + CA data
apnscp_ca_rpm: http://yum.apnscp.com/apnscp-latest.rpm
# Platform version. Changes internally with each new release
apnscp_platform_version: 8.0
# Any-version support. Set to true to implicitly enable all AV languages below
anyversion_enabled: '{{ passenger_enabled }}'
# By default this is tied to Passenger's presence
# Or individually toggle languages below. May break Passenger!
anyversion_python: '{{ anyversion_enabled }}'
anyversion_ruby: '{{ anyversion_enabled }}'
anyversion_node: '{{ anyversion_enabled }}'
anyversion_go: '{{ anyversion_enabled }}'
# apnscp fixes
nvm_version: master
pyenv_version: 1.2.4
pyenv_virtenv_version: 1.1.5
rbenv_version: 1.1.2
goenv_version: 1.14.0
# Default MySQL version
mariadb_version: 10.4
# Default PostgreSQL version
pgsql_version: 12
# Others may be installed via "rbenv install", this is used primarily for
# bootstrapping Passenger. system Ruby shipped with RHEL 7 (2.0) is too
# old and too broken to be of use
sys_ruby_version: "{{ (anyversion_ruby or passenger_enabled) | ternary('2.5.8','system') }}"
rbenv_usergems_version: ad2fd08
# 4.x kernel from EL. Newer technology, potentially less stable
prefer_experimental_kernel: false
# Enable kernel crash dumps. Unlikely to be encountered in production
kdump_enabled: "{{ not (prefer_experimental_kernel | bool) and apnscp_debug }}"
# Enable remote FTP connections. FTP will always be enable for Wordpress
# to operate. If off, WebDAV, sftp, ssh (rsync), and git are the only
# means to manage files
ftp_enabled: "{{ not has_dns_only }}"
# Maximum PASV port when user_daemons disabled. Supports up to 10 concurrent transfers
pasv_max_port: 40010
# Enable outgoing filtering with rspamd - experimental
rspamd_enabled: "{{ spamfilter == 'rspamd' and mail_enabled | bool }}"
# Use SpamAssassin or rspamd for spam filtering. rspamd is experimental
spamassassin_enabled: '{{ (spamfilter == "spamassassin") and (mail_enabled | bool) }}'
# Enable installing MongoDB
mongodb_enabled: false
# apnscp is part of a multi-server environment
# requires extra configuration
# see https://github.com/apisnetworks/cp-proxy
data_center_mode: false
# Allow remote database connections
allow_remote_db: "{{ data_center_mode }}"
# Per-class overrides
pgsql_remote_connections: "{{ allow_remote_db }}"
mysql_remote_connections: "{{ allow_remote_db }}"
# Location for per-user mail
maildir_location: ~/Mail
# Perform an account creation validation at the end of this playbook
# Allows for consistency checks
apnscp_verify_account: true
# Run Bootstrapper after update (upcp --auto)
apnscp_update_bootstrapper: "{{ apnscp_nightly_update }}"
# Perform periodic FLARE checks
apnscp_flare_check: "{{ apnscp_nightly_update }}"
# URI to query for FLARE checks, leave empty to use default endpoint
apnscp_flare_endpoint:
# Program used to update ApisCP
apnscp_build_helper: "{{ apnscp_root }}/build/upcp.sh"
# apnscp hostname used for email/LE setup
# ${HOSTNAME} will use ansible_nodename or your system hostname
apnscp_hostname: "${HOSTNAME}"
# Enable SELinux
# true is absolutely guaranteed to break your system
# debug places selinux in permissive mode
# off disables selinux alltogether
# off is recommended - a reboot is required
# Only enable if you plan on helping out!
use_selinux: false
# Backwards compatibility
clamav_enable: "{{ clamav_enabled | default(true) }}"
# CI via Docker
dockerized: false

# Virus scanning group used by ClamAV
clamav_scan_group: virusgroup
# Change sshd port - note that ssh is monitored for
# brute-force and attackers blocked following
# fail2ban/configure-jails settings. Change at your own risk.
sshd_port: 22
# Maximum number of retries for components that may fail due
# to flaky network settings
network_max_retries: 2
compile_max_jobs: "{{ (not has_low_memory) | ternary(ansible_processor_vcpus + 1, 1) }}"
# Ansible lockfile
bootstrapper_acquire_lock: true
bootstrapper_lock_file: "{{ apnscp_root }}/storage/run/.ansible.lock"
bootstrapper_lock_max_wait: 900
# Max time to wait on lockfile release
yum_lock_max_wait: 300

# IPv4/6 family detection
has_ipv4: "{{ (ansible_default_ipv4 | length) > 0 }}"
has_ipv6: "{{ (ansible_default_ipv6 | length) > 0 }}"

# Robust nameservers
# Reference performance: https://www.dnsperf.com/#!dns-resolvers
dns_robust_nameservers: '{{ has_ipv4 | ternary(["1.0.0.1","1.1.1.1"], ["2606:4700:4700::1111","2606:4700:4700::1001"]) }}'

# Apply pending migrations prior to certain stages
# Useful when using a prebuilt image
# Possible values: db, platform (unused), or true
apply_pending_migrations: "{{ lookup('env', 'APNSCP_APPLY_PENDING_MIGRATIONS') | default(False, true) }}"

# Use Mitogen to accelerate installation
mitogen_acceleration: true

# Enable TLS v1.0/TLS v1.1
mail_insecure_ssl: false

# OS has EFI/UEFI BIOS
# Jinja2 shipped with CentOS 7 doesn't contain "equalto" test
has_uefi: "{{ ansible_mounts | selectattr('fstype', 'contains', 'efivarfs') | list | length > 0 }}"

# DNS-only mode
dns_default_provider: "{{ not has_dns_only | ternary('builtin', 'powerdns') }}"
# Default to null in DNS-only mode
mail_default_provider: "{{ not has_dns_only | ternary('builtin', 'null') }}"

# Compilation temp directory, /tmp is memory-based, /var/tmp disk-based
build_tmpdir: "{{ (ansible_memory_mb['nocache']['free'] > 512) | ternary('/tmp', '/var/tmp') }}"

# Group with /proc visibility used in CentOS 7
procfs_visgroup: procviz

# Enable SCL support. Required for Remi (package-based) multiPHP and PostGIS
has_scl: true

# IP address of upstream proxy host. Set if panel is accessed via a cp-proxy instance
# or if panel is default target of a cp-proxy instance.
cp_proxy_ip: ""

# Forcefully override several configurations.
force: false
