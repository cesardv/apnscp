---
- name: Autopopulate IPs
  set_fact:
    "{{ '__my' + item.key }}": "{{ lookup('url', item.value, wantlist=True, errors='ignore') | default([], true) | difference(hostvars[inventory_hostname]['ansible_all_' + item.key + '_addresses']) | join('')}}"
  loop_control:
    label: "{{ item.key }} Looking up IP via {{ item.value }}"
  with_dict:
    ipv4: http://myip4.apnscp.com
    ipv6: http://myip6.apnscp.com
- name: Template initial files
  copy:
    src: "{{ item }}"
    dest: "{{ apnscp_root }}/config/{{ item | basename | regex_replace('\\.dist$', '') }}"
    force: no
    remote_src: yes
  loop_control:
    label: "Copying {{ item | basename }}"
  with_fileglob:
    - "{{ apnscp_root }}/config/*.dist"
- name: Verify apnscp bootstrapper completed
  stat: path={{ apnscp_root }}/resources/apnscp.ca
  register: s
- fail:
    msg: "apnscp bootstrapper isn't completed properly"
  when: not s.stat.exists

- template: src=templates/apnscp.init.j2 dest=/etc/systemd/user/apnscp.init mode=0755 backup=no
- template: src=templates/apnscp.service.j2 dest=/etc/systemd/system/apnscp.service mode=0644
  register: copied
- template: src=templates/apnscp.sysconf.j2 dest=/etc/sysconfig/apnscp force=no backup=yes
- systemd: daemon_reload=yes enabled=yes name=apnscp
  when: copied.changed
- include_tasks: set-platform-version.yml
  vars:
    version: "{{ apnscp_platform_version }}"
- name: Add RPM macros
  template: src=templates/rpm-macros.j2 dest=/etc/rpm/macros.apnscp
- name: Create shared filesystem slice {{ apnscp_shared_root }}
  file:
    path: "{{ apnscp_shared_root }}"
    mode: 0711
    state: directory
  register: dir_created

- name: Bind /run/systemd to {{ apnscp_shared_root }}
  include_role: role=apnscp/install-services
  vars:
    templated_services:
      - service: virtual-bindings
        template: apnscp-shared-root.mount.j2
    services:
      - name: "{{ virtual_bindings_systemd }}"
        files:
          - src: "{{ virtual_bindings_service_file }}"
            dest: "/etc/systemd/system/{{ virtual_bindings_systemd }}"
        systemd:
          name: "{{ virtual_bindings_systemd }}"
          state: started
          enabled: yes
- include_tasks: set-config.yml
  vars:
    section: "{{ item.section }}"
    option: "{{ item.option }}"
    value: "{{ item.value }}"
    check_value: "{{ item.check_value | default(true) }}"
  with_items: "{{ user_config_defaults }}"
- name: Create apnscp user {{ apnscp_system_user }}
  user:
    home: "{{ apnscp_root }}/storage"
    createhome: no
    shell: /sbin/nologin
    name: "{{ apnscp_system_user }}"
    comment: "ApisCP System User"
    system: yes
    state: present
  register: system_user_created

- name: Set apnscp user in config.ini
  ini_file:
    owner: "{{ apnscp_system_user }}"
    path: "{{apnscp_root}}/config/custom/config.ini"
    section: core
    option: apnscp_system_user
    value: "{{ apnscp_system_user }}"

- file: path=/var/log/bw state=directory
- file: path=/etc/virtualhosting state=directory mode=0700 owner="{{ apnscp_system_user }}"
- name: Set /etc/opcenter/webhost ownership
  file: path=/etc/opcenter/webhost state=directory mode=0700 owner="{{ apnscp_system_user }}"
- name: Set /etc/opcenter ownership
  file: path=/etc/opcenter state=directory mode=0700 owner="{{ apnscp_system_user }}"
  register: c

- name: Update /var/subdomain ownership
  include_role: name=apache/configure tasks_from=assert-subdomain-ownership.yml
  when: c.changed

- include_vars: "vars/opcenter-links.yml"
- name: Link opcenter resources to /etc
  include_tasks: create-link-opcenter-controls.yml
  vars:
    path: "{{ item.path }}"
    src: "{{item.src}}"
  with_items: "{{ opcenter_links }}"

- stat: path="{{ apnscp_root }}/storage/opcenter/namebased_ip_addrs"
  register: nbchk

- copy:
    content: "{{ apnscp_ip4_address | default([ansible_default_ipv4.address]) | join('\n') }}"
    dest: "{{ apnscp_root }}/storage/opcenter/namebased_ip_addrs"
  when: not nbchk.stat.exists or nbchk.stat.size == 0
  notify: Restart virtualhosting
- stat: path="{{ apnscp_root }}/storage/opcenter/namebased_ip6_addrs"
  register: nbchk
- block:
    - copy:
        content: "{{ apnscp_ip6_address | default([ansible_default_ipv6.address]) | join('\n') }}"
        dest: "{{ apnscp_root }}/storage/opcenter/namebased_ip6_addrs"
      notify: Restart virtualhosting
    - name: Set IPv6 address
      include_tasks: set-config.yml
      vars:
        section: dns
        option: my_ip6
        value: "{{ apnscp_ip6_address | default(ansible_default_ipv6.address) }}"
  when: >
    (not nbchk.stat.exists or nbchk.stat.size == 0) and
    ansible_default_ipv6 is defined and 'address' in ansible_default_ipv6
- stat: path="{{ apnscp_root }}/storage/opcenter/interface"
  register: stif
- copy:
    content: "{{ apnscp_interface | default(ansible_default_ipv4.interface) }}"
    dest: "{{ apnscp_root }}/storage/opcenter/interface"
  when: not stif.stat.exists or stif.stat.size == 0

- name: "Configure virtualhosts from namebased_ip_addrs"
  set_fact:
    addr_list: "{{ addr_list + lookup('file', item).split('\n') | reject('match', '^\\s*$') | map('regex_replace', '^(.*?(?=:).*)$', '[\\1]') | list }}"
  with_items:
    - "{{ apnscp_root }}/storage/opcenter/namebased_ip_addrs"
    - "{{ apnscp_root }}/storage/opcenter/namebased_ip6_addrs"

- name: Configure proxy-only private networking
  include_role: name=systemd/override-config
  vars:
    service: apnscp
    config:
      - group: Service
        vars:
          PrivateNetwork: "{{ proxy_intraserver_only | ternary('yes', 'no') }}"
      - group: Unit
        vars:
          JoinsNamespaceOf: mysql.service postgresql.service cp-proxy.service

- stat: path="{{ apnscp_root }}/config/httpd-custom.conf"
  register: httpdchk
- set_fact:
    proxy_external: "{{ (cp_proxy_ip | default([], true) | type_debug == 'list') | ternary(cp_proxy_ip, [cp_proxy_ip | string]) | ipaddr('public') }}"
  when: cp_proxy_ip | default('', true) | length > 0
- set_fact:
    proxy_internal: "{{ (cp_proxy_ip | default(['127.0.0.1'], true) | type_debug == 'list') | ternary(cp_proxy_ip, [cp_proxy_ip | string]) | difference(proxy_external) }}"
  when: cp_proxy_ip | default('', true) | length > 0
- copy:
    # copy doesn't seem to honor Jinja2 pragma ???
    content: |
      #jinja2: lstrip_blocks: True
      ServerName {{ apnscp_fqdn | default(ansible_nodename) }}
      User {{ apnscp_system_user }}
      Group {{ apnscp_system_user }}
      {% if not has_proxy_only %}
        {% for addr in addr_list %}
          {% if addr in (-1 == addr.find(":")) | ternary(ansible_all_ipv4_addresses, ansible_all_ipv6_addresses) %}
            Use VHost {{ addr }}
          {% endif %}
        {% endfor %}
      {% endif %}

      {% if has_proxy_only or cp_proxy_ip | default('', true) | length > 0 %}
        LoadModule remoteip_module sys/httpd/modules/mod_remoteip.so
        RemoteIPHeader X-Forwarded-For

        {% if proxy_internal | length > 0 %}
          RemoteIPInternalProxy {{ proxy_internal | join(" ") }}
        {% endif %}

        {% if proxy_external | length > 0 %}
          RemoteIPTrustedProxy {{ proxy_external | join(" ") }}
        {% endif %}
      {% endif %}
    dest: "{{ apnscp_root }}/config/httpd-custom.conf"
    owner: "{{ apnscp_system_user }}"
    group: "{{ apnscp_system_user }}"
    validate: 'echo %s && httpd -t -f {{ apnscp_root }}/config/httpd.conf'
    backup: yes
  when: not httpdchk.stat.exists or ((force | default(false, true)) | bool)
  notify: Restart apnscp

  # bootstrap.sh compatibility
- name: Set default DNS providers
  include_tasks: "set-config.yml"
  vars:
    section: dns
    option: "{{ item.key }}"
    value: "{{ item.value }}"
  with_dict:
    provider_default: "{{ dns_default_provider | default('builtin', true )}}"
    provider_key: "{{ dns_default_provider_key | default('', true )}}"
- name: Set default mail provider
  include_tasks: "set-config.yml"
  vars:
    section: mail
    option: "{{ item.key }}"
    value: "{{ item.value }}"
  with_dict:
    provider_default: "{{ mail_default_provider | default('builtin', true )}}"
    provider_key: "{{ mail_default_provider_key | default('', true )}}"
- name: Copy /etc/resolv.conf config to config.ini
  set_fact:
    nameservers: "{{ (nameservers | default([])) + [item]}}"
  with_lines:
    - "egrep '^\\s*nameserver\\b' /etc/resolv.conf | cut -d' ' -f2"
  when: apnscp_nameservers is not defined
- include_tasks: set-config.yml
  vars:
    section: "dns"
    option: recursive_ns
    value: "{{ nameservers | join(',')}}"
    check_value: yes
  when: apnscp_nameservers is not defined

- name: "{{ has_screenshots | ternary('Enable', 'Disable') }} chromium screenshot support"
  yum:
    name: chromedriver,chromium-headless,chromium
    state: "{{ has_screenshots | ternary('present', 'absent') }}"
    autoremove: "{{ has_screenshots | ternary(omit, 'true') }}"

# Ensure apnscp ownership is correct
- name: Change apnscp ownership
  command: chown -c -R {{ apnscp_system_user }}:{{ apnscp_system_user }} {{ apnscp_root }}
  register: o
  changed_when: o.stdout != "" and apnscp_notify_ownership_change
  args:
    warn: False
