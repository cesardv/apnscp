# Notes on Timescale cagg:
# - Does not support timezones in daily buckets (daylight saving time fluctuations)
# - Bandwidth is relative to server timezone, start/end of day
# - site_totals_hourly_view is the cagg with timezone applied
# - site_totals_daily_view is an aggregate grouped by zone
# See also https://github.com/timescale/timescaledb/issues/1248
---
- set_fact:
    __apnscp_password: "{{ apnscp_password }}"
    __appliance_password: "{{ appliance_password}}"
  no_log: True

- stat: path="{{ apnscp_db_config }}"
  register: exists
# Override random passwords if config exists
- block:
  - set_fact:
      oldconfig: "{{ lookup('file', apnscp_db_config ) | from_yaml }}"
    no_log: True
  - set_fact:
      exists: {stat: {exists: false} }
    # db.yaml.dist leaves field empty
    when: not 'password' in oldconfig['apnscp'] or not oldconfig['apnscp']['password']
  - set_fact:
      __apnscp_password: "{{ oldconfig['apnscp']['password'] | default(apnscp_password) }}"
      __appliance_password: "{{ oldconfig['appldb']['password'] | default(appliance_password) }}"
    no_log: True
  when: exists.stat.exists
- file: path="{{ apnscp_db_config}}" state=touch
  when: not exists.stat.exists

- name: Verify {{ apnscp_db }} database exists
  mysql_db:
    name: "{{ apnscp_db }}"
    state: present
  register: exists

- name: Create {{ apnscp_db }} database
  mysql_db:
    name: "{{ apnscp_db }}"
    state: import
    config_file: /root/.my.cnf
    target: "{{ role_path }}/files/mysql/apnscp.sql"
  when: exists.changed

# MySQL-python is dumb and can't detect authentication_string usage
- name: Verify {{ apnscp_user }} user exists
  command: mysql -u "{{ apnscp_user }}" -p"{{ __apnscp_password | quote}}"
  register: need_update
  failed_when: false
  changed_when: need_update.rc != 0
  no_log: True

- name: Create {{ apnscp_user }} user
  mysql_user:
    name: "{{ apnscp_user }}"
    password: "{{__apnscp_password}}"
    priv: "{{apnscp_db}}.*:ALL"
    host: localhost
    state: present
    login_user: root
    update_password: "{{ (need_update.rc != 0) | ternary('always', 'on_create') }}"
  register: user_exists
  notify: Restart apnscp
  no_log: True

- include_role: name=common/merge-yaml-config
  vars:
    file: "{{apnscp_db_config}}"
    position: "{{ db_config_name }}"
    items:
      - {'user': "{{apnscp_user}}"}
      - {'password': "{{__apnscp_password}}"}
      - {'host': "localhost"}
      - {'database': "{{ apnscp_db }}"}
    notify: Restart apnscp
  with_items: "{{ db_population_names }}"
  loop_control:
    loop_var: db_config_name
  when: user_exists.changed

- name: Verify {{ appliance_db }} database exists
  postgresql_db:
    name: "{{ appliance_db}}"
    state: present
    login_user: root
  register: db_exists

- name: Verify {{ appliance_user }} user exists
  postgresql_user:
    role_attr_flags: SUPERUSER
    name: "{{ appliance_user }}"
    encrypted: yes
    password: "{{__appliance_password}}"
    state: present
    login_user: root
    db: "{{ appliance_db }}"
  register: user_exists
  notify: Restart apnscp

- name: Update {{ appliance_db }} owner
  postgresql_owner:
    new_owner: "{{ appliance_user }}"
    db: template1
    obj_name: "{{ appliance_db }}"
    obj_type: database
    login_user: root

- include_role: name=common/merge-yaml-config
  vars:
    file: "{{ apnscp_db_config}}"
    position: "{{ appliance_db}}"
    items:
      - {'user': "{{appliance_user}}"}
      - {'password': "{{__appliance_password}}"}
      - {'host': "localhost"}
      - {'database': "{{ appliance_db }}"}
    notify: Restart apnscp
  when: user_exists.changed
  no_log: True

- name: Create {{ appliance_db }} database
  block:
    - tempfile:
        path: /tmp
        state: file
        prefix: appldb
      register: temp
    - template:
        src: "{{ role_path }}/files/pgsql/appldb.sql.j2"
        dest: "{{ temp.path }}"
    - postgresql_db:
        name: "{{ appliance_db}}"
        state: restore
        target: "{{ temp.path }}"
        owner: "{{ appliance_user }}"
        login_password: "{{ __appliance_password }}"
        login_user: "{{appliance_user}}"
        login_host: 127.0.0.1
    - file:
        path: "{{ temp.path }}"
        state: absent
  when: db_exists.changed

- name: Verify {{appliance_user}} grants
  postgresql_privs:
    db: template1
    privs: ALL
    obj: "{{ appliance_db }}"
    role: "{{ appliance_user }}"
    login_user: root
    type: database
    state: present
- name: Install TimescaleDB extension
  postgresql_ext:
    name: timescaledb
    db: "{{ appliance_db }}"
    login_user: "{{ appliance_user }}"
    login_password: "{{ __appliance_password }}"
    login_host: 127.0.0.1
- name: Apply hypertable conversions
  include_tasks: convert-hypertable.yml
  vars:
    table: "{{ item.table }}"
    queries: "{{ item.queries | default([]) }}"
    column: "{{ item.column }}"
  with_items: "{{ hypertable_conversions }}"

- name: Populate APNSCP_APPLIANCE_DB
  lineinfile:
    path: /etc/sysconfig/apnscp
    regexp: '^\s*APNSCP_APPLIANCE_DB\s*='
    line: 'APNSCP_APPLIANCE_DB="{{ appliance_db | quote}}"'
- meta: flush_handlers
# @XXX
# ./artisan migrate --dbonly --force should be called now
# before proceeding further with admin setup if apply_pending_migrations is set.
# This pukes if opcenter/passwd isn't populated from afi scaffolding in Artisan,
# requiring deferment until apnscp/create-admin
#
# Potential for issues to arise if critical DB schema changes between image and migrations
