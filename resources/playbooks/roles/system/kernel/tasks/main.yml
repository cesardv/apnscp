---
- name: Install long-term kernel
  include_role: name=packages/install tasks_from=setup-channel.yml
  vars:
    rv:
      bootstrap:
        repo_rpm: "{{ kernel_repo_url }}"
        repo_key: "{{ kernel_repo_key }}"
        # https://...
        gpg_check: False
      # defer to enable elrepo-kernel
      rpms: []
    channel: null
    notify: Reboot nag
  when: prefer_experimental_kernel | bool
- name: Validate kdump installed
  command: rpm -qi kexec-tools
  register: r
  changed_when: false
  failed_when: false
  args:
    warn: false
  # Experimental kernel doesn't reserve space for
- block:
  - name: "{{ kdump_enabled | ternary('Enable', 'Disable') }} kdump service"
    systemd:
      name: kdump
      state: "{{ kdump_enabled | ternary('started', 'stopped') }}"
      enabled: "{{ kdump_enabled | ternary('yes', 'no') }}"
    when: r.rc == 0
  rescue:
    - name: "Disable kdump"
      systemd:
        name: kdump
        state: stopped
        enabled: no
      # CentOS Stream
      # @until Ansible 2.9.16
      environment:
        LANGUAGE: en_US
      register: r
      failed_when: r is failed and not 'Service is in unknown state' in r.msg
- name: "{{ crashkernel_enabled | ternary('Add', 'Remove') }} crashkernel support"
  replace:
    path: /etc/default/grub
    regexp: 'crashkernel=(?:auto|no|yes)'
    replace: "crashkernel={{ crashkernel_enabled | ternary('auto', 'no') }}"
  notify: Update grub
  register: crash_activate
- name: Check if elrepo exists
  stat: path="{{ elrepo_repo_cfg }}"
  register: r

- name: "{{ prefer_experimental_kernel | ternary('Enable', 'Disable') }} EL-kernel repo"
  ini_file:
    path: "{{ elrepo_repo_cfg }}"
    section: elrepo-kernel
    option: enabled
    value: "{{ prefer_experimental_kernel | ternary(1, 0) }}"
  when: r.stat.exists

- name: "Install {{ custom_kernel_rpm }}"
  yum: name="{{ custom_kernel_rpm }}" state=latest
  when: prefer_experimental_kernel | bool

# Vendors may ship custom kernels with the OS that are difficult to pick out,
# e.g. dediserve and OVH both ship custom kernels (CentOS Plus, custom respectively)
# Look for the kernel as a package & gracefully handle aforementioned edge cases
- name: Query kernel version
  shell: >
    rpm -q "{{ prefer_experimental_kernel | ternary(custom_kernel_rpm, 'kernel') }}" --queryformat="%{VERSION}-%{RELEASE}\n" | tail -n 1
  args:
    warn: false
  ignore_errors: true
  register: release
  changed_when: false
  when: custom_kernel_rpm | default(None, true) != None

- name: Get current kernel version
  command: uname -r
  changed_when: false
  register: curker

- name: List all installed kernels
  shell: >
    rpm -q "{{ custom_kernel_rpm }}" --queryformat="%{VERSION}-%{RELEASE}\n" | sed '$d' | grep -vE "{{ release.stdout | regex_escape() }}|{{ curker.stdout | regex_replace('\.x86_64$', '') | regex_escape() }}"
  args:
    warn: false
  failed_when: false
  register: all
  changed_when: (all.stdout | trim) != ''
  when:
    - not release is failed
    - (custom_kernel_remove_additional | bool)

- name: Remove additional kernels
  yum: name='{{ (all.stdout | trim).split("\n") | map("regex_replace", "^", custom_kernel_rpm + "-")  | list }}' state=absent
  when:
    - not release is failed
    - (custom_kernel_remove_additional | bool) and (all.stdout | trim) != ''


- block:
  - name: Enumerate kernels (CentOS 7)
    shell: >
      awk -F\' '/^menuentry / {print $2}' {{ grub_cfg }} |
      cat -n | awk '{print $1-1,$1="",$0}' | grep -F "{{ release.stdout }}" |
      awk  '{print $1}'
    register: idx
    failed_when: idx.stdout == ""
    changed_when: false
    when:
      - ansible_distribution_major_version == '7'
      - (custom_kernel_rpm | default(None, true)) != None

    # Prefer rpm.labelCompare if this produces invalid results
    # """
    # Optionally it might sort the menu based on the machine-id and version fields,
    # and possibly others.
    # """
  - name: Enumerate kernels (CentOS 8+)
    shell: >
      ls -1 /boot/loader/entries | sort -Vr |
      cat -n | awk '{print $1-1,$1="",$0}' | grep -F "{{ release.stdout }}" |
      awk  '{print $1}'
    register: idx
    failed_when: idx.stdout == ""
    changed_when: false
    when:
      - ansible_distribution_major_version != '7'
      - (custom_kernel_rpm | default(None, true)) != None

  - name: Get active kernel
    command: >
      grub2-editenv list
    register: grub2list
    changed_when: false

  - name: "Change default kernel to {{ release.stdout | default('--SKIPPED--', true) }}"
    command: >
      grub2-set-default "{{ idx.stdout | default(0) }}"
    when:
      - always_update_kernel
      - custom_kernel_rpm | default(None, true) != None
      - -1 == grub2list.stdout.find(release.stdout)
      - 0 != curker.stdout.find(release.stdout)
    register: activate
    notify: Update grub
  when:
    - not release is failed
  rescue:
    - name: Ignore kernel failures (custom kernel)
      debug: msg="Skipping kernel activation due to errors"

- block:
  - debug: msg="Reboot to bring up new kernel"
    when: not kernel_automated_reboot
  - name: Reboot OS for kernel activation
    include_role: name=common/fail-and-save
    vars:
      reboot: true
      msg: |
        KERNEL UPGRADED. REBOOTING AUTOMATICALLY.
    when: kernel_automated_reboot
  when: >-
    activate is defined and activate is changed
    or
    crash_activate is defined and crash_activate is changed
