# Install Argos Monit helper
# Must come last since RPM installation triggers a Monit
# restart and unless everything is installed...
---
- name: Install argos
  yum: name=argos state=installed
  register: c
- name: Install ntfy
  pip: name=ntfy state=present version="{{ ntfy_version }}"
- name: Verify monit enabled
  systemd: name=monit state={{ c.changed | ternary('reloaded','started') }} enabled=yes
- name: Update default credentials in monitrc
  replace:
    regexp: '(^\s*)allow admin:monit'
    replace: '\1allow admin:{{ lookup("password", "/dev/null chars=ascii_letters,digits length=16") }}'
    path: /etc/monitrc
    owner: root
    mode: 0600
  notify: Restart monit
  no_log: true
- name: Update apnscp monitoring profile
  template:
    src: templates/apnscp.conf.j2
    dest: /etc/monit.d/apnscp.conf
    force: yes
  notify: Restart monit

- name: Check if / and {{ apnscp_account_root }} are cross-linked
  block:
    - stat: path=/
      register: s1
    - stat: path={{ apnscp_account_root }}
      register: s2
    - set_fact:
        home_is_crosslinked: "{{ s1.stat.dev != s2.stat.dev }}"

- name: Update monitoring profiles
  include_tasks: toggle-service.yml
  vars:
    name: "{{ item.name }}"
    state: "{{ (item.when | bool) | ternary('enabled', 'disabled') }}"
  with_items:
    - '{{ argos_monitoring_services + [{"name": home_monitor, "when": home_is_crosslinked}] }}'
- name: Set {{ argos_user }} => {{ argos_delivery_user }} alias
  include_role: name=mail/configure-postfix tasks_from=manage-alias.yml
  vars:
    email: "{{ argos_user }}"
    destination: "{{ argos_delivery_user }}"

- name: Extract relay credentials
  set_fact:
    # *sigh* would love to do this in one go
    username: '{{ lookup("file", "/etc/monit.d/00-argos.conf") | regex_search(auth_regex, multiline=True) | regex_replace(auth_regex, "\1") }}'
    password: '{{ lookup("file", "/etc/monit.d/00-argos.conf") | regex_search(auth_regex, multiline=True) | regex_replace(auth_regex, "\3") }}'
  no_log: True
- block:
  - name: Test SMTP server
    mail:
      host: 127.0.0.1
      port: 587
      # Specify a hostname for the email to bounce back
      to: "monit-smtp-check@localhost"
      subject: Argos SMTP test
      username: "{{ username }}"
      password: "{{ password }}"
    register: r
    failed_when: r is failed and "5.1.1" not in r.msg
  rescue:
    - fail: msg='{{ r }}'
    - name: Change password for {{ username}}
      user:
        name: "{{ username }}"
        password: "{{ password | password_hash('sha512') }}"
      no_log: True
