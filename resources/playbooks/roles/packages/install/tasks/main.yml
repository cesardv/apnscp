# Copyright (c) 2017 Apis Networks
# vim:ft=ansible:
---
- include_tasks: configure-default-vars.yml
- name: "Verify apnscp priority"
  ini_file:
    path: /etc/yum.repos.d/apnscp.repo
    section: apnscp
    option: priority
    value: 1
    state: present
  when: false

- name: Enable fastestmirror support
  ini_file:
    path: "{{ (ansible_distribution_major_version == '7') | ternary('/etc/yum.conf', '/etc/dnf/dnf.conf') }}"
    section: main
    option: fastestmirror
    value: "True"

- include_tasks: centos8-dnf.yml
  when: ansible_distribution_major_version != "7"

- name: "Install MariaDB"
  tags: ['install', 'sql', 'mysql']
  block:
    - stat: path=/etc/yum.repos.d/mariadb.repo
      register: mariadb_stat
    - template: src=mariadb.repo.j2 dest=/etc/yum.repos.d/mariadb.repo owner=root group=root mode=0644
      when: mariadb_stat is defined and mariadb_stat.stat.exists == False
      register: mariadb_repo

- name: "Remove MySQL/MariaDB system packages"
  yum: name="mariadb*,mysql*" state=absent
  when: maradb_stat is defined and mariadb_stat.stat.exists == False

- name: "Install PostgreSQL"
  block:
      # Satisfied by configure-rhel.yml task
    - name: Install SCL repository
      yum: name=centos-release-scl state="{{ has_scl | ternary('present', 'absent') }}"
      when:
        - ansible_distribution == 'CentOS'
        - ansible_distribution_major_version == "7"
    - set_fact:
        # PostgreSQL packages are xx-<MAJOR><MINOR> sans period
        pgversion: "{{ pgsql_version | default(pgsql.default) }}"
        pg_pkgversion: '{{ pgsql_version | default(pgsql.default) | regex_replace("\.","") }}'
    - include_vars: file="vars/rpms/pgdg.yml" name=pg
    - name: Check if pgdg repo exists
      stat: path="{{ pg.repo_path }}"
      register: rpmstat
      # moot with https
    - yum: name={{ pg.repo_location[pgversion] }} state=present disable_gpg_check=yes
      register: result
      until: result is succeeded
      retries: "{{ network_max_retries }}"
      when: not rpmstat.stat.exists
      # 2019/04/16 - RPM spec broken
    - name: "Remove PostgreSQL system package"
      yum: name="postgresql-libs" state=absent
      when:
        - not rpmstat.stat.exists
        - result.changed
    - name: Check if GPG key needs relink
      stat: path=/etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG
      register: r
    - name: Link PGDG-{{pg_pkgversion}} to PGDG
      file:
        path: "/etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG"
        state: link
        src: "/etc/pki/rpm-gpg/RPM-GPG-KEY-PGDG-{{ pg_pkgversion }}"
      when: not r.stat.exists
    - name: Validate if new repo format used
      stat: path="{{ pg.repo_path }}"
      register: r
    - name: Disable all non-essential PostgreSQL repos
      ini_file:
        path: "{{ pg.repo_path }}"
        section: 'pgdg{{ item | regex_replace("\.","")  }}'
        option: enabled
        value: "0"
      with_items:  "{{ pg.repo_location.keys() | reject('match', '^' + pg_pkgversion + '$') | list }}"
      when:
        - r.stat.exists
        - -1 != lookup('file', pg.repo_path).find('pgdg' + (item | regex_replace("\.","")) + ']')

    - name: Enable PostgreSQL v{{ pgversion }} repo
      ini_file:
        path: "{{ pg.repo_path }}"
        section: "pgdg{{ pg_pkgversion }}"
        option: enabled
        value: "1"
      when: r.stat.exists
  tags: ['install', 'sql', 'pgsql']

- name: Remove bogus failovermethod= directive
  lineinfile:
    path: "{{ item }}"
    regexp: '^\s*failovermethod\s*='
    state: absent
  with_items:
    - "{{ pg.repo_path }}"
    - /etc/yum.repos.d/apnscp.repo
    - /etc/yum.repos.d/apnscp-testing.repo
  when: ansible_distribution_major_version == '8'

- name: "Install MongoDB"
  tags: ['install', 'sql', 'mongodb']
  block:
    - stat: path=/etc/yum.repos.d/mongodb.repo
      register: mongodb_stat
    - template: src=mongodb.repo.j2 dest=/etc/yum.repos.d/mongodb.repo owner=root group=root mode=0644
      when: mongodb_stat is defined and mongodb_stat.stat.exists == False
  when: mongodb_enabled | bool

- name: "Install CloudFlare"
  tags: ['cloudflare', 'apache']
  block:
    - stat: path=/etc/yum.repos.d/mod_cloudflare.repo
      register: cf_stat
    - yum:
        name: "http://pkg.cloudflare.com/cloudflare-release-latest.el{{ ansible_distribution_major_version }}.rpm"
        state: present
      register: result
      retries: "{{ network_max_retries }}"
      until: result is succeeded
      when:
        - cf_stat.stat is defined and not cf_stat.stat.exists
  when: ansible_distribution_major_version == "7"

- include_tasks: setup-channel.yml
  vars:
    channel: "{{ repo.name }}"
  tags: ["install"]
  when: not "when" in repo or repo.when
  with_items:
    - name: apnscp.yml
    - name: mariadb.yml
    - name: pgdg.yml
    - name: centos.yml
    - name: epel.yml
    - name: cloudflare.yml
      when: "{{ ansible_distribution_major_version == '7' }}"
    - name: mongodb.yml
    - name: remove.yml
    - name: misc.yml
  loop_control:
    loop_var: repo

  # Pull down later to avoid dep conflict
- name: "Install Google Pagespeed"
  block:
    - stat: path=/etc/yum.repos.d/mod-pagespeed.repo
      register: ps_stat
    - yum:
        # Installing mod-pagespeed automatically pulls in repo config
        name: "https://dl-ssl.google.com/dl/linux/direct/mod-pagespeed-stable_current_{{ ansible_machine }}.rpm"
        disable_gpg_check: yes
        state: present
      register: result
      until: result is succeeded
      retries: "{{ network_max_retries }}"
      when: ps_stat.stat is defined and not ps_stat.stat.exists
  tags: ['pagespeed', 'apache']
#- name: Update all packages
#  yum: name=* state=latest
#  when: False
#
# @XXX 2.5.2 bug with appending to vars via set_fact
# https://github.com/ansible/ansible/pull/38302
#
- block:
  - name: Downgrade Ansible
    yum: name=ansible-2.4.2.0-2.el7 state=installed allow_downgrade=yes
  - include_role: name=common/fail-and-save
    vars:
      msg: |
          Ansible 2.5.1 is installed and has been downgraded automatically to
          2.4.2. A restart is necessary.

          See ansible/ansible#38271 for details of downgrade.
  when: ansible_version['full'] == "2.5.1"
- include_role: name=systemd/hotfix tasks_from=ansible-72985.yml
  vars:
    reboot: true
  when: ansible_version['full'] == "2.9.16"
