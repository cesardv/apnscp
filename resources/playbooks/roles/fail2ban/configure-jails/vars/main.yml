---
jails:
  #  higher hit count?
  - name: postfix
    vars:
      port: 25,587,465
      logpath: "%(dovecot_log)s{{ f2b_postfix_tail | default(f2b_use_tail) | ternary(' tail', '') }}"
      banaction: "{{ f2b_postfix_banaction | default(f2b_default_banaction) }}"
      enabled: "true"
      bantime: "{{ f2b_dovecot_bantime | default(f2b_bantime) }}"
      findtime: "{{ f2b_postfix_findtime | default(f2b_findtime) }}"
      maxretry: "{{ f2b_postfix_maxretry | default(f2b_maxretry) }}"
      filter: "{{ fail2ban_version is version('0.10.0', '>=') | ternary('postfix-apnscp', 'postfix') }}"
      chain: "{{ f2b_postfix_chain | default(f2b_ban_chain) }}"
  - name: postfix-sasl
    vars:
      port: 25,587,465
      logpath: "%(dovecot_log)s{{ f2b_postfix_tail | default(f2b_use_tail) | ternary(' tail', '') }}"
      banaction: "{{ f2b_postfix_banaction | default(f2b_default_banaction) }}"
      enabled: "true"
      bantime: "{{ f2b_postfix_bantime | default(f2b_bantime) }}"
      findtime: "{{ f2b_postfix_findtime | default(f2b_findtime) }}"
      maxretry: "{{ f2b_postfix_maxretry | default(f2b_maxretry) }}"
      chain: "{{ f2b_postfix_chain | default(f2b_ban_chain) }}"
  - name: evasive
    vars:
      port: 80,443
      logpath: "%(syslog_daemon)s{{ f2b_evasive_tail | default(f2b_use_tail) | ternary(' tail', '') }}"
      banaction: "{{ f2b_evasive_banaction | default(f2b_default_banaction) }}"
      ignoreip: 127.0.0.1
      filter: "apache-modevasive"
      bantime: "{{ f2b_evasive_bantime | default(120) }}"
      maxretry: "{{ f2b_evasive_maxretry | default (1) }}"
      findtime: "{{ f2b_evasive_findtime | default(3600) }}"
      chain: "{{ f2b_evasive_chain | default(f2b_ban_chain) }}"
      enabled: "true"
  - name: recidive
    vars:
      protocol: all
      port: all
      # In fail2ban >= 0.10, firewallcmd-ipset[actiontype="<allports>"] is used
      banaction: "%(banaction_allports)s"
      enabled: "true"
      logpath: "{{ fail2ban_log }}{{ f2b_recidive_tail | default(f2b_use_tail) | ternary(' tail', '') }}"
      bantime: "{{ f2b_recidive_bantime }}"
      findtime: "{{ f2b_recidive_findtime }}"
      maxretry: "{{ f2b_recidive_maxretry }}"
      chain: "{{ f2b_recidive_chain | default(f2b_ban_chain) }}"
  - name: dovecot
    vars:
      port: 110,995,143,993
      bantime: "{{ f2b_dovecot_bantime | default(f2b_bantime) }}"
      findtime: "{{ f2b_dovecot_findtime | default(f2b_findtime) }}"
      maxretry: "{{ f2b_dovecot_maxretry | default(f2b_maxretry) }}"
      logpath: "%(dovecot_log)s{{ f2b_dovecot_tail | default(f2b_use_tail) | ternary(' tail', '') }}"
      enabled: "{{ mail_enabled | ternary('true', 'false') }}"
      banaction: "{{ f2b_dovecot_banaction | default(f2b_default_banaction) }}"
      chain: "{{ f2b_dovecot_chain | default(f2b_ban_chain) }}"
  - name: sshd
    vars:
      port: "{{ ((sshd_port | type_debug)  == 'list') | ternary(sshd_port,[sshd_port]) | join(',')}}"
      bantime: "{{ f2b_sshd_bantime | default(f2b_bantime) }}"
      findtime: "{{ f2b_sshd_findtime | default(f2b_findtime) }}"
      maxretry: "{{ f2b_sshd_maxretry | default(f2b_maxretry) | int}}"
      logpath: "%(sshd_log)s{{ f2b_sshd_tail | default(f2b_use_tail) | ternary(' tail', '') }}"
      filter: "{{ fail2ban_version is version('0.10.0', '>=') | ternary('sshd', f2b_sshd_filter_mode) }}"
      mode: "{{ f2b_sshd_filter_mode }}"
      chain: "{{ f2b_sshd_chain | default(f2b_ban_chain) }}"
      enabled: "true"
  - name: spambots
    vars:
      port: 25,587
      bantime: "{{ f2b_spambots_bantime | default(f2b_bantime) }}"
      findtime: "{{ f2b_spambots_findtime | default(f2b_findtime) }}"
      maxretry: "{{ f2b_spambots_maxretry | default(f2b_maxretry) }}"
      logpath: "%(postfix_log)s{{ f2b_spambots_tail | default(f2b_use_tail) | ternary(' tail', '') }}"
      banaction: "{{ f2b_spambots_banaction | default(f2b_default_banaction) }}"
      chain: "{{ f2b_spambots_chain | default(f2b_ban_chain) }}"
      enabled: "true"
  - name: vsftpd
    vars:
      port: ftp,ftp-data,ftps,ftps-data
      bantime: "{{ f2b_vsftpd_bantime | default(f2b_bantime) }}"
      findtime: "{{ f2b_vsftpd_findtime | default(f2b_findtime) }}"
      maxretry: "{{ f2b_vsftpd_maxretry | default(f2b_maxretry) }}"
      logpath: "%(vsftpd_log)s{{ f2b_vsftpd_tail | default(f2b_use_tail) | ternary(' tail', '') }}"
      banaction: "{{ f2b_vsftpd_banaction | default(f2b_default_banaction) }}"
      chain: "{{ f2b_vsftpd_chain | default(f2b_ban_chain) }}"
      enabled: "{{ ftp_enabled | ternary('true', 'false') }}"
  - name: mysqld
    vars:
      port: 3306
      # rename jail to "mysqld" for consistency
      filter: mysqld-auth
      bantime: "{{ f2b_mysqld_bantime | default(f2b_bantime) }}"
      findtime: "{{ f2b_mysqld_findtime | default(f2b_findtime) }}"
      maxretry: "{{ f2b_mysqld_maxretry | default(f2b_maxretry) }}"
      logpath: "/var/lib/mysql/mysqld.log{{ f2b_mysqld_tail | default(f2b_use_tail) | ternary(' tail', '') }}"
      banaction: "{{ f2b_mysqld_banaction | default(f2b_default_banaction) }}"
      chain: "{{ f2b_mysqld_chain | default(f2b_ban_chain) }}"
      enabled: "{{ mysql_remote_connections | ternary('true', 'false') }}"
  - name: pgsql
    vars:
      port: 5432
      # rename jail to "mysqld" for consistency
      filter: postgresql
      bantime: "{{ f2b_pgsql_bantime | default(f2b_bantime) }}"
      findtime: "{{ f2b_pgsql_findtime | default(f2b_findtime) }}"
      maxretry: "{{ f2b_pgsql_maxretry | default(f2b_maxretry) }}"
      logpath: "/var/log/secure{{ f2b_pgsql_tail | default(f2b_use_tail) | ternary(' tail', '') }}"
      banaction: "{{ f2b_pgsql_banaction | default(f2b_default_banaction) }}"
      chain: "{{ f2b_pgsql_chain | default(f2b_ban_chain) }}"
      enabled: "{{ pgsql_remote_connections | ternary('true', 'false') }}"
  # @TODO PostgreSQL
regex_updates:
  - filter: mysqld-auth.conf
    section: Definition
    option: failregex
    value: >-
      ^%(__prefix_line)s\s*(?:\d+ |\d{1,2}:\d{2}:\d{2}(?:\s+\d+)?\s*)?\[\w+\] Access denied for user '[^']+'@'<HOST>' (to database '[^']*'|\(using password: (YES|NO)\))*\s*$
  # Bad MariaDB! Bad bad! Pads left two spaces on time
  - filter: mysqld-auth.conf
    section: Init
    option: datepattern
    value: '%%Y(?P<_sep>[-/.])%%m(?P=_sep)%%d\s{1,2}%%H:%%M:%%S(?:,%%f)?'
