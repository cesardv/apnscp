@extends("auth.auth-common")
@section('title', "Hosting Account Notice")
@section('notice')
    <p>
        This is to confirm that your account <b>{{ $what }}</b> has changed @if ($domain) for domain <b>{{ $domain }}</b> @endif
        @if ($username)(username: {{ $username }}) @endif. Please contact us immediately if you did not authorize this change.
    </p>
@endsection