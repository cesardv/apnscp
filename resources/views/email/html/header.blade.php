@if (APNSCPD_HEADLESS)
	<tr>
	<td class="header">
        <h1 id="logo" style="height:40px;width: 200px;display: block;margin: 5px auto 0 auto;">
            <a href="{{ $url }}">
                <img src="{{ \Auth_Redirect::getPreferredUri() }}/images/apps/login/logo-lg.png" border="0" class="emailImage" style="height:40px !important;"/>
            </a>
        </h1>
    </td>
	</tr>
@endif
