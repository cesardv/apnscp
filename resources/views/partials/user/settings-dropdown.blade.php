<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"
   aria-controls="#ui-profile-links">
	@if ($avatar = UCard::get()->getAvatar())
		<img src="{{ $avatar }}"
			 class="avatar rounded img img-rounded img-fluid" id="account-img"/>
	@endif
	{{ UCard::get()->getUser() }}
</a>
<div class="dropdown-menu mt-0 dropdown-menu-form @if(\Preferences::get(\Page_Renderer::THEME_SWAP_BUTTONS, false)) dropdown-menu-left @else dropdown-menu-right @endif" id="ui-profile-links">
	<div class="dropdown-item small" id="login-domain">
		@if (!UCard::get()->hasPrivilege('admin'))
			@if ($domains = UCard::get()->getImpersonableDomains())
				<form action="{{ Template_Engine::init()->getPathFromApp('dashboard') }}/hijack" method="POST">
					<select class="custom-select bg-transparent border-0" id="hijackDomain" name="hijack">
						<option value="">{{ UCard::get()->getDomain() }}</option>
						@foreach ($domains as $domain)
							<option value="{{ $domain }}">
								{{ $domain }}
							</option>
						@endforeach
					</select>
				</form>
			@else
				<a href="/apps/dashboard" class="text-center w-100">{{ UCard::get()->getDomain() }}</a>
			@endif
		@else
			<em class="w-100 d-block text-center">--ADMIN--</em>
		@endif
	</div>
	<a href="{{ Template_Engine::init()->getPathFromApp('changeinfo') }}" class="dropdown-item">
		Settings
		<i class="fa fa-cog"></i>
	</a>
	<a href="{{ MISC_KB_BASE }}" class="dropdown-item">
		Help Center <i class="fa fa-lightbulb-o"></i>
	</a>
	@if (UCard::is('site') && @cmd('crm_enabled'))
		<a href="/apps/troubleticket" class="dropdown-item">
			Support<i class="fa fa-life-ring"></i>
		</a>
	@endif
	<a href="{{ Template_Engine::init()->getPathFromApp('sitemap') }}" id="ui-app-index-link" class="dropdown-item text-small">
		App Index
		<i class="fa fa-sitemap"></i>
	</a>
	<div class="dropdown-divider"></div>
	@if (\Auth::profile()->getImpersonator())
		<a href="{{ Template_Engine::init()->getPathFromApp('dashboard') }}?revertid={{ \Auth::profile()->getImpersonator() }}"
		   class="dropdown-item">
			Revert Login-As
			<i class="fa fa-users"></i>
		</a>
	@endif
	<a href="/logout" class="dropdown-item">
		Logout <i class="fa fa-sign-out"></i>
	</a>
</div>