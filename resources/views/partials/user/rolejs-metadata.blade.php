<script language="JavaScript" type="text/javascript">
	//<![CDATA[
	@if (!\Auth_UI::public_page())
		var session = {
			id: '{{ session_id() }}',
			server: '{!! SERVER_NAME_SHORT !!}',
			serverFull: '{!! SERVER_NAME !!}',
			user: '{!! $_SESSION['username'] !!}',
			domain: '{!! $_SESSION['domain'] !!}',
			appName: '{!! $Page->getApplicationTitle() !!}',
			appId: '{!! $Page->getApplicationID(); !!}',
			debug: {!! (int)is_debug() !!},
			role: '{!! UCard::get()->getRoleAsString() !!}',
			clientIp: '{!! \Auth::client_ip() !!}',
			name: '{!! session_name() !!}',
			svc: {!! json_encode($Page->getRoleHelperServiceData(), JSON_HEX_QUOT|JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS) !!}
		};
	@else
		var session = {
			debug: {!! (int)is_debug() !!}
		};
	@endif
	//]]>
</script>