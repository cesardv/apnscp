<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TimescaleUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	// TimescaleDB gets loaded via Laravel
		(new \CLI\Yum\Synchronizer\Plugins\Trigger\TimescaledbPostgresqlAnyVersion())->update('');
    	$pdo = PostgreSQL::pdo();
    	if (TELEMETRY_ARCHIVAL_COMPRESSION) {
			$pdo->exec('ALTER TABLE metrics SET (timescaledb.compress, timescaledb.compress_segmentby = \'site_id,attr_id\');');
			(new \Daphnie\Chunker($pdo))->setCompressionPolicy(TELEMETRY_COMPRESSION_THRESHOLD);
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
