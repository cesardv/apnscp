<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PurgeErroneousMailbox extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		\Illuminate\Support\Facades\DB::connection('pgsql')->getPdo()->exec("UPDATE email_lookup SET fs_destination =  '' WHERE fs_destination = 'Mail/'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
