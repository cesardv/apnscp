#!/bin/sh
[[ -f /etc/sysconfig/apnscp ]] && . /etc/sysconfig/apnscp
APNSCP_ROOT=${APNSCP_ROOT:-/usr/local/apnscp}
${APNSCP_ROOT}/bin/cmd -o json scope:list | jq -r '.[]' | while read scope ; do echo "**name:** $scope  " ; ( cpcmd -o json scope:info $scope 2> /dev/null | jq -r 'del(.["value"])' | python -c 'import sys, yaml, json; yaml.safe_dump(json.load(sys.stdin), sys.stdout, default_flow_style=False)' ) | sed -e 's!^\([^-].*\)$!\1  !g' ; echo "" ; done
