#!/bin/bash
set -euo pipefail
[[ -f /etc/sysconfig/apnscp ]] && . /etc/sysconfig/apnscp
APNSCP_APPLIANCE_DB=${APNSCP_APPLIANCE_DB:-appldb}
APNSCP_ROOT=${APNSCP_ROOT:-/usr/local/apnscp}
# env USE_MITOGEN="" to disable
USE_MITOGEN=1
# env USE_AUTOMATIC="" to disable
USE_AUTOMATIC=1

AUTOMATIC=/bin/true
if [[ $USE_AUTOMATIC -ne 1 ]]; then
	AUTOMATIC=/bin/false
fi

if [[ -z ${CLEAN:-} ]]; then
	echo "clean.sh is intended to wipe an existing apnscp install for imaging."
	echo "Run 'env CLEAN=1 clean.sh' to irreversibly wipe apnscp data from this machine and clean. Exiting."
	exit 1
fi

LINES="$(grep '\.' /etc/virtualhosting/mappings/domainmap || true)"
if test ! -z "$LINES"; then
	echo "clean.sh only works on a pristine environment. One or more domains detected. Exiting."
	exit 1
fi

yum update -y

cd /usr/local/apnscp
rm -rf config/license.pem storage/logs/* storage/run/{appendonly.aof,.ansible.lock} \
	storage/certificates/{data,accounts} storage/opcenter/{passwd,namebased_ip_addrs,namebased_ip6_addrs} \
	config/httpd-custom.conf config/db.yaml config/custom/config.ini storage/tmp/* /tmp/* \
	storage/constants.php /root/.bash_history /root/.{my.cnf,pgpass} /root/{.composer,.ansible} \
	/root/apnscp-bootstrapper.log /root/license.* /root/.monit* /root/.rnd /etc/authlib/authpgsqlrc \
	/root/.ssh/{authorized_keys,known_hosts} /root/.ssh/.viminfo /etc/monit.d/00-argos* /etc/ssl/certs/{pgsql,mysql}-server.pem
grep -rslF "${APNSCP_APPLIANCE_DB}" /etc/postfix/*.cf | xargs rm -f
truncate -s 0 /.socket/{wtmp,btmp}
cd /

rpm -e argos monit
rm -f /etc/monitrc.rpmsave

sudo -u postgres dropuser root

find /root/Mail -type f -exec rm -f {} \;
find /var/log -type f -exec truncate -s 0 {} \;

rm -rf /var/log/*.gz /var/log/*.[0-9] /var/log/*-????????
rm -rf /var/cache/mod_pagespeed/v*

history -c
rm -f "${HISTFILE:-/root/.bash_history}"
unset HISTFILE

[[ -f /swap ]] && swapoff -a && mkswap /swap

PYTHONPATH=""
if [[ -n "${USE_MITOGEN:-}" ]]; then
	PIP=/usr/bin/pip
	[[ -f /usr/bin/pip3 ]] && PIP=/usr/bin/pip3
	$PIP install -qU mitogen
	PYTHONPATH="$( ($PIP show mitogen | grep -E '^Location:' | cut -d' ' -f2) || true)"
fi
PLAYBOOK=${PLAYBOOK:-prebuilt-quickstart.yml}

cat <<EOF >/root/resume_apnscp_setup.sh
#!/bin/sh
chmod 711 ${APNSCP_ROOT}
[[ ! -z "$PYTHONPATH" ]] && STRATEGY_PATH="${PYTHONPATH}/ansible_mitogen/plugins/strategy"
STRATEGY="linear"
[[ -d "\$STRATEGY_PATH" ]] && STRATEGY="mitogen_linear"
export ANSIBLE_LOG_PATH="/root/apnscp-bootstrapper.log" \
    ANSIBLE_STDOUT_CALLBACK="default" \
    ANSIBLE_STRATEGY_PLUGINS="\$STRATEGY_PATH" \
    ANSIBLE_STRATEGY="\$STRATEGY"
    cd ${APNSCP_ROOT}/resources/playbooks && /usr/bin/ansible-playbook -l localhost -c local "$PLAYBOOK" && \
    rm -f /root/resume_apnscp_setup.sh
EOF

[[ -d /etc/cloud/cloud.cfg.d ]] && cat <<EOF >/etc/cloud/cloud.cfg.d/99-apnscp.cfg
# cloud-config
# ApisCP provisioning

runcmd:
  - >-
    /bin/sh -c '[[ ! -f "${APNSCP_ROOT}/config/license.pem" ]] &&
	  curl -f -A "apnscp bootstrapper" -o "${APNSCP_ROOT}/config/license.pem" "https://bootstrap.apnscp.com"
  - $AUTOMATIC && systemctl start bootstrapper-resume
package_upgrade: true
EOF

[[ -f "${APNSCP_ROOT}/build/motd.src" ]] && cp "${APNSCP_ROOT}/build/motd.src" "/etc/motd"

systemctl enable bootstrapper-resume
chmod 755 /root/resume_apnscp_setup.sh

passwd -d root
passwd -l root
