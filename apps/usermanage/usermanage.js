$(document).ready(function () {
	$('tr.entry').highlight();
	$('.ui-action-delete').click(function () {
		return confirm("Are you sure you want to delete " + $(this).val() + "?");
	});

	$('.filter-users').bind('click change', function (e) {
		return do_filter(e);
	}).bind('keydown', function (e) {
		if (e.keyCode === $.ui.keyCode.ESCAPE) {
			$('#reset_filter').click();
			$('#filter').val('');
		} else if (e.keyCode === $.ui.keyCode.ENTER) {
			$(this).triggerHandler('click');
		}
	});

	$('#users').tablesorter({headers: {0: {sorter: false}, 4: {sorter: false}, 5: {sorter: false}}});
	$('#users thead tr th:eq(5), #users thead tr th:eq(0), #users thead tr th:eq(4)').addClass('nosort');
	update_filters();
});

/**
 * Apply field filter
 */
var services = ['cp', 'web', 'ftp', 'ssh', 'mail'];
var $filters = $('#searchop').clone();

function do_filter(e) {
	if (e.type == 'change' && e.target.id == 'filter_spec') {
		return update_filters();
	} else if (e.target.id == "do_filter") {
		var field = $('#filter_spec').val(),
			patternbase = $('#filter').val(),
			op = $('#searchop').val(), re,
			pattern = patternbase,
			/** base regex comparator */
			comparator;

		switch (op) {
			case 'contains':
				break;
			case 'notcontains':
				pattern = '^(?!.*' + pattern + ').*$';
				break;
			case 'is':
				pattern = pattern.toLowerCase();
				comparator = function (field) {
					return $(this).children('td.' + field).text().trim().toLowerCase() != pattern;
				}
				break;
			case 'isnot':
				pattern = pattern.toLowerCase();
				comparator = function (field) {
					return $(this).children('td.' + field).text().trim().toLowerCase() == pattern;
				}
				break;
			case 'service-enabled':
				comparator = function (field) {
					return !$('td.status_cell .' + field + ' span', this).hasClass('ui-action-is-enabled');
				}
				break;
			case 'service-disabled':
				comparator = function (field) {
					return $('td.status_cell .' + field + ' span', this).hasClass('ui-action-is-enabled');
				}
				break;
		}
		// some switches may define their own comparator
		// if none is set revert to the regex-based comparator
		if (!comparator) {
			comparator = function (field) {
				re = new RegExp(pattern, "i");
				return re.test($(this).children('td.' + field).text().trim()) == false;
			}
		}

		$('tr.entry').filter(function () {
			return comparator.call(this, field);
		}).fadeOut('fast', function () {
			$(this).removeClass('ui-highlight').find(':checkbox').prop('checked', false);
		});

		var $el = $('<li/>').addClass("label ui-filter-spec " +
			'filter-comp-' + op + ' filter-' + field).text(patternbase).click(function () {
			return remove_filter.apply(this, [$(this).data('comparator'), $(this).data("type")]);
		}).data({
			type: field,
			comparator: comparator
		});

		$('ul.ui-active-filters').append($el);

		if ($('#reset_filter:hidden').length > 0) {
			$('#reset_filter').fadeIn();
		}

		return false;
	} else if (e.target.id == "reset_filter") {
		$('.filter-users ul.ui-active-filters').empty();
		$('#reset_filter').hide();
		$('tr.entry:hidden').fadeIn('fast');
		return false;
	}

	return true;
}

/**
 * update filter operations
 */
function update_filters() {
	var $el = $('#filter_spec'),
		keep = ['service-enabled', 'service-disabled'];
	$('#searchop').replaceWith($filters.clone());

	if ($.inArray($el.val(), services) >= 0) {
		$('#searchop option').each(function (e) {
			var val = $(this).val();
			if (val != 'service-enabled' && val != 'service-disabled') {
				$(this).remove();
			}
		});
		$('#filter').fadeOut();
	} else {
		$('#searchop option[value=service-enabled], ' +
			'#searchop option[value=service-disabled]').remove();
		if ($('#filter:hidden')) {
			$('#filter').fadeIn();
		}
	}

	return true;
}

/**
 * Remove filter and relist hidden items
 */
function remove_filter(comparator, field) {
	$('tr.entry:hidden').filter(function () {
		return comparator.call(this, field);
	}).fadeIn('fast');

	$(this).remove();
	if ($('.ui-active-filters li').length == 0) {
		$('#reset_filter').fadeOut();
	}
	return false;
}