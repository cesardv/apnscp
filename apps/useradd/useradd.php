<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\useradd;

	use Error_Reporter;
	use Opcenter\Auth\Password;
	use Page_Container;

	include_once(INCLUDE_PATH . '/lib/html/userdefaults-loader.php');

	class Page extends Page_Container
	{
		private $def_options;
		// merged options -- default + local (/etc/usertemplate)
		private $options;

		public function __construct()
		{
			parent::__construct();

			$this->load_user_defaults();
			$this->options = $this->def_options;
			$this->init_js('multiselect', 'slider', 'browser');

			$this->add_javascript('/apps/useradd/useradd.js');

			if ($this->isModeSwitch('user')) {
				$user = $_GET['user'];
				$this->options['username'] = $user;
				if (!$this->user_exists($user)) {
					error("invalid user specified `%s'", $user);
				}
				$this->hide_pb();
			}
		}

		private function load_user_defaults()
		{
			$this->def_options = \User_Defaults::load_defaults();

		}

		public function _render()
		{
			// hook called after processing postback
			$user = $this->get_option('username');
			$max = floor(\Formatter::changeBytes((int)$this->common_get_service_value('diskquota',
				'quota'), 'MB', $this->common_get_service_value('diskquota', 'units')));
			$this->add_javascript('var quota = {"max":' . $max . ', "default":' . ($this->def_options['disk_quota'] ?? 0) . '};' . "\n" .
				'var edituser  = {home:"' . \Util_Conf::home_directory($user) . '", username: "' . $user . '" };',
				'internal',
				false);
		}

		public function get_option($name, $type = 'text', $opt_cond = null)
		{
			if (!isset($this->options[$name])) {
				return '';
			}
			switch ($type) {
				case 'text':
					return $this->options[$name];
				case 'checkbox':
					return $this->options[$name] ? 'checked="1"' : '';
				case 'option':
					if (is_array($this->options[$name])) {
						return in_array($opt_cond, $this->options[$name]) ? 'SELECTED' : '';
					}

					return $this->options[$name] == '*' ||
					$this->options[$name] == $opt_cond ? 'SELECTED' :
						'';
			}

			return null;

		}

		public function get_options()
		{
			return $this->options;
		}

		public function getMode()
		{
			if (isset($_GET['user'])) {
				return 'user';
			}

			return 'add';
		}

		public function on_postback($params)
		{
			if (isset($params['user'])) {
				$settings = $this->get_user_settings($params['user']);;
				$this->def_options = $settings;
				if (isset($params['ch_user'])) {
					// save user settings
					$params['username'] = $params['user'];
					$this->options = $this->merge_options($params);
					$this->chuser($_GET['user'], $this->merge_options($params), $this->def_options);

					return;
				} else {
					// load user settings
					$this->options = $this->def_options;

					return;

				}
			} else if (isset($params['add_user'])) {
				if (!$params['add_user']) {
					return error("no username specified");
				}
				if ($this->bindless()->user_exists($params['username'])) {
					return error("user `" . $params['username'] . "' exists");
				}
				$this->add_user($params);
				if ($this->get_severity() < Error_Reporter::E_ERROR) {
					foreach (array('username', 'password', 'gecos') as $idx) {
						unset($this->options[$idx]);
					}
				} else if ($this->user_exists($params['username'])) {
					warn("User created with errors");
				}

				if (isset($params['setdefault'])) {
					\User_Defaults::set_defaults($params);
					// set new options as default
				}

			}

		}

		private function get_user_settings($user)
		{
			if (!$this->user_exists($user)) {
				return array();
			}
			$this->no_bind();
			$pwd = $this->user_getpwnam($user);
			$imap = $this->email_user_enabled($user, 'imap');
			$smtp = $this->email_user_enabled($user, 'smtp');
			$mailboxes = $this->email_list_mailboxes('local', $user);
			$ftp = $this->ftp_user_enabled($user);
			$quota = $this->user_get_quota($user);

			$subdomains = array();
			$sub_mode = null;
			$mail_domains = array();
			$uid = $this->user_get_uid_from_username($user);
			foreach ($mailboxes as $mail_domain) {
				if ($mail_domain['user'] != $user && $mail_domain['uid'] != $uid) {
					continue;
				}
				$mail_domains[] = $mail_domain['domain'];
			}

			$prefix = $this->getAuthContext()->domain_fs_path();
			foreach (glob($prefix . '/var/subdomain/' . $user . '{,.*}', GLOB_BRACE) as $subdomain) {
				$subdomain = basename($subdomain);
				if ($subdomain == $user) {
					$subdomains = '*';
					break;
				}
				$subdomains[] = $subdomain;
			}

			$opts = array();
			$opts['username'] = $user;
			/**
			 * in unlimited quotas qhard is reported as the account ceiling, but soft is 0
			 */
			$opts['disk_quota'] = !$quota['qsoft'] ? 0 : round($quota['qhard'] / 1024);
			$opts['gecos'] = htmlentities($pwd['gecos']);
			$opts['email'] = array_get(\Preferences::factory($user), 'email');
			$opts['email_domains'] = $mail_domains;
			$opts['email_create'] = sizeof($mail_domains) > 0;
			$opts['email_enable'] = $imap || $smtp;
			$opts['email_imap'] = $imap;
			$opts['email_smtp'] = $smtp;
			$opts['ftp_enable'] = $ftp;
			mute_warn();
			$opts['ftp_jail'] = $this->ftp_user_jailed($user);
			$opts['ftp_jail_path'] = null;
			if ($this->ftp_has_configuration($user)) {
				$opts['ftp_jail_path'] = (string)$this->ftp_get_option($user, 'local_root');
			}
			unmute_warn();

			$opts['ssh_enable'] = \Util_Conf::get_svc_config('ssh', 'enabled') && $this->ssh_user_enabled($user);
			$opts['crontab_enable'] = $opts['ssh_enable'] && $this->crontab_user_permitted($user);
			$opts['cp_enable'] = $this->auth_user_permitted($user, 'cp');
			$opts['dav_enable'] = $this->auth_user_permitted($user, 'dav');

			$opts['subdomain_enable'] = $subdomains === '*' || \count($subdomains) > 0;
			$opts['subdomain_domains'] = !$opts['subdomain_enable'] ? \Util_Conf::all_domains() : $subdomains;
			$opts['shell'] = $pwd['shell'] ?? '/bin/bash';
			$this->do_bind();

			return $opts;

		}

		private function merge_options($opts)
		{
			foreach ($this->def_options as $key => $val) {
				if (!isset($opts[$key])) {
					$opts[$key] = '';
				}
			}

			return array_merge($this->def_options, $opts);
		}

		private function chuser($user, $new_opts, $old_opts)
		{
			$ch_opts = array_merge($old_opts, \Util_PHP::array_diff_assoc_recursive($new_opts, $old_opts));
			if ($ch_opts['email']) {
				if (!preg_match(\Regex::EMAIL, $ch_opts['email'])) {
					return error("invalid email address `%s'", $ch_opts['email']);
				}
				$this->setEmail($user, $ch_opts['email']);
			}
			if ($ch_opts['disk_quota'] == 'none' || isset($ch_opts['disk-unlimited'])) {
				$ch_opts['disk_quota'] = 0;
			}
			if ($ch_opts['password']) {
				if ($ch_opts['password'] != $ch_opts['password_confirm']) {
					return error("password mismatch");
				}

				$this->auth_change_password($ch_opts['password'], $user);
				unset($this->options['password']);
			}
			if ($old_opts['gecos'] != $ch_opts['gecos'] || $old_opts['shell'] !== $ch_opts['shell']) {
				$this->user_usermod_driver($user, [
					'gecos' => $ch_opts['gecos'],
					'shell' => $ch_opts['shell'] ?: '/bin/bash'
				]);
			}

			if ($ch_opts['disk_quota'] != $old_opts['disk_quota']) {
				$this->user_change_quota($user, $ch_opts['disk_quota']);
			}

			mute_warn();
			if (!is_array($new_opts['email_domains'])) {
				$new_opts['email_domains'] = array();
			}
			if (!is_array($new_opts['subdomain_domains'])) {
				$new_opts['subdomain_domains'] = array();
			}
			/**
			 * Mailbox was enabled, disable and reject imap and smtp
			 */
			if ($old_opts['email_enable'] && !$ch_opts['email_enable']) {
				$email_domains = array();
				$new_opts['email_imap'] = $new_opts['email_smtp'] = false;
			} else {
				$email_domains = $new_opts['email_domains'];
			}

			if ($email_domains != $old_opts['email_domains']) {
				$this->email_address_setup($user, $email_domains);
			}

			if ($new_opts['email_imap'] != $old_opts['email_imap'] ||
				$new_opts['email_smtp'] != $old_opts['email_smtp']) {
				$this->email_toggle($user,
					$new_opts['email_imap'],
					$new_opts['email_smtp']);
			}
			if ($new_opts['ftp_enable'] != $old_opts['ftp_enable'] ||
				$new_opts['ftp_jail'] != $old_opts['ftp_jail'] ||
				$new_opts['ftp_jail_path'] != $old_opts['ftp_jail_path']) {
				$this->ftp_toggle($user, $ch_opts['ftp_enable'], $ch_opts['ftp_jail'], $ch_opts['ftp_jail_path']);

			}
			if ($old_opts['subdomain_domains'] == '*' &&
				sizeof($ch_opts['subdomain_domains']) != sizeof($this->get_domains()) &&
				$ch_opts['subdomain_domains'] != $old_opts['subdomain_domains'] ||
				$ch_opts['subdomain_enable'] != $old_opts['subdomain_enable']) {
				$subdomains = !$ch_opts['subdomain_enable'] ? array() : $new_opts['subdomain_domains'];
				$this->web_toggle($user, $subdomains);

			}
			if ($ch_opts['cp_enable'] != $old_opts['cp_enable']) {
				$this->cp_toggle($user, $ch_opts['cp_enable']);
			}

			if ($ch_opts['dav_enable'] != $old_opts['dav_enable']) {
				$this->dav_toggle($user, $ch_opts['dav_enable']);
			}

			if ($ch_opts['ssh_enable'] != $old_opts['ssh_enable']) {
				$this->ssh_toggle($user, $ch_opts['ssh_enable']);
			}

			if ($ch_opts['crontab_enable'] != $old_opts['crontab_enable']) {
				$this->crontab_toggle($user, $ch_opts['crontab_enable']);
			}

			unmute_warn();

			if (!$this->errors_exist()) {
				//header('Location: /apps/usermanage?edited', true, 302);
				return;
			}

		}

		public function setEmail($user, $email)
		{
			$auth = \Auth::context($user, \Session::get('domain'));
			$prefs = \Preferences::factory($auth);
			$prefs->unlock(\apnscpFunctionInterceptor::factory($auth));
			$prefs['email'] = $email;
		}

		private function email_address_setup($user, $email_domains)
		{
			//var_dump($user, $email_domains);
			$mailboxes = $this->email_list_mailboxes(\Email_Module::MAILBOX_SINGLE, $user);
			$uid = $this->user_get_uid_from_username($user);
			$domainset = array();
			foreach ($email_domains as $d) {
				$domainset[] = array(
					'domain' => $d,
					'uid'    => $uid,
					'type'   => \Email_Module::MAILBOX_USER
				);
			}
			if (!$mailboxes) {
				$filterkeep = $domainset;
				$filterremove = [];
			} else {
				$filterremove = array_udiff($mailboxes, $domainset, function ($a, $b) {
					if ($a['type'] != \Email_Module::MAILBOX_USER) {
						return 0;
					} else if ($a['uid'] != $b['uid']) {
						return 0;
					}

					return strcmp($a['domain'], $b['domain']);
				});
				$filterkeep = array_udiff($domainset, $filterremove, function ($a, $b) {
					if ($a['type'] != \Email_Module::MAILBOX_USER) {
						return 0;
					}
					if ($a['uid'] != $b['uid']) {
						return 0;
					}

					return strcmp($b['domain'], $a['domain']);
				});
			}
			foreach ($filterremove as $e) {
				$domain = $e['domain'];
				$this->email_delete_mailbox($user, $domain);
			}

			foreach ($filterkeep as $e) {
				$email_add = $e['domain'];
				if ($this->email_address_exists($user, $email_add)) {
					$mailbox = $this->email_list_mailboxes(null, $user, $email_add);
					if ($mailbox[0]['destination'] !== $user) {
						info("Mail address `%s@%s' already exists - not overwriting", $user, $email_add);
					}
					continue;
				}
				$this->email_add_mailbox($user, $email_add, $uid);
			}
		}

		private function email_toggle($user, $enable_imap = 1, $enable_smtp = 1)
		{
			if ($enable_imap) {
				$this->email_permit_user($user, 'imap');
			} else {
				$this->email_deny_user($user, 'imap');
			}

			if ($enable_smtp) {
				$this->email_permit_user($user, 'smtp');
			} else {
				$this->email_deny_user($user, 'smtp');
			}
		}

		private function ftp_toggle($user, $toggle = 1, $jail = 0, $jail_path = '')
		{
			if ($toggle) {
				$this->ftp_permit_user($user);
			} else {
				$this->ftp_deny_user($user);
			}

			if ($jail) {
				$this->ftp_jail_user($user, $jail_path);
			} else {
				$this->ftp_unjail_user($user);

			}

		}

		public function get_domains()
		{
			$domains = $this->aliases_list_aliases();
			$domains[] = \Util_Conf::get_svc_config('siteinfo', 'domain');
			asort($domains);

			return $domains;
		}

		private function web_toggle($user, $subdomains = array())
		{
			if (strstr($user, '_') && $subdomains) {
				return error("cannot enable subdomains for user with - in name");
			}
			$this->no_bind();
			mute_warn();
			$root = null;
			if ($this->ftp_has_configuration($user)) {
				$root = $this->ftp_get_option($user, 'local_root');
			}
			$subdomain_path = $root ? $root
				: '/home/' . $user . '/public_html';
			unmute_warn();

			if ($subdomains) {
				if (!$this->file_exists($subdomain_path)) {
					$this->file_create_directory($subdomain_path) &&
					$this->file_chown($subdomain_path, $user);
				}
				$prefix = $this->getAuthContext()->domain_fs_path();
				if (strstr($subdomain_path, '/home/') &&
					!is_readable($prefix . '/home/' . $user)) {
					$this->file_chmod('/home/' . $user, 711);
				}

				$domains = $this->get_domains();
				if (sizeof($domains) == sizeof($subdomains)) {
					$this->web_add_subdomain($user, $subdomain_path);
				} else {
					if (file_exists($prefix . '/var/subdomain/' . $user)) {
						$this->web_remove_subdomain($user);
					}
					foreach ($subdomains as $subdomain) {
						$this->web_add_subdomain($user . '.' . $subdomain, $subdomain_path);
					}
				}
			} else {
				$this->web_remove_user_subdomain($user);
			}
			$this->do_bind();
		}

		private function cp_toggle($user, $toggle = 1)
		{
			if ($toggle && !$this->bindless()->auth_user_permitted($user, 'cp')) {
				return $this->auth_permit_user($user, 'cp');
			}

			return $this->bindless()->auth_user_permitted($user, 'cp') && $this->auth_deny_user($user, 'cp')
				|| true;
		}

		private function dav_toggle($user, $toggle = 1)
		{
			if ($toggle && !$this->bindless()->auth_user_permitted($user, 'dav')) {
				return $this->auth_permit_user($user, 'dav');
			}

			return $this->bindless()->auth_user_permitted($user, 'dav') && $this->auth_deny_user($user, 'dav')
				|| true;
		}

		private function ssh_toggle($user, $enable = 1)
		{
			if (!$enable) {
				$this->ssh_deny_user($user);
				$this->crontab_deny_user($user);
			} else {
				$this->ssh_permit_user($user);
			}
		}

		private function crontab_toggle($user, $enable = 1)
		{
			if (!$enable) {
				$this->crontab_deny_user($user);
			} else {
				$this->crontab_permit_user($user);
			}
		}

		private function add_user($params)
		{
			$user = $params['username'];
			$userorig = $user;
			$user = strtolower($user);
			if ($user !== $userorig) {
				warn("username `$userorig' converted to lowercase");
			}
			if ($params['email'] && !preg_match(\Regex::EMAIL, $params['email'])) {
				return error("Invalid email addresss `%s'", $params['email']);
			}
			$this->options = $this->merge_options($params);
			if (isset($params['random-password'])) {
				$params['password'] = Password::generate(16);
			} else if ($params['password'] != $params['password_confirm']) {
				return error("Password confirmation mismatch");
			}

			if (isset($params['disk-unlimited']) || $params['disk_quota'] == 'none') {
				$params['disk_quota'] = 0;
			}
			$success = $this->user_add($user,   // username
				$params['password'],   // password
				$params['gecos'],
				$params['disk_quota'],
				[
					'ftp'   => isset($params['ftp_enable']),
					'cp'    => isset($params['cp_enable']),
					'ssh'   => isset($params['ssh_enable']) && \Util_Conf::get_svc_config('ssh', 'enabled'),
					'imap'  => isset($params['email_imap']),
					'smtp'  => isset($params['email_smtp']),
					'shell' => $params['shell'] ?? null
				]);
			if (!$success) {
				return false;
			}

			if ($params['email']) {
				$this->setEmail($user, $params['email']);
			}

			$email_domains = array();
			if (isset($params['email_enable']) && isset($params['email_create']) && !isset($params['email_domains'])) {
				warn("e-mail enabled, but no domains assigned for e-mail");
			} else if (isset($params['email_domains'])) {
				$email_domains = $params['email_domains'];
			}

			if ($email_domains) {
				$this->email_address_setup($user, $email_domains);
			}

			if (isset($params['ftp_enable']) && isset($params['ftp_jail'])) {
				$this->ftp_jail_user($user, $params['ftp_jail_path']);
			}

			if (isset($params['ssh_enable']) && \Util_Conf::get_svc_config('ssh', 'enabled')) {
				// crontab support may be on a deny basis by default,
				// explicitly name user for support
				if (isset($params['crontab_enable'])) {
					$this->crontab_permit_user($user);
				} else {
					$this->crontab_deny_user($user);
				}

			}

			if (isset($params['subdomain_enable']) && isset($params['subdomain_domains'])) {
				$this->web_toggle($user, $params['subdomain_domains']);
			}
		}

		public function get_mode()
		{
			return isset($_GET['user']) ? 'edit' : 'add';
		}

		public function get_mailbox_domains()
		{
			return $this->email_list_virtual_transports();
		}

		private function set_def_options($opts)
		{
			$this->def_options = $opts;
		}
	}
