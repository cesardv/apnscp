<div class="d-flex align-content-around">
	@if ($Page->get_mode() == 'edit')
		<div class="head5 ml-auto d-none d-md-block">
			<a class="ui-action ui-action-switch-app ui-action-label" href="/apps/usermanage">Return to User
				Overview</a>
		</div>
	@elseif ($Page->get_mode() === 'add')
		<div class="head5 ml-auto d-none d-md-block">
			Change Mode: <a class="ui-action ui-action-switch-app ui-action-label" href="/apps/usermanage">Manage
				Users</a>
		</div>
	@endif
</div>

<form method="post" class="" data-toggle="@if ($Page->get_mode() == 'add') validator @endif" id="userForm">
	@includeWhen($Page->get_mode() != 'defaults', 'partials.edit-fields')

	<h4 class="">General Service Configuration</h4>
	<hr/>
	<fieldset class="form-group my-3 row">
		<h5 class="col-12 form-control-label bg-white">
			<i class="fa fa-hdd-o"></i>
			Storage
		</h5>
		<div class="col-sm-6 form-inline">
			<label class="pl-0 form-inline custom-checkbox custom-control align-items-center mr-0 d-flex">
				<input type="checkbox" class="custom-control-input" name="disk-unlimited"
				       id="disk_unlimited" value="0" @if (!$Page->get_option('disk_quota')) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				Unlimited
			</label>

			<span class="text-muted mx-3">or</span>

			<div class="form-inline d-inline-block">
				<div class="d-flex align-items-center">
					<input type="text" id="disk_quota" name="disk_quota" class="d-inline-block form-control mr-1" min="0" size="4" value="{{ $Page->get_option('disk_quota') }}"/> MB
				</div>
				<div id='disk_slider' class='mt-1 ui-slider-1 hidden-sm-down'></div>
			</div>
		</div>
	</fieldset>
	<hr/>
	@if (\cmd('email_configured'))
		<fieldset class="form-group my-3 row">
			<h5 class="col-12 form-control-label">
				<i class="fa fa-envelope"></i>
				Email
			</h5>
			<div class="col-sm-6">
				<div class="d-flex align-items-center">
					<label class="pl-0 mb-0 form-inline custom-checkbox custom-control align-items-center d-inline-flex">
						<input type="checkbox" class="custom-control-input" name="email_enable" id="email_enable"
						       value="1" {{ $Page->get_option('email_enable', 'checkbox') }} />
						<span class="custom-control-indicator"></span>
						Enable
					</label>

					<button name="Advanced" type="button" id="email_advanced" value="Advanced" class="btn btn-secondary"
					        data-toggle="collapse"
					        data-target="#emailOptions" aria-expanded="false" aria-controls="emailOptions">
						<i class="ui-action-advanced ui-action"></i>
						Advanced
					</button>
				</div>

				<div id="emailOptions" class="collapse mt-2">
					<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-flex">
						<input type="checkbox" class="custom-control-input" name="email_smtp" value="1"
						       id="email_smtp" {{ $Page->get_option('email_smtp', 'checkbox') }} />
						<span class="custom-control-indicator"></span>
						User may send email through server
					</label>

					<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-flex">
						<input type="checkbox" class="custom-control-input" name="email_imap" value="1"
						       id="email_imap" {{ $Page->get_option('email_imap', 'checkbox') }} />
						<span class="custom-control-indicator"></span>
						User may login to IMAP/POP3 server
					</label>

					@if ($Page->get_mode() == 'add')
						<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-flex">
							<input type="checkbox" class="custom-control-input" name="email_create" value="1"
							       id="email_create" {{ $Page->get_option('email_create', 'checkbox') }} />
							<span class="custom-control-indicator"></span>
							Create email addresses named after user
						</label>
					@endif

					<label>
						Create e-mail addresses for the selected domains:
					</label>
					<select name="email_domains[]" class="custom-select form-control" id="email_domains" multiple="1">
						@foreach (\Util_Conf::mailbox_domains() as $domain)
							<option value="{{ $domain }}" {{ $Page->get_option('email_domains', 'option', $domain) }}
								>{{ $domain }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</fieldset>
	@endif

	@if (\cmd('ftp_enabled'))
		<hr/>
		<fieldset class="form-group my-3 row">
			<h5 class="col-12 form-control-label">
				<i class="fa fa-upload"></i>
				FTP
			</h5>
			<div class="col-md-8">
				<div class="align-items-center d-flex">
					<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-inline-flex mb-0">
						<input type="checkbox" class="custom-control-input" name="ftp_enable" id="ftp_enable"
						       value="1" {{ $Page->get_option('ftp_enable', 'checkbox') }} />
						<span class="custom-control-indicator"></span>
						Enable
					</label>
					@if ($Page->get_mode() != 'defaults')
						<button type="button" name="Advanced" id="ftp_advanced" value="Advanced" class="btn btn-secondary"
						        data-toggle="collapse"
						        data-target="#ftpOptions" aria-expanded="false" aria-controls="ftpOptions">
							<i class="ui-action-advanced ui-action"></i>
							Advanced
						</button>
					@endif

					<div class="inline-block" data-toggle="tooltip" title="Users may not access folders outside of the jail">
						<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-flex mb-0 ml-3">
							<input type="checkbox" class="custom-control-input" name="ftp_jail" id="ftp_jail"
							       value="1" {{ $Page->get_option('ftp_jail', 'checkbox') }} />
							<span class="custom-control-indicator"></span>
							Jail to home directory
						</label>
					</div>
				</div>
				@if ($Page->get_mode() != 'defaults')
					<div class="row ">
						<div id="ftpOptions" class="col-12 mt-2 collapse">
							<label>Custom Home Directory</label>
							<div class="row">
								<div class="col-xl-6 col-12 input-group">
									<input type="text" class="form-control" placeholder="Select a path" name="ftp_jail_path"
									       value="{{ $Page->get_option('ftp_jail_path') }}" id="custom_jail"
									       class="dir_browser"/>
									<div class="btn-group">
	                                    <button type="button" id="jail_chpath" class="btn btn-secondary" name="jail_chpath">
	                                        <i class="fa fa-folder-open"></i>
	                                        Browse
	                                    </button>
	                                </div>
								</div>
							</div>
							<span class="text-muted">
	                            <i class="fa fa-sticky-note-o"></i>
	                            Will be owned by the user, leave blank for default
	                        </span>
						</div>
					</div>
				@endif
			</div>
		</fieldset>
	@endif

	@if (DAV_ENABLED)
		<hr/>
		<fieldset class="form-group my-3 row">
			<h5 class="col-12 form-group-label">
				<i class="fa fa-upload"></i>
				WebDAV
				<span class="small" data-toggle="tooltip"
				      title="Includes Web Disk (Files > Web Disk). Unlike FTP, users cannot be jailed."><i
						class="fa fa-sticky-note-o"></i> What's this?</span>
			</h5>
			<div class="col-sm-6">
				<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-flex">
					<input type="checkbox" class="custom-control-input" name="dav_enable" id="dav_enable"
					       value="1" {{ $Page->get_option('dav_enable', 'checkbox') }} />
					<span class="custom-control-indicator"></span>
					Enable
				</label>
			</div>
		</fieldset>
	@endif
	<hr/>
	<fieldset class="form-group my-3 row">
		<h5 class="col-12 form-control-label">
			<i class="fa fa-desktop"></i>
			Subdomain
		</h5>
		<div class="col-sm-6">
			<div class="d-flex align-items-center">
				<label class="mb-0 pl-0 form-inline custom-checkbox custom-control align-items-center d-inline-flex">
					<input type="checkbox" class="custom-control-input" name="subdomain_enable" id="subdomain_enable"
					       value="1" {{ $Page->get_option('subdomain_enable', 'checkbox') }} />
					<span class="custom-control-indicator"></span>
					Enable
				</label>
				<button type="button" name="Advanced" id="subdomain_advanced" value="Advanced" class="btn btn-secondary" data-toggle="collapse"
					data-target="#subdomainOptions" aria-expanded="false" aria-controls="subdomainOptions">
					<span class="ui-action-advanced ui-action"></span>
					Advanced
				</button>
			</div>
			<div id="subdomainOptions" class="collapse mt-2">
				Create subdomain on following domains: <br/>
				<select name="subdomain_domains[]" class="custom-select form-control" id="subdomain_domains" multiple="1">
					@foreach (\Util_Conf::all_domains() as $domain)
						<option value="{{ $domain }}" {{ $Page->get_option('subdomain_domains', 'option', $domain) }}
							>{{ $domain }}</option>
					@endforeach
				</select>
			</div>
		</div>
	</fieldset>
	<hr/>
	<fieldset class="form-group my-3 row">
		<h5 class="col-12 form-control-label">
			<i class="fa fa-rocket"></i>
			Control Panel
		</h5>
		<div class="col-sm-6">
			<label class="pl-0 form-inline custom-checkbox custom-control mt-1 align-items-center d-flex">
				<input type="checkbox" class="custom-control-input" name="cp_enable" id="cp_enable" value="1"
						{{ $Page->get_option('cp_enable', 'checkbox') }} />
				<span class="custom-control-indicator"></span>
				Enable
			</label>
		</div>
	</fieldset>
	<hr/>
	@if (\Util_Conf::get_svc_config('ssh', 'enabled'))
	<fieldset class="form-group my-3 row">
		<h5 class="col-12 form-group-label">
			<i class="fa fa-terminal"></i>
			Terminal
		</h5>

		<div class="col-12">
			<label class="pl-0 form-inline custom-checkbox custom-control mt-1 align-items-center d-flex">
				<input type="checkbox" class="custom-control-input" name="ssh_enable" id="ssh_enable"
				       value="1" {{ $Page->get_option('ssh_enable', 'checkbox') }} />
				<span class="custom-control-indicator"></span>
				Enable
			</label>
			<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-flex">
				<input type="checkbox" class="custom-control-input" name="crontab_enable" id="crontab_enable"
				       value="1" {{ $Page->get_option('crontab_enable', 'checkbox') }} />
				<span class="custom-control-indicator"></span>
				Task scheduling (crontab)
			</label>
		</div>
		<div class="col-12">
			<label class="form-control-label">
				Login shell
				<select name="shell" class="custom-select">
					@php
						$activeShell = $Page->get_option('shell', 'text');
					@endphp
					@foreach (\cmd('user_get_shells') as $shell)
						<option value="{{ $shell }}" @if ($activeShell == $shell) selected="SELECTED" @endif>
							{{ $shell }}
						</option>
					@endforeach
				</select>
			</label>
		</div>
	</fieldset>
	@endif

	<div class="row">
		<div class="col-12">
			@if ($Page->get_mode() == 'add')
				<button type="submit" class="btn btn-primary my-3" value="Add User" name="add_user">
					Add User
				</button>
			@elseif ($Page->get_mode() == 'defaults')
				<button type="submit" class="btn btn-primary my-3" value="Set Defaults" name="save">
					Set Defaults
				</button>
			@else
				<button type="submit" class="btn btn-primary my-3" value="Save Changes" name="ch_user">
					Save Changes
				</button>
			@endif
			<button type="reset" class="btn btn-secondary ml-3 my-3 warn" value="Reset" name="reset">
				Reset
			</button>
		</div>
	</div>

	@if ($Page->get_mode() === "add")
		<fieldset class="row form-group">
			<div class="col-12 mt-1">
				<label class="pl-0 form-inline custom-checkbox custom-control mt-1 align-items-center d-flex">
					<input type="checkbox" class="custom-control-input" name="setdefault" value="1" id="setdefault"/>
					<span class="custom-control-indicator"></span>
					Set as default for new users
				</label>
			</div>
		</fieldset>
	@endif
</form>

<form method="post" action="{{ HTML_Kit::page_url_params() }}" class="hide" id="change-form">
	<fieldset class="form-group  py-3">
		<label class="" for="new-data">New Username</label>
		<div class=" input-group">
			<div class="input-group-addon">
				<i class="fa fa-user"></i>
			</div>
			<input id="new-data" class="form-control" type="text" name="new-value"
			       value="{{ $Page->get_option('username') }}" />
		</div>
	</fieldset>
	<button type="submit" name="change" class="btn btn-primary" value="1">
		Change Username
	</button>
</form>
