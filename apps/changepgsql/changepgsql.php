<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\changepgsql;

	use Page_Container;

	class Page extends Page_Container
	{

		public $upload_id;

		public function __construct()
		{
			parent::__construct();
			if ($this->getMode() !== "list") {
				$this->upload_id = $upload_id = uniqid();
				$maxsize = \HTML_Kit::maxUpload();
				$this->add_javascript('var upload_id=' . "'" . $upload_id . "', MAX_SIZE=" . $maxsize . ";", 'internal',
					false, true);
				$this->init_js('upload');
			}

			$this->add_javascript('/apps/changemysql/mysql_manager.js');
			$this->add_css('/apps/changemysql/changemysql.css');
			$this->add_javascript('var __state = "' . $this->getMode() . '";', 'internal', false, true);
			if ($this->isModeSwitch('mode')) {
				$this->hide_pb();
			}

		}

		public function getMode()
		{
			if (isset($_GET['mode']) && $_GET['mode'] == 'list') {
				return $_GET['mode'];
			}

			return 'add';
		}

		public function _render()
		{
			$this->view()->share([
				'enabled' => $this->pgsql_enabled(),
				'mode'    => $this->getMode()
			]);
		}

		public function on_postback($params)
		{

			//var_export($params);
			if (isset($params['Create_User'])) {
				$status = $this->
				sql_add_pgsql_user($params['new_user_name'],
					$params['password_confirm'],
					$params['new_max_connections']);
				$this->bind($status);

				return $status;
			} else if (isset($params['d'])) {
				$user = \Util_PHP::unserialize(base64_decode($params['d']));
				$status = $this->sql_delete_pgsql_user($user, false);
				$this->bind($status);

				return $status;
			} else if (isset($params['Vacuum'])) {
				$a = array_keys($params['Vacuum']);
				$status = $this->sql_pg_vacuum_db(array_pop($a));
				// vacuum output
				if (!$status instanceof FileError) {
					$this->bind($status);
				} else {
					warn($status->message);
				}

				return $status;
			} else if (isset($params['save_changes'])) {
				$user = \Util_PHP::unserialize(base64_decode($params['state']));
				$status = $this->
				sql_edit_pgsql_user($user,
					($params['password'] ? $params['password'] : null),
					(int)$params['max_connections']);
				$this->bind($status);

				return $status;
			} else if (isset($params['CreateDB'])) {
				$status = $this->sql_create_pgsql_database($params['database_name']);
				$this->bind($status);
				if (isset($params['backup_db']) && !$this->errors_exist()) {
					$status = $this->sql_add_pgsql_backup(
						\Util_Conf::get_svc_config('mysql', 'dbaseprefix') . $params['database_name'],
						$params['b_extension'],
						$params['b_frequency'],
						$params['b_hold']
					);
					$this->bind($status);
					if ($status && isset($params['file']) && !strncmp($params['file'], '/tmp/', 5)) {
						$this->sql_import_pgsql($params['database_name'], $params['file']);
						$this->file_delete($params['file']);
					}
				}

				return $status;
			} else if (isset($params['drop'])) {
				$status = $this->sql_delete_pgsql_database($params['drop']);
				$this->bind($status);

				return $status;
			} else if (isset($params['Save_Grants'])) {
				$params = array_change_key_case($params);
				list($user, $host, $db) = \Util_PHP::unserialize(base64_decode($params['state']));
				$output = $this->sql_add_pgsql_user_permissions($user, $host, $db, $params);
				$this->bind($output);

				return $output;
			} else if (isset($params['export'])) {
				$dbs = array_keys($params['export']);
				// single export for now
				$db = array_pop($dbs);
				$path = $this->sql_export_pgsql_pipe($db);
				if (!$path) {
					return error("failed to initialize file download");
				}

				$fname = sprintf("%s-%s.sql", $db, date('Ymd'));
				\HTML_Kit::makeDownloadable($path, $fname);
				return;
			} else if (isset($params['empty'])) {
				foreach (array_keys($params['empty']) as $empty) {
					$this->sql_empty_pgsql_database($empty);
				}
				if (!$this->pb_succeeded()) {
					return;
				}
			}
		}

		public function getDatabases()
		{
			return $this->sql_list_pgsql_databases();
		}

		public function userHasPerms($mName, $mDB)
		{
			return $this->sql_get_pgsql_user_permissions($mName, $mDB);
		}

		public function getDBSize($mName)
		{
			return \Formatter::commafy(\Formatter::reduceBytes($this->sql_get_database_size('postgresql', $mName)));
		}

		public function getPerms($mUser, $mHost, $mDB)
		{
			return $this->sql_get_pgsql_user_permissions($mUser, $mHost, $mDB);
		}

		public function getUsers()
		{
			return $this->sql_list_pgsql_users();
		}

		public function getDBPrefix()
		{
			return $this->sql_get_prefix();
		}
	}

?>
