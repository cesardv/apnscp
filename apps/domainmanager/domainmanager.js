$(document).ready(function () {
	apnscp.hinted();
	$('[data-toggle=tooltip]').tooltip();
	var __modify_htmldir = true;
	$('#enable_email').click(function () {
		if ($(this).prop('checked')) {
			$('#clone_domain_row').show();
		} else {
			$('#clone_domain_row').hide();
		}
		return true;
	});

	$('#type').change(function () {
		var type = $(this).val(),
			$el = $($('option:selected', this).data('target'));
		$el.siblings().hide().end().show();

		return true;
	}).change();
	$('#addForm').submit(function () {
		// disabling input inhibits posting the form control...
		var $el = $('<input type="hidden" value="1" />').attr('name', $('#submitBtn').attr('name'));
		$('i', '#submitBtn').removeClass('ui-action-add').addClass('fa-spinner fa-pulse').end().prop('disabled', true).append($el);

	});
	$('#browse-docroot, #browse-home').bind({
		click: function (e) {
			var home, re, selectField;
			switch (e.target.id) {
				case 'browse-docroot':
					home = '/var/www';
					selectField = '#doc_root_path';
					break;
				case 'browse-home':
					home = $('#user-home-extra select[name=user_home] :selected').val();
					selectField = '#user_path';
					break;
			}
			var re = new RegExp(
				'^' + home + '/?'
			);

			apnscp.explorer({
				filter: 'filter=file,0;chown,1;show,' + home,
				selectField: $(selectField),
				onSelect: function (file) {
					file = file.replace(re, '');
					$('#selected_dir').text(file);
				}
			}, home);
			return false;

		}
	});

	$('#user-list').change(function () {
		var user = $(this).val();
		$('#userPath').text(user + "/");
	});
	$('#domain').change(function () {
		if (!__modify_htmldir) return false;
		suggested = $(this).val().replace(/^www\./i, "");
		$('#doc_root_path').val(suggested);
		$('#user_path').val(suggested);
	});

	/**
	 * Filter events
	 */
	$('#domainFilter').click(function (e) {
		if (e.target.id == "do_filter") {
			var field = $('#filter_spec').val(),
				pattern = $('#filter').val(),
				re = new RegExp(pattern, "i"),
				selector = '.' + field;
			$('.ui-app .entry').filter(function () {
				return re.test($(this).find('input', selector).val()) == false;
			}).fadeOut('fast', function () {
				$(this).removeClass('ui-highlight').find(':checkbox').prop('checked', false);
			});
			var $el = $('<li class="ui-action-delete filter-' + field + '" />').text(pattern).click(function () {
				remove_filter.apply(this, [pattern, $(this).data("selector")]);
				return false;
			});
			$el.data("selector", selector);
			$(this).find('.ui-active-filters').append($el);
			if ($('#reset_filter:hidden').length > 0) {
				$('#reset_filter').fadeIn();
			}
			return false;
		}
		if (e.target.id == "reset_filter") {
			$(this).find('.ui-active-filters').empty();
			$('#reset_filter').hide();
			return $('.ui-app .entry:hidden').fadeIn();
		}
	}).bind('keydown', function (e) {
		if (e.keyCode === $.ui.keyCode.ESCAPE) {
			$('#reset_filter').click();
			$('#filter').val('');
		} else if (e.keyCode === $.ui.keyCode.ENTER) {
			$(this).triggerHandler('click');
		}
	});
	;

	/**
	 * Actions column events
	 */
	$('.entry .actions').click(function (e) {
		var $target = $(e.target);
		if ($target.hasClass('ui-action-manage-files')) {
			var win = window.open('/apps/filemanager?mini&cwd=' + $target.val(), 'file-manager-window', 'menubar=no,toolbar=no');
			win.focus();
			return false;
		} else if ($target.hasClass('ui-action-delete')) {
			return confirm("Do you want to delete the domain " + e.target.value + "?");
		} else if ($target.hasClass('ui-action-edit')) {
			make_editable.apply(this, [e.target.value]);
			return false;
		}
		return true;
	});

	// user furnished directory for HTML location
	$('#doc_root_path, #user_path').each(function () {
		$(this).change(function () {
			__modify_htmldir = false;
			return true;
		});
	});
});

function remove_filter(pattern, selector) {
	var re = new RegExp(pattern, "i")
	$('div.entry:hidden').filter(function () {
		return re.test($(this).find('input', selector).val()) == false;
	}).fadeIn('fast');

	$(this).remove();
	if ($('.ui-active-filters li').length == 0) {
		$('#reset_filter').fadeOut();
	}
	return false;
}

function make_editable(domain) {
	var $row = $(this).parent();
	$(':input[readonly]', $row).each(function () {
		if ($(this).data('immutable')) {
			return;
		}
		var $html;

		switch ($(this).parent().data('source')) {
			case 'user':
				var owner = $(this).val();
				$html = $('<select name="user"  class="removable form-control custom-select"/>');
				for (user in users) {
					var selected = '', user = users[user];
					if (user == owner)
						selected = " selected='SELECTED'";
					$html.append($('<option value="' + user + '"' + selected + '>' + user + '</option>'));
				}
				break;
			case 'file':
				var path = $(this).val(), $selectField = $(this);
				$html = $('<button id="browse" type="button" class="removable btn btn-secondary" name="browse_path">\n' +
					'<i class="fa fa-folder-open"></i>\n' +
					'Browse\n' +
					'</button>').click(function() {
						var re = new RegExp(
							'^' + path + '/?'
						);

						apnscp.explorer({
							filter: 'filter=file,0;chown,1;show,/var/www;show,/home',
							selectField: $selectField,
							onSelect: function (localFile) {
								$('#selected_dir').text(localFile);
							}
						}, path);
				});
				break;
		}
		$prev = $('<input type="hidden" />').attr({
			name: $(this).attr("name") + '-old',
			value: $(this).val()
		});
		var $el = $(this).prop('readonly', false).attr('data-original', $(this).val()).find(':hidden').remove().end().after([$prev, $html]);
	});
	var $reset = $('<button />').attr({
		'class': 'btn btn-secondary warn ml-1',
		'name': 'reset',
		'type': 'button'

	}).text("Reset").click(function () {
		var $row = $(this).closest('.row');
		$(':input[data-original]', $row).each(function () {
			$(this).attr({
				type: 'text'
			}).val($(this).data('original')).prop('readonly', true).siblings(':hidden').remove();
		});
		$row.find('.removable').remove();
		$(this).closest('.save-group').siblings('.input-group').show().end().remove();
		return true;
	});

	var $saveGroup = $(
		'<div class="btn-group save-group">' +
		'<button type="submit" class="btn btn-primary" name="edit">Save</button>' +
		'</div>'
	).append($reset);
	$('.actions .input-group', $row).hide().parent().append($saveGroup);

	return false;
}