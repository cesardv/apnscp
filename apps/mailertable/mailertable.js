$(document).ready(function () {
	$('#subdomain, #domain').change(function (e) {
		var empty, fill, $populate,
			name = e.target.name;
		if (name == 'subdomain') {
			empty = 'domain';
			fill = 'subdomain';
		} else {
			empty = 'subdomain';
			fill = 'domain';
		}

		var val = $('#' + fill).val(),
			$populate = $('#' + empty), oldVal;
		oldVal = $populate.val();

		$populate.empty();

		for (var item in host_map[name][val]) {
			var i = host_map[name][val][item];
			$populate.append(
				$('<option ' + (i == oldVal ? 'selected="SELECTED"' : '') + 'value="' + i + '">' + (i == "" ? '&lt;none&gt;' : i) + '</option>')
			);
		}
		updatePreview();
		return true;
	}).change();

	$('#entry-form').submit(function () {
		$(this).find('.btn-primary').prop("disabled", true).append(apnscp.indicator());
		return true;
	})
	$('#routes .ui-action-delete').click(function () {

		var route = $(this).text(),
			hash = $(this).attr('rel'),
			$conf = $("<div>Are you sure you want to deauthorize this server from handling mail for" +
				" <b>" + route + "</b>?</div>");
		var modal = apnscp.modal($conf, {
			buttons:
				[
					$("<button type='button' class='btn btn-secondary ui-action-delete warn'>").text("Delete").click(function () {
						var location = window.location.pathname + "?d=" + hash;
						apnscp.assign_url(location);
					})
				]
		});
		modal.modal();
		return false;
	});
});

function updatePreview() {
	var subdomain = $('#subdomain').val(),
		domain = $('#domain').val(),
		$exsub = $('#ex-subdomain'),
		$exdomain = $('#ex-domain');
	if (subdomain) {
		$exsub.text(subdomain).show();
	} else {
		$exsub.hide();
	}
	$exdomain.text(domain);
}