<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\diskbd;

	use Error_Reporter;
	use Image_Color;
	use Page_Container;

	class Page extends Page_Container
	{
		private $quota_list = array();
		private $service_list;

		public function __construct()
		{
			parent::__construct();
			$this->add_css('diskbd.css');

			$this->add_javascript('diskbd.js');

			$scripts = array('flot', 'flot.pie');

			$browser = \HTML_Kit::get_browser();
			call_user_func_array(array($this, 'init_js'), $scripts);
		}

		public static function get_storage()
		{
			$page = new Page();

			return $page->getData();
		}

		public function getData()
		{
			$key = 'diskbd.data';
			$cache = \Cache_Account::spawn();
			$storage = $cache->get($key);
			if ($storage) {
				return $storage;
			}

			$storage = array(
				'users'    => $this->generateQuotas(),
				'services' => $this->generateServiceUsage()
			);
			$admin = \Util_Conf::call('common_get_admin_username');
			$storage['users'][$admin]['used'] -= $storage['services']['logs']['used'];
			foreach ($storage as $category => $items) {
				$colors = $this->_assignColors($items, $category);
				$storage[$category] = array_merge($storage[$category], $colors);
			}

			// fix apache guestimate
			$sum = 0;
			foreach (array_keys($storage) as $k) {
				foreach ($storage[$k] as $s) {
					$sum += $s['used'];
				}
			}
			// logs are under the same uid as the account holder

			// 4 KB block size

			// in earlier iterations, if apache used < 0 it is skipped
			// this could indicate an extraordinary circumstance such if
			// mail pickup is polluted with messages from the uid,
			// which is not counted but will adjust the apache quota < 0
			$apache = $storage['users'][\Web_Module::WEB_USERNAME]['used'];
			$drift = ceil($apache / FILESYSTEM_BLKSZ) * FILESYSTEM_BLKSZ;
			$storage['users'][\Web_Module::WEB_USERNAME]['used'] = max(0, $drift);
			if ($storage['users'][\Web_Module::WEB_USERNAME]['used'] < 0) {
				$msg = "Anomalous storage usage - " . \UCard::get()->getDomain();
				Error_Reporter::report($msg);
			}
			// $apache = $storage['users']['apache'];
			// calc new total with drift removed
			//$storage['users']['apache']['value'] =
			//    ($apache['quota']-$drift)/(($apache['quota']+$drift)/$apache['value']);
			$cache->set($key, $storage, 60);

			return $storage;
		}

		// wrapper to getData for AJAX

		public function generateQuotas()
		{
			if ($this->quota_list) {
				return $this->quota_list;
			}
			$max = $this->site_get_account_quota();
			$users = $this->getUsers();
			$quotas = $this->user_get_quota($users);
			$total = array(
				'quota' => 0,
				'label' => 'Total',
				'files' => 0,
				'total' => 0
			);
			foreach ($users as $user) {
				$quota = $quotas[$user];
				$this->quota_list[$user] = array(
					'used'  => $quota['qused'],
					'label' => $user,
					'files' => $quota['fused']
				);
				$total['quota'] += $quota['qused'];
				$total['files'] += $quota['fused'];
			}

			return $this->quota_list;
		}

		public function getUsers()
		{
			$users = array_keys($this->user_get_users());
			$users[] = \Web_Module::WEB_USERNAME;

			return $users;
		}

		public function generateServiceUsage()
		{
			$this->service_list = array();

			$this->service_list['logs'] = array(
				'label' => 'Web Logs',
				'used'  => $this->logs_get_webserver_log_usage(),
				'files' => null,
			);

			if ($this->sql_enabled('mysql')) {
				$mysql_size = 0;
				foreach ($this->sql_list_mysql_databases() as $db) {
					$mysql_size += $this->sql_get_database_size('mysql', $db);
				}
				// KB
				$mysql_size /= 1024;
				$this->service_list['mysql'] = array(
					'label' => 'MySQL',
					'used'  => $mysql_size,
					'files' => null
				);
			}

			if ($this->sql_enabled('postgresql')) {
				$postgresql_size = 0;
				foreach ($this->sql_list_pgsql_databases() as $db) {
					$postgresql_size += $this->sql_get_database_size('postgresql', $db) / 1024;
				}
				$this->service_list['postgresql'] = array(
					'label' => 'PostgreSQL',
					'used'  => $postgresql_size,
					'files' => null,
				);
			}

			return $this->service_list;
		}

		private function _assignColors(array $items, $category)
		{
			foreach ($items as $key => $b) {
				$item = $key;
				$extinfo = null;
				if (isset($b['extended'])) {
					// via bandwidth_get_by_date
					$extinfo = $b['extended'];
				}

				$b['color'] = $this->_setColor($category, $item, $extinfo);
				$items[$key] = $b;
			}

			return $items;
		}

		/**
		 * Set hex color for item on graph
		 *
		 * @param string $item
		 * @param string $graph
		 * @param string $color hex color
		 */
		private function _setColor($type, $item, $extinfo = null, $color = null)
		{
			if (is_null($color)) {
				list ($h, $s, $l) = Image_Color::randhsl();
				$color = Image_Color::hsl2hex($h, $s, $l);
			} else {
				if ($color[0] != '#' || strspn($color, 'abcdef0123456789', 1)) {
					return error("invalid hex value `%s'", $color);
				}
			}
			if ($extinfo) {
				$item = $item . ' ' . $extinfo;
			}
			if (!isset($this->_legend[$type])) {
				$this->_legend[$type] = array();
			}
			$this->_legend[$type][$item] = $color;

			return $color;
		}

		public function on_postback($params)
		{
			$prefix = $this->getAuthContext()->domain_fs_path();
			$home = \Util_Conf::home_directory();
			if (isset($params['get_list']) &&
				false !== ($file = $this->user_generate_quota_list())) {
				if (!$this->file_copy($home . '/' . $file, '/tmp/' . $file, true)) {
					fatal("unable to copy file list to /tmp");
				}
				$this->file_delete($home . '/' . $file);

				header('Content-Disposition: attachment; filename="filelist-' . \Util_Conf::login_domain() . '.txt"');
				header("Content-Type: application/octet-stream");
				header("Content-Transfer-Encoding: binary");
				header("Pragma: no-cache");
				header("Content-Length: " . filesize($prefix . '/tmp/' . $file));
				header("Expires: 0");

				ob_clean();
				readfile($prefix . '/tmp/' . $file);
				register_shutdown_function(array($this, 'cleanup'), $file);
				exit();
			}
		}

		/**
		 * Remove file created by user_generate_quota_list
		 *
		 * @param string $file
		 */
		public function cleanup($file)
		{
			$this->file_exists('/tmp/' . $file) &&
			$this->file_delete('/tmp/' . $file);
		}

		public function getEmailSpoolSize($user)
		{
			if ($user == \Web_Module::WEB_USERNAME) {
				return null;
			}
			$status = $this->email_get_spool_size($user) / 1024;

			return $status;
		}

		public function getFileCount($uid)
		{
			return $this->quota_list[$uid]['files'];
		}

		public function getQuotaSum()
		{
			$sum = 0;
			foreach ($this->generateQuotas() as $user => $quota) {
				$sum += $quota['used'];
			}

			return $sum;
		}

		public function getAccountQuota()
		{
			return \UCard::get()->getStorage();
		}

		public function getQuota($user)
		{
			return $this->quota_list[$user];
		}

		public function getMySQLDatabaseSize()
		{
			if (!$this->sql_enabled('mysql')) {
				return 0;
			}

			return $this->service_list['mysql']['used'];
		}

		public function getPgSQLDatabaseSize()
		{
			if (!$this->sql_enabled('pgsql')) {
				return 0;
			}

			return $this->service_list['postgresql']['used'];
		}

		public function getLogUsage()
		{
			return $this->service_list['logs']['used'];
		}

		public function getTotalUsage()
		{
			return $this->site_get_account_quota();

		}

		public function getFileMap()
		{

		}

		public function clearMap()
		{

		}

		private function _getCache()
		{

		}

		private function _setCache()
		{
		}

	}

?>
