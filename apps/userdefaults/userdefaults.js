$(document).ready(function () {
	$('[data-toggle=tooltip]').tooltip();
	$('#username').focus();
	$('#random-password').on('change', function () {
		if ($(this).prop('checked')) {
			$('#password').val("XXXXXX").prop('disabled', true);
			$('#password_confirm').val("XXXXXX");
		} else {
			$(['#password', '#password_confirm']).each(function (i, v) {
				$(v).prop('disabled', false).val("").removeAttr("formnovalidate");
			});
		}
		return true;
	});
	$('#disk_slider').slider({
		minValue: 0,
		maxValue: 100,
		step: 5,
		startValue: Math.floor($('#disk_quota').val() / quota['max'] * 100),
		slide: function (e, ui) {
			$('#disk_quota').val(Math.floor(ui.value / 100 * quota['max']));
		},
		change: function (e, ui) {
			if (ui.value == 0 || ui.value == 100) {
				$('#disk_quota').val(0);
				$('#disk_unlimited').prop('checked', true);
			} else {
				$('#disk_unlimited').prop('checked', false);

			}
		}
	});

	$('#email_domains, #subdomain_domains').multiSelect({
		noneSelected: "Select domains",
		oneOrMoreSelected: "% domains selected"
	});

});

$(window).on('load', function () {
	var updateQuota = function () {
		var val = $(this).val();
		if (val > quota.max) val = 0;

		$('#disk_slider').slider('value', Math.floor(val / quota.max * 100));
		$(this).val(val);
	};

	$('#password').focus(function () {
		$('.password-confirm-container').fadeIn('fast');
		$('.password-random-container').fadeOut('fast');
	}).blur(function () {
		if ($("#password").val()) {
			$('.password-random-container').fadeOut('fast');
		} else {
			$('.password-random-container').fadeIn('fast');
			$('.password-confirm-container').val("").fadeOut('fast');
		}
	});

	/*$('#password').checkPassword({

	 });*/

	$('#disk_quota').change(function () {
		if ($(this).val() > 0)
			$('#disk_unlimited').prop('checked', false);
	});

	$('#disk_quota').focus(function () {
		$('#disk_slider').show();
	});

	$('#disk_quota').change(updateQuota);//.change();

	$('#jail_chpath').click(
		function () {
			var dialog = apnscp.modal($('#file_container'));
			apnscp.explorer({
				filter: 'filter=dir;show,/;show,/var/www;show,/home;show,/usr/local',
				onSelect: function (file, b) {
					$('#selected_dir').text(file);
				}
			});
			$('#select_dir').click(function () {
				$('#custom_jail').val($("#selected_dir").text());
				$('#file_container').hide();
				dialog.modal('hide');
				return true;
			})
			return false;
		}
	);
	$('#email_enable').click(function () {
		$('#email_smtp, #email_imap').prop('checked', $(this).prop('checked'));
		return true;
	});

	$('#ssh_enable').click(function () {
		$('#crontab_enable').prop('checked', $(this).prop('checked'));
		return true;
	});
});
