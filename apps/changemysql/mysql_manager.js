var MODE = window.location.href.indexOf("changepgsql") > -1 ? 'pgsql' : 'mysql',
	BACKUP_HOME = '~/' + MODE + '_backups';
$(window).on('load', function () {
	$('#backup_exp_button').click(function () {
		$('#options').toggle('fast');
		return false;
	});

	$('#password').focus(function () {
		$('.password-confirm-container').show('fade', 500);
	});

	if (__state == 'add') {
		initializeUploader();
	}

	if (__state != 'list') return;

	$('#privilegeMode').bind('change click', function (e) {
		var $el = $(':input:checked', this), hide = $el.data('hide'),
			attr;
		if (hide[0] !== '#') {
			hide = '.' + hide;
		}
		$('.privileges').show().filter(hide).hide();
		$(hide).each(function () {
			var attr = $(this).attr('aria-controls');
			if (!attr) {
				return true;
			}
			$('#' + attr).collapse('hide');
		});
		return true;
	}).change();
	$('label.toggle').click(function () {
		$(this).find(':input').click();
	});

	$('.toggle_all').toggleAll({changed: true});

	$('#save-user, #create-user').click(function () {
		if ($('#password').val().length < 1) return true;
		if ($('#password').val() != $('#password_confirm').val()) {
			alert("Passwords do not match");
			$('#password').setCursorPosition(0, -1);
			return false;
		}
	});

	$('.privileges').click(listenPrivileges);
	$('[data-toggle="changedb"]').change(function(e) {
		var that = $(this), lang = that.data('language'),
			db = that.data('db'), newowner = that.val();
		// change fires after option has changed
		that.val(that.data('owner'));
		apnscp.cmd(lang + '_change_owner', [db, newowner]).then(function (ret) {
			if (ret.success) {
				apnscp.addMessage("Database owner changed to " + newowner);
				that.val(newowner);
				that.data('owner', newowner);
			}
		});
	});
	$('#database-list').click(function (e) {
		var $e = $(e.target);
		if ($e.hasClass('ui-action-restore')) {
			importDialog($e.data('db'));
			return false;
		} else if ($e.hasClass('ui-action-snapshot')) {
			$e.siblings('.ajax-msg').remove();
			var db = $e.data('db'), date = new Date(),
				timeSpec = "" + date.getFullYear() + ("0" + (date.getMonth() + 1)).substr(-2) +
					("0" + (date.getDate())).substr(-2) + ("0" + (date.getHours())).substr(-2) +
					("0" + (date.getMinutes())).substr(-2) + ("0" + (date.getSeconds())).substr(-2);
			$e.removeClass('ui-action-snapshot').append($('<i class="ui-ajax-indicator ui-ajax-loading"></i>'));
			apnscp.cmd('sql_export_' + MODE, [db, BACKUP_HOME + '/' + db + '-' + timeSpec + '-snapshot.sql']).done(function (e) {
				$e.addClass('fa-check');

				$('#snapshotModal').find('.dbname').text(db).end().find('.filename').text(e.return).end().modal('show');
			}).fail(function (e) {
				var resp = JSON.parse(e.responseText),
					msg = resp.errors ? resp.errors[0] : "unable to snapshot db";
				$e.addClass('ui-ajax-error');
			}).always(function () {
				$e.addClass('ui-action-snapshot').find('.ui-ajax-indicator').remove();
			});
			return false;
		}
		return true;
	});
});

function importDialog(db) {
	var re = new RegExp("^" + RegExp.quote(db) + '-20\\d\\d\\d\\d\\d\\d(?:\\d+-snapshot)?\\.');
	apnscp.cmd('file_get_directory_contents', [BACKUP_HOME]).done(function (ret) {
		var $entries = [], files = ret.return.reverse();
		for (var i in files) {
			var file = files[i];
			if (!file.filename.match(re)) {
				continue;
			}
			var snapshot = '';
			if (file.filename.match(/-snapshot\.sql$/)) {
				snapshot = '<i class="mx-1 ui-action ui-action-snapshot ui-action-label"></i>';
			}
			var $frag = $("<li class='list-inline' title='" + file.filename + "'></li>").tooltip().append(
				$('<label class="custom-control custom-radio font-weight-normal">').text(' (' + Math.round(file.size / 1024 * 100) / 100 + ' KB) ' + new Date(file.atime * 1000)).prepend([
						$('<input />').attr({
							'name': 'restore',
							value: file.filename,
							'class': 'custom-control-input',
							'type': 'radio'
						}).prop('required', true),
						$('<span class="custom-control-indicator"></span>'),
						snapshot
					]
				)
			);
			$entries.push($frag);
		}
		$('#db-list').empty().append($entries);
		if ($entries.length > 0) {
			$('#db-list + .note').hide();
		} else {
			$('#db-list + .note').show();
		}
		$('#importModal').modal('show').one('shown.bs.modal', function () {
			$('#importModal').find('form').validator().find('.export').prop('disabled', false).removeClass('disabled');
		});

	});
	$('#importModal').find('.db-name').text(db);
	var reqColl = [], dfd;
	$('#importModal form').unbind('.modal').on('click.modal', function (e) {

		var $el = $(e.target), $ajax = $('#importModal .ajax-response'),
			$indicator = $ajax.siblings('.btn-primary').find('.fa');
		if ($el.hasClass('export')) {
			var dbvar = {};
			dbvar[db] = true;
			$el.attr('name', 'export[' + db + ']');
			reqColl = $(this).closest('form').validator('destroy').find(':input[required]').prop('required', false);
			return true;
		} else if ($el.hasClass('import')) {
			$(reqColl).prop('required', true);
			$ajax.empty();
			// call once to avoid multiple binds
			$(this).closest('form').validator('validate').one('submit', function (e) {
				if (e.isDefaultPrevented()) {
					return false;
				}
				dfd = $.Deferred();
				$indicator.addClass('fa-spinner fa-pulse');
				var backup = $(':input[name=restore]:checked', this).val();
				apnscp.cmd('sql_empty_' + MODE + '_database', [db]).done(function () {
					// check extension, decompress as necessary
					apnscp.cmd('sql_import_' + MODE, [db, BACKUP_HOME + '/' + backup]).done(function (e) {
						dfd.resolve(true);
					}).fail(function (xhr) {
						dfd.reject(xhr);
					});
				}).fail(function (xhr) {
					dfd.reject(xhr);
				});
				dfd.done(function () {
					$ajax.removeClass('text-danger').addClass('text-success').text("Success!");
					setTimeout(function () {
						$('#importModal').modal('hide');
					}, 2500);
				}).fail(function (xhr) {
					var resp = JSON.parse(xhr.responseText);
					$ajax.removeClass('text-success').addClass('text-danger').text("failed: " + (resp.errors[0] || "general failure"));
				}).always(function () {
					$indicator.removeClass('fa-spinner fa-pulse');
				});
				return false;
			}).submit();
			return false;
		}
	}).on('submit.modal', function (e) {
		if (reqColl.length) {
			return true;
		}
		if (e.isDefaultPrevented()) {
			return false;
		}
		return dfd;
	});

}

function initializeUploader() {
	var sizeBox = document.getElementById('sizeBox'),
		progress = document.getElementById('progress');
	var uploader = new ss.SimpleUpload({
		url: '/apps/filemanager?Upload&s=' + session.id + '&cwd=' + '/tmp/' + '&id=' + upload_id,
		name: 'uploaded_file',
		button: "upload-file",
		method: "POST",
		dataType: 'json',
		progressUrl: "/apps/filemanager/filemanager-ajax.php?fn=upload_progress&s=" + session.id + '&id=' + upload_id,
		responseType: 'json',
		maxSize: MAX_SIZE,
		multiple: true,

		onSubmit: function (filename, extension) {
			// Create the elements of our progress bar
			var progress = document.createElement('div'), // container for progress bar
				bar = document.createElement('div'), // actual progress bar
				fileSize = document.createElement('div'), // container for upload file size
				wrapper = document.createElement('div'), // container for this progress bar
				//declare somewhere: <div id="progressBox"></div> where you want to show the progress-bar(s)
				progressBox = document.getElementById('progressBox'); //on page container for progress bars

			// Assign each element its corresponding class
			progress.className = 'progress progress-striped';
			bar.className = 'progress-bar progress-bar-success';
			fileSize.className = 'size';
			wrapper.className = 'wrapper';

			// Assemble the progress bar and add it to the page
			progress.appendChild(bar);
			wrapper.innerHTML = '<div class="name"><i class="ui-ajax ui-ajax-indicator ui-ajax-loading"></i> ' + filename + '</div>'; // filename is passed to onSubmit()
			wrapper.appendChild(fileSize);
			wrapper.appendChild(progress);
			progressBox.appendChild(wrapper); // just an element on the page to hold the progress bars

			// Assign roles to the elements of the progress bar
			this.setProgressBar(bar); // will serve as the actual progress bar
			this.setFileSizeBox(fileSize); // display file size beside progress bar
			this.setProgressContainer(wrapper); // designate the containing div to be removed after upload
			var opts = {'upload_overwrite': true};
			this.setData(opts);
			$('#create_db').prop('disabled', true);
			$('#import-file, #import-file-input').remove();
			return true;
		},

		// Do something after finishing the upload
		// Note that the progress bar will be automatically removed upon completion because everything
		// is encased in the "wrapper", which was designated to be removed with setProgressContainer()
		onComplete: function (filename, response, btn, size) {
			$('#create_db').prop('disabled', false);
			if (!response) {
				alert(filename + 'upload failed');
				return false;
			} else if (!response.success) {
				return this._opts.onError.call(this, filename, null, 200, null, response, btn, size);
			}
			$('#addForm').append($('<input />').attr(
				{
					'type': 'hidden',
					'id': 'import-file-input',
					'name': 'file',
					'value': '/tmp/' + filename
				}
			));
			var $file = $('<p id="import-file"><i class="fa fa-file-o"></i> ' +
				filename + ' (' + Math.round(size / 1024 * 100) / 100 + 'MB)</p>').hide().prepend(
				$('<button>').attr({
					'class': 'warn btn btn-secondary ui-action ui-action-delete ui-action-label mr-1',
					'name': 'delete',
					'type': 'button'
				}).text("Delete").click(function () {
					apnscp.cmd('file_delete', [$('#import-file-input').val()]);
					$('#import-file-input, #import-file').remove();
				}));
			$('#progressBox').append($file.fadeIn('fast'));
			return true;

		},
		onError: function (filename, type, status, statusText, response, uploadBtn, size) {
			$('#create_db').prop('disabled', false);
			if (status != 200) {
				// @TODO undefined offset top error
				return true;
			}
			$('#uploadContainer').append(
				$('<p>').text(filename + " failed: " + (response.msg || type)).addClass("alert alert-danger")
			);
			return false;
		}
	});
}


function listenPrivileges(e) {

	var t = e.target;
	var $t = $(e.target);
	if (t.type != undefined && t.type.toUpperCase() == 'CHECKBOX') {
		e.stopPropagation();
		var $p = $t.closest('label');
		if ($t.prop('checked')) {
			$t.prop('checked', true).closest('label').addClass('bg-success');
		} else {
			$t.prop('checked', false).closest('label').removeClass('bg-success');
		}
		return true;
	} else if (t.nodeName.toUpperCase() == 'A' && $t.hasClass('ui-expandable')) {
		$('#' + $t.attr('rel')).slideToggle('fast');
		$t.toggleClass('ui-collapsed').toggleClass('ui-expanded');
		return false;
	} else if ($t.is(':reset')) {
		$t.parent().find('li.ui-highlight').click().removeClass('ui-highlight');
		return false;
	} else if (t.nodeName.toUpperCase() == 'LABEL') {
		if ($t.find(':checkbox').prop('checked')) {
			$t.removeClass('bg-success');
		} else {
			$t.addClass('bg-success');
		}
		return true;
	}
	return true;
}
