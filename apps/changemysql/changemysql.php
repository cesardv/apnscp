<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\changemysql;

	use Page_Container;

	class Page extends Page_Container
	{

		public $upload_id;

		public function __construct()
		{
			parent::__construct();
			if ($this->getMode() !== "list") {
				$this->upload_id = $upload_id = uniqid();
				$maxsize = \HTML_Kit::maxUpload();
				$this->add_javascript('var upload_id=' . "'" . $upload_id . "', MAX_SIZE=" . $maxsize . ";", 'internal',
					false, true);
				$this->init_js('upload');
			}
			$this->add_javascript('mysql_manager.js');
			$this->add_css('changemysql.css');


			$this->add_javascript('var __state = "' . $this->getMode() . '";', 'internal', false, true);
			if ($this->isModeSwitch(array('mode', 'db')) || $this->isModeSwitch('mode')) {
				$this->hide_pb();
			}
		}

		public function getMode()
		{
			if (isset($_GET['mode']) && $_GET['mode'] == 'list') {
				return $_GET['mode'];
			}

			return 'add';
		}

		public static function sort_perm_rank($a, $b)
		{
			if ($a['rank'] > $b['rank']) {
				return -1;
			} else if ($a['rank'] < $b ['rank']) {
				return 1;
			}

			if ($a['user'] == $b['user']) {
				return strnatcmp($a['host'], $b['host']);
			}

			return strnatcmp($a['user'], $b['user']);
		}

		public function on_postback($params)
		{

			if (isset($params['hosts'])) {
				/**
				 * @XXX workaround for Echelon and older servers (platform = 4) where session data is not saved
				 * with APC enabled prior to redirect
				 */
				$_SESSION['mysql-show_hosts'] = ($params['hosts'] == 'show');

				session_write_close();
				header('Location: ' . \HTML_Kit::page_url_params(array('hosts' => null), false), true, 302);
				exit;
			}

			if (isset($params['export'])) {
				$dbs = array_keys($params['export']);
				// single export for now
				$db = array_pop($dbs);
				$path = $this->sql_export_mysql_pipe($db);
				if (!$path) {
					return error("failed to initialize file download");
				}
				$fname = sprintf("%s-%s.sql", $db, date('Ymd'));
				\HTML_Kit::makeDownloadable($path, $fname);
				return;
			} else if (isset($params['Create_User'])) {
				if (strlen($params['password']) < 7) {
					return error("SQL password must be more than 6 characters");
				} else if ($params['password'] != $params['password_confirm']) {
					return error("Passwords do not match");
				}
				$this->sql_add_mysql_user(
					$params['new_user_name'],
					$params['new_user_host'],
					$params['password'],
					$params['new_max_user_connections'],
					$params['new_max_updates'],
					$params['new_max_questions'],
					(isset($params['new_use_ssl']) ? $params['new_ssl_type'] : ''),
					$params['new_cipher'],
					$params['new_issuer'],
					$params['new_subject']
				);

				return;

			} else if (isset($params['users']['delete'])) {
				foreach (array_keys($params['users']['delete']) as $user) {
					foreach (array_keys($params['users']['delete'][$user]) as $host) {
						$this->sql_delete_mysql_user($user, $host, true);
					}
				}

			} else if (isset($params['Save_User'])) {
				list($user, $host) = explode(" ", $params['users']['state'], 2);
				$password = isset($params['password']) ? $params['password'] : null;
				$params = $params['users']['edit'];
				if ($this->sql_edit_mysql_user(
					$user,
					$host,
					array(
						'host'                 => $params['host'],
						'password'             => $password ?: null,
						'max_user_connections' => $params['max_user_connections'],
						'max_updates'          => $params['max_updates'],
						'max_questions'        => $params['max_questions'],
						'use_ssl'              => isset($params['use_ssl']),
						'ssl_type'             => $params['ssl_type'],
						'ssl_cipher'           => $params['ssl_cipher'],
						'x509_subject'         => $params['x509_subject'],
						'x509_issuer'          => $params['x509_issuer']
					)
				)) {
					header('Location: ' .
						str_replace('&amp;', '&', \HTML_Kit::page_url_params(array('host' => $params['host']))), true,
						302
					);
					exit;
				}

				return;
			} else if (isset($params['CreateDB'])) {
				$status = $this->sql_create_mysql_database($params['database_name']);
				if (isset($params['backup_db']) && !$this->errors_exist()) {
					$db = \Util_Conf::get_svc_config('mysql', 'dbaseprefix') . $params['database_name'];
					$ret = $this->sql_add_mysql_backup($db,
						$params['b_extension'],
						$params['b_frequency'],
						$params['b_hold']
					);
					$this->bind($ret);
					if ($ret && isset($params['file']) && !strncmp($params['file'], '/tmp/', 5)) {
						$this->sql_import_mysql($params['database_name'], $params['file']);
						$this->file_delete($params['file']);
					}
				}

				return $status;
			} else if (isset($params['Save_Grants'])) {
				foreach (array_keys($params['editdb']) as $db) {
					$new = $params['editdb'][$db];
					$old = $params['editdb'][$db]['state'];
					foreach ($old['simple'] as $user => $ginfo) {

					}
					foreach ($old['complex'] as $user => $ginfo) {
						foreach ($ginfo as $host => $privileges) {
							$ch_privileges = '';
							$privileges = \Util_PHP::unserialize(base64_decode($privileges));
							$privileges_old = array_change_key_case(array_filter($privileges), CASE_UPPER);
							$privileges_new = array();
							if (isset($new['complex'][$user][$host])) {
								$privileges_new = array_change_key_case($new['complex'][$user][$host], CASE_UPPER);
							}
							$diff = array_diff_key($privileges_new, $privileges_old);
							$diffrem = array_map(function ($a) {
								return false;
							}, array_diff_key($privileges_old, $privileges_new));
							$diff = array_merge($diff, $diffrem);
							if (count($diff) > 0) {
								// complex privileges changed
								$ch_privileges = $privileges_new;
							} else {
								// Check simple privileges for change
								$privold = $old['simple'][$user][$host];
								$simple = \Util_PHP::unserialize(base64_decode($privold));
								$read_new = $write_new = false;

								$read_old = $simple['READ'];
								$write_old = $simple['WRITE'];


								if (isset($new['simple'][$user][$host])) {
									$privnew = array_change_key_case($new['simple'][$user][$host], CASE_UPPER);
									$read_new = isset($privnew['READ']);
									$write_new = isset($privnew['WRITE']);
								}


								if ($read_old != $read_new || $write_old != $write_new) {
									$ch_privileges = array(
										'select'      => $read_new,
										'insert'      => $write_new,
										'update'      => $write_new,
										'delete'      => $write_new,
										'create'      => $write_new,
										'drop'        => $write_new,
										'index'       => $write_new,
										'alter'       => $write_new,
										'lock_tables' => ($write_new | $read_new),
										'show_view'   => $read_new,
										'create_view' => $write_new
									);
								}
							}
							if ($ch_privileges) {
								$this->sql_set_mysql_privileges($user, $host, $db, $ch_privileges);
							}
						}
					}
				}

				return;
			} else if (isset($params['drop'])) {
				foreach (array_keys($params['drop']) as $drop) {
					$this->sql_delete_mysql_database($drop);
				}
				if (!$this->pb_succeeded()) {
					return;
				}
				header('Location: ' .
					str_replace('&amp;', '&', \HTML_Kit::page_url_params(array('mode' => 'list', 'db' => ""))), true,
					302
				);
				exit();
			} else if (isset($params['empty'])) {
				foreach (array_keys($params['empty']) as $empty) {
					$this->sql_empty_mysql_database($empty);
				}
				if (!$this->pb_succeeded()) {
					return;
				}
			} else if (isset($params['repair'])) {
				foreach (array_keys($params['repair']) as $repair) {
					$this->sql_repair_mysql_database($repair);
				}
				if (!$this->pb_succeeded()) {
					return;
				}
			}
		}

		public function getDatabases()
		{
			return $this->sql_list_mysql_databases();
		}

		public function getDBSize($mName)
		{
			return \Formatter::commafy(\Formatter::reduceBytes($this->sql_get_database_size('mysql', $mName)));
		}

		public function getPerms($mUser, $mHost, $mDB)
		{
			return $this->sql_get_mysql_privileges($mUser, $mHost, $mDB);
		}

		public function getUsers()
		{
			return $this->sql_list_mysql_users();
		}

		public function getDBPrefix()
		{
			return $this->sql_get_prefix();
		}

		public function show_hosts()
		{
			return isset($_SESSION['mysql-show_hosts']) &&
				$_SESSION['mysql-show_hosts'];
		}

	}

?>
