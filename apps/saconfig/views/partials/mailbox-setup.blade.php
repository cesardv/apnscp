<h2>Feedback Participation</h2>
<p>
	Help us improve your filter quality! Train messages by drag-and-drop into a new IMAP folder called &quot;AutoSpam&quot;.
	<a class="ui-action ui-action-label ui-action-kb" href="{{ MISC_KB_BASE }}/e-mail/pop3-vs-imap-e-mail-protocols/">IMAP</a>
	is absolutely necessary to use this feature.
</p>
<figure class="figure">
	<img src="/images/apps/saconfig/autospam.gif" class="figure-img img-fluid"
	     alt="Example drag-and-drop in Thunderbird"/>
	<figcaption class="figure-caption text-center">Training is a breeze once enabled!</figcaption>
</figure>

<form method="post" action="<?php print \HTML_Kit::page_url(); ?>">
	@if (!$Page->autoSpamExists())
		<p>Enable the folder to begin.</p>
		<button name="autospam" class="btn btn-primary" value="enable">Enable Folder</button>
	@else
		<p>Your learning folder is presently enabled.</p>
		<button name="autospam" class="btn btn-primary" value="disable">Disable Folder</button>
	@endif
</form>
