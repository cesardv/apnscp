<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\saconfig;

	use apps\saconfig\models\Tips;
	use Illuminate\Http\Request;
	use Illuminate\Validation\Validator;
	use Opcenter\Mail\Services\Spamassassin;
	use Page_Container;

	class Page extends Page_Container
	{
		private $sa;

		public function _init()
		{
			parent::_init();
			$this->sa = new \apps\saconfig\models\SpamAssassin($_POST);
		}

		public function _layout()
		{
			parent::_layout();
			$this->add_javascript('saconfig.js');
			$this->add_css('saconfig.css');
		}

		protected function rules()
		{
			$base = [];
			if (Spamassassin::present()) {
				$base = ['spam_score' => 'required|numeric|between:0,20'];
			}
			return $base + [
				'prefix_custom'       => 'required_if:tagging_method,prefix_custom|regex:/^[a-z\-\[\]_\+0-9\.]+$/i',
				'mailbox_type'        => ['required', 'in:pop3,imap'],
				'move_custom_mailbox' => [
					'nullable',
					'required_if:delivery_mailbox,move_custom',
					'regex:/^[a-z@\-\[\]_\+0-9\.]+$/i'
				],
				'delivery_mailbox'    => ['required_unless:mailbox_type,pop3', 'required_with:delivery_mailbox,1']
			];
		}

		protected function ruleMessages()
		{
			return [
				'spam_score.between'               => "Spam score must be between 0 and 20",
				'prefix_custom.regex'              => "Invalid custom mailbox name",
				'delivery_mailbox.required_unless' => "Unable to move email to a separate inbox when using POP3",
				'move_custom_mailbox.required_if'  => "Enter a mailbox to move spam into",
				'move_custom_mailbox.regex'        => "Invalid custom mailbox name",
			];
		}

		// @var SpamAssassin
		public function route(Request $request)
		{
			$this->sa = new \apps\saconfig\models\SpamAssassin($request->all());
			$v = array();
			if ($request->input("generate")) {
				/** @var Validator $v */
				$v = \Illuminate\Support\Facades\Validator::make(
					$request->all(),
					$this->rules(),
					$this->ruleMessages()
				);
				//$v->sometimes('delivery_mailbox', 'required_unless:mailbox_type,pop3')
				if ($v->passes()) {
					return $this->post($request);
				}

			}
			foreach ($v->getMessageBag()->messages() as $error) {
				error(__($error[0]));
			}
			return view('index', $this->sa->getSettings());
		}

		public function index() {
			return view('index', $this->sa->getSettings());
		}

		public function post(Request $request)
		{
			$this->sa->process();
			$recipe = $this->sa->getRecipe();
			$saconfig = $this->sa->getConfig();
			$settings = $this->sa->getSettings();
			if (\UCard::is('site')) {
				$this->spamfilter_set_delivery_threshold((int)$request->post("global_score"));
			}

			if ($this->sa->getSetting('output') === "apply") {
				if (!$this->save($saconfig, $recipe)) {
					$settings['output'] = 'view';
				}
			}

			return view('index', $settings)->with(
				[
					'recipe'     => $recipe,
					'filtername' => $this->sa->getLDAFilter(),
					'saconfig'   => $saconfig
				]
			);

		}

		protected function save($userprefs, $recipe)
		{
			if (Spamassassin::present() && $userprefs) {
				if (!$this->updateFile('~/.spamassassin/user_prefs', $userprefs)) {
					return false;
				}
			}

			return $this->updateFile('~/.mailfilter', $recipe);

		}

		private function updateFile(string $file, string $contents): bool {
			$base = '';
			if ($this->file_exists($file)) {
				$base = $this->file_get_file_contents($file);
			}

			$contents = $this->replaceBlock($base, $contents);
			if (!$this->file_exists(\dirname($file))) {
				$this->file_create_directory(\dirname($file));
			}
			return $this->file_put_file_contents($file, $contents) ?: error("Failed to update %s", $file);
		}

		private function replaceBlock(string $src, string $block): string {
			$re = \Regex::compile(\Regex::BLOCK_C, [
				'begin' => \apps\saconfig\models\SpamAssassin::OPENING_MARKER,
				'end'   => \apps\saconfig\models\SpamAssassin::CLOSING_MARKER
			]);

			$new = preg_replace_callback($re, static function ($match) use ($block) {
				return $match['body'] = $block;
			}, $src, -1, $count);
			if (!$count) {
				$new = rtrim($src) . "\n" . $block. "\n";
			}

			return $new;

		}

		public function getTip($index)
		{
			return (new Tips)->get($index);
		}

		public function getFilterProper() {
			return Spamassassin::present() ? 'SpamAssassin' : 'rspamd';
		}
	}