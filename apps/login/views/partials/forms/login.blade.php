<div class="form-group">
	<div class="input-icon fa fa-user"></div>
	<input autofocus="autofocus" class="form-control" id="username" name="username"
	    tabindex="1" type="text" value="{{ $Page->getUsername() }}"/>
	<label for="username" class="placeholder">
		@if (STYLE_VERBOSE_LOGIN !== 'thin')
			{{ _("Username") }}
		@else
			{{ _("Login") }}
		@endif
	</label>
</div>

@if (STYLE_VERBOSE_LOGIN !== 'thin')
	<div class="form-group">
		<div class="input-group">
			<input class="form-control" id="domain" name="domain"
			       tabindex="2" value="{{ $Page->getDomain() }}" type="text"/>
			<div class="input-icon fa fa-cloud"></div>
			<label for="domain" class="placeholder">{{ _("Domain") }}</label>
			@if (STYLE_VERBOSE_LOGIN === true)
				<div class="input-group-addon form-inline options border-left-0">
					<label for="isAdmin" class="custom-control custom-checkbox  mr-0 mb-0 align-items-center">
						<input type="checkbox" name="admin" value="1" id="isAdmin" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="">{{ _("Administrator") }}</span>
					</label>
				</div>
			@endif
		</div>
	</div>
@endif

<div class="form-group">
	<div class="input-icon fa fa-lock"></div>
	<input autocomplete="off" class="form-control password" id="password" name="password"
		tabindex="3" type="password"/>
		<label for="password" class="placeholder">
			{{ _("Password") }}
		</label>
</div>

<button class="btn btn-primary login-indicator btn-lg btn-block" name="commit" tabindex="4"
        type="submit"
        value="Log In">
	<i class="fa"></i>
	{{ _("Login") }}
</button>

@if ($pageMode === 'login')
	<div class="row options mt-4">
		<div class="login-tooltip col-6 mt-1" data-toggle="tooltip"
		     title="Store your username + domain for future logins">
			<label class="custom-control custom-checkbox align-items-center">
				<input type="checkbox" value="username" class="custom-control-input"
				       name="remember" id="remember" @if ($Page->getParam('remember')) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				<span class="">
					{{ _("Remember me") }}
				</span>
			</label>
		</div>

		<div class="login-tooltip col-6 mt-1" data-toggle="tooltip"
		     title="Auto-logout after 15 minutes of inactivity">
			<label class="custom-control custom-checkbox  align-items-center">
				<input type="checkbox" value="1" class="custom-control-input"
				       name="autologout" id="nologout"
				       @if ($Page->getParam('autologout')) checked="CHECKED" @endif />
				<span class="custom-control-indicator"></span>
				<span class="">
					{{ _("Inactivity logout") }}
				</span>
			</label>
		</div>
	</div>
@endif