<div class="col-12 mt-3 text-center status-indicator">
	@php
		$tmp = $Page->getSystemStatus();
		$status = $tmp['status'];
		$marker = $tmp['marker'];
		$title = $status;
		if ($marker) {
			$title .= ' - ' . $marker;
		}
	@endphp
	<a data-toggle="tooltip" title="{{ $title }}"
	   href="{{ $Page->getSystemStatusUrl() }}">
		<span class="fa fa-circle mr-1 {{ $Page->status2Css($status) }}"></span
		>Network Status
	</a>
</div>