$(window).on('load', function () {
	$('.login-indicator').ajaxWait();
	if ($('#username').val() != '') {
		$('#password').focus();
	} else {
		$('#username').focus();
	}

	$('[data-toggle="tooltip"]').tooltip();

	$('.login-container input:text').on('input', function (e) {
		$(e.currentTarget).attr('data-empty', !e.currentTarget.value);
	}).each(function() {
		$(this).triggerHandler('input');
	});


	$('#isAdmin').on('change', function () {
		if ($(this).prop('checked')) {
			$('#domain').val("").attr('data-empty', false).prop('disabled', true);
		} else {
			$('#domain').attr('data-empty', true).prop('disabled', false);
		}
	});
});
