<ul class="list-inline col-12">
	<li class="list-inline-item pr-2">
		<i class="fa fa-map-marker"></i>
		Active Timezone: {{ date_default_timezone_get() }}
	</li>
	<li class="list-inline-item">
		<i class="fa fa-clock-o"></i>
		Timezone Time: {{ date('H\:m\:s O T') }}
	</li>
</ul>