<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\cloudflare;

	class Page extends \apps\webapps\Page
	{
		private $_mode = 'list';
		// active personality target
		private $_target;
		private $_path;

		public function __construct()
		{
			// use v2 layout with bootstrap
			// @todo maybe add hooks?
		}
	}
