<!-- main object data goes here... -->

<form method="post" action="prlanguages">

	<H3>Advanced Language Support</H3>
	<div class="row">
		<?php if (!$Page->ssh_enabled()): ?>
		<div class="col-12">
			<p class="note alert alert-info">
				This account lacks privileges for advanced language frameworks.
				Upgrade today to include support for
				<i class="fa fa-node-w"></i> Node, <i class="fa fa-ruby"></i> Ruby, and <i class="fa fa-python"></i>
				Python.
			</p>
		</div>
		<?php else: ?>
		<div class="col-12">
			<p class="note">
				All frameworks may be setup within the terminal in the control panel
				(Dev &gt; <a class="ui-action ui-action-label ui-action-switch-app" href="/apps/terminal">Terminal</a>)
				or by
				logging in via <a href="<?=MISC_KB_BASE?>/terminal/accessing-terminal/">SSH</a>.
			</p>
			<h4>Language &amp; Framework Guides</h4>
			<ul class="list-unstyled">
				<li class="my-3">
					<i class="fa fa-2x mr-1 fa-list-item fa-ruby"></i>
					Ruby on Rails: <a class="ui-action ui-action-label ui-action-kb"
					                  href="<?=MISC_KB_BASE?>/ruby/setting-rails-passenger/">Setting up Rails with
						Passenger</a>
				</li>
				<li class="my-3">
					<i class="fa fa-2x mr-1 fa-list-item fa-node-w"></i>
					Node.js: <a class="ui-action ui-action-label ui-action-kb"
					            href="<?=MISC_KB_BASE?>/guides/running-node-js/">Running Node.js</a>
				</li>
				<li class="my-3">
					<i class="fa fa-2x mr-1 fa-list-item fa-js"></i>
					Express: <a class="ui-action ui-action-label ui-action-kb"
					            href="<?=MISC_KB_BASE?>/guides/installing-express/">Installing Express</a>
				</li>
				<li class="my-3">
					<i class="fa fa-2x mr-1 fa-list-item fa-ghost"></i>
					Ghost: <a class="ui-action ui-action-label ui-action-kb"
					          href="<?=MISC_KB_BASE?>/guides/installing-ghost/">Installing Ghost</a>
				</li>
				<li class="my-3">
					<i class="fa fa-2x mr-1 fa-list-item fa-meteor"></i>
					Meteor: <a class="ui-action ui-action-label ui-action-kb"
					           href="<?=MISC_KB_BASE?>/guides/running-meteor/">Running Meteor</a>
				</li>
				<li class="my-3">
					<i class="fa fa-2x mr-1 fa-list-item fa-django"></i>
					Django: <a class="ui-action ui-action-label ui-action-kb"
					           href="<?=MISC_KB_BASE?>/python/django-quickstart//">Django Quickstart</a>
				</li>
				<li class="my-3">
					<i class="fa fa-2x mr-1 fa-list-item fa-python"></i>
					Python: <a class="ui-action ui-action-label ui-action-kb"
					           href="<?=MISC_KB_BASE?>/python/using-wsgi/">Using WSGI</a>
				</li>
				<li class="my-3">
					<i class="fa fa-2x mr-1 fa-list-item fa-redis"></i>
					Redis: <a class="ui-action ui-action-label ui-action-kb"
					          href="<?=MISC_KB_BASE?>/guides/running-redis/">Running Redis</a>
				</li>
				<li class="my-3">
					<i class="fa fa-2x mr-1 fa-list-item fa-mongo"></i>
					MongoDB: <a class="ui-action ui-action-label ui-action-kb"
					            href="<?=MISC_KB_BASE?>/guides/running-mongodb/">Running MongoDB</a>
				</li>
			</ul>
		</div>
		<?php endif; ?>
	</div>
</form>
