<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */


namespace apps\scopes;

use apps\pagespeed\models\Scope;
use Opcenter\Admin\Settings\Setting;

class Page extends \Page_Container {

	public function __construct($doPostback = true)
	{
		parent::__construct($doPostback);
	}


	public function _layout()
	{
		$this->add_javascript('scopes.js');
		$this->add_css('scopes.css');
		parent::_layout();
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(string $scope = '')
	{
		$this->add_javascript('var globalScopeName = ' . json_encode($scope) . '; var SCOPE_EVENT_NS = ".scope";', 'internal', false, true);
		return view('index', [
			'scopes' => $this->loadScopes(),
			'activeScope' => $scope
		]);
	}

	public function showAll()
	{
		return view('partials.show-all', ['scopes' => $this->loadScopes()]);
	}

	public function loadScopes() {
		return $this->getApnscpFunctionInterceptor()->scope_list();
	}

	public function getScopeDescription(string $scope) {
		$c = Setting::className($scope);
		if (!$c) {
			return '';
		}
		return (new $c)->getHelp();
	}

	public function apply(string $scope, array $args = []): bool
	{
		if (isset($args[0]) && is_array($args[0])) {
			$tmp = [];
			foreach ($args as $v) {
				if (!isset($v['name'])) {
					continue;
				}
				$name = $v['name'];
				if (false !== ($pos = strpos($name, '['))) {
					$name = substr($name, $pos+1, -1);
				}
				$tmp[$name] = \Util_Conf::inferType($v['value']);
			}
			$args = $tmp;
		}
		$info = $this->getApnscpFunctionInterceptor()->scope_info($scope);
		if (\is_array($info['settings']) && isset($info['settings'][0])) {
			// enum
			$args = array_pop($args);
		} else if ($info['settings'] === 'bool') {
			$args = (bool)($args['args'] ?? $args[0]);
		}
		return \apps\scopes\models\Scope::factory($scope)->set((array)$args);
	}
}