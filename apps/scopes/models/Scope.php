<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

namespace apps\scopes\models;

use Illuminate\Support\Facades\View;
use Opcenter\Admin\Settings\Setting;
use Opcenter\CliParser;

class Scope {
	use \NamespaceUtilitiesTrait;

	use \ContextableTrait;
	use \apnscpFunctionInterceptorTrait;

	/**
	 * @var string
	 */
	protected $settings;

	protected $name;

	protected $default;

	protected $info;

	protected $value;
	/**
	 * @var array
	 */
	protected $scope;

	protected $related = [];

	/**
	 * Create Scope
	 *
	 * @param string               $name
	 * @param \Auth_Info_User|null $ctx
	 * @return static
	 */
	public static function factory(string $name, \Auth_Info_User $ctx = null): self
	{
		$class = Setting::className($name);
		$parts = [
			substr($tmp = self::getNamespace($class), strrpos($tmp, '\\') + 1),
			self::getBaseClassName($class)
		];
		$namespace = self::appendNamespace(implode('\\', array_merge(['scopes'], $parts)));
		$parts = array_map(function ($v) { return snake_case($v, '-'); }, $parts);
		$name = implode('.', $parts);
		$ctx = $ctx ?? \Auth::profile();
		if (class_exists($namespace)) {
			return $namespace::instantiateContexted($ctx, [$name]);
		}

		return self::instantiateContexted($ctx, [$name]);
	}

	protected function __construct(string $name)
	{
		$scope = $this->getApnscpFunctionInterceptor()->scope_info($name);
		$this->scope = [
			'group' => substr($name, 0, strpos($name, '.')),
			'name' => substr($name, strpos($name, '.')+1)
		];
		$this->settings = $scope['settings'] ?? 'mixed';
		$this->info = $scope['info'] ?? 'Not available';
		$this->value = $scope['value'];
		$this->default = $scope['default'];
		$this->related = $this->getApnscpFunctionInterceptor()->scope_list($this->getGroup() . ':*');
	}

	public function getName(): string {
		return $this->scope['name'];
	}

	public function getGroup(): string {
		return $this->scope['group'];
	}

	public function getFullyQualifiedScope(): string
	{
		return $this->getGroup() . '.' . $this->getName();
	}

	public function getDefault() {
		return new ValuePresenter($this->default);
	}

	public function getHelp() {
		return $this->info;
	}

	public function getValue() {
		return new ValuePresenter($this->value);
	}

	public function getRelated(): array
	{
		return $this->related;
	}

	public function getSettings() {
		return $this->settings;
	}

	public function isChanged(): bool
	{
		return $this->default !== $this->value;
	}

	private function deepInfer($val): array
	{
		foreach ($val as $k => $v) {
			if (is_array($v)) {
				$v = $this->deepInfer($v);
			} else if (!is_bool($v)) {
				$v = \Util_Conf::inferType($v);
			}
			$val[$k] = $v;
		}

		return $val;
	}

	public function set($val) {
		$val = $this->deepInfer((array)$val);
		return $this->getApnscpFunctionInterceptor()->scope_set(
			$this->getFullyQualifiedScope(),
			...(isset($val[0]) ? $val : [$val])
		);
	}

	public function render(array $vars = []) {
		$name = $this->getFullyQualifiedScope();
		return View::first(
			[
				"partials.scopes.${name}.scope-scene",
				"partials.scope-scene",
			],
			[
				'scope'   => $this,
				'activeScope' => $name,
			] + $vars
		);
	}
}