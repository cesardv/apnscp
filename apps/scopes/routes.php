<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

	use apps\scopes\models\Scope;
	use Illuminate\Support\Facades\Route;
	use Illuminate\Support\Facades\View;
	use Opcenter\Admin\Bootstrapper\Config;
	use Symfony\Component\Yaml\Yaml;

Route::post('apply/{scope}', function (Page $p, string $scope) {
	$args = request()->post('args');
	if (!$p->apply($scope, (array)$args)) {
		return false;
	}
	if (request()->wantsJson()) {
		return true;
	}
	return $p->index($scope);
});

Route::get('/', 'Page@index');
Route::get('/apply/{name}', 'Page@index');
Route::post('export', static function () {
	$type = request()->post('get');
	$deleteAfter = false;
	if ($type === 'bootstrapper') {
		$downloadas = 'apnscp-vars-runtime.yml';
		$cfg = tempnam(storage_path(), 'bs-export');
		$settings = \apnscpFunctionInterceptor::init()->scope_get('cp.bootstrapper');
		// works because permissions in /root prevent reading from frontend <3
		$bootstrapLoader = (new Config());
		$filtered = array_filter($settings, static function ($value, $key) use ($bootstrapLoader) {
			return $bootstrapLoader->default($key) !== $value;
		}, ARRAY_FILTER_USE_BOTH);
		file_put_contents($cfg, Yaml::dump(
			$filtered,
			2,
			2,
			Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK | Yaml::DUMP_OBJECT_AS_MAP | Yaml::DUMP_OBJECT)
		);
		$deleteAfter = true;
	} else if ($type === 'config') {
		$downloadas = $cfg = config_path('custom/config.ini');
	} else {
		abort(403);
	}

	return response()->download($cfg, basename($downloadas))->deleteFileAfterSend($deleteAfter);
});

Route::post('render/{name?}', function(Page $p, string $name = null) {
	if (!$name) {
		return $p->showAll();
	}
	$scope = Scope::factory($name);
	return $scope->render([
		'activeScope' => $name
	]);
});
