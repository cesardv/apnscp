<form method="post" data-toggle="validator">
	<ul class="col-12 col-sm-4 col-lg-3 col-xl-2 mb-3 list-group float-left nav nav-pills" id="menu" role="tablist"
	    aria-multiselectable="true">
		<li class="nav-item block">
			<a href="#site" data-toggle="pill" class="nav-link active" data-parent="#menu" role="tab">User
				Information</a>
		</li>
		<li class="nav-item block">
			<a href="#theme" data-toggle="pill" class="nav-link" data-parent="#menu" role="tab">Theme</a>
		</li>
		<li class="nav-item block">
			<a href="#localization" data-toggle="pill" class="nav-link" data-parent="#menu" role="tab">Localization</a>
		</li>
		@if (!UCard::is('admin'))
			<li class="nav-item block">
				<a href="#authentication" data-toggle="pill" class="nav-link" data-parent="#menu" role="tab">Third-Party
					Authentication</a>
			</li>
		@endif
		<li class="nav-item block">
			<a href="#security" data-toggle="pill" class="nav-link" data-parent="#menu" role="tab">Security</a>
		</li>
		@if (UCard::is('site'))
			<li class="nav-item block">
				<a href="#dev" data-toggle="pill" class="nav-link" data-parent="#menu" role="tab">Development</a>
			</li>
		@elseif (UCard::is('admin'))
			<li class="nav-item block">
				<a href="#system" data-toggle="pill" class="nav-link" data-parent="#menu" role="tab">System Settings</a>
			</li>
		@endif
		@if (UCard::is('site') || UCard::is('admin'))
			<li class="nav-item block">
				<a href="#apps" data-toggle="pill" class="nav-link" data-parent="#menu" role="tab">App Settings</a>
			</li>
		@endif
		<li class="nav-item block">
			<a href="#ssl" data-toggle="pill" class="nav-link" data-parent="#ssl" role="tab">SSL</a>
		</li>
		<li class="block mt-3 text-right">
			<input type="submit" value="Save Changes" class="btn main" name="save"/>
		</li>
	</ul>

	<div class="col-12 col-sm-8 col-lg-9 col-xl-10 float-left tab-content">
		@include('partials.user-info')

		@include('partials.theme-behavior')
		@include('partials.localization')
		@include('partials.third-party')
		@include('partials.security')
		@include('partials.dev')
		@includeWhen(UCard::is('admin'), 'partials.admin')
		@include('partials.app-settings')
		@include('partials.ssl')
	</div>


</form>

<div id="change-steps" class="py-1 hide">
	<div class="input">
		<div class="change-form form-group" id="change-form">
			<label class="hinted" for="new-data">new username</label>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-user"></i></span>
				<input id="new-data" class="form-control" type="text" name="new-value"
				       value="{{ $_SESSION['username'] }}"/>
			</div>

		</div>
	</div>
</div>

<div id="upload-theme" class="py-1 hide">
	<div class="input">
		<h3 class="mb-3"><i class="fa fa-paint-brush"></i> Use Your Theme</h3>
		<p class="">
			Use your own theme in {{ PANEL_BRAND }}. To get started, download
			the <a href="https://github.com/apisnetworks/apnscp-bootstrap-sdk">Bootstrap SDK</a>.
			Only 1 theme may be used per account. Theme is overwritten every time
			it is updated.
		</p>
		<form method="post" action="{{ HTML_Kit::page_url() }}" enctype="multipart/form-data">
			<div class="btn-group">
				<label class="custom-file font-weight-normal">
					<input type="file" name="themefile" id="upload-file" class="custom-file-input">
					<span class="custom-file-control">
		                <i class="fa fa-upload"></i>
						<span class="filename"></span>
		            </span>
				</label>&nbsp;
				<button type="submit" name="theme-upload" value="1" class="btn btn-primary" id="theme-upload">
					Upload Theme
				</button>
			</div>
		</form>
	</div>
</div>