@php $analyticsUser = $Page->getAnalyticsUser(); @endphp
<label class="form-control-static">
	Google Analytics API Key
</label>
<fieldset class="form-group">
	@if ($analyticsUser)
		<button class="btn btn-secondary warn" name="delete-ga-key" id="delete-ga-key">
			<i class="ui-action ui-action-delete"></i>
			Delete
		</button>
		&lt;user id hidden&gt;
	@else
		<input type="text" class="form-control" name="ga-client-id" id="ga-client-id"
		       value="{{ $analyticsUser }}"/>
		<div class="alert alert-info" role="alert" id="origin-note">
			<i class="fa fa-sticky-note"></i>
			Add <b>http{{ (\Util_HTTP::isSecure() ? 's' : '')}}://{{\Util_HTTP::getHost()}}</b>
			as a <b>Authorized JavaScript origin</b>.
			(see KB: <a class="ui-action ui-action-label ui-action-kb"
			            href="{{ MISC_KB_BASE }}/control-panel/linking-google-analytics">Linking Google
				Analytics to {{ PANEL_BRAND }}</a>)
		</div>
		@if (is_debug())
			<div class="ui-action ui-action-label ui-action-tooltip" data-toggle="tooltip"
			     title="Providing your Client Secret will skip the occasional Google login screen before Analytics loads">
				API Client Secret
			</div>
			<input type="text" class="form-control" name="ga-client-secret" id="ga-client-secret"
			       value="" placeholder="optional"/>
		@endif
	@endif
</fieldset>
