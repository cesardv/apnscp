<div id="apps" class="tab-pane" role="tabpanel" aria-labelledby="">
	@includeWhen(\UCard::is('admin') || cmd('dns_configured'), 'partials.apps.dns-manager')
	@includeWhen(\UCard::is('admin'), 'partials.apps.nexus')
	@include('partials.apps.webapps')
</div>