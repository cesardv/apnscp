<h4 class="">
	<i class="ui-action ui-action-label ui-menu-category-nexus d-inline"></i> Nexus
</h4>
<fieldset class="form-group">
	<div class="form-check">
		<label class="custom-control custom-checkbox mb-0 align-items-center">
			<input type="hidden" value="0"
			       name="pref{{ \HTML_Kit::prefify(apps\nexus\Page::NOTIFY_CREATE_KEY) }}" />
			<input class="form-check-input custom-control-input"
			       @if (\Preferences::get(apps\nexus\Page::NOTIFY_CREATE_KEY)) checked @endif
			       type="checkbox" value="1" name="pref{{ \HTML_Kit::prefify(apps\nexus\Page::NOTIFY_CREATE_KEY) }}" />
			<span class="custom-control-indicator"></span>
			<i class="fa fa-sticky-note-o mr-1" data-toggle="tooltip"
			   title="A welcome email contains all login information in addition to helpful advice."></i>
			Send welcome emails on account creation
		</label>
	</div>

	<div class="form-check">
		<label class="custom-control custom-checkbox mb-0 align-items-center">
			<input type="hidden" value="0"
			       name="pref[nexus][show-inodes]"/>
			<input class="form-check-input custom-control-input"
			       @if (\Preferences::get('nexus.show-inodes')) checked @endif
			       type="checkbox" value="1" name="pref[nexus][show-inodes]">
			<span class="custom-control-indicator"></span>
			<i class="fa fa-sticky-note-o mr-1" data-toggle="tooltip" title="inode limits often serve as a nonobvious restriction to unlimited storage, but are useless enforcements on 64-bit filesystems."></i>
			Show inode Consumption
		</label>
	</div>

	<div class="form-check">
		<label class="custom-control custom-checkbox mb-0 align-items-center">
			<input type="hidden" value="0"
			       name="pref[nexus][show-resources]"/>
			<input class="form-check-input custom-control-input"
			       @if (!CGROUP_SHOW_USAGE) disabled @elseif (\Preferences::get('nexus.show-resources', false)) checked @endif
			       type="checkbox" value="1" name="pref[nexus][show-resources]">
			<span class="custom-control-indicator"></span>
			<i class="fa fa-sticky-note-o mr-1" data-toggle="tooltip"
			   title="Show cgroup resource usage."></i>
			Show System Resource Usage
		</label>
	</div>

	<h4>Indicator Thresholds</h4>
	<p class="help">
		Threshold appear next to monitored resources for each site in Nexus. These provide
		fast visual recognition while scanning for potential issues.
		<i class="fa fa-circle text-gray-dark"></i> always displays for unknown/quotaless sites.
		<i class="fa fa-circle text-success"></i> displays for unremarkable sites.
	</p>
	<label class="mr-3 mt-2">
		<i class="fa fa-circle text-warning"></i>
		Warning Threshold %
		<input type="number" min="0" max="100" class="form-control"
	       name="pref[nexus][threshold-warning]" value="{{ \Preferences::get('nexus.threshold-warning', Page_Container::kernelFromApp('nexus')::THRESHOLD_WARNING) }}"/>
	</label>
	<label class="mt-2">
		<i class="fa fa-circle text-danger"></i>
		Danger Threshold %
		<input type="number" min="0" max="100" class="form-control"
	       name="pref[nexus][threshold-danger]" value="{{ \Preferences::get('nexus.threshold-danger', Page_Container::kernelFromApp('nexus')::THRESHOLD_DANGER) }}"/>
	</label>
</fieldset>
