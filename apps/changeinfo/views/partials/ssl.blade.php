<div id="ssl" class="tab-pane" role="tabpanel" aria-labelledby="">
	{{-- MAIN server certificate should remain for compatibility across services --}}
	<div class="mb-3">
		<h4>SSL Key Type</h4>
		<p>
			Change the key type used for SSL. EC has a smaller payload and 20% faster throughput but cannot
			negotiate with <a href="https://support.globalsign.com/customer/en/portal/articles/1995283-ecc-compatibility">older software</a> (Windows Vista, RHEL 6.5, Mac OS X 10.6 not supported).
			@if (\UCard::is('admin'))
			Once changed, a new certificate must be reissued via <code>cpcmd letsencrypt:renew</code>.
			@else
				Once changed, a new certificate must be reissued via <b>Web</b> &gt;
					<a href="{{ Template_Engine::init()->getPathFromApp('ssl') }}">SSL Certificates</a>.
			@endif
		</p>
		<fieldset>
			<select class="custom-select" name="pref{{\HTML_Kit::prefify(\Opcenter\Crypto\Letsencrypt\Preferences::KEY_TYPE) }}">
				@foreach(['EC', 'RSA'] as $type)
					<option value="{{ $type }}"
				        @if (\Preferences::get(\Opcenter\Crypto\Letsencrypt\Preferences::KEY_TYPE, \Opcenter\Crypto\Letsencrypt\DispatchServices\Acmephp\KeypairFinder::DEFAULT_KEY) === $type) selected="SELECTED" @endif>
						{{ $type }}
					</option>
				@endforeach
			</select>
		</fieldset>
	</div>

	<div class="mb-3">
		<h4>Strict Tolerance</h4>
		<p>
			Change the behavior of certificate renewal. A single failure in a renewal for any reason will cause the entire
			renewal chain to fail. All domains attached to the account <b>must</b> be active and owned by the account for
			this to work.
			@if (\UCard::is('admin'))
				To remove a domain change <code>ssl_hostnames</code> <a href="/apps/scopes/apply/cp.bootstrapper">Scope</a> value,
				then restart {{ PANEL_BRAND }}.
			@else
				To remove a domain visit <b>Web</b> &gt; <a href="{{ Template_Engine::init()->getPathFromApp('ssl') }}">SSL Certificates</a>
				when tolerance is disabled.
			@endif
		</p>
		<label class="custom-control custom-checkbox mb-0">
			<input type="hidden" name="pref{{\HTML_Kit::prefify(\Opcenter\Crypto\Letsencrypt\Preferences::SENSITIVITY) }}" value="0" />
			<input type="checkbox" class="custom-control-input"
			       name="pref{{\HTML_Kit::prefify(\Opcenter\Crypto\Letsencrypt\Preferences::SENSITIVITY) }}" value="1" @if (Preferences::get(\Opcenter\Crypto\Letsencrypt\Preferences::SENSITIVITY, LETSENCRYPT_STRICT_MODE)) checked="CHECKED" @endif />
			<span class="custom-control-indicator"></span>
			Enable strict tolerance. Certificates may not drop a hostname during renewal or issuance due to DNS or unreachability.
		</label>
	</div>

	<div class="mb-3">
		<h4>Verify IP</h4>
		<p class="mb-0">
			Ensure the IP address of the hostname
			@if (UCard::init()->hasPrivilege('admin'))
				({{ \Opcenter\Net\Ip4::my_ip() }})
			@else
				({{ cmd('dns_get_public_ip') }})
			@endif
			matches the domain prior to requesting a SSL certificate.
		</p>
		<ul class="pl-3 mt-3">
			<li class="mb-1">If a domain has expired, enabling <b>Verify IP</b> will remove the domain from the list prior to requesting SSL.
				Enabling with <b>Strict tolerance</b> ensures that domains that have expired <em>do not</em> halt automated renewals.
			</li>
			<li>
				Disabling <em>Verify IP</em> with <b>Strict tolerance</b> enabled will cause a certificate renewal to fail if
				any domain is placed behind a proxy, such as CloudFlare, and the IP address does not match
				@if (\UCard::init()->hasPrivilege('admin'))
					{{ \Opcenter\Net\Ip4::my_ip() }}.
				@else
					{{ \cmd('dns_get_public_ip') }}.
				@endif
			</li>
		</ul>
		<label class="custom-control custom-checkbox mb-0">
			<input type="hidden" name="pref{{\HTML_Kit::prefify(\Opcenter\Crypto\Letsencrypt\Preferences::VERIFY_IP) }}" value="0"/>
			<input type="checkbox" class="custom-control-input"
			       name="pref{{\HTML_Kit::prefify(\Opcenter\Crypto\Letsencrypt\Preferences::VERIFY_IP) }}" value="1"
			       @if (Preferences::get(\Opcenter\Crypto\Letsencrypt\Preferences::VERIFY_IP, LETSENCRYPT_VERIFY_IP)) checked="CHECKED" @endif />
			<span class="custom-control-indicator"></span>
			Perform IP validation prior to certificate request.
		</label>
	</div>
</div>