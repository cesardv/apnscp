<div class="modal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 id="modalLabel" class="modal-title">Edit <span id="edit-email-name"></span></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-12">
						<fieldset class="form-group row" id="address-entry">
							<div class="col-12">
								<div class="input-group">
									<span class="input-group-addon"><i class='fa fa-user'></i></span>
									<input type="text" maxlength="52" class="form-control col-4"
									       placeholder="user"
									       name="username" id="username" aria-describedby="user name"/>
									<span class="input-group-addon">@</span>
									<select name="domain[]" class="form-control custom-select" id="domain">
										@foreach ($Page->get_transports() as $host)
											<option id="{{ $host }}" value="{{ $host }}">
												{{ $host }}
											</option>
										@endforeach
									</select>
								</div>
							</div>
						</fieldset>
					</div>
				</div>

				<fieldset class="form-group row">
					<label class="col-md-12">Delivery Type</label>
					<div class="col-md-12">
						<div class="btn-group d-flex form-inline justify-content-end" data-toggle="buttons"
						     id="deliveryType">
							<div class="input-group mr-auto">
								<span class="input-group-addon ui-action ui-action-label ui-action-email-local"></span>
								<label class="btn btn-outline-secondary active form-control-static d-flex align-items-center"
								       data-toggle="">
									<input type="radio" class="radio-inline mr-2" data-toggle="radio-collapse"
									       aria-expanded="false"
									       data-parent="deliveryOptions" aria-controls="userContainer"
									       data-target="#userContainer" id="type_single" name="type" value="v"
									       checked="CHECKED"/>
									Single User
								</label>
							</div>
							<div class="input-group">
								<span class="input-group-addon ui-action-label ui-action ui-action-email-forward"></span>
								<label class="btn btn-outline-secondary form-control-static d-flex align-items-center"
								       for="type_forwarded">
									<input type="radio" id="type_forwarded" class="radio-inline mr-2" name="type"
									       value="a"
									       data-parent="deliveryOptions" data-toggle="radio-collapse"
									       aria-controls="forwardContainer" data-target="#forwardContainer"/>
									Forward or Multiple Users
								</label>
							</div>
						</div>
					</div>
				</fieldset>

				<div class="panel-group" id="deliveryOptions">

					<div class="panel form-group collapse" id="userContainer">
						<label for="mailbox">Deliver to this user account:</label>

						<select name="mailbox" class="local-delivery form-control custom-select" id="mailbox">
							@foreach ($Page->get_users() as $user => $data)
								<option value="{{ $user }}">
									{{ $user }}
								</option>
							@endforeach
						</select>
					</div>

					<div class="panel form-group clearfix row collapse" id="forwardContainer">
						<div class="form-group col-12 clearfix">
							<label>Include these external email addresses:</label>
							<textarea name="alias_remote" id="alias_remote" rows="3"
							          class="forward-delivery form-control"></textarea>
							<small>separate multiple addresses with commas</small>
						</div>

					</div>


				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="edit-save">Save</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->