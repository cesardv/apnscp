<div class="alert alert-warning">
	<h4>Disabled Address Warning</h4>
	<p>
		One or more e-mail addresses have been disabled on this account. A disabled e-mail account
		will be unable to receive e-mail unless it is re-enabled. E-mail accounts may be re-enabled
		by clicking the <span class="ui-action ui-action-enable ui-action-label">enable icon</span>,
		or by selecting multiple addresses and selecting <em>Enable Selected Addresses</em>.
		<br/><br/>
		Remember to check <a class="ui-action ui-action-label ui-action-switch-app" href="/apps/usermanage">storage
			use</a>.
		Accounts are automatically disabled if they exceed their storage limits for 5 or more days. Storage limits
		may be
		re-adjusted through <a class="ui-action ui-action-label ui-action-switch-app" href="/apps/usermanage">Manage
			Users</a>.
	</p>
</div>

<h3>Disabled Addresses</h3>
<table width="100%" id="disabled_mailbox_routes" class="tablesorter table">
	<thead>
		<tr>
			<th class="nosort text-center"></th>
			<th width="120">
				User
			</th>
			<th width="170">
				Domain
			</th>
			<th>
				Destination
			</th>
			<th class="nosort center" width="90">
				Actions
			</th>
		</tr>
	</thead>
	<tbody>
		@include('partials.address-list', ['accounts' => $Page->get_mailboxes('disabled'), 'listMode' => 'disabled'])
	</tbody>

	<tfoot>
	<tr>
		<td colspan="5" class="cell1" align="center">
			<button type="submit" class="btn btn-secondary" name="sel_enable" value="Enable Selected Addresses">
				Enable Selected Addresses
			</button>
			<button type="submit" class="btn btn-secondary warn" name="sel_delete"
			        value="Delete Selected Addresses">
				Delete Selected Addresses
			</button>
		</td>
	</tr>
	</tfoot>
</table>

<hr />