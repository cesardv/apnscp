<div class="bg-light row mb-1">
	<div class="heading d-flex col-12 py-2">
		<h4 class="mb-0">
			{{ SERVER_NAME }}
			<span class="d-none d-md-inline-block">({{ Opcenter\Net\IpCommon::my_ip() }})</span>
			&mdash;
			{{ count($Page->listAccounts()) }} {{ \Illuminate\Support\Str::plural('account', \count($Page->listAccounts())) }}
			<span class="d-none d-sm-inline">
				&middot;
				{{ count($Page->listDomains()) }}  {{ \Illuminate\Support\Str::plural('domain', \count($Page->listDomains())) }}
			</span>
		</h4>
		<div id="account-sortables" class="ml-auto d-none d-md-block"></div>
	</div>
</div>
<form method="POST" action="{{ \HTML_Kit::page_url() }}">
	<input type="hidden" name="suspend-reason" value="" id="suspensionReason" />
	@php
			$showInodes = \Preferences::get('nexus.show-inodes', false);
			$showResources = \Preferences::get('nexus.show-resources', false);
	@endphp
	@foreach ($Page->listAccounts() as $site => $meta)
		@continue(!$meta)
	<div class="row mb-1 hostname account-item" data-domain="{{ $meta['siteinfo']['domain'] }}" data-addon="{{ $meta['aliases']['aliases'] }}" data-email="{{ implode(',',(array)$meta['siteinfo']['email']) }}" data-admin-user="{{ $meta['siteinfo']['admin_user'] }}" data-site-id="{{ $meta['site_id'] }}"
	     data-invoice="{{ $meta['billing']['invoice'] }}">
		<div class="col-12 d-flex align-items-md-center flex-column flex-md-row align-content-center align-items-start">
			<div class="d-inline-flex align-items-center">
				<label class="custom-control custom-checkbox mb-0"
				       for="{{ "check-${site}" }}">
					<input type="checkbox" id="check-{{$site}}" class="custom-control-input"
				       name="select[]" value="{{ $site }}" />
					<span class="custom-control-indicator"></span>
				</label>
				<div class="btn-group mr-3">
					<a class="ui-action ui-action-select ui-action-label btn btn-secondary"
					   href="/apps/nexus/edit/{{ $site }}">
						Select
					</a>
					<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
					        aria-haspopup="true"
					        aria-expanded="false">
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<div class="dropdown-menu" aria-labelledby="">
						<button class="ui-action-visit-site ui-action-label dropdown-item ui-action btn btn-block"
						        name="hijack" value="{{ $site }}">
							Login As
						</button>
						@if (array_get($meta, 'active', true))
						<button class="ui-action-disable ui-action-label dropdown-item ui-action btn btn-block"
						        type="submit"
						        name="suspend" value="{{ $site }}">
							Suspend Site
						</button>
						@else
						<button class="ui-action-activate ui-action-label dropdown-item ui-action btn btn-block"
						        type="submit"
						        name="activate" value="{{ $site }}">
							Activate Site
						</button>
						@endif
						<div class="dropdown-divider"></div>
						<button title="delete" type="submit"
						        class="dropdown-item btn btn-block warn ui-action-label ui-action ui-action-delete"
						        data-toggle="tooltip"
						        data-title="Permanently delete this account and its data"
						        data-domain="{{ $meta['siteinfo']['domain'] }}"
						        name="delete" value="{{ $site }}">
							Delete Site
						</button>
					</div>
				</div>

				<div class="d-inline-flex">
					@if (!$meta['active'])
						<i class="ui-action-disable ui-action-label text-danger" data-toggle="tooltip"
						   title="Account is suspended"></i>
					@endif
					@if ($meta['amnesty']['diskquota'])
						<i class="fa fa-medkit text-info ui-action-label" data-toggle="tooltip"
						   title="Account is under storage amnesty until {{ (new DateTime())->setTimestamp($meta['amnesty']['diskquota'])->format('r') }}"></i>
					@endif
						@if ($meta['amnesty']['diskquota'])
							<i class="fa fa-link text-info ui-action-label" data-toggle="tooltip"
							   title="Account is subordinate to {{ $meta['billing']['invoice'] }}"></i>
						@endif
					{{ $meta['siteinfo']['domain'] }}
				</div>

			</div>

			<div class="d-inline-flex">
				<div class="ml-auto mr-0 d-inline-flex account-attributes">
					<span class="admin-user ml-3 d-none attribute">
						<i class="fa fa-user"></i>
						{{ $meta['siteinfo']['admin_user'] }}
					</span>

					<span class="addon ml-3 d-none attribute">
						<i class="fa fa-globe"></i>
						{{ $meta['aliases']['aliases'] }}
					</span>

					<span class="site-id ml-3 d-none attribute">
						<i class="fa fa-globe"></i>
						{{ $meta['site_id'] }}
					</span>
					<span class="email ml-3 d-none attribute">
						<i class="fa fa-envelope"></i>
						{{ implode(',', (array)$meta['siteinfo']['email']) }}
					</span>

					<span class="invoice ml-3 d-none attribute">
						<i class="fa fa-money"></i>
						{{ $meta['billing']['invoice'] }}
					</span>
				</div>
			</div>

			@include('partials.resource-usage', [
				'storage'    => $Page->getResourceUsage('storage', $site),
				'bandwidth'  => $Page->getResourceUsage('bandwidth', $site),
				'cgroup'     => $showResources ? $Page->getResourceUsage('cgroup', $site) : []
			])

		</div>
		<hr class="d-block my-2 w-100 d-md-none" />
	</div>
	@endforeach

	<hr />
	<div class="row">
		<div class="col-12 mb-3">
			<h3>With selected</h3>
			<button name="suspend-bulk" type="submit" class="btn btn-secondary mr-3 mt-3 ui-action-disable warn">
				Suspend Sites
			</button>
			<button name="activate-bulk" type="submit" class="btn btn-secondary mr-3 mt-3 ui-action-activate">
				Activate Sites
			</button>
		</div>
		<div class="col-12 mt-3">
			<b>Command-line tools</b>
			<ul class="list-styled pl-3">
				<li/> <code class="font-normal">DeleteDomain --since="last month"</code>: remove suspended accounts older than a month from the server.
				<li/> <code>cpcmd scope:set opcenter.account-cleanup <em>AGE</em></code>: set a periodic cleanup schedule.
			</ul>
		</div>
	</div>
</form>

@include('partials.suspend-modal')