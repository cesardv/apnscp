<form method="POST" action="{{ \HTML_Kit::page_url() }}">
	<button type="submit" name="hijack" value="{{ $domain }}" class="btn btn-secondary ui-action-visit-site ui-action-label ui-action">
		Login As
		@if ($Page->getCreateTarget()) {{ $domain }} @endif
	</button>
</form>
