<div class="row align-items-stretch">
	<div class="col-6">
		@include('partials.return-overview')
	</div>
	<div class="col-6 text-right">
		@includeWhen($mode === 'edit' || !empty($Page->getCreateTarget()), 'partials.btns.login-as', ['domain' => $Page->getSite()])
	</div>
</div>
<hr/>
<div class="row">
	<div class="col-12">
		<form method="post" action="{{ HTML_Kit::page_url() }}">
			<div class="form-group">
				<label for="domainName">Domain name</label>
				<input type="text" class="form-control" id="domainName" placeholder="Enter domain"
				       name="config[siteinfo][domain]" value="{{ $Page->getInput('siteinfo.domain') }}">
			</div>
			<div class="form-group">
				<label for="email">Contact email</label>
				<input type="text" class="form-control" id="email" placeholder="Contact email"
				       name="config[siteinfo][email]"
				       value="{{ $Page->getInput('siteinfo.email', \cmd('common_get_email')) }}">
			</div>
			<div class="form-group">
				<label for="admin">Admin username</label>
				<input type="text" class="form-control" id="admin" placeholder="myuser"
				       name="config[siteinfo][admin_user]" value="{{ $Page->getInput('siteinfo.admin_user') }}">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<div class="input-group">
					<input type="password" class="form-control" id="password" placeholder="Password"
				           name="config[auth][tpasswd]" value="{{ $Page->getInput('auth.tpasswd') }}">
					@if ($mode === 'new')
						<div class="hidden-sm-down input-group-addon form-inline options border-left-0">
							<label class="custom-control custom-checkbox  d-inline-flex mr-0 mb-0 align-items-center">
								<input type="checkbox" name="opts[crypt]" value="1"
								       @if ($Page->getInput('crypt', false, 'opts.')) checked @endif
								       class="custom-control-input"/>
								<span class="custom-control-indicator mr-2"></span>
								<span class="">{{ _("Hide in welcome") }}</span>
							</label>
						</div>
					@endif
				</div>
			</div>
			<div class="form-group">
				<label for="notes">Account notes</label>
				<textarea class="form-control" name="config[siteinfo][notes]">{{ implode("\n", (array)$Page->getInput('siteinfo.notes')) }}</textarea>
			</div>

			<div class="d-flex">
				<h2>Service Configuration</h2>
				<div class="ml-auto d-inline-flex align-items-center">
					@if ($mode === 'new')
						<label class="custom-switch hidden-sm-down custom-control mb-0 pl-0 mr-3">
							<input type="checkbox" @if ($Page->getInput('notify', \Preferences::get(\apps\nexus\Page::NOTIFY_CREATE_KEY), 'opts.')) checked @endif
							class="custom-control-input" name="opts[notify]" value="1" />
							<span class="custom-control-indicator mr-2"></span>
							Send welcome email
						</label>
					@endif
					@include("partials.btns.${mode}-site")
				</div>

			</div>
			<hr/>
			<div class="row bg-light">
				<div class="col-12 d-flex align-items-center">
					<label class="my-1">
						Plan template
						<select class="custom-select mr-3" name="config[siteinfo][plan]" id="plan">
							@foreach ($Page->getPlans() as $plan)
								<option @if ($Page->planActive($plan)) selected="SELECTED" @endif>{{ $plan }}</option>
							@endforeach
						</select>
					</label>
					<div class="ml-auto text-light">
						<i class="ui-action ui-action-visit-site"></i>
						use <a href="https://docs.apiscp.com/admin/Plans/#creating-plans" class="">opcenter:plan</a> to manage plans
					</div>
				</div>
			</div>
			@foreach ($Page->getServices() as $svc => $options)
				<div class="service-grouping">
					<div class="pl-0 pr-0 pb-0 form-control-lg">
						<label class="d-flex custom-control custom-checkbox" for="{{ "opt${svc}enabled" }}" >
							<input type="hidden" name="config[{{$svc}}][enabled]"
						       value="{{ $Page->serviceIs($svc, 'enabled', \Opcenter\Service\Validators\Common\MustBeEnabled::class) ? '1' : '0' }}" />
							<input type="checkbox" class="custom-control-input service-toggle" data-service="{{ $svc }}"
							       @if ($Page->serviceIs($svc, 'enabled', \Opcenter\Service\Validators\Common\MustBeEnabled::class))
							        disabled="DISABLED"
							       @endif
							       data-target="{{ "#${svc}Options" }}" name="config[{{$svc}}][enabled]"
							       value="1" data-target="{{ "#${svc}Options" }}"
							       @if ($Page->getInput("${svc}.enabled")) checked="CHECKED"
							       @endif id="{{ "opt${svc}enabled" }}" />

							<span class="custom-control-indicator mt-1"></span>
							<span class="mr-2">{{ $svc }}</span>
							<span class="align-self-end text-right d-block ml-auto">{{ $options['enabled']['help'] }}</span>
						</label>

					</div>
					<div class="service-vars ml-4 collapse @if ($Page->getInput("${svc}.enabled")) show @endif" id="{{ "${svc}Options" }}">
						@foreach ($options as $optname => $optinfo)
							@continue($optname === 'enabled' || $Page->isIgnored("${svc}.${optname}"))
							<div class="form-group mb-2">
								<div class="row service-var @if ($Page->hasChange($svc, $optname)) has-change @endif">
									<div class="col-12 col-md-6 col-xl-4">
										@if ($Page->isBool($optinfo))
											<div class="">
												<label class="custom-control custom-checkbox"
												       for="{{ "opt${svc}${optname}" }}">
													<input type="hidden" name="config[{{$svc}}][{{$optname}}]"
													       value="{{ (int)$Page->getInput("${svc}.${optname}", 0) }}"/>
													<input type="checkbox" class="custom-control-input"
													       @if ($Page->serviceIs($svc, $optname, \Opcenter\Service\Validators\Common\MustBeEnabled::class))
														       disabled="DISABLED"
													       @endif
													       name="config[{{$svc}}][{{$optname}}]" value="1"
													       @if ($Page->getInput("${svc}.${optname}")) checked="CHECKED"
													       @endif id="{{ "opt${svc}${optname}" }}"/>
													<span class="custom-control-indicator"></span>
													{{ $optinfo['help'] }}
												</label>
											</div>
										@else
											<label class="form-label">
												{{ $optname }}
											</label>
											@if (is_array($input = $Page->getInput("${svc}.${optname}")))
												<textarea class="form-control"
												          name="config[{{$svc}}][{{$optname}}]">{{
												          implode(PHP_EOL, array_key_map(
												          static function ($k, $v) use ($input) {
															// prevent parsing IPv6 [0:IPv6,1:IPv6] as 0:IPv6\n1:IPv6...
															// simply checking if isset($input[0]) may be better.
												            if (isset($input[0])) {
												                return $v;
												            }
												            return "${k}:${v}";
												          }, $input))
										          }}</textarea>
												<p class="text-info">Value is array, separate each entry with
													linebreak</p>
											@elseif (\is_array($optinfo['range']))
												<select class="custom-select d-block w-100" name="config[{{$svc}}][{{$optname}}]">
													@foreach ($optinfo['range'] as $val):
													<option @if ($input === $val) selected="SELECTED" @endif>{{ $val }}</option>
													@endforeach
												</select>
											@else
												<input type="text" class="form-control"
												       name="config[{{$svc}}][{{$optname}}]" type="form-control"
												       value="{{ $input }}"/>
											@endif
											@if ($Page->isNullable($svc, $optname))
												<label class="custom-control mt-1 custom-checkbox"
												       for="{{ "opt${svc}${optname}Nulled" }}">
													<input type="checkbox" class="custom-control-input default-nullable"
													       name="config[{{$svc}}][{{$optname}}]"
													       value="{{ \Opcenter\Service\Contracts\DefaultNullable::NULLABLE_MARKER }}"
													       id="{{ "opt${svc}${optname}Nulled" }}"/>
													<span class="custom-control-indicator"></span>
													Revert to DEFAULT
												</label>
											@endif
											<p class="form-text text-muted">
												{{ $optinfo['help'] }}
											</p>
										@endif
									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
				<hr />
			@endforeach
			@include("partials.btns.${mode}-site")

		</form>
	</div>
</div>