$(document).ready(function () {

	$('.ajax-wait').ajaxWait({reset: false});
	//$('[data-toggle="tooltip"]').tooltip();
	$('.service-toggle').change(function() {
		$($(this).data('target')).collapse('toggle');
		return true;
	});

	$('#ui-app .ui-action-disable').click(function (e) {
		if ($(e.target).hasClass('.btn-primary')) {
			return true;
		}
		var sites = $(this).val();
		if (!sites) {
			sites = $('#accountManagement').serializeArray().map(function (v) {
				return v.value;
			});
		}
		$('#suspendModal').modal('show').on('shown.bs.modal', function () {

			var formVal = (Array.isArray(sites) ? sites : [sites]).filter(function (el) {
				return !!el;
			});
			$(this).find('.btn-primary:eq(0)').val(formVal.join(',')).end().find('.site-list').text(formVal.map(function (v) {
				return SITES.hasOwnProperty(v) ? SITES[v] : v;
			}).join(", "));
			$('#enableReason').click(function () {
				var checked = $(this).prop('checked');
				$('#reason').prop('disabled', !checked);
				if (checked) {
					$('#reason').focus();
				}
			});
		});
		return false;
	});

	$('.account-item').highlight();

	$('.default-nullable').change(function() {
		var $input = $(this).closest('.service-var').find(':input:first');
		if ($(this).prop('checked')) {
			$input.prop('disabled', true);
		} else {
			$input.prop('disabled', false);
		}
	});

	$('#ui-app .ui-action-delete').click(function(e) {
		return confirm("Are you sure you want to delete " + $(this).data('domain') + "?");
	});

	$('#plan').change(function () {
		$(this).closest('form').append($('input').attr('name', 'change-plan')).submit();
		return true;
	});

	$('.filter-accounts').bind('click change', function (e) {
		do_filter(e);
		return false;
	}).bind('keydown', function (e) {
		if (e.keyCode === $.ui.keyCode.ESCAPE) {
			$('#reset_filter').click();
			$('#filter').val('');
		} else if (e.keyCode === $.ui.keyCode.ENTER) {
			$(this).triggerHandler('click');
		}
	});

	update_filters();
});
// @TODO extract to general utility module
var $filters = $('#searchop').clone();

function do_filter(e) {
	if (e.type == 'change' && e.target.id == 'filter_spec') {
		return update_filters();
	} else if (e.target.id == "do_filter") {
		var field = $('#filter_spec').val(),
			patternbase = $('#filter').val(),
			op = $('#searchop').val(), re,
			pattern = patternbase,
			/** base regex comparator */
			comparator;

		switch (op) {
			case 'contains':
				break;
			case 'notcontains':
				pattern = '^(?!.*' + pattern + ').*$';
				break;
			case 'is':
				pattern = pattern.toLowerCase();
				comparator = function (field) {
					return $(this).data(field).toString().trim().toLowerCase() != pattern;
				};
				break;
			case 'isnot':
				pattern = pattern.toLowerCase();
				comparator = function (field) {
					return $(this).data(field).toString().trim().toLowerCase() == pattern;
				};
				break;
		}
		// some switches may define their own comparator
		// if none is set revert to the regex-based comparator
		if (!comparator) {
			comparator = function (field) {
				re = new RegExp(pattern, "i");
				return re.test($(this).data(field).toString().trim()) == false;
			}
		}

		$('.row.hostname').filter(function () {
			if (!comparator.call(this, field)) {
				var $dataField = $('.account-attributes .attribute.' + field, this);
				$dataField.data('refcnt', (parseInt($dataField.data('refcnt')) || 0) + 1);
				$dataField.removeClass('d-none');
				return false;
			}
			return true;
		}).fadeOut('fast', function () {});

		var $el = $('<li class="mr-3"/>').addClass("label ui-filter-spec " +
			'filter-comp-' + op + ' filter-' + field).text(patternbase).click(function () {
			return remove_filter.apply(this, [$(this).data('comparator'), $(this).data("type")]);
		}).data({
			type: field,
			comparator: comparator
		});

		$('ul.ui-active-filters').append($el);

		if ($('#reset_filter:hidden').length > 0) {
			$('#reset_filter').removeClass('hide').get(0).style = '';
		}

		return false;
	} else if (e.target.id == "reset_filter") {
		$('.filter-accounts ul.ui-active-filters').empty();
		$('#reset_filter').hide();
		$('.row.hostname .account-attributes .attribute').data('refcnt', 0).addClass('d-none');
		$('.row.hostname:hidden').fadeIn('fast');
		return false;
	}

	return true;
}

/**
 * update filter operations
 */
function update_filters() {
	var $el = $('#filter_spec');
	$('#searchop').replaceWith($filters.clone());

	if ($.inArray($el.val(), []) >= 0) {
		$('#searchop option').each(function (e) {
			var val = $(this).val();
			if (val != 'service-enabled' && val != 'service-disabled') {
				$(this).remove();
			}
		});
		$('#filter').fadeOut();
	} else {
		if ($('#filter:hidden')) {
			$('#filter').fadeIn();
		}
	}

	return true;
}

/**
 * Remove filter and relist hidden items
 */
function remove_filter(comparator, field) {
	$('.row.hostname').filter(function() {
		var $attrData = $('.account-attributes .attribute.' + field, this),
			refcnt = parseInt($attrData.data('refcnt') || 0);

		if (!comparator.call(this, field)) {
			if (!refcnt) {
				$attrData.addClass('d-none');
			}
			$attrData.data('refcnt', --refcnt);
			return true;
		}
		return false;
	}).fadeIn('fast');

	$(this).remove();
	if ($('.ui-active-filters li').length == 0) {
		$('#reset_filter').fadeOut();
	}
	return false;
}


