<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\summary;

	use NumberFormatter;

	include_once(INCLUDE_PATH . '/lib/html/userdefaults-loader.php');
	include_once(INCLUDE_PATH . '/lib/modules/email.php');

	class Info_Bit
	{
		private $title;
		private $data;
		private $format;

		public function __construct($title, $data = array(), $format = array())
		{
			$this->title = $title;
			$this->data = $data;
			$this->format = $format;
		}

		public function append(Info_Bit $child)
		{
			$this->data[] = $child;
		}
	}

	class Page extends \Page_Container
	{
		use \FilesystemPathTrait;

		//$('ul li:contains(Domains) ul','#e-mail').html();
		private $info;
		private $lvparser = 0;
		private $p_info = null;
		private $i_info = null;

		private $group = null;
		private $plv_html = -1;

		public function __construct()
		{
			parent::__construct();

			$this->info = array();

			$this->add_javascript('summary.js');
			$this->add_javascript('AccountSummary.truncate();', 'internal', true, true);

			if ($this->getAuthContext()->level & PRIVILEGE_ADMIN) {
				$this->_init_info_admin();
			} else if ($this->getAuthContext()->level & PRIVILEGE_RESELLER) {
				$this->_init_info_reseller();
			} else if ($this->getAuthContext()->level & PRIVILEGE_SITE) {
				$this->_init_info_site();
			} else {
				$this->_init_info_user();
			}

			$this->p_info = array();
			$this->i_info = $this->info;

			$this->domains = array();


		}

		private function _init_info_admin()
		{

		}

		private function _init_info_reseller()
		{


		}

		private function _init_info_site()
		{
			$formatter = NumberFormatter::create(\Auth::profile()->language, NumberFormatter::CURRENCY);
			$this->info = array(
				'General Information' => $this->getGeneralInformation(),
				'Billing'             => array(
					'Customer Since'                                                             => date('F Y',
						$this->billing_get_customer_since()),
					'Package Type'                                                               => $this->billing_get_package_type(),
					'Account Standing'                                                           => $this->_billing_standing(),
					'Payment Date'                                                               => $this->_next_payment_date('date') .
						' &#8212; ' . $formatter->format($this->_next_payment_date('amount')),
					$this->_edit_inline('Payment Method',
						$this->linkIfEnabled('changebilling'))                                   => $this->_payment_method()
				)
			);
			$this->info['Mail'] = $this->getMail();

			$this->info['SQL'] = array(
				'General Settings' => array(
					'Prefix'        => $this->common_get_service_value('mysql', 'dbaseprefix'),
					'Primary User'  => $this->common_get_service_value('mysql', 'dbaseadmin'),
					'Database Host' => 'localhost'
				),
				$this->_edit_inline('MySQL', $this->linkIfEnabled('changemysql'))
								   => $this->_sql_info('mysql'),
				$this->_edit_inline('PostgreSQL', $this->linkIfEnabled('changepgsql'))
								   => $this->_sql_info('pgsql')
			);

			$this->info['Web'] = $this->getWeb();
			$this->info['Development'] = $this->_development_tools();
			if (!$this->getApnscpFunctionInterceptor()->billing_configured()) {
				unset($this->info['Billing']);
			}

		}

		private function getGeneralInformation()
		{
			$config = [
				$this->_edit_inline('Primary Domain', '/apps/changeinfo')  => $this->common_get_service_value('siteinfo',
					'domain'),
				$this->_edit_inline('Admin Username',
					'/apps/changeinfo')                                    => $this->common_get_service_value('siteinfo',
					'admin_user'),
				$this->_edit_inline('Contact Address',
					'/apps/changeinfo')                                    => $this->common_get_service_value('siteinfo',
					'email'),
				"IP Address"                                               => $this->dns_get_public_ip()
			];
			if ($this->dns_get_public_ip() !== ($internal = array_get($this->common_get_ip_address(), '0', null))) {
				$config['Internal IP Address'] = $internal;
			}
			$config += [
				"Storage Usage"                                                              => [
					$this->_disk_quota(),
					$this->common_get_service_value('diskquota', 'enabled') ?
						"<button name='amnesty' id='amnesty' class='btn btn-secondary'>Storage Amnesty</button>" : ''
				],
				"Bandwidth"                                                                  => [
					$this->_bandwidth_usage() . ' <br />Rollover on ' . $this->_bandwidth_rollover(),
				],
				$this->_edit_inline('Users', $this->linkIfEnabled('usermanage'))             => [
					'Users'              => $this->_user_cnt() . '/' . $this->common_get_service_value('users', 'max',
							'&#8734;'),
					'Over Storage Limit' => ' ' . sprintf("%01u", $this->_user_cnt_quota())
				],
				$this->_edit_inline('Domains Hosted', $this->linkIfEnabled('domainmanager')) => [
					(count($this->_domains_hosted())) . '/' . $this->common_get_service_value('aliases', 'max',
						'&#8734;'),
					'Domain Expirations' => $this->_domain_expirations()
				]
			];

			return $config;
		}

		private function _edit_inline($title, $url)
		{
			if (!$url) {
				return $title;
			}

			return '<a class="" href="' . $url . '"><i class="fa fa-pencil-square-o mr-1"></i>' . $title . '</a>';
		}

		private function _disk_quota()
		{
			if (\UCard::init()->hasPrivilege('site', 'user')) {
				$quota = $this->common_get_disk_quota();
			}

			if (!$quota['total']) {
				return sprintf("%.2f MB/&infin;", $quota['used'] / 1024);
			}

			return sprintf("%.2f MB/%.2f MB (%u%%)", $quota['used'] / 1024, $quota['total'] / 1024,
				$quota['used'] / $quota['total'] * 100);
		}

		private function _bandwidth_usage()
		{
			$bw = $this->bandwidth_usage();

			return sprintf("%.2f GB/%.2f GB (%u%%)",
				$bw['used'] / 1024 / 1024 / 1024,
				$bw['total'] / 1024 / 1024 / 1024,
				$bw['used'] / $bw['total'] * 100
			);
		}

		private function _bandwidth_rollover()
		{
			return date('F jS', $this->bandwidth_rollover());
		}

		/**
		 * Get link if app enabled for account
		 *
		 * @param string $app
		 * @return string|null
		 */
		private function linkIfEnabled(string $app): ?string
		{
			static $cache = [];
			if (array_key_exists($app, $cache)) {
				return $cache[$app];
			}
			$engine = \Template_Engine::init();
			$link = null;
			if ($id = $engine->getApplicationFromId($app)) {
				$link = $id->getLink();
			}

			return $cache[$app] = $link;
		}

		/**
		 *     General
		 **/
		private function _user_cnt()
		{
			$users = $this->user_get_user_count();

			return $users['users'];
		}

		private function _user_cnt_quota()
		{
			$users = $this->user_get_quota(array_keys($this->user_get_users()));
			$warn_threshold = \User_Defaults::get_value('disk_quota_warn');
			$cnt = 0;
			foreach ($users as $user => $quota) {
				if ($quota['qhard'] && $quota['qused'] / $quota['qhard'] * 100 >= $warn_threshold) {
					$cnt++;
				}
			}

			return $cnt;
		}

		private function _domains_hosted()
		{
			return \Util_Conf::all_domains();
		}

		private function _domain_expirations()
		{
			$domains = \Util_Conf::all_domains();
			$expirations = array();
			$unknown = 0;
			foreach ($domains as $domain) {
				$exp = $this->dns_domain_expiration($domain);
				if (!$exp) {
					$unknown++;
				}
				$expirations[$domain] = $exp;

			}
			asort($expirations);

			$expirations = array_map(function ($domain, $ts) {
				return "$domain: " . ($ts > 0 ? date("Y-m-d", $ts) : "UNKNOWN");
			}, array_keys($expirations), array_values($expirations));

			return array(($unknown > 0 ? $unknown . " unknown" : ""), $expirations);

		}

		private function _billing_standing()
		{
			$status = $this->billing_get_status();
			if ($status == 0) {
				return $this->_edit_inline('Pending Expiration',
					'https://apisnetworks.com/billing?s=' . $this->billing_get_renewal_hash());
			} else if ($status == -1) {
				return 'Good Standing - non-recurring';
			} else if ($status == 1) {
				return "Good Standing - recurring";
			}

			return "UNKNOWN";
		}

		private function _next_payment_date($field)
		{
			$pd = $this->billing_get_next_payment();
			if ($field == 'amount') {
				return $pd['amount'];
			}
			if ($pd['amount'] < 0) {
				$hash = $this->billing_get_renewal_hash();
				$resp = 'Due By ';
				if ($hash) {
					$resp .= ' (<a href="https://apisnetworks.com/billing?s=' . $hash . '">renew</a>)';
				}

				return $resp;
			}

			return date('F jS, Y', $pd['date']);
		}

		private function _payment_method()
		{
			$method = $this->billing_get_payment_method();
			if ($method == 'credit') {
				$cc_info = $this->billing_get_credit_card_information();

				return $cc_info ? strtoupper($cc_info['cc_type']) . ' &mdash; ' . $cc_info['cc_number'] :
					'Credit Card (Removed)';
			} else if ($method == 'paypal') {
				return 'PayPal';
			} else if ($method == 'check') {
				return 'Check';
			}

			return 'UNKNOWN';
		}

		private function getMail()
		{
			$items = array(
				'POP3/IMAP/SMTP Server' => $this->common_get_service_value('mail',
					'mailserver'),
				'Spam Filtering'        => 'enabled'
			);
			if (!$this->email_configured()) {
				return $items + ['Provider' => $this->email_get_provider()];
			}
			$items += [
				$this->_edit_inline('Email Addresses', $this->linkIfEnabled('mailboxroutes'))
														 => array(
					'User'     => sprintf("%u/%s", count($this->email_list_mailboxes()),
						$this->common_get_service_value('user', 'max', '&#8734;')),
					'Forwards' => sprintf("%u/%s", count($this->email_list_aliases()),
						MAIL_DISABLED_FORWARDING ? 'N/A' : '&#8734;'),
					'Disabled' => sprintf("%u ",
						sizeof($this->email_list_mailboxes(\Email_Module::MAILBOX_DISABLED)))
				),
				$this->_edit_inline('Domains Accepting Email',
					$this->linkIfEnabled('mailertable')) => array($this->_sort($this->email_list_virtual_transports()))

			];

			return $items;
		}

		private function _sort($arr, $method = 'domain')
		{
			if ($method == 'domain') {
				\Util_Conf::sort_domains($arr);
			}

			return $arr;
		}

		private function _sql_info($lang)
		{
			if ($lang != 'pgsql' && $lang != 'mysql') {
				warn($lang . ": invalid language");
			}
			if (!$this->sql_enabled($lang)) {
				return array('disabled' => '');
			}

			$fn = function ($method, $args = []) {
				return \Util_Conf::call($method, $args);
			};

			return array(
				'Database Count' => sprintf("%u/%s", count($fn('sql_list_' . $lang . '_databases')),
					$this->common_get_service_value($lang, 'dbasenum', '&#8734;')),
				'User Count'     => sprintf("%u/%s", count($fn('sql_list_' . $lang . '_users')),
					$this->common_get_service_value($lang, 'dbasenum', '&#8734;')),
				'Database List'  => $this->_list_databases($lang, $fn)
			);

		}

		/**
		 *     SQL
		 */
		private function _list_databases($lang, &$fn)
		{
			$db_list = array();
			foreach ($fn('sql_list_' . $lang . '_databases') as $db) {
				$backup = $fn('sql_get_' . $lang . '_backup_config', $db);
				$db = '<strong>' . $db . '</strong>';
				if (!$backup) {
					$db_list[$db] = array('Backup Disabled');
					continue;
				}
				$db_list[$db] = array(
					'Next Backup' => date('Y-m-d', $backup['next']),
					'Frequency'   => $backup['span'] . ' day' . ($backup['span'] > 1 ? 's' : '')
				);
			}

			return $db_list;
		}

		private function getWeb()
		{
			$opts = array(
				$this->_edit_inline('SSL', $this->linkIfEnabled('ssl'))               => (
				$this->ssl_permitted() ? 'enabled and ' .
					($this->ssl_cert_exists() ? 'active' : 'inactive') : 'disabled'),
				$this->_edit_inline('Subdomains', $this->linkIfEnabled('subdomains')) => $this->_site_subdomains(),
			);

			if (DAV_APACHE) {
				$opts[$this->_edit_inline('WebDAV', $this->linkIfEnabled('webdav'))] = $this->_webdav_locations();
			}

			$opts['PHP Model'] = $this->php_jailed() ? 'PHP-FPM' : 'ISAPI';
			$opts['Web User'] = $this->common_get_service_value('apache', 'webuser', \Web_Module::WEB_USERNAME);

			$opts['HTTP Base Path'] = $this->domain_fs_path();

			return $opts;
		}

		private function _site_subdomains()
		{
			$subMax = $this->common_get_service_value('apache', 'subnum', '&#8734;');

			return array(
				'Local'  => sizeof($this->web_list_subdomains('local')) . '/' . $subMax,
				'Global' => sizeof($this->web_list_subdomains('global')) . '/' . $subMax
			);
		}

		private function _webdav_locations()
		{
			$locations = array();
			foreach ($this->web_list_dav_locations() as $location) {
				$locations[] = $location['path'];
			}

			return $locations;
		}

		private function _development_tools()
		{

			$params = array(
				$this->_edit_inline('Hosting Platform',
					MISC_KB_BASE . '/platform/determining-platform-version/') => sprintf('v%.2f', platform_version()),
				'Terminal Access'                                             => $this->common_get_service_value('ssh',
					'enabled') ? 'enabled' : 'disabled'
			);
			if (version_compare(platform_version(), 5, '>=')) {
				$cgroups = $this->cgroup_get_controllers();
				foreach ($cgroups as $c) {
					$cgroups[$c] = $this->cgroup_get_usage($c);
				}
				$params['Processes'] = count($cgroups['cpu']['procs']) . '/' . $cgroups['cpu']['maxprocs'];
				if ($this->common_get_service_value('cgroup', 'enabled')) {
					$params['CPU Wall (24 hour)'] = sprintf('%.2fs used<br />%.2fs available',
						$cgroups['cpu']['used'],
						$cgroups['cpu']['free']
					);
					$params['CPU Usage (24 hour)'] = sprintf('%.2fs user<br />%.2fs system',
						$cgroups['cpu']['cumuser'],
						$cgroups['cpu']['cumsystem']
					);
				} else {
					$params['CPU Usage'] = sprintf('%.2fs user<br />%.2fs system',
						$cgroups['cpu']['user'],
						$cgroups['cpu']['system']
					);
				}
				$params['Memory Usage'] = sprintf('%.2f MB/%d MB<br />Peak: %.2f MB',
					$cgroups['memory']['used'] / 1024 / 1024,
					$cgroups['memory']['limit'] / 1024 / 1024,
					$cgroups['memory']['peak'] / 1024 / 1024
				);
			}
			if (!$this->common_get_service_value('ssh', 'enabled')) {
				return $params;
			}
			$ports = $this->ssh_port_range();
			if ($ports) {
				if (!\is_array($ports[0])) {
					$ports = '[' . implode(',', $ports) . ']';
				} else {
					$ptmp = array();
					foreach ($ports as $p) {
						[$min, $max] = $p;
						$ptmp[] = '[' . $min . ',' . $max . ']';
					}
					$ports = implode(", ", $ptmp);
				}
				$params['TCP Port Range'] = $ports;
			}
			$params['Version Control'] = 'enabled';
			$params[$this->_edit_inline('Crontab',
				$this->linkIfEnabled('crontab'))] = ($this->crontab_enabled() ? 'enabled' : 'disabled');

			return $params;
		}

		private function _init_info_user()
		{
			$this->info = array(
				'General Information' => array(
					'Disk Usage' => $this->_disk_quota(),
					'Username'   => $this->getAuthContext()->username,
					$this->_edit_inline('Full Name', '/apps/changeinfo')
								 => $this->_full_name(),
					'Admin Emil' => $this->site_get_admin_email()
				),
				'Email'               => array(
					$this->_edit_inline('Vacation Mode', $this->linkIfEnabled('vacation'))
					=> $this->email_vacation_exists() ? 'enabled' : 'disabled'
				)
			);


		}

		private function _full_name()
		{
			$pwd = $this->user_getpwnam($this->getAuthContext()->username);

			return $pwd['gecos'];
		}

		public function htmlize_el($el)
		{
			static $i = 0;

			if (!$el) {
				while ($this->plv_html >= 0) {
					print '</div>';
					$this->plv_html--;
				}

				return false;
			}

			$level = $el['level'];
			$title = $el['title'];
			$data = $el['data'];

			if ($level < $this->plv_html) {
				print str_repeat('</div>', ($this->plv_html - $level) + 1);
			} else if ($level === $this->plv_html) {
				echo '</div>';
				if ($i % 4 === 0) {
					$i = 0;
					echo '<div class="clearfix hidden-xs-down"></div>';

				}
			}
			if ($level === 0) {
				$this->group = str_replace(' ', '_', strtolower($title));
				print '<h3 class="col-12" id="' .
					$this->group . '">' . $title . '</h3><div class="row mb-3 clearfix">';
			} else if ($level === 1) {
				$i++;
				print '<div class="mb-3 col-sm-6 col-md-4 float-md-left ' . $this->group . '"><h4 class="mb-0">' . $title . '</h4>' . $data;
			} else if ($level > 1) {
				print '<div class="lvl-' . $level . '">';

				if (!is_int($title)) {
					print $title . ': ';
				}
				if ($data !== null) {
					print '<span>' . $data . '</span>';
				}
			}

			$this->plv_html = $level;

		}

		public function htmlize_el1($el)
		{
			if (!$el) {
				while ($this->plv_html >= 0) {
					print '</div>';
					$this->plv_html--;
				}

				return false;
			}

			$level = $el['level'];
			$dtitle = $el['title'];
			$dnode = $el['data'];
			$html = '';
			$css = 'cat-' . $level;
			if ($level > $this->plv_html) {
				$html .= '<div class="container root-' . $level . '">';

			} else {
				$html .= '</div>';
				if ($level < $this->plv_html) {
					$lvlptr = $this->plv_html;
					while ($level < $lvlptr) {
						$html .= '</div>';
						$lvlptr--;
					}
					$level = $lvlptr;
				}

			}
			if (!is_int($dtitle) && $level == 0) { // category
				$html .= '<div class="col-md-6 ' . $css . '" id="' . str_replace(' ', '_', strtolower($dtitle)) . '">' .
					'<h3>' . $dtitle . '</h3>';
			} else if (!is_int($dtitle)) {         // category item
				$html .= '<div class="' . $css . ($dnode && $level > 1 ? ' inline' : '') . '">';

				$css = $dnode ? $css . " col2" : $css;

				if ($dnode && $level > 1) {       // key => value
					$html .= '<ul class="cat-' . $level . ' inline">' .
						'<li class="col2 cat-' . $level . '">' . $dtitle . ':</li>' .
						'<li class="col2 node">' . $dnode . '</li>' .
						'</ul>';
				} else {                          // item
					$html .= $dtitle ? '<span class="title">' . $dtitle . '</span>' : $node;
					if (!is_int($dtitle) && $dnode) { //
						$html .= '<ul class="cat-' . $level . '">' .
							'<li class="node">' . $dnode . '</li>' .
							'</ul>';
					}
				}
			} else {
				$html .= '<li class="node">' . $dnode;
			}

			$this->plv_html = $level;

			return $html;
		}

		/**
		 * Collapse an infinitely nested array into a
		 * single dimension
		 */
		public function enum_info()
		{
			$i_info = &$this->i_info;
			if (is_null($i_info) || $this->lvparser < 0) {
				return array();

			}

			$key = key($i_info);
			$val = current($i_info);
			$level = $this->lvparser;

			next($i_info);

			if ($val === false) {                 // end of current row
				if (!$this->p_info) {
					return false;
				}

				$this->lvparser--;                // drop a level
				$cur = array_pop($this->p_info);
				$level = $this->lvparser;
				$this->i_info = &$cur['data'];    // *BEEP* *BEEP*

				return $this->enum_info();

			} else if (is_array($val)) {          // descend another level b
				$this->lvparser++;

				$this->p_info[] = array(
					'title' => key($i_info),
					'data'  => &$i_info
				);
				unset($this->i_info);
				$this->i_info = &$val;
			}

			if (is_null($key)) {
				return $this->enum_info();
			}

			$r = array(
				'level' => $level,
				'title' => $key,
				'data'  => is_array($val) ? null : $val
			);

			return $r;
		}

	}
