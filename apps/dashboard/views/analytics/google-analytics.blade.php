@if (!$Page->getAnalyticsUser())
	<p class="ga-enable-note text-center small">
		<span class="badge badge-info">TIP</span>
		<a href="{{ MISC_KB_BASE }}/control-panel/linking-google-analytics/">Enable Google Analytics</a> for live
		website
		traffic reports
	</p>
@else
	<div id="google-analytics" class="row clearfix hide google-analytics-container">
		<div id="ga-origin-mismatch">
			<p class="alert alert-info">
				Before viewing statistics, add
				<b>{{ 'http', (\Util_HTTP::isSecure() ? 's' : ''), '://', \Util_HTTP::getHost() }}</b>
				as a <b>Authorized JavaScript origin</b> in Google
				<a href="https://code.google.com/apis/console">API Console</a> &gt; Credentials. <br/><br/>
				See KB: <a class="ui-action ui-action-label ui-action-kb"
				           href="{{  MISC_KB_BASE }}/control-panel/linking-google-analytics">Linking Google Analytics
					to {{ PANEL_BRAND }}</a>.
			</p>
		</div>
		<div id="view-selector"></div>
		<div id="active-users-container">
			<div class="ActiveUsers">Online: <b class="ActiveUsers-value">0</b></div>
		</div>
		<div id="timeline">
		</div>
		<span id="ga-authorize"></span>
		<div class="ga-modes mb-3 col-12">
			<a href="#" class="mode btn active p-3 mr-3" data-attr="ga:sessions">Sessions</a>
			<a href="#" class="mode btn p-3 mr-3" data-attr="ga:avgSessionDuration">Durations</a>
			<a href="#" class="mode btn p-3 mr-3" data-attr="ga:pageviews,ga:sessions">Page Views</a>
			<a href="#" class="mode btn p-3 mr-3" data-attr="ga:newUsers,ga:organicSearches">Organic SEO</a>
		</div>
	</div>
@endif
