<div id="container-statistics" class="">
	<div class="row">
		<div class="col-6 col-sm-4 col-md-3 col-lg-2">
			<h2 class="title mt-0 stats">
				Analytics
			</h2>
		</div>
		@include('partials.quick-links')
	</div>

	@include('analytics.google-analytics')

	<div class="row usage-gauges justify-content-around">
		@include('partials.gauge', [
			'title' => 'Storage',
			'label' => "%d MB/%d MB",
			'data' => [
				UCard::get()->getStorage()['used'] / 1024,
				UCard::get()->getStorage()['total'] / 1024
			],
			'id' => 'storage-gauge']
		)

		@includeWhen(UCard::is('site') && \Util_Conf::call('bandwidth_enabled'), 'partials.gauge', [
			'title' => 'Bandwidth',
			'label' => "%d GB/%d GB<br />Rollover: %s",
			'data' => [
				\UCard::get()->getBandwidth()['used'],
				\UCard::get()->getBandwidth()['total'],
				\UCard::get()->getBandwidth()['rollover']
			],
			'id'    => 'bandwidth-gauge'
		])

		@includeWhen(UCard::is('site') && \cmd('cgroup_enabled'), 'partials.gauge', [
			'title' => 'CPU',
			'label' => '%.2fs system<br/>%.2fs user',
			'data' => [
				\UCard::get()->getCgroups()['cpu']['system'],
				\UCard::get()->getCgroups()['cpu']['user']
			],
			'id'    => 'cpu-gauge'
		])

		@php
			$memlimit = $mempeak = $memused = 0;
			if (UCard::is('site') && \cmd('cgroup_enabled')) {
				$memused = \UCard::get()->getCgroups()['memory']['used'] / 1024 / 1024;
				$mempeak = \UCard::get()->getCgroups()['memory']['peak'] / 1024 / 1024;
				$memlimit = \UCard::get()->getCgroups()['memory']['limit'] / 1024 / 1024;
			} else {
				$memstats = \Opcenter\System\Memory::stats();
				$memlimit = $memstats['memtotal']/1024;
				$mempeak = ($memstats['memtotal'] - $memstats['memfree'])/1024;
				$memused = ($memstats['memtotal'] - $memstats['memavailable'])/1024;
			}
		@endphp

		@include('partials.gauge', [
			'title' => 'Memory',
			'label' => "%d MB/%d MB<br />%d MB peak",
			'data' => [
				$memused, $memlimit, $mempeak
			],
			'id'    => 'memory-gauge'
		])

		@includeWhen(UCard::is('site'), 'partials.gauge', [
			'title' => 'Processes',
			'label' => "%d processes/%d max",
			'data' => [
				count(\UCard::get()->getCgroups()['cpu']['procs']),
				\UCard::get()->getCgroups()['cpu']['maxprocs']
			],
			'id'    => 'processes-gauge'
		])

		@include('partials.gauge', [
			'title' => 'Load Average',
			'label' => "%.2f/%d",
			'data'  => [
				sys_getloadavg()[0],
				NPROC
			],
			'id'    => 'load-average-gauge'
		])
		@if (UCard::is('admin'))
			<div id="rampart"></div>
		@endif
	</div>
</div>