<p class="alert alert-info">
	<i class="fa fa-info-circle"></i>
	Your {{ PANEL_BRAND }} trial license expires in {{ \Opcenter\License::get()->daysUntilExpire() }}
	days.
	Purchase a license from <a href="https://my.apiscp.com/register" target="_blank">my.apiscp.com</a> before then.
</p>
<hr/>