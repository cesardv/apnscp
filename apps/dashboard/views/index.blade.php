@extends('theme::layout')

@section('content')
	@includeWhen(UCard::get()->hasPrivilege('admin') && \Opcenter\License::get()->isTrial(), 'partials.demo-expiration')
	@include('analytics-container')
	@includeWhen(
		UCard::get()->hasPrivilege('site') && SCREENSHOTS_ENABLED && \cmd('common_get_service_value', 'apache', 'enabled'),
		'glances.webapps'
	)

	@includeWhen(!\Ucard::is('admin'), 'processes')
	@if (UCard::is('admin'))
		<div id="argos">
			@include('glances.argos')
		</div>
	@endif
	@includeWhen(Template_Engine::init()->get_info(), 'overview')
	@include('partials.modals.overage')
    @include('partials.modals.blacklist')
@endsection