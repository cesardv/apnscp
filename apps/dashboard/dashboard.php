<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\dashboard;

	class Page extends \Page_Container
	{
		use \ImpersonableTrait;

		protected $favoritesSkipList = ['troubleticket'];
		private $_pageviews;

		public function __construct()
		{
			parent::__construct();

			$this->setTitle('Dashboard');
			\Page_Renderer::hide_breadcrumbs();
			\Page_Renderer::hide_overview();
			if (!$this->errors_exist()) {
				$this->hide_pb();
			}

			$scripts = array('flot', 'flot.pie');

			call_user_func_array(array($this, 'init_js'), $scripts);
			$this->add_javascript('dashboard.js');
			$this->add_css('dashboard.css');
			$this->checkRampart();
			$this->add_javascript('var F2B_PREFIX="' . \Rampart_Module::FAIL2BAN_IPT_PREFIX . '";', 'internal', false, true);
			$this->add_javascript('var GAUGE_HELPERS = ' . json_encode(['nproc' => NPROC]) . ';', 'internal', false, false);

			$id = $this->getAnalyticsUser();

			if (is_array($id)) {
				$this->loadAnalytics($id['user'], $id['secret']);
			} else {
				$this->loadAnalytics($id);
			}

			if ($this->auth_is_inactive()) {
				$hash = $this->billing_get_renewal_hash();
				$reason = $this->auth_inactive_reason() ?:
					\ArgumentFormatter::format("Account is inactive");
				$this->add_javascript('warnInactive(' . json_encode($hash) . ", " . json_encode($reason) . ');', 'internal', true);
			}
			$this->_check_storage();
		}

		public function index() {
			if ($id = request()->get('revertid')) {
				// backwards compatibility
				$this->revertHijack($id);
			} else if ($user = request()->get('hijack')) {
				// backwards compatibility
				$this->hijack($user);
			}
			return view('index');
		}

		private function checkRampart()
		{
			if (\UCard::is('user')) {
				return false;
			}
			$str = 'var hasRampart=' . ($this->rampart_enabled() ? 'true' : 'false') . ';';
			$this->add_javascript($str, 'internal', false, true);
		}

		public function getAnalyticsUser()
		{
			return \Util_Conf::get_analytics_api_key();
		}

		private function loadAnalytics($id, $secret = null)
		{
			$args = $preamble = '';
			if ($id) {
				$preamble = '(function(w,d,s,g,js,fs){
                g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
                js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
                js.src="https://apis.google.com/js/platform.js";
                fs.parentNode.insertBefore(js,fs);js.onload=function(){g && typeof(g.load) == "function" && g.load("analytics");};
                }(window,document,"script"));';
				$args = '"' . $id . '"';
				if ($secret) {
					$args .= ', ' . '"' . $secret . '"';
				}
			}

			$this->add_javascript($preamble . 'var analytics = new Analytics();  analytics.gauges(); if (typeof(gapi) != "undefined") { gapi.analytics.ready(function() { ' .
				'analytics.run(' . $args . ');});}', 'internal', false);
		}

		private function _check_storage()
		{
			if (isset($_SESSION['storage-warning-nag'])) {
				return true;
			}

			$storage = \UCard::get()->getStorage();

			// don't show nag if quotaless
			if ($storage['total'] < 1) {
				return false;
				// only nag if storage within 98.5%
			} else if ($storage['free'] / $storage['total'] > 0.015) {
				return false;
			}

			if (\UCard::get()->hasPrivilege('site')) {
				$role = 'site';
			} else {
				$role = 'user';
			}

			$this->add_javascript('alertStorage("' . $role . '");', 'internal', false);

			return true;
		}

		public static function formatTime($time)
		{
			$e = $_SERVER['REQUEST_TIME'] - $time;
			if ($e < 1) {
				return '<span class="label label-success">NEW</span>';
			}
			if ($e < 60) {
				return '' . str_pad($e, 2, '0', STR_PAD_LEFT) . ' s';
			} else if ($e < 3600) {
				return sprintf('%2d min %02d sec', floor($e / 60),
					$time % 60
				);
			} else if ($e < 86400) {
				$hour = floor($e / 3600);

				$min = ($e - ($hour * 3600)) / 60;

				return sprintf('%2d h %02d min ago', $hour, $min);
			} else if ($e < 86400 * 30) {
				$days = floor($e / 86400);
				$sec = $e - $days * 86400;
				$hour = floor($sec / 3600);
				$min = floor(($sec - $hour * 3600) / 60);
				$sec = ($sec - $hour * 3600) % 60;

				return sprintf('%2d d %2d h',
					$days,
					$hour
				);
			} else {
				return date('Mj', $time);
			}
		}

		public function favorites($max)
		{
			$view = (array)\Preferences::get('pageview');
			arsort($view, SORT_NUMERIC);
			$slice = [];
			$i = 0;
			foreach ($view as $k => $v) {
				$i++;
				if ($i > $max) {
					break;
				}
				if ($k === $this->getApplicationID() || \in_array($k, $this->favoritesSkipList, true) ||
					!\Template_Engine::init()->getApplicationFromId($k)) {
					$i--;
					continue;
				}
				if (in_array($k, $slice, true)) {
					// aliased app name
					$i--;
					continue;
				}
				$slice[] = $k;
			}

			return $slice;
		}

		public function lastLogin()
		{
			return $this->getCard()->lastAccess();
		}

		public function getProcesses()
		{
			return $this->pman_get_processes();
		}

		public function hijack(string $user)
		{
			if (!$this->site_hijack($user)) {
				return false;
			}
			\Auth::get_driver()->login_success();
			exit();
		}

		public function revertHijack($id) {
			if ($id === \session_id()) {
				return;
			}
			\Auth_Anvil::anvil();
			if (!\apnscpSession::init()->exists($id)) {
				\Page_Renderer::display_postback_notifier();

				return error('Failed to resume previous session - session expired');
			}
			assert($this->getAuthContext()->getImpersonator() === $id, 'Impersonator matches');
			// no going back!
			$this->restoreImpersonator($this->getAuthContext());
			\Auth_Anvil::remove();
			$seen = false;
			foreach (headers_list() as $header) {
				if (0 === strpos('Location:', $header)) {
					$seen = true;
					break;
				}
			}
			if (!$seen) {
				header('Location: /apps/dashboard', true, 302);
			}
			exit;
		}

		public function getUserFromUid($uid)
		{
			$user = $this->user_get_username_from_uid($uid);

			return $user ? $user : '#' . $uid;
		}
	}
