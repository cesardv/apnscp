var colors, PRIMARY_CHART_COLOR, RAMPART_ADDRS = [], argosTimer = null;
$(document).ready(function () {
	colors = apnscp.themeColors();
	PRIMARY_CHART_COLOR = colors.primary;
	Pman();
	if (session.role !== 'admin') {
		return;
	}

	populateRampart();
	prefetchNexus();
	bindArgosActions();
});

function prefetchNexus() {
	// preload all data to speed up initial interaction
	$.get('/apps/nexus');
}

function populateRampart() {
	return getIps().done(function () {
		$('#ipAddress').autocomplete({
			minLength: RAMPART_ADDRS.length > 4000 ? 4 : RAMPART_ADDRS.length > 1000 ? 3 : 2,
			source: function (request, response) {
				response(RAMPART_ADDRS.filter(function (item) {
					return -1 !== item.ip.indexOf(request.term);
				}));
			},
			select: function (event, ui) {
				$('#ipAddress').val(ui.item.ip);
				return false;
			},
			classes: {
				'ui-autocomplete': 'dropdown-menu'
			}
		}).autocomplete("instance")._renderItem = function (ul, item) {
			return $("<li class='py-0'></li>")
				.data("item.autocomplete", item)
				.append("<span class='dropdown-item py-0 d-flex'>" + item.ip + "<span class='align-self-end my-auto ml-auto font-weight-normal text-uppercase badge badge-default'>" + item.jail + "</span></span>")
				.appendTo(ul);
		};
	});
}

function bindArgosActions() {
	$('#argos').on('click', function (e) {
		e.preventDefault();

		if (e.target.id === 'argosLink') {
			var savedClass = $(e.target).classList;
			apnscp.cmd('argos_test', [], {indicator: $('i', e.target)}).done(function () {
				$(e.target).classList = savedClass;
				apnscp.addMessage('Argos test successful');

			});

			return;
		}

		if ("restart" in e.target.dataset) {
			var savedClass = $(e.target).classList, name = e.target.dataset.restart;
			apnscp.cmd('argos_restart', [name], {indicator: $(e.target)}).done(function () {
				apnscp.addMessage(name + " service restarted");
			}).always(function () {
				$(e.target).classList = savedClass;
			});
		} else if ("monitor" in e.target.dataset) {
			var savedClass = $(e.target).classList, name = e.target.dataset.monitor;
			apnscp.cmd('argos_monitor', [name], {indicator: $(e.target)}).done(function () {
				apnscp.addMessage(name + " service monitored");
				argosRefresh();
			}).always(function () {
				$(e.target).classList = savedClass;
			});
		}
	});
	argosRefresh();
}

function argosRefresh() {
	clearTimeout(argosTimer);
	apnscp.render([], 'argos').done(function (ret) {
		$('#argos').html(ret);
		argosTimer = setTimeout(argosRefresh, 10000);
	});
}

function getIps() {
	return $.ajax(apnscp.call_app_ajax(null, null, [], {
		url: '/apps/' + session.appId + '/rampart/all',
		method: 'GET',
		dataType: 'json',
	})).then(function (ret) {
		var stats = {};
		Object.keys(ret.return).forEach(function(jail) {
			if (0 !== jail.indexOf(F2B_PREFIX)) {
				return;
			}
			var label = jail.substr(F2B_PREFIX.length);
			stats[label] = 0;

			Object.values(ret.return[jail]).forEach(function (entry) {
				stats[label]++;
				RAMPART_ADDRS.push({ip: entry.host, jail: label});
			});
		});
		var chunker = function (num) {
			return ('000' + num).slice(-3);
		};
		RAMPART_ADDRS.sort(function (a, b) {
			var num1 = Number(a.ip.split(".").map(chunker).join(''));
			var num2 = Number(b.ip.split(".").map(chunker).join(''));
			return num1 - num2;
		});

		return apnscp.render(
			{entries: stats},
			'/apps/' + session.appId + '/rampart/render'
		).done(function (html) {
			$('#rampart').replaceWith($(html));
			$('#rampart').find(':input.master-select').on('change', function () {
				// @TODO bug in 4.0.0
				var $that = $(this), $target = $($(this).data('target'));
				if ($that.is(':checked')) {
					$target.collapse('show').on('shown.bs.collapse', function () {
						$that.prop('checked', true);
					});
				} else {
					$target.collapse('hide');
				}
			});
			$('#refreshRampart').one('click', function () {

				$('#rampart > table').fadeOut(function () {
					populateRampart();
				});
			});

			$('.flush-form').submit(function (e) {
				e.preventDefault();
				var $form = $(this), serialized = $form.serializeArray();
				apnscp.post(serialized, $form.attr('action')).done(function () {
					$form.closest(".data-row").children('.active').fadeOut(function () {
						$(this).text("0").fadeIn('fast');
					});
					apnscp.addMessage($form.closest('.data-row').data('jail') + " flushed");
				})
			});
			$('#rampartIpForm').submit(function (e) {
				e.preventDefault();
				var IP = $('#ipAddress').val(), mode = $('#ipAddress').data('mode');
				apnscp.post({ip: IP}, $(this).attr('action') + '/' + mode).done(function () {
					for (var idx in RAMPART_ADDRS) {
						if (RAMPART_ADDRS[idx].ip === IP) {
							var $row = $('#rampart [data-jail="' + RAMPART_ADDRS[idx].jail + '"]');
							delete RAMPART_ADDRS[idx];
							$row.children('.active').fadeOut('fast', function (e) {
								$(this).text(Math.max(0, parseInt($(this).text())-1)).fadeIn('fast');
							})
						}
					}
					apnscp.addMessage(IP + " has been " + mode + 'ed');
					$('#ipAddress').val("").focus();
				});
			});
		});
	});
}

$(window).on('load', function () {
	var lastPopover = null;
	$(".ui-tooltip").click(function () {
		if (lastPopover && lastPopover.get(0) != this) {
			lastPopover.popover('dispose');
		}
		var that = this;
		var tip, href, appId = session.appId;
		if ($(this).attr('rel')) tip = $(this).attr('rel');
		else tip = $(this).attr('id');
		if (!this.href) href = '/ajax?engine=tip&tip=' + tip + '&app=' + appId;
		else href = this.href;

		$.getJSON(href, {app: appId}).then(function (data) {
			$(that).popover({
				content: data.body,
				title: data.title,
				html: true,
				placement: 'top'
			});
			lastPopover = $(that);
			$(that).popover('show');
			$('body').one('click', function() {
				if (lastPopover) {
					lastPopover.popover('dispose');
				}
			});
			return false;
		});
		return false;
	});
	$('[data-toggle=tooltip]').tooltip();

	$('.kill').click(function () {
		var pid = $(this).val(), that = $(this);
		that.find('.ui-ajax').remove().append($('<span class="ui-ajax-loading-small ui-ajax ui-ajax-loading">'));
		apnscp.cmd('pman_kill', [pid]).done(function () {
			var parent = that.closest('.process');
			parent.fadeOut('fast', function (e) {
				parent.remove();
				// @todo recalc usage
			});
		}).fail(function (resp) {
			that.find('.ui-ajax-loading').removeClass('ui-ajax-loading').addClass('ui-ajax-error');
		});

	});

	$('#overview-expand').click(function () {
		if ($('#overview-expand a').hasClass('ui-expanded')) {
			$(this).nextAll('div').slideUp('medium');
			$('#overview-expand a').text("show more").addClass('ui-collapsed').removeClass('ui-expanded');
		} else {
			$(this).nextAll('div').hide().removeClass('hide').slideDown('fast');
			$('#overview-expand a').text("show less").addClass('ui-expanded').removeClass('ui-collapsed');

		}
		return false;
	});

	var cmds = [];
	if (session.role === 'admin' || (session.role === 'site' && hasRampart)) {
		cmds.push(apnscp.cmd('rampart_banned_services').done(function (status) {
			if (!status['success'] || !status['return'].length) {
				return true;
			}
			var services = status['return'].map(function(e) { return e.substr(F2B_PREFIX.length); }).join(", ");
			var modal = apnscp.modal(null, {dialog: $('#blacklistModal'), queue: true}).on('show.bs.modal', function() {
				var that = $(this);
				that.find('.services').text(services);
				apnscp.cmd('rampart_get_reason', []).done(function (ret) {
					if (ret.return) {
						that.find('.reason').show().end().find('.log').text(ret.return);
					} else {
						that.find('.reason').hide();
					}
				})
				that.on('submit', function () {
					apnscp.cmd('rampart_unban').done(function () {
						modal.modal('hide');
					});
					return false;
				});
			});
			modal.modal('show');
		}));
	}

	if (session.role === "user" || session.role === "site") {
		// ensure this loads last
		cmds.push(apnscp.cmd('email_vacation_exists').done(function (status) {
			if (!status['return']) {
				return true;
			}
			var modal = apnscp.modal([
				$('<p><b>Howdy!</b> Looks like you are back from vacation.</p>'), $('<button type="button">').addClass('btn btn-primary').text("Remove Vacation Responder").click(function () {
					apnscp.cmd('email_remove_vacation').done(function () {
						modal.modal('hide');
					});
				})
			], {title: "Vacation Responder", queue: true});
			modal.modal('show');
		}));
	}
	setTimeout(function () {
		$.ajaxQueue(cmds);
	}, 500);


});

function warnInactive(hash, msg) {
	apnscp.cmd('billing_get_renewal_link', [hash]).then(function (data) {
		$('#modal').addClass('no-close').on('show.bs.modal', function () {
			if (data['return']) {
				msg += ' Please remit payment through the <a href="' + data['return'] + '">billing portal</a> to resume service.';
			}
			$(this).find('.modal-body').empty().append(
				$('<p class="alert alert-danger text-center">' + msg + '</p>')
			)
		}).modal({keyboard: false});
	})
}


function alertStorage(role) {
	var $cluster = $('#ui-storage-cluster'),
		$clusterClone = $cluster.clone().addClass('float-none my-3'),
		explanation = null;
	if (role == "site") {
		var $indicator = $('#amnesty-indicator');
		$('#amnesty').click(function () {
			$('#amnesty-status-msg').remove();
			apnscp.cmd('site_storage_amnesty', [], {indicator: $indicator}).done(function (e) {
				var $frag = $('<p id="amnesty-status-msg" class="alert alert-success mb-0"/>').html("<i class='fa fa-check'></i> Storage amnesty granted. Log out, then log in to see changes.");
				$indicator.closest('.modal-body').replaceWith($frag);
			}).fail(function (e, msg) {
				var json = $.parseJSON(e.responseText);
				if (json.errors) {
					var $frag = $('<p id="amnesty-status-msg" class="col-xs-12 alert alert-danger mx-3 ui-ajax-error-msg ui-ajax-error-msg-inline"/>').text(json.errors[0]);
					$indicator.parent().parent().after($frag);
				}
				return true;
			});
		});
	}
	$clusterClone.css({
		width: ($cluster.outerWidth() + (
			apnscp.browser.type == "msie" ? 2 : 0
		)) + 'px',
		position: 'initial',
		overflow: 'visible',
		width: '200px'
	});

	$('#modal .modal-body').prepend([$('<div class="mx-auto text-center">').append($clusterClone), $("#overage").show()]);
	$('#modal').modal();//.modal().attr('id', 'storage-crisis');
}

var Analytics = function () {
	this._auth = {
		expiry: -1
	}, this._gaContainer = 'google-analytics',
		this._gaViewContainer = 'view-selector',
		this._gaAuthorizationContainer = 'ga-authorize',
		this._gaTimelineContainer = 'timeline',
		this.gaugeMap = {
			'Storage': function () {
				var cmd;
				if (session.role == 'site') {
					cmd = 'site_get_account_quota';
				} else if (session.role == 'user') {
					cmd = 'user_get_quota';
				} else if (session.role == 'admin') {
					cmd = 'admin_get_storage';
				} else {
					return;
				}
				return apnscp.cmd(cmd).then(
					function (data) {
						if (!data.success) {
							return {data: [0, 0]};
						}
						return {
							data: [
								parseInt((data['return'].qused) / 1024),
								!data['return'].qhard ? NaN : parseInt((data['return'].qhard - data['return'].qused) / 1024)
							], labels: [
								'Used',
								'Free'
							], unit: ' MB'
						};
					}
				);
			},
			'Bandwidth': function () {
				if (session.role != "site") {
					return null;
				}
				return apnscp.cmd('bandwidth_usage').then(
					function (data) {
						if (!data.success) {
							return [0, 0];
						}
						return {
							data: [
								data['return'].used / 1024 / 1024 / 1024,
								data['return'].total === -1 ? NaN : (data['return'].total - data['return'].used) / 1024 / 1024 / 1024
							], labels: [
								'Used',
								'Free'
							], unit: ' GB'
						};
					}
				)
			},
			'CPU': function () {
				return apnscp.cmd('cgroup_get_usage', ['cpu']).then(
					function (data) {
						if (!data.success) {
							return [0, 0];
						}
						return {
							data: [
								data['return'].used,
								parseInt(data['return'].limit - data['return'].used),
							], labels: [
								'Used',
								'Free',
							], unit: 's'
						};
					}
				)
			},
			'Memory': function () {
				return apnscp.cmd('cgroup_get_usage', ['memory']).then(
					function (data) {
						if (!data.success) {
							return [0, 0];
						}

						return {
							data: [
								data['return'].used / 1024 / 1024,
								(data['return'].peak - data['return'].used) / 1024 / 1024,
								(data['return'].limit - data['return'].used) / 1024 / 1024
							], labels: [
								'Used',
								'Peak',
								'Free'
							], unit: ' MB'
						};
					}
				)
			},
			'Processes': function () {
				return apnscp.cmd('cgroup_get_usage', ['cpu']).then(
					function (data) {
						if (!data.success) {
							return [0, 0];
						}
						return {
							data: [
								data['return'].procs.length,
								data['return'].maxprocs - data['return'].procs.length
							], labels: [
								'Active',
								'Available'
							]
						};
					}
				)
			},
			'Load Average': function () {
				return apnscp.cmd('common_get_load').then(
					function (data) {
						if (!data.success) {
							return [0, 0];
						}
						var load = parseFloat(data.return["1"]);
						return {
							data: [
								load,
								parseFloat(GAUGE_HELPERS.nproc) - load
							], labels: [
								'Current',
								'Max',
							], labelData: [
								load,
								parseFloat(GAUGE_HELPERS.nproc)
							]
						};
					}
				)
			}
		};
};

Analytics.prototype.gauges = function() {
	for (var gauges in $('#gauges')) {

	}
	for (var gauge in this.gaugeMap) {
		this.loadGauge(gauge, this.gaugeMap[gauge]);
	}
};

Analytics.prototype.run = function (id, secret) {
	if (id) {
		this.loadGoogleAnalytics(id, secret);
	}

};

Analytics.prototype.createActiveViewerWidget = function () {
	return gapi.analytics.createComponent(
		"ActiveUsers", {
			initialize: function () {
				this.activeUsers = 0
			}, execute: function () {
				this.polling_ && this.stop(),
					this.render_(),
					gapi.analytics.auth.isAuthorized() ?
						this.getActiveUsers_() : gapi.analytics.auth.once("success", this.getActiveUsers_.bind(this))
			}, stop: function () {
				clearTimeout(this.timeout_), this.polling_ = !1, this.emit("stop", {activeUsers: this.activeUsers})
			}, render_: function () {
				var e = this.get();
				this.container = "string" == typeof e.container ? document.getElementById(e.container) : e.container, this.container.innerHTML = e.template || this.template, this.container.querySelector("b").innerHTML = this.activeUsers
			}, getActiveUsers_: function () {
				var e = this.get(), t = 1e3 * (e.pollingInterval || 5);
				if (isNaN(t) || 5e3 > t) throw new Error("Frequency must be 5 seconds or more.");
				this.polling_ = !0, gapi.client.analytics.data.realtime.get({
					ids: e.ids,
					metrics: "rt:activeUsers"
				}).execute(function (e) {
					var i = e.totalResults ? +e.rows[0][0] : 0, s = this.activeUsers;
					this.emit("success", {
						activeUsers: this.activeUsers
					}), i != s && (this.activeUsers = i, this.onChange_(i - s)),
					(this.polling_ = !0) && (this.timeout_ = setTimeout(this.getActiveUsers_.bind(this), t))
				}.bind(this))
			}, onChange_: function (e) {
				var t = this.container.querySelector("b");
				t && (t.innerHTML = this.activeUsers), this.emit("change", {
					activeUsers: this.activeUsers,
					delta: e
				}), e > 0 ? this.emit("increase", {
					activeUsers: this.activeUsers,
					delta: e
				}) : this.emit("decrease", {activeUsers: this.activeUsers, delta: e})
			}, template: '<div class="ActiveUsers">Active Users: <b class="ActiveUsers-value"></b></div>'
		});
};

Analytics.prototype.createChart = function (query) {
	var query = $.extend({}, {
		'dimensions': 'ga:date',
		'metrics': 'ga:sessions',
		'start-date': '30daysAgo',
		'end-date': 'yesterday',
	}, query);

	return new gapi.analytics.googleCharts.DataChart({
		reportType: 'ga',
		query: query,
		chart: {
			type: 'LINE',
			container: this._gaTimelineContainer,
			options: {
				width: '100%',
				colors: [PRIMARY_CHART_COLOR, colors.secondary]
			},

		}

	});
};

Analytics.prototype.loadGoogleAnalytics = function (id, secret) {
	var self = this, removeAjax = function (status) {
		$("#" + self._gaContainer).hide().removeClass("hide").fadeIn('fast').addClass("analytics-" + status);
	};
	gapi.analytics.ready(function () {

		// Step 3: Authorize the user.

		var CLIENT_ID = id, CLIENT_SECRET = secret;
		if (!$('#ga-authorize').length) {
			return;
		}
		var authOptions = {
			container: self._gaAuthorizationContainer,
			clientid: CLIENT_ID,
			userInfoLabel: '',
		};
		/*if (CLIENT_SECRET) {
			authOptions.serverAuth = {
				access_token: CLIENT_SECRET
			};
			authOptions.immediate = true;
		}*/

		gapi.client.setApiKey(CLIENT_SECRET);

		gapi.analytics.auth.authorize(authOptions).on('error', function (e) {
			if (e.error.message == "immediate_failed") {
				removeAjax('failed');
				$('#ga-origin-mismatch').show();

			}
		}).on('needsAuthorization', function (e) {
			$('#' + self._gaContainer).removeClass("hide");
		}).on('signIn', function () {
			$('#' + self._gaContainer).hide().removeClass('hide').fadeIn('fast');
			$('#ga-authorize').hide();
		});

		self.createActiveViewerWidget();

		// Step 4: Create the view selector.

		self.gaViewSelector = new gapi.analytics.ViewSelector({
			container: self._gaViewContainer
		});

		// Step 5: Create the timeline chart.

		self.gaTimeline = analytics.createChart({metrics: 'ga:sessions'});

		self.activeUsers = new gapi.analytics.ext.ActiveUsers({
			container: 'active-users-container',
			pollingInterval: 5
		});

		self.activeUsers.once('success', function (data) {
			var element = this.container.firstChild;
			var timeout;

			this.on('change', function (data) {
				var element = this.container.firstChild;
				var animationClass = data.delta > 0 ? 'is-increasing' : 'is-decreasing';
				element.className += (' ' + animationClass);

				clearTimeout(timeout);
				timeout = setTimeout(function () {
					element.className =
						element.className.replace(/ is-(increasing|decreasing)/g, '');
				}, 3000);
			});
		});

		// Step 6: Hook up the components to work together.
		gapi.analytics.auth.on('success', function (response) {
			self._auth.expiry = response.expires_at || Date.now() + 3600;
			self.gaViewSelector.execute();
		});

		/** Remove AJAX loading */
		self.gaTimeline.once('success', function (data) {
			removeAjax('enabled');
		});

		var mainChartRowClickListener;

		self.gaViewSelector.on('change', function (ids) {
			var newIds = {
				query: {
					ids: ids
				}
			}
			self.activeUsers.set(newIds.query).execute();
			self.changeAnalyticsMode(newIds);
		});

		$('.ga-modes .mode').click(function (ev) {
			var type = $(this).addClass('active').attr('data-attr');
			$(this).siblings('.active').removeClass('active');
			self.gaTimeline.set({
				query: {
					metrics: type
				}
			}).execute();

		});
	});
};


Analytics.prototype.changeAnalyticsMode = function (params) {
	return this.gaTimeline.set(params || {}).execute();
};

Analytics.prototype.loadGauge = function (name, callback) {
	var id = name.toLowerCase().replace(/ /, '-') + '-gauge';
	if (!$('#' + id).length) {
		return;
	}
	callback.call().done(function (data) {
		new Gauge(name, id, data);
	});

};

var Gauge = function (name, id, data) {
	var obj = this.create(name, id, data),
		canvas = obj.getCanvas(), value, label,
		html = new Array('<ul class="label-pair">');
	if (!data.labels || !data.labels.length) {
		return;
	}
	for (var i in data.labels) {
		value = (data.labelData && data.labelData[i] || data.data[i]),
			label = data.labels[i];
		html.push("<li class='label'>" + label +
			'</li><li class="data">' + Math.round(value * 100) / 100 + (data.unit || '') + '</li>');
	}
	$(canvas).parent().parent().after($(html.join("") + '</ul>'));
};


Gauge.prototype.create = function (name, id, data) {
	var endColor = colors.inactive,
		colorIndex = [PRIMARY_CHART_COLOR, colors.secondary];
	if (data.length < 1) {
		return false;
	}
	var datapoints = [], n = 0;
	for (var point in data.data) {
		var color = colorIndex[n];
		datapoints.push(
			{
				label: point,
				data: Math.max(0, data.data[point]),
				color: color
			}
		);
		n++;
	}
	datapoints[datapoints.length - 1].color = endColor;
	return $.plot('#' + id + ' .gauge', datapoints, {
		hooks: {
			draw: [function (plot, canvas) {
				var sum = datapoints.reduce(function (a, b) {
						return a + b.data;
					}, 0),
					pct = datapoints[0].data / sum;
				$(canvas.canvas).parent().prepend(
					'<span class="graph-label" style="width: 100%; margin-top: calc(150px/2 - 4em); vertical-align: middle;">' +
					(isNaN(pct) ? 'N/A' : (Math.round(100 * Math.min(1, pct)) + '%')) + '</span>'
				);
				return true;
			}]
		},

		series: {
			pie: {
				innerRadius: 0.80,
				clickable: true,
				hoverable: true,
				show: true,
				radius: 1,
				combine: {
					threshold: 0.03
				},
				label: {
					show: false,
					radius: 3 / 4,
				},
				formatter: function (label, series) {
					return '';
				},
				combine: 0.02,
				stroke: {
					width: 0
				}
			}
		},
		width: '100%',
		legend: {
			show: false,
			margin: 0
		}
	});
};

var Process = function (proc) {
	this.name = proc['comm'];
	this.pid = proc['pid'];
	this.memory = proc['memory'];
	this.age = proc['startutime'];
	this.user = proc['user'];

	process = function (field) {
		return field;
	}
	return {
		getAll: function () {
			return [this.pid, this.name];
		},
		get: function (field) {
			return this.field;
		},
	}

	user: null
};

var Pman = function () {
	return;
	apnscp.call_app(session.appId, 'getProcesses').then(function (data) {
		if (!data) {
			return;
		}
		//console.log(data);
		Object.keys(data).map(function (key) {
			var proc = data[key];
			Pman.prototype.add(proc);
		});
	});
};

Pman.prototype.kill = function (pid) {
	apnscp.cmd('pman_kill', pid).pipe(function (resp) {
		console.log(resp);
	});
};

Pman.prototype.add = function (proc) {
	return;
	// @TODO
	var collection = [], newproc = new Process();
	newproc.name = proc['comm'];
	console.log(proc);
};



