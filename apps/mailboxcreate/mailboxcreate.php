<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\mailboxcreate;

	use Page_Container;
	use Symfony\Component\Yaml\Yaml;

	class Page extends \apps\mailboxroutes\Page
	{
		protected function loadBlade(string $template = 'index', string $path = null): \BladeLite
		{
			$path = webapp_path('mailboxroutes/views');

			return parent::loadBlade($template, $path);
		}

		public function getMode()
		{
			return 'add';
		}
	}
