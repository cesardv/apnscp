@php
	// @var PoolStatus $pool
	$pool = $Page->activePool();
@endphp

<div class="mb-3">
	<form method="post">
		<input type="hidden" name="php-pool" id="poolName" value="{{ $Page->getName() }}" />
		<h4>{{ $Page->getName() }}</h4>
		<div class="btn-group" role="group" aria-label="Pool actions">
			<div class="btn-group" role="group">
				<button id="btnGroupState" type="button" class="btn btn-secondary dropdown-toggle"
				        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				</button>
				<div class="dropdown-menu" aria-labelledby="btnGroupState">
					<button type="submit"
					        class="dropdown-item ui-action btn-block ui-action-restore ui-action-label btn-block warn"
					        name="restart" value="{{ $Page->getName() }}">
						Restart
					</button>
					@if ($pool->running())
						<button type="submit"
						        class="dropdown-item ui-action btn-block ui-action-disable ui-action-label btn-block warn"
						        name="stop" value="{{ $Page->getName() }}">
							Stop
						</button>
					@else
						<button type="submit"
						        class="dropdown-item ui-action btn-block ui-action-enable ui-action-label btn-block"
						        name="start" value="{{ $Page->getName() }}">
							Start
						</button>
					@endif
				</div>
				<h6 class="mb-0 d-flex align-self-center ml-2 text-uppercase">
					@if ($pool->running())
						<i class="ui-action-is-enabled text-success"></i> Running (PID {{ $pool->getPid() }})
					@else
						<i class="ui-action-is-disabled"></i> Inactive (socket activation)
					@endif
				</h6>
			</div>
		</div>

		<table class="table table-condensed table-responsive-md">
			@php
				$log = $Page->getPoolLogName($Page->getName());
				$stat = \cmd('file_stat',$log)
			@endphp
			<tbody>
				@if ($log !== null)
					<tr>
						<th class="pt-4">Log file</th>
						<td>
							<button type="submit" name="download"
							        class="mr-2 btn btn-secondary ui-action ui-action-save ui-action-label ui-action-download"
							        value="{{ $log }}">
								Download
							</button>
							<table class="table-sm">
								<tr>
									<th class="text-left">Path</th>
									<td>{{ $log }}</td>
								</tr>
								<tr>
									<th class="text-left">Size</th>
									<td>{{  Formatter::reduceBytes(array_get($stat, 'size', 0))  }}</td>
								</tr>
								<tr>
									<th class="text-left">Last write</th>
									<td>
										{{ \UCard::init()->format((new DateTime())->setTimestamp(array_get($stat, 'mtime', time())), '',  IntlDateFormatter::LONG,  IntlDateFormatter::LONG) }}
									</td>
								</tr>
							</table>
						</td>
					</tr>
				@endif
				<tr>
					<th class="pt-4">Owner</th>
					<td>
						@if (HTTPD_FPM_USER_CHANGE)
							<div class="btn-group">
								<select name="pool-owner" class="custom-select">
									@php
										$activeUser = \cmd('user_get_username_from_uid', $pool->getUser());
									@endphp
									@foreach([cmd('common_get_service_value', 'siteinfo', 'admin_user'), cmd('web_get_sys_user')] as $user)
										@continue(!\cmd('user_exists', $user))
										<option value="{{ $user }}" @if ($activeUser === $user) selected="SELECTED" @endif>
											{{$user}}
										</option>
									@endforeach
								</select>
								<button type="submit" name="change-owner"
								        class="btn btn-secondary ui-action ui-action-save ui-action-label ui-action-run"
								        value="{{ $activeUser }}">
									Change Owner
								</button>
							</div>
						@else
							{{ \cmd('web_get_sys_user') }}
						@endif
					</td>
				</tr>
				<tr>
					<th class="pt-4">Configuration</th>
					<td>
						<button class="btn btn-secondary ui-action ui-action-label ui-action-details ml-auto"
						        data-toggle="collapse"
						        data-target="#configurationTarget" data-pool="{{ $Page->getName() }}" id="info"
						        aria-expanded="false"
						        aria-controls="configurationTarget">
							Show phpinfo
						</button>
						<div id="configurationTarget" class="collapse mt-1 w-100">
							<i class="ui-ajax ui-ajax-indicator ui-ajax-loading"></i> Loading information
						</div>

					</td>
				</tr>
				<tr>
					<th class="pt-4">Pool version</th>
					<td>
						<div class="btn-group">
							@php
								$activeVersion = \cmd('php_pool_get_version', $pool->getName());
								$versions = \cmd('php_pool_versions');
								sort($versions);
							@endphp
							<select name="pool-version" class="custom-select">
								@foreach($versions as $version)
									<option value="{{ $version }}"
									        @if ($activeVersion === $version) selected="SELECTED" @endif>
										{{$version}}
									</option>
								@endforeach
							</select>
							<button type="submit" name="change-version"
							        class="btn btn-secondary ui-action ui-action-save ui-action-label ui-action-run"
							        value="{{ $activeVersion }}">
								Change Version
							</button>
						</div>
					</td>
				</tr>
				@includeWhen(false, 'partials.cache-config', ['config' => $Page->getCacheConfig()])
				@if (TELEMETRY_ENABLED && cmd('telemetry_has', 'php-active'))
					<tr>
						<th>24 hour usage</th>
						<td>
							@php
								$rangeused = collect(\cmd('telemetry_range', ['php-active', 'php-idle', 'php-requests'], -86400, null, null, false));
								$activeStats = $rangeused->filter(function($v) {
									return $v['name'] == 'php-active';
								});
								$maxUsed = $activeStats->max('val');

							@endphp
							@php
								$maxChildren = $Page->getPoolConfiguration($Page->getName())[$Page->getName()]['pm.max_children'];
							@endphp
							<b>Peak</b> {{ (int)$maxUsed }} used / {{ $maxChildren }} max
						</td>
					</tr>
				@endif
				@if ($pool->running())
					<tr>
						<th>Uptime</th>
						<td>
							@php
								$duration = Formatter::time(time() - $pool->getStart());
							@endphp
							@if (!$duration)
								<span class="badge badge-success">NEW</span>
								<em>just started</em>
							@else
								{{ $duration }}
							@endif
						</td>
					</tr>
				@includeWhen($duration, 'partials.perf-data')
				@endif
			</tbody>
		</table>
	</form>
</div>