<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2021
 */

namespace apps\php_pools;

use GuzzleHttp\Exception\RequestException;
use HTTP\SelfReferential;
use Opcenter\Contracts\VirtualizedContextable;

class Build implements VirtualizedContextable {
	use \ContextableTrait {
		instantiateContexted as instantiateContextedReal;
	}

	use \apnscpFunctionInterceptorTrait;
	use \FilesystemPathTrait;

	protected $hostname;
	/**
	 * @var string
	 */
	protected $file;
	/**
	 * @var string
	 */
	protected $ip;

	private function __construct(string $ip, string $hostname)
	{
		$this->hostname = $hostname;
		$this->ip = $ip;
	}

	public function __destruct()
	{
		$this->clean();
	}

	protected function clean(): void
	{
		if (!$this->file) {
			return;
		}

		$this->file_delete($this->file);
	}

	public static function instantiateContexted(\Auth_Info_User $context, ...$ctor): self
	{
		return static::instantiateContextedReal($context, ...$ctor);
	}

	public function readFromPath(string $path): ?string
	{
		if (null === ($this->file = $this->file_temp($path, 'cp-build-check'))) {
			fatal('Bogus path detected');
		}
		$this->file_chmod($this->file, 644);

		$new = $this->file_expose($this->file, 'write');

		$fullPath = $this->domain_fs_path($this->file);
		file_put_contents($this->domain_shadow_path($new), '<?php phpinfo();', FILE_APPEND);
		// cleanup immediately, avoid unlink() errors by file:rename
		$this->file_delete($new);
		if (is_link($fullPath . '.php') || !$this->file_rename($this->file, $this->file .= '.php'))
		{
			fatal('Failed to rename build inspector file!');
		}
		try {
			$contents = SelfReferential::instantiateContexted($this->getAuthContext(),
				[$this->hostname, $this->ip])->get(basename($this->file))->getBody()->getContents();
		} catch (RequestException $e) {
			$code = $e->getResponse()->getStatusCode();
			if ($code >= 300 && $code < 400) {
				warn("Received redirection code `%d' on request", $code);
			} else {
				error("Received bad response from request: %d %s", $code, $e->getResponse()->getReasonPhrase());
			}
			return null;
		} finally {
			$this->clean();
		}

		return $contents;
	}
}