<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * @TODO needs urgent TLC
	 */
	namespace apps\filemanager;

	use Error_Reporter;
	use HTTP_Request2;
	use Page_Container;

	class Page extends Page_Container
	{
		const SHADOW_MIN_LEN = 60;
		const NATIVE_CHARSET = 'UTF-8';
		const TRANSLIT_KEY = 'fm.implicit-translit';

		public $upload_id;
		protected $temp_dir;
		private $cwd;
		private $state = 'list';
		private $clipboard = array();
		// file properties
		private $registered_extensions;
		private $_props = array();
		private $dir_entries;

		public function __construct()
		{
			parent::__construct();
			\Page_Renderer::display_on_error();
			$this->temp_dir = TEMP_DIR;
			$this->state = 'list';
			if (isset($_SESSION['fm_clipboard'])) {
				$this->clipboard = $_SESSION['fm_clipboard'];
			}
			$this->registered_extensions = $this->file_get_registered_extensions();
			$this->state = $this->view_state();
			$this->cwd = $this->getCurrentPath();

			if ($this->state == 'view_file') {
				$file = basename($this->cwd);
				$ext = $this->getExtension($file);
				if (!isset($_GET['editor']) && $ext == 'html') {
					$_GET['editor'] = 'rich';
				}
				if ($this->editor_mode() == 'rich') {
					$this->_initializeEditor();
				}
			}

			if (isset($_GET['mini'])) {
				\Page_Renderer::hide_all();
			}

			$this->add_css('FileManager.css');

			$this->init_page_js();
		}

		public function view_state()
		{
			if (isset($_GET['ep'])) {
				return 'edit_perms';
			}
			if (isset($_GET['download'])) {
				return 'download';
			}
			if (isset($_GET['co'])) {
				return 'list_archive';
			}
			if (isset($_GET['f'])) {
				$file = urldecode($_GET['f']);
				$stat = $this->file_stat($file);
				if (!$stat || $stat instanceof Exception) {
					// file does not exist, replace with a 403 maybe?
					return error("file `%s' does not exist", $file);
				}
				if ($stat['file_type'] == 'file') {
					return 'view_file';
				}
			}

			return 'list';
		}

		public function getCurrentPath()
		{
			if ($this->state == 'edit_perms') {
				return $_GET['ep'];
			} else if ($this->state == 'list_archive') {
				return $_GET['co'];
			}
			if (isset($_GET['f'])) {
				$path = urldecode($_GET['f']);
			} else if (isset($_POST['cwd'])) {
				$path = urldecode($_POST['cwd']);
			} else if (isset($_GET['cwd'])) {
				$path = urldecode($_GET['cwd']);
			} else {
				$path = $this->cwd;
				if (!$path) {
					$path = $this->user_getpwnam($this->getAuthContext()->username);
					$path = $path['home'];
				}
			}

			$newpath = array();
			$pathElement = strtok($path, "/");

			while ($pathElement !== false) {
				if ($pathElement == '..') {
					if (sizeof($newpath) - 1 > 0) {
						return error("You turn the page and find the Table of Contents. The End.");
					}
					array_pop($newpath);
				} else if ($pathElement && $pathElement != "/") {
					$newpath[] = $pathElement;
				}
				$pathElement = strtok("/");
			}

			return '/' . rtrim(join('/', $newpath), '/');
		}

		public function getExtension($file)
		{
			return substr($file, strrpos($file, '.')
				- strlen($file) + 1);
		}

		public function editor_mode()
		{
			return isset($_GET['editor']) && $_GET['editor'] == 'rich' ? 'rich' : 'raw';
		}

		private function _initializeEditor()
		{
			$this->init_js('tinymce');
			$props = $this->getProperties($this->cwd);
			$schema = "html";
			if ($props['html5']) {
				$schema = "html5";
			}

			$plugins = array(
				"spellchecker",
				"pagebreak",
				"layer",
				"table",
				"save",
				"tabfocus",
				"advlist",
				"emoticons",
				"searchreplace",
				"insertdatetime",
				"preview",
				"media",
				"searchreplace",
				"print",
				"contextmenu",
				"paste",
				"image",
				"directionality",
				"fullscreen",
				"noneditable",
				"visualchars",
				"nonbreaking",
				"template",
				"autoresize"
			);

			if ($props['html']) {
				$plugins[] = 'fullpage';
			}
			$this->add_javascript('$("#code").tinymce({
                    script_url: "/js/tiny_mce/tinymce.min.js",
                    mode : "textareas",
                    theme : "modern",
                    plugins : "' . implode(",", $plugins) . '",
                    schema: "' . $schema . '",
                    verify_html: false,
                    autoresize_max_height: 600,
                    // Theme options
                    theme_modern_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
                    theme_modern_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,tablecontrols",
                    theme_modern_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage,|,fullpage",
                    theme_modern_toolbar_location : "top",
                    theme_modern_toolbar_align : "left",
                    theme_modern_statusbar_location : "bottom",
                    theme_modern_resizing : true,
                    // Skin options
                    theme_advanced_toolbar_location : "top",
                    theme_advanced_toolbar_align : "left",
                    theme_advanced_statusbar_location : "bottom",
                    theme_advanced_resizing: true,
                    add_unload_trigger : true,
                    remove_linebreaks : false,
                    inline_styles : false,
                    tab_focus: ":prev,:next"
                        
            	});
            ', 'internal', false, false);
		}

		public function getProperties($file)
		{
			if (isset($this->_props[$file])) {
				return $this->_props[$file];
			}
			$props = array(
				'html5'   => false,
				'charset' => null,
				'html'    => false,
				'head'    => false,
				'mime'    => null,
			);
			$ext = $this->getExtension($file);
			$props['mime'] = $this->getMimeType($file);
			$html_ext = array('html', 'htm', 'php', 'shtml', 'phtml');
			if (!in_array($ext, $html_ext)) {
				return $props;
			}

			$contents = $this->file_get_file_contents($file);
			mute_warn(true);
			$dom = new \DOMDocument();

			if (!$contents || !@$dom->loadHTML($contents)) {
				return $props;
			}
			unmute_warn();
			$maxlen = 64;
			// in case PHP is interpersed within the document
			if ($html_ext == "php" || $html_ext == "phtml") {
				$maxlen = 2048;
			}
			if (stristr(substr($contents, 0, $maxlen), "<!DOCTYPE html>")) {
				$props['html5'] = true;
			}
			$props['html'] = true;
			$charset = null;
			foreach ($dom->getElementsByTagName('meta') as $a) {
				$tmp = $a->getAttribute("charset");
				if ($tmp) {
					$charset = $tmp;
					break;
				}
				$tmp = $a->getAttribute("content");
				if (strstr($tmp, "charset")) {
					$tmp = trim(substr($tmp, strpos($tmp, "=")), '"=\'>');
					$charset = $tmp;
				}
			}

			// $charset contains weird additional markup, default to utf-8
			if (!$charset) {
				$charset = $this->detectCharsetFromContent($contents);
			}

			$charset = trim($charset);
			$illegalchars = "!@#$%^&*()+=<>?'\"\\/";
			$encodings = mb_list_encodings();
			array_map(function ($a) {
				return strtoupper($a);
			}, $encodings);
			if (strspn($charset, $illegalchars)) {
				$charset = null;
			} else if (!in_array(strtoupper($charset), $encodings)) {
				$charset = null;
			}
			$props['charset'] = strtoupper($charset);
			$this->_props[$file] = $props;

			return $props;
		}

		public function getMimeType($file)
		{
			$status = $this->getApnscpFunctionInterceptor()->file_get_mime_type($file);
			$this->bind($status);

			return $status;
		}

		public function detectCharsetFromContent($data)
		{
			$charset = $this->_detectCharsetFromMeta($data);
			if ($charset) {
				return $charset;
			}
			if (function_exists('mb_detect_order')) {
				mb_detect_order("ASCII,UTF-8,ISO-8859-1,windows-1252,iso-8859-15");
			}

			return strtoupper(mb_detect_encoding($data));

		}

		/**
		 * Detect charset from meta tag
		 *
		 * @TODO last ditch effort if mb_detect_order() fails us
		 */
		private function _detectCharsetFromMeta($data)
		{
			$pos = strpos($data, "charset");
			// Per HTML5 spec, charset may only be declared within first 1024 bytes
			// http://www.w3.org/TR/html5-diff/
			if ($pos === false || $pos > 1024) {
				return false;
			}
			$str = substr($data, $pos, 60);
			// formatted as <meta content="text;html charset=iso-8859-1">
			$charset = strtok($str, "=");
			if ($charset) {
				$charset = strtok('"\'>');
			}

			return strtoupper($charset);
		}

		private function init_page_js()
		{
			$state = $this->getState();
			if ($state instanceof Exception) {
				$state = 'list';
			}
			if (is_array($state)) {
				// @XXX debugging
				\Error_Reporter::report(var_export($state, true));
			}
			$this->add_javascript('var __esprit_fm  = "' . $state . '";
                var __esprit_cwd = "' . $this->get_cwd() . '";', 'internal', false);
			$this->add_javascript('/apps/filemanager/filemanager.js');
			if ($this->state == 'list_archive') {
				$this->add_head('<style type="text/css">#dir-hdr, #file-root { display: none; }</style>');
			}
			$this->init_js('browser');
			if ($this->state == 'list') {
				$this->init_js('browser', 'upload');
				$this->upload_id = uniqid();
				$maxsize = \HTML_Kit::maxUpload();
				$this->add_javascript('var upload_id=' . "'" . $this->upload_id . "', MAX_SIZE=" . $maxsize . ";",
					'internal', false, true);
			} else if ($this->state == 'view_file') {
				if ($this->editor_mode() == 'rich') {
					$this->add_javascript('var edited = false;',
						'internal', false, true);
				} else {
					$this->add_javascript('var edited = false, tinyMCE = null;', 'internal', false, true);
				}
				$newmode = $this->editor_mode() == 'raw' ? 'rich' : 'raw';
				$this->add_javascript('$(\'#editor\').click( function() {
                            if ((tinyMCE != null && tinyMCE.activeEditor.isDirty() || tinyMCE == null && edited) && !confirm(\'Are you sure you would like to change editor modes before saving?  Any unsaved changes will be lost.\'))
                                return false;
                            edited = false; tinyMCE = null;
                            location.href = \'/apps/filemanager.php?f=' . urldecode($_GET['f']) . '&editor=' . $newmode . '\';
                        });
                        $("#save").click(function() { edited = false ;  return true; } );
                        window.onbeforeunload = function () {if (tinyMCE != null && tinyMCE.activeEditor.isDirty()
                         || tinyMCE == null && edited) return "You have unsaved changes."; };
                        $(\'#code\').keypress(function () { edited = true; $(\'#code\').unbind(); });',
					'internal', true);

			}
		}

		public function getState()
		{
			return $this->state;
		}

		public function setState($mState)
		{
			$this->state = $mState;
		}

		public function get_cwd()
		{
			$path = $this->getCurrentPath();
			switch ($this->state) {
				case 'list_archive':
					return dirname($path);
				default:
					return $path;
			}
		}

		public function __destruct()
		{
			if (!is_null($this->clipboard)) {
				$_SESSION['fm_clipboard'] = $this->clipboard;
			}
			parent::__destruct();
		}

		public function clipboardIsEmpty()
		{
			return !sizeof($this->clipboard);
		}

		/**
		 *
		 * @TODO rewrite when time permits
		 *
		 */
		public function on_postback($params)
		{
			if (isset($params['cwd'])) {
				$this->cwd = $params['cwd'];
			}
			if (isset($params['cwd']) && sizeof($params) == 1) {
				$this->getDirectoryContents();
				if ($this->pb_succeeded()) {
					return $this->hide_pb();
				}

				return;
			}
			if (isset($params['Create_Symlink'])) {
				if (substr($params['symlink_name'], 0, 1) != '/') {
					$params['symlink_name'] = $this->cwd . '/' . $params['symlink_name'];
				}
				$status = $this->file_symlink(
					$params['symlink_target'],
					$params['symlink_name']
				);

				return $this->bind($status);
			} else if (isset($params['Download_Remote'])) {
				if (!$params['remote_url'] || !preg_match(\Regex::URL, $params['remote_url'])) {
					return error("missing or invalid URL");
				}

				// download file to temp directory and extract
				return $this->_downloadFile($params['remote_url'], !empty($params['extract']));

			} else if (isset($params['rename'])) {
				for ($i = 0, $n = sizeof($params['ren_orig']); $i < $n; $i++) {
					$params['ren_orig'][$i] = $this->cwd . '/' . $params['ren_orig'][$i];
					$params['ren_new'][$i] = trim($params['ren_new'][$i]);
					if ($params['ren_new'][$i][0] != '/') {
						$params['ren_new'][$i] = $this->cwd . '/' . $params['ren_new'][$i];
					}
					$this->file_rename($params['ren_orig'][$i], $params['ren_new'][$i]);
				}
				//$this->file_rename($params['ren_orig'],$params['ren_new']);
			} else if (isset($params['f'])) {
				$file = urldecode($params['f']);
				if (isset($params['Save_Changes']) || isset($params['code'])) {
					/** save file edit */
					if (!isset($params['file']['name'])) {
						return error("no filename");
					}
					$this->state = 'view_file';
					$this->cwd = rtrim($params['file']['name'], '/');
					$_GET['f'] = $this->cwd;
					$code = $params['code'];
					if (isset($params['file']['charset']) && $params['file']['charset'] !== self::NATIVE_CHARSET) {
						/* translate code from UTF-8 back to native format */
						$flags = \Preferences::get(self::TRANSLIT_KEY) ? '//TRANSLIT' : '';

						if (false === ($tmp = iconv(self::NATIVE_CHARSET, $params['file']['charset'] . $flags,
								$code))) {
							warn("Charset of file detected as `%s'. Invalid characters detected in file " .
								"outside codepage of `%s'. This file has been implicitly changed to " . self::NATIVE_CHARSET .
								" to compensate. You can toggle implicit conversion under " .
								"Account > Settings > Development > Code Page Conversion.",
								$params['file']['charset'],
								$params['file']['charset']
							);
						} else {
							$code = $tmp;
						}
					}
					$eol = $params['file']['eol'];
					if (isset($params['eol'])) {
						$eol = $params['eol'];
						info("forced EOL `%s'", ucwords($eol));
					} else if ($eol === 'windows') {
						// avoid an extra substitution since the HTTP spec
						// calls for form data to post as \r\n
						$eol = null;
					}
					$this->saveChanges($code, $params['file']['name'], $eol);
					$this->_invalidateQuota();
				}
				/** view the file contents */
				$this->cwd = $file;
				$stat = $this->file_stat($file);
				if ($stat instanceof Exception || !$stat || $this->errors_exist()) {
					return;
				}
				if ($stat['file_type'] == 'dir' || $stat['link'] == 2) {
					$this->state = 'list';
				} else {

					$this->cwd = rtrim($file, '/');
					$this->state = 'view_file';
				}

			} else if (isset($params['co'])) {
				/** compressed file view */
				$this->cwd = $params['co'];
				$this->state = 'list_archive';
				$this->contents = $this->getDirectoryContents();

			} else if (isset($params['download'])) {
				/** download a file */
				$this->cwd = dirname($params['download']);
				/** delete the file in case it exists */
				/** TODO: Add a locking mechanism */

				$path = $this->file_make_path($params['download']);
				if (!is_readable($path)) {
					// insufficient permission to access
					$this->file_delete($this->glob_escape($this->temp_dir . '/' . basename($params['download'])));
					$status = $this->file_copy($this->glob_escape($params['download']), $this->temp_dir);
					if (!$status || $status instanceof Exception) {
						return;
					}
					$path = $this->getApnscpFunctionInterceptor()->file_make_path($this->temp_dir . '/' . basename($params['download']));
				}
				if (!file_exists($path) || is_link($path)) {
					header("HTTP/1.1 403 Forbidden", true, 403);
					exit();
				}
				\HTML_Kit::makeDownloadable(
					$path,
					str_replace([';', '"'], '_', basename($params['download'])),
					false
				);
				register_shutdown_function(function () use ($params) {
					$this->getApnscpFunctionInterceptor()->file_delete($this->glob_escape($this->temp_dir . '/' . basename($params['download'])));
				});

				exit(0);

			} else if (isset($params['d'])) {
				// delete a file or directory
				$this->state = 'list';
				$params['d'] = base64_decode($params['d']);
				$this->cwd = \dirname($params['d']);
				$this->delete($params['d'], true);
				$this->_invalidateQuota();
			} else if (isset($params['ep'])) {
				// edit permissions
				$this->state = 'edit_perms';
				$this->cwd = $params['ep'];
				/** edit permissions */
				if (isset($params['eol'])) {
					// eol change
					$target = $params['eol'];
					$status = $this->convert_eol($params['file'], $target);
					$this->bind($status);

				} else if (isset($params['Change_Permissions'])) {
					// permission change
//                    if ($file['file_type'] != 'dir')
//                        $this->cwd = basename($params['ep']);
					$permission = array();
					if (isset($params['permission'])) {
						$permission = $params['permission'];
					}
					$original = $params['original_permissions'];
					$recursive = isset($params['recursive']);
					$newperm = (int)sprintf("%o", array_sum(array_map('hexdec', $permission)));
					if ($recursive || $original != $newperm) {
						$this->changePermissions($params['file'],
							$newperm,
							$recursive);
					}
					$this->changeOwner($params['file'],
						$params['user_ownership'],
						isset($params['chown_recursive']));
				}


			} else if (isset($params['Create'])) {
				/** create directory */
				$this->cwd = $params['cwd'];
				$this->state = 'list';
				$status = $this->createDirectory($this->cwd . '/' . $params['Directory_Name']);
				$this->_invalidateQuota();
				$this->bind($status);
				if ($status == false) {
					return error("Target directory " . $params['Directory_Name'] . " exists");
				}

				return $status;
			} else if (isset($params['Create_File'])) {
				$file = $params['cwd'] . '/' . $params['File_Name'];
				$this->state = 'list';
				$status = $this->file_touch($file);
				$this->bind($status);

				return $status;
			} else if (isset($params['Delete'], $params['file'])) {
				/** multi var delete */
				$this->state = 'list';
				$this->cwd = $params['cwd'];

				return $this->delete($params['file'], true) && $this->_invalidateQuota();

			} else if (isset($params['Upload'])) {
				/** upload a file */
				$this->cwd = $params['cwd'];
				$this->handleFileUpload();
				$this->_invalidateQuota();
			} else if (isset($params['Add_Clipboard'])) {
				if (!isset($params['file'])) {
					return warn("no files selected");
				}

				if ($this->isCompressedFile($params['cwd'])) {
					$this->state = 'list_archive';
				} else {
					$this->state = 'list';
				}

				$this->cwd = $params['cwd'];
				$this->addToClipboard($params['file']);
				/** clipboard information */

			} else if (isset($params['Clipboard_Clear'])) {
				if ($this->isCompressedFile($params['cwd'])) {
					$this->state = 'list_archive';
				} else {
					$this->state = 'list';
				}
				if (isset($params['Clipboard_Items'])) {
					$this->clipboard = array_diff($this->clipboard, $params['Clipboard_Items']);
				}
				$this->cwd = $params['cwd'];
			} else if (isset($params['Download_Folder'])) {
				$this->download_files((array)$this->get_cwd());
			} else if (isset($params['Clipboard_Download'])) {
				if (!isset($params['Clipboard_Items'])) {
					return warn("no files selected");
				}
				$this->download_files($params['Clipboard_Items']);
			} else if (isset($params['Clipboard_Copy'])) {
				if (!isset($params['Clipboard_Items'])) {
					return warn("no files selected");
				}
				if ($this->isCompressedFile($params['cwd'])) {
					$this->state = 'list_archive';
				} else {
					$this->state = 'list';
				}

				$this->cwd = $params['cwd'];
				$this->copy_files($params['Clipboard_Items'], $this->cwd);
				//$this->clipboard = array_diff($this->clipboard, $params['Clipboard_Items']);


			} else if (isset($params['Clipboard_Move'])) {
				if ($this->isCompressedFile($params['cwd'])) {
					$this->state = 'list_archive';
				} else {
					$this->state = 'list';
				}
				if (!isset($params['Clipboard_Items'])) {
					return warn("no files selected!");
				}
				$this->cwd = $params['cwd'];
				if ($this->file_move($params['Clipboard_Items'], $this->cwd)) {
					foreach ($params['Clipboard_Items'] as $item) {
						$key = array_search($item, $this->clipboard);
						if ($key !== false) {
							$this->clipboard[$key] = $this->cwd . '/' . basename($item);
						}
					}
				}
				//$this->clipboard = array_diff($this->clipboard, $params['Clipboard_Items']);
			} else if (isset($params['Clipboard_Symlink'])) {
				if (!isset($params['Clipboard_Items'])) {
					return warn("no files selected!");
				}

				$this->cwd = $params['cwd'];
				foreach ($params['Clipboard_Items'] as $symlink) {
					$this->file_symlink($symlink,
						$this->cwd . '/' . basename($symlink));
				}
			} else if (isset($params['Extract'])) {
				/** extract compressed archive */
				$this->state = 'list';
				$this->cwd = $params['archive_path'];
				$src = $params['archive_path'];
				$dest = $params['Extract_Path'];
				$arhead = $params['archive_head'];

				if (isset($params['extract_subdir'])) {
					$dest = $dest . '/' . $this->extract_name_from_archive($src);

					if (isset($params['extract_noconflict'])) {
						mute_warn();
						$this->no_bind();
						$dest = $this->file_noconflict($dest);
						$this->do_bind();
						unmute_warn();
					}

				}

				$this->extract_files($src, $dest);
				if ($arhead) {
					$dest .= '/' . $arhead;
				}
				if (!$this->errors_exist()) {
					header('Location: ' . \HTML_Kit::page_url() . '?cwd=' . \HTML_Kit::url_encode($dest));
					$this->_invalidateQuota();
					exit;
				}
			}
		}

		public function getDirectoryContents()
		{
			if (isset($this->dir_entries)) {
				// broken?
				//return $this->dir_entries;
			}

			return $this->__getDirectory($this->cwd);
		}

		private function __getDirectory($dir)
		{
			if ($this->is_compressed($dir)) {
				$files = $this->file_get_archive_contents($this->cwd);
			} else {
				$files = $this->file_get_directory_contents($this->cwd);
			}

			return $files;
		}

		public function is_compressed($mFile)
		{

			return $this->isCompressedFile($mFile);

		}

		public function isCompressedFile($mFile)
		{
			return $this->getApnscpFunctionInterceptor()->file_is_compressed($mFile);

		}

		private function _downloadFile($file, bool $extract = false)
		{
			static $lastLocation;

			if (isset($lastLocation) && $lastLocation == $file) {
				return error("infite redirect loop detected: " .
					"http redirect redirected to itself, loop on `%s'", $file);
			}

			$status = -1;
			$output = array();
			$prefix = $this->getAuthContext()->domain_fs_path();
			$tmp = tempnam($prefix . '/tmp', 'wget');
			$dest = $this->cwd . "/" . basename($file);
			$src = '/tmp/' . basename($tmp);
			if (extension_loaded('curl')) {
				$adapter = new \HTTP_Request2_Adapter_Curl();
			} else {
				$adapter = new \HTTP_Request2_Adapter_Socket();
			}
			$http = new HTTP_Request2(
				$file,
				HTTP_Request2::METHOD_GET,
				array(
					'adapter'    => $adapter,
					'store_body' => false
				)
			);
			$observer = new \HTTP_Request2_Observer_SaveDisk($tmp);
			$http->attach($observer);
			try {
				$response = $http->send();
				$code = $response->getStatus();
				switch ($code) {
					case 200:
						break;
					case 403:
						return error("URL request forbidden by server");
					case 404:
						return error("URL not found on server");
					case 301:
					case 302:
						$lastLocation = $file;
						$newLocation = $response->getHeader('location');
						info("request moved to new location, following `%s'", $newLocation);

						return $this->_downloadFile($newLocation, $extract);
					default:
						return error("URL request failed, code `%d': %s",
							$code, $response->getReasonPhrase());
				}
				$content = $response->getHeader('content-type');
				$okcontent = array('application/octet-stream', 'application/zip');
				if (!in_array($content, $okcontent)) {
					Error_Reporter::report($content);
				}
				// this returns nothing as xfer is saved directly to disk
				$http->getBody();
			} catch (\HTTP_Request2_Exception $e) {
				return error("fatal error retrieving URL: `%s'", $e->getMessage());
			}
			// write file
			$this->file_endow_upload(basename($src)) && $this->file_move($src, $dest);
			$this->_invalidateQuota();
			$this->file_purge();

			if ($extract) {
				if (!$this->isCompressedFile($dest)) {
					return info("file is not a compressed archive, not decompressing!");
				}
				$this->file_extract($dest, $this->cwd);
				$this->file_delete($dest);
			}


			return true;
		}

		private function _invalidateQuota()
		{
			\Session::forget('quota');
		}

		public function saveChanges($mData, $mFile, $eol)
		{
			if ($eol) {
				$ending = $this->_os2EOL($eol);
				$mData = $this->_convertEOL($mData, $ending);
			} else if (!strncmp($mData, "#!", 2) && ($mData[2] === "/" || $mData[2] === " ")) {
				// #!/ and #! / are both OK
				// #!       / is too, but a performance-wrecking fringe case
				info("Shell script detected, forcing Unix line endings");
				$mData = $this->_convertEOL($mData, "\n");
			}

			$output = $this->getApnscpFunctionInterceptor()->file_put_file_contents($mFile, $mData, true);
			$this->bind($output);

			return $output;
		}

		private function _os2EOL($eol)
		{
			$eol = strtolower($eol);
			$ending = "\r\n";
			if ($eol == "unix") {
				return "\n";
			}
			if ($eol == "mac") {
				return "\r";
			}

			return $ending;
		}

		/**
		 * Convert file to new EOL
		 *
		 * @param string $data
		 * @param string $style
		 * @return string
		 */
		private function _convertEOL($data, $style)
		{
			return preg_replace('!\R!u', $style, $data);
		}

		public function glob_escape($mStr)
		{
			if (is_array($mStr)) {
				foreach ($mStr as $str) {
					$paths[] = substr($str, 0, (strrpos($str, '/'))) . str_replace(array(
							'*',
							'[',
							']',
							'{',
							'}',
							'?',
							' '
						),
							array('\*', '\[', '\]', '{', '\}', '\?', '\\ '),
							substr($str, strrpos($str, '/')));
				}

				return $paths;
			} else {
				return substr($mStr, 0, strrpos($mStr, '/')) . str_replace(array('*', '[', ']', '{', '}', '?', ' '),
						array('\*', '\[', '\]', '\{', '\}', '\?', '\\ '),
						substr($mStr, strrpos($mStr, '/')));
			}
		}

		public function delete($file, $recursive = true)
		{
			return $this->file_delete($this->glob_escape($file), $recursive);
		}

		private function convert_eol($mFile, $mEOL)
		{
			return $this->file_convert_eol($mFile, $mEOL);
		}

		public function changePermissions($mFile, $mMode, $mRecursive = false)
		{
			$status = $this->getApnscpFunctionInterceptor()->file_chmod($mFile, $mMode, $mRecursive);
			$this->bind($status);

			return $status;
		}

		public function changeOwner($mFile, $mUser, $mRecursive = false)
		{
			return $this->getApnscpFunctionInterceptor()->file_chown($mFile, $mUser, $mRecursive);
		}

		public function createDirectory($mPath)
		{
			$status = $this->getApnscpFunctionInterceptor()->file_create_directory($mPath);
			$this->bind($status);

			return $status;
		}

		public function handleFileUpload()
		{
			$ret = $this->_handleFileUploadRaw();

			if (!is_ajax()) {
				return (bool)$ret;
			}

			$buffer = Error_Reporter::flush_buffer(Error_Reporter::E_ERROR | Error_Reporter::E_WARNING);
			if ($buffer) {
				$buffer = array_pop($buffer);
				$msg = $buffer['message'];
			} else {
				$msg = '';
			}

			echo json_encode(
				array(
					'success' => $ret && Error_Reporter::get_severity() == Error_Reporter::E_OK,
					'file'    => $ret,
					'msg'     => $msg,
				)
			);
			exit();
		}

		private function _handleFileUploadRaw()
		{
			if (isset($_POST['uploaded_file'])) {
				$uploads = $_POST['uploaded_file'];
			} else if (isset($_FILES['uploaded_file'])) {
				$uploads = $_FILES['uploaded_file'];
			} else {
				return warn("No files uploaded");
			}
			$localNames = '';
			if (isset($_POST['uploaded_saveas'])) {
				$localNames = $_POST['uploaded_saveas'];
			}

			// single file upload
			if (!is_array($uploads['name'][0])) {
				foreach (array('name', 'type', 'tmp_name', 'error', 'size') as $key) {
					$uploads[$key] = (array)$uploads[$key];
				}
			}
			$overwrite = !empty($_POST['upload_overwrite']);

			$prefix = $this->getAuthContext()->domain_fs_path();
			for ($i = 0, $n = count($uploads['name']); $i < $n; $i++) {
				$name = $uploads['name'][$i];
				$size = $uploads['size'][$i];
				$tmp_name = $uploads['tmp_name'][$i];
				$error = $uploads['error'][$i];

				$local = '';
				if (isset($localNames[$i])) {
					$local = trim($localNames[$i]);
				}
				if (!$local) {
					$local = basename($name);
				}

				if ($error != UPLOAD_ERR_OK) {
					switch ($error) {
						case UPLOAD_ERR_INI_SIZE:
							$msg = 'File exceeds maximum size, ' . ini_get('upload_max_filesize');
							break;
						case UPLOAD_ERR_PARTIAL:
							$msg = 'Upload interrupted, file only partially uploaded.';
							break;
						case UPLOAD_ERR_NO_FILE:
							$msg = 'No file uploaded';
							break;
						case UPLOAD_ERR_CANT_WRITE:
							$msg = 'Cannot write to temp folder';
							break;
					}

					if ($i > 0 && $error == UPLOAD_ERR_NO_FILE) {
						continue;
					}
					error("Upload failed! " . $msg);
					continue;

				}

				$quota = $this->common_get_disk_quota();
				if ($quota['total'] && ($size / 1024 + $quota['used']) > $quota['total']) {
					error(sprintf("File size exceeds free space by %.2f MB",
						($size / 1024 + $quota['used'] - $quota['total']) / 1024));
					unlink($tmp_name);
					continue;
				}
				$mvtmp = tempnam($prefix . '/tmp/', 'up');
				if (!move_uploaded_file($tmp_name, $mvtmp)) {
					unlink($tmp_name);

					return false;
				}
				chmod($mvtmp, 0644);

				$this->file_endow_upload(basename($mvtmp));
				if (!$this->file_move(TEMP_DIR . '/' . basename($mvtmp), $this->cwd . '/' . $local, $overwrite)) {
					$this->file_delete(TEMP_DIR . '/' . basename($mvtmp));

					return false;
				}
			}

			return $uploads['name'][0];

		}

		public function addToClipboard($mFiles)
		{
			$this->clipboard = array_unique(array_merge($this->clipboard, $mFiles));
		}

		public function download_files($files)
		{
			$path = $this->file_initialize_download($files);
			if (!$path) {
				return error("failed to initialize file download");
			}
			$fname = sprintf("%s-%s.tar", \Util_Conf::login_domain(), hash("crc32b", time()));
			\HTML_Kit::makeDownloadable($path, $fname, true);

			exit(0);
		}

		public function copy_files($mSrc, $mDest, $overwrite = true)
		{
			$this->file_copy($this->glob_escape($mSrc), $mDest, $overwrite);

		}

		public function extract_name_from_archive($archive)
		{
			return basename($archive, $this->file_compression_extension($archive));
		}

		public function file_noconflict($file)
		{
			if ($file[0] != '/') {
				$file = $this->get_cwd() . $file;
			}
			if (!$this->file_exists($file)) {
				return $file;
			}
			$i = 1;
			do {
				$nfile = $file . '(' . $i . ')';
				if (!$this->file_exists($nfile)) {
					info("`%s' exists, target changed to %s", $file, $nfile);

					return $nfile;
				}
				$i++;
			} while ($i < 500);

			return error('%s: conflicting filename', $file);
		}

		public function extract_files($mArchive, $mDest)
		{
			if (!$this->getApnscpFunctionInterceptor()->file_exists($mDest)) {
				$this->file_create_directory($mDest);
			}

			return $this->file_extract($mArchive, $mDest);
		}

		public function isUpload()
		{
			return isset($_POST['Upload']);
		}

		public function printPermissionSet($mStatArr, $mType, $mPermGroup)
		{
			$shift = 0;
			switch ($mType) {
				case 'read':
					$mask = 256;
					break;
				case 'write':
					$mask = 128;
					break;
				case 'execute':
					$mask = 64;
					break;
			}
			switch ($mPermGroup) {
				case 'user':
					$shift = 0;
					break;
				case 'group':
					$shift = 3;
					break;
				case 'other':
					$shift = 6;
					break;
			}
			if ((($mask >> $shift) & $mStatArr['permissions']) == ($mask >> $shift)) {
				return 'CHECKED';
			} else {
				return '';
			}
		}

		public function canEdit($mPath)
		{

		}

		public function resolveACLS($mData)
		{
			if (!$mData) {
				return null;
			}

		}

		public function getPerms()
		{
			return $this->getApnscpFunctionInterceptor()->file_stat($this->cwd);
		}

		public function canModify($mFile)
		{
			return $this->getApnscpFunctionInterceptor()->file_can_modify($mFile);
		}

		public function statParent()
		{
			$dir = $this->getCurrentPath();

			return $this->statFile($dir);
		}

		public function statFile($file)
		{
			$status = $this->getApnscpFunctionInterceptor()->file_stat($file);
			$this->bind($status);

			return $status;
		}

		public function getFileContents($file)
		{
			return $this->getApnscpFunctionInterceptor()->file_get_file_contents($file, true);
		}

		public function getClipboardContents()
		{
			return $this->clipboard;
		}

		public function getLinkType($mType, $mNLinks)
		{
			switch ($mType) {
				case 2:
					return 'Symbolic dir link';
				case 1:
					return 'Symbolic file link';
				case 0:
					if ($mNLinks > 1) {
						return 'Hard Link';
					}

					return 'n/a';
			}
		}

		public function getACLs($file)
		{
			return $this->getApnscpFunctionInterceptor()->file_get_acls($file);
		}

		public function getIconType($type, $link, $canRead = true, $canExecute = true, $referent = '', $portable = true)
		{
			switch ($type) {
				case 'file':
					if ($canRead) {
						return '/images/apps/filemanager/file.gif';
					} else {
						return '/images/apps/filemanager/file-forbidden.gif';
					}
				case 'dir':
					if ($canRead && $canExecute) {
						return '/images/apps/filemanager/folder.gif';
					} else {
						return '/images/apps/filemanager/folder-forbidden.gif';
					}
				case 'link':
					if ($link == 1) {
						if (is_null($referent)) {
							return '/images/apps/filemanager/file-symlink-broken.gif';
						}
						if ($portable) {
							return '/images/apps/filemanager/file-symlink.gif';
						}

						return '/images/apps/filemanager/file-nonportable.gif';
					} else if ($link == 2) {
						if (is_null($referent)) {
							return '/images/apps/filemanager/folder-symlink-broken.gif';
						}
						if ($portable) {
							return '/images/apps/filemanager/folder-symlink.gif';
						}

						return '/images/apps/filemanager/folder-nonportable.gif';
					}

					return '/images/apps/filemanager/file-symlink-broken.gif';
			}
		}

		public function getParentDirectory()
		{
//            if ($this->cwd[((strlen($this->cwd)) - 1)] == '/') {
			/** is directory, get parent */
//                print "Returning ".substr($this->cwd,0,
//                              strrpos(substr($this->cwd,0,strrpos($this->cwd,'/')),'/'));
			$parent = $this->cwd;
			if (strlen($parent) > 1) {
				$parent = rtrim($parent, '/');
			}

			return dirname($parent);
			//substr($this->cwd,0,strrpos($this->cwd,'/'));
//            } else
			return substr($this->cwd, 0,
					strrpos($this->cwd, '/')) . '/';

		}

		public function determinePerms($perms)
		{
			$info = "";
			if (($perms & 0xC000) == 0xC000) {
				// Socket
				$info = 's';
			} else if (($perms & 0xA000) == 0xA000) {
				// Symbolic Link
				$info = 'l';
			} else if (($perms & 0x8000) == 0x8000) {
				// Regular
				$info = '-';
			} else if (($perms & 0x6000) == 0x6000) {
				// Block special
				$info = 'b';
			} else if (($perms & 0x4000) == 0x4000) {
				// Directory
				$info = 'd';
			} else if (($perms & 0x2000) == 0x2000) {
				// Character special
				$info = 'c';
			} else if (($perms & 0x1000) == 0x1000) {
				// FIFO pipe
				$info = 'p';
			} else {
				// Unknown
				$info = '-';
			}

			// Owner
			$info .= (($perms & 0x0100) ? 'r' : '-');
			$info .= (($perms & 0x0080) ? 'w' : '-');
			$info .= (($perms & 0x0040) ?
				(($perms & 0x0800) ? 's' : 'x') :
				(($perms & 0x0800) ? 'S' : '-'));

			// Group
			$info .= (($perms & 0x0020) ? 'r' : '-');
			$info .= (($perms & 0x0010) ? 'w' : '-');
			$info .= (($perms & 0x0008) ?
				(($perms & 0x0400) ? 's' : 'x') :
				(($perms & 0x0400) ? 'S' : '-'));

			// World
			$info .= (($perms & 0x0004) ? 'r' : '-');
			$info .= (($perms & 0x0002) ? 'w' : '-');
			$info .= (($perms & 0x0001) ?
				(($perms & 0x0200) ? 't' : 'x') :
				(($perms & 0x0200) ? 'T' : '-'));

			return $info;

		}

		public function list_users()
		{
			$users = array_keys($this->getApnscpFunctionInterceptor()->user_get_users());
			$users[] = \Web_Module::WEB_USERNAME;

			return $users;
		}

		public function get_mode()
		{
			switch ($this->state) {
				case 'edit_perms':
					return 'ep';
				case 'view_file':
					return 'f';
				default:
					return 'cwd';
			}
		}

		public function diskUsage()
		{
			if (!isset($_SESSION['quota']) ||
				($_SERVER['REQUEST_TIME'] - $_SESSION['quota']['ttl']) > 900) {
				$quota = $this->user_get_quota($_SESSION['username']);
				$free = ($quota['qhard'] - $quota['qused']) * 1024;
				if ($free > 0) {
					$free = \Formatter::reduceBytes($free, 2, 2);
				} else {
					$free = 0;
				}
				$total = \Formatter::reduceBytes(($quota['qhard'] * 1024), 2, 2);
				$_SESSION['quota'] = array(
					'info' => array('free' => $free, 'total' => $total),
					'ttl'  => $_SERVER['REQUEST_TIME']
				);

			}

			return $_SESSION['quota']['info'];
		}

		public function fileEditable(string $mime): bool
		{
			if (false !== strpos($mime, "text") || false !== strpos($mime, "empty")) {
				return true;
			}

			if ($mime === 'application/json') {
				return true;
			}

			return false;
		}
	}
