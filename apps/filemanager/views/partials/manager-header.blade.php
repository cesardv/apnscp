<tr id="dir-hdr">
	<td class="head1" width="25" align="center">&nbsp;</td>
	<td class="head1 name">Name</td>
	<td class="head1 right">Size</td>
	<td class="head1 right">Owner</td>
	<td class="head1 attr">Attributes</td>
	<td class="head1 actions">Action</td>
</tr>
<tr id="file-root">
	<td class="cell1" align="center">
		@if ($Page->getState() == 'list')
			<input type="checkbox" id="select_all"/>
		@endif
	</td>
	<td class="cell1 name" align="left">
		<a class="node node-parent-dir"
		   href="filemanager?cwd={{ $Page->getParentDirectory() }}">
			Parent Directory
		</a>
	</td>
	<td class="cell1" align="right"></td>
	<td class="cell1" align="right">root</td>
	<td class="cell1" align="center"><code>drwx------</code></td>
	<td class="cell1" align="center"><img src="/images/spacer.gif" alt="" height="1" width="70"/></td>
</tr>