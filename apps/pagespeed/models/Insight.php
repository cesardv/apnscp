<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

namespace apps\pagespeed\models;

use Google_Service_Pagespeedonline_LighthouseAuditResultV5 as Audit;
use Google_Service_Pagespeedonline_LighthouseResultV5 as Lighthouse;

class Insight {
	protected $begin;
	/**
	 * @var InsightDetails
	 */
	protected $diagnostics;

	protected $test;
	/**
	 * @var string
	 */
	protected $hash;

	public function __construct(Lighthouse $data)
	{
		$this->hash = hash('sha256', microtime(true) . spl_object_hash($this));
		$this->begin = $data->getFetchTime();
		$this->test = $data->getAudits();

		$this->intakeDetails(array_get($this->test, 'diagnostics'));
	}

	public function getHash(): string
	{
		return $this->hash;
	}

	/**
	 * Get Pagespeed audit record
	 *
	 * @param Audit $details
	 * @throws \ArgumentError
	 */
	private function intakeDetails(Audit $details) {
		$details = array_get($details->getDetails(), 'items.0', []);
		$details['ttfb'] = data_get($this->test, 'time-to-first-byte.numericValue', 0);
		$details['firstPaint'] = data_get($this->test, 'first-meaningful-paint.numericValue', 0);
		$details['interactive'] = data_get($this->test, 'interactive.numericValue', 0);
		$details['speedIndex'] = (int)data_get($this->test, 'speed-index.numericValue', 0);
		$this->diagnostics = new InsightDetails($details);
	}

	/**
	 *
	 * @return string base64-encoded
	 */
	public function getScreenshot(): ?string {
		return data_get($this->test, 'final-screenshot.details.data');
	}

	public function getStart() {
		return $this->begin;
	}

	public function getDetails(): InsightDetails
	{
		return $this->diagnostics;
	}
}