<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2020
	 */

	namespace apps\webapps\models;

	use Template\Searchlet;

	class InstallableAppSearch extends Searchlet
	{
		public function operations(): array
		{
			return [];
		}

		public function formAction(): ?string
		{
			return null;
		}

		public function specs(): array
		{
			return [
				'name' => 'App Name',
			];
		}

		public function filterClass(): string
		{
			return 'filter-apps';
		}

		public function searchText(): string
		{
			return $_GET['app'] ?? '';
		}
	}

