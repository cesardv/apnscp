@include('partials.bulk-apps')
<hr/>
@include('partials.search')
<hr/>
<div class="row">
	@foreach ($Page->mergeHostsApps() as $docroot => $meta)
		@continue(empty($docroot))

		@php
			$pane = $proxy->get($meta['hostname'], $meta['path'] ?? null);
			$href = \HTML_Kit::new_page_url_params(null, [
				'hostname' => $pane->getHostname(),
				'path'     => $pane->getPath()
			]);
		@endphp
		@if ($Page->isWideFormat())
			@include('layout.list-wide')
		@else
			@include('layout.list-compact')
		@endif

	@endforeach
</div>