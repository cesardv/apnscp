<div class="col-12 item hostname mb-1" data-hostname="{{ $pane->getHostname() }}" data-type="{{ $pane->getApplicationType() }}"
     data-docroot="{{ $pane->getDocumentRoot() }}">
	<div class="d-flex align-items-center">
		<div class="mt-1">
			@include('layout.list-actions')
		</div>

		<div class="d-inline-block form-control-static mt-1 mx-2 @if ($pane->isSubdomain()) subdomain @else domain @endif">
			{{ $pane->getLocation() }}
		</div>

		<div class="meta-inline form-control-static mx-2 mt-1">
			@include('layout.app-meta')
		</div>


		<div class="ml-auto mr-0 attributes d-none d-lg-inline-flex align-items-center">
			<span class="docroot ml-3 d-none attribute">
				<i class="fa fa-folder"></i>
				{{ $pane->getDocumentRoot() }}
			</span>

			<span class="type ml-3 d-none attribute">
				<i class="fa fa-question"></i>
				{{ $pane->getApplicationType() }}
			</span>

			<span class="hostname ml-3 d-none attribute">
				<i class="fa fa-globe"></i>
				{{ $pane->getHostname() }}
			</span>
		</div>
	</div>
</div>