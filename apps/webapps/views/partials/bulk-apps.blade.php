<form method="post" id="massDetectionForm">
	<fieldset class="">
		<button class="btn btn-secondary ajax-wait mr-2 my-1" type="button" name="scan-all" id="scanAll">
			<i class="fa fa-eye"></i>
			Scan for apps
		</button>
		<button class="btn btn-secondary ajax-wait mr-2 my-1" id="updateAll" disabled type="button" name="scan-all"
		        id="scanAll">
			<i class="ui-action ui-action-update"></i>
			Update all apps
		</button>
		<label class="custom-control custom-checkbox my-1"
		       for="showDetected">
			<input type="checkbox" id="showDetected" class="custom-control-input"
			       name="show-detected" value="1"/>
			<span class="custom-control-indicator"></span>
			Display only detected apps
		</label>
	</fieldset>
</form>