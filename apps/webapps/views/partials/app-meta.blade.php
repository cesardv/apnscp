<div class="app-meta">
	<form action="{{ \HTML_Kit::page_url_params() }}" method="post" id="metaForm" class="">
		<h4 class="align-content-center mx-auto d-flex text-center">
			<div class="mx-auto align-items-center d-inline-flex">
				<figure title="{{ ucwords($app->getName()) }}" class="mr-1 align-self-baseline align-middle app-icon app-icon-sm mb-0 mr-0 d-inline-flex @if ($app->isInstalling()) active @endif">
					@php
						$iconFamily = $app->getPane()->displayIcon() ?: 'unknown';
						if ($app->isInstalling()) {
							$iconFamily = 'unknown';
						}
					@endphp

					@includeFirst(["@webapp(" . $iconFamily . ")::icon", 'partials.icons.global'], ['appName' => $app->getPane()->displayIcon()])
				</figure>
				<span>
					App Meta
				</span>
			</div>
		</h4>
		<hr />
		<div class="card border-0">
			<div class="card-block pt-0 px-0">
				<div class="ui-webapp-panel row">
					@includeWhen(SCREENSHOTS_ENABLED, 'master::partials.shared.wa-screenshot',
						['href' => $pane->getUrl(), 'hoverText' => _('Visit')]
					)
				</div>
				@unless (SCREENSHOTS_ENABLED)
				<h5>Type</h5>
				@endunless
				<div class="card-text justify-content-center">
					@unless (SCREENSHOTS_ENABLED)
						<p>
							{{ ucwords($app->getName()) }}
						</p>
					@endunless
					<div class="mb-3">
						<div class="btn-group">
							<button type="button" id=""
							        class="dropdown-toggle btn btn-secondary ui-action ui-action-label"
							        name="download" data-toggle="dropdown" aria-haspopup="true"
							        aria-expanded="false" value="{{ $app->getAppRoot() }}">
								<i class="fa ui-action-download"></i>
								Download
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<div class="dropdown-menu dropdown-menu-right">
								<button type="submit" name="download" class="dropdown-item "
								        value="{{ $app->getAppRoot() }}">
									<i class="fa-fw fa-folder fa"></i>
									Site Files
								</button>
								<button type="submit" name="download-db" value=""
								        class="dropdown-item dbname">
									<i class="fa-fw fa-database fa"></i>
									Database
								</button>
							</div>
						</div>
					</div>

					<h5>Path</h5>
					<p class="card-text text-truncate">
						@if (($docroot = $app->getDocumentRoot()) !== $appPath)
							<i data-toggle="tooltip" data-title="Linked from {{ $docroot }}" class="ui-action-symlink ui-action"></i>
						@endif
						{{ $appPath }}
					</p>
					@if ($app->hasUninstall())
						<div class="float-xs mb-3">
							@include('partials.actions.uninstall')
						</div>
					@endif

					@if (!SCREENSHOTS_ENABLED && !$pane->getPath())
						<h5>URL</h5>
						<p class="card-text text-truncate"><a href="{{ $pane->getUrl() }}">{{ $pane->getUrl() }}</a></p>
					@endif

					@if ($app->hasAdmin())
						<h5>Admin Portal</h5>
						@php $admin = $pane->getUrl() . $app->getAdminPath(); @endphp
						<p class="card-text text-truncate">
							<a href="{{ $admin }}">
								{{ $admin }}
							</a>
						</p>
					@endif

					@if ($app->isInstalling() || $app->applicationPresent())
						<h5>Version</h5>
						<p class="card-text">
							{{ $app->getVersion() }}
							@include('partials.version-check', ['mode' => $app->versionStatus($app->getVersion())])
						</p>
						@if ($app->versionStatus($app->getVersion()) === false)
							<button type="submit" id="updateNote"
							        class="ajax-wait btn btn-secondary ui-action ui-action-label" name="update"
							        value="{{$app->getDocumentMetaPath()}}">
								<i class="ui-action ui-action-update"></i>
								Update
							</button>
						@endif
					@endif
				</div>
			</div>
			@includeWhen(!$app->applicationPresent() && !$app->isInstalling() && !$Page->installPending(), 'partials.options-wrapper')
		</div>
	</form>
</div>