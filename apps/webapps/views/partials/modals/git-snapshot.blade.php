@component('theme::partials.app.modal')
	@slot('title')
		Snapshots &ndash; {{ $app->getHostname() }}
	@endslot
	@slot('id')
		createModal
	@endslot
	<div>
		<h5>Create snapshot</h5>
		<div class="">
			<fieldset class="form-group">
				Description
				<label class="custom-control custom-checkbox mb-0">

					<input name="git-comment" type="text" class="form-control" value="{{ _("User snapshot") }}"
					       required/>
				</label>
			</fieldset>
		</div>
	</div>
	@slot('buttons')
		<button type="submit" class="btn btn-primary"
	        name="git-checkpoint" value="{{ $app->getDocumentMetaPath() }}">
			<i class="ui-action ui-action-snapshot"></i>
			Create Snapshot
		</button>
	@endslot
@endcomponent