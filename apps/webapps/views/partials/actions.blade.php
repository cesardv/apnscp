<h5>Development</h5>
@includeWhen($app->allowGit(), 'partials.actions.git')

@includeWhen($app->hasReconfigurable('duplicate'), 'partials.actions.duplicate')

@includeWhen($app->getAppRoot(), 'partials.actions.edit-database')

@includeWhen($app->getAppRoot(), 'partials.actions.file-manager')


<h5>Security</h5>
@include('partials.actions.fortification')

@include('partials.actions.audit')

@includeWhen(ANTIVIRUS_INSTALLED, 'partials.actions.antivirus')

<h5>Access</h5>
@includeWhen($app->hasChangePassword(), 'partials.actions.change-password', ['app' => $app])

@includeWhen($app->hasRecovery(), 'partials.actions.recovery')

<h5>Behavior</h5>
@include('partials.actions.manifest')

@include('partials.actions.detect')

@include('partials.actions.options')

@includeWhen($app->hasReconfigurable('migrate'), 'partials.actions.migrate')

@if ($app->applicationPresent())
	<h5>{{ $app->getName() }} Features</h5>
	@includeIf("@webapp(" . $app->getModuleName() . ")::actions")
	@includeIf("@webapp(" . $app->getAppFamily() . ")::actions", ['app' => $app])
@endif