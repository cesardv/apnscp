$(document).ready(function () {
	var __modify_htmldir = true;
	$('#submitBtn').ajaxWait();
	$('#browse_path').click(
		function () {
			var path = $('#subdomain_fs').filter(':not(.hinted)').val() || '/var/www';
			apnscp.explorer({
				selected: path, selectField: '#subdomain_fs', onLoad: function () {
					$('#select_dir').click(function () {
						/** @todo move #select_dir to apnscp.js var */
						__modify_htmldir = false;
					})
				}
			});
		});

	$('select.multiselect').multiSelect({
		noneSelected: "select domain(s)",
		selectAllText: "Global subdomain",
		oneOrMoreSelected: "% domains bound"
	});
	$('#subdomain').change(function () {
		if (!__modify_htmldir) return false;
		var token = $.trim($(this).val()).replace(/^www\./i, "");
		if (!token) {
			return;
		}
		var suggested = '/var/www/' + token;
		var $selectedDomains = $(':input[name="domains[]"]:checked');
		if ($selectedDomains.length === 1) {
			suggested += '.' + $selectedDomains.val();
		}
		$('#subdomain_fs').val(suggested);
	});
	$('#fallthrough').change(function () {
		if ($(this).prop('checked')) {
			$('#subdomain').val("").prop('disabled', true);
		} else {
			$('#subdomain').prop('disabled', false).focus();
		}
		return true;
	});

	$('[data-toggle=tooltip]').tooltip();

	$('#addForm').submit(function () {
		// disabling input inhibits posting the form control...
		/*var $el = $('<input type="hidden" value="1" />').attr('name', $('#submitBtn').attr('name'));
		$('i', '#submitBtn').removeClass('ui-action-add').addClass('fa-spinner fa-pulse').end().prop('disabled', true).append($el);
		return true;*/
	});

});
