<h3>Add Subdomain</h3>

<div class="row">
	<div class="col-12 col-md-6 col-xl-3 form-group" id="subdomainType">
		<label class="d-block">Subdomain</label>
		<div class="input-group">
			<button type="button" name="subdomainAdvanced" id="subdomainAdvanced"
			        class="btn btn-border-thin btn-secondary dropdown-toggle" data-toggle="dropdown"
			        aria-haspopup="true" aria-expanded="false">
				<span class="sr-only">Toggle Options</span>
			</button>
			<div class="dropdown-menu dropdown-menu-form" aria-labelledby="subdomainAdvanced">
				<label class="dropdown-item form-control-static" data-toggle="tooltip"
				       title="A fallthrough is hit when a hostname does not match any existing subdomains">
					<label class="custom-control custom-checkbox mb-0" for="fallthrough">
						<input type="checkbox" class="custom-control-input" name="subdomain_ft" id="fallthrough"
						       value="1"/>
						<span class="custom-control-indicator"></span>
						<span class="fallthrough-indicator">
								<i class="fa fa-sign-in fa-rotate-90"></i>
							</span>
						Create subdomain fallthrough
					</label>
				</label>
			</div>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-cloud"></i></span>
				<input type="text" class="form-control" id="subdomain" name="subdomain" value=""/>

			</div>

		</div>

		<div class="input-group">

		</div>
	</div>

	<div class="col-12 col-md-6 col-xl-3 form-group" id="affinityType">
		<div>
			<label data-toggle="tooltip" title="Global binding creates a subdomain on all domains now and future"><i
						class="fa fa-sticky-note-o"></i> Domain Binding</label>
			<div class="input-group align-items-center">
				<span class="input-group-addon"><i class="fa fa-bullseye"></i></span>
				<select class="form-control custom-select multiselect" name="domains[]" id="local_domains">
					@foreach ($Page->list_domains() as $domain)
						<option value="{{ $domain }}">{{ $domain }}</option>
					@endforeach
				</select>
			</div>

		</div>
	</div>

	<div class="col-12 col-md-6 col-xl-6 form-group">
		<label class="" data-toggle="tooltip" title="Website content for the subdomain is served from here"><i
					class="fa fa-sticky-note-o"></i> Document Root</label>
		<div class="input-group doc-root" role="group">
			<span class="input-group-addon"><i class="fa fa-folder"></i></span>
			<input type="text" class="form-control" name="subdomain_path" id="subdomain_fs" value=""/>
			<span class="btn-group">
					<button id="browse_path" type="button" class="btn btn-secondary" name="browse_path">
                        <i class="fa fa-folder-open"></i>
                        Browse
                    </button>
				</span>
		</div>
	</div>

	<div class="col-12 col-md-6 col-xl-3 form-group">
		<label class="hidden-xs-down input-group">&nbsp;</label>

		<div class="btn-group">
			<button type="submit" value="Add Subdomain" id="submitBtn" class="btn btn-primary" name="Add_Subdomain">
				<!-- disabled form control disables posting... -->
				<i class="ui-action-label ui-action-add fa"></i>
				Add Subdomain
			</button>
			<button class="btn btn-secondary dropdown-toggle ui-action ui-action-advanced ui-action-label"
			        data-toggle="collapse" aria-controls="advancedOptions" data-target="#advancedOptions"
			        aria-expanded="true">
				<span class="sr-only">Toggle Advanced</span>
			</button>
		</div>
	</div>
</div>

<div class="row form-group">
	<div class="collapse mt-1 col-12" id="advancedOptions">
		<h4>Advanced Options</h4>
		<fieldset class="form-group">
			<label class="form-label">
				User Ownership
			</label>
			<select class="form-control custom-select" name="user">
				@foreach ($Page->getSubdomainUsers() as $user)
					<option value="{{ $user }}">
						{{ $user }}
					</option>
				@endforeach
			</select>
		</fieldset>

		<fieldset class="form-group">
			<label class="form-label">
				<a href="/apps/webapps" class="ui-action ui-action-label ui-action-switch-app">Webapp</a>
				Preload
			</label>
			<select class="form-control custom-select" name="app-preload">
				<option value="">None</option>
				<option value="wordpress">Wordpress</option>
				<option value="wpperfstack">Wordpress + Performance Stack</option>
				<option value="wpsecstack">Wordpress + Security Stack</option>
				<option value="wprectack">Wordpress + Security + Performance Stack</option>
				<option value="drupal">Drupal 7</option>
				<option value="drupal8">Drupal 8</option>
			</select>
		</fieldset>
	</div>
</div>