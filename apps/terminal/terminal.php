<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\terminal;

	use Opcenter\Crypto\NaiveCrypt;
	use Page_Container;

	class Page extends Page_Container
	{
		public function __construct()
		{
			parent::__construct();
			\Page_Renderer::show_full_help();
			$this->add_css('#ui-app { padding: 0 !important;};', 'internal');
			$this->add_javascript('
    $( "#terminal-container" ).resizable({
      handles: "n, s",
      maxWidth: $("#ui-app").width()
    });', 'internal', true, true);
			$this->add_head('<meta http-equiv="X-UA-Compatible" content="IE=edge" />');

		}

		public function getLogin()
		{
			$ucard = \UCard::get();
			$login = $ucard->getUser();
			$domain = $ucard->getDomain();

			return $login . ($domain ? '%23' . $domain : '');
		}

		public function getPassword()
		{
			// don't set password for now, too many reports of passwords
			// getting entered in plaintext
			return '';
			if (!\Util_HTTP::isSecure() && !is_debug()) {
				return '';
			}
			if (empty($_SESSION['password']) || !($_SESSION['password'] instanceof NaiveCrypt)) {
				return (string)$_SESSION['password'];
			}
			return $_SESSION['password']->get();
		}
	}